import axios from 'axios';
import { API_URL } from "./constants";

export const callApi = (endpoint, method = 'POST', body, fileToUpload = false) => {
    // localStorage.setItem('loader', true);
    let headers = {};
    let authToken = false;
    let baseUrl = `${API_URL}${endpoint}`
 
    // localStorage.setItem('loader', true);
    let isTokenAvailable = localStorage.getItem('token');
    
    if(isTokenAvailable){
        authToken = isTokenAvailable;
    }

    if (method) {
        headers['X-Requested-With'] = 'XMLHttpRequest';
    }

    if (fileToUpload) {
        headers['Content-Type'] = 'multipart/form-data';
    } else {
        headers['Content-Type'] = 'application/json;charset=UTF-8';
        headers['Cookie'] = 'authorization=3f7ba011281b14c191601c5be6d4644c13ba2596; authenticate=3f7ba011281b14c191601c5be6d4644c13ba2596; csrftoken=yfDBRi7tb39pCYhOve6EmHYIUZuW4ROOkAWxQRNYq5CCogLHTTPIQKlnGrnqrcId';
        headers['Accept'] = 'application/json, text/plain, */*';
        headers['Connection']= 'keep-alive';
        headers['X-CSRFToken']= 'h6DU29HvP6GLpTquIsFWSh6OZSugTEgRI0AH3iGFi5K65yJP3nQQnSbcDjZqLk0D';
        // headers['Accept-Language'] = 'en-GB,en-US;q=0.9,en;q=0.8';
        // headers['Sec-Fetch-Mode'] = 'cors'
    }

    if (authToken) {
        headers['Authorization'] = `Token ${authToken}`;
    }

    return axios({
        method: method,
        url: baseUrl,
        data: body,
        headers: headers
    }).then((response) => {
        localStorage.setItem('loader', false);
        return response;
    }).catch((err) => err);
}
