import axios from "axios";
import { toast } from "react-toastify";

export default {
  setupInterceptors: store => {
    // Add a response interceptor
    axios.interceptors.response.use(
      function (response) {

        if (response.data.code >= 400 && response.data.code <= 500) { // in case response status is 200 and response code is >= 400
          toast.error(response.data.message);
        }

        let dataReturn = response.data;
        dataReturn['code'] = response.status;
        return dataReturn;
      },
      function (error) { // in case request error code is >= 400
        //catches if the session ended!
        if(error && error.response && error.response.data == "Unauthorized." && error.response.status == 401){
          localStorage.clear();
          window.location.href = 'http://localhost:3000'; // change this to real site address
        } 

        if (error && error.response && error.response.status) {
          localStorage.setItem('loader', false);
          // if (error.response.status === 401) {} 
          // You are not authorized 
          // store.dispatch({ type: REFRESH_TOKEN });
          toast.error(error.response.data.message);
          return Promise.reject(error.response.data);
        }
      }
    );
  }
};
