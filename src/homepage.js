import React, { Component } from 'react'
import {
  Button, Container, Row, Col, Carousel,
  CarouselItem,
  CarouselIndicators,
  Card,
  CardBody,
  CardImg,
  CardTitle,
  CardText,
  CardHeader,
  Modal, ModalHeader, ModalBody
} from 'reactstrap';
import Header from "./header";
import Footer from "./footer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


const items = [
  {
    id: 1,
    src: require('./assets/images/review-01.png'),
    altText: 'WikiReview 1',
    title: `Sammy's Furniture Store`,
    comment: `This Store had a sign out that said only service dogs could come in. I had my two dogs with me and I wanted to come in anyway. The employee asked if my dogs were service dogs and I said no, but I wanted to shop anyway. She said she was sorry, but I could not bring then in. How rude! I won't be back!`,
    rating: 1
  },
  {
    id: 2,
    src: require('./assets/images/review-02.png'),
    altText: 'WikiReview 2',
    title: `MatchBox Brew`,
    comment: `I had a reservation for 8pm here and I was running a little behind. I did not want to lose my table so I put the pedal to the metal and a cop pulled me over! I got a speeding ticket and had to pay a fine al because I wanted to make my reservation at 8 at the restaurant. I was so upset that I was not able to enjoy muself at all.`,
    rating: 1
  },
  {
    id: 3,
    src: require('./assets/images/review-03.png'),
    altText: 'WikiReview 3',
    title: `Firehouse Kitchen`,
    comment: `I came to this restaurant last week and had a terrible time. My girfriend of two years broke up with me at this restaurant and now the experience is completely ruined for me. I won't ever go back here because of it!`,
    rating: 1
  }
];

class Homepage extends Component {
  constructor(props) {
    super(props)
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    // document.body.scrollTop = 0; // For Safari
    this.state = {
      activeIndex: 0,
      animating: false,
      modal: false, 
    }
  }

  toggle = () => {
    this.setState({ modal: !this.state.modal })
  }

  next = () => {
    let { animating, activeIndex } = this.state
    if (animating) return;
    let activeIdx = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    this.setState({ activeIndex: activeIdx })
  }

  previous = () => {
    let { animating, activeIndex } = this.state
    if (animating) return;
    const activeIdx = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    this.setState({ activeIndex: activeIdx })
  }

  goToIndex = (newIndex) => {
    let { animating, activeIndex } = this.state;
    if (animating) return;
    this.setState({ activeIndex: newIndex })
  }

  render() {
    let { activeIndex, modal } = this.state;
    return (
      <div className="App">
        <Header />
        <header className="masthead bg-dark text-light position-relative">
          <Container className="h-100 my-auto">
            <Row className="h-100 align-items-center justify-content-center text-center">
              <Col lg={10}>
                <div className="mb-5">
                  <h1 className=" text-white mb-3">
                    WikiReviews is the solution for corporations to enhance their online reputations.
                </h1>
                </div>
                <div>
                  <div className="mb-4">
                    <Button color="dark" size="lg" className="btn-circle" onClick={this.toggle}>
                      <FontAwesomeIcon
                        size="lg"
                        className="ml-1"
                        icon={["fas", "play"]}
                      />
                    </Button>
                  </div>
                  <div className="fs-24 ff-alt">See how it works</div>
                </div>
              </Col>
            </Row>
          </Container>
          <div id="scrollDown" className="d-none d-lg-block">
            <div className="text-center">
              <div className="arrow-icon">
                <a href="#wikiDifference" className="text-decoration-none d-inline-block">
                  <span className="text-white d-block mb-3 text-shadow-sm text-uppercase font-weight-bolder">Discover the <br /> WikiReview Difference</span>
                  <svg width="40" viewBox="0 0 50 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M49.6821 0.491037C49.2581 0.0671151 48.569 0.0671151 48.145 0.491037L25.0011 23.6372L1.85497 0.493279C1.43104 0.069357 0.741864 0.069357 0.317942 0.493279C-0.105981 0.917202 -0.105981 1.60638 0.317942 2.0303L24.2315 25.9439C24.4446 26.1547 24.7228 26.2613 25.0011 26.2613C25.2794 26.2613 25.5576 26.1548 25.7686 25.9418L49.6821 2.02806C50.106 1.60414 50.106 0.91496 49.6821 0.491037Z" fill="white"></path>
                  </svg>
                </a>
              </div>
            </div>
          </div>
        </header>

        {/* WikiDifference */}
        <section className="page-section bg-dark text-light" id="wikiDifference">
          <Container>
            <Row className="justify-content-center">
              <Col lg="10" className="text-center">
                <h2 className="section-title text-white mt-0 mb-3">The WikiReviews Difference</h2>
                <p className="mb-4 fs-32">We won't do a bait &amp; switch on you</p>
                <hr className="divider light mb-4" />
                <div>
                  <p>Remember when the largest social media site allowed you to message out to your followers for free?</p>
                  <p>Remember all of the time and energy you spent getting followers and now you have to pay the site just to message out to them?</p>
                  <p>The same site lured you in with promises to post content to your followers for free, but all of that has changed. Now only 1-3% of your posts are shown to some of your followers unless you pay a lot of money for all of your followers to see all of your content.</p>
                  <p className="font-weight-bold">Feel betrayed?</p>
                  <p>Well, you're not alone! The solution is WikiReviews.</p>
                  <p>WikiReviews is both a social media and review site and we won't ever limit posts shown to your followers unless they choose not to see it.</p>
                </div>
              </Col>
            </Row>
          </Container>
        </section>

        <section className="page-section text-dark">
          <Container>
            <Row className="justify-content-center">
              <Col lg={10} className="text-center">
                <h2 className="section-title text-dark mt-0 mb-4">Tired of not being able to dispute fake and irrelevant reviews?</h2>
                <hr className="divider dark mb-4" />
              </Col>
            </Row>
          </Container>
          <Container fluid className="container-lg">
            <Row className="justify-content-center">
              <Col xl={10}>
                <div className="text-center">
                  <Carousel
                    activeIndex={activeIndex}
                    next={this.next}
                    previous={this.previous}
                  >
                    {items && Array.isArray(items) && items.length > 0 ?
                      items.map((item) => {
                        return (
                          <CarouselItem
                            onExiting={() => this.setState({ animating: true })}
                            onExited={() => this.setState({ animating: false })}
                            key={item.id}
                          >
                            <Card className="flex-md-row shadow m-4 rounded">
                              <CardHeader className="p-0 col-md-5 bg-white text-md-left border-0">
                                <CardImg height="100%" src={item.src} alt={item.altText} style={{ objectFit: 'cover' }} />
                              </CardHeader>
                              <CardBody className="col-md-7">
                                <div className="d-flex flex-column h-100">
                                  <CardTitle className="ff-alt text-dark fs-20 font-weight-bold">{item.title}</CardTitle>
                                  <div className="my-auto">
                                    <blockquote className="blockquote">
                                      <CardText className="fs-15 font-weight-semi-bold">
                                        <FontAwesomeIcon size="2x" icon={["fas", "quote-left"]} />
                                        <span className="mx-2">{item.comment}</span>
                                        <FontAwesomeIcon className="align-top" size="2x" icon={["fas", "quote-right"]} />
                                      </CardText>
                                    </blockquote>
                                  </div>
                                  <div className="mt-auto">
                                    {/* {item.rating} */}
                                    <div>
                                      <FontAwesomeIcon size="2x" icon={["fas", "star"]} />
                                      <FontAwesomeIcon size="2x" icon={["far", "star"]} />
                                      <FontAwesomeIcon size="2x" icon={["far", "star"]} />
                                      <FontAwesomeIcon size="2x" icon={["far", "star"]} />
                                      <FontAwesomeIcon size="2x" icon={["far", "star"]} />
                                    </div>
                                  </div>
                                </div>
                              </CardBody>
                            </Card>
                          </CarouselItem>
                        );
                      }) : null}
                    <CarouselIndicators className="position-relative mt-2 type-dots dark" items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
                  </Carousel>
                </div>
              </Col>
            </Row>
          </Container>
          <Container>
            <Row className="justify-content-center">
              <Col lg={10} className="text-center">
                <div>
                  <p>So many business owners are put at an unfair disadvantage by getting poor reviews by either fake reviews or reviews that are irrelevant to the service the business provides.</p>
                  <p>At WikiReviews, we have the perfect solution which allows business owners to dispute fake and irrelevant reviews.</p>
                  <p className="font-weight-bold">All parties can upload any evidence to support their case and then we ask the community to vote on what should happen to that review.</p>
                  <p>This will prevent businesses from getting hurt by fake and irrelevant reviews while creating a strong community with real, unbiased reviews.</p>
                </div>
              </Col>
            </Row>
          </Container>
        </section>

        {/* Promise Section */}
        <section className="page-section bg-dark text-white" id="ourPromise">
          <Container>
            <Row className="justify-content-center">
              <Col lg="10" className="text-center">
                <div className="mb-4">
                  <img src={require('./assets/images/w-brand.png')} className="img-fluid" alt="WikiReview Logo" />
                </div>
                <h2 className="section-title text-white text-shadow mt-0 mb-4">Our promise to you</h2>
                <hr className="divider light mb-4" />
                <div>
                  <p className="text-shadow">On WikiReviews, you have unlimited posting on our platform where all your followers see all your posts in their feeds. We do not limit posts nor do we ask companies to boost (ie, pay extra for) their posts.</p>
                  <p className="text-shadow">Unlimited messaging to all your followers without having to pay extra.</p>
                  <p className="text-shadow">Whether you have 1 or 5 million followers, the cost will be the same.</p>
                  <p className="text-shadow">We also promise to cap the maximum annual price increase, if any, at 10%</p>
                </div>
              </Col>
            </Row>
          </Container>
        </section>

        <section className="page-section bg-dark text-light">
          <Container>
            <Row className="justify-content-center">
              <Col lg className="mb-5 mb-lg-0">
                <h3 className="text-center fs-40">What is WikiReviews?</h3>
                <div className="mt-4">
                  <ul className="list-unstyled text-justify check-list">
                    <li className="mb-3">WikiReviews is a full-featured community review site dedicated to reinserting a healthy dose of humanity into the world of online reviews.</li>
                    <li className="mb-3">We believe today's online review sites are flawed, unbalanced and unfair and we've decided to do something about it.</li>
                    <li className="mb-3">At WikiReviews, we're bringing fairness back into the review process by ditching algorithms and putting the power in your hands... where it belongs.</li>
                    <li className="mb-3">We've created a wiki-based social review platform that allows users to share their experiences within a trusted community and get connected to great businesses like yours</li>
                  </ul>
                </div>
              </Col>
              <Col xs='auto' className="align-self-center d-none d-lg-block">
                <img src={require('./assets/images/hr-vertical.png')} className="img-fluid" alt="line" />
              </Col>
              <Col lg>
                <h3 className="text-center fs-40">Benefits of WikiReviews</h3>
                <div className="mt-4">
                  <ul className="list-unstyled text-justify check-list">
                    <li className="mb-3"> Connect directly with consumers. Comment on reviews, offer promotions and take your customer service to the next level.</li>
                    <li className="mb-3">No more anonymous flaming! Our advanced participation system lets the community authenticate users, curb trolls and minimize fake reviews.</li>
                    <li className="mb-3">Customize settings to personalize your listing, mitigate employee responses and receive advanced data analysis on your customer base.</li>
                  </ul>
                </div>
              </Col>
            </Row>
          </Container>
        </section>

        <Footer />

        <Modal isOpen={modal} toggle={this.toggle} className={"videoModal"} >
          <ModalHeader toggle={this.toggle}></ModalHeader>
          <ModalBody className="p-0">
            <video width="100%" controls allowFullScreen autoPlay loop>
              <source src="https://s3-us-west-2.amazonaws.com/wrstaticvideos/final_video2/biz_wikireviews_master_1.mp4" type="video/mp4" />
            </video>
          </ModalBody>

        </Modal>

      </div>
    )
  }
}

export default Homepage;
