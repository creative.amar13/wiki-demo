import React, { Component } from 'react';
import { connect } from "react-redux";
import { BrowserRouter as Router, Switch, Route, withRouter, Link, Redirect } from 'react-router-dom';
import ReactJoyride, { CallBackProps, EVENTS, STATUS } from "react-joyride";
import { Button } from "reactstrap";
import Homepage from "./homepage";
import About from "./component/pages/about";
import Faq from "./component/pages/faq";
import Terms from "./component/pages/terms";
import Dashboard from "./component/pages/dashboard";
import Settings from "./component/pages/admin/settings";
import PublicRoute from "./component/PublicRoute";
import PrivateRoute from "./component/PrivateRoute";
import manifesto from './component/pages/manifesto';
import Press from './component/pages/press';
import PrivacyPolicy from './component/pages/privacy-policy';
import Guidelines from './component/pages/guidelines';

const loaderExludedRoutes = ['/', '/about', '/faq', '/terms', '/menifesto', '/press', '/privacy-policy', '/guidelines'];

const preDefinedSteps = [
  {
    target: ".step-1",
    // title: `Hello ${this.props.profileData && this.props.profileData.user && this.props.profileData.user.first_name !== null ? this.props.profileData.user.first_name : 'xyz'} and welcome to WikiReviews!`,
    content:
      "This is a brief tour that will show you how to setup your corporate account. This includes how to set up users, roles and branches.  You will also learn how to fill out company information so it is properly displayed for each location.  Finally, you will learn how to create posts, engage with your customers and respond to and dispute reviews.",
    disableBeacon: true,
    disableOverlayClose: true,
    hideCloseButton: true,
    placement: 'center',
    spotlightClicks: true,
    styles: {
      options: {
        zIndex: 1000,
      },
    },
  },
  {
    target: ".step-2",
    title: 'Statistics',
    content:
      "You can easily see a dashboard summary of all the locations monthly statistics as well as the number of (put in red symbol) overdue, (put in yellow symbol) pending and (put in green symbol) completed tasks.",
    placement: "right",
    disableBeacon: true,
  },
  {
    target: ".step-3",
    content:
      "Click on colored circles and tabs with numbers to filter the results.",
    placement: "top",
  },
  {
    target: ".step-4",
    content:
      "Modify the dashboard statistics by time frame by using the pulldowns here",
    placement: "left",
  },
  {
    target: ".step-5",
    content:
      "The listing owner guide is a quick reference guide to use at any time.  This is simply a template which can be used or it can be edited to suit your needs.",
    placement: "left",
  },
  {
    target: ".step-6",
    content:
      "The listing owner guide is a quick reference guide to use at any time.  This is simply a template which can be used or it can be edited to suit your needs.",
    placement: "left",
  },
  {
    target: ".step-7",
    content:
      "Search here for a specific branch by city, state or zip or by customer service representative name.  This search will filter results on the map as well as all the data below the map.",
    placement: "top",
  },
  {
    target: ".step-8",
    content:
      "The information shown here can be filtered by the search box above.",
    placement: "top",
  },
  {
    target: ".step-9",
    content:
      "The data shown here reflects the filters from the search box.",
    placement: "top",
  },
  {
    target: ".step-10",
    content:
      "Clicking on the settings icon takes you to a section where you can create and edit employees, branches and user roles.  This is the main administrator section to properly manage users, branches and user roles on the platform.",
    placement: "left",
  },
  {
    target: ".step-11",
    content:
      "From here you can add or remove employees who will be responding to customer messages, reviews and inquiries.  Once an employee’s email is added, they will receive an emailed link to create their own password.  You will also assign each user to a specific role which gives users permissions to do various tasks.",
    placement: "top",
  },
  {
    target: ".step-12",
    content:
      "Here you can assign the branch location(s) to a specific employee and even assign backup branches to employees in case one is sick.",
    placement: "top",
  }
];

class RouteLinks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nextScreen: false,
      run: false,
      userFirstName: "",
      steps: preDefinedSteps,
    };
  }
  componentDidMount() {
    console.log('componentDidMount');
  }

  componentWillUnmount() {
    console.log('componentUnMount');
  }

  handleClick = () => {
    // e.preventDefault();
    // console.log('Here !!!! in handle-click', this.state);
    this.setState({ run: true, nextScreen: false, steps: preDefinedSteps });
  };

  handleJoyrideCallback = (CallBackProps) => {
    let { status, type, step, index, action, lifecycle } = CallBackProps;
    const finishedStatuses = [STATUS.FINISHED, STATUS.SKIPPED];
    // console.log({finishedStatuses},'-finishedStatuses-');
    if (finishedStatuses.includes(status)) {
      this.setState({ run: false });
    } else if ([EVENTS.TARGET_NOT_FOUND].includes(type)) {

      // console.log('ready type', type)
      // console.log('ready before', step)
      // console.log('ready CallBackProps', CallBackProps)

      if (step && step.target === '.step-11') {

        this.setState({
          run: false
        }, () => {
          localStorage.setItem('redirect', true);
          setTimeout(() => {
            // console.log(window.location.pathname, 'pathname');
            this.setState({
              steps: [
                {
                  target: ".step-11",
                  content:
                    "From here you can add or remove employees who will be responding to customer messages, reviews and inquiries.  Once an employee’s email is added, they will receive an emailed link to create their own password.  You will also assign each user to a specific role which gives users permissions to do various tasks.",
                  placement: "right",
                  disableBeacon: true,
                  disableOverlayClose: true,
                  hideCloseButton: true,
                },
                {
                  target: ".step-12",
                  // content:
                  //   "Here you can assign the branch location(s) to a specific employee and even assign backup branches to employees in case one is sick.",
                  placement: "left",
                },
                {
                  target: ".step-13",
                  content:
                    "You can easily edit employee information here such as the branches they are assigned to, backup branches and their email address.",
                  placement: "top",
                },
                {
                  target: ".step-14",
                  content:
                    "You can edit and add new branch location data here. ",
                  placement: "top",
                },
                {
                  target: ".step-15",
                  content:
                    "Here you can create/remove/edit different roles and you can assign different permissions to each role.  This will ensure that employees assigned to a role are only able to do the tasks designated. ",
                  placement: "top",
                },
                {
                  target: ".step-16",
                  content:
                    "Here you set the criteria for what is overdue, pending or completed.  All red, yellow and green colored alerts are based upon the settings below.",
                  placement: "top",
                }
              ],
              run: true,
              nextScreen: true
            })
          }, 2000);
        });
      }
    }
    // tslint:disable:no-console
    // console.groupCollapsed(type);
    // console.log(CallBackProps);
    // console.groupEnd();
    // tslint:enable:no-console
  };
  renderReactJoyRide = ({ Tooltip }) => {
    return (
      <ReactJoyride
        callback={this.handleJoyrideCallback}
        steps={this.state.steps}
        run={this.state.run}
        continuous={true}
        tooltipComponent={Tooltip}
        showProgress
        hideBackButton={false}
        disableOverlayClose={true}
        locale={{ close: 'Close' }}
        styles={{
          options: {
            arrowColor: "#fff",
            backgroundColor: "#fff",
            overlayColor: "rgba(0, 0, 0, 0.6)",
            primaryColor: "mediumaquamarine",
            textColor: "#333",
            zIndex: 1000,
          },
        }}
      />
    )
  }

  handleRedirect = () => {
    console.log('here')

    return (
      <Redirect to='/corporatesettings' />
      // <Router>
      //   <PrivateRoute component={Settings} path="/corporatesettings" exact />
      // </Router>
    )
  }

  render() {
    const Tooltip = ({
      continuous,
      index,
      isLastStep,
      step,
      backProps,
      closeProps,
      primaryProps,
      skipProps,
      tooltipProps, }) => {
        // console.log(step,'------ step------');
      if (index === "2" || index === 2) {

        if(this.state.nextScreen && step && step.target === '.step-13'){
          return (<div className="cs-toolpop" {...tooltipProps}>
            <h3>Hello {this.state.userFirstName} and welcome to WikiReviews!</h3>
            <div>This is a brief tour that will show you how to setup your corporate account. This includes how to set up users, roles and branches.  You will also learn how to fill out company information so it is properly displayed for each location.  Finally, you will learn how to create posts, engage with your customers and respond to and dispute reviews.</div>
            <div className="d-flex mt-4 mb-3">
              <Button {...primaryProps} color="primary">
                Ok
            </Button>
              <Button {...closeProps} color="link" className="ml-auto" onClick={() => { this.setState({ run: false }) }}>
                End Tour
            </Button>
            </div>
            <Link to="/corporateprofile" onClick={() => { this.setState({ run: false }) }}>
              {'Skip this part and I’ll explore on my own'}
            </Link>
          </div>)
        }
        else{
          return (<div {...tooltipProps}>

            <div>
              <div className="stat-bubble with-line" data-type="overall">
                &nbsp;
                    </div>
              <div className="stat-bubble with-line" data-type="reviews">
                &nbsp;
                    </div>
              <div className="stat-bubble with-line" data-type="branches">
                &nbsp;
                    </div>
            </div>
            <div className="intro-block" data-type="stat-intro">
              {step.content}
            </div>
  
            {continuous && (
              <Button {...primaryProps} color="primary" className="bt-btn">
                {'Got It'}
              </Button>
            )}
  
          </div>)
        }

        
        

      } else if (index === "0" || index === 0) {
        // console.log()
        if (!this.state.nextScreen) {
          return (<div className="cs-toolpop" {...tooltipProps}>
            <h3>Hello {this.state.userFirstName} and welcome to WikiReviews!</h3>
            <div>This is a brief tour that will show you how to setup your corporate account. This includes how to set up users, roles and branches.  You will also learn how to fill out company information so it is properly displayed for each location.  Finally, you will learn how to create posts, engage with your customers and respond to and dispute reviews.</div>
            <div className="d-flex mt-4 mb-3">
              <Button {...primaryProps} color="primary">
                Ok
            </Button>
              <Button {...closeProps} color="link" className="ml-auto" onClick={() => { this.setState({ run: false }) }}>
                End Tour
            </Button>
            </div>
            <Link to="/corporateprofile" onClick={() => { this.setState({ run: false }) }}>
              {'Skip this part and I’ll explore on my own'}
            </Link>
          </div>)
        } else {
          return (
            <div className="cs-toolpop" {...tooltipProps}>
              <h3> {step.title}</h3>
              <div>{step.content}</div>
              <div className="d-flex mt-4 mb-3">
                <Button {...primaryProps} color="primary">
                  {'Ok'}
                </Button>
              </div>
            </div>
          )
        }
      } else if (index == 11 || index == '11') {
        return (
          <div className="cs-toolpop" {...tooltipProps}>
            {step.title && <h3>{step.title}</h3>}
            {step.content && <div>{step.content}

            </div>}
            <div className="d-flex mt-4 mb-3">
              <Button {...primaryProps} color="primary" onClick={() => {
                this.setState({
                  run: false,
                  nextScreen: false
                })
              }}>
              </Button>
              <Button {...closeProps} color="link" className="ml-auto" onClick={() => {
                this.setState({
                  run: false,
                  nextScreen: false
                })
              }}>
                End Tour
              </Button>
            </div>
            {index === 0 ?
              <Link
                to="/corporateprofile"
                onClick={() => {
                  this.setState({
                    run: false
                  })
                }}
              >
                Skip this part and I’ll explore on my own
                </Link> :
              ""}

          </div>
        )

      } else {
        return (
          <div className="cs-toolpop" {...tooltipProps}>
            {step.title && <h3>{step.title}</h3>}
            {step.content && <div>{step.content}

            </div>}
            <div className="d-flex mt-4 mb-3">
              <Button {...primaryProps} color="primary">
                Ok
            </Button>
              <Button {...closeProps} color="link" className="ml-auto" onClick={() => {
                this.setState({
                  run: false,
                  nextScreen: false
                })
              }}>
                End Tour
            </Button>
            </div>
            {index === 0 ?
              <Link
                to="/corporateprofile"
                onClick={() => {
                  this.setState({
                    run: false
                  })
                }}
              >
                Skip this part and I’ll explore on my own
              </Link> :
              ""}

          </div>
        )
      }
    };
    return (
      <div>
        {/* <Button className="btn btn-primary test-tour" style={{ height: '200px' }} onClick={this.handleClick}>Test tour</Button>
        <Button className="btn btn-primary test-tour" style={{ height: '20px' }} onClick={() => { this.props.history.push('/corporatesettings') }}>Test tour</Button> */}

        {this.renderReactJoyRide({ Tooltip })}
        <Router>
          <Switch>
            <PublicRoute restricted={true} component={Homepage} path="/" exact />
            <PublicRoute restricted={false} component={About} path="/about" exact />
            <PublicRoute restricted={false} component={Faq} path="/faq" exact />
            <PublicRoute restricted={false} component={Terms} path="/terms" exact />
            <PublicRoute restricted={false} component={manifesto} path="/manifesto" exact />
            <PublicRoute restricted={false} component={Press} path="/press" exact />
            <PublicRoute restricted={false} component={PrivacyPolicy} path="/privacy-policy" exact />
            <PublicRoute restricted={false} component={Guidelines} path="/guidelines" exact />
            <PrivateRoute clickTourStart={this.handleClick} component={Dashboard} path="/corporateprofile" exact />
            <PrivateRoute clickTourStart={this.handleClick} component={Settings} path="/corporatesettings" exact />
          </Switch>
        </Router>
      </div>
    );
  }
}
export { loaderExludedRoutes }
export default withRouter(connect()(RouteLinks));