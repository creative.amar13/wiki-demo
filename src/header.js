import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  Button,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink,
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  Spinner,
  Label,
  ModalFooter,
  FormText,
} from "reactstrap";
import {
  AvForm,
  AvField,
  AvGroup,
  AvInput,
  AvFeedback,
  AvRadioGroup,
  AvRadio,
  AvCheckboxGroup,
  AvCheckbox,
} from "availity-reactstrap-validation";
import { Form, FormGroup, Input, Row, Col, CustomInput } from "reactstrap";
import Logo from "./assets/images/w-brand-black.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { auth_login_request, is_auth_login_true, forgot_password, signup_request } from "./actions/auth";
import { emailRegex, nameRegex } from "./utils/validation";
import { setTimeout } from "timers";
import isEmail from "validator/es/lib/isEmail";
import { stat } from "fs";

class Header extends Component {
  constructor(props) {
    super(props);
    this.refLoginForm = React.createRef();
    this.state = {
      className: null,
      isOpen: false,
      modal: false,
      fmodal: false,
      email: "",
      password: "",
      scrollEnd: false,
      isChecked: false,
      isSpinner: false,
      isLoggInDone: false,
      isdisabledLogin: true,
      isdisabledForgot: true,
      forgotemail: "",
      signUpModalToggle: false,
      signUpCompleteModalToggle: false,
      adminSignUpModalToggle: false,
      isFranchise: "Yes"
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps &&
      nextProps.auth_response &&
      nextProps.auth_response.profileid
    ) {
      let token = nextProps.auth_response.token;
      if (token !== undefined) {
        localStorage.setItem("token", token);
        this.setState({ isSpinner: false, isLoggInDone: true });
      }
    }
    if (
      nextProps &&
      nextProps.signUpRessponse
    ) {
      this.setState({ isSpinner: false, signUpModalToggle: false, signUpCompleteModalToggle: true });
    }

    if (nextProps && nextProps.auth_error) {
      this.setState({ isSpinner: false });
    }

    if (nextProps && nextProps.signUpErrorRessponse) {
      this.setState({ isSpinner: false });
    }
  }

  // static getDerivedStateFromProps(props, state) {
  //   console.log('ssssssssss', props, state)
  // } 

  toggleNavbar = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  toggle = () => {
    this.setState({ fmodal: !this.state.fmodal });
    this.setState({ modal: !this.state.modal });
  };

  modalToggle = (isTrue) => {
    if (isTrue === true) {
      this.setState({ modal: false }, () => this.props.is_auth_login_true());
    } else {
      let modal = !this.state.modal;
      if (modal) {
        this.setState({
          email: "",
          password: "",
          scrollEnd: false,
          isChecked: false,
          isSpinner: false,
          isLoggInDone: false,
          isdisabledLogin: true,
          forgotemail: ""
        });
      }
      this.setState({ modal });
    }
  };

  handleSubmitForgot = () => {
    let { forgotemail } = this.state;
    let data = {
      email: forgotemail,
    };
    this.props.forgot_password(data);
    let fmodal = !this.state.fmodal;
    this.setState({ fmodal });
    // this.modalToggle();
  };


  handleChangeForm = (e) => {
    let { name, value } = e.target;
    this.setState({ [name]: value });
  };


  handleChange = (e) => {
    let { name, value } = e.target;
    this.setState({ [name]: value }, () => this.checkValid());
  };

  handleChangeEmail = (e) => {
    let { name, value } = e.target;
    this.setState({ [name]: value }, () => this.checkValidEmail());
  };


  handleSubmit = () => {
    let { email, password } = this.state;
    let data = {
      identification: email,
      password: password,
      biz_terms: true,
      subdomain: "enterprise",
    };
    this.setState({ isSpinner: true }, () => this.props.login_request(data));
    // this.modalToggle();
  };

  handleSubmitEnterprise = () => {
    const { signup_request } = this.props
    const { firstName,
      emailAddress,
      lastName,
      jobTitle,
      phone1,
      phone2,
      phone3,
      isFranchise,
      storeLocations,
      companyOwned,
      companyName, storeLocationsUsa } = this.state
    this.setState({ isSpinner: true })
    signup_request({
      "first_name": firstName,
      "last_name": lastName,
      "email": emailAddress,
      "job_title": jobTitle,
      "company_name": companyName,
      "phone": phone1 + phone2 + phone3,
      "is_franchise": isFranchise === "Yes" ? 1 : 0,
      "store_in_usa_count": storeLocationsUsa ? storeLocationsUsa : 0,
      "companies_owned_count": companyOwned ? companyOwned : 0,
      "store_count": storeLocations ? storeLocations : 0,
      "password": "test@123"
    })
    // this.setState({ signUpModalToggle: false, signUpCompleteModalToggle: true })
    // this.modalToggle();
  };


  handleScroll = (e) => {

    let scrollHeight = e.target.scrollHeight - Math.trunc(e.target.scrollTop);
    let clientHeight = e.target.clientHeight;
    let heightDifference = scrollHeight - clientHeight;

    // const bottom =
    //   e.target.scrollHeight - Math.trunc(e.target.scrollTop) ===
    //   e.target.clientHeight;
      // if (bottom) {
      // this.setState({ scrollEnd: true, isLoggInDone: false }, () =>
      //   this.checkValid()
      // );
      if (heightDifference <=2) {
      this.setState({ scrollEnd: true, isLoggInDone: false }, () =>
        this.checkValid()
      );
    }
  };

  checkValid = () => {
    let { scrollEnd, isChecked, email, password, isdisabledLogin } = this.state;
    if (
      scrollEnd == true &&
      isChecked == true &&
      email !== "" &&
      isEmail(email) &&
      password !== ""
    ) {
      this.setState({ isdisabledLogin: false });
    } else {
      if (isdisabledLogin !== true) {
        this.setState({ isdisabledLogin: true });
      }
    }
  };

  checkValidEmail = () => {
    let { forgotemail, isdisabledForgot } = this.state;
    if (
      isEmail(forgotemail)
    ) {
      this.setState({ isdisabledForgot: false });
    } else {
      if (isdisabledForgot !== true) {
        this.setState({ isdisabledForgot: true });
      }
    }
  };

  hitAfterCheck = () => {
    setTimeout(() => {
      this.modalToggle(true);
      // this.props.history.push('/corporateprofile');
    }, 1000);
  };

  checkIsLoggedIn = () => {
    if (this.state.isLoggInDone) {
      this.hitAfterCheck();
      return <FontAwesomeIcon icon="check" size="1x" />;
    }
  };

  render() {
    let {
      email,
      password,
      scrollEnd,
      isChecked,
      isSpinner,
      isLoggInDone,
      firstName,
      emailAddress,
      lastName,
      jobTitle,
      phone1,
      phone2,
      phone3,
      isFranchise,
      storeLocations,
      companyOwned,
      companyName,
      storeLocationsUsa
    } = this.state;

    let { auth_response, auth_error } = this.props;

    return (
      <React.Fragment>
        <Navbar
          dark
          expand="md"
          className="fixed-top py-3 navbar-scrolled"
          id="siteNav"
        >
          <Container fluid className="container-lg">
            <NavbarBrand className="d-flex flex-column flex-lg-row align-items-lg-center">
              <span className="text-uppercase mr-2">{"WikiReviews"}</span>{" "}
              <span className="small">for Business Owners</span>
            </NavbarBrand>
            <NavbarToggler onClick={this.toggleNavbar} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto align-items-center my-2 my-md-0" navbar>
                <NavItem>
                  <NavLink href="#wikiDifference">
                    The WikiReviews Difference
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="#ourPromise">Our Promise</NavLink>
                </NavItem>
              </Nav>

              <div className="ml-md-4 text-center">
                <Button
                  color="primary"
                  block
                  className="px-5 px-md-4 btn-sm-inline-block"
                  onClick={() => {
                    this.setState({
                      signUpModalToggle: true
                    })
                  }}
                >
                  Signup
                </Button>
              </div>


              <div className="ml-md-4 text-center">
                <Button
                  color="primary"
                  block
                  className="px-5 px-md-4 btn-sm-inline-block"
                  onClick={this.modalToggle}
                >
                  Login
                </Button>
              </div>
            </Collapse>
          </Container>
        </Navbar>

        <Modal
          isOpen={this.state.modal}
          toggle={this.modalToggle}
          className="custom-pop"
        >
          <ModalHeader toggle={this.modalToggle}>LOG IN</ModalHeader>
          <ModalBody>
            <div className="pt-2 pb-1 px-lg-5 mb-3 flex-column d-flex justify-content-center align-items-center">
              <span className="mb-3">
                <img src={Logo} alt="Logo" />
              </span>
              <span>Already registered? Login here</span>
            </div>
            <AvForm
              ref={this.refLoginForm}
              autoComplete="off"
              onValidSubmit={this.handleSubmit}
            >

              <FormGroup>
                <div
                  className="biz_toc"
                  id="corp_terms"
                  onScroll={this.handleScroll}
                >
                  {" "}
                  <span>Terms &amp; Conditions</span>
                  <span className="blinking ml-3">Please read and scroll all the way to the bottom</span>
                  <br />
                  <br />
                  THIS DOCUMENT CONTAINS VERY IMPORTANT INFORMATION REGARDING
                  RIGHTS AND OBLIGATIONS, AS WELL AS CONDITIONS, LIMITATIONS,
                  AND EXCLUSIONS THAT MIGHT APPLY TO COMMERCIAL MERCHANTS, SMALL
                  BUSINESSES OR RETAILERS, PRODUCT MANUFACTURERS, OR INDIVIDUALS
                  USING THE WEBSITE OR SERVICES FOR ANY COMMERCIAL PURPOSE, AND
                  ANY OF THEIR AUTHORIZED AGENTS EACH, OR TOGETHER,'YOU' PAYING
                  FOR AND USING ANY OF THE COMPANYS SERVICES DEFINED BELOW, AND
                  AS PRESENTED ON BIZ.WIKIREVIEWS.COM ,AS MAY BE AMENDED FROM
                  TIME TO TIME.PLEASE READ THE TERMS AND CONDITIONS (OR THE
                  'TERMS') AND THESE MERCHANT TERMS OF USE ('SERVICE TERMS')
                  CAREFULLY.THESE SERVICE TERMS REQUIRE THE USE OF ARBITRATION
                  TO RESOLVE DISPUTES, RATHER THAN JURY TRIALS.
                  <br /> <br />
                  1. THE WIKIREVIEWS WEBSITE PROVIDES PAYING MERCHANTS FEATURES
                  CONTAINED ON BIZ.WIKIREVIEWS.COM (AS MAY BE AMENDED FROM TIME
                  TO TIME), INCLUDING, BUT NOT LIMITED TO, MANAGING YOUR
                  BUSINESS LISTING(S), OFERING A PLATFORM TO DYNAMICALLY POST
                  PROMOTIONS AND SPECIAL OFFERS TO CUSTOMERS, RESPONDING TO
                  CUSTOMER REVIEWS, ENGAGING WITH CUSTOMERS, REVIEWING USER
                  ANALYTICS, DISPUTING FAKE OR IRRELEVANT REVIEWS, AND PROMOTING
                  ORIGINAL CONTENT (TOGETHER, THE 'SERVICES').
                  <br />
                  <br />
                  BY PLACING AN ORDER FOR SERVICES FROM THIS WEBSITE OR BY
                  CLICKING 'I AGREE' TO THESE SERVICE TERMS WHEN THEY ARE
                  PRESENTED TO YOU, YOU AFFIRM THAT YOU ARE OF LEGAL AGE TO
                  ENTER INTO THIS AGREEMENT, AND YOU ACCEPT AND ARE BOUND BY
                  THESE SERVICE TERMS.YOU AFFIRM THAT IF YOU PLACE AN ORDER ON
                  BEHALF OF AN ORGANIZATION OR COMPANY, YOU HAVE THE LEGAL
                  AUTHORITY TO BIND ANY SUCH ORGANIZATION OR COMPANY TO THESE
                  SERVICE TERMS AND THE TERMS OF USE.
                  <br />
                  <br />
                  YOU MAY NOT ORDER SERVICES FROM THIS WEBSITE IF YOU (A) DO NOT
                  AGREE TO THESE SERVICE TERMS, (B) ARE NOT OF LEGAL AGE TO FORM
                  A BINDING CONTRACT WITH WIKIREVIEWS, INC., OR (C) ARE
                  PROHIBITED FROM ACCESSING OR USING THIS WEBSITE OR ANY OF THIS
                  WEBSITES CONTENTS OR SERVICES BY APPLICABLE STATE OR FEDERAL
                  LAW.
                  <br />
                  <br />
                  These Merchant Terms of Use (these 'Merchant Terms') apply to
                  the purchase of Services through www.wikireviews.com, the use
                  of the business portal contained at biz.wikireviews.com, the
                  mobile version of the website, and all assThese Merchant Terms
                  reference the Privacy Policy (together, the 'Terms'), that
                  applies generally to the use of our Website and App.You should
                  carefully review our Privacy Policy before placing an order
                  for Services.ociated subdomains, the WikiReviews mobile
                  application (the 'App'), and WikiReviews social media pages
                  (collectively, the 'Website' or the 'Site').These Merchant
                  Terms are subject to change by WikiReviews, Inc.(referred to
                  as 'WikiReviews,' 'us,' 'we,' or 'our' as the context may
                  require) without prior written notice at any time, in our sole
                  discretion, except that changes to increase payment terms may
                  require additional actions as required by applicable law.Any
                  changes to the Merchant Terms will be in effect as of the
                  'Last Updated Date' referenced on the Website or App or on
                  this page.You should review these Merchant Terms prior to
                  purchasing any Services that are available through this
                  Website or App.Your continued use of this Website or App after
                  the 'Last Updated Date' will constitute your acceptance of and
                  agreement to such changes, and you will be billed according to
                  these terms.
                  <br />
                  <br />
                  These Merchant Terms reference the Privacy Policy (together,
                  the 'Terms'), that applies generally to the use of our Website
                  and App.You should carefully review our Privacy Policy before
                  placing an order for Services.
                  <br />
                  <br />
                  2. The Services. The Services offered may change from time to
                  time with proper notice from us subject to the update
                  procedures contained in the Terms.For non-commercial users, we
                  are a free-to-use community website that provides users
                  information sourced from the feedback and commentary of other
                  users.We exercise commercially reasonable efforts to meet this
                  purpose by offering commercial users like you tiers of
                  Services at different price points, and have implemented many
                  features, content, and functionality to enable you to upload
                  certain common multimedia (in our sole discretion) to set up a
                  listing (for your business, product, or project).
                  <br />
                  <br />
                  3. Merchant Account. To use certain features of the Website or
                  App and Services specific to you, you must register for a
                  Merchant Account ('Merchant Account') by following the
                  directions specified on the Website or App.You represent and
                  warrant that:
                  <br />
                  <br />
                  (a) all required registration information you provide to us
                  related to your Merchant Account is truthful and accurate; and
                  <br />
                  <br />
                  (b) you will maintain the accuracy of such information.You may
                  downgrade (to a free account once the subscription term has
                  ended) or upgrade (to a paid account) your Merchant Account at
                  any time, for any reason, by doing so within your Merchant
                  Account.We may terminate or suspend your Merchant Account in
                  accordance with these Merchant Terms below, and suspend
                  payment accordingly.You are responsible for maintaining the
                  confidentiality of your Merchant Account using commercially
                  reasonable measures and safeguards for such login information,
                  and are fully responsible for all activities occurring under
                  your Merchant Account.You agree to immediately notify us of
                  any unauthorized or unrecognized use, or suspected
                  unauthorized use of your Merchant Account or any other breach
                  of security.We cannot and will not be liable for any loss or
                  damage arising from your failure to comply with the above
                  requirements.
                  <br />
                  <br />
                  4. License; Restrictions on our Website and App.
                  <br />
                  <br />
                  (a) Subject to these Merchant Terms, we hereby grant you a
                  non-transferable, non-exclusive, license to use the Website or
                  App and Services identified in your applicable order form for
                  your use.
                  <br />
                  <br />
                  (b) The rights granted to you in these Merchant Terms are
                  subject to the following restrictions:
                  <br />
                  <br />
                  (i) you will not license, sell, rent, lease, transfer, assign,
                  distribute, host, or otherwise commercially exploit the
                  Website or App or Services;
                  <br />
                  <br />
                  (ii) you shall not modify, make derivative works of,
                  disassemble, reverse compile or reverse engineer any part of
                  the Website or App or Services;
                  <br />
                  <br />
                  (iii) you shall not access the Website or App or Service in
                  order to build a similar or competitive service; and
                  <br />
                  <br />
                  (iv) except as expressly stated elsewhere in these Merchant
                  Terms, no part of the Website or App or Services may be
                  copied, reproduced, distributed, republished, downloaded,
                  displayed, posted or transmitted in any form or by any
                  means.Any future release, update, or other addition to
                  functionality of the Website or App or Services shall be
                  subject to these Merchant Terms.All copyright and other
                  proprietary notices on any Website or App or Services content
                  must be retained on all copies thereof.
                  <br />
                  <br />
                  (c) We reserve the right at any time to modify, suspend, or
                  discontinue the Website or App or Services or any part thereof
                  with or without notice to you.You agree that we will not be
                  liable to you or to any third party for any modification,
                  suspension, or discontinuance of the Website or App or
                  Services or any part thereof.
                  <br />
                  <br />
                  (d) Excluding your Uploaded Content (defined below), you
                  acknowledge that all the intellectual property rights,
                  including copyrights, patents, trademarks, trade dress, and
                  trade secrets in the Website or App and Services are owned by
                  us or our licensors.The provision of the Website or App or
                  Services does not transfer to you or any third party any
                  rights, title or interest in or to such intellectual property
                  rights.We and our suppliers, retailers, Merchants (as
                  applicable) and commercial partners reserve all rights not
                  granted in these Merchant Terms.
                  <br />
                  <br />
                  5. Uploaded Content.
                  <br />
                  <br />
                  (a) 'Uploaded Content' means any and all information and
                  content that you submit to, or use with the Website or App or
                  Services, including without limitation any coupons,
                  promotional codes, giveaways, samples, and other offers listed
                  by you and approved by us on the Website or App (the
                  'Promotions').You are solely responsible for the Uploaded
                  Content, and assume all risks associated with your use of the
                  Uploaded Content, including any reliance on its accuracy,
                  completeness, or usefulness by other users of the Website or
                  App and Services, or any disclosure of your Uploaded Content
                  that makes you or any third party personally identifiable.You
                  hereby represent and warrant that your Uploaded Content does
                  not violate the Acceptable Use Policy, in Section 6.
                  <br />
                  <br />
                  (b) You may not state or imply that your Uploaded Content is
                  in any way provided, sponsored or endorsed by us.We are not
                  obligated to backup any Uploaded Content and we may delete
                  Uploaded Content at any time.You are solely responsible for
                  creating any backup copies of your Uploaded Content.
                  <br />
                  <br />
                  (c) You hereby grant, and represent and warrant that you have
                  the right to grant to us an irrevocable, non-exclusive,
                  royalty-free, perpetual and fully paid worldwide license to
                  reproduce, distribute, publicly display and perform, and
                  otherwise use your Uploaded Content and to grant sublicenses
                  of the foregoing, for the purpose of including your Uploaded
                  Content in the Website or App and Services and developing and
                  distributing new, original content for use in conjunction with
                  the Website or App and Services.You agree to irrevocably waive
                  any claims and assertions of moral rights or attribution with
                  respect to your Uploaded Content.Some of your Uploaded Content
                  may continue to be publicly displayed and distributed on the
                  Website or App or Services following termination or expiration
                  of these Merchant Terms.
                  <br />
                  <br />
                  6. Acceptable Use Policy.
                  <br />
                  <br />
                  (a) You agree not to use the Website or App or Services to
                  collect, upload, transmit, display, or distribute any Uploaded
                  Content:
                  <br />
                  <br />
                  (i) that violates any third-party right, including any
                  copyright, trademark, patent, trade secret, moral right,
                  privacy right, right of publicity, or any other intellectual
                  property or proprietary right,
                  <br />
                  <br />
                  (ii) that is unlawful, harassing, abusive, tortious,
                  threatening, harmful, invasive of anothers privacy, vulgar,
                  defamatory, false, intentionally misleading, trade libelous,
                  pornographic, obscene, patently offensive (e.g, material that
                  promotes racism, bigotry, hatred, or physical harm of any kind
                  against any group or individual) or otherwise objectionable
                  material of any kind or nature or which is harmful to minors
                  in any way,
                  <br />
                  <br />
                  (iii) in violation of any law, regulation, or obligations or
                  restrictions imposed by any third party,
                  <br />
                  <br />
                  (iv) in violation of our Content Guidelines, or
                  <br />
                  <br />
                  (v) is otherwise designed, in our sole discretion, to attack
                  or damage the reputation of your competitors or other users on
                  the Website or App.
                  <br />
                  <br />
                  (b) You may not use the Services to post or communicate any
                  information in contravention of any applicable laws or
                  regulations, or in violation of our Content Guidelines (that
                  may be found at
                  https://wikireviews.com/guidelines#content-guidelines).All the
                  Uploaded Content you post on the Site must comply with all
                  applicable laws, whether national, state, or provincial,
                  including, without limitation, those regulating the
                  advertising of products and provision of any Promotions. You
                  must comply with all applicable laws, regulations, and
                  industry standards in conducting your business, and in the
                  event that any person, entity, or government authority accuses
                  you of any illegal conduct, we shall have the right in its
                  sole discretion to immediately suspend or terminate your use
                  of the Services upon notification in accordance these Merchant
                  Terms.
                  <br />
                  <br />
                  (c) Without limiting the generality of the foregoing, you may
                  not use the Services to post or communicate any information
                  (including Uploaded Content) which (i) makes any deceptive,
                  false, or misleading assertions or statements about your
                  products or services, or (ii) promotes over consumption of
                  your products or services.
                  <br />
                  <br />
                  (d) You agree not to post reviews on the Website or App (using
                  your Merchant Account or any other standard Wikireviews
                  Account) that are (i) about your own products, employees,
                  retail location, or brand, or a competitors products,
                  employees, retail location, or brand or (ii) your products or
                  competing products.Reviews based on secondhand, non-personal
                  experience, or are otherwise designed for any purpose other
                  than providing other users on the Website or App with an
                  accurate description of your personal experience, are not
                  allowed. In addition to the other restrictions contained in
                  the Content Guidelines, reviews posted on the Website or App
                  must not: (i) be written exclusively in capital letters, (ii)
                  be plagiarized, (iii) contain spam or advertisements, (iv)
                  contain personally identifying information about any person,
                  (v) contain overly detailed or sexual descriptions of an
                  individuals physical appearance, or lewd personal attacks
                  against a specific individual or group of individuals, (vi)
                  contain references to products other than the product or
                  entity being reviewed, or (vii) contain unrelated personal
                  grievances.
                  <br />
                  <br />
                  (e) Image files must exclusively feature the products they
                  illustrate and must not include body parts, messy or cluttered
                  backgrounds, currency, paraphernalia, or other any objects
                  other than the product itself.Image files must be clear, and
                  products should be centered in the image file. Image files
                  must contain an accurate depiction of the product they
                  illustrate.Image files cannot contain pornography or other
                  graphic images and must otherwise abide by the guidelines set
                  forth in this Section.
                  <br />
                  <br />
                  (f) The following guidelines apply in addition to the Content
                  Guidelines to Merchants with regard to Merchant activities and
                  actions:
                  <br />
                  <br />
                  (i) Any attempt to circumvent the established WikiReviews
                  sales process for Services or to divert WikiReviews users to
                  another website or sales process providing the same or similar
                  services is prohibited. Specifically, any advertisements,
                  marketing messages (special offers) or calls to action' that
                  lead, prompt, or encourage WikiReviews users or guests to
                  leave the WikiReviews Website or App to engage in services
                  similar to the Services provided to you are prohibited.This
                  may include the use of email or the inclusion of hyperlinks,
                  URLs or web addresses within any Merchant-generated
                  confirmation email messages or any product/listing description
                  fields.
                  <br />
                  <br />
                  (ii) In order for any Merchant to list any businesses,
                  locations, or products on the Site, you must represent and
                  warrant that your facilities, business, locations, and
                  products are in full compliance with applicable local,
                  regional, national, and international law.We do not promote,
                  nor do we facilitate, to the best of our knowledge, any
                  shipping originating from outside the United States and Canada
                  and its territories.We reserve the right to inspect and audit
                  your business registration and records in order to ensure your
                  compliance with applicable laws and these Merchant Terms of
                  Use.
                  <br />
                  <br />
                  (iii) The Merchant name (identifying a Merchants business
                  entity on the Site) must be a name that: accurately identifies
                  the seller; is not misleading; and the seller has the right to
                  use (that is, the name cannot include the trademark of, or
                  otherwise infringe on, any trademark or other intellectual
                  property right of any person).
                  <br />
                  <br />
                  (iv) Furthermore, a Merchant cannot use a business name that
                  contains an email suffix such as .com, .net, .biz, and so on.
                  <br />
                  <br />
                  (v) Unsolicited email and phone communications with
                  WikiReviews users, email and phone communications other than
                  as necessary for order fulfillment and related customer
                  service, and emails and phone calls related to marketing
                  communications of any kind (including within otherwise
                  permitted communications) are prohibited.
                  <br />
                  <br />
                  (vi) users either guests or registered users and merchants may
                  communicate with one another privately via the wikireviews
                  private messaging service,depending on the type of wikireviews
                  account utilized,subject to the terms in the content
                  guidelines.
                  <br />
                  <br />
                  (vii)Operating and maintaining multiple Merchant Accounts for
                  the same listing location is prohibited.
                  <br />
                  <br />
                  (viii) If you upload excessive amounts of Uploaded Content or
                  data repeatedly, or otherwise use the service in an excessive
                  or unreasonable way, Company may in its sole discretion
                  restrict or block your access to product listings or analytics
                  or any other functions that are being misused until you stop
                  its misuse.
                  <br />
                  <br />
                  (ix) Any attempts to manipulate ratings, feedback, or reviews
                  (related to the Business Intelligence or otherwise) is
                  expressly prohibited.In addition, Merchants may not post
                  abusive or inappropriate feedback or include personal
                  information about a transaction partner or another
                  Merchant.You may request feedback from a Buyer.
                  <br />
                  <br />
                  (x) Any attempt to manipulate search rank is prohibited. You
                  may not solicit or knowingly accept fake or fraudulent search
                  requests or communications.You may not provide compensation to
                  users for purchasing your products.In addition, you may not
                  make claims regarding a product's best/top seller rank in the
                  product detail page information, including the title and
                  description.
                  <br />
                  <br />
                  (xi) Any attempt to manipulate the WikiReviews search and
                  browse experience is expressly prohibited.
                  <br />
                  <br />
                  (g) We reserve the right (but have no obligation) to review
                  any Uploaded Content, investigate or take appropriate action
                  against you in our sole discretion if we suspect you have
                  violated the Acceptable Use Policy (including Section (e)
                  above) or any other provision of these Merchant Terms or
                  otherwise create liability for us.
                  <br />
                  <br />
                  7. Prices and Payment Terms.
                  <br />
                  <br />
                  (a) The prices we charge for using our Services are listed at
                  https://biz.wikireviews.com.
                  <br />
                  <br />
                  (b) All prices, discounts, and promotions posted on the
                  Website or App are subject to change without notice.The price
                  charged for Services will be the price in effect at the time
                  the order is placed and will be set out in your online order
                  invoice.Price increases will only apply to orders placed or
                  renewals after such changes.All taxes and charges, as may be
                  required by applicable law, will be added to your total, and
                  will be itemized in your online order invoice.We strive to
                  display accurate price information; however we may, on
                  occasion, make inadvertent typographical errors, inaccuracies
                  or omissions related to pricing and availability.We reserve
                  the right to correct any errors, inaccuracies, or omissions at
                  any time and to cancel any orders arising from such
                  occurrences.
                  <br />
                  <br />
                  (c) We may offer from time to time promotions on the Website
                  or App that may affect pricing and that are governed by terms
                  and conditions separate from these Merchant Terms.If there is
                  a conflict between the terms for a promotion and these
                  Merchant Terms, the Merchant Terms will govern.
                  <br />
                  <br />
                  (d) Terms of payment are within our sole discretion and,
                  unless otherwise agreed by us in writing, payment must be
                  received by us before our acceptance of an order.We accept
                  credit cards for all purchases.You represent and warrant that
                  (i) the credit card information you supply to us is true,
                  correct and complete, (ii) you are duly authorized to use such
                  credit card for the purchase, (iii) charges incurred by you
                  will be honored by your credit card company, and (iv) you will
                  pay charges incurred by you at the posted prices, including
                  shipping and handling charges and all applicable local taxes,
                  if any, regardless of the amount quoted on the Website or App
                  at the time of your order.
                  <br />
                  <br />
                  (e) If you elect to pay by credit card, you hereby authorize
                  us to bill your credit card for the Services ordered on a
                  yearly or monthly basis, respectively.Any amounts not paid
                  when due shall bear interest at the rate of two percent (2%)
                  per month, or the maximum legal rate, if less.If any fee
                  cannot be charged to your credit card for any reason, we may
                  send you, via email or other method, notice of such
                  non-payment and a link for you to update your payment
                  information.If such non-payment is not remedied within seven
                  (7) days after receiving such notice of non-payment, then we
                  may (i) provide any Service you have ordered, including any
                  advertising space, to another paying commercial user, (ii) may
                  permanently revoke existing offers for pricing or discounts,
                  or (iii) may terminate the applicable Service(s).
                  <br />
                  <br />
                  (f) If your payment method fails or your account is past due,
                  we may collect fees (including the late fees described in
                  Section (e) above) owed by charging other payment methods on
                  file with us, retain collection agencies and legal counsel,
                  and, for accounts over 90 days past due, request that our
                  payment processor deduct the amount owed from your account
                  balance. We, or the collection agencies we retain, may also
                  report information about your account to credit bureaus, and
                  as a result, late payments, missed payments, or other defaults
                  on your account may be reflected in your credit report.If you
                  wish to dispute the information we reported to a credit bureau
                  (i.e., Experian, Equifax or TransUnion) please contact us at
                  info@wikireviews.com.If you wish to dispute the information a
                  collection agency reported to a credit bureau regarding your
                  Merchant Account, you must contact the collection agency
                  directly.
                  <br />
                  <br />
                  8. Order Acceptance; Term and Termination.
                  <br />
                  <br />
                  (a) You agree that your order is an offer to buy, under these
                  Merchant Terms, the tier of Services listed in your order for
                  the term you select (either a monthly or annual term).All
                  orders for additional Services must be accepted by us in a
                  commercially reasonable time or we will not be obligated to
                  sell the Services to you.We may choose not to accept orders at
                  our sole discretion even when you view an invoice in your
                  online Merchant Account.
                  <br />
                  <br />
                  (b) Once you order the Services, your monthly or annual
                  subscription to the applicable Services will automatically
                  renew for successive one-month or one-year periods,
                  respectively, until you cancel your Services, subject to
                  California Civil Code 17600, where applicable.
                  <br />
                  <br />
                  (c) You may cancel, downgrade (if a paid account, to a free
                  account once the subscription term has ended) or upgrade (if a
                  free account, to a paid account) your Merchant Account at any
                  time, for any reason, by doing so within your Merchant
                  Account.
                  <br />
                  <br />
                  (d) Please note that downgrading from an annual plan during
                  the first nine months to a monthly or free plan will cause the
                  revocation of any posted discounts for subscribers of the
                  annual plan that shall be automatically applied to, and offset
                  against, any amounts paid to us. Downgrades after the ninth
                  month of an annual term will be applied to the renewal term,
                  and you will be fully responsible for all fees owed unless you
                  terminate your Merchant Account.
                  <br />
                  <br />
                  9. WikiReviews Community. One of our core features offered to
                  Merchants and users is our 'WikiReviews Community.' While we
                  try to offer reliable data, we cannot promise that the
                  WikiReviews Community or other content provided through the
                  Services will always be available, accurate, complete, and
                  up-to-date.
                  <br />
                  <br />
                  10. Refunds. ALL PAYMENTS TO US ARE NON-REFUNDABLE, IN WHOLE
                  OR IN PART, FOR ANY REASON WHATSOEVER, INCLUDING BUT NOT
                  IMITED TO CANCELLATION OF SERVICES.WE OFFER NO REFUNDS ON ANY
                  SERVICES (AND INTEGRATED PRODUCTS) DESIGNATED ON THIS WEBSITE
                  AS NON-REFUNDABLE.NOTWITHSTANDING THE ABOVE, IN THE EVENT THAT
                  YOU ASSERT THAT WE ARE NOT IN COMPLIANCE WITH THE OBLIGATIONS
                  UNDER THIS AGREEMENT, UNLESS SUCH NON-COMPLIANCE OR BREACH IS
                  WILLFUL AND INTENTIONAL, YOU WILL PROVIDE US WITH WRITTEN
                  NOTICE OF SUCH BREACH (INCLUDING THE SECTION YOU CITE AS
                  BREACHED) AND A 10 BUSINESS DAY OPPORTUNITY TO CURE SUCH
                  NONCOMPLIANCE PRIOR TO ANY WITHHOLDING OF PAYMENT OF ANY
                  CONSIDERATION OR FEES OWED TO US UNDER THIS AGREEMENT, OR
                  TAKING ANY OTHER ACTION.
                  <br />
                  <br />
                  11. Merchants Warranty and Certain Disclaimers for the
                  Services and Uploaded Content.
                  <br />
                  <br />
                  (a) We do not manufacture or control any of the products
                  available for review on our Website or App.The availability of
                  products through our Website or App does not indicate an
                  affiliation with or endorsement of any product, service or
                  manufacturer.Accordingly, we do not provide any warranties
                  with respect to the products offered on our Website or
                  App.However, the products and services offered on our Website
                  may be covered by the manufacturers warranty, Merchants
                  warranty, or as you may decide and detail in the products
                  description on our Website or included with the product.
                  <br />
                  <br />
                  (b) We, our directors, employees, agents, representatives,
                  suppliers, partners, and content providers do not warrant that
                  (i) the Services will be secure or available at any particular
                  time or location, (ii) any specific defects or errors will be
                  corrected, or that (iii) the result of your using our
                  Services, the Website, or the App will meet your requirements.
                  Your use of our Services, the Website, and the App is solely
                  at your own risk.
                  <br />
                  <br />
                  (c) Save to the extent required by law, we have no special
                  relationship with or fiduciary duty to you.You acknowledge
                  that we have no control over, and no duty to take any action
                  regarding (i) which users gain access to the Services, (ii)
                  what Uploaded Content you access via the Services (whether
                  yours or others), (iii) what effects the Uploaded Content may
                  have on you, (iv) your interpretation of, or use of, the
                  Uploaded Content, or (v) other action you may take as a result
                  of having been exposed to the Uploaded Content.
                  <br />
                  <br />
                  (d) You release us from all liability for your having acquired
                  or not acquired Uploaded Content through the Services.The
                  Services, the Website, or the App may contain, or direct you
                  to websites containing, information that some people may find
                  offensive or inappropriate. We make no representations
                  concerning any Uploaded Content contained in or accessed
                  through the Services, the Website, or the App, and we will not
                  be responsible or liable for the accuracy, copyright
                  compliance, legality, or decency of material contained in or
                  accessed through the Services, the Website, or the App.
                  <br />
                  <br />
                  (e) You understand that we do not, in any way, screen users,
                  nor do we inquire into the backgrounds of users or attempt to
                  verify their backgrounds or statements.We make no
                  representations or warranties as to the conduct of users or
                  the veracity of any information users provide.
                  <br />
                  <br />
                  (f) Wikireviews, Inc. is not affiliated with any particular
                  search engine (i.e. Google, Bing, etc.). We are not employed
                  by, or paid by, search engine companies and we operate
                  completely as an independent third party to the search
                  engines.In addition, (i) we do not control or manipulate
                  demand for any product or service, (ii) we do not control
                  click-through rate(s) to a website as a result of search
                  engine position, and (iii) we do not manipulate the search
                  engine results. While our long-tested and proven methods for
                  search engine optimization have worked in the past, we cannot
                  control the search engines decisions (including the
                  implementation or modification of algorithms) regarding
                  placement, and we cannot guarantee a particular placement on
                  search engine rankings.
                  <br />
                  <br />
                  (g) ALL SERVICES OFFERED BY US TO YOU ON THIS WEBSITE ARE
                  PROVIDED 'AS IS' WITHOUT ANY WARRANTY WHATSOEVER, INCLUDING,
                  WITHOUT LIMITATION, ANY (i) WARRANTY OF MERCHANTABILITY; (ii)
                  WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE; OR (iii)
                  WARRANTY AGAINST INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS
                  OF A THIRD PARTY; WHETHER EXPRESS OR IMPLIED BY LAW, COURSE OF
                  DEALING, COURSE OF PERFORMANCE, USAGE OF TRADE, OR OTHERWISE.
                  SOME JURISDICTIONS LIMIT OR DO NOT ALLOW THE DISCLAIMER OF
                  IMPLIED OR OTHER WARRANTIES SO THE ABOVE DISCLAIMER MAY NOT
                  APPLY TO YOU.
                  <br />
                  <br />
                  12. Limitation of Liability. IN NO EVENT SHALL WE BE LIABLE TO
                  YOU OR ANY THIRD PARTY FOR CONSEQUENTIAL, INDIRECT,
                  INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR ENHANCED DAMAGES,
                  LOST PROFITS OR REVENUES OR DIMINUTION IN VALUE, ARISING OUT
                  OF, OR RELATING TO, AND/OR IN CONNECTION WITH ANY BREACH OF
                  THESE SERVICE TERMS, REGARDLESS OF (A) WHETHER SUCH DAMAGES
                  WERE FORESEEABLE, (B) WHETHER OR NOT WE WERE ADVISED OF THE
                  POSSIBILITY OF SUCH DAMAGES AND (C) THE LEGAL OR EQUITABLE
                  THEORY (CONTRACT, TORT OR OTHERWISE) UPON WHICH THE CLAIM IS
                  BASED. OUR SOLE AND ENTIRE MAXIMUM LIABILITY AND YOUR SOLE AND
                  EXCLUSIVE REMEDY SHALL BE LIMITED TO THE ACTUAL AMOUNT PAID BY
                  YOU FOR THE SERVICES YOU HAVE ORDERED THROUGH OUR WEBSITE, UP
                  TO $100.00. The limitation of liability set forth above shall:
                  (i) only apply to the extent permitted by law and (ii) not
                  apply to (A) liability resulting from our gross negligence or
                  willful misconduct and (B) death or bodily injury resulting
                  from our acts or omissions.
                  <br />
                  <br />
                  13. Privacy; Compliance with Privacy Laws.
                  <br />
                  <br />
                  (a) We respect your privacy. Our Privacy Policy governs the
                  processing of all personal data collected from you in
                  connection with your purchase of Services through the Website
                  or App.
                  <br />
                  <br />
                  (b) We agree to comply with the Canadian Personal Information
                  and Protection of Electronic Documents Act (PIPEDA) and such
                  other regulations pertaining to the privacy of personal
                  information with respect to the receipt and use of personal
                  information relating to our users. Including the terms in the
                  Privacy Policy, we will properly secure such information, use
                  such information for the purpose it was made available and as
                  disclosed in these Merchant Terms and the Privacy Policy, and
                  we will cooperate with access requests (subject to the
                  procedure of the Privacy Policy).
                  <br />
                  <br />
                  14. Post-Termination. Subject to Section 13 above, these
                  Merchant Terms will remain in full force and effect while you
                  use the Website or App or Services.We may (a) suspend your
                  rights to use the Website or App or Services (including your
                  Merchant Account) or (b) terminate your Merchant Account, at
                  any time for any reason at our sole discretion, including for
                  any use of the Website or App or Services in violation of
                  these Merchant Terms or the Content Guidelines by notifying
                  you at the e-mail address or phone number that we currently
                  have on file for you. It is your responsibility to ensure that
                  we have accurate contact information on file for you.Upon
                  termination of this Agreement, your Merchant Account and right
                  to access and use the Website or App and Services will
                  terminate immediately.You understand that any termination of
                  your Merchant Account involves deletion of your Uploaded
                  Content associated therewith from our live databases.We will
                  not have any liability whatsoever to you for any termination
                  of this Agreement, including for termination of your Merchant
                  Account or deletion of your Uploaded Content.
                  <br />
                  <br />
                  15. Force Majeure. We will not be liable or responsible to
                  you, nor be deemed to have defaulted or breached these
                  Merchant Terms, for any failure or delay in our performance
                  under these Terms when and to the extent such failure or delay
                  is caused by or results from acts or circumstances beyond our
                  reasonable control, including, without limitation, acts of
                  God, flood, fire, earthquake, explosion, governmental actions,
                  war, invasion or hostilities (whether war is declared or not),
                  terrorist threats or acts, riot or other civil unrest,
                  national emergency, revolution, insurrection, epidemic,
                  lockouts, strikes or other labor disputes (whether or not
                  relating to our workforce), or restraints or delays affecting
                  carriers or inability or delay in obtaining supplies of
                  adequate or suitable materials, materials or telecommunication
                  breakdown or power outage. In addition, we will not be liable
                  to you for any failure to perform our obligations as a result
                  of, without limitation, any mechanical, electronic, or
                  communications failure or degradation (including 'line-noise'
                  interference).
                  <br />
                  <br />
                  16. Indemnification. You shall defend, indemnify, and hold
                  WikiReviews, Inc., its affiliates and each of its and its
                  affiliates employees, contractors, directors, and
                  representatives from all losses, costs, actions, claims,
                  damages, expenses (including reasonable legal costs) or
                  liabilities, that arise from or relate to your use or misuse
                  of, or access to, the Website, the App, the Services, or
                  otherwise from your Uploaded Content, violation of this
                  Agreement or our Content Guidelines, or infringement by you or
                  of any third party using your Merchant Account, of any
                  intellectual property or other right of any person or entity
                  (save to the extent that a court of competent jurisdiction
                  holds that such claim arose due to an act or omission of
                  ours).We reserve the right to assume the exclusive defense and
                  control of any matter otherwise subject to indemnification by
                  you, in which event you will assist and cooperate with us in
                  asserting any available defenses.
                  <br />
                  <br />
                  17. Governing Law and Jurisdiction. This Website and App are
                  operated from the United States.All matters arising out of or
                  relating to these Merchant Terms are governed by and construed
                  in accordance with the internal laws of the State of
                  California without giving effect to any choice or conflict of
                  law provision or rule (whether of the State of California or
                  any other jurisdiction) that would cause the application of
                  the laws of any jurisdiction other than those of the State of
                  California.
                  <br />
                  <br />
                  18. Dispute Resolution and Binding Arbitration.
                  <br />
                  <br />
                  YOU AND COMPANY ARE AGREEING TO GIVE UP ANY RIGHTS TO LITIGATE
                  CLAIMS IN A COURT OR BEFORE A JURY.OTHER RIGHTS THAT YOU WOULD
                  HAVE IF YOU WENT TO COURT MAY ALSO BE UNAVAILABLE OR MAY BE
                  LIMITED IN ARBITRATION.
                  <br />
                  <br />
                  ANY CLAIM, DISPUTE OR CONTROVERSY (WHETHER IN CONTRACT, TORT
                  OR OTHERWISE, WHETHER PRE-EXISTING, PRESENT OR FUTURE, AND
                  INCLUDING STATUTORY, CONSUMER PROTECTION, COMMON LAW,
                  INTENTIONAL TORT, INJUNCTIVE AND EQUITABLE CLAIMS) BETWEEN YOU
                  AND US ARISING FROM OR RELATING IN ANY WAY TO YOUR PURCHASE OF
                  PRODUCTS OR SERVICES THROUGH THE WEBSITE, WILL BE RESOLVED
                  EXCLUSIVELY AND FINALLY BY BINDING ARBITRATION.
                  <br />
                  <br />
                  The arbitration will be administered by the American
                  Arbitration Association ('AAA') in Orange County, California,
                  in accordance with the Consumer Arbitration Rules (the 'AAA
                  Rules') then in effect, except as modified by this Section 17.
                  (The AAA Rules are available at www.adr.org/arb_med or by
                  calling the AAA at 1-800-778-7879.) The Federal Arbitration
                  Act will govern the interpretation and enforcement of this
                  Section 17. The arbitrator will have exclusive authority to
                  resolve any dispute relating to arbitrability and/or
                  enforceability of this arbitration provision, including any
                  unconscionability challenge or any other challenge that the
                  arbitration provision or the agreement is void, voidable or
                  otherwise invalid. The arbitrator will be empowered to grant
                  whatever relief would be available in court under law or in
                  equity. Any award of the arbitrator(s) will be final and
                  binding on each of the parties, and may be entered as a
                  judgment in any court of competent jurisdiction.
                  <br />
                  <br />
                  You agree to an arbitration on an individual basis. In any
                  dispute, NEITHER YOU NOR COMPANY WILL BE ENTITLED TO JOIN OR
                  CONSOLIDATE CLAIMS BY OR AGAINST OTHER CUSTOMERS IN COURT OR
                  IN ARBITRATION OR OTHERWISE PARTICIPATE IN ANY CLAIM AS A
                  CLASS REPRESENTATIVE, CLASS MEMBER OR IN A PRIVATE ATTORNEY
                  GENERAL CAPACITY.The arbitral tribunal may not consolidate
                  more than one persons claims, and may not otherwise preside
                  over any form of a representative or class proceeding. The
                  arbitral tribunal has no power to consider the enforceability
                  of this class arbitration waiver and any challenge to the
                  class arbitration waiver may only be raised in a court of
                  competent jurisdiction. If any provision of this arbitration
                  Section is found unenforceable, the unenforceable provision
                  will be severed and the remaining arbitration terms will be
                  enforced.
                  <br />
                  <br />
                  19. Assignment. You will not assign any of your rights or
                  delegate any of your obligations under these Merchant Terms
                  without our prior written consent.Any purported assignment or
                  delegation in violation of this Section 18 is null and void.No
                  assignment or delegation relieves you of any of your
                  obligations under these Merchant Terms.
                  <br />
                  <br />
                  20. No Waivers. The failure by us to enforce any right or
                  provision of these Merchant Terms will not constitute a waiver
                  of future enforcement of that right or provision. The waiver
                  of any right or provision will be effective only if in writing
                  and signed by a duly authorized representative of Company.
                  <br />
                  <br />
                  21. No Third-Party Beneficiaries. These Merchant Terms do not
                  and are not intended to confer any rights or remedies upon any
                  person other than you.
                  <br />
                  <br />
                  22. Notices.
                  <br />
                  <br />
                  (a) To You. We may provide any notice to you under these
                  Merchant Terms by: (i) sending a message to the email address
                  you provide for your Merchant Account or (ii) by posting to
                  the Website or App.Notices sent by email will be effective
                  when we send the email and notices we provide by posting will
                  be effective upon posting.It is your responsibility to keep
                  your email address current.
                  <br />
                  <br />
                  (b) To Us. To give us notice under these Merchant Terms, you
                  must contact us by email at notify@wikireviews.com or by
                  certified mail at 26 Burlingame, Irvine, California 92602.We
                  may update the email address for notices to us by posting a
                  notice on the Website or App.
                  <br />
                  <br />
                  23. Severability. If any provision of these Merchant Terms is
                  invalid, illegal, void or unenforceable, then that provision
                  will be deemed severed from these Merchant Terms and will not
                  affect the validity or enforceability of the remaining
                  provisions of these Merchant Terms.
                  <br />
                  <br />
                  {
                    "24. Entire Agreement. Our order confirmation, these Merchant Terms, the Content Guidelines, and our Privacy Policy will be deemed the final and integrated agreement between you and us on the matters contained herein."
                  }
                </div>
              </FormGroup>
              <FormGroup className="d-flex" style={{ cursor: scrollEnd ? 'auto' : 'not-allowed' }}>
                <CustomInput
                  type="checkbox"
                  id="exampleCustomCheckbox"
                  checked={this.state.isChecked}
                  disabled={!this.state.scrollEnd}
                  onChange={(e) => {
                    this.state.scrollEnd &&
                      this.setState({ isChecked: e.target.checked }, () =>
                        this.checkValid()
                      );
                  }}
                />{" "}
                <label style={{ opacity: this.state.scrollEnd ? 1 : 0.5 }}>
                  By logging in you agree to WikiReviews{" "}
                  <a
                    href="/terms"
                    className="cs-label under-link"
                    target="_blank"
                  >
                    Terms &amp; Conditions.
                  </a>
                </label>
              </FormGroup>
              {(scrollEnd == true && isChecked == false) && (<span className="blinking">
                <FontAwesomeIcon className="blinking" icon={'arrow-up'} color="#000" /> Please read and check </span>)}
              <div className="pb-3 pt-0 px-lg-5">
                <FormGroup>
                  <AvField
                    style={{ borderColor: auth_error ? "red" : "grey" }}
                    id="email"
                    type="email"
                    value={email}
                    errorMessage="please enter a valid email"
                    onChange={this.handleChange}
                    name="email"
                    placeholder="Email Address"
                    validation={{ email: true }}
                  // validation={{pattern: { value: emailRegex, errorMessage: "please enter a valid email" }}}
                  />
                </FormGroup>
                <FormGroup>
                  <AvField
                    style={{ borderColor: auth_error ? "red" : "grey" }}
                    id="password"
                    type="password"
                    value={password}
                    onChange={this.handleChange}
                    name="password"
                    id="pass"
                    placeholder="Enter Password"
                  />
                </FormGroup>
                <div className="text-center">
                  <span className="text-danger">
                    {auth_error ? "Invalid Credentials" : ""}
                  </span>
                </div>
              </div>

              <FormGroup>
                <Row className="align-items-center">
                  <Col xs="auto">
                    <span className="as-link small" onClick={this.toggle}>
                      Forgot Password ?
                    </span>
                  </Col>
                  <Col className="d-flex">
                    <Button
                      type="submit"
                      disabled={this.state.isdisabledLogin}
                      color="primary"
                      className={scrollEnd && isChecked ? "button-flasing ml-auto mw" : "ml-auto mw"}
                    >
                      {isSpinner && <Spinner size="sm" />}
                      {this.checkIsLoggedIn()}
                      {` `}
                      {`Login`}
                    </Button>
                  </Col>
                </Row>
              </FormGroup>
            </AvForm>
          </ModalBody>
        </Modal>

        <Modal
          isOpen={this.state.fmodal}
          toggle={this.toggle}
          className="custom-pop"
        >
          <ModalHeader toggle={this.toggle}>RESET YOUR PASSWORD</ModalHeader>
          <ModalBody>
            <div className="pt-2 pb-1 pr-5 pl-5 mb-3 flex-column d-flex justify-content-center align-items-center">
              <span className="mb-3">
                <img src={Logo} alt="Logo" />
              </span>
            </div>
            <AvForm onValidSubmit={this.handleSubmitForgot}>
              <div className="pb-3 pt-0 pr-5 pl-5">
                <div
                  className="social-block clearfix ng-scope"
                  id="controller"
                  ng-controller="LoginController"
                  ng-init="frompath('Login');"
                >
                  <ul className="clearfix">
                    <li className="face-book">
                      <a className="text-light" href="#!" ng-click="login_fb()">
                        <FontAwesomeIcon
                          icon={["fab", "facebook-f"]}
                          color="#fff"
                        />
                        Login with FACEBOOK
                      </a>
                    </li>
                    <li className="google">
                      <script
                        src="https://apis.google.com/js/client:platform.js"
                        async=""
                        defer=""
                      ></script>

                      <FontAwesomeIcon icon={["fab", "google"]} />
                      <a
                        className="text-light"
                        href="#!"
                        id="googleSignin"
                        onClick="gapi.auth.signIn(additionalParams);"
                      >
                        Login with GOOGLE+
                      </a>
                    </li>
                  </ul>
                </div>

                <FormGroup>
                  <AvField
                    type="email"
                    name="forgotemail"
                    id="email"
                    placeholder="Your Email Address"
                    errorMessage="please enter a valid email"
                    onChange={this.handleChangeEmail}
                    validation={{ email: true }}
                  />
                </FormGroup>
                <FormGroup className="d-flex">
                  <Button
                    color="primary"
                    className="ml-auto mw"
                    disabled={this.state.isdisabledForgot}
                  >
                    Submit
                  </Button>
                </FormGroup>
              </div>
            </AvForm>
          </ModalBody>
        </Modal>

        <Modal isOpen={this.state.signUpModalToggle} toggle={() => this.setState({ signUpModalToggle: !this.state.signUpModalToggle })} >
          <ModalHeader toggle={() => this.setState({ signUpModalToggle: !this.state.signUpModalToggle })}>Customer Sign Up</ModalHeader>
          <AvForm
            // ref={this.refLoginForm}
            autoComplete="off"
            onValidSubmit={this.handleSubmitEnterprise}
          >
            <ModalBody>

              <Row xs={1} lg={2} form>
                <Col>
                  <FormGroup>
                    <Label className="text-dark font-weight-normal">First Name</Label>
                    <AvField name="firstName" value={firstName || ''}
                      onChange={this.handleChangeForm}
                      className="primary" type="text" required
                      validate={{
                        required: { value: true, errorMessage: 'Please enter your First Name' },
                        pattern: { value: nameRegex, errorMessage: 'Special characters not allowed' },
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label className="text-dark font-weight-normal">Last Name</Label>
                    <AvField name="lastName" value={lastName || ''} onChange={this.handleChangeForm} required className="primary" type="text"
                      validate={{
                        required: { value: true, errorMessage: 'Please enter your Last Name' },
                        pattern: { value: nameRegex, errorMessage: 'Special characters not allowed' },
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label className="text-dark font-weight-normal">Your Job Title</Label>
                    <AvField name="jobTitle" value={jobTitle || ''} onChange={this.handleChangeForm} required className="primary" type="text"
                      validate={{
                        required: { value: true, errorMessage: 'Please enter your Job Title' },
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label className="text-dark font-weight-normal">Email Address</Label>
                    <AvField name="emailAddress" value={emailAddress || ''} onChange={this.handleChangeForm} required className="primary" type="email"
                      validate={{
                        required: { value: true, errorMessage: 'Please enter your Email' },
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col lg={12}>
                  <FormGroup>
                    <Label className="text-dark font-weight-normal">Phone Number</Label>
                    <div className="d-flex mx-n2">
                      <div className="col-sm-3 px-2">
                        <AvField name="phone1" value={phone1 || ''} onChange={this.handleChangeForm} required className="primary" className="primary" type="number" placeholder="(555)" min="0" max={999}
                          validate={{
                            required: { value: true, errorMessage: 'Please enter a number' },
                            minLength: { value: 3, errorMessage: 'invalid number' },
                          }}
                        />
                      </div>
                      <div className="col-sm-3 px-2">
                        <AvField name="phone2" value={phone2 || ''} onChange={this.handleChangeForm} required className="primary" className="primary" type="number" placeholder="555" min="0" max={999}
                          validate={{
                            required: { value: true, errorMessage: 'Please enter a number' },
                            minLength: { value: 3, errorMessage: 'invalid number' },
                          }}
                        />
                      </div>
                      <div className="col-sm-6 px-2">
                        <AvField name="phone3" value={phone3 || ''} onChange={this.handleChangeForm} required className="primary" className="primary" type="number" placeholder="5555" min="0" max={9999}
                          validate={{
                            required: { value: true, errorMessage: 'Please enter a number' },
                            minLength: { value: 4, errorMessage: 'invalid number' },
                          }} />
                      </div>
                    </div>
                  </FormGroup>
                </Col>
              </Row>


              <Row form>
                <Label lg={6}>Company Name</Label>
                <Col lg={6}>
                  <FormGroup>
                    <AvField name="companyName" value={companyName || ''} onChange={this.handleChangeForm} required className="primary" type="text"
                      validate={{
                        required: { value: true, errorMessage: 'Please enter your Company Name' },
                      }}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row form>
                <Label lg={6}>Is this a Franchise?</Label>
                <Col lg={6}>
                  <FormGroup>
                    <AvField name="isFranchise" value={isFranchise} onChange={this.handleChangeForm} className="primary" type="select">
                      <option>Yes</option>
                      <option>No</option>
                    </AvField >
                  </FormGroup>
                </Col>
              </Row>
              {isFranchise === "Yes" ? (
                <>
                  <Row form>
                    <Label lg={6}>How many store locations in USA & Canada?</Label>
                    <Col lg={6}>
                      <FormGroup>
                        <AvField name="storeLocationsUsa" value={storeLocationsUsa || ''} onChange={this.handleChangeForm} className="primary" type="number" required
                          validate={{
                            required: { value: true, errorMessage: 'Please enter a store locations in USA & Canada' },
                            min: { value: 0, errorMessage: 'Please enter positive value' },
                          }}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row form>
                    <Label lg={6}>How many are company owned?</Label>
                    <Col lg={6}>
                      <FormGroup>
                        <AvField name="companyOwned" value={companyOwned || ''} onChange={this.handleChangeForm} className="primary" type="number" required
                          validate={{
                            required: { value: true, errorMessage: 'Please enter a store locations in USA & Canada' },
                            min: { value: 0, errorMessage: 'Please enter positive value' },
                          }}
                        />
                      </FormGroup>
                    </Col>
                  </Row>

                </>
              ) : (
                  <Row form>
                    <Label lg={6}>How many Store Locations?</Label>
                    <Col lg={6}>
                      <FormGroup>
                        <AvField name="storeLocations" value={storeLocations || ''} onChange={this.handleChangeForm} className="primary" type="number"
                          validate={{
                            required: { value: true, errorMessage: 'Please enter a store locations' },
                          }}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                )}

            </ModalBody>
            <ModalFooter className="px-0">
              <Button color="primary" type="submit" > {isSpinner && <Spinner size="sm" />} Submit</Button>
            </ModalFooter>
          </AvForm>
        </Modal>

        <Modal scrollable centered isOpen={this.state.signUpCompleteModalToggle} toggle={() => this.setState({ signUpCompleteModalToggle: !this.state.signUpCompleteModalToggle })} >
          <ModalHeader toggle={() => this.setState({ signUpCompleteModalToggle: !this.state.signUpCompleteModalToggle })}>Sign Up Complete</ModalHeader>
          <ModalBody>
            Thank you for contacting us. We will be contacting you in the next 24-48 hours. In the meantime you can reach out to Sunil Wagle directly by phone at <a href="tel:3109936486">(310)-993-6486</a> or by email at: <a href="mailto:swagle@wikireviews.com">swagle@wikireviews.com</a>
            <br />
            <div className="mt-2">
              Thank You.
            </div>
            <div className="text-right mt-3">
              <Button color="dark" onClick={async () => await this.setState({ signUpCompleteModalToggle: false })}>Close</Button>
            </div>
          </ModalBody>
        </Modal>

        <Modal scrollable isOpen={this.state.adminSignUpModalToggle} toggle={() => this.setState({ adminSignUpModalToggle: !this.state.adminSignUpModalToggle })} >
          <ModalHeader toggle={() => this.setState({ adminSignUpModalToggle: !this.state.adminSignUpModalToggle })}>Customer Sign Up</ModalHeader>
          <ModalBody>
            <div className="text-dark font-weight-bold fs-18 mb-3">Administrator Info</div>
            <Row xs={1} lg={2} form>
              <Col>
                <FormGroup>
                  <Label className="text-dark font-weight-normal">First Name</Label>
                  <Input className="primary" type="text" />
                </FormGroup>
              </Col>
              <Col>
                <FormGroup>
                  <Label className="text-dark font-weight-normal">Last Name</Label>
                  <Input className="primary" type="text" />
                </FormGroup>
              </Col>
              <Col>
                <FormGroup>
                  <Label className="text-dark font-weight-normal">Email Address</Label>
                  <Input className="primary" type="email" />
                </FormGroup>
              </Col>
              <Col lg={12}>
                <FormGroup>
                  <Label className="text-dark font-weight-normal">Phone Number</Label>
                  <div className="d-flex mx-n2">
                    <div className="col-sm-3 px-2">
                      <Input className="primary" type="number" placeholder="(555)" />
                    </div>
                    <div className="col-sm-3 px-2">
                      <Input className="primary" type="number" placeholder="555" />
                    </div>
                    <div className="col-sm-6 px-2">
                      <Input className="primary" type="number" placeholder="5555" />
                    </div>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row form>
              <Label lg={6}>Company Name</Label>
              <Col lg={6}>
                <FormGroup>
                  <Input className="primary" type="text" />
                </FormGroup>
              </Col>
            </Row>
            <Row form>
              <Label lg={6}>Add Listings from search or Excel file?</Label>
              <Col lg={6}>
                <FormGroup>
                  <Input className="primary" type="select">
                    <option>Search</option>
                    <option>Excel File</option>
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <hr />
            <Row form>
              <Label xs={12}>Search for a Business</Label>
              <Col xs={12}>
                <FormGroup>
                  <Input className="primary" type="search" />
                  <div className="shadow-sm" style={{ maxHeight: "400px", overflowY: "auto" }}>
                    <ul className="list-unstyled mb-2 p-2">
                      <li className="d-flex">
                        <CustomInput
                          className="mr-2"
                          type="checkbox"
                          id={"branch_" + 1}
                          name="select_multiple_companies"
                        />
                        <Label
                          for={"branch_" + 1}
                          className="flex-fill fs-14 text-dark font-weight-normal"
                        >
                          Company Branch name 1
                        </Label>
                      </li>
                      <li className="d-flex">
                        <CustomInput
                          className="mr-2"
                          type="checkbox"
                          id={"branch_" + 2}
                          name="select_multiple_companies"
                        />
                        <Label
                          for={"branch_" + 2}
                          className="flex-fill fs-14 text-dark font-weight-normal"
                        >
                          Company Branch name 2
                        </Label>
                      </li>
                      <li className="d-flex">
                        <CustomInput
                          className="mr-2"
                          type="checkbox"
                          id={"branch_" + 3}
                          name="select_multiple_companies"
                        />
                        <Label
                          for={"branch_" + 3}
                          className="flex-fill fs-14 text-dark font-weight-normal"
                        >
                          Company Branch name 3
                        </Label>
                      </li>
                    </ul>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row form>
              <Label lg={6}>Upload Excel File</Label>
              <Col lg={6}>
                <FormGroup>
                  <Input type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                  <FormText color="muted">
                    Upload a valid Excel file, that contains your business list with info.
                  </FormText>
                </FormGroup>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter className="px-0">
            <Button color="primary" onClick={() => this.setState({ signUpModalToggle: false, signUpCompleteModalToggle: true })}>Submit</Button>
          </ModalFooter>
        </Modal>

      </React.Fragment>
    );
  }
}

const mapState = (state) => ({
  auth_response: state.auth.auth_login,
  auth_error: state.auth.auth_error,
  signUpRessponse: state.auth.signUp,
  signUpErrorRessponse: state.auth.signUp_error,
});

const mapProps = (dispatch) => ({
  is_auth_login_true: () => dispatch(is_auth_login_true()),
  login_request: (data) => dispatch(auth_login_request(data)),
  signup_request: (data) => dispatch(signup_request(data)),
  forgot_password: (data) => dispatch(forgot_password(data)),
});

export default withRouter(connect(mapState, mapProps)(Header));
