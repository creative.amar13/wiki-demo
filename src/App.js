import React from 'react';
import './App.scss';
import RouteLinks from "./router";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"


function App() {
  return (
    <div className="App">
      <ToastContainer />
      <RouteLinks />
    </div>
  );
}

export default App;
