import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import {
  Navbar,
  Container,
  NavbarBrand,
  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Row,
  Col,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Button,
  Badge
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ReactJoyride, { CallBackProps, EVENTS, STATUS } from "react-joyride";
import { setInitialAuth } from './actions/auth'
import { post_notifications, get_notifications, set_nofiticationCount } from "./actions/user";

class AppHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isToggle: false,
      viewNotifType: "notif",
      run: false,
      userFirstName: "",
      messageID: "",
      dropdownOpenNotification: false,
      fetchNotiMessage: [],
      fetchNotiFeeds: [],
      fetchNotiRequest: [],
      totalNotificationCount: 0,
      msgCount: 0,
      requestCount: 0,
      feedsCount: 0,
      steps: [
        {
          target: ".step-1",
          // title: `Hello ${this.props.profileData && this.props.profileData.user && this.props.profileData.user.first_name !== null ? this.props.profileData.user.first_name : 'xyz'} and welcome to WikiReviews!`,
          content:
            "This is a brief tour that will show you how to setup your corporate account. This includes how to set up users, roles and branches.  You will also learn how to fill out company information so it is properly displayed for each location.  Finally, you will learn how to create posts, engage with your customers and respond to and dispute reviews.",
          disableBeacon: true,
          disableOverlayClose: true,
          hideCloseButton: true,
          placement: 'center',
          spotlightClicks: true,
          styles: {
            options: {
              zIndex: 1000,
            },
          },
        },
        {
          target: ".step-2",
          title: 'Statistics',
          content:
            "You can easily see a dashboard summary of all the locations monthly statistics as well as the number of (put in red symbol) overdue, (put in yellow symbol) pending and (put in green symbol) completed tasks.",
          placement: "right",
          disableBeacon: true,
        },
        {
          target: ".step-3",
          content:
            "Click on colored circles and tabs with numbers to filter the results.",
          placement: "top",
        },
        {
          target: ".step-4",
          content:
            "Modify the dashboard statistics by time frame by using the pulldowns here",
          placement: "left",
        },
        {
          target: ".step-5",
          content:
            "The listing owner guide is a quick reference guide to use at any time.  This is simply a template which can be used or it can be edited to suit your needs.",
          placement: "left",
        },
        {
          target: ".step-6",
          content:
            "The listing owner guide is a quick reference guide to use at any time.  This is simply a template which can be used or it can be edited to suit your needs.",
          placement: "left",
        },
        {
          target: ".step-7",
          content:
            "Search here for a specific branch by city, state or zip or by customer service representative name.  This search will filter results on the map as well as all the data below the map.",
          placement: "top",
        },
        {
          target: ".step-8",
          content:
            "The information shown here can be filtered by the search box above.",
          placement: "top",
        },
        {
          target: ".step-9",
          content:
            "The data shown here reflects the filters from the search box.",
          placement: "top",
        },
        {
          target: ".step-10",
          content:
            "Clicking on the settings icon takes you to a section where you can create and edit employees, branches and user roles.  This is the main administrator section to properly manage users, branches and user roles on the platform.",
          placement: "left",
        },
        {
          target: ".step-11",
          content:
            "From here you can add or remove employees who will be responding to customer messages, reviews and inquiries.  Once an employee’s email is added, they will receive an emailed link to create their own password.  You will also assign each user to a specific role which gives users permissions to do various tasks.",
          placement: "right",
        },
        {
          target: ".step-12",
          content:
            "Here you can assign the branch location(s) to a specific employee and even assign backup branches to employees in case one is sick.",
          placement: "left",
        },
      ],
    };
  }

  componentWillReceiveProps(nextProps) {
    const { profileData, DataDataData } = nextProps;
    if (profileData && profileData.user && profileData.user.first_name) {
      this.setState({
        userFirstName: profileData.user.first_name,
        messageID: profileData?.seen_notification_number?.messages
      })
    }
    if (
      nextProps.fetch_my_notification &&
      Object.keys(nextProps.fetch_my_notification).length > 0
    ) {
      if (nextProps.fetch_my_notification?.unreadmsgs) {
        this.setState({
          fetchNotiMessage: nextProps.fetch_my_notification?.unreadmsgs?.unreadmsgs,
          totalNotificationCount: nextProps.fetch_my_notification?.unreadmsgs?.msgcount,
          msgCount: nextProps.fetch_my_notification?.unreadmsgs?.msgcount
        });
      }
      if (nextProps.fetch_my_notification?.connections) {
        this.setState({
          fetchNotiRequest: nextProps.fetch_my_notification?.connections?.friendrequest,
          totalNotificationCount: nextProps.fetch_my_notification?.connections?.friendrequestcount,
          requestCount: nextProps.fetch_my_notification?.connections?.friendrequestcount
        });
      }
      if (nextProps.fetch_my_notification?.feeds) {
        this.setState({
          fetchNotiFeeds: nextProps.fetch_my_notification?.feeds?.feeds,
          totalNotificationCount: nextProps.fetch_my_notification?.feeds?.feedscount,
          feedsCount: nextProps.fetch_my_notification?.feeds?.feedscount
        });
      }
    }
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):

    if (this.props.fetch_my_notification !== prevProps.fetch_my_notification) {
      if (this.props.fetch_my_notification?.unreadmsgs !== prevProps.fetch_my_notification?.unreadmsgs) {
        this.setState({ fetchNotiMessage: this.props.fetch_my_notification?.unreadmsgs?.unreadmsgs, totalNotificationCount: this.props.fetch_my_notification?.unreadmsgs?.msgcount, msgCount: this.props.fetch_my_notification?.unreadmsgs?.msgcount });
      }
      if (this.props.fetch_my_notification?.connections !== prevProps.fetch_my_notification?.connections) {
        this.setState({ fetchNotiRequest: this.props.fetch_my_notification?.connections?.friendrequest, totalNotificationCount: this.props.fetch_my_notification?.connections?.friendrequestcount, requestCount: this.props.fetch_my_notification?.connections?.friendrequestcount });
      }
      if (this.props.fetch_my_notification?.feeds !== prevProps.fetch_my_notification?.feeds) {
        this.setState({ fetchNotiFeeds: this.props.fetch_my_notification?.feeds?.feeds, totalNotificationCount: this.props.fetch_my_notification?.feeds?.feedscount, feedsCount: this.props.fetch_my_notification?.feeds?.feedscount });
      }
    }
  }

  componentDidMount() {

    this.props.get_notifications();
    try {
      this.intervalCheck = setInterval(async () => {
        await this.props.get_notifications();
      }, 60000);
    } catch (e) {
      console.log(e);
    }
  }

  // handleNotifications = () => {
  //   const { post_notifications } = this.props;
  //   const { viewNotifType, messageID } = this.state;

  //   if (viewNotifType === "msg") {
  //     post_notifications({ "type": "messages", "id": 16291 });

  //   } else if (viewNotifType === "notif") {
  //     post_notifications({ "type": "feeds" });
  //   }
  // };

  handleNotificationCount = (type) => {
    let { feedsCount, requestCount, msgCount } = this.state;
    let { profileData, } = this.props;

    let data = {};
    data.type = type;
    if (feedsCount > 0 && type == "feeds") {
      this.props.set_nofiticationCount(data);
    }
    // if (requestCount > 0 && type == "friends") {
    //   this.props.set_nofiticationCount(data);
    // }
    if (
      msgCount > 0 &&
      type == "messages") {
      // data.id = 16291;
      data.id = profileData?.seen_notification_number?.messages || '';
      this.props.set_nofiticationCount(data);
    }
  }


  setFeedCount = (feedId, feedstype, type) => {
    let data = {};
    data.type = feedstype;
    data.id = feedId;
    this.props.set_nofiticationCount(data);
  }

  handleClick = (e) => {
    e.preventDefault();
    this.setState({ run: true, });
  };

  handleJoyrideCallback = (CallBackProps) => {
    let { status, type, step, index, action, lifecycle } = CallBackProps;
    const finishedStatuses = [STATUS.FINISHED, STATUS.SKIPPED];

    if (finishedStatuses.includes(status)) {
      this.setState({ run: false });
    } else if ([EVENTS.TARGET_NOT_FOUND].includes(type)) {

      if (step && step.target === '.step-11') {

        this.setState({
          run: false
        }, () => {
          this.props.history.push('/corporatesettings');
          setTimeout(() => {
            this.setState({ run: true })
          }, 3000)
        })
      }
    }
  };
  renderReactJoyRide = ({ Tooltip }) => {
    return (
      <ReactJoyride
        callback={this.handleJoyrideCallback}
        steps={this.state.steps}
        run={this.state.run}
        continuous={true}
        tooltipComponent={Tooltip}
        showProgress
        hideBackButton={false}
        disableOverlayClose={true}
        locale={{ close: 'Close' }}
        styles={{
          options: {
            arrowColor: "#fff",
            backgroundColor: "#fff",
            overlayColor: "rgba(0, 0, 0, 0.6)",
            primaryColor: "mediumaquamarine",
            textColor: "#333",
            zIndex: 10000,
          },
        }}
      />
    )
  }
  onMouseEnterCategory = (item) => {
    this.setState({ [item]: true });
  }

  onMouseLeaveCategory = (item) => {
    this.setState({ [item]: false });
  }
  toggleCategory = (item) => {
    this.setState(prevState => ({
      [item]: !prevState[item]
    }));
  }

  render() {
    const { profileData, post_notifications } = this.props;
    const {
      fetchNotiMessage,
      fetchNotiFeeds,
      fetchNotiRequest,
      totalNotificationCount,
      feedsCount,
      requestCount,
      msgCount,
    } = this.state;
    let totalBellCOunt = feedsCount + msgCount
    let user_name = "";
    let profile_pic = "";
    if (profileData && profileData.user) {
      if (
        profileData.user &&
        profileData.user.first_name &&
        profileData.user.last_name
      ) {
        user_name = `${profileData.user.first_name} ${profileData.user.last_name}`;
        profile_pic = `${profileData && profileData.current_profile_file}`;
      }
    }

    const Tooltip = ({
      continuous,
      index,
      isLastStep,
      step,
      backProps,
      closeProps,
      primaryProps,
      skipProps,
      tooltipProps, }) => {

      if (index === "2" || index === 2) {

        return (<div {...tooltipProps}>

          <div>
            <div className="stat-bubble with-line" data-type="overall">
              &nbsp;
                  </div>
            <div className="stat-bubble with-line" data-type="reviews">
              &nbsp;
                  </div>
            <div className="stat-bubble with-line" data-type="branches">
              &nbsp;
                  </div>
          </div>
          <div className="intro-block" data-type="stat-intro">
            {step.content}
          </div>

          {continuous && (
            <Button {...primaryProps} color="primary" className="bt-btn">
              {'Got It'}
            </Button>
          )}

        </div>)

      } else if (index === "0" || index === 0) {

        return (<div className="cs-toolpop" {...tooltipProps}>


          <h3>Hello {this.state.userFirstName} and welcome to WikiReviews!</h3>
          <div>This is a brief tour that will show you how to setup your corporate account. This includes how to set up users, roles and branches.  You will also learn how to fill out company information so it is properly displayed for each location.  Finally, you will learn how to create posts, engage with your customers and respond to and dispute reviews.</div>
          <div className="d-flex mt-4 mb-3">


            <Button {...primaryProps} color="primary">
              Ok
            </Button>
            <Button {...closeProps} color="link" className="ml-auto" onClick={() => {
              this.setState({
                run: false
              })
            }}>
              End Tour
            </Button>
          </div>
          <Link
            to="/corporateprofile"
            onClick={() => {
              this.setState({
                run: false
              })
            }}
          >
            Skip this part and I’ll explore on my own
              </Link>
        </div>)
      }
      else {
        return (
          <div className="cs-toolpop" {...tooltipProps}>
            {step.title && <h3>{step.title}</h3>}
            {step.content && <div>{step.content}

            </div>}
            <div className="d-flex mt-4 mb-3">
              <Button {...primaryProps} color="primary">
                Ok
            </Button>
              <Button {...closeProps} color="link" className="ml-auto" onClick={() => {
                this.setState({
                  run: false
                })
              }}>
                End Tour
            </Button>
            </div>
            {index === 0 ?
              <Link
                to="/corporateprofile"
                onClick={() => {
                  this.setState({
                    run: false
                  })
                }}
              >
                Skip this part and I’ll explore on my own
              </Link> :
              ""}

          </div>

        )
      }
    };
    return (
      <React.Fragment>
        {/* {this.renderReactJoyRide({ Tooltip })} */}

        <Navbar dark expand="xs" className="fixed-top" id="appNav">
          <Container fluid>
            <Row className="flex-grow-1 align-items-center">
              <Col>
                <Nav navbar>
                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav className="text-primary">
                      <FontAwesomeIcon icon="bars" size="lg" />
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem href="/about" target="_blank">
                        About
                      </DropdownItem>
                      <DropdownItem href="/faq" target="_blank">
                        FAQ
                      </DropdownItem>
                      <DropdownItem href="/manifesto" target="_blank">
                        Manifesto
                      </DropdownItem>
                      <DropdownItem href="/guidelines" target="_blank">
                        Guidelines
                      </DropdownItem>
                      <DropdownItem href="/privacy-policy" target="_blank">
                        Privacy Policy
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </Nav>
              </Col>
              <Col xs="auto">
                <NavbarBrand
                  // href="/"
                  onClick={() => { this.props.history.push('/corporateprofile') }}
                  role="button"
                  className="m-0 d-flex flex-column flex-md-row align-items-md-center text-center"
                >
                  <span className="text-uppercase mr-0 mr-md-2">
                    WikiReviews
                  </span>{" "}
                  <span className="small">for Business</span>
                </NavbarBrand>
              </Col>
              <Col className="d-flex">
                <Nav className="ml-auto" navbar>
                  <UncontrolledDropdown nav inNavbar className="d-none d-md-inline-block">
                    {this.props.history.location.pathname === "/corporateprofile" ?
                      <DropdownToggle nav className="text-nowrap biztour" onClick={(e) => {
                        this.props.clickTourStart()
                      }}>
                        {"Tour"}
                      </DropdownToggle> :
                      null}
                    {/* <DropdownToggle nav className="text-nowrap biztour" >
                        {"Tour"}
                      </DropdownToggle> */}
                  </UncontrolledDropdown>

                  <UncontrolledDropdown
                    nav
                    inNavbar
                    onMouseOver={() => this.onMouseEnterCategory('dropdownOpenNotification')}
                    onMouseLeave={() => {
                      this.onMouseLeaveCategory('dropdownOpenNotification')
                      this.setState({
                        viewNotifType: "notif",
                      })
                    }}
                    isOpen={this.state.dropdownOpenNotification}
                    toggle={() => this.toggleCategory('dropdownOpenNotification')}
                  >
                    <DropdownToggle nav>
                      {totalNotificationCount && totalNotificationCount > 0 ? (
                        <FontAwesomeIcon icon="bell" onMouseOver={() => this.setFeedCount(fetchNotiFeeds[0]?.id, 'feeds', true)} style={{ color: "red" }} />
                      ) : (
                          <>
                            <FontAwesomeIcon icon="bell" />
                            {totalBellCOunt > 0 ?
                              <Badge className="ml-1" color="primary"> {totalBellCOunt}</Badge>
                              // <div className="notification-count _sm">{totalBellCOunt}</div>
                              : ""}
                          </>
                        )}
                    </DropdownToggle>
                    <DropdownMenu className="dropdown-list" right>
                      <Nav className="nav-fill" tabs>
                        <NavItem>
                          <NavLink
                            active={this.state.viewNotifType === "notif"}
                            onClick={() => {
                              this.setState({ viewNotifType: "notif" });
                              this.handleNotificationCount("feeds")
                            }}
                          >
                            <div className="text-uppercase fs-12">
                              Notifications
                              {feedsCount > 0 ?
                                <Badge className="ml-1" color="primary"> {feedsCount}</Badge>
                                // <div className="notification-count _sm"> {feedsCount}</div>
                                : ""}
                            </div>

                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            active={this.state.viewNotifType === "msg"}
                            onClick={() => {
                              this.setState({ viewNotifType: "msg" });
                              this.handleNotificationCount("messages")
                            }}
                          >
                            <div className="text-uppercase fs-12">Messages
                            {msgCount > 0 ?
                                <Badge className="ml-1" color="primary"> {msgCount}</Badge>
                                // <div className="notification-count _sm"> {msgCount}</div>
                                : ""}
                            </div>
                            {/* Remove when 0 */}

                          </NavLink>
                        </NavItem>
                      </Nav>
                      <TabContent activeTab={this.state.viewNotifType}>
                        <TabPane tabId="notif">
                          <ul className="list-unstyled">
                            {fetchNotiFeeds && Array.isArray(fetchNotiFeeds) && fetchNotiFeeds.length > 0 ?
                              fetchNotiFeeds.slice(0, 5).map((itemFeed, indexFeed) => (
                                <li
                                  className="d-flex dropdown-item" data-type="notif" key={indexFeed}
                                  role="button"
                                >
                                  <div className="mr-3 mt-1">
                                    <div className="img-circle">
                                      <Link
                                        to="#"
                                        //  to={{ pathname: `/people/${itemFeed.actor.username}` }}
                                        target="_blank" className="mr-2"><img src={itemFeed?.actor?.current_profile_pic}
                                          className="img-fluid"
                                          onError={(error) => {
                                            error.target.src = require('./assets/images/user-01.jpg')
                                          }}
                                          alt={'no-image'}

                                        />
                                      </Link>
                                    </div>
                                  </div>
                                  {(itemFeed?.description == "commented_on_post") && (
                                    <div>
                                      <span className="font-weight-medium">
                                        <Link
                                          to="#"
                                          //  to={{ pathname: `/people/${itemFeed.actor.username}` }}
                                          target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                        {!itemFeed.post_created_by ?
                                          <Link
                                            to="#"
                                          // to={{ pathname: "/myprofile", state: { mainViewType: "posts" } }}
                                          >{itemFeed?.verb}</Link>
                                          :
                                          <Link
                                            to="#"
                                          // to={{ pathname: `/people/${itemFeed.post_created_by}` }}
                                          >{itemFeed?.verb}</Link>
                                        }
                                        {/* Live Refrence Link <a ng-if="!obj.post_created_by" href="/myprofile?notif_tab=post&pid={[{obj.object.parentid}]}&cmntid={[{obj.object.objectid}]}">{[{obj.verb}]}</a>
																								<a ng-if="obj.post_created_by" href="/people/{[{obj.post_created_by}]}?notif_tab=post&pid={[{obj.object.parentid}]}&cmntid={[{obj.object.objectid}]}">{[{obj.verb}]}</a>*/}
                                      </span>
                                    </div>
                                  )}
                                  {(itemFeed?.description == "owner_call_action_response") && (
                                    <div>
                                      <span className="font-weight-medium">
                                        <Link
                                          to="#"
                                          //  to={{ pathname: `/people/${itemFeed.actor.username}` }}
                                          target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                        {!itemFeed.post_created_by ?
                                          <Link
                                            to="#"
                                          // to={{ pathname: "/myprofile", state: { mainViewType: "messages" } }}
                                          >{itemFeed?.verb}</Link>
                                          :
                                          <Link to="#!">{itemFeed?.verb}</Link>
                                        }

                                      </span>
                                    </div>
                                  )}
                                  {(itemFeed?.description == "nailed_post" || itemFeed?.description == "priceless_post" || itemFeed?.description == "lol_post" || itemFeed?.description == "bummer_post" || itemFeed?.description == "woohoo_post" || itemFeed?.description == "insightful_post") && (
                                    <div>
                                      <span className="font-weight-medium">
                                        <Link
                                          to="#"
                                          // to={{ pathname: `/people/${itemFeed.actor.username}` }}
                                          target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                        <Link
                                          to="#"
                                        // to={{ pathname: "/myprofile", state: { mainViewType: "posts" } }}
                                        >{itemFeed?.verb}</Link>
                                        {/* Live Refrence Link <a href="/myprofile?notif_tab=post&pid={[{obj.object.parentid}]}&cmntid={[{obj.object.objectid}]}">{[{obj.verb}]}</a>*/}
                                      </span>
                                    </div>
                                  )}

                                  {(itemFeed?.description == "commented_on_review") && (
                                    <div>
                                      <span className="font-weight-medium">
                                        <Link
                                          to="#"
                                          // to={{ pathname: `/people/${itemFeed.actor.username}` }} 
                                          target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                        <Link
                                          to="#"
                                        // to={{ pathname: "/myprofile", state: { mainViewType: "posts" } }}
                                        >{itemFeed?.verb}</Link>
                                        {/*Live Refrence Link <a href="/myprofile?notif_tab=post&pid={[{obj.object.parentid}]}&cmntid={[{obj.object.objectid}]}">{[{obj.verb}]}</a> */}
                                      </span>
                                    </div>
                                  )}
                                  {(itemFeed?.description == "nailed" || itemFeed?.description == "priceless" || itemFeed?.description == "lol" || itemFeed?.description == "bummer" || itemFeed?.description == "woohoo" || itemFeed?.description == "insightful" || itemFeed?.description == "helpful" || itemFeed?.description == "watch_new_review" || itemFeed?.description == "pin_new_review") && (
                                    <div>
                                      <span className="font-weight-medium">
                                        <Link
                                          to="#"
                                          //  to={{ pathname: `/people/${itemFeed.actor.username}` }}
                                          target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                        {Object.keys(itemFeed.target).length > 0 && itemFeed.target.category ?
                                          <Link
                                            to="#"
                                          //  to={{ pathname: `/${itemFeed.target.category}/${itemFeed.target.slug && itemFeed.target.slug != "" ? itemFeed.target.slug : null}`, state: { id: itemFeed?.target?.userentry_id } }}
                                          >{itemFeed?.verb}</Link>
                                          :
                                          <Link to="#!">{itemFeed?.verb}</Link>
                                        }
                                        {/*Live Refrence Link <a  href="/{[{obj.target.category}]}/{[{obj.target.userentry_id}]}/{[{obj.target.slug}]}?rid={[{obj.target.review_id}]}">{[{obj.verb}]}</a>*/}
                                      </span>
                                    </div>
                                  )}

                                  {(itemFeed?.description == "watch_answered_question" || itemFeed?.description == "watch_asked_question" || itemFeed?.description == "pin_asked_question" || itemFeed?.description == "owner_response_on_question" || itemFeed?.description == "community_response_on_question") && (
                                    <div>
                                      <span className="font-weight-medium">
                                        {itemFeed?.description == "watch_answered_question" || itemFeed?.description == "watch_asked_question" || itemFeed?.description == "pin_asked_question" ?
                                          <Link
                                            to="#"
                                            // to={{ pathname: `/people/${itemFeed.actor.username}` }}
                                            target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                          :
                                          <Link to="#!">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                        }
                                        {Object.keys(itemFeed.target).length > 0 && itemFeed.target.category ?
                                          <Link
                                            to="#"
                                          // to={{ pathname: `/${itemFeed.target.category}/${itemFeed.target.slug && itemFeed.target.slug != "" ? itemFeed.target.slug : null}`, state: { id: itemFeed?.target?.userentry_id, notif_tab: "qa" } }}
                                          >{itemFeed?.verb}</Link>
                                          :
                                          <Link to="#!">{itemFeed?.verb}</Link>
                                        }
                                        {/*Live Refrence Link <a href="/{[{obj.target.category}]}/{[{obj.target.userentry_id}]}/{[{obj.target.slug}]}?notif_tab=questions&cid={[{obj.object.objectid}]}&qid={[{obj.object.parentid}]}">{[{obj.verb}]}</a> */}
                                      </span>
                                    </div>
                                  )}

                                  {(itemFeed?.description == "tagged" || itemFeed?.description == "post_tagged" || itemFeed?.description == "feed_tagged") && (
                                    <div>
                                      <span className="font-weight-medium">
                                        <Link
                                          to="#"
                                          //  to={{ pathname: `/people/${itemFeed.actor.username}` }}
                                          target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                        <Link
                                          to="#"
                                        //  to={{ pathname: `/people/${itemFeed.actor.username}` }}
                                        >{itemFeed?.verb}</Link>
                                        {/*Live Refrence Link <a href="/people/{[{ obj.actor.username}]}?notif_tab=post&pid={[{obj.object.objectid}]}">{[{obj.verb}]}</a>*/}
                                      </span>
                                    </div>
                                  )}
                                  {(itemFeed?.description == "upload_crp") && (
                                    <div>
                                      <span className="font-weight-medium">
                                        <Link
                                          to="#"
                                          // to={{ pathname: `/people/${itemFeed.actor.username}` }}
                                          target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                        {Object.keys(itemFeed.target).length > 0 && itemFeed.target.category ?
                                          <Link
                                            to="#"
                                          //  to={{ pathname: `/${itemFeed.target.category}/${itemFeed.target.slug && itemFeed.target.slug != "" ? itemFeed.target.slug : null}`, state: { id: itemFeed?.target?.userentry_id } }}
                                          >{itemFeed?.verb}</Link>
                                          :
                                          <Link to="#!">{itemFeed?.verb}</Link>
                                        }
                                        {/*Live Refrence Link <a href="/response/{[{obj.object.parentid}]}"> {[{obj.verb}]}</a>*/}
                                      </span>
                                    </div>
                                  )}

                                  {(itemFeed?.description == "crp_new_review" || itemFeed?.description == "friend_review_crp") && (
                                    <div>
                                      <span className="font-weight-medium">
                                        {itemFeed?.description == "friend_review_crp" ?
                                          <Link
                                            to="#"
                                            // to={{ pathname: `/people/${itemFeed.actor.username}` }} 
                                            target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                          :
                                          <Link to="#!">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                        }
                                        {Object.keys(itemFeed.target).length > 0 && itemFeed.target.category ?
                                          <Link
                                            to="#"
                                          //  to={{ pathname: `/${itemFeed.target.category}/${itemFeed.target.slug && itemFeed.target.slug != "" ? itemFeed.target.slug : null}`, state: { id: itemFeed?.target?.userentry_id } }}
                                          >{itemFeed?.verb}</Link>
                                          :
                                          <Link to="#!">{itemFeed?.verb}</Link>
                                        }
                                        {/*Live Refrence Link <a href="/response/{[{obj.object.parentid}]}/?notif_tab=crp&prid={[{obj.object.parentid}]}&rid={[{obj.object.objectid}]}">{[{obj.verb}]}</a>*/}
                                      </span>
                                    </div>
                                  )}

                                  {(itemFeed?.description == "biz_msg") && (
                                    <div>
                                      <span className="font-weight-medium">
                                        <Link to={{ pathname: `/people/${itemFeed.actor.username}` }} target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                        {Object.keys(itemFeed.target).length > 0 && itemFeed.target.category ?
                                          <Link to={{ pathname: `/${itemFeed.target.category}/${itemFeed.target.slug && itemFeed.target.slug != "" ? itemFeed.target.slug : null}`, state: { id: itemFeed?.target?.userentry_id } }}>{itemFeed?.verb}</Link>
                                          :
                                          <Link to="#!">{itemFeed?.verb}</Link>
                                        }
                                        {/*Live Refrence Link <a href="/{[{obj.target.category}]}/{[{obj.target.userentry_id}]}/{[{obj.target.slug}]}?{[{obj.notif_tab}]}">{[{obj.verb}]}</a>*/}
                                      </span>
                                    </div>
                                  )}

                                  {(itemFeed?.description == "pin_add_special_discount" ||
                                    itemFeed?.description == "watch_owner_edit_additional_info" ||
                                    itemFeed?.description == "watch_edit_additional_info" ||
                                    itemFeed?.description == "watch_add_special_discount" ||
                                    itemFeed?.description == "biz_add_employee" ||
                                    itemFeed?.description == "biz_new_review" ||
                                    itemFeed?.description == "watch_owner_add_special_discount" ||
                                    itemFeed?.description == "watch_owner_add_coupons" ||
                                    itemFeed?.description == "watch_add_coupons" ||
                                    itemFeed?.description == "biz_add_coupon" ||
                                    itemFeed?.description == "watch_edit_coolfacts" ||
                                    itemFeed?.description == "watch_added_advisory" ||
                                    itemFeed?.description == "watch_added_tips" ||
                                    itemFeed?.description == "watch_added_warning") && (
                                      <div>
                                        <span className="font-weight-medium">
                                          <Link
                                            to="#"
                                            // to={{ pathname: `/people/${itemFeed.actor.username}` }} 
                                            target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                          {Object.keys(itemFeed.target).length > 0 && itemFeed.target.category ?
                                            <Link
                                              to="#"
                                            // to={{ pathname: `/${itemFeed.target.category}/${itemFeed.target.slug && itemFeed.target.slug != "" ? itemFeed.target.slug : null}`, state: { id: itemFeed?.target?.userentry_id } }}
                                            >{itemFeed?.verb}</Link>
                                            :
                                            <Link to="#!">{itemFeed?.verb}</Link>
                                          }
                                          {/*Live Refrence Link <a href="/{[{obj.target.category}]}/{[{obj.target.userentry_id}]}/{[{obj.target.slug}]}?{[{obj.notif_tab}]}">{[{obj.verb}]}</a>*/}
                                        </span>
                                      </div>
                                    )}

                                  {(itemFeed?.description == "watch_add_solution" ||
                                    itemFeed?.description == "watch_add_problem" || itemFeed?.description == "watch_added_discussion" || itemFeed?.description == "watch_respond_discussion" || itemFeed?.description == "pin_added_discussion") && (
                                      <div>
                                        <span className="font-weight-medium">
                                          <Link
                                            to="#"
                                            // to={{ pathname: `/people/${itemFeed.actor.username}` }} 
                                            target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                          {Object.keys(itemFeed.target).length > 0 && itemFeed.target.category ?
                                            <Link
                                              to="#"
                                            //  to={{ pathname: `/${itemFeed.target.category}/${itemFeed.target.slug && itemFeed.target.slug != "" ? itemFeed.target.slug : null}`, state: { id: itemFeed?.target?.userentry_id, notif_tab: itemFeed?.notif_tab } }}
                                            >{itemFeed?.verb}</Link>
                                            :
                                            <Link to="#!">{itemFeed?.verb}</Link>
                                          }
                                          {/*Live Refrence Link <a href="/{[{obj.target.category}]}/{[{obj.target.userentry_id}]}/{[{obj.target.slug}]}?notif_tab={[{obj.notif_tab}]}&cid={[{obj.object.objectid}]}&qid={[{obj.object.parentid}]}">{[{obj.verb}]}</a>*/}
                                        </span>
                                      </div>
                                    )}

                                  {(itemFeed?.description == "watch_edit_website" || itemFeed?.description == "watch_owner_edit_website" || itemFeed?.description == "watch_edit_additional_info" || itemFeed?.description == "watch_owner_edit_phone" || itemFeed?.description == "watch_edit_phone" || itemFeed?.description == "watch_edit_model_number" || itemFeed?.description == "watch_owner_upload_media" || itemFeed?.description == "watch_upload_media" || itemFeed?.description == "watch_edit_email" || itemFeed?.description == "watch_owner_edit_email" || itemFeed?.description == "watch_owner_edit_name" || itemFeed?.description == "watch_edit_name" || itemFeed?.description == "watch_edit_paymentoptions" || itemFeed?.description == "watch_owner_edit_paymentoptions" || itemFeed?.description == "watch_owner_edit_address" || itemFeed?.description == "watch_edit_address" || itemFeed?.description == "watch_owner_change_hop" || itemFeed?.description == "watch_change_hop" || itemFeed?.description == "watch_upload_media" || itemFeed?.description == "watch_edit_model_number") && (
                                    <div>
                                      <span className="font-weight-medium">
                                        {itemFeed?.description != "watch_upload_media" || itemFeed?.description != "pin_change_hop" || itemFeed?.description != "watch_owner_edit_email" || itemFeed?.description != "watch_owner_edit_phone" || itemFeed?.description != "watch_owner_edit_paymentoptions" || itemFeed?.description != "watch_owner_edit_address" ?
                                          <Link to="#!">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                          :
                                          <Link
                                            to="#"
                                            // to={{ pathname: `/people/${itemFeed.actor.username}` }} 
                                            target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
                                        }
                                        {Object.keys(itemFeed.target).length > 0 && itemFeed.target.category ?
                                          <Link
                                            to="#"
                                          // to={{ pathname: `/${itemFeed.target.category}/${itemFeed.target.slug && itemFeed.target.slug != "" ? itemFeed.target.slug : null}`, state: { id: itemFeed?.target?.userentry_id } }}
                                          >{itemFeed?.verb}</Link>
                                          :
                                          <Link to="#!">{itemFeed?.verb}</Link>
                                        }
                                        {/*Live Refrence Link <a href="/{[{obj.target.category}]}/{[{obj.target.userentry_id}]}/{[{obj.target.slug}]}">{[{obj.verb}]}</a>*/}
                                      </span>
                                    </div>
                                  )}

                                  {(itemFeed?.description == "pin_change_hop" || itemFeed?.description == "pin_add_coupons") && (
                                    <div>
                                      <span className="font-weight-medium">
                                        {Object.keys(itemFeed.target).length > 0 && itemFeed.target.category ?
                                          <Link
                                            to="#"
                                          //  to={{ pathname: `/${itemFeed.target.category}/${itemFeed.target.slug && itemFeed.target.slug != "" ? itemFeed.target.slug : null}`, state: { id: itemFeed?.target?.userentry_id } }}
                                          >{itemFeed?.verb}</Link>
                                          :
                                          <Link to="#!">{itemFeed?.verb}</Link>
                                        }
                                        {/*Live Refrence Link <a href="/{[{obj.target.category}]}/{[{obj.target.userentry_id}]}/{[{obj.target.slug}]}">{[{obj.verb}]}</a>*/}
                                      </span>
                                    </div>
                                  )}

                                  {/* {(itemFeed?.description == "following") && (
																			<div>
																				<span className="font-weight-medium">
																				    <Link to={{ pathname: `/people/${itemFeed.actor.username}` }} target="_blank" className="mr-2">{itemFeed?.actor?.first_name} {itemFeed?.actor?.last_name}</Link>
																					{itemFeed?.verb}
																					<div className="text-right">
																					<Button color="primary" className="btn" size="xs" onClick={() => { this.handleRemoveFollower(itemFeed?.actor?.id)}}>Unfollow</Button>
																					</div>
																				</span>
																			</div>
																			)} */}
                                </li>
                              ))
                              : <div className="px-3 mt-2">
                                <span>See Notifications</span>
                              </div>}
                          </ul>
                          {fetchNotiFeeds && Array.isArray(fetchNotiFeeds) && fetchNotiFeeds.length > 0 ?
                            <div className="px-3 mt-2">
                              <Link
                                // to="#"
                                to="/notifications"
                                className="text-white ff-base fs-14">
                                See all notifications
                              </Link>
                            </div>
                            :
                            ""
                          }
                        </TabPane>
                        <TabPane tabId="msg">
                          <ul className="list-unstyled">
                            {fetchNotiMessage && Array.isArray(fetchNotiMessage) && fetchNotiMessage.length > 0 ?
                              fetchNotiMessage.slice(0, 5).map((itemMsg, indexMsg) => (
                                <li className="d-flex dropdown-item" data-type="notif" role="button" key={indexMsg}>
                                  <div className="mr-3 mt-1">
                                    <div className="img-circle">
                                      <img src={itemMsg?.current_profile_pic}
                                        className="img-fluid"
                                        onError={(error) => {
                                          error.target.src = require('./assets/images/user-01.jpg')
                                        }}
                                        alt={'no-image'}
                                      />
                                    </div>
                                  </div>
                                  <div className="flex-fill">
                                    <span className="font-weight-medium">
                                      {itemMsg.business_owner ?
                                        <Link to={{ pathname: `/${itemMsg.content_type}/${itemMsg.slug && itemMsg.slug != "" ? itemMsg.slug : null}`, state: { id: itemMsg?.business_id } }} target="_blank" className="mr-2">{itemMsg?.first_name} {itemMsg.last_name && itemMsg.last_name != null ? itemMsg.last_name : ""}</Link>
                                        :
                                        <Link to={{ pathname: `/people/${itemMsg.username}` }} target="_blank" className="mr-2">{itemMsg?.first_name} {itemMsg.last_name && itemMsg.last_name != null ? itemMsg.last_name : ""}</Link>

                                      }
                                    </span>
                                    {/* Message below */}
                                    <div>
                                      {itemMsg?.body.length > 20 ? itemMsg?.body.replace(/(.{20})..+/, "$1…") : itemMsg?.body}
                                    </div>

                                    {/* Time stamp */}
                                    <small className="text-right d-block">{itemMsg?.sent_at}</small>
                                  </div>
                                </li>
                              ))
                              : <div className="px-3 mt-2">
                                <span>No New Messages</span>
                              </div>}
                          </ul>
                          {fetchNotiMessage && Array.isArray(fetchNotiMessage) && fetchNotiMessage.length > 0 ?
                            <div className="px-3 mt-2">
                              <Link
                                // to={{ pathname: "/myprofile", state: { mainViewType: "messages" } }}
                                to="/corporate_msg_inbox"
                                className="text-white ff-base fs-14">
                                View Inbox
                              </Link>
                            </div>
                            : ""}
                        </TabPane>
                      </TabContent>
                    </DropdownMenu>
                  </UncontrolledDropdown>

                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav className="text-nowrap">
                      <span className="d-none d-md-inline-block">{user_name} </span>&nbsp;&nbsp;
                      {/* <FontAwesomeIcon icon="user-circle" /> */}
                      {profile_pic ? (
                        <img
                          src={profile_pic}
                          style={{
                            height: "30px",
                            width: "30px",
                            borderRadius: "50%",
                          }}
                        />
                      ) : (
                          <FontAwesomeIcon icon="user-circle" />
                        )}
                    </DropdownToggle>
                    <DropdownMenu
                      onClick={() => {
                        localStorage.clear();
                        this.props.setInitialAuth();
                        this.props.history.push('/')
                        // window.location.reload();
                      }}
                      right
                    >
                      <DropdownItem>
                        <span>{"Logout"}</span>
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </Nav>
              </Col>
            </Row>
          </Container>
        </Navbar>
      </React.Fragment>
    );
  }
}

const mapState = (state) => ({
  profileData: state.user.current_user,
  current_role_and_permissions: state.user.current_role_and_permissions,
  fetch_my_notification: state.user.get_notifications_data,
});
const mapDispatch = {
  post_notifications,
  get_notifications,
  set_nofiticationCount,
  setInitialAuth
};

export default withRouter(connect(mapState, mapDispatch)(AppHeader));
