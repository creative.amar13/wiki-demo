import React, {  Component } from 'react';
import { Container,Row, Col } from 'reactstrap';
import { library } from "@fortawesome/fontawesome-svg-core";
import { far } from "@fortawesome/free-regular-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from 'react-router-dom';

library.add(fab, far, fas);
class Footer extends Component{

    render(){
        
        return(
            
                <React.Fragment>
                    <footer className="footer text-center bg-light py-4">
                        <Container fluid={true} className="container-xl">
                            <Row className="align-items-center">
                                <Col xs={{order: 1}} className="col-lg text-lg-left tex-algin">WIKIREVIEWS © 2020</Col>
                                <Col lg={{size: 'auto', order: 2}} xs={{order: 3}} className=" text-center tex-algin">
                                <div className="d-md-inline-flex">
                                    <div className="mb-2 mb-md-0">
                                    <Link to="/about" target="_blank" className="mx-2 font-weight-bold text-decoration-none text-dark">About</Link> |
                                    <Link to="/press" target="_blank" className="mx-2 font-weight-bold text-decoration-none text-dark">Press</Link> |
                                    <Link to="/faq" target="_blank" className="mx-2 font-weight-bold text-decoration-none text-dark">FAQ</Link> |
                                    <Link to="/guidelines" target="_blank" className="mx-2 font-weight-bold text-decoration-none text-dark">Guidelines</Link>
                                    </div>
                                    <div className="d-none d-md-inline-block">|</div>
                                    <div>
                                    <Link to="/terms" target="_blank" className="mx-2 font-weight-bold text-decoration-none text-dark">Terms &amp; Conditions</Link> |
                                    <Link to="/privacy-policy" target="_blank"  className="mx-2 font-weight-bold text-decoration-none text-dark">Privacy Policy</Link>
                                    </div>
                                </div>
                                </Col>
                                <Col lg={{ order: 3 }} xs={{ order: 2 }} className="col-lg text-lg-right my-3 my-lg-0">
								<div className="mobile-app-footer">
        	
									<a href="https://itunes.apple.com/us/app/wikireviews/id1019136386?mt=8" className="app-store" target="_blank" rel="noopener noreferrer">&nbsp;</a><br/>
									
									<a href="javascript:void(0);" className="play-store" target="_blank" rel="noopener noreferrer">&nbsp;</a>
								</div>
                                <a className="btn btn-dark btn-circle mx-2" href="http://www.facebook.com/wikireviews" target="_blank" rel="noopener noreferrer">
                                    <FontAwesomeIcon icon={["fab", "facebook-f"]} />
                                </a>
                                <a className="btn btn-dark btn-circle mx-2" href="http://www.twitter.com/wikireviews" target="_blank" rel="noopener noreferrer">
                                    <FontAwesomeIcon icon={["fab", "twitter"]} />
                                </a>
                                </Col>
                            </Row>
                        </Container>
                    </footer>
                </React.Fragment>

        )
    }
}

export default Footer;