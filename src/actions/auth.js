import { callApi } from '../utils/apiCaller';
import { get_user_role_and_permissions } from "./user";
import { toast } from "react-toastify";
export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_ERROR = 'AUTH_ERROR';
export const IS_LOGIN_TRUE = 'IS_LOGIN_TRUE';
export const IS_SIGNUP_SUCCESS = 'IS_SIGNUP_SUCCESS';
export const IS_SIGNUP_ERROR = 'IS_SIGNUP_ERROR'
export const SET_INITIAL_AUTH_NEW = 'SET_INITIAL_AUTH_NEW';

export const setInitialAuth = () => ((dispatch) => (dispatch({ type: SET_INITIAL_AUTH_NEW, payload: null })));


export const auth_login_request_ = (data) => {
    return {
        type: AUTH_LOGIN,
        payload: data
    }
}

export const auth_login_error_ = (data) => {
    return {
        type: AUTH_ERROR,
        payload: data
    }
}

export const is_auth_login_true = (data) => {
    return {
        type: IS_LOGIN_TRUE,
        payload: data
    }
}

export const auth_signup_request_ = (data) => {
    return {
        type: IS_SIGNUP_SUCCESS,
        payload: data
    }
}

export const auth_signup_error_ = (data) => {
    return {
        type: IS_SIGNUP_ERROR,
        payload: data
    }
}



// AUTH LOGIN REQUEST
export const auth_login_request = (data) => {

    return (dispatch) => {
        callApi('/api-token/login/auth/', 'POST', data)
            .then((response) => {
                if (response && response.non_field_errors && response.non_field_errors.length > 0) {
                    return dispatch(auth_login_error_(response))
                }

                if (response && response.token) {
                    localStorage.setItem('token', response.token);
                    localStorage.setItem('profileId', response.profileid);
                    dispatch(get_user_role_and_permissions())
                    return dispatch(auth_login_request_(response))
                }
            })
    }
}


// Forgot your password  


export const forgot_password = (data) => {
    return (dispatch) => {
        callApi(`/api/password/reset/`, "POST", data).then((response) => {
            if (response && response.code !== 400 && response.code !== 500) {
                let displayMsg = "Something went wrong!"
                if (response.message) {
                    displayMsg = response.message;
                } else {
                    displayMsg = response.email;
                }
                toast(`${displayMsg}`, {
                    autoClose: 3500,
                    className: "black-background",
                    bodyClassName: "red-hunt",
                    progressClassName: "cc",
                });
                //return dispatch(add_password_value_(response));
            }
        });
    };
};

// SIGNUP REQUEST
export const signup_request = (data) => {
    return (dispatch) => {
        callApi('/api/corporate-signup/', "POST", data)
            .then((response) => {
                if (response && response.non_field_errors && response.non_field_errors.length > 0) {
                    return dispatch(auth_signup_error_(response))
                }
                return dispatch(auth_signup_request_(response))
            })
    }
}
