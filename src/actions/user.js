import { callApi, callApiAdvance } from "../utils/apiCaller";
import { toast } from "react-toastify";
import { get_review_count } from '../actions/counts';
import NotificationJSON from '../../src/utils/notification.json'
import NotificationPageJSON from '../../src/utils/notificationPage.json'

export const MY_PROFILE = "MY_PROFILE";
export const MAP_CORDINATES = "MAP_CORDINATES";
export const CORPORATE_ID = "CORPORATE_ID";
export const CORPORATE_ID_DETAILS = "CORPORATE_ID_DETAILS";
export const GET_CHART_STATS = "GET_CHART_STATS";
export const ADD_ADDTIONAL_DATA = "ADD_ADDTIONAL_DATA";
export const GET_ADMIN_STATS = "GET_ADMIN_STATS";
export const GET_BRANCH_COUNT = "GET_BRANCH_COUNT";
export const GET_FEED_LIST = "GET_FEED_LIST";
export const GET_EMPLOYEE_LIST = "GET_EMPLOYEE_LIST";
export const GET_MY_POSTS_LIST = "GET_MY_POSTS_LIST";
export const GET_SHARE_WITH_LIST = "GET_SHARE_WITH_LIST";
export const GET_ALBUM_TYPES_LIST = "GET_ALBUM_TYPES_LIST";
export const GET_ALBUM_TYPE_DATA = "GET_ALBUM_TYPE_DATA";
export const ADD_NAME = "ADD_NAME";
export const BUSINESS_UPDATE = "BUSINESS_UPDATE";
export const GET_MESSAGES_LIST = "GET_MESSAGES_LIST";
export const GET_ACCOUNT_LIST = "GET_ACCOUNT_LIST";
export const GET_BIZ_CALLON_DETAIL_LIST = "GET_BIZ_CALLON_DETAIL_LIST";
export const GET_CORPORATE_CALL_TO_ACTION = "GET_CORPORATE_CALL_TO_ACTION";
export const GET_ALL_BRANCHES = "GET_ALL_BRANCHES";
export const GET_BIZ_CALLON_TYPE = "GET_BIZ_CALLON_TYPE";
export const DELETE_CORPORATE_CALL = "DELETE_CORPORATE_CALL";
export const POST_CORPORATE_CALL = "POST_CORPORATE_CALL";
export const UPDATE_FEEDBACK_REPLY = "UPDATE_FEEDBACK_REPLY";
export const BUSINESS_CATEGORIES = "BUSINESS_CATEGORIES";
export const FEEDBACK_QUESTIONS_ANSWERED = "FEEDBACK_QUESTIONS_ANSWERED";
export const FEEDBACK_QUESTIONS_PENDING = "FEEDBACK_QUESTIONS_PENDING";
export const QA_QUESTIONS_ANSWERED = "QA_QUESTIONS_ANSWERED";
export const QA_QUESTIONS_PENDING = "QA_QUESTIONS_PENDING";
export const UPDATE_QA_REPLY = "UPDATE_QA_REPLY";
export const BUSINESS_SUB_CATEGORIES = "BUSINESS_SUB_CATEGORIES";
export const UPDATE_PAYMENT_OPTION = "UPDATE_PAYMENT_OPTION";
export const ADD_POSITION = "ADD_POSITION";
export const USER_ROLE_AND_PERMISSION = "USER_ROLE_AND_PERMISSION";
export const GET_SEARCH_BRANCH_CSR = "GET_SEARCH_BRANCH_CSR";
export const GET_SEARCH_BRANCH_CSR_LOADING = "GET_SEARCH_BRANCH_CSR_LOADING";
export const GET_SEARCH_BRANCH_CSR_ERROR = "GET_SEARCH_BRANCH_CSR_ERROR";
export const ADD_PASSWORD = "ADD_PASSWORD";
export const GET_PROFESSION_TYPE = "GET_PROFESSION_TYPE";
export const MENU_ITEM_LIST = "MENU_ITEM_LIST";
export const TOP_BAR_COUNTS = "TOP_BAR_COUNTS";
export const FILTERED_COUNTS = "FILTERED_COUNTS";
export const GET_EMPLOYEE_DETAIL = "GET_EMPLOYEE_DETAIL";
export const GET_REVIEWS_LIST = "GET_REVIEWS_LIST";
export const GET_BRANCH_MESSAGE_COUNT = "GET_BRANCH_MESSAGE_COUNT";
export const GET_CORPORATE_QA_COUNT = "GET_CORPORATE_QA_COUNT";
export const GET_STATISTICS_OWNER = "GET_STATISTICS_OWNER";
export const GET_BRANCH_COPY = "GET_BRANCH_COPY";
export const GET_BRANCH_DATA = "GET_BRANCH_DATA";
export const GET_MEDIA_DATA = "GET_MEDIA_DATA";
export const GET_NOTIFICATIONS_DATA = "GET_NOTIFICATIONS_DATA";
export const POST_NOTIFICATIONS_DATA = "POST_NOTIFICATIONS_DATA";
export const FETCH_NOTIFICATION_DATA = "FETCH_NOTIFICATION_DATA";
export const MENU_ITEM_CONTENT = "MENU_ITEM_CONTENT";
export const BRANCH_ENTITY_ID = "BRANCH_ENTITY_ID";
export const CORPORATE_COPY_DETAILS = "CORPORATE_COPY_DETAILS";
export const GET_FEEDBACK_LIST = "GET_FEEDBACK_LIST";
export const GET_QA_LIST = "GET_QA_LIST";
export const UPDATE_ADDITIONAL_INFO = "UPDATE_ADDITIONAL_INFO";
export const DELETE_MENU_ITEM = "DELETE_MENU_ITEM";
export const DELETE_ADDITIONAL_INFO = "DELETE_ADDITIONAL_INFO";

export const GET_ALL_DISPUTES_DATA = "GET_ALL_DISPUTES_DATA";
export const GET_DISPUTE_DATA = "GET_DISPUTE_DATA";
export const GET_SUB_CATEGORIES_DATA = "GET_SUB_CATEGORIES_DATA";
export const GET_AMENITIES_DATA = "GET_AMENITIES_DATA";
export const REVIEW_STATUS = "REVIEW_STATUS";
export const GET_SPECIFOC_BRANCH_CORPORATE_CALL_TO_ACTION = "GET_SPECIFOC_BRANCH_CORPORATE_CALL_TO_ACTION";
export const GET_CIRCLE_CLICK_BRANCHES = "GET_CIRCLE_CLICK_BRANCHES";
export const GET_MOVE_SET = "GET_MOVE_SET";





export const get_reviews_list_ = (data) => ({ type: GET_REVIEWS_LIST, payload: data, })
export const get_reviews_list = (filterType, pageNo = 1) => {
  let AppBranchID = localStorage.getItem('branchID')
  let url = `/api/corporatereviews/?filter=${filterType}&page=${pageNo}`
  if (AppBranchID && AppBranchID !== null && AppBranchID !== 'null') {
    url = `/api/corporatereviews/?filter=${filterType}&page=${pageNo}&associated_to=${AppBranchID}`
  }
  return (dispatch) => {
    callApi(url, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_reviews_list_(response));
      }
    });
  };
};

export const get_reviews_list_entity_id = (entity_id, filtertype) => {
  return (dispatch) => {
    // callApi(`/api/reviews/?entry=&associated_to=${entity_id}&filter=${filtertype}`, 'GET').then((response) => {
    callApi(`/api/corporatereviews/?entry=&associated_to=${entity_id}&filter=${filtertype}`, 'GET').then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_reviews_list_(response));
      }

    })
  }
}

export const add_reviews_discussion = (data, filterType, pageNo = 1, mapBranchClickData) => {
  return (dispatch) => {
    callApi(`/api/reviewdiscussion/?review=${data.review}`, "POST", data).then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          toast(`Comment added successfully!`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          if (mapBranchClickData) {
            return dispatch(get_admin_stats());
          } else {
            dispatch(get_review_count())
            return dispatch(get_reviews_list(filterType, pageNo));
          }
        }
      }
    );
  };
};

export const delete_reviews_discussion = (
  id,
  entity,
  filterType,
  pageNo = 1
) => {
  return (dispatch) => {
    callApi(
      `/api/deleterecord/?entityid=${id}&entity=${entity}`,
      "DELETE"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Comment deleted successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_reviews_list(filterType, pageNo));
      }
    });
  };
};

export const add_reviews_reply = (data, filterType, pageNo = 1) => {
  return (dispatch) => {
    callApi(`/api/reviewdiscussion/?review=${data.review}`, "POST", data).then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          toast(`Comment added successfully!`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          return dispatch(get_reviews_list(filterType, pageNo));
        }
      }
    );
  };
};

export const edit_reviews_discussion = (data, filterType, pageNo = 1) => {
  return (dispatch) => {
    callApi(`/api/reviewdiscussion/?review=${data.review}`, "PUT", data).then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          toast(`Comment updated successfully!`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          return dispatch(get_reviews_list(filterType, pageNo));
        }
      }
    );
  };
};

export const add_message = (data, viewMessagesType, viewMessagesSubType, mapBranchClickData) => {
  return (dispatch) => {
    callApi("/api/messagetobizowner/", "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Message added successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (mapBranchClickData) {
          dispatch(get_admin_stats());
        } else {
          return dispatch(
            get_messages_list(viewMessagesType, viewMessagesSubType)
          );
        }
      }
    });
  };
};

export const get_messages_list_ = (data) => {
  return {
    type: GET_MESSAGES_LIST,
    payload: data,
  };
};

export const get_messages_list = (viewMessagesType, viewMessagesSubType) => {
  let url = "";
  let AppBranchID = localStorage.getItem('branchID');


  if (viewMessagesType === "call_to_action") {
    if (viewMessagesSubType === "inbox") {
      url = "/api/requestappointment/";
    } else if (viewMessagesSubType === "draft") {
      url = "/api/actionresponse/?type=draft";
    } else if (viewMessagesSubType === "sent") {
      url = "/api/actionresponse/?type=sent";
    }
  } else if (viewMessagesType === "business") {
    if (AppBranchID && AppBranchID !== null && AppBranchID !== 'null') {
      url = `/api/corporatemessage/?associated_msg=${AppBranchID}`
    } else {
      url = "/api/corporatemessage/";
    }
  }

  return (dispatch) => {
    callApi(url, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        if (viewMessagesSubType === "inbox") {
          return dispatch(get_messages_list_(response));
        } else if (viewMessagesSubType === "sent") {
          return dispatch(get_messages_list_(response));
        } else if (viewMessagesSubType === "draft") {
          return dispatch(get_messages_list_(response));
        } else
          return dispatch(get_messages_list_(response));
      }
    });
  };
};

export const set_messages_action = (messageId, data, viewMessagesType, viewMessagesSubType) => {
  let url = `/api/requestappointment/${messageId}/`;
  return (dispatch) => {
    callApi(url, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_messages_list(viewMessagesType, viewMessagesSubType));
      }
    });
  };
};

export const set_messages_submit_draft = (data, viewMessagesType, viewMessagesSubType) => {
  let url = `/api/actionresponse/`;
  return (dispatch) => {
    callApi(url, "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_messages_list(viewMessagesType, viewMessagesSubType));
      }
    });
  };
};

export const set_drafted_messages = (data, id, viewMessagesType, viewMessagesSubType) => {
  let url = `/api/actionresponse/${id}`;
  return (dispatch) => {
    callApi(url, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_messages_list(viewMessagesType, viewMessagesSubType));
      }
    });
  };
};

export const delete_message = (id, viewMessagesType, viewMessagesSubType) => {
  let url = `/api/requestappointment/${id}/`;
  return (dispatch) => {
    callApi(url, "DELETE").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_messages_list(viewMessagesType, viewMessagesSubType));
      }
    });
  };
};

export const add_feed_comment = (url, data, filterType, pageNo) => {
  return (dispatch) => {
    callApi(url, "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Comment added successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_feed_list(filterType, pageNo));
      }
    });
  };
};

export const edit_feed_comment = (data, filterType, pageNo) => {
  return (dispatch) => {
    callApi("/api/otherfeeddiscussion/?", "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Comment updated successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_feed_list(filterType, pageNo));
      }
    });
  };
};

export const delete_feed_comment = (id, filterType, pageNo) => {
  return (dispatch) => {
    callApi(
      `/api/deleterecord/?entityid=${id}&entity=others_comment`,
      "DELETE"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Comment deleted successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_feed_list(filterType, pageNo));
      }
    });
  };
};

export const delete_business_categories = (entityid, userId, type) => {
  return (dispatch) => {
    callApi(
      `/api/deleterecord/?entity=taxonomy&entityid=${entityid}&userentry=${userId}`,
      "DELETE"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Category deleted successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(userId))
        } else { dispatch(corporate_id_details(userId)); }
      }
    });
  };
};

export const add_feed_comment_reply = (url, data, filterType, pageNo) => {
  return (dispatch) => {
    callApi(url, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Comment added successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_feed_list(filterType, pageNo));
      }
    });
  };
};

export const get_feed_list_ = (data) => {
  return {
    type: GET_FEED_LIST,
    payload: data,
  };
};

export const get_feed_list = (filterType, pageNo = 1) => {
  let AppBranchID = localStorage.getItem('branchID');
  let url = `/api/bizownerfeeds/?&filter_type=${filterType}&page=${pageNo}`

  if (AppBranchID && AppBranchID !== null && AppBranchID !== 'null') {
    url = `/api/bizownerfeeds/?id=${AppBranchID}&filter_type=${filterType}&page=${pageNo}`;
  }
  return (dispatch) => {
    callApi(url, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_feed_list_(response));
      }
    });
  };
};

// get employee profession type Listing
export const get_profession_type_ = (data) => {
  return {
    type: GET_PROFESSION_TYPE,
    payload: data,
  };
};

export const get_profession_type = (pageNo) => {
  return (dispatch) => {
    callApi(`/api/profession/?&page=${pageNo}`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_profession_type_(response));
      }
    });
  };
};

// add employee
export const add_employee = (data, reviews_professional__is_present) => {
  return (dispatch) => {
    callApi("/api/professional/?type=", "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Employee added successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_employee_list(reviews_professional__is_present));
      }
    });
  };
};

// copy add branch
export const copy_add_branch = (data) => {
  return (dispatch) => {
    callApi("/api/copyaddbranch/", "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Data copied successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
      }
    });
  };
};

export const fresh_add_branch = (data) => {
  return (dispatch) => {
    callApi("/api/copyfreshbranchdata/", "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Data copied successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
      }
    });
  };
};

// upload corporate media
export const upload_corporate_media = (data, type, userId) => {
  return (dispatch) => {
    callApi("/upload/uploadcorporatemedia", "POST", data).then((response) => {
      if (
        response &&
        response.code !== 400 &&
        response.code !== 500 &&
        response != "Error in connection"
      ) {
        if (type === 'copyBranch') {
          toast(`Image uploaded successfully!`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          return dispatch(corporate_copy_details(userId))
        } else if (type === 'companyLogo' || type === 'UploadBackgroundVideoOrImage') {

          toast(`Media uploaded successfully!`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          return dispatch(corporate_id_details(userId))
        }

      } else {
        toast(`Something Went Wrong!`, {
          autoClose: 2500,
          className: "toast-container",
          toastClassName: "dark-toast",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
      }
    });
  };
};

// update employee
export const update_employee = (
  data,
  employeeId,
  reviews_professional__is_present
) => {
  return (dispatch) => {
    callApi(`/api/professional/${employeeId}/`, "PUT", data).then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          toast(`Employee updated successfully!`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          return dispatch(get_employee_list(response.is_present));
        }
      }
    );
  };
};

// delete employee

export const delete_employee = (employId, professionalisPresent) => {
  return (dispatch) => {
    callApi(`/api/professionaluserentries/?empID=${employId}`, "DELETE").then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          toast(`Employee deleted successfully!`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          return dispatch(get_employee_list(professionalisPresent));
        }
      }
    );
  };
};

// approve employee

export const approve_employee = (
  data,
  approve,
  employId,
  professionalisPresent
) => {
  return (dispatch) => {
    callApi(
      `/api/professionaluserentries/?empID=${employId}&approve=${approve}`,
      "PUT",
      data
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Employee approve successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_employee_list(professionalisPresent));
      }
    });
  };
};

// disapprove employee

export const disapprove_employee = (
  data,
  disapprove,
  employId,
  professionalisPresent
) => {
  return (dispatch) => {
    callApi(
      `/api/professionaluserentries/?empID=${employId}&disapprove=${disapprove}`,
      "PUT",
      data
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Employee disapprove successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_employee_list(professionalisPresent));
      }
    });
  };
};

// get employee Listing
export const get_employee_list_ = (data) => ({ type: GET_EMPLOYEE_LIST, payload: data })
export const get_employee_list = (reviews_professional__is_present) => {
  return (dispatch) => {
    callApi(
      `/api/corporateprofessional/?&reviews_professional__is_present=${reviews_professional__is_present}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_employee_list_(response));
      }
    });
  };
};

// get employee Detail
export const get_employee_detail_ = (data) => ({ type: GET_EMPLOYEE_DETAIL, payload: data, })
export const get_employee_detail = (employeeId) => {
  return (dispatch) => {
    callApi(`/api/corporateprofessional/${employeeId}/?`, "GET").then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          return dispatch(get_employee_detail_(response));
        }
      }
    );
  };
};

// get all branch copy
export const get_branch_copy_ = (data) => {
  return {
    type: GET_BRANCH_COPY,
    payload: data,
  };
};

export const get_branch_copy = (country) => {
  return (dispatch) => {
    callApi(`/api/getallbranchcopy/?country=${country}`, "GET").then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          return dispatch(get_branch_copy_(response));
        }
      }
    );
  };
};

// get branch data
export const get_branch_data_ = (data) => {
  return {
    type: GET_BRANCH_DATA,
    payload: data,
  };
};

export const get_branch_data = (branch) => {
  return (dispatch) => {
    /*callApi(`/api/branchdata/?branch=${branch}`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_branch_data_(response));
      }
    });*/
    callApi(`/api/branchdata/?branch=${branch}`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_branch_data_(response));
      }
    });
  };
};

// get account data
export const get_account_list_ = (data) => {
  return {
    type: GET_ACCOUNT_LIST,
    payload: data,
  };
};

export const get_account_list = (c_dtls = true) => {
  return (dispatch) => {
    callApi(`/api/bizaccountupgrade/?&c_dtls=${c_dtls}`, "GET").then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          return dispatch(get_account_list_(response));
        }
      }
    );
  };
};

// get corporate call to action
export const get_corporate_call_list_ = (data) => {
  return {
    type: GET_CORPORATE_CALL_TO_ACTION,
    payload: data,
  };
};

export const get_corporate_call_list = (listing_id) => {
  return (dispatch) => {
    callApi(
      `/api/corporatecalltoaction/?&listing_id=${listing_id}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_corporate_call_list_(response));
      }
    });
  };
};

export const get_specific_branch_corporate_call_list_ = (data) => {
  return {
    type: GET_SPECIFOC_BRANCH_CORPORATE_CALL_TO_ACTION,
    payload: data,
  };
};

export const get_specific_branch_corporate_call_list = (listing_id) => {
  return (dispatch) => {
    callApi(
      `/api/corporatecalltoaction/?&listing_id=${listing_id}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_specific_branch_corporate_call_list_(response));
      }
    });
  };
};

export const add_my_post = (data, filterType, pageNo, branchID) => {
  return (dispatch) => {
    callApi("/api/feeds/?entity=", "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Post added successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_my_posts_list(filterType, pageNo, branchID));
      }
    });
  };
};

export const add_my_post_reply = (data, filterType, pageNo, branchID) => {
  return (dispatch) => {
    callApi("/api/savereply/", "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Comment added successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_my_posts_list(filterType, pageNo, branchID));
      }
    });
  };
};

export const add_my_post_comment = (id, data, filterType, pageNo, branchID) => {
  return (dispatch) => {
    callApi(`/api/feeds/${id}/?entity=`, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Comment added successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_my_posts_list(filterType, pageNo, branchID));
      }
    });
  };
};

export const delete_my_post = (postId, filterType, pageNo, branchID) => {
  return (dispatch) => {
    callApi(`/api/deleteuserfeeds/${postId}/`, "DELETE").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Post deleted successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_my_posts_list(filterType, pageNo, branchID));
      }
    });
  };
};

export const delete_my_post_comment = (
  messageId,
  userEntry,
  filterType,
  pageNo
) => {
  return (dispatch) => {
    callApi(
      `/api/deleterecord/?entity=others_comment&entityid=${messageId}&userentry=${userEntry}`,
      "DELETE"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Comment deleted successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_my_posts_list(filterType, pageNo));
      }
    });
  };
};

export const edit_my_post = (data, type, filterType, pageNo) => {
  return (dispatch) => {
    callApi(`/api/feeds/${data.id}/?entity=`, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(
          `${type.charAt(0).toUpperCase() + type.slice(1)
          } updated successfully!`,
          {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          }
        );
        return dispatch(get_my_posts_list(filterType, pageNo));
      }
    });
  };
};

export const get_my_posts_list_ = (data) => {
  return {
    type: GET_MY_POSTS_LIST,
    payload: data,
  };
};

export const get_my_posts_list = (filterType, pageNo = 1, branchID) => {
  return (dispatch) => {
    callApi(
      `/api/feeds/?postID=undefined&filter_type=${filterType}&owner_feed=true&page=${pageNo}&entity=${branchID}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_my_posts_list_(response));
      }
    });
  };
};

export const get_my_posts_list_by_id = (entity_id, filterType, pageNo = 1) => {
  return (dispatch) => {
    callApi(
      `/api/feeds/?id=${entity_id}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        // return dispatch(get_my_posts_list_(response));
      }
    });
  };
};

export const get_album_types_list_ = (data) => {
  return {
    type: GET_ALBUM_TYPES_LIST,
    payload: data,
  };
};

export const get_album_types_list = () => {
  return (dispatch) => {
    callApi(
      `/api/taxonomy/?category=Album&depth=true&delsel=&ancestor=&sub_cat=&parent=`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_album_types_list_(response));
      }
    });
  };
};

export const get_album_type_data_ = (data) => {
  return {
    type: GET_ALBUM_TYPE_DATA,
    payload: data,
  };
};

export const get_album_type_data = (mediaType, albumType, pageNo) => {
  return (dispatch) => {
    callApi(
      `/upload/list/${mediaType}/?album=${albumType}&page=${pageNo}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_album_type_data_(response));
      }
    });
  };
};

export const delete_selected_gallery_media = (data) => {
  return (dispatch) => {
    callApi(`/upload/multiuploader_delete_multiple/`, "POST", data).then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          return true;
        }
      }
    );
  };
};

export const delete_selected_gallery_media_Upload = (data) => {
  return (dispatch) => {
    callApi(`/upload/multiuploader_delete/${data}/`, "POST", data).then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          toast(`Image deleted!`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          return true;
        }
      }
    );
  };
};

export const admin_media_apload = (data) => {
  return (dispatch) => {
    callApi("/api/improve/?type=%20&type=media", "POST", data).then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          toast(`Post added successfully!`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          // return dispatch(get_my_posts_list(filterType, pageNo));
        }
      }
    );
  };
};

export const get_share_with_list_ = (data) => {
  return {
    type: GET_SHARE_WITH_LIST,
    payload: data,
  };
};

export const get_share_with_list = (filterType, userEntry, isBizOwner) => {
  return (dispatch) => {
    callApi(
      `/api/pins/?type=${filterType}&userentry=${userEntry}&biz_owner=${isBizOwner}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_share_with_list_(response));
      }
    });
  };
};

export const get_biz_callon_details_ = (data) => {
  return {
    type: GET_BIZ_CALLON_DETAIL_LIST,
    payload: data,
  };
};

// GET BIZ CALLON DETAIL

export const get_biz_callon_details = (data1) => {
  let user_id = data1.user_id;
  let listing_id = data1.listing_id;
  return (dispatch) => {
    callApi(
      `/api/biz_callon_details/?user_id=${user_id}&listing_id=${listing_id}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_biz_callon_details_(response));
      }
    });
  };
};

export const getallbranch_ = (data) => {
  return {
    type: GET_ALL_BRANCHES,
    payload: data,
  };
};

// ALL BRANCHES
export const getallbranch = () => {
  return (dispatch) => {
    callApi("/api/getallbranch/", "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(getallbranch_(response));
      }
    });
  };
};

export const get_biz_callon_type_ = (data) => {
  return {
    type: GET_BIZ_CALLON_TYPE,
    payload: data,
  };
};

// GET BIZ CALLON TYPE
export const get_biz_callon_type = () => {
  return (dispatch) => {
    callApi("/api/get_biz_callon_type/", "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_biz_callon_type_(response));
      }
    });
  };
};

export const corporate_call_to_action_ = (data) => {
  return {
    type: POST_CORPORATE_CALL,
    payload: data,
  };
};

// POST CORPORATE CALL TO ACTION
export const corporate_call_to_action = (data, id, type) => {
  return (dispatch) => {
    callApi("/api/corporatecalltoaction/", "POST", data).then((response) => {
      if (
        response &&
        //response.token !== undefined &&
        response.code !== 400 &&
        response.code !== 500
      ) {
        toast(`You have added a call to action successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (type === "copyBranch") {
          return dispatch(get_specific_branch_corporate_call_list(id))
        } else return dispatch(corporate_call_to_action_(response));
      }
    });
  };
};

export const delete_corporate_call_ = (data) => {
  return {
    type: DELETE_CORPORATE_CALL,
    payload: data,
  };
};

// DELETE CORPORATE CALL
export const delete_corporate_call = (corp_code, id, type) => {
  return (dispatch) => {
    callApi(`/api/corporatecalltoaction/?&code=${corp_code}`, "DELETE").then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          toast(`Delete successfully!`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          if (type === 'copyBranch') {
            return dispatch(get_specific_branch_corporate_call_list(id))
          } else {
            return dispatch(delete_corporate_call_(response));
          }
        }
      }
    );
  };
};

export const current_user_profile_ = (data) => {
  return {
    type: MY_PROFILE,
    payload: data,
  };
};

// CURRENT_USER_PROFILE
export const current_user_profile = () => {
  return (dispatch) => {
    callApi("/api/myprofile/?q=", "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(current_user_profile_(response));
      }
    });
  };
};

export const upload_move_set_profile_media = (data) => {
  return (dispatch) => {
    callApi(
      `/panning/?left=${data.left}&top=${data.top}&x=${data.x}&y=${data.y}`,
      "GET",
      data
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        // dispatch(upload_move_set_profile_media_());
        return dispatch(corporate_id());
      }
    });
  };
};

// export const upload_move_set_profile_media_ = (data) => {
//   return {
//     type: GET_MOVE_SET,
//     payload: data,
//   };
// };

export const get_map_cordinates_ = (data) => {
  return {
    type: MAP_CORDINATES,
    payload: data,
  };
};


export const get_map_cordinates = () => {
  return (dispatch) => {
    callApi("/api/adminbranchcsr/?type=branchmap", "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_map_cordinates_(response));
      }
    });
  };
};

// get_map_cordinates in file
export const get_map_cordinates_file_ = (data) => {
  return {
    type: 'MAP_CORDINATES_FILE',
    payload: data,
  };
};

export const get_map_cordinates_file = () => {
  return (dispatch) => {
    // callApi("/api/adminbranchcsr/?type=branchmap_data", "GET").then((response) => {

    callApi("/api/adminbranchcsr/?type=branchmap", "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        const apiData = response; if (apiData) {

          try {
            // let res = apiData.replace(/\t+$/, "");
            // let objRes = JSON.parse(res);
            let objRes = apiData;
            let existing_ids = []
            let responseData = []
            let data = []
            let lat_sum = 0, lat_long_sum = 0, lat_count = 0, lat_long_count = 0;
            objRes.data.map((e, index) => {
              if (existing_ids.indexOf(e.entries_id) < 0) { // check if id already exists or not
                existing_ids.push(e.entries_id)
                lat_sum += e.latitude ? e.latitude : 0
                lat_long_sum += e.longitude ? e.longitude : 0
                data.push(e)
                lat_count += e.latitude ? 1 : 0
                lat_long_count += e.longitude ? 1 : 0

              }
            })
            responseData['max_lat'] = lat_sum / lat_count;
            responseData['max_long'] = lat_long_sum / lat_long_count;
            responseData['total'] = responseData.length;
            responseData['data'] = data;
            if (responseData.length > 10) {
              responseData['zoomsize'] = 6
            }
            else {
              responseData['zoomsize'] = 5
            }
            return dispatch(get_map_cordinates_file_(responseData));

          } catch (e) {
            console.log('responseData', e); // error in the above string (in this case, yes)!
          }
        }

      }
    });
  };
};


// Corporate ID

export const corporate_id_ = (data) => {
  return {
    type: CORPORATE_ID,
    payload: data,
  };
};

export const corporate_id = () => {
  return (dispatch) => {
    callApi("/api/corporatedetails/", "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        dispatch(corporate_id_details(response.id));
        dispatch(get_top_bar_counts(response.id));
        return dispatch(corporate_id_(response));
      }
    });
  };
};

// Corporate ID Details

export const corporate_id_details_ = (data) => {
  return {
    type: CORPORATE_ID_DETAILS,
    payload: data,
  };
};

export const corporate_id_details = (id) => {
  return (dispatch) => {
    callApiAdvance({ endpoint: `/api/business/${id}/`, method: "GET", loader: false }).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        try {
          return dispatch(corporate_id_details_(response));
        }
        catch (e) {
          console.log(e)
        }
      }
    });
  };
};

export const clear_corporate_id_details = () => {
  return (dispatch) => {
  dispatch(corporate_copy_details_([]));
  };
};

// for copy branch ID Details

export const corporate_copy_details_ = (data) => {
  return {
    type: CORPORATE_COPY_DETAILS,
    payload: data,
  };
};
export const corporate_copy_details = (id) => {
  return (dispatch) => {
    callApi(`/api/business/${id}/`, "GET").then((response) => {

      if (response && response.code !== 400 && response.code !== 500) {
        try {
          return dispatch(corporate_copy_details_(response));
        } catch (e) {
          console.log(e); // error in the above string (in this case, yes)!
        }
      }
    });
  };
};

// Add value in additional Info

export const add_additional_value_ = (data) => {
  return {
    type: ADD_ADDTIONAL_DATA,
    payload: data,
  };
};

export const add_additional_value = (userId, data, type) => {
  return (dispatch) => {
    callApi(`/api/business/${userId}/`, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        let toastMsg = "";
        if (data.type === "meet_the_business_owner") {
          toastMsg = "Meet the Business Owner Updated Successfully";
        } else if (data.type === "business_owner_history") {
          toastMsg = "Business Owner History Updated Successfully";
        } else if (data.type === "specialties") {
          toastMsg = "Specialties Updated Successfully";
        }
        toast(`${toastMsg}!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });

        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(userId))
        } else { dispatch(corporate_id_details(userId)); }

        return dispatch(add_additional_value_(response));
      }
    });
  };
};

// Add Position

export const add_position_value_ = (data) => {
  return {
    type: ADD_POSITION,
    payload: data,
  };
};

export const add_position_value = (data, type) => {
  return (dispatch) => {
    callApi(`/api/myprofile/?q=`, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Position Updated Successfully`, {
          // ${response.message}!
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });

        if (type === 'copyBranch') {
          dispatch(current_user_profile())
        } else { return dispatch(add_position_value_(response)); }


      }
    });
  };
};

// Add Password

export const add_password_value_ = (data) => {
  return {
    type: ADD_PASSWORD,
    payload: data,
  };
};

export const add_password_value = (data) => {
  return (dispatch) => {
    callApi(`/api/password/change/`, "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Password updated successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(add_password_value_(response));
      }
    });
  };
};


// get chart stats

export const get_chart_stats_ = (data) => {
  return {
    type: GET_CHART_STATS,
    payload: data,
  };
};

export const get_chart_stats = () => {
  return (dispatch) => {
    callApi(`/api/corporatestatisticsowner/?entry=`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_chart_stats_(response));
      }
    });
  };
};

// get admin stats

export const get_admin_stats_ = (data) => {
  return {
    type: GET_ADMIN_STATS,
    payload: data,
  };
};

export const get_admin_stats = () => {
  return (dispatch) => {
    callApi(`/api/adminstats/`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_admin_stats_(response));
      }
    });
  };
};

// get branch bar

export const get_branch_count_data_ = (data) => {
  return {
    type: GET_BRANCH_COUNT,
    payload: data,
  };
};

export const get_branch_count_data = (userRoleType) => {
  return (dispatch) => {
    callApi(`/api/adminbranchcsr/?type=branchCSR&utype=${userRoleType}`, "GET").then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          return dispatch(get_branch_count_data_(response));
        }
      }
    );
  };
};

// Add Name

export const add_name_ = (data) => {
  return {
    type: ADD_NAME,
    payload: data,
  };
};

export const add_name = (data, type) => {
  return (dispatch) => {
    callApi(`/api/myprofile/?q=`, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Name updated successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });

        if (type === 'copyBranch') {
          dispatch(current_user_profile())
        } else { dispatch(add_name_(response)); }

      }
    });
  };
};

// post business update

export const post_business_update_ = (data) => {
  return {
    type: BUSINESS_UPDATE,
    payload: data,
  };
};

export const post_business_update = (data, isAdd, userId, type) => {
  return (dispatch) => {
    callApi(`/api/businessupdate/`, "POST", data).then((response) => {
      try {

        if (response && response.code !== 400 && response.code !== 500) {
          let displayMsg = "";
          if (data.type === "website_form") {
            displayMsg = "Website";
          } else if (data.type === "phone_form") {
            displayMsg = "Phone";
          } else if (data.type === "email_form") {
            displayMsg = "Email";
          }
          toast(`${displayMsg} ${isAdd ? "Business info added" : "Business info updated"} successfully!`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });

          if (type === 'copyBranch') {
            dispatch(corporate_copy_details(userId))
          } else { dispatch(corporate_id_details(userId)); }

          return dispatch(post_business_update_(response));
        }
      } catch (e) {
        console.log(e)
      }
    });
  };
};

// ADD SPECIAL OFFER

export const post_special_offer = (data, isAdd, userId, type) => {
  //let listing = "";
  //let offer_type = data.offer_type && data.offer_type != "" ? data.offer_type : "";
  return (dispatch) => {
    callApi(`/api/specialoffer/?listing_id=&offer_type=${data.offer_type}`, "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        let displayMsg = "Special Offer";
        toast(`${displayMsg} ${isAdd ? "added" : "updated"} successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(userId))
        } else { dispatch(corporate_id_details(userId)); }
      }
    });
  };
};

// UPDATE SPECIAL OFFER
export const update_special_offer = (data, userId, type) => {
  let id = data.entityid && data.entityid != "" ? data.entityid : 0;
  return (dispatch) => {
    callApi(`/api/specialoffer/${id}/`, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        let displayMsg = "Special Offer";
        toast(`${displayMsg} updated successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(userId))
        } else { dispatch(corporate_id_details(userId)); }
      }
    });
  };
};

// DELETE SPECIAL OFFER

export const delete_offer = (id, userId, type) => {
  return (dispatch) => {
    callApi(`/api/specialoffer/${id}/`, "DELETE").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Special offer deleted successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(userId))
        } else { dispatch(corporate_id_details(userId)); }
      }
    });
  };
};

// GET BUSINESS CATEGORY

export const get_business_category_data_ = (data) => {
  return {
    type: BUSINESS_CATEGORIES,
    payload: data,
  };
};

export const get_business_category_data = (type) => {
  // Business
  return (dispatch) => {
    callApi(
      `/api/taxonomy/?category=${type}&depth=true&delsel=&ancestor=&sub_cat=&parent=`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        if (type == "Business") {
          return dispatch(get_business_category_data_(response));
        }
        return dispatch(get_business_sub_category_data_(response));
      }
    });
  };
};

// GET BUSINESS SUB-CATEGORY

export const get_business_sub_category_data_ = (data) => {
  return {
    type: BUSINESS_SUB_CATEGORIES,
    payload: data,
  };
};

// delete business record

export const delete_business_record = (entity, entityid, userentry, type) => {
  return (dispatch) => {
    callApi(
      `/api/deleterecord/?entity=${entity}&entityid=${entityid}&userentry=${userentry}`,
      "DELETE"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Your ${entity} has been deleted!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(userentry))
        } else { dispatch(corporate_id_details(userentry)); }
      }
    });
  };
};

// update feedback comment

export const update_feedback_reply_ = (data) => {
  return {
    type: UPDATE_FEEDBACK_REPLY,
    payload: data,
  };
};

export const update_feedback_reply = (data) => {
  return (dispatch) => {
    callApi(`/api/feeds/${data.id}/?entity=`, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        // return dispatch(update_feedback_reply_(response));
      }
    });
  };
};

export const get_feedback_questions_data = (type) => {
  let AppBranchID = localStorage.getItem('branchID');
  let url = `/api/questions/?type=${type}&subType=feedback&page=1`

  if (AppBranchID && AppBranchID !== null && AppBranchID !== 'null') {
    url = `/api/questions/?entity=${AppBranchID}&type=${type}&subType=feedback&page=1`;
  }
  const request = callApi(url, "GET");
  return (dispatch) =>
    request.then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          if (type === "answered") {
            dispatch({
              type: FEEDBACK_QUESTIONS_ANSWERED,
              payload: response,
            });
          } else if (type === "pending") {
            dispatch({
              type: FEEDBACK_QUESTIONS_PENDING,
              payload: response,
            });
          }
        }
      },
      (error) => console.log("error", error)
    );
};

export const update_payment_options_ = (data) => {
  return {
    type: UPDATE_PAYMENT_OPTION,
    payload: data,
  };
};

export const update_payment_options = (userId, data, type) => {
  return (dispatch) => {
    callApi(
      `/api/corporatepaymentoptions/?corporatelistingid=${userId}`,
      "POST",
      data
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Payment Options updated successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(userId));
        } else { dispatch(corporate_id_details(userId)); }
      }
    });
  };
};

export const delete_payment_option = (entityid, userentry, type) => {
  return (dispatch) => {
    callApi(
      `/api/deleterecord/?entity=payment&entityid=${entityid}&userentry=${userentry}`,
      "DELETE"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Payment option deleted successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });

        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(userentry));
        } else {
          dispatch(corporate_id_details(userentry));
        }

      }
    });
  };
};

// POST BUSINESS CATEGORIES

export const post_business_category_data = (data, userId, type) => {
  return (dispatch) => {
    callApi(`/api/bizownerinfoupdate/`, "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Category updated successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(userId));
        } else { dispatch(corporate_id_details(userId)); }
      }
    });
  };
};

// get QA data
export const get_qa_data = (type) => {

  let AppBranchID = localStorage.getItem('branchID')
  let url = `/api/corporatequestions/?filter=${type}&page=1`
  if (AppBranchID && AppBranchID !== null && AppBranchID !== 'null') {
    url = `/api/corporatequestions/?filter=${type}&page=1&map_brid=${AppBranchID}`
  }
  // const request = ;
  return (dispatch) =>
    callApi(url, "GET").then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          if (type == "answered") {
            dispatch({
              type: QA_QUESTIONS_ANSWERED,
              payload: response,
            });
          }

          if (type == "pending") {
            dispatch({
              type: QA_QUESTIONS_PENDING,
              payload: response,
            });
          }
        }
      }
      // (error) => console.log("error", error)
    );
};

// update QA reply/comment

export const update_qa_reply_ = (data) => {
  return {
    type: UPDATE_QA_REPLY,
    payload: data,
  };
};

export const update_qa_reply = (data) => {
  return (dispatch) => {
    callApi(`/api/feeds/${data.id}/?entity=`, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        // return dispatch(update_qa_reply_(response));
      }
    });
  };
};

// POST HOURS OF OPERATION

export const post_hours_of_operation = (data, userId, type) => {
  let param = {}
  param['day_of_week'] = data.day_of_week
  param['start_time'] = data.start_time
  param['info'] = data.info
  param['entries_id'] = data.entries_id
  if (data.next_day) {
    param['end_time'] = data.end_time.toString() + " 1"
  }
  else {
    param['end_time'] = data.end_time.toString() + " 0"
  }

  // return 0
  return (dispatch) => {
    callApi(`/api/hoursofoperation/`, "POST", param).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Hours added successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(userId))
        } else { dispatch(corporate_id_details(userId)); }
      }
    });
  };
};

// PUT HOURS OF OPERATION

export const put_hours_of_operation = (data, id, userId, type) => {
  let param = {}
  param['day_of_week'] = data.day_of_week
  param['start_time'] = data.start_time
  param['info'] = data.info
  param['entries_id'] = data.entries_id
  if (data.next_day) {
    param['end_time'] = data.end_time.toString() + " 1"
  }
  else {
    param['end_time'] = data.end_time.toString() + " 0"
  }
  return (dispatch) => {
    callApi(`/api/hoursofoperation/${id}/`, "PUT", param).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Hours of Operation updated successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(userId))
        } else { dispatch(corporate_id_details(userId)); }
      }
    });
  };
};

// DELETE HOURS OF OPERATIONS

export const delete_hours_of_operation = (id, userId, type) => {
  return (dispatch) => {
    callApi(`/api/hoursofoperation/${id}/`, "DELETE").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Hours deleted successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(userId))
        } else { dispatch(corporate_id_details(userId)); }
      }
    });
  };
};

// delete questions/corporateQuestiosn

export const delete_questions_corporateQuestiosn = (data) => {
  return (dispatch) => {
    callApi(
      `/api/deleterecord/?entityid=${data && data.entityid}&entity=${data && data.entity
      }`,
      "DELETE"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        // return dispatch(get_feed_list(filterType, pageNo));
      }
    });
  };
};

// add comment feedback and QA

export const add_comment_feedback_qa = (data, apiName, id) => {
  return (dispatch) => {
    callApi(`/api/questions/${id}/?entity=`, "PUT", data).then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          if (apiName === "corporatequestions") {
            dispatch(get_qa_data("pending"));
            dispatch(get_qa_data("answered"));
          }
          if (apiName === "questions") {
            dispatch(get_feedback_questions_data("pending"));
            dispatch(get_feedback_questions_data("answered"));
          }
        }
      }
    );
  };
};
export const get_user_role_and_permissions_ = (data) => {
  return {
    type: USER_ROLE_AND_PERMISSION,
    payload: data,
  };
};

export const get_user_role_and_permissions = () => {
  return (dispatch) => {
    callApi(`/api/userolesettings/`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        dispatch(get_user_role_and_permissions_(response));
      }
    });
  };
};

// search bar CSR

export const get_search_branch_csr_ = (data) => {
  return {
    type: GET_SEARCH_BRANCH_CSR,
    payload: data,
  };
};

export const get_search_branch_csr = (data, search) => {
  return (dispatch) => {
    dispatch({
      type: GET_SEARCH_BRANCH_CSR_LOADING,
    });
    callApiAdvance({
      endpoint: `/api/searchbranchcsr/?q=${data.query}&type=${data.userRole}`,
      method: "GET",
      loader: false
    }).then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          try {
            response["data"] = data.query;
            dispatch(get_search_branch_csr_(response));
          }
          catch (e) {
            console.log(e)
          }
        }
      },
      (error) => {
        dispatch({
          type: GET_SEARCH_BRANCH_CSR_ERROR,
        });
      }
    );
  };
};

// get Menu Items
export const get_menu_list_ = (data) => {
  return {
    type: MENU_ITEM_LIST,
    payload: data,
  };
};

export const get_menu_list = (id) => {
  return (dispatch) => {
    callApi(`/api/getmenu/?${id}`, "GET").then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {

          return dispatch(get_menu_list_(response));
        }
      }
    );
  };
};

// get Menu Content
export const get_menu_content_ = (data) => {
  return {
    type: MENU_ITEM_CONTENT,
    payload: data,
  };
};

export const get_menu_content = (id, element) => {
  return (dispatch) => {
    let qa = window.unescape(`/api/getmenu/?${id}/&menuType=${element}`);
    callApi(qa, "GET").then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          return dispatch(get_menu_content_(response));
        }
      }
    );
  };
};

export const add_service_item = (data, id) => {
  return (dispatch) => {
    callApi(`/api/getmenu/?id=${id}`, "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Service item added successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        dispatch(get_menu_list(id));
      }
    });
  };
};

export const add_service_media = (data) => {
  return (dispatch) => {
    callApi(`/api/improve/?type=media`, "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return true;
      }
    });
  };
};

export const get_statistics_owner_ = (data) => {
  return {
    type: GET_STATISTICS_OWNER,
    payload: data,
  };
};
export const get_statistics_owner = (data) => {
  return (dispatch) => {
    callApiAdvance({ endpoint: `/api/statisticsowner/?entry=${data}`, method: "GET", loader: false }).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {

        dispatch(get_chart_stats_(response));
        dispatch(get_statistics_owner_(response));
      }
    });
  };
};


export const get_branch_message_count_ = (data) => {
  return {
    type: GET_BRANCH_MESSAGE_COUNT,
    payload: data,
  };
};

export const get_branch_message_count = (data) => {
  return (dispatch) => {
    callApiAdvance({ endpoint: `/api/branchmessagecount/?id=${data}`, method: "GET", loader: false }).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_branch_message_count_(response));
      }
    });
  };
};

export const get_corporate_qa_count_ = (data) => {
  return {
    type: GET_CORPORATE_QA_COUNT,
    payload: data,
  };
};

export const get_corporate_qa_count = (data) => {
  return (dispatch) => {
    callApiAdvance({ endpoint: `/api/corporateqacount/?map_brid=${data}`, method: "GET", loader: false }).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_corporate_qa_count_(response));
      }
    });
  };
};

export const get_owner_review_count1_ = (data) => {
  return {
    type: GET_CORPORATE_QA_COUNT,
    payload: data,
  };
};

export const get_owner_review_count1 = (data) => {
  return (dispatch) => {
    callApi(`/api/owner-review-count1/?id=${data}`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_owner_review_count1_(response));
      }
    });
  };
};

export const get_top_bar_counts_ = (data) => {
  return {
    type: TOP_BAR_COUNTS,
  };
};

// get badge branch message count
export const get_top_bar_counts = (id) => {
  return (dispatch) => {
    callApi(`/api/scrollbranch/?count=${id}&utype=nonadmin`, "GET").then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          dispatch(get_top_bar_counts_(response));
        }
      }
    );
  };
};

export const get_stats_filtered_data_ = (data) => {
  return {
    type: FILTERED_COUNTS,
    payload: data
  };
};

export const get_stats_filtered_data = (value) => {
  return (dispatch) => {
    callApi(`/api/adminstats/?filter_by=${value}`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        dispatch(get_stats_filtered_data_(value));
        dispatch(get_admin_stats_(response))
      }
    });
  };
};

export const media_action = (data) => {
  return {
    type: GET_MEDIA_DATA,
    payload: data,
  };
};

export const get_all_media_contents = ({ media_type, user_type }) => {
  let AppBranchID = localStorage.getItem('branchID');
  let url = `/api/corporatemedia/?media_type=${media_type}&user_type=${user_type}`

  if (AppBranchID && AppBranchID !== null && AppBranchID !== 'null') {
    url = `/api/corporatemedia/?media_type=${media_type}&user_type=${user_type}&entity=${AppBranchID}`;
  }
  return (dispatch) => {
    callApi(url, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(media_action(response));
      }
    });
  };
};

export const get_notifications_ = (data) => ({ type: GET_NOTIFICATIONS_DATA, payload: data });
export const get_notifications = () => {
  return (dispatch) => {
    callApiAdvance({ endpoint: `/api/notification/`, method: "GET", loader: false }).then((response) => {

      // callApi(`/api/notification/`, "GET", { type: "feeds" }).then(
      //   (response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_notifications_(response));
      }
    }
    );
  };
};

export const set_nofiticationCount = (data) => {
  return (dispatch) => {
    callApi(`/api/notification/`, "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_notifications());
      }
    });
  };
};

export const fetch_notification_ = (data) => {
  return {
    type: FETCH_NOTIFICATION_DATA,
    payload: data,
  };
};

export const fetch_notification_data = (page) => {
  return (dispatch) => {
    callApi(
      `/api/notification/?notify=true&page=${page}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(fetch_notification_(response));
      }
    });
  };
};

export const post_notifications_ = (data) => ({ type: POST_NOTIFICATIONS_DATA, payload: data });
export const post_notifications = (data) => {
  return (dispatch) => {
    callApi(`/api/notification/`, "POST", data).then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          return dispatch(post_notifications_(response));
        }
      }
    );
  };
};

export const get_data_by_role = (roleID, days) => {
  let branches = [];

  return (dispatch) => {
    callApi(
      `/api/corporaterolecount/?role_id=${roleID}&by_days=${days}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        // return dispatch(fetch_notification_(response));
        branches = response?.branch_data && Array.isArray(response?.branch_data) &&
          response?.branch_data.length > 0 && response?.branch_data.map((branch) => {
            return branch[0]
          })
        branches.map((newItem) => {
          let branchColor = '';
          branchColor = newItem.color?.overall?.red > 0 ?
            'red' : newItem.color?.overall?.orange > 0 ? 'orange' : 'green'
          if (branchColor !== '') {
            newItem.countDays = days;
            newItem.color = branchColor;
          }
        })
        return dispatch(get_circle_click_branches_(branches))
      }
    });
  };
};

export const get_data_by_color = (color, days) => {
  let branches = [];
  return (dispatch) => {
    let newdata = []
    callApi(
      `/api/corporaterolecount/?color=${color}&by_days=${days}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        branches = response?.branch_data && Array.isArray(response?.branch_data) &&
          response?.branch_data.length > 0 && response?.branch_data.map((branch) => {
            return branch[0]
          })
        branches.map((newItem) => {
          newItem.countDays = days;
          newItem.color = color;
        })
        return dispatch(get_circle_click_branches_(branches))
      }
    });
  };
};

export const get_branch_data_by_circle_click = ({ color, type, days, queryType, branchID }) => {

  return (dispatch) => {
    if (queryType === "corporatereviewsnew") {
      // callApi(`/api/${queryType}/?colour=${color}&days=${days}`, "GET").then((response) => {
      callApi(`/api/${queryType}/?filter_type=custom&color=${color}&type=reviews&by_days=${days}&branch_id=${branchID}`, "GET").then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          if (queryType === 'corporatereviewsnew' && type === 'new') {
            dispatch({
              type: 'TOPBAR_REVIEWS',
              payload: color
            })
            return dispatch(get_reviews_list_(response));
          }
          else if (queryType === 'corporatereviewsnew' && type === 'disputed') {
            dispatch({
              type: 'TOPBAR_DISPUTED_REVIEWS',
              payload: color
            })
            return dispatch(get_reviews_list_(response));
          }
        }
      });
    } else if (queryType === "corporatemessage") {
      callApi(`/api/${queryType}/?filter_type=custom&color=${color}&type=${type}&by_days=${days}&branch_id=${branchID}`, "GET").then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          if (queryType === 'corporatemessage' && type === 'message') {
            dispatch({
              type: 'TOPBAR_MESSAGE',
              payload: color
            })
            return dispatch(get_messages_list_(response));
          }
        }
      });
    } else if (queryType === "corporatequestions") {
      callApi(`/api/${queryType}/?filter=${type}&color=${color}&by_days=${days}&branch_id=${branchID}`, "GET").then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          if (queryType === 'corporatequestions') {
            dispatch({
              type: 'TOPBAR_QA',
              payload: color
            })
            return dispatch(get_qa_list_(response));
          }
        }
      });
    }
    else if (queryType === "corporatefeedbackcount") {

      // callApi(`/api/${queryType}/?filter_fb=${type}`, "GET").then((response) => {
      callApi(`/api/${queryType}/?filter_fb=${type}&color=${color}&by_days=${days}&branch_id=${branchID}`, "GET").then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {

          if (queryType === 'corporatefeedbackcount' && type === 'pending') {
            dispatch({
              type: 'TOPBAR_FEEDBACK',
              payload: color
            })
            return dispatch(get_feedback_list_(response));
          }
        }
      });
    }
  };
};

export const get_review_data_by_circle_click = ({ color, type, days, queryType, getBranches }) => {
  return (dispatch) => {
    dispatch({
      type: 'INIT_TOPBAR',
    });
    if (queryType === "corporatereviewsnew") {
      // callApi(`/api/${queryType}/?colour=${color}&days=${days}`, "GET").then((response) => {
      callApi(`/api/${queryType}/?filter_type=custom&color=${color}&type=reviews&by_days=${days}&getbranches=yes`, "GET").then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          if (queryType === 'corporatereviewsnew' && type === 'new') {
            response.forEach(
              (key) => {
                if (key !== 'code') {
                  key.countDays = days
                }
              }
            );
            return dispatch(get_circle_click_branches_(response));
          }
          else if (queryType === 'corporatereviewsnew' && type === 'disputed') {
            response.forEach(
              (key) => {
                if (key !== 'code') {
                  key.countDays = days
                  key.reviewType = type
                }
              }
            );
            // return dispatch(get_reviews_list_(response));
            return dispatch(get_circle_click_branches_(response));
          }
        }
      });
    } else if (queryType === "corporatemessage") {
      callApi(`/api/${queryType}/?filter_type=custom&color=${color}&type=${type}&by_days=${days}&getbranches=yes`, "GET").then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          if (queryType === 'corporatemessage' && type === 'message') {
            response.forEach(
              (key) => {
                if (key !== 'code') {
                  key.countDays = days
                }
              }
            );
            return dispatch(get_circle_click_branches_(response));
          }
        }
      });
    } else if (queryType === "corporatequestions") {
      callApi(`/api/${queryType}/?filter=${type}&color=${color}&by_days=${days}&getbranches=yes`, "GET").then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          if (queryType === 'corporatequestions') {
            response.forEach(
              (key) => {
                if (key !== 'code') {
                  key.countDays = days
                }
              }
            );
            return dispatch(get_circle_click_branches_(response));
            // return dispatch(get_qa_list_(response));
          }
        }
      });
    }
    else if (queryType === "corporatefeedbackcount") {

      callApi(`/api/${queryType}/?filter_fb=${type}&color=${color}&type=feedback&by_days=${days}&getbranches=yes`, "GET").then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {

          if (queryType === 'corporatefeedbackcount' && type === 'pending') {
            // dispatch({
            //   type: 'TOPBAR_FEEDBACK',
            //   payload: color
            // })
            // return dispatch(get_feedback_list_(response));
            response.forEach(
              (key) => {
                if (key !== 'code') {
                  key.countDays = days
                  key.type = 'feedback'
                }
              }
            );
            return dispatch(get_circle_click_branches_(response));
          }
        }
      });
    }
  };
};

export const get_circle_click_branches_ = (data) => {
  return {
    type: GET_CIRCLE_CLICK_BRANCHES,
    payload: data,
  };
};


export const get_feedback_list_ = (data) => ({ type: GET_FEEDBACK_LIST, payload: data, })
export const get_qa_list_ = (data) => ({ type: GET_QA_LIST, payload: data, })

export const update_on_list_view_branch = ({ entry }) => {
  return (dispatch) => {
    callApi(`/api/statisticsowner/?entry=${entry}`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        dispatch(get_chart_stats_(response));
        dispatch(get_statistics_owner_(response));
      }
    });
  };
};


export const set_branch_ID_local_storage = (id) => {

  return (dispatch) => {
    localStorage.setItem('branchID', id);
    dispatch(corporate_id_details(id));
    dispatch(get_review_count());
    dispatch(get_branch_message_count(id));
    dispatch(get_corporate_qa_count(id));
    dispatch(get_statistics_owner(id));
    // dispatch(get_my_posts_list_by_id(id));

    dispatch({
      type: BRANCH_ENTITY_ID,
      payload: id
    })
  }
};

export const get_tabs_data_entity_id = (entity, type, subType) => {
  return (dispatch) => {
    let callString = `/api/questions/?entity=${entity}&type=${type}&subType=${subType}&page=1`;
    callApi(callString, 'GET').then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {

        if (type === "answered" && subType === undefined) {
          return dispatch({
            type: QA_QUESTIONS_ANSWERED,
            payload: response,
          });
        } else if (type === "pending" && subType === undefined) {
          return dispatch({
            type: QA_QUESTIONS_PENDING,
            payload: response,
          });
        } else if (type === "answered" && subType === 'feedback') {
          return dispatch({
            type: FEEDBACK_QUESTIONS_ANSWERED,
            payload: response,
          });
        } else if (type === "pending" && subType === 'feedback') {
          return dispatch({
            type: FEEDBACK_QUESTIONS_PENDING,
            payload: response,
          });
        } else {
          console.log('feedback Here');
        }
      }
    })
    // (error) => console.log("feedback error", error)
  }
};


export const enableBranchForTour = (data) => (dispatch, getState) => {
  if (getState || getState === "true") {
    dispatch(
      { type: 'ACTIVE_TAB_BRANCH', payload: data }
    );
  }
}
export const enableRolesForTour = (data) => (dispatch, getState) => {
  if (getState || getState === "true") {
    dispatch({ type: 'ACTIVE_TAB_ROLES', payload: data });
  }
}
export const enableSettingsForTour = (data) => (dispatch, getState) => {
  if (getState || getState === "true") {
    dispatch({ type: 'ACTIVE_TAB_SETTINGS', payload: data });
  }
}

export const update_additional_info_ = (data) => ({ type: UPDATE_ADDITIONAL_INFO, payload: "updated", })

export const update_additional_info = (data, entry, type) => {
  return (dispatch) => {
    callApi(`/api/business/${entry}/`, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Info updated successfully!`, {
          autoClose: 2000,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });

        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(entry))
          return dispatch(update_additional_info_("updated"));
        } else {
          dispatch(corporate_id_details(entry))
          return dispatch(update_additional_info_("updated"));
        }

      }
    });
  };
};

export const delete_additional_info_ = (data) => ({ type: DELETE_ADDITIONAL_INFO, payload: "updated", })

export const delete_additional_info = (data, entry, type) => {
  return (dispatch) => {
    callApi(`/api/business/${entry}/`, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Info deleted successfully!`, {
          autoClose: 2000,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        if (type === 'copyBranch') {
          dispatch(corporate_copy_details(entry))
          return dispatch(update_additional_info_("updated"));
        } else {
          dispatch(corporate_id_details(entry))
          return dispatch(update_additional_info_("updated"));
        }

      }
    });
  };
};

export const Delete_menu_item_ = (data) => ({ type: DELETE_MENU_ITEM, payload: "deleted", })


export const delete_menu_item = (item) => {
  return (dispatch) => {
    callApi(`/api/getmenu/?menuType=${item}`, "DELETE").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Menu deleted successfully!`, {
          autoClose: 2000,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(Delete_menu_item_("updated"));

      }
    });
  };
};

export const clear_search_branch_csr = (data) => {
  return (dispatch) => {
    dispatch({
      type: GET_SEARCH_BRANCH_CSR,
      payload: [data],
    });
  }
}


//dispute review
export const checkStatusDispatch = (data) => {
  return {
    type: REVIEW_STATUS,
    payload: data,
  };
};
export const add_dispute_review = (data, filterType, pageNo = 1) => {
  let formData = new FormData();
  if (data?.check_status) {
    formData.append("check_status", data.check_status);
    formData.append("review_id", data.review_id);

    return async (dispatch) => {
      callApi(`/api/disputes/`, "POST", formData, true).then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          if (response?.Status) {
            dispatch(checkStatusDispatch(response.Status));
            toast(response.Status, {
              autoClose: 5000,
              className: "black-background",
              bodyClassName: "red-hunt",
              progressClassName: "cc",
            });
          }
          else {
            dispatch(checkStatusDispatch(response.Status));
          }
        }
      });
    };
  }
  else {
    formData.append("reason", data.reason);
    formData.append("comment", data.comment);
    formData.append("review_id", data.review_id);
    for (let i = 0; i < data.dispute_file.length; i++) {
      formData.append("dispute_file", data.dispute_file[i]);
    }

    return async (dispatch) => {
      callApi(`/api/disputes/`, "POST", formData, true).then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          if (response.Message) {
            toast(response.Message, {
              autoClose: 3000,
              className: "black-background",
              bodyClassName: "red-hunt",
              progressClassName: "cc",
            });
          }
          else {
            toast(`Review has been marked as disputed!`, {
              autoClose: 3000,
              className: "black-background",
              bodyClassName: "red-hunt",
              progressClassName: "cc",
            });
          }
          return dispatch(get_reviews_list(filterType, pageNo));
        }
      });
    };
  }
};

export const all_disputes_action = (data) => {
  return {
    type: GET_ALL_DISPUTES_DATA,
    payload: data,
  };
};

export const get_all_disputed_reviews = () => {

  let AppBranchID = localStorage.getItem('branchID');
  let url = `/api/corporatereviews/?filter=disputed&page=1`

  if (AppBranchID && AppBranchID !== null && AppBranchID !== 'null') {
    url = `/api/corporatereviews/?filter=disputed&page=1&associated_to=${AppBranchID}`
  }
  return (dispatch) => {
    callApi(
      // `/api/corporatereviews/?filter=disputed&page=1&associated_to=${AppBranchID}`,
      url, "GET").then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          let data = []
          response && response.results && response.results.map((res) => {
            if (res.is_review_flag) {
              data.push(res)
            }
          })
          return dispatch(all_disputes_action(data));
        }
      });
  };
}

export const get_dispute_action = (data) => {
  return {
    type: GET_DISPUTE_DATA,
    payload: data,
  };
};

export const get_dispute_discussion = (review_id) => {
  return (dispatch) => {
    callApi(
      `/api/disputes/?review_id=${review_id}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_dispute_action(response));
      }
    });
  };
}

export const delete_dispute = (review_id) => {
  return (dispatch) => {
    callApi(
      `/api/disputes/?review_id=${review_id}`,
      "DELETE"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Dispute has been removed!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
      }
      dispatch(get_all_disputed_reviews())
    });
  };

}

export const add_dispute_reply = (data) => {
  let formData = new FormData();
  formData.append('review_id', data.review_id);
  formData.append('flag_content_id', data.flag_content_id);
  formData.append('flag_id', data.flag_id);
  formData.append('comment', data.comment);

  for (let i = 0; i < data.dispute_reply_file.length; i++) {
    formData.append('dispute_reply_file', data.dispute_reply_file[i]);
  }

  return (dispatch) => {
    callApi(`/api/disputes-history/`, "POST", formData, true).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        if (response.done) {
          toast("Your reply has been added Successfully!", {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
        }
      }
      return dispatch(get_dispute_discussion(data.review_id));
    });
  };
}

export const delete_dispute_reply = (pk, review_id) => {
  return (dispatch) => {
    callApi(
      `/api/disputes-history/?flag_content_id=${pk}`,
      "DELETE"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Reply has been removed!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
      }
      return dispatch(get_dispute_discussion(review_id));
    });
  };
}

export const get_sub_categories_action = (data) => {
  return {
    type: GET_SUB_CATEGORIES_DATA,
    payload: data,
  };
};

export const get_sub_categories = (item) => {
  return (dispatch) => {
    callApi(
      `/api/additional-attributes-by-id/?category=${item.id}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_sub_categories_action(response));
      }
    });
  };

}

export const get_amenities_options_action = (data) => {
  return {
    type: GET_AMENITIES_DATA,
    payload: data,
  };

}

export const get_amenities_options = (id) => {
  return (dispatch) => {
    callApi(
      `/api/additional-attributes-options-by-id/?amenity=${id}`,
      "GET"
    ).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_amenities_options_action(response));
      }
    });
  };

}

export const update_listing_owner_guide = (data, entry) => {
  return (dispatch) => {
    callApi(`/api/business/${entry}/`, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Listing Owner Guide Updated successfully!`, {
          autoClose: 2000,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
      }
    });
  };
};
