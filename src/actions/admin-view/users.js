import { callApi } from "../../utils/apiCaller";

export const ADD_USER = "ADD_USER";
export const FETCH_USERS_SUCCESS = "FETCH_USERS_SUCCESS";
export const FETCH_USERS_FAILURE = "FETCH_USERS_FAILURE";

export const addUser = (user) => ({
  type: ADD_USER,
  payload: { user },
});

export const fetchUsersSuccess = (users) => ({
  type: FETCH_USERS_SUCCESS,
  payload: { users },
});

export const fetchUsersFailure = (error) => ({
  type: FETCH_USERS_FAILURE,
  payload: { error },
});

export const fetchUsers = () => {
  return (dispatch, getState) => {
    callApi("/", "GET")
      .then((res) => res.data)
      .then((data) => {
        dispatch(fetchUsersSuccess(data));
      })
      .catch((error) => {
        dispatch(fetchUsersFailure(error));
      });
  };
};
