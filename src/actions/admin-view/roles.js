import { callApi } from "../../utils/apiCaller";

export const ADD_ROLE = "ADD_ROLE";
export const FETCH_ROLES_SUCCESS = "FETCH_ROLES_SUCCESS";
export const FETCH_ROLES_FAILURE = "FETCH_ROLES_FAILURE";

export const addRole = (role) => ({
  type: ADD_ROLE,
  payload: { role },
});

export const fetchRolesSuccess = (roles) => ({
  type: FETCH_ROLES_SUCCESS,
  payload: { roles },
});

export const fetchRolesFailure = (error) => ({
  type: FETCH_ROLES_FAILURE,
  payload: { error },
});

export const fetchRoles = () => {
  return (dispatch, getState) => {
    callApi("/api/getallrole/", "GET")
      .then((res) => res.results.result)
      .then((data) => {
        dispatch(fetchRolesSuccess(data));
      })
      .catch((error) => {
        dispatch(fetchRolesFailure(error));
      });
  };
};
