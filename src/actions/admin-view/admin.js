import { callApi } from "../../utils/apiCaller";
import { toast } from "react-toastify";

export const GET_ROLES_LIST = "GET_ROLES_LIST";
export const GET_COUNTRIES_LIST = "GET_COUNTRIES_LIST";
export const GET_STATES_LIST = "GET_STATES_LIST";
export const GET_BRANCHES_LIST = "GET_BRANCHES_LIST";
export const GET_PEOPLE_LIST = "GET_PEOPLE_LIST";
export const GET_PEOPLE_DATA = "GET_PEOPLE_DATA";
export const GET_ROLEVIEW = "GET_ROLEVIEW";
export const ADD_BRANCH = "ADD_BRANCH";
export const FETCH_BRANCHES_SUCCESS = "FETCH_BRANCHES_SUCCESS";
export const FETCH_BRANCHES_FAILURE = "FETCH_BRANCHES_FAILURE";
export const GET_BRANCH_SUCCESS = "GET_BRANCH_SUCCESS";
export const GET_BRANCH_FAILURE = "GET_BRANCH_FAILURE";
export const GET_STATE_SUCCESS = "GET_STATE_SUCCESS";
export const GET_STATE_FAILURE = "GET_STATE_FAILURE";
export const UPADATE_BRANCH_SUCCESS = "UPADATE_BRANCH_SUCCESS";
export const UPADATE_BRANCH_FAILURE = "UPADATE_BRANCH_FAILURE";
export const DELETE_BRANCH_SUCCESS = "DELETE_BRANCH_SUCCESS";
export const DELETE_BRANCH_FAILURE = "DELETE_BRANCH_FAILURE";
export const CLEAR_ROLEVIEW_STORE_STATE = "CLEAR_ROLEVIEW_STORE_STATE";

export const ADD_BRANCH_SUCCESS = "ADD_BRANCH_SUCCESS";
export const ADD_BRANCH_FAILURE = "ADD_BRANCH_FAILURE";

export const GET_BRANCH_LIST = "GET_BRANCH_LIST";

export const GET_SETTINGS_LIST = "GET_SETTINGS_LIST";

export const get_branches_list_ = (data) => {
  return {
    type: GET_BRANCHES_LIST,
    payload: data,
  };
};

export const get_branches_list = () => {
  return (dispatch) => {
    callApi(`/api/getallbranch/`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_branches_list_(response.results && response.results.result));
      }
    });
  };
};

export const get_countries_list_ = (data) => {
  return {
    type: GET_COUNTRIES_LIST,
    payload: data,
  };
};

export const get_countries_list = () => {
  return (dispatch) => {
    callApi(`/api/getAllCountry/`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_countries_list_(response));
      }
    });
  };
};

export const get_states_list_ = (data) => {
  return {
    type: GET_STATES_LIST,
    payload: data,
  };
};

export const get_states_list = (country) => {
  return (dispatch) => {
    callApi(`/api/getStates/?country=${country || `United States`}`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_states_list_(response));
      }
    });
  };
};

export const get_roles_list_ = (data) => {
  return {
    type: GET_ROLES_LIST,
    payload: data,
  };
};

export const get_roles_list = () => {
  return (dispatch) => {
    callApi(`/api/getallrole/`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_roles_list_(response.results));
      }
    });
  };
};
export const get_roles_list_filter_data = (value) => {
  return (dispatch) => {
    callApi(`/api/getallrole/?by_days=${value}`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_roles_list_(response.results));
      }
    });
  };
};

// export const addBranch = (branch) => ({
//   type: ADD_BRANCH,
//   payload: { branch },
// });

export const getBranch = (payload) => {
  return (dispatch, getState) => {
    callApi(`/api/branchview/?branch_id=${payload.id}`, "GET")
      .then((res) => res)
      .then((data) => {
        dispatch({
          type: GET_BRANCH_SUCCESS,
          payload: { data },
        });
      })
      .catch((error) => {
        dispatch({
          type: GET_BRANCH_FAILURE,
          payload: { error },
        });
      });
  };
};

export const getStates = (payload) => {
  return (dispatch, getState) => {
    callApi(`/api/getStates/?country=${payload.country}`, "GET")
      .then((res) => res)
      .then((data) => {
        dispatch({
          type: GET_STATE_SUCCESS,
          payload: { data },
        });
      })
      .catch((error) => {
        dispatch({
          type: GET_STATE_FAILURE,
          payload: { error },
        });
      });
  };
};

export const updateBranch = (payload) => {
  let data = {
    name: payload.name,
    address: payload.address,
    city: payload.city,
    state: payload.state,
    country: payload.country,
    zipcode: payload.zipcode,
    admin_entries_id: payload.adminEntriesId
  };
  return (dispatch, getState) => {
    callApi(`/api/branchview/?branch_id=${payload.id}`, "PUT", data)
      .then((res) => res)
      .then((data) => {
        dispatch({
          type: UPADATE_BRANCH_SUCCESS,
          payload: { data },
        });
        dispatch(get_branches_list());
      })
      .catch((error) => {
        dispatch({
          type: UPADATE_BRANCH_FAILURE,
          payload: { error },
        });
      });
  };
};

export const deleteBranch = (payload) => {
  return (dispatch, getState) => {
    callApi(`/api/branchview/?branch_id=${payload.id}`, "DELETE")
      .then((res) => res)
      .then((data) => {
        dispatch({
          type: DELETE_BRANCH_SUCCESS,
          payload: { data },
        });
        dispatch(get_branches_list());
      })
      .catch((error) => {
        dispatch({
          type: DELETE_BRANCH_FAILURE,
          payload: { error },
        });
      });
  };
};

export const addBranch = (payload) => {
  let data = {
    name: payload.name,
    address: payload.address,
    city: payload.city,
    state: payload.state,
    country: payload.country,
    zipcode: payload.zipcode,
  };
  return (dispatch, getState) => {
    callApi(`/api/branchview/`, "POST", data)
      .then((res) => res)
      .then((data) => {
        dispatch({
          type: ADD_BRANCH_SUCCESS,
          payload: { data },
        });
      })
      .catch((error) => {
        dispatch({
          type: ADD_BRANCH_FAILURE,
          payload: { error },
        });
      });
  };
};

export const get_roleview_ = (data) => {
  return {
    type: GET_ROLEVIEW,
    payload: data,
  };
};

export const get_roleview = (id) => {
  return (dispatch) => {
    callApi(`/api/roleview/?role_id=${id}`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_roleview_(response));
      }
    });
  };
};

export const delete_role = (id) => {
  return (dispatch) => {
    callApi(`/api/roleview/?role_id=${id}`, "DELETE").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Role deleted successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        dispatch({
          type: CLEAR_ROLEVIEW_STORE_STATE,
        });
        return dispatch(get_roles_list());
      }
    });
  };
};

export const add_new_role_ = (data) => {
  return {
    type: GET_ROLEVIEW,
    payload: data,
  };
};

export const add_new_role = (data) => {
  return (dispatch) => {
    callApi(`/api/roleview/`, "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Role added successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_roles_list());
      }
    });
  };
};

export const add_user = (data) => {
  return (dispatch) => {
    callApi(`/api/peopleview/`, "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        if (Object.keys(response).length > 0) {
          let message = "";
          if (response.details && response.details == 1) {
            message = "Email already exists!";
          } else {
            message = "New user created successfully!";
          }
          toast(`${message}`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          return dispatch(get_people_list());
        }
      }
    });
  };
};

export const edit_user = (id, data) => {
  return (dispatch) => {
    callApi(`/api/peopleview/?csr_id=${id}&isprimary=true`, "PUT", data).then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          toast(`User details updated successfully!`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          return dispatch(get_people_list());
        }
      }
    );
  };
};

export const get_people_list_ = (data) => {
  return {
    type: GET_PEOPLE_LIST,
    payload: data,
  };
};

export const get_people_list = () => {
  return (dispatch) => {
    callApi(`/api/getallpeople/`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_people_list_(response));
      }
    });
  };
};

export const get_people_data_ = (data) => {
  return {
    type: GET_PEOPLE_DATA,
    payload: data,
  };
};

export const get_people_data = (id) => {
  return (dispatch) => {
    callApi(`/api/peopleview/?csr_id=${id}`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_people_data_(response));
      }
    });
  };
};

export const change_active_status = (id, type) => {
  return (dispatch) => {
    callApi(`/api/peopleview/?csr_id=${id}&type=${type}`, "DELETE").then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          let message = "";
          if (type === 1) {
            message = "Record Deleted";
          }
          else if (type === 2) {
            message = "User details disabled successfully";
          } else if (type === 3) {
            message = "User details enabled successfully";
          }
          toast(`${message}`, {
            autoClose: 2500,
            className: "black-background",
            bodyClassName: "red-hunt",
            progressClassName: "cc",
          });
          return dispatch(get_people_list());
        }
      }
    );
  };
};

export const search_people = (searchString) => {
  return (dispatch) => {
    callApi(`/api/searchpeople/?search_str=${searchString}`, "GET").then(
      (response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          return dispatch(get_people_list_(response));
        }
      }
    );
  };
};

export const edit_save_role = (id, data) => {
  return (dispatch) => {
    callApi(`/api/roleview/?role_id=${id}`, "PUT", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Role updated successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        dispatch({
          type: CLEAR_ROLEVIEW_STORE_STATE,
        });
        return dispatch(get_roles_list());
      }
    });
  };
};

export const get_settings_list_ = (data) => {
  return {
    type: GET_SETTINGS_LIST,
    payload: data,
  };
};

export const get_settings_list = () => {
  return (dispatch) => {
    callApi(`/api/getallsettings/`, "GET").then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_settings_list_(response.details));
      }
    });
  };
};

export const update_settings = (data) => {
  return (dispatch) => {
    callApi(`/api/peoplesettings/`, "POST", data).then((response) => {
      if (response && response.code !== 400 && response.code !== 500) {
        toast(`Settings updated successfully!`, {
          autoClose: 2500,
          className: "black-background",
          bodyClassName: "red-hunt",
          progressClassName: "cc",
        });
        return dispatch(get_settings_list());
      }
    });
  };
};
