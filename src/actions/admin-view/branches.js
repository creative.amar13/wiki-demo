import { callApi } from "../../utils/apiCaller";

export const ADD_BRANCH = "ADD_BRANCH";
export const FETCH_BRANCHES_SUCCESS = "FETCH_BRANCHES_SUCCESS";
export const FETCH_BRANCHES_FAILURE = "FETCH_BRANCHES_FAILURE";

export const addBranch = (branch) => ({
  type: ADD_BRANCH,
  payload: { branch },
});

export const fetchBranchesSuccess = (branches) => ({
  type: FETCH_BRANCHES_SUCCESS,
  payload: { branches },
});

export const fetchBranchesFailure = (error) => ({
  type: FETCH_BRANCHES_FAILURE,
  payload: { error },
});

export const fetchBranches = () => {
  return (dispatch, getState) => {
    callApi("/", "GET")
      .then((res) => res.data)
      .then((data) => {
        dispatch(fetchBranchesSuccess(data));
      })
      .catch((error) => {
        dispatch(fetchBranchesFailure(error));
      });
  };
};



