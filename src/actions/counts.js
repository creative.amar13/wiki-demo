import { callApi } from "../utils/apiCaller";
import { toast } from "react-toastify";

export const FEED = "FEED";
export const MY_POST = "MY_POST";
export const REVIEWS = "REVIEWS";
export const FEEDBACK = "FEEDBACK";
export const QA = "QA";
export const MESSAGES = "MESSAGES";


export const qa_count_dispatcher = (data) => ({ type: QA, payload: data });
export const get_qa_count = () => {
    return (dispatch) => {
        callApi(`/api/corporateqacount/`, "GET").then((response) => {
            if (response && response.code !== 400 && response.code !== 500) {
                return dispatch(qa_count_dispatcher(response));
            }
        });
    };
};

export const feedback_count_dispatcher = (data) => ({ type: FEEDBACK, payload: data });
export const get_feedback_count = () => {
    return (dispatch) => {
        callApi(`/api/corporatefeedbackcount/`, "GET").then((response) => {
            if (response && response.code !== 400 && response.code !== 500) {
                return dispatch(feedback_count_dispatcher(response));
            }
        });
    };
};

export const review_count_dispatcher = (data) => ({ type: REVIEWS, payload: data });
export const review_tab_count_ = (data) => ({ type: 'GET_CORPORATE_REVIEW_COUNT', payload: data });
export const get_review_count = () => {
    
    let AppBranchID = localStorage.getItem('branchID')
    let url = `/api/corporatereviewcount/`
    if (AppBranchID && AppBranchID !== null && AppBranchID !== 'null') {
        url = `/api/owner-review-count1/?id=${AppBranchID}`
    }
    return (dispatch) => {
        callApi(url, "GET").then((response) => {
            if (response && response.code !== 400 && response.code !== 500) {
                dispatch(review_tab_count_(response));
                return dispatch(review_count_dispatcher(response));
            }
        });
    };
};

export const message_count_dispatcher = (data) => ({ type: MESSAGES, payload: data });
export const get_message_count = () => {
    return (dispatch) => {
        callApi(`/api/corporatemessagecount/`, "GET").then((response) => {
            if (response && response.code !== 400 && response.code !== 500) {
                return dispatch(message_count_dispatcher(response));
            }
        });
    };
};
