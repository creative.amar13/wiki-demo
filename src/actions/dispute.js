import { callApi, callApiAdvance } from "../utils/apiCaller";
import querystring from "query-string";
import { toast } from "react-toastify";

export const ADD_DISPUTE_STATUS = "ADD_DISPUTE_STATUS";
export const GET_DISPUTE_DATA = "GET_DISPUTE_DATA";
export const DISPUTE_MODAL_STATUS = "DISPUTE_MODAL_STATUS";
export const ALL_DISPUTED_REVIEWS = "ALL_DISPUTED_REVIEWS";
export const GET_JURY_DUTY_DATA = "GET_JURY_DUTY_DATA";
export const GET_DISPUTE_DRAFT = "GET_DISPUTE_DRAFT";
export const DELETE_DISPUTE_DRAFT = "DELETE_DISPUTE_DRAFT";

export const add_dispute_action = (data) => {
    return {
      type: ADD_DISPUTE_STATUS,
      payload: data,
    };
};
  
export const add_dispute_review = (data, file='home') => {
    let formData = new FormData();    
    formData.append("reason", data.reason);
    formData.append("comment", data.comment);
    formData.append("review_id", data.review_id);
    formData.append("embedLinks", data.embedLinks);
    formData.append("is_draft", data.is_draft)
    formData.append("was_draft", data.was_draft)
    formData.append("review_owner", data.review_owner)
    formData.append("is_administrative_review", data.is_administrative_review)
    if( data.is_administrative_review){
      formData.append("admin_reason", data.admin_reason)
    }
    for (let i = 0; i < data.dispute_file.length; i++) {
    formData.append("dispute_file", data.dispute_file[i]);
    }
    
    return async (dispatch) => {
      callApi(`/api/disputes/`, "POST", formData, true).then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {
          if (response.Message) {
            toast(response.Message, {
              autoClose: 3000,
              className: "black-background",
              bodyClassName: "red-hunt",
              progressClassName: "cc",
            });
          }
          else {
            toast(`Review has been marked as disputed!`, {
              autoClose: 3000,
              className: "black-background",
              bodyClassName: "red-hunt",
              progressClassName: "cc",
            });
          }                     
          }
          return dispatch(add_dispute_action(response)); 
      });
    };    
};

export const get_dispute_action = (data) => {
    return {
        type: GET_DISPUTE_DATA,
        payload: data,
    };
};

export const get_dispute_discussion = (review_id) => {
    return (dispatch) => {
        callApi(`/api/disputes/?review_id=${review_id}`, "GET").then((response) => {
        if (response && response.code !== 400 && response.code !== 500) {
            return dispatch(get_dispute_action(response));
        }
        });
    };
};

export const add_dispute_reply = (data) => {
    let formData = new FormData();
    formData.append("review_id", data.review_id);
    formData.append("flag_content_id", data.flag_content_id);
    formData.append("flag_id", data.flag_id);
    formData.append("comment", data.comment);
    formData.append("embedLinks", data.embedLinks);

  
    for (let i = 0; i < data.dispute_reply_file.length; i++) {
      formData.append("dispute_reply_file", data.dispute_reply_file[i]);
    }
  
    return (dispatch) => {
      callApi(`/api/disputes-history/`, "POST", formData, true).then(
        (response) => {
          if (response && response.code !== 400 && response.code !== 500) {
            if (response.done) {
              toast("Your reply has been added Successfully!", {
                autoClose: 6500,
                className: "black-background",
                bodyClassName: "red-hunt",
                progressClassName: "cc",
              });
            }
          }
          return dispatch(get_dispute_discussion(data.review_id));
        }
      );
    };
  };

export const delete_dispute = (review_id) => {
    return (dispatch) => {
        callApi(`/api/disputes/?review_id=${review_id}`, "DELETE").then(
        (response) => {
            if (response && response.code !== 400 && response.code !== 500) {
            toast(`Dispute has been removed!`, {
                autoClose: 3000,
                className: "black-background",
                bodyClassName: "red-hunt",
                progressClassName: "cc",
            });
            }
        }
        );
    };
};

export const delete_dispute_reply = (pk, review_id) => {
    return (dispatch) => {
      callApi(`/api/disputes-history/?flag_content_id=${pk}`, "DELETE").then(
        (response) => {
          if (response && response.code !== 400 && response.code !== 500) {
            toast(`Reply has been removed!`, {
              autoClose: 3000,
              className: "black-background",
              bodyClassName: "red-hunt",
              progressClassName: "cc",
            });
          }
          return dispatch(get_dispute_discussion(review_id));
        }
      );
    };
  };

export const dispute_modal_status = (params) => {
  return {
    type: DISPUTE_MODAL_STATUS,
    payload: params,
  };
}

export const get_dispute_draft_action = (params) => {
  return {
    type: GET_DISPUTE_DRAFT,
    payload: params,
  };
}

export const get_dispute_draft = (review_id) => {
  return (dispatch) => {
    callApi(`/api/dispute-reviews/?draft=true&review=${review_id}`, "GET").then((response) => {
    if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(get_dispute_draft_action(response));
    }
    });
  };
}

export const delete_dispute_draft_action = (params) => {
  return {
    type: DELETE_DISPUTE_DRAFT,
    payload: params,
  };
}

export const delete_dispute_draft = (review_id) => {
  return (dispatch) => {
    callApi(`/api/dispute-reviews/?draft=true&review=${review_id}`, "DELETE").then((response) => {
    if (response && response.code !== 400 && response.code !== 500) {
        return dispatch(delete_dispute_draft_action(response));
    }
    });
  };
}




