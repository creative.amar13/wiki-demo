import { callApi } from "../utils/apiCaller";
import { toast } from "react-toastify";

export const UPDATE_BRANCH_FEED = "UPDATE_BRANCH_FEED";
export const UPDATE_BRANCH_MY_POST = "UPDATE_BRANCH_MY_POST";
export const UPDATE_BRANCH_REVIEWS = "UPDATE_BRANCH_REVIEWS";
export const UPDATE_BRANCH_FEEDBACK = "UPDATE_BRANCH_FEEDBACK";
export const UPDATE_BRANCH_QA = "UPDATE_BRANCH_QA";
export const UPDATE_BRANCH_MESSAGES = "UPDATE_BRANCH_MESSAGES";
/*

10 tabs 
10 tabs -- account DataCue


*/