import React, { Component } from 'react';
import { connect } from "react-redux";
import { BrowserRouter as Router, Switch, Route, withRouter, Link, Redirect } from 'react-router-dom';
import ReactJoyride, { CallBackProps, EVENTS, STATUS } from "react-joyride";
import { Button } from "reactstrap";
import Homepage from "./homepage";
import About from "./component/pages/about";
import Faq from "./component/pages/faq";
import Terms from "./component/pages/terms";
import Dashboard from "./component/pages/dashboard";
import Settings from "./component/pages/admin/settings";
import PublicRoute from "./component/PublicRoute";
import PrivateRoute from "./component/PrivateRoute";
import manifesto from './component/pages/manifesto';
import Press from './component/pages/press';
import PrivacyPolicy from './component/pages/privacy-policy';
import Guidelines from './component/pages/guidelines';
import { enableBranchForTour, enableRolesForTour, enableSettingsForTour } from "./actions/user";
import SignUp from './component/pages/sign-up';
import NoticeBoard from './component/pages/noticeBoard';
import UserInbox from './component/pages/inbox';
import AllNotifications from './component/pages/all-notifications';





const loaderExludedRoutes = ['/', '/about', '/faq', '/terms', '/menifesto', '/press', '/privacy-policy', '/guidelines'];

const preDefinedSteps = [
  {
    target: ".step-1",
    // title: `Hello ${this.props.profileData && this.props.profileData.user && this.props.profileData.user.first_name !== null ? this.props.profileData.user.first_name : 'xyz'} and welcome to WikiReviews!`,
    content:
      "This is a brief tour that will show you how to setup your corporate account. This includes how to set up users, roles and branches.  You will also learn how to fill out company information so it is properly displayed for each location.  Finally, you will learn how to create posts, engage with your customers and respond to and dispute reviews.",
    disableBeacon: true,
    disableOverlayClose: true,
    hideCloseButton: true,
    placement: 'center',
    spotlightClicks: true,
    styles: {
      options: {
        zIndex: 1000,
      },
    },
  },
  {
    target: ".step-2",
    title: 'Statistics',
    content:
      "You can easily see a dashboard summary of all the locations monthly statistics as well as the number of (put in red symbol) overdue, (put in yellow symbol) pending and (put in green symbol) completed tasks.",
    placement: "right",
    disableBeacon: true,
  },
  // {
  //   target: ".step-3",
  //   content:
  //     "Click on colored circles and tabs with numbers to filter the results.",
  //   placement: "top",
  // },
  // {
  //   target: ".step-4",
  //   content:
  //     "Modify the dashboard statistics by time frame by using the pulldowns here",
  //   placement: "left",
  // },
  // {
  //   target: ".step-5",
  //   content:
  //     "The listing owner guide is a quick reference guide to use at any time.  This is simply a template which can be used or it can be edited to suit your needs.",
  //   placement: "left",
  // },
  // {
  //   target: ".step-6",
  //   content:
  //     "The listing owner guide is a quick reference guide to use at any time.  This is simply a template which can be used or it can be edited to suit your needs.",
  //   placement: "left",
  // },
  // {
  //   target: ".step-7",
  //   content:
  //     "Search here for a specific branch by city, state or zip or by customer service representative name.  This search will filter results on the map as well as all the data below the map.",
  //   placement: "top",
  // },
  // {
  //   target: ".step-8",
  //   content:
  //     "The information shown here can be filtered by the search box above.",
  //   placement: "top",
  // },
  // {
  //   target: ".step-9",
  //   content:
  //     "The data shown here reflects the filters from the search box.",
  //   placement: "top",
  // },
  // {
  //   target: ".step-10",
  //   content:
  //     "Clicking on the settings icon takes you to a section where you can create and edit employees, branches and user roles.  This is the main administrator section to properly manage users, branches and user roles on the platform.",
  //   placement: "left",
  // },
  {
    target: ".step-11",
    content:
      "From here you can add or remove employees who will be responding to customer messages, reviews and inquiries.  Once an employee’s email is added, they will receive an emailed link to create their own password.  You will also assign each user to a specific role which gives users permissions to do various tasks.",
    placement: "top",
  },
  {
    target: ".step-12",
    content:
      "Here you can assign the branch location(s) to a specific employee and even assign backup branches to employees in case one is sick.",
    placement: "top",
  },
];

class RouteLinks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nextScreen: false,
      run: false,
      userFirstName: "",
      steps: preDefinedSteps,
    };
  }
  // componentDidMount() {
  //   console.log('componentDidMount');
  // }

  // componentWillUnmount() {
  //   console.log('componentUnMount');
  // }

  componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.activeTabBranch) {
      this.setState({ run: true });
    }
    if (nextProps && nextProps.activeRolesTab) {
      this.setState({ run: true });
    }
    if (nextProps && nextProps.activeSettingsTab) {
      this.setState({ run: true });
    }

  }

  handleClick = () => {
    // e.preventDefault();
    // console.log('Here !!!! in handle-click', this.state);
    this.setState({ run: true, nextScreen: false, steps: preDefinedSteps });
  };

  handleJoyrideCallback = (CallBackProps) => {
    let { status, type, step, index, action, lifecycle } = CallBackProps;
    const finishedStatuses = [STATUS.FINISHED, STATUS.SKIPPED];
    // console.log({ finishedStatuses, status }, '-finishedStatuses-');
    console.log(step.target, CallBackProps, 'CallBackProps');
    if (finishedStatuses.includes(status)) {
      // console.log('tour -- end == here !!!')
      if (step.target === '.step-11' || step.target === '.step-16') {
        this.setState({ run: false });
      }
    } else if ([EVENTS.TARGET_NOT_FOUND].includes(type)) {

      if (this.props.isAdmin) {
        if (step && step.target === '.step-11') {
          this.setState({
            run: false
          }, () => {
            localStorage.setItem('redirect', true);
            setTimeout(() => {
              // console.log(window.location.pathname, 'pathname');
              this.setState({
                steps: [
                  {
                    target: ".step-11",
                    content:
                      "From here you can add or remove employees who will be responding to customer messages, reviews and inquiries.  Once an employee’s email is added, they will receive an emailed link to create their own password.  You will also assign each user to a specific role which gives users permissions to do various tasks.",
                    placement: "right",
                    disableBeacon: true,
                    disableOverlayClose: true,
                    hideCloseButton: true,
                  },
                  {
                    target: ".step-12",
                    content:
                      "Here you can assign the branch location(s) to a specific employee and even assign backup branches to employees in case one is sick.",
                    placement: "top",
                  },
                  {
                    target: ".step-13",
                    content:
                      "You can easily edit employee information here such as the branches they are assigned to, backup branches and their email address.",
                    placement: "right",
                    styles: {
                      options: {
                        overlayColor: "rgba(0, 0, 0, 0.6)",
                        zIndex: 1000,
                      },
                    }
                  },
                  {
                    target: ".step-14",
                    content:
                      "You can edit and add new branch location data here. ",
                    placement: "top",
                    disableBeacon: true,
                    disableOverlayClose: true,
                    hideCloseButton: true,
                    styles: {
                      options: {
                        zIndex: 1000,
                      },
                    },
                  },
                  {
                    target: ".step-15",
                    content:
                      "Here you can create/remove/edit different roles and you can assign different permissions to each role.  This will ensure that employees assigned to a role are only able to do the tasks designated. ",
                    placement: "top",
                    disableBeacon: true,
                    disableOverlayClose: true,
                    hideCloseButton: true,
                    styles: {
                      options: {
                        overlayColor: "rgba(0, 0, 0, 0.6)",
                        zIndex: 1000,
                      },
                    }
                  },
                  {
                    target: ".step-16",
                    content:
                      "Here you set the criteria for what is overdue, pending or completed.  All red, yellow and green colored alerts are based upon the settings below.",
                    placement: "top",
                    disableBeacon: true,
                    disableOverlayClose: true,
                    hideCloseButton: true,
                  }
                ],
                run: true,
                nextScreen: true
              })
            }, 500);
          });
        }
      } else {
        this.setState({
          run: false
        })
      }
    }
  };

  renderReactJoyRide = ({ Tooltip }) => {
    return (
      <ReactJoyride
        callback={this.handleJoyrideCallback}
        steps={this.state.steps}
        run={this.state.run}
        continuous={true}
        tooltipComponent={Tooltip}
        showProgress
        hideBackButton={false}
        disableOverlayClose={true}
        locale={{ close: 'Close' }}
        styles={{
          options: {
            arrowColor: "#fff",
            backgroundColor: "#fff",
            overlayColor: "rgba(0, 0, 0, 0.6)",
            primaryColor: "mediumaquamarine",
            textColor: "#333",
            zIndex: 1000,
          },
        }}
      />
    )
  }

  handleStep_13 = () => {
    this.setState({ run: false }, () => {
      this.props.enableBranchForTour(true);
      // setTimeout(() => {
      //   this.setState({ run: true });
      // }, 1500);
    })
  }

  handleStep_14 = () => {
    this.setState({ run: false }, () => {
      this.props.enableRolesForTour(true);
      // setTimeout(() => {
      //   this.setState({ run: true });
      // }, 1500);
    })
  }

  handleStep_15 = () => {
    this.setState({ run: false }, () => {
      this.props.enableSettingsForTour(true);
      // setTimeout(() => {
      //   this.setState({ run: true });
      // }, 1500);
    })
  }


  handleStep_16 = () => {
    this.setState({ run: false, step: preDefinedSteps });
  }


  renderDefault = ({ tooltipProps, step, closeProps, primaryProps, checkStep }) => {
    console.log('Here !', step)
    return (
      <div
        className={
          checkStep === "14" || checkStep === "15" || checkStep === "16" ?
            "cs-toolpop setting-joyride" :
            "cs-toolpop"}
        {...tooltipProps}
      >
        {step.title && <h3>{step.title}</h3>}
        {step.content && <div>{step.content}</div>}
        <div className="d-flex mt-4 mb-3">
          {checkStep !== null ? (
            <Button {...primaryProps} color="primary"
              onClick={() => {
                if (checkStep == "13") {
                  this.handleStep_13();
                }

                if (checkStep == "14") {
                  this.handleStep_14();
                }

                if (checkStep == "15") {
                  this.handleStep_15();
                }

                if (checkStep == "16") {
                  this.handleStep_16();
                }
              }}>
              {'Ok'}
            </Button>

          ) : (
              <Button {...primaryProps} color="primary">
                {'Ok'}
              </Button>
            )}
          <Button {...closeProps} color="link" className="ml-auto"
            onClick={() => {
              this.setState({ run: false, nextScreen: false })
            }}>
            {'End Tour'}
          </Button>
        </div>
      </div>
    )

  }

  render() {
    // console.log(this.state, 'state---!11---')
    const Tooltip = ({
      continuous,
      index,
      isLastStep,
      step,
      backProps,
      closeProps,
      primaryProps,
      skipProps,
      tooltipProps, }) => {
      // console.log(step, '------ step------');
      if (step.target == '.step-3') {
        return (
          <div {...tooltipProps}>
            <div>
              <div className="stat-bubble with-line" data-type="overall">
                {` `}{` `}
              </div>
              <div className="stat-bubble with-line" data-type="reviews">
                {` `}{` `}
              </div>
              <div className="stat-bubble with-line" data-type="branches">
                {` `}{` `}
              </div>
            </div>
            <div className="intro-block" data-type="stat-intro">
              {step.content}
            </div>
            {continuous && (
              <Button {...primaryProps} color="primary" className="bt-btn">
                {'Got It'}
              </Button>
            )}
          </div>
        )
      } else if (step.target == '.step-1') {
        return (<div className="cs-toolpop" {...tooltipProps}>
          <h3>{`Hello ${this.state.userFirstName} and welcome to WikiReviews!`}</h3>
          <div>{'This is a brief tour that will show you how to setup your corporate account. This includes how to set up users, roles and branches.  You will also learn how to fill out company information so it is properly displayed for each location.  Finally, you will learn how to create posts, engage with your customers and respond to and dispute reviews.'}</div>
          <div className="d-flex mt-4 mb-3">
            <Button {...primaryProps} color="primary">
              {'Ok'}
            </Button>
            <Button {...closeProps} color="link" className="ml-auto" onClick={() => { this.setState({ run: false }) }}>
              {'End Tour'}
            </Button>
          </div>
          <Link to="/corporateprofile" onClick={() => { this.setState({ run: false }) }}>
            {'Skip this part and I’ll explore on my own'}
          </Link>
        </div>)
      } else if (step.target == '.step-10') {
        return (
          <div className="cs-toolpop" {...tooltipProps}>
            {step.title && <h3>{step.title}</h3>}
            {step.content && <div>{step.content}</div>}
            <div className="d-flex mt-4 mb-3">
              <Button {...primaryProps} color="primary">
                {/*// onClick={() => {
                //   this.setState({ run: false, nextScreen: false })
                // }}>*/}
                {'Ok'}
              </Button>
              <Button {...closeProps} color="link" className="ml-auto"
                onClick={() => {
                  this.setState({ run: false, nextScreen: false })
                }}>
                {'End Tour'}
              </Button>
            </div>
          </div>
        )
      } else {
        if (step.target == '.step-13') {
          let element = document.getElementById("react-joyride-step-2");
          if (element) {
            element.scrollIntoView();
            element.removeAttribute("id");

          }
          return this.renderDefault({ tooltipProps, step, closeProps, primaryProps, checkStep: "13" });
        }

        if (step.target == '.step-14') {
          return this.renderDefault({ tooltipProps, step, closeProps, primaryProps, checkStep: "14" });
        }

        if (step.target == '.step-15') {
          return this.renderDefault({ tooltipProps, step, closeProps, primaryProps, checkStep: "15" });
        }

        if (step.target == '.step-16') {
          return this.renderDefault({ tooltipProps, step, closeProps, primaryProps, checkStep: "16" });
        }

        return this.renderDefault({ tooltipProps, step, closeProps, primaryProps, checkStep: null });
      }
    }

    // let element = document.getElementById("react-joyride-step-2");
    // if (element) {
    //   element.removeAttribute("id");
    //   element.setAttribute("id", "step_2");
    //   element.scrollIntoView();
    // }

    return (
      <Router>
        {this.renderReactJoyRide({ Tooltip })}
        <Switch>
          <PublicRoute restricted={true} component={Homepage} path="/" exact />
          <PublicRoute restricted={false} component={About} path="/about" exact />
          <PublicRoute restricted={false} component={Faq} path="/faq" exact />
          <PublicRoute restricted={false} component={Terms} path="/terms" exact />
          <PublicRoute restricted={false} component={manifesto} path="/manifesto" exact />
          <PublicRoute restricted={false} component={Press} path="/press" exact />
          <PublicRoute restricted={false} component={PrivacyPolicy} path="/privacy-policy" exact />
          <PublicRoute restricted={false} component={Guidelines} path="/guidelines" exact />
          <PublicRoute restricted={false} component={SignUp} path="/sign-up" exact />
          <PrivateRoute clickTourStart={this.handleClick} component={UserInbox} path="/corporate_msg_inbox" exact />
          <PrivateRoute clickTourStart={this.handleClick} component={AllNotifications} path="/notifications" exact />
          <PrivateRoute clickTourStart={this.handleClick} component={Dashboard} path="/corporateprofile" exact />
          <PrivateRoute clickTourStart={this.handleClick} component={Settings} path="/corporatesettings" exact />
          <PrivateRoute component={NoticeBoard} path="/notice-board" exact />
        </Switch>
      </Router>
    );
  }
}

const mapState = (state) => ({
  activeTabBranch: state.user.active_branch_tab,
  activeRolesTab: state.user.active_roles_tab,
  activeSettingsTab: state.user.active_settings_tab,
  isAdmin: state.user.isAdmin,

})

const mapProps = (dispatch) => ({
  enableBranchForTour: (data) => dispatch(enableBranchForTour(data)),
  enableRolesForTour: (data) => dispatch(enableRolesForTour(data)),
  enableSettingsForTour: (data) => dispatch(enableSettingsForTour(data)),
})

export { loaderExludedRoutes }
export default withRouter(connect(mapState, mapProps)(RouteLinks));
