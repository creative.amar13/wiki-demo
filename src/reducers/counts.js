const initialState = {
    feed: {},
    mypost: {},
    review: {},
    message: {},
    feedback: {},
    qa: {},
}

const counts = (state = initialState, action) => {
    switch (action.type) {
        case "FEEDS":
            state.feed = action.payload;
            return { ...state };

        case "MY_POST":
            state.mypost = action.payload;
            return { ...state };

        case "REVIEWS":
            state.review = action.payload;
            return { ...state };

        case "MESSAGES":
            state.message = action.payload;
            return { ...state };

        case "FEEDBACK":
            state.feedback = action.payload;
            return { ...state };

        case "QA":
            state.qa = action.payload;
            return { ...state };

        default:
            return state;
    }
};

export default counts;
