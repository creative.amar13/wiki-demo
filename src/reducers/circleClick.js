const initialState = {
    topBarReview: false,
    topBarReviewColor: null,
    topBarDisputedReview: false,
    topBarDisputedReviewColor: null,
    topBarMessage: false,
    topBarMessageColor: null,
    topBarFeedback: false,
    topBarFeedbackColor: null,
    topBarQA: false,
    topBarQAColor: null,
    selected_Branches_Data: null

}

const circleClick = (state = initialState, action) => {
    // console.log({ action })
    switch (action.type) {
        case "INIT_TOPBAR":
            state = initialState;
            return { ...state };
        case "TOPBAR_REVIEWS":
            state.topBarReview = true;
            state.topBarReviewColor = action.payload;
            return { ...state };
        case "TOPBAR_DISPUTED_REVIEWS":
            state.topBarDisputedReview = true;
            state.topBarDisputedReviewColor = action.payload;
            return { ...state };
        case "TOPBAR_MESSAGE":
            state.topBarMessage = true;
            state.topBarMessageColor = action.payload;
            return { ...state };
        case "TOPBAR_QA":
            state.topBarQA = true;
            state.topBarQAColor = action.payload;
            return { ...state };
        case "TOPBAR_FEEDBACK":
            state.topBarFeedback = true;
            state.topBarFeedbackColor = action.payload;
            return { ...state };
        case "GET_CIRCLE_CLICK_BRANCHES":
            state.selected_Branches_Data = action.payload;
            return { ...state };
        default:
            return state;
    }
};

export default circleClick;
