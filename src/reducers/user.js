import { act } from "react-dom/test-utils";

const initialState = {
  branch_id: null,
  current_user: null,
  map_cordinates: null,
  corporate_id: null,
  corporate_id_details: null,
  corporate_copy_details: null,
  additional_data: null,
  updated_additional_data: null,
  chart_stats: null,
  admin_stats: null,
  branch_count: null,
  feed_list: null,
  employee_list: null,
  my_posts_list: null,
  reviews_list: null,
  share_with_list: null,
  album_types_list: null,
  album_type_data: null,
  new_name: null,
  business_update: null,
  messages_list: null,
  account_list: null,
  biz_callon_detail: null,
  corporate_call: null,
  all_branches: null,
  biz_callon_type: null,
  post_corporate_call: null,
  business_category: null,
  feedback_questions_answered: [],
  feedback_questions_pending: [],
  payment_update: null,
  business_sub_category: null,
  qa_questions_answered: [],
  qa_questions_pending: [],
  current_role_and_permissions: null,
  new_position: null,
  profession_type: [],
  current_role_and_permissions: null,
  isAdmin: null,
  search_branch_csr_data: [],
  search_branch_csr_data_loading: false,
  menuItems: null,
  top_bar_stats: null,
  filtered_stats_count: null,
  employeeDetail: null,
  corporate_review_count: null,
  branch_message_count: null,
  corporate_qa_count: null,
  statistics_owner_tab_count: null,
  getbranch_copy: null,
  getbranch_data: null,
  get_media_data: null,
  get_notifications_data: null,
  get_notification: null,
  post_notifications_data: null,
  update_pay_option: null,
  owner_message_count: null,
  menu_items_content: null,
  owner_qa_count: null,
  owner_feedback_count: null,
  active_branch_tab: null,
  active_roles_tab: null,
  active_settings_tab: null,
  feedback_list: null,
  qa_list: null,
  map_cordinates_file: null,
  get_all_disputes_data: null,
  get_dispute_data: null,
  sub_category_data: null,
  amenities_data: null,
  get_review_status: null,


};

const user = (state = initialState, action) => {
  switch (action.type) {
    case "MY_PROFILE":
      state.current_user = action.payload;
      return { ...state };
    case "BRANCH_ENTITY_ID":
      state.branch_id = action.payload;
      return { ...state };

    case "MAP_CORDINATES":
      state.map_cordinates = action.payload;
      return { ...state };
    case "MAP_CORDINATES_FILE":
      state.map_cordinates_file = action.payload;
      return { ...state };

    case "CORPORATE_ID":
      state.corporate_id = action.payload;
      return { ...state };

    case "CORPORATE_ID_DETAILS":
      state.corporate_id_details = action.payload;
      state.corporate_id_details["updated"] = Math.random();
      return { ...state };

    case "CORPORATE_COPY_DETAILS":
      state.corporate_copy_details = action.payload;
      state.corporate_copy_details["updated"] = Math.random();
      return { ...state };
    case "GET_SPECIFOC_BRANCH_CORPORATE_CALL_TO_ACTION":
      state.specific_branch_corporate_call_list = action.payload;
      return { ...state };

    case "ADD_ADDTIONAL_DATA":
      state.additional_data = action.payload;
      return { ...state };

    case "UPDATE_ADDITIONAL_INFO":
      state.updated_additional_data = "updated";
      return { ...state };

    case "DELETE_ADDITIONAL_INFO":
      state.delete_additional_data = "deleted";
      return { ...state };

    case "DELETE_MENU_ITEM":
      state.delete_menu_item = action.payload
      return { ...state };

    case "GET_CHART_STATS":
      state.chart_stats = action.payload;
      return { ...state };

    case "GET_ADMIN_STATS":
      state.admin_stats = action.payload;
      return { ...state };

    case "GET_BRANCH_COUNT":
      state.branch_count = action.payload;
      return { ...state };

    case "GET_FEED_LIST":
      state.feed_list = action.payload;
      return { ...state };

    case "GET_EMPLOYEE_LIST":
      state.employee_list = action.payload;
      return { ...state };

    case "GET_ACCOUNT_LIST":
      state.account_list = action.payload;
      return { ...state };

    case "GET_BIZ_CALLON_DETAIL_LIST":
      state.biz_callon_detail = action.payload;
      return { ...state };

    case "GET_CORPORATE_CALL_TO_ACTION":
      state.corporate_call = action.payload;
      return { ...state };

    case "GET_ALL_BRANCHES":
      state.all_branches = action.payload;
      return { ...state };

    case "GET_BIZ_CALLON_TYPE":
      state.biz_callon_type = action.payload;
      return { ...state };

    case "GET_MY_POSTS_LIST":
      state.my_posts_list = action.payload;
      return { ...state };

    case "GET_REVIEWS_LIST":
      state.reviews_list = action.payload;
      state.get_all_disputes_data = null;
      return { ...state };

    case "GET_CORPORATE_REVIEW_COUNT":
      state.corporate_review_count = action.payload;
      return { ...state };

    case "GET_OWNER_MESSAGE_COUNT":
      state.owner_message_count = action.payload;
      return { ...state };

    case "GET_OWNER_QA_COUNT":
      state.owner_qa_count = action.payload;
      return { ...state };

    case "GET_OWNER_FEEDBACK_COUNT":
      state.owner_feedback_count = action.payload;
      return { ...state };

    case "GET_BRANCH_MESSAGE_COUNT":
      state.branch_message_count = action.payload;
      return { ...state };

    case "GET_CORPORATE_QA_COUNT":
      state.corporate_qa_count = action.payload;
      return { ...state };

    case "GET_STATISTICS_OWNER":
      state.statistics_owner_tab_count = action.payload;
      return { ...state };

    case "GET_SHARE_WITH_LIST":
      state.share_with_list = action.payload;
      return { ...state };

    case "GET_ALBUM_TYPES_LIST":
      state.album_types_list = action.payload;
      return { ...state };

    case "GET_ALBUM_TYPE_DATA":
      state.album_type_data = action.payload;
      return { ...state };

    case "DELETE_CORPORATE_CALL":
      state.delete_corporate_call = action.payload;
      return { ...state };

    case "POST_CORPORATE_CALL":
      state.post_corporate_call = action.payload;
      return { ...state };

    case "ADD_NAME":
      state.new_name = action.payload;
      return { ...state };

    case "BUSINESS_UPDATE":
      state.business_update = action.payload;

    case "GET_MESSAGES_LIST":
      state.messages_list = action.payload;
      return { ...state };

    case "BUSINESS_CATEGORIES":
      state.business_category = action.payload;
      return { ...state };
    case "FEEDBACK_QUESTIONS_PENDING":
      state.feedback_questions_pending = action.payload;
      return { ...state };
    case "FEEDBACK_QUESTIONS_ANSWERED":
      state.feedback_questions_answered = action.payload;
      return { ...state };
    case "QA_QUESTIONS_PENDING":
      state.qa_questions_pending = action.payload;
      return { ...state };
    case "QA_QUESTIONS_ANSWERED":
      state.qa_questions_answered = action.payload;
      return { ...state };

    case "BUSINESS_SUB_CATEGORIES":
      state.business_sub_category = action.payload;
      return { ...state };

    case "USER_ROLE_AND_PERMISSION":
      if (action.payload.role == "") {
        state.isAdmin = true;
      } else {
        state.current_role_and_permissions = action.payload;
        state.isAdmin = false;
      }
      return { ...state };

    case "ADD_POSITION":
      state.new_position = action.payload;
      return { ...state };

    case "GET_PROFESSION_TYPE":
      state.profession_type = action.payload;
      return { ...state };

    case "GET_SEARCH_BRANCH_CSR_LOADING":
      state.search_branch_csr_data_loading = true;
      return { ...state };
    case "GET_SEARCH_BRANCH_CSR":
      state.search_branch_csr_data = action.payload;
      state.search_branch_csr_data_loading = false;
      return { ...state };
    case "GET_SEARCH_BRANCH_CSR_ERROR":
      state.search_branch_csr_data_loading = false;
      return { ...state };

    case "MENU_ITEM_LIST":
      state.menuItems = action.payload;
      return { ...state };

    case "GET_EMPLOYEE_DETAIL":
      state.employeeDetail = action.payload;
      return { ...state };

    case "TOP_BAR_COUNTS":
      state.top_bar_stats = action.payload;
      return { ...state };

    case "FILTERED_COUNTS":
      state.filtered_stats_count = action.payload;
      return { ...state };

    case "GET_BRANCH_COPY":
      state.getbranch_copy = action.payload;
      return { ...state };

    case "GET_BRANCH_DATA":
      state.getbranch_data = action.payload;
      return { ...state };

    case "GET_MEDIA_DATA":
      state.get_media_data = action.payload;
      return { ...state };

    case "GET_NOTIFICATIONS_DATA":
      state.get_notifications_data = action.payload;
      return { ...state };

    case "POST_NOTIFICATIONS_DATA":
      state.post_notifications_data = action.payload;
      return { ...state };

    case "FETCH_NOTIFICATION_DATA":
      state.get_notification = action.payload;
      return { ...state };

    case "UPDATE_PAYMENT_OPTION":
      state.update_pay_option = action.payload;
      return { ...state };

    case "MENU_ITEM_CONTENT":
      state.menu_items_content = action.payload;
      return { ...state };

    case 'ACTIVE_TAB_BRANCH':
      state.active_branch_tab = action.payload;
      return { ...state };

    case 'ACTIVE_TAB_ROLES':
      state.active_branch_tab = false;
      state.active_roles_tab = action.payload;
      return { ...state };

    case 'ACTIVE_TAB_SETTINGS':
      state.active_branch_tab = false;
      state.active_roles_tab = false;
      state.active_settings_tab = action.payload;

    case "GET_FEEDBACK_LIST":
      state.feedback_list = action.payload;
      return { ...state };

    case "GET_QA_LIST":
      state.qa_list = action.payload;
      return { ...state };

    case "GET_ALL_DISPUTES_DATA":
      state.get_all_disputes_data = action.payload;
      state.reviews_list = null;
      return { ...state };

    case "GET_DISPUTE_DATA":
      state.get_dispute_data = action.payload;
      return { ...state };

    case "GET_SUB_CATEGORIES_DATA":
      state.sub_category_data = action.payload;
      return { ...state };

    case "GET_AMENITIES_DATA":
      state.amenities_data = action.payload;
      return { ...state };

    case "REVIEW_STATUS":
      state.get_review_status = action.payload;
      return { ...state };


    default:
      return state;
  }
};

export default user;
