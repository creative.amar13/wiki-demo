const initialState = {
    auth_login: localStorage.getItem('token') ? localStorage.getItem('token') : null,
    isLoggedIn: localStorage.getItem('token') !== null ? true : false,
    branchID: localStorage.setItem('branchID', null),
    auth_error: null,
    signUp: null,
    signUp_error: null
}

const auth = (state = initialState, action) => {
    switch (action.type) {

        case 'SET_INITIAL_AUTH_NEW':
            return {
                auth_login: null,
                isLoggedIn: false,
                branchID: null,
                auth_error: null,
                signUp: null,
                signUp_error: null
            }

        case "AUTH_LOGIN":
            state.auth_login = action.payload;
            return { ...state }

        case "AUTH_ERROR":
            state.auth_login = null;
            state.auth_error = action.payload;
            state.isLoggedIn = false;
            return { ...state }

        case "IS_LOGIN_TRUE":
            state.isLoggedIn = true;
            return { ...state }

        case "IS_SIGNUP_SUCCESS":
            state.signUp = action.payload;
            return { ...state }

        case "IS_SIGNUP_ERROR":
            state.signUp = null;
            state.signUp_error = action.payload;
            return { ...state }


        default:
            return state
    }
}

export default auth;
