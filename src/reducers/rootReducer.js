import { combineReducers } from "redux";
import auth from "./auth";
import user from "./user";
import admin from "./admin-view/admin";
import count from "./counts";
import circleClick from "./circleClick";
import { roles as adminViewRoles } from "./admin-view/roles";
import { users as adminViewUsers } from "./admin-view/users";
import { branches as adminViewBranches } from "./admin-view/branches";
import dispute from "./dispute";

export default combineReducers({
  auth,
  user,
  admin,
  count,
  circleClick,
  adminViewRoles,
  adminViewUsers,
  adminViewBranches,
  dispute

});
