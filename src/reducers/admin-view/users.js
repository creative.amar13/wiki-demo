import * as actions from "../../actions/admin-view/users";

const initialState = {
  items: [],
  error: null,
};

export const users = (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCH_USERS_SUCCESS:
      return {
        ...state,
        items: action.payload.users,
        error: null,
      };
    case actions.FETCH_USERS_FAILURE:
      return {
        ...state,
        items: [],
        error: action.payload.error,
      };
    default:
      return state;
  }
};
