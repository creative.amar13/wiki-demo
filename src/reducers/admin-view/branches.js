import * as actions from "../../actions/admin-view/branches";

const initialState = {
  items: [],
  error: null,
};

export const branches = (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCH_BRANCHES_SUCCESS:
      return {
        ...state,
        items: action.payload.branches,
        error: null,
      };
    case actions.FETCH_BRANCHES_FAILURE:
      return {
        ...state,
        items: [],
        error: action.payload.error,
      };
    default:
      return state;
  }
};
