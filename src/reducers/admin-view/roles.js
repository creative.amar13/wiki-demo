import * as actions from "../../actions/admin-view/roles";

const initialState = {
  items: [],
  error: null,
};

export const roles = (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCH_ROLES_SUCCESS:
      return {
        ...state,
        items: action.payload.roles,
        error: null,
      };
    case actions.FETCH_ROLES_FAILURE:
      return {
        ...state,
        items: [],
        error: action.payload.error,
      };
    default:
      return state;
  }
};
