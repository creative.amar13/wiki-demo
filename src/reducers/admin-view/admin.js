import * as actions from "../../actions/admin-view/admin";
const initialState = {
  people_list: null,
  people_data: null,
  countries_list: [],
  states_list: [],
  roles_list: [],
  branches_list: [],
  settings_list: [],
  roleview: null,
  TYPE: "",
};

const admin = (state = initialState, action) => {
  switch (action.type) {
    case "GET_SETTINGS_LIST":
      state.settings_list = action.payload;
      return { ...state };

    case "GET_ROLES_LIST":
      state.roles_list = action.payload;
      return { ...state };
    case "GET_ROLEVIEW":
      state.roleview = action.payload;
      return { ...state };
    case "CLEAR_ROLEVIEW_STORE_STATE":
      state.roleview = null;
      return { ...state };
    case "GET_BRANCH_LIST":
      state.branches_list = action.payload;
      return { ...state };


    case actions.FETCH_BRANCHES_SUCCESS:
      return {
        ...state,
        items: action.payload.branches,
        TYPE: "ADMIN_ALL_BRANCHES",
        error: null,
      };
    case actions.FETCH_BRANCHES_FAILURE:
      return {
        ...state,
        items: [],
        error: action.payload.error,
      };

    case actions.GET_BRANCH_SUCCESS:
      return {
        ...state,
        items: action.payload,
        TYPE: "ADMIN_GET_BRANCH",
        error: null,
      };
    case actions.GET_BRANCH_FAILURE:
      return {
        ...state,
        items: [],
        error: action.payload.error,
      };
    case actions.GET_STATE_SUCCESS:
      return {
        ...state,
        items: action.payload,
        TYPE: "ADMIN_GET_STATE",
        error: null,
      };
    case actions.GET_STATE_FAILURE:
      return {
        ...state,
        items: [],
        error: action.payload.error,
      };

    case actions.UPADATE_BRANCH_SUCCESS:
      return {
        ...state,
        items: action.payload,
        TYPE: "ADMIN_UPDATE_STATE",
        error: null,
      };
    case actions.UPADATE_BRANCH_FAILURE:
      return {
        ...state,
        items: [],
        error: action.payload.error,
      };
    case actions.DELETE_BRANCH_SUCCESS:
      return {
        ...state,
        items: action.payload,
        TYPE: "ADMIN_DELETE_STATE",
        error: null,
      };
    case actions.DELETE_BRANCH_FAILURE:
      return {
        ...state,
        items: [],
        error: action.payload.error,
      };

    case actions.ADD_BRANCH_SUCCESS:
      return {
        ...state,
        items: action.payload,
        TYPE: "ADMIN_ADD_BRANCH",
        error: null,
      };
    case actions.ADD_BRANCH_FAILURE:
      return {
        ...state,
        items: [],
        error: action.payload.error,
      };

    case "GET_BRANCHES_LIST":
      state.branches_list = action.payload;
      return { ...state };

    case "GET_PEOPLE_LIST":
      state.people_list = action.payload;
      return { ...state };

    case "GET_PEOPLE_DATA":
      state.people_data = action.payload;
      return { ...state };

    case "GET_COUNTRIES_LIST":
      state.countries_list = action.payload;
      return { ...state };

    case "GET_STATES_LIST":
      state.states_list = action.payload;
      return { ...state };

    default:
      return state;
  }
};

export default admin;
