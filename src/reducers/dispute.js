import * as actions from "../actions/dispute";

const initialState = {
  add_dispute_status: null,
  get_dispute_data: null,
  get_dispute_modal_status: null,
  get_dispute_draft_data: null,
  delete_dispute_draft_data: null,
};


const dispute = (state = initialState, action) => {
  switch (action.type) {

    case "ADD_DISPUTE_STATUS":
      state.add_dispute_status = action.payload;
      return { ...state };
    
    case "GET_DISPUTE_DATA":
        state.get_dispute_data = action.payload;
        return { ...state };

    case "DISPUTE_MODAL_STATUS":
        state.get_dispute_modal_status = action.payload;
        return { ...state };

    case "GET_DISPUTE_DRAFT":
      state.get_dispute_draft_data = action.payload;
      return { ...state };

    case "DELETE_DISPUTE_DRAFT":
      state.delete_dispute_draft_data = action.payload;
      return { ...state };

    default:
      return state;
  }
};

export default dispute;
