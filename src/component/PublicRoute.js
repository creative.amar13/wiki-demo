import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from "react-redux";

class PublicRoute extends Component {

    isLoggedIn = () => {
        return this.props.isLoggedIn === true ? true : false
    }

    render() {
        const { component: Component, restricted, ...rest } = this.props;
        let isRestrict = restricted;
        let isLoggedIn = this.isLoggedIn()
        return (
            <Route {...rest} render={props =>
                (isLoggedIn && isRestrict ?
                    <Redirect to="/corporateprofile" />
                    : <Component {...props} />)} />
        )
    }
}

const mapState = (state) => {
    return {
        authStates: state.auth,
        isLoggedIn: state.auth.isLoggedIn
    }
}

export default connect(mapState)(PublicRoute);
