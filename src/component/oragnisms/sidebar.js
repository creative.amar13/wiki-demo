import React, { Component } from 'react';

import BranchCSR from "./../molecules/branchCSR";
import BusinessInfo from "./../molecules/businessInfo";
import MenuItems from "./../molecules/menuItems";
import AdditionalInfo from "./../molecules/additionalInfo";
import AboutBusiness from "./../molecules/aboutBusiness";
import Specialities from "./../molecules/specailOffers";
class SideBar extends Component {

    render() {

        return (

            <React.Fragment>
                <BranchCSR />
                <BusinessInfo/>
                <MenuItems />
                <AdditionalInfo />
                <AboutBusiness />
                <Specialities />
            </React.Fragment>
        )
    }
}
export default SideBar;