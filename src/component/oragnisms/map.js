import React, { Component } from "react";
import {
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  Button,
  Container,
  Row,
  Col,
  CardBody,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Collapse,
  Card,
  Badge,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import mapboxgl from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import moment from "moment";
import {
  current_user_profile,
  get_search_branch_csr,
  clear_search_branch_csr,
  get_map_cordinates,
  get_map_cordinates_file,
  get_review_data_by_circle_click,
  get_branch_data_by_circle_click,
  // get_statistics_owner,
  // get_branch_message_count,
  // get_corporate_qa_count,
  update_on_list_view_branch,
  set_branch_ID_local_storage,
  get_branch_message_count_,
  get_reviews_list_entity_id,
  get_data_by_role,
  get_data_by_color,
} from "../../actions/user";
import { callApi } from "../../utils/anotherCaller";

mapboxgl.accessToken = `pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw`;
let count = 0;
class Map extends Component {
  mapRef = React.createRef();

  constructor(props) {
    super(props);
    this.clearSearch = false;
    this.myScrollRef = React.createRef();
    this.state = {
      brnachesStats: null,
      onColorClick: false,
      keyMAP: Math.random(),
      circleClickfilter: false,
      circleClickfilteredData: [],
      isToggleList: false,
      isMapListCollapseOpen: true,
      activeTab: "1",
      viewDays: 30,
      statView: "30_days",
      ViewReviewsType: "new",
      ViewMessagesType: "call_to_action",
      ViewFeedbackType: "answered",
      ViewQAType: "answered",
      lng: -129.891,
      lat: 35.7608,
      zoom: 3.6,
      viewEmpType: "current_emp",
      addmodal: false,
      isOpen: false,
      map_box_item: [],
      map_cordinates: null,
      fullName: null,
      currentRolePermission: "",
      locationSearch: "",
      userRole: "nonadmin",
      searchBranchCsrData: [],
      topBarStats: null,
      hasMore: true,
      customListArray: [],
      customList: [],
      branchID: '',
      branchIDAvgRating: "",
      branchIDStatOverAll: null,
      branchIDName: null,
      branchIDFirstName: null,
      branchIDZipcode: null,
      branchIDType: "",
      allBranches: '',
      selectedRoleData: null,
      selectedRoleName: '',
      selectedRoleCount: null,
    };
  }

  async componentWillReceiveProps(nextProps) {
    let { locationSearch, searchBranchCsrData } = this.state;
    // if (nextProps.branchID) {
    //   await this.setState({
    //     branchID: nextProps.branchID
    //   }, () => {
    //     this.fetchDataOnSearchClick()
    //   });
    // }
    if (nextProps.isAdmin) {
      this.setState({ userRole: "admin" });
    }
    if (this.state.branchID === '' && this.state.branchIDType === "") {
      if (nextProps.all_branches && nextProps.all_branches.results && Object.keys(nextProps.all_branches.results).length > 0) {
        this.setState({ allBranches: nextProps.all_branches.results }, () => this.firstFetchArray())
      }
    }

    if (nextProps.top_bar_stats) {
      let data = nextProps.top_bar_stats;
      if (data && data.stats) {
        this.setState({ topBarStats: data.stats });
      }
    }
    if (nextProps.admin_stats) {
      let items = nextProps.admin_stats;
      let dataItems = {
        overall: items['overall'],
      };

      this.setState({
        brnachesStats: dataItems,
      });
    }
    if (
      nextProps.search_branch_csr_data &&
      nextProps.search_branch_csr_data.length > 0 &&
      nextProps.search_branch_csr_data.data === locationSearch
    ) {
      let nextPropsSearchBranchItems = nextProps.search_branch_csr_data;
      if (nextPropsSearchBranchItems && Array.isArray(nextPropsSearchBranchItems) && nextPropsSearchBranchItems.length) {
        let findItem = nextPropsSearchBranchItems.find(item => item.zipcode == locationSearch);
        let data_set = [];
        let nextPropsSearchBranchItemsByCount = nextPropsSearchBranchItems.slice(0);
        nextPropsSearchBranchItemsByCount.sort(function (a, b) {
          return a.distance - b.distance;
        });

        nextPropsSearchBranchItemsByCount.code = nextPropsSearchBranchItems.code;
        nextPropsSearchBranchItemsByCount.data = nextPropsSearchBranchItems.data;

        this.setState({
          searchBranchCsrData: nextPropsSearchBranchItemsByCount,
        });
      }
    } else if (
      nextProps.search_branch_csr_data &&
      nextProps.search_branch_csr_data.length === 0 &&
      nextProps.search_branch_csr_data.data === locationSearch
    ) {
      this.setState({
        searchBranchCsrData: [],
      });
    } else {
      if (nextProps?.search_branch_csr_data?.length == 1 && nextProps?.search_branch_csr_data[0].set_item) {
        this.setState({
          searchBranchCsrData: nextProps.search_branch_csr_data,
        });
      }
    }

    if (this.state.userRole === 'admin') {
      if (nextProps.map_cordinates_file &&
        nextProps.map_cordinates_file.data &&
        this.state.onColorClick === false) {
        let map_cordinates = nextProps.map_cordinates_file;
        let map_box_item = [];
        map_cordinates.data.forEach((dt) => {
          map_box_item.push({
            data: dt,
            entries_id: dt.entries_id,
            type: dt.name,
            geometry: {
              type: "Point",
              coordinates: [dt.longitude, dt.latitude],
            },
            // branch_counts: dt.stats_data && dt.stats_data.stats && dt.stats_data.stats.overall,
            branch_counts: dt.stats_data && dt.stats_data.stats && dt.stats_data.stats.overall,
            properties: {
              title: dt.name,
              address1: `${dt.address1},`,
              country: `${dt.country}`,
              description: `${dt?.city ? dt.city + ',' : ""} ${dt?.state ? dt.state + ',' : ""} ${dt?.zipcode ? dt.zipcode : ""}`
            },
          });
        });

        this.setState(
          {
            // customListArray: nextProps.map_cordinates.data.slice(0, 1),
            map_cordinates: this.state.userRole === 'admin' ? nextProps.map_cordinates_file : nextProps.map_cordinates,
            lng: map_cordinates.max_long,
            lat: map_cordinates.max_lat,
            zoom: map_box_item.length > 15 ? 3 : map_cordinates.zoomsize,
            map_box_item,
          },
          () => {
            this.mapCordinatesSet();
          }
        );
      }

    } else {

      if (nextProps.map_cordinates && nextProps.map_cordinates.data) {
        let map_cordinates = nextProps.map_cordinates;

        let map_box_item = [];
        map_cordinates.data.forEach((dt) => {
          map_box_item.push({
            data: dt,
            entries_id: dt.entries_id,
            type: dt.name,
            geometry: {
              type: "Point",
              coordinates: [dt.longitude, dt.latitude],
            },
            branch_counts: dt.stats_data && dt.stats_data.stats && dt.stats_data.stats.overall,
            properties: {
              title: dt.name,
              address1: `${dt.address1},`,
              country: `${dt.country}`,
              description: `${dt.city}, ${dt.state} ${dt.zipcode},`,
            },
          });
        });


        this.setState(
          {
            // customListArray: nextProps.map_cordinates.data.slice(0, 1),
            map_cordinates: this.state.userRole === 'admin' ? nextProps.map_cordinates_file : nextProps.map_cordinates,
            lng: map_cordinates.max_long,
            lat: map_cordinates.max_lat,
            zoom: map_cordinates.zoomsize,
            map_box_item,
          },
          () => {
            this.mapCordinatesSet();
          }
        );
      }
    }

    if (nextProps.profileData) {
      let profileData = nextProps.profileData;
      // let userData = profileData.user;

      this.setState({
        fullName:
          (profileData.user && profileData.user.first_name) +
          " " +
          (profileData.user && profileData.user.last_name),
      });
    }
  }


  componentDidUpdate(prevProps, prevState) {
    // Typical usage (don't forget to compare props):
    if (this.props.filtered_stats_count !== prevProps.filtered_stats_count) {
      this.setState({
        viewDays: this.props.filtered_stats_count,
      })
    }
    if (this.props.selectedBranchesData !== prevProps.selectedBranchesData) {
      const { map_cordinates, lng, lat, zoom, map_box_item, } = this.state

      let filteredValues = [];
      let new_map_box_item = [];
      let list_view_data = [];
      this.props && this.props.selectedBranchesData && Array.isArray(this.props.selectedBranchesData) && this.props.selectedBranchesData.map((element1, index) => {
        map_cordinates && Array.isArray(map_cordinates.data) && map_cordinates.data.filter((element2, index) => {
          if (element1.entries_id === element2.entries_id) {
            element2.color = element1?.color;
            element2.countDays = element1?.countDays;
            element2.count = element1?.count;
            element2.type = element1?.type;
            element2.stats_data = element1?.stats_data;
            filteredValues.push(element2);
          }
        });
      });
      filteredValues.forEach((dt) => {
        new_map_box_item.push({
          data: dt,
          entries_id: dt.entries_id,
          type: dt.name,
          geometry: {
            type: "Point",
            coordinates: [dt.longitude, dt.latitude],
          },
          // branch_counts: dt.stats_data && dt.stats_data.stats && dt.stats_data.stats.overall,
          branch_counts: dt.stats_data && dt.stats_data.stats && dt.stats_data.stats.overall,
          properties: {
            title: dt.name,
            address1: `${dt.address1},`,
            country: `${dt.country}`,
            description: `${dt?.city ? dt.city + ',' : ""} ${dt?.state ? dt.state + ',' : ""} ${dt?.zipcode ? dt.zipcode : ""}`
          },
        });
        list_view_data.push({
          count: dt.count,
          countDays: dt.countDays,
          color: dt.color,
          type: dt.type,
          stats: dt.stats_data.stats,
          details: dt.stats_data.details,
          ratings: dt.stats_data.ratings,
          address: {
            address1: dt.address1,
            address2: dt.address2,
            city: dt.city,
            country: dt.country,
            id: dt.entries_id,
            latitude: dt.latitude,
            longitude: dt.longitude,
            listing_name: dt.name,
            profile_pic: dt.profile_pic,
            state: dt.state,
            zipcode: dt.zipcode,
          },
          branch: {
            address1: dt.address1,
            address2: dt.address2,
            city: dt.city,
            country: dt.country,
            id: dt.entries_id,
            latitude: dt.latitude,
            longitude: dt.longitude,
            listing_name: dt.name,
            profile_pic: dt.profile_pic,
            state: dt.state,
            zipcode: dt.zipcode,
          }
        })

      });

      let byCount = list_view_data.slice(0);
      byCount.sort(function (a, b) {
        return b.count - a.count;
      });



      this.setState(prevState => ({
        map_cordinates: {
          ...prevState.map_cordinates,
          data: filteredValues,
        },
        map_box_item: new_map_box_item,
        keyMAP: Math.random(),
        // customListArray: list_view_data,
        circleClickfilteredData: byCount,
        circleClickfilter: true,
      }),

        () => {
          this.mapCordinatesSet();
          this.updateOnTitle(this.state.circleClickfilteredData[0]);
        })
    }
  }

  handleSearchDropDown = async (id) => {
    const {
      set_branch_ID_local_storage
    } = this.props;
    set_branch_ID_local_storage(id);
    this.fetchDataOnSearchClick();
  };

  fetchDataOnSearchClick = async () => {
    let { branchID, branchIDType } = this.state;
    // let data = branchID;
    try {
      let result = await callApi(
        // `/api/singlebranchcsr/?num=${branchIDType === 'branch' ? 6949590 : branchID}&type=${branchIDType}&utype=admin`,
        `/api/singlebranchcsr/?num=${branchID}&type=${branchIDType}&utype=admin`,
        "GET"
      );

      result["branch"] = branchID;
      await this.setState({
        customList: [result],
        branchIDName: branchIDType === 'branch' ? result?.address?.listing_name : `${result && result.csr_details[0] && result.csr_details[0].first_name} ${result && result.csr_details[0] && result.csr_details[0].last_name}`,
        branchIDZipcode: branchIDType === 'branch' ? result.address.zipcode : "",
        branchIDFirstName: branchIDType === 'branch' ? "" : `${result && result.csr_details[0].first_name}`,
        branchIDAvgRating: result.average_rating,
        branchIDStatOverAll: result.stats.overall
      });
    } catch (e) {
      console.log(e);
    }

  };

  firstFetchArray = async () => {
    let { allBranches, circleClickfilter } = this.state;
    let data = allBranches.result[0];

    if (data && data.id && circleClickfilter === false) {
      try {
        let result = await callApi(
          `/api/scrollbranch/?count=${data.id}&utype=admin`,
          "GET"
        );
        if (result?.code === 200) {
          result["branch"] = data;
          this.setState({ customListArray: [result] });
        }
      } catch (e) {
        console.log(e);
      }
    }
  };

  FetchArrayListClick = async (SelectedID) => {
    let data = SelectedID;

    if (data) {
      try {
        let result = await callApi(
          `/api/scrollbranch/?count=${data}&utype=admin`,
          "GET"
        );
        if (result.code === 200) {
          result["branch"] = data;
          this.setState({
            customListArray: [result],
            customList: [result]
          });
        }
      } catch (e) {
        console.log(e);
      }
    }
  };
  toggleMapListCollapse = () =>
    this.setState(
      { isMapListCollapseOpen: !this.state.isMapListCollapseOpen },
      () => {
        this.forceUpdate();
      }
    );

  componentDidMount() {
    this.setInitialAtFirst();
  }

  setInitialAtFirst = () => {
    this.mapCordinatesSet();
    // this.props.get_map_cordinates();
    this.props.get_map_cordinates_file();
    // this.props.current_user_profile();
  }

  mapCordinatesSet = () => {
    const { lng, lat, zoom, map_box_item, circleClickfilter } = this.state;
    let currentState = this.state;
    const {
      isAdmin,
      set_branch_ID_local_storage,
      handleMapMarkerClick,
    } = this.props;
    const map = new mapboxgl.Map({
      container: this.mapRef.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [lng, lat],
      zoom,
    });
    map.addControl(new mapboxgl.NavigationControl());
    let filteredValues = [];
    this.state && this.state.searchBranchCsrData && this.state.searchBranchCsrData.map((element1, index) => {
      map_box_item.filter((element2, index) => {
        if (element1.id === element2.entries_id) {
          filteredValues.push(element2);
        }
        // return element1.id === element2.entries_id;
      });
    });

    if (this.state.searchBranchCsrData.length > 0) {
      let elementSet = filteredValues[0]?.geometry?.coordinates;
      filteredValues.forEach(function (marker) {
        const { branch_counts } = marker;
        let rating_image = (marker.data.stats_data && marker.data.stats_data.ratings && marker.data.stats_data.ratings[0] && marker.data.stats_data.ratings[0].rating);
        rating_image = rating_image ? rating_image : 'norating.png'
        let reviews_count = marker.data.stats_data && marker.data.stats_data.stats.review_count.count
        let disputed_count = marker.data.stats_data && marker.data.stats_data.stats.dispute_review.count
        let message_count = marker.data.stats_data && marker.data.stats_data.stats.message.count
        let feedback_count = marker.data.stats_data && marker.data.stats_data.stats.feedback.count
        let qa_count = marker.data.stats_data && marker.data.stats_data.stats.qa.count
        let overall_count = marker.data.stats_data && marker.data.stats_data.stats.overall
        let barclass = '';
        barclass = overall_count && overall_count.red > 0 ? 'type-red' : overall_count && overall_count.orange > 0 ? 'type-orange' : 'type-green'
        // create a HTML element for each feature
        var el = document.createElement("div");
        // el.className = "marker";
        el.className = branch_counts && branch_counts.red > 0 ?
          'marker-red' : branch_counts && branch_counts.orange > 0 ?
            'marker-orange' : branch_counts && branch_counts.green > 0 ?
              'marker-green' : 'marker-green';

        // make a marker for each feature and add to the map
        new mapboxgl.Marker(el)
          .setLngLat(marker.geometry.coordinates)
          .addTo(map);
        const markerItems = new mapboxgl.Marker(el)
          .setLngLat(marker.geometry.coordinates)
          .setPopup(
            new mapboxgl.Popup({ offset: 25 }) // add popups
              .setHTML(
                // '<span class="text-primary-dark fs-12 font-weight-bold">Bridget Bettany</span>' +
                '<h3 class="text-secondary">' +
                marker.properties.title +
                '</h3>' +
                '<div class="fs-12 text-secondary-dark ff-base">' +
                '<span>' +
                marker.properties.address1 +
                '</span><span>' +
                marker.properties.description +
                '</span><span>' +
                marker.properties.country +
                '</span>' +
                '</div>' +
                '<div><img class="img-fluid" src="https://stagingdatawikireviews.s3.amazonaws.com/images/star/blue/' + rating_image + '"/></div>' +
                '<ul class="d-flex flex-wrap mx-n1 mt-2 text-secondary-dark">' +
                '<li class="w-50 px-1 ff-base"><span class="mr-3">' + reviews_count + '</span>Reviews</li>' +
                '<li class="w-50 px-1 ff-base"><span class="mr-3">' + qa_count + '</span>Q&A</li>' +
                '<li class="w-50 px-1 ff-base"><span class="mr-3">' + disputed_count + '</span>Disputed Reviews</li>' +
                '<li class="w-50 px-1 ff-base"><span class="mr-3">' + feedback_count + '</span>Feedbacks</li>' +
                '<li class="w-50 px-1 ff-base"><span class="mr-3">' + message_count + '</span>Messages</li>' +
                '</ul>' +
                // below category-bar has dynamic class of 'type-green', 'type-orange' and 'type-red'
                `<div class="category-bar ${barclass}">&nbsp;</div>`
              )
          )
          .addTo(map);

        markerItems.getElement().addEventListener("click", () => {
          set_branch_ID_local_storage(marker.entries_id);
          handleMapMarkerClick();
        });
      });
      if (elementSet) {
        map.flyTo({
          center: [elementSet[0], elementSet[1]],
          essential: true,
          zoom: 3.7
        });
      }
      // map.zoomIn({ duration: 200 });
    } else {
      map_box_item.forEach(function (marker) {
        const { branch_counts } = marker;
        let rating_image = (marker.data.stats_data && marker.data.stats_data.ratings && marker.data.stats_data.ratings[0] && marker.data.stats_data.ratings[0].rating);
        rating_image = rating_image ? rating_image : 'norating.png'
        let reviews_count = marker?.data?.stats_data && marker?.data?.stats_data?.stats?.review_count?.count
        let disputed_count = marker?.data?.stats_data && marker?.data?.stats_data?.stats?.dispute_review?.count
        let message_count = marker?.data?.stats_data && marker?.data?.stats_data?.stats?.message?.count
        let feedback_count = marker?.data?.stats_data && marker?.data?.stats_data?.stats?.feedback?.count
        let qa_count = marker?.data?.stats_data && marker?.data?.stats_data?.stats?.qa?.count
        let overall_count = marker?.data?.stats_data && marker?.data?.stats_data?.stats?.overall
        let barclass = '';
        barclass = overall_count && overall_count.red > 0 ? 'type-red' : overall_count && overall_count.orange > 0 ? 'type-orange' : 'type-green'
        // create a HTML element for each feature
        var el = document.createElement("div");

        // el.className = "marker";
        el.className = circleClickfilter ?
          marker?.data?.color ? `marker-${marker.data.color}` : 'marker-green' :
          branch_counts && branch_counts.red > 0 ?
            'marker-red' : branch_counts && branch_counts.orange > 0 ?
              'marker-orange' : branch_counts && branch_counts.green > 0 ?
                'marker-green' : 'marker-green';

        // make a marker for each feature and add to the map
        new mapboxgl.Marker(el)
          .setLngLat(marker.geometry.coordinates)
          .addTo(map);

        const markerItems = new mapboxgl.Marker(el)
          .setLngLat(marker.geometry.coordinates)
          .setPopup(
            new mapboxgl.Popup({ offset: 25 }) // add popups
              // Custom Popup design starts below
              .setHTML(
                // '<span class="text-primary-dark fs-12 font-weight-bold">Bridget Bettany</span>' +
                '<h3 class="text-secondary">' +
                marker.properties.title +
                '</h3>' +
                '<div class="fs-12 text-secondary-dark ff-base">' +
                '<span>' +
                marker.properties.address1 +
                '</span><span>' +
                marker.properties.description +
                '</span><span>' +
                marker.properties.country +
                '</span>' +
                '</div>' +
                '<div><img class="img-fluid" src="https://stagingdatawikireviews.s3.amazonaws.com/images/star/blue/' + rating_image + '"/></div>' +
                '<ul class="d-flex flex-wrap mx-n1 mt-2 text-secondary-dark">' +
                '<li class="w-50 px-1 ff-base"><span class="mr-3">' + reviews_count + '</span>Reviews</li>' +
                '<li class="w-50 px-1 ff-base"><span class="mr-3">' + qa_count + '</span>Q&A</li>' +
                '<li class="w-50 px-1 ff-base"><span class="mr-3">' + disputed_count + '</span>Disputed Reviews</li>' +
                '<li class="w-50 px-1 ff-base"><span class="mr-3">' + feedback_count + '</span>Feedbacks</li>' +
                '<li class="w-50 px-1 ff-base"><span class="mr-3">' + message_count + '</span>Messages</li>' +
                '</ul>' +
                // below category-bar has dynamic class of 'type-green', 'type-orange' and 'type-red'
                `<div class="category-bar ${barclass}">&nbsp;</div>`
              )
          )
          .addTo(map);

        if (!circleClickfilter) {
          markerItems.getElement().addEventListener("click", () => {
            set_branch_ID_local_storage(marker.entries_id)
            handleMapMarkerClick()

          });
        }
      });
    }

    map.on("load", function () {
      map.resize();
    });

    map.on("move", () => {
      const { lng, lat } = map.getCenter();
      this.map = map;
      this.setState({
        lng: lng.toFixed(4),
        lat: lat.toFixed(4),
        zoom: map.getZoom().toFixed(2),
      });
    });
  };

  toggleListMap = () => {
    this.setState({ isToggleList: !this.state.isToggleList }, () => {
      this.mapCordinatesSet();

    });
  };

  handleLocationSearch = async (evt) => {

    if (!evt.target.value) {
      this.props.get_branch_message_count_(null);
    }
    await this.setState({
      [evt.target.name]: evt.target.value,
    });
  };

  updateOnTitle = (item) => {
    const { dotSelectedType, dotSelectedColor, dotSelectedName, dotSelectedDays, dotSelectedqueryType } = this.props;

    let dotSelectedTabName = dotSelectedName === '' ? 'mystat' : dotSelectedName;
    let data_type = {
      'mystat': '1',
      'reviews': '4',
      'message': '5',
      'qa': '7',
      'feedback': '6',
      'disputed_Review': '4'
    }

    let elem = document.getElementById("scrollTillTab");
    elem.scrollIntoView();

    // this.props.setTabReviews(data_type[dotSelectedTabName], true, count);
    // this.props.get_branch_data_by_circle_click({
    //   color: dotSelectedColor,
    //   type: dotSelectedType,
    //   days: dotSelectedDays,
    //   queryType: dotSelectedqueryType,
    //   branchID: item?.branch?.id,
    // });

    if (
      item &&
      item.branch &&
      item.branch.id !== null &&
      item.branch.id !== undefined
    ) {
      this.props.setTabReviews(data_type[dotSelectedTabName], true, count);
      this.props.get_branch_data_by_circle_click({
        color: dotSelectedColor,
        type: dotSelectedType,
        days: dotSelectedDays,
        queryType: dotSelectedqueryType,
        branchID: item?.branch?.id,
      });
      let entry = item.branch.id;
      this.props.update_on_list_view_branch({ entry });
      this.props.set_branch_ID_local_storage(entry)
    }
  };

  updateBranchData = (item) => {
    if (item) {
      let entry = item;
      this.props.update_on_list_view_branch({ entry });
      this.props.set_branch_ID_local_storage(entry)
    }
  };

  // checkItemCount = (item, subItem) => {
  //   if (item && item[subItem] !== null) {
  //     return item[subItem]
  //   }
  //   return 0;
  // }

  checkItemCount = ({ data }) => {
    return (
      <div className="d-flex flex-wrap">
        <span>{data?.count} </span>
        <span hidden className="ml-auto">
          {data?.green > 0 && (
            <span className="px-2 d-inline-flex align-items-center">
              <span className="badge badge-success rounded-circle">
                {data?.green}
              </span>
            </span>
          )}
          {data?.orange > 0 && (
            <span className="px-2 d-inline-flex align-items-center">
              <span className="badge badge-warning rounded-circle">
                {data?.orange}{" "}
              </span>
            </span>
          )}
          {data?.red > 0 && (
            <span className="px-2 d-inline-flex align-items-center">
              <span className="badge badge-danger rounded-circle">
                {data?.red}
              </span>
            </span>
          )}
        </span>
      </div>
    );
  };
  checkItemCountCircle = ({ data, item, clickType }) => {
    const { circleClickfilter } = this.state;
    let data_type = { 'reviews': '4', 'message': '5', 'qa': '7', 'feedback': '6', 'disputed_Review': '4' }
    let type = ''
    let tab = ''
    let queryType = ''
    let branchID = item?.address?.id;
    let countDays = ''
    if (item?.type === 'message') {
      type = item.type;
      countDays = item.countDays;
      tab = 'message';
      queryType = 'corporatemessage';
    } else if (item?.type === 'QA') {
      type = 'pending';
      countDays = item.countDays;
      tab = 'qa';
      queryType = 'corporatequestions';
    } else if (item?.type === 'reviews') {
      type = 'new';
      countDays = item.countDays;
      tab = 'reviews';
      queryType = 'corporatereviewsnew';
    } else if (item?.type === 'feedback') {
      type = 'pending';
      countDays = item.countDays;
      tab = 'feedback';
      queryType = 'corporatefeedbackcount';
    }
    // else if (item?.reviewType === 'disputed') {
    //   type = 'disputed_Review';
    //   countDays = item.countDays;
    //   tab = 'disputed_Review';
    //   queryType = 'corporatereviewsnew';
    // }

    // if (circleClickfilter === true) {

    //   if (item?.type === clickType) {
    //     return (
    //       <div className="d-flex flex-wrap align-items-start mx-n2" style={{ minWidth: '150px' }}>
    //         {item?.color === 'green' && (
    //           <Badge color="success" className="fixed-width rounded-circle mx-2"
    //             onClick={() =>
    //               this.updateTabData({ color: "green" }, type, data_type[tab], queryType, branchID, countDays, item?.count)
    //             }
    //           >
    //             {item.count}
    //           </Badge>
    //         )}
    //         {item?.color === 'orange' && (
    //           <Badge color="warning" className="fixed-width rounded-circle mx-2"
    //             onClick={() =>
    //               this.updateTabData({ color: "orange" }, type, data_type[tab], queryType, branchID, countDays, item?.count)
    //             }
    //           >
    //             {item.count}
    //           </Badge>
    //         )}
    //         {item?.color === 'red' && (
    //           <Badge color="danger" className="fixed-width rounded-circle mx-2"
    //             onClick={() =>
    //               this.updateTabData({ color: "red" }, type, data_type[tab], queryType, branchID, countDays, item?.count)
    //             }
    //           >
    //             {item.count}
    //           </Badge>
    //         )}
    //       </div >
    //     );
    //   }
    // } else {
    return (
      <div className="d-flex flex-wrap align-items-start mx-n2" style={{ minWidth: '150px' }}>
        {data?.green > 0 && (
          <Badge color="success" className="fixed-width rounded-circle mx-2"
          // onClick={item?.type === clickType ? () => {
          //   this.updateBranchData(branchID);
          //   this.updateTabData({ color: "green" }, type, data_type[tab], queryType, branchID, countDays, item?.count)
          // }
          //   : ""}
          >
            {data?.green}
          </Badge>
        )}
        {data?.orange > 0 && (
          <Badge color="warning" className="fixed-width rounded-circle mx-2"
          // onClick={item?.type === clickType ? () => {
          //   this.updateBranchData(branchID)
          //   this.updateTabData({ color: "orange" }, type, data_type[tab], queryType, branchID, countDays, item?.count)
          // }
          //   : ""}
          >
            {data?.orange}
          </Badge>
        )}
        {data?.red > 0 && (
          <Badge color="danger" className="fixed-width rounded-circle mx-2"
          // onClick={item?.type === clickType ? () => {
          //   this.updateBranchData(branchID)
          //   this.updateTabData({ color: "red" }, type, data_type[tab], queryType, branchID, countDays, item?.count)
          // }
          //   : ""}
          >
            {data?.red}
          </Badge>
        )}
      </div >
    );
    // }
  };

  updateTabData = ({ color }, tabType, tab, queryType, branchID, countDays, count) => {
    let elem = document.getElementById("scrollTillTab");
    elem.scrollIntoView();
    this.props.setTabReviews(tab, true, count);
    // this.setState({ ViewReviewsType: "new" })
    this.props.get_branch_data_by_circle_click({
      color,
      type: tabType,
      days: countDays,
      queryType,
      branchID: branchID,
    });
  };

  handleListViewRatingText = (rating) => {
    switch (rating) {
      case 'AssortmentofVehicles':
        return 'Assortment of Vehicles';

      case 'BathroomCleanliness':
        return 'Bathroom Cleanliness';

      case 'FoodQuality':
        return 'Food Quality';

      case 'PromptnessinServingMeal':
        return 'Promptness in ServingMeal';

      case 'QualityofAutomobiles':
        return 'Quality of Automobiles';

      case 'ReasonablyPriced':
        return 'Reasonably Priced';

      case 'RestaurantCleanliness':
        return 'Restaurant Cleanliness';

      case 'TurnaroundTime':
        return 'Turnaround Time';

      case 'WillingnesstoNegotiate':
        return 'Willingness to Negotiate';

      case 'WillingnesstoNegotiate':
        return 'Willingness to Negotiate';

      case 'AbilitytoObtainLoanForYou':
        return 'Ability to Obtain Loan For You';

      case 'avg_rating':
        return 'Avg Rating';

      case 'EthicallyOperateInRecession':
        return 'Ethically Operate In Recession';

      case 'LoanApprovalProcess':
        return 'Loan Approval Process';

      case 'Professionalism':
        return 'Professionalism';

      case 'QualityofLoan':
        return 'Quality of Loan';

      case 'Trustworthy':
        return 'Trustworthy';

      case 'TurnaroundTime':
        return 'Turn Around Time';

      case 'CleanStore':
        return 'Clean Store';

      case 'KnowledgeableStaff':
        return 'Knowledgeable Staff';

      case 'ProductAssortment':
        return 'Product Assortment';

      case 'ProductQuality':
        return 'Product Quality';

      case 'Reputation':
        return 'Reputation';

      default:
        break;

    }
  }

  renderListView = () => {
    let { customListArray, circleClickfilteredData, circleClickfilter } = this.state;
    let map_cordinates = "";
    if (circleClickfilter == true) {
      map_cordinates = circleClickfilteredData;

    } else {
      map_cordinates = customListArray;

    }

    if (Array.isArray(map_cordinates) && map_cordinates.length > 0) {
      return map_cordinates.map((item, index) => {
        let { address, stats, details, ratings } = item;
        // let addr = `${address.address1 ? address.address1 : ''} ${address.address2 ? address.address2 : ''}, ${address.city ? address.city : ''},  ${address.state ? address.state : ''}, ${address.country ? address.country : ''}, ${address.zipcode ? address.zipcode : ''},`;
        let addr = `${address.address1 ? address.address1 : ''} ${address.address2 ? address.address2 + ',' : ''} ${address.city ? address.city + ',' : ''} ${address.state ? address.state + ',' : ''} ${address.country ? address.country + ',' : ''} ${address.zipcode ? address.zipcode + ',' : ''}`;

        let review_count = stats?.review_count;
        let dispute_review = stats?.dispute_review;
        let message = stats?.message;
        let borderColor = item.color === 'red' ? '--danger' : item.color === 'orange' ? '--warning' : item.color === 'green' ? '--success' : '--success';
        let qa = stats?.qa;
        let feedback = stats?.feedback;
        let overall = stats?.overall;
        let avg_rating = null;
        if (ratings && ratings.length > 0) {
          avg_rating = ratings.find(item => item.key == "avg_rating");
        }

        return (
          <div
            key={index}
            className="mb-3 text-dark bg-white p-3"
            style={{
              borderRight: `8px solid var(${circleClickfilter ? borderColor : overall?.red > 0 ? '--danger' : overall?.orange > 0 ? '--warning' : '--success'})`,
            }}
          >
            <Row key={index}>
              <Col lg="4">
                <div>
                  <div className="stat dark">
                    <div className="d-flex flex-nowrap mx-n2 mb-2">
                      <div className="px-2 ff-headings fs-25 mt-n2">
                        {this.checkItemCount({ data: review_count })}
                      </div>
                      <div className="px-2">
                        <div className="type fs-16">{"Reviews"}</div>
                      </div>
                      <div className="px-2 ml-auto col-auto">
                        {this.checkItemCountCircle({ data: review_count, item: item, clickType: 'reviews' })}
                      </div>
                    </div>
                  </div>
                  <div className="stat dark">
                    <div className="d-flex flex-nowrap mx-n2 mb-2">
                      <div className="px-2 ff-headings fs-25 mt-n2">
                        {this.checkItemCount({ data: dispute_review })}
                      </div>
                      <div className="px-2">
                        <div className="type fs-16">{"Disputed Reviews"}</div>
                      </div>
                      <div className="px-2 ml-auto col-auto">
                        {this.checkItemCountCircle({ data: dispute_review })}
                      </div>
                    </div>
                  </div>
                  <div className="stat dark">
                    <div className="d-flex flex-nowrap mx-n2 mb-2">
                      <div className="px-2 ff-headings fs-25 mt-n2">
                        {this.checkItemCount({ data: message })}
                      </div>
                      <div className="px-2">
                        <div className="type fs-16">{"Respond to Messages"}</div>
                      </div>
                      <div className="px-2 ml-auto col-auto">
                        {this.checkItemCountCircle({ data: message, item: item, clickType: 'message' })}
                      </div>
                    </div>
                  </div>
                  <div className="stat dark">
                    <div className="d-flex flex-nowrap mx-n2 mb-2">
                      <div className="px-2 ff-headings fs-25 mt-n2">
                        {this.checkItemCount({ data: qa })}
                      </div>
                      <div className="px-2">
                        <div className="type fs-16">{`Respond to Q A`}</div>
                      </div>
                      <div className="px-2 ml-auto col-auto">
                        {this.checkItemCountCircle({ data: qa, item: item, clickType: 'QA' })}
                      </div>
                    </div>
                  </div>
                  <div className="stat dark">
                    <div className="d-flex flex-nowrap mx-n2 mb-2">
                      <div className="px-2 ff-headings fs-25 mt-n2">
                        {this.checkItemCount({ data: feedback })}
                      </div>
                      <div className="px-2">
                        <div className="type fs-16">{"Respond to Feedback"}</div>
                      </div>
                      <div className="px-2 ml-auto col-auto">
                        {this.checkItemCountCircle({ data: feedback, item: item, clickType: 'feedback' })}
                      </div>
                    </div>
                  </div>
                </div>
              </Col>
              <Col lg="4">
                <Card className="text-center">
                  <CardBody>

                    <a
                      onClick={(e) => this.updateOnTitle(item)}
                      className="h2 fs-44 text-secondary text-decoration-none"
                      href="javascript:void(0);"
                    >
                      {address.listing_name}
                    </a>
                    <address className="text-secondary-dark">
                      {addr}
                    </address>
                    {avg_rating && avg_rating.rating ?
                      <div className="text-center mb-3">
                        <img src={`https://stagingdatawikireviews.s3.amazonaws.com/images/star/midium-size/blue/${avg_rating.rating}`} alt='ratings' />
                      </div>
                      : ''}
                    {details && details.CSR && details.CSR.name ? (
                      <div>
                        <img
                          width="30"
                          height="30"
                          className="rounded-circle object-fit-cover"
                          src={details.CSR.profile_pic}
                          alt="Branch"
                        />{" "}
                        <span className="ml-2 font-weight-bold fs-18">{details.CSR.name}</span>
                      </div>
                    ) : null}

                    <div hidden className="text-primary mt-2">
                      Bessie Logan, Mike Fence
                            </div>
                  </CardBody>
                </Card>
              </Col>
              <Col lg="4">
                <ul className="mt-3 list-unstyled text-right">
                  {ratings && Array.isArray(ratings) && ratings.length > 0 ?
                    ratings.filter(item => item.key !== 'avg_rating').map((item, index) => (
                      <li className="mb-2 fs-14 ff-base font-weight-bold" key={index}>
                        <Row className="justify-content-center justify-content-lg-end" noGutters>
                          <Col xs={6}>
                            <div className="px-2 text-right">
                              {this.handleListViewRatingText(item.key) || item.key}
                            </div>
                          </Col>
                          <Col xs={6} lg="auto">
                            <div className="px-2 text-left">
                              <img className="img-fluid" src={`https://stagingdatawikireviews.s3.amazonaws.com/images/star/blue/${item.rating}`} alt="Rating" />
                            </div>
                          </Col>
                        </Row>
                      </li>
                      // <div class="mt-2 mb-2">
                      //   <span>{this.handleListViewRatingText(item.key)}</span>
                      //   {/* <span>{item.key}</span> */}
                      //   <span>
                      //     <img src={`https://stagingdatawikireviews.s3.amazonaws.com/images/star/midium-size/blue/${item.rating}`} alt='ratings' />
                      //   </span>
                      // </div>
                    ))
                    : ''}
                </ul>
              </Col>
            </Row>
          </div>
        );
      });
    }

    return "No Data Available";
  };

  renderListViewOnClick = () => {
    let { customList, branchIDType } = this.state;
    let map_cordinates = customList;

    if (Array.isArray(map_cordinates) && map_cordinates.length > 0) {
      return map_cordinates.map((item, index) => {
        let { address, stats, details, ratings } = item;
        let addr = branchIDType === 'branch' ? `${address.address1 ? address.address1 : ''} ${address.address2 ? address.address2 : ''}, ${address.city ? address.city : ''},  ${address.state ? address.state : ''}, ${address.country ? address.country : ''}, ${address.zipcode ? address.zipcode : ''},` : "";
        let signedUpOn = branchIDType === 'branch' ? "" : item.other_details && item.other_details.personal && item.other_details.personal.joining_date;
        let formatedSignupDate = signedUpOn !== "" ? moment(signedUpOn, "YYYY-MM-DD").format("MMM, YYYY") : ""
        let backuUP = branchIDType === 'branch' ? "" : item.other_details && item.other_details.backup;
        let backuUPFor = branchIDType === 'branch' ? "" : item.other_details && item.other_details.backup_for;
        let AboutMe = branchIDType === 'branch' ? "" : item.other_details && item.other_details.personal && item.other_details.personal.about_me;
        let current_profile_pic_id = branchIDType === 'branch' ? "" : item.csr_details && item.csr_details[0] && item.csr_details[0].current_profile_pic_id;

        let review_count = stats.review_count;
        let dispute_review = stats.dispute_review;
        let message = stats.message;
        let qa = stats.qa;
        let feedback = stats.feedback;
        let overall = stats.overall;
        let avg_rating = null;
        if (ratings && ratings.length > 0) {
          avg_rating = ratings.find(item => item.key == "avg_rating");
        }

        return (
          <div
            key={index}
            className="mb-3 text-dark bg-white p-3"
            style={{
              borderRight: `8px solid var(${overall.red > 0 ? '--danger' : overall.orange > 0 ? '--warning' : '--success'})`,
            }}
          >
            <Row key={index}>
              <Col lg="4">
                <div>
                  <div className="stat dark">
                    <div className="d-flex flex-nowrap mx-n2 mb-2">
                      <div className="px-2 ff-headings fs-25 mt-n2">
                        {this.checkItemCount({ data: review_count })}
                      </div>
                      <div className="px-2">
                        <div className="type fs-16">{"Reviews"}</div>
                      </div>
                      <div className="px-2 ml-auto col-auto">
                        {this.checkItemCountCircle({ data: review_count })}
                      </div>
                    </div>
                  </div>
                  <div className="stat dark">
                    <div className="d-flex flex-nowrap mx-n2 mb-2">
                      <div className="px-2 ff-headings fs-25 mt-n2">
                        {this.checkItemCount({ data: dispute_review })}
                      </div>
                      <div className="px-2">
                        <div className="type fs-16">{"Disputed Reviews"}</div>
                      </div>
                      <div className="px-2 ml-auto col-auto">
                        {this.checkItemCountCircle({ data: dispute_review })}
                      </div>
                    </div>
                  </div>
                  <div className="stat dark">
                    <div className="d-flex flex-nowrap mx-n2 mb-2">
                      <div className="px-2 ff-headings fs-25 mt-n2">
                        {this.checkItemCount({ data: message })}
                      </div>
                      <div className="px-2">
                        <div className="type fs-16">{"Respond to Messages"}</div>
                      </div>
                      <div className="px-2 ml-auto col-auto">
                        {this.checkItemCountCircle({ data: message })}
                      </div>
                    </div>
                  </div>
                  <div className="stat dark">
                    <div className="d-flex flex-nowrap mx-n2 mb-2">
                      <div className="px-2 ff-headings fs-25 mt-n2">
                        {this.checkItemCount({ data: qa })}
                      </div>
                      <div className="px-2">
                        <div className="type fs-16">{`Respond to Q A`}</div>
                      </div>
                      <div className="px-2 ml-auto col-auto">
                        {this.checkItemCountCircle({ data: qa })}
                      </div>
                    </div>
                  </div>
                  <div className="stat dark">
                    <div className="d-flex flex-nowrap mx-n2 mb-2">
                      <div className="px-2 ff-headings fs-25 mt-n2">
                        {this.checkItemCount({ data: feedback })}
                      </div>
                      <div className="px-2">
                        <div className="type fs-16">{"Respond to Feedback"}</div>
                      </div>
                      <div className="px-2 ml-auto col-auto">
                        {this.checkItemCountCircle({ data: feedback })}
                      </div>
                    </div>
                  </div>
                </div>
              </Col>
              {this.state.branchIDType === 'branch' ?
                <>
                  <Col lg="4">
                    <Card className="text-center">
                      <CardBody>

                        <a
                          onClick={(e) => this.updateOnTitle(item)}
                          className="h2 fs-44 text-secondary text-decoration-none"
                          href="javascript:void(0);"
                        >
                          {address.listing_name}
                        </a>
                        <address className="text-secondary-dark">
                          {addr}
                        </address>
                        {avg_rating && avg_rating.rating ?
                          <div className="text-center mb-3">
                            <img src={`https://stagingdatawikireviews.s3.amazonaws.com/images/star/midium-size/blue/${avg_rating.rating}`} alt='ratings' />
                          </div>
                          : ''}
                        {details && details.CSR && details.CSR.name ? (
                          <div>
                            <img
                              width="30"
                              height="30"
                              className="rounded-circle object-fit-cover"
                              src={details.CSR.profile_pic}
                              alt="Branch"
                            />{" "}
                            <span className="ml-2 font-weight-bold fs-18">{details.CSR.name}</span>
                          </div>
                        ) : null}

                        <div hidden className="text-primary mt-2">
                          Bessie Logan, Mike Fence
                            </div>
                      </CardBody>
                    </Card>
                  </Col>
                  <Col lg="4">
                    <Badge color={
                      this.state.branchIDStatOverAll &&
                        this.state.branchIDStatOverAll?.red > 0 ?
                        'danger' : this.state.branchIDStatOverAll?.orange > 0 ? 'warning' : 'success'
                    }
                      className="badge-empty rounded-circle badge-max">
                      &nbsp;
                    </Badge>
                    <ul className="mt-3 list-unstyled text-right">
                      {ratings && Array.isArray(ratings) && ratings.length > 0 ?
                        ratings.filter(item => item.key !== 'avg_rating').map((item, index) => (
                          <li className="mb-2 fs-14 ff-base font-weight-bold" key={index}>
                            <Row className="justify-content-center justify-content-lg-end" noGutters>
                              <Col xs={6}>
                                <div className="px-2 text-right">
                                  {this.handleListViewRatingText(item.key)}
                                </div>
                              </Col>
                              <Col xs={6} lg="auto">
                                <div className="px-2 text-left">
                                  <img className="img-fluid" src={`https://stagingdatawikireviews.s3.amazonaws.com/images/star/blue/${item.rating}`} alt="Rating" />
                                </div>
                              </Col>
                            </Row>
                          </li>
                          // <div class="mt-2 mb-2">
                          //   <span>{this.handleListViewRatingText(item.key)}</span>
                          //   {/* <span>{item.key}</span> */}
                          //   <span>
                          //     <img src={`https://stagingdatawikireviews.s3.amazonaws.com/images/star/midium-size/blue/${item.rating}`} alt='ratings' />
                          //   </span>
                          // </div>
                        ))
                        : ''}
                    </ul>
                  </Col>
                </> :
                <>
                  <Col lg="4">
                    <div className="text-center py-3">
                      <div className="user-photo">
                        <img
                          className="img-circle _100x100"
                          src={`${current_profile_pic_id ? current_profile_pic_id : require("../../assets/images/friend-photo.png")}`}
                          onError={(error) =>
                            (error.target.src = require("../../assets/images/friend-photo.png"))
                          }
                          alt="" />
                        <Badge color={
                          this.state.branchIDStatOverAll &&
                            this.state.branchIDStatOverAll?.red > 0 ?
                            'danger' : this.state.branchIDStatOverAll?.orange > 0 ? 'warning' : 'success'
                        }
                          className="badge-empty rounded-circle">
                          &nbsp;
                                      </Badge>
                      </div>
                      <div className="fs-50 text-dark ff-headings">{this.state.branchIDName} </div>
                      <UncontrolledDropdown>
                        <DropdownToggle
                          className="d-inline-block text-center text-dark"
                          color="transparent"
                          size="sm"
                          caret
                        >
                          <span className="text-secondary-dark fs-14 font-weight-bold d-inline-block"
                            style={{ minWidth: '100px' }}>
                            {item.csr_branch_ratings && item.csr_branch_ratings.length || 0} Branches
                          </span>
                        </DropdownToggle>
                        {item.csr_branch_ratings && item.csr_branch_ratings.length > 0 ? <DropdownMenu
                          className="dropdown-list text-left"
                        >
                          {item && item.csr_branch_ratings && item.csr_branch_ratings.map((element) => {

                            return <DropdownItem key={element.id}>
                              <div className="d-flex align-items-center">
                                <div className="text-secondary ff-headings fs-20 mr-2">
                                  {element.name}
                                </div>
                                <div className="ml-auto">
                                  <img className="mr-2" title={element.rating} src={`https://stagingdatawikireviews.s3.amazonaws.com/images/star/blue/${element.rating}`} alt={element.rating} />
                                  <Badge color={overall.red > 0 ? 'danger' : overall.orange > 0 ? 'warning' : 'success'} className="badge-empty rounded-circle">&nbsp;</Badge>
                                </div>
                              </div>
                            </DropdownItem>
                          })
                          }
                        </DropdownMenu> : ""}
                      </UncontrolledDropdown>
                    </div>
                  </Col>
                  <Col lg="4">
                    <div className="fs-14 font-weight-bold text-center text-lg-left">
                      <div className="text-secondary-dark mb-2">Signed up on: <span className="text-dark">{formatedSignupDate}</span></div>
                      {backuUP && Array.isArray(backuUP) && backuUP.length > 0 ?
                        <div className="text-secondary-dark mb-2">{this.state.branchIDFirstName} Backups: <br />
                          <span className="text-primary">
                            {backuUP.map((element) => {
                              return <><a href="" target="_blank">{element.name_backup}</a>, </>
                            })}
                          </span>
                        </div> : ""}
                      {backuUP && Array.isArray(backuUP) && backuUPFor.length > 0 ?
                        <div className="text-secondary-dark mb-2">{this.state.branchIDFirstName} is backup for: <br />
                          <span className="text-primary">
                            {backuUPFor.map((element) => {
                              return <><a href="" target="_blank">{element.name_backup_for}</a>, </>
                            })}
                          </span>
                        </div> : ""}
                      <div className="text-secondary-dark mb-2">Private Notes: <br /> <span className="text-dark">{AboutMe ? AboutMe : 'none'}</span></div>
                      <div className="text-center text-lg-right mt-3">
                        <Button color="primary" size="sm">Contact {this.state.branchIDFirstName}</Button>
                      </div>
                    </div>
                  </Col>

                </>}</Row>
          </div>
        );
      });
    }

    return "No Data Available";
  };
  updateReviews = ({ color }) => {

    // let elem = document.getElementById("scrollTillTab");
    // elem.scrollIntoView();
    // this.props.setTabReviews();
    // this.props.get_review_data_by_circle_click({
    //   color,
    //   type: "reviews",
    //   days: "3000",
    // });
  };

  onScroll = (e) => {
    let { myScrollRef } = this;
    // let count = 0;
    let clientHeight = myScrollRef.current.clientHeight;
    let scrollHeight = myScrollRef.current.scrollHeight;
    const scrollTop = this.myScrollRef.current.scrollTop.toFixed() - 1;
    let scrollTopCalulated = scrollHeight - clientHeight;
    let scrollMinimun_8 = scrollTopCalulated - 8;
    let scrollMinimun_6 = scrollTopCalulated - 6;
    let scrollMinimun_5 = scrollTopCalulated - 5;
    let scrollMinimun_3 = scrollTopCalulated - 3;
    let scrollMinimun_1 = scrollTopCalulated - 1;

    if (
      scrollTopCalulated == scrollTop ||
      scrollTop == scrollMinimun_1 ||
      scrollTop == scrollMinimun_3 ||
      scrollTop == scrollMinimun_5 ||
      scrollTop == scrollMinimun_6 ||
      scrollTop == scrollMinimun_8
    ) {
      count++;
      if (count == 1) {
        this.fetchMoreData();
      }
      count = 0;
    }

    /*
    let getValue = scrollTopCalulated - scrollTop;
    if (getValue <= 6) {
      count++;
      if (count == 1) {
        this.fetchMoreData();
        count = 0;
      }
      count = 0;
    }*/
  };

  fetchMoreData = async () => {
    let { allBranches, customListArray, circleClickfilter } = this.state;
    let mapData = allBranches.result;
    let currentLength = customListArray.length;
    let nextLength = customListArray.length + 1;
    let pushedData = mapData.slice(currentLength, nextLength);
    let data = pushedData[0];
    if (data && data.id !== null && data.id !== undefined && circleClickfilter === false) {
      try {
        let result = await callApi(
          `/api/scrollbranch/?count=${data.id}&utype=admin`,
          "GET"
        );
        if (result && result.address.id) {
          let address_id = result.address.id;
          let isTrue = customListArray.find(
            (dt) => dt.address.id == address_id
          );
          if (isTrue == undefined) {
            result["branch"] = data;
            customListArray.push(result);
            this.setState({ customListArray });
          }
        }
      } catch (e) {
        console.log(e);
      }
    }
  };

  doneTyping = async () => {
    this.enbaleTextTyping = false;
    const { get_search_branch_csr } = this.props;
    const data = {
      query: this.state.locationSearch,
      userRole: this.state.userRole,
    };
    if (data && data.query !== '') {
      await get_search_branch_csr(data);
    } else {
      this.setState({ searchBranchCsrData: [] }, () => { this.setInitialAtFirst() });
    }
  }

  handleRoleSelect = (id) => {
    const { get_data_by_role } = this.props;
    get_data_by_role(id, this.state.viewDays);
  }

  handleColorSelect = ({ color }) => {
    const { get_data_by_color } = this.props;
    get_data_by_color(color, this.state.viewDays);
  }

  render() {
    let { isToggleList, brnachesStats, customListArray } = this.state;
    const { isAdmin, search_branch_csr_data_loading } = this.props;
    let branchCount = this.props.branchCount;
    let userAssignedRoles = this.props.assignedUserRoles;
    let typingTimer;  //timer identifier
    let doneTypingInterval = 2500;

    return (
      <React.Fragment>
        <section>
          <Container>
            <Row>
              <Col xs="12">
                <div className="d-flex flex-column flex-column-reverse mb-2">
                  <Collapse isOpen={this.state.isMapListCollapseOpen}>
                    <div className="mt-2">
                      {/* Map area below */}
                      <div
                        ref={this.myScrollRef}
                        onScroll={this.state.branchID ? "" : this.onScroll}
                        className="map-list"
                        style={{ display: isToggleList ? "block" : "none" }}
                      >
                        {this.state.branchID !== '' && this.state.branchIDType !== '' ? this.renderListViewOnClick() : this.renderListView()}
                      </div>
                      <div
                        className="test-1"
                        style={{ display: !isToggleList ? "block" : "none" }}
                      >
                        <div ref={this.mapRef} id="mapContainer" />
                      </div>
                    </div>
                  </Collapse>
                  <div className="collapse-header">
                    <div className="bg-dark text-white py-2 px-3">
                      <div className="d-flex">
                        <div className="mr-2 flex-fill">
                          <div className="d-lg-flex mx-n2 align-items-center">
                            <div className="px-2 mb-2 mb-lg-0">
                              <Button
                                color="link"
                                size="sm"
                                onClick={this.toggleListMap}
                                className="text-white text-decoration-none px-0 text-nowrap"
                              >
                                <FontAwesomeIcon
                                  icon="list"
                                  size="lg"
                                  fixedWidth
                                />
                                {isToggleList
                                  ? `Go to Map View`
                                  : "Go to List View"}
                              </Button>
                            </div>
                            <div className="px-2 mb-2 mb-lg-0">
                              <FormGroup className="mb-0 position-relative step-8">
                                <InputGroup className="flex-nowrap">
                                  <Input
                                    className="bg-light light mw-100 map-search-input step-7"
                                    type="search"
                                    name="locationSearch"
                                    placeholder="Branch name, City, State, Zip or CSR name"
                                    onChange={this.handleLocationSearch}
                                    // onBlur={() => {
                                    //   console.log('')
                                    // }}
                                    onClick={() => {
                                      setTimeout(() => {
                                        if (!this.state.locationSearch.length && this.clearSearch) {
                                          this.clearSearch = false;
                                          this.setState({
                                            searchBranchCsrData: [],
                                            locationSearch: '',
                                            branchID: '',
                                            branchIDName: '',
                                            customList: [],
                                            branchIDName: null,
                                            branchIDFirstName: null,
                                            branchIDAvgRating: null,
                                            branchIDStatOverAll: null,
                                          }, () => {
                                            this.props.clear_search_branch_csr([]);
                                            this.setInitialAtFirst();
                                          });
                                        }
                                      }, 500)
                                    }}
                                    onKeyDown={(e) => {
                                      this.clearSearch = true;
                                      this.enbaleTextTyping = true;
                                      clearTimeout(typingTimer);
                                    }}
                                    onKeyUp={(e) => {
                                      this.enbaleTextTyping = true;
                                      clearTimeout(typingTimer);
                                      typingTimer = setTimeout(this.doneTyping, doneTypingInterval);
                                    }}
                                    value={this.state.locationSearch}
                                  />
                                  <InputGroupAddon addonType="append">
                                    <Button
                                      color="light"
                                      className="py-0"
                                      onClick={this.handleLocationSearch}
                                    >
                                      <FontAwesomeIcon icon="search" />{" "}
                                    </Button>
                                  </InputGroupAddon>
                                </InputGroup>
                                {/* Instant Results, show only if has one or more results */}
                                <ul className="list-unstyled instant-results w-100 step-2">
                                  {search_branch_csr_data_loading &&
                                    this.state.locationSearch.length > 0 ? (
                                      <li>
                                        <div className="text-center bg-white p-2">
                                          <img
                                            src={`https://d190bgl4kh5dsd.cloudfront.net/images/w-share.gif`}
                                            alt="loading-wikireview"
                                            width="24"
                                          />
                                        </div>
                                      </li>
                                    ) : (
                                      ""
                                    )}
                                  {this.state.searchBranchCsrData.length ===
                                    0 &&
                                    this.state.locationSearch.length === 0 ? (
                                      ""
                                    ) : this.state.searchBranchCsrData.length ===
                                      0 &&
                                      this.state.locationSearch.length > 0 &&
                                      search_branch_csr_data_loading === false ? (
                                        <div className="result-item result-link">
                                          {this.enbaleTextTyping ? 'Loading ...' : 'No result found'}
                                        </div>
                                      ) : (
                                        this.state.searchBranchCsrData &&
                                        Array.isArray(
                                          this.state.searchBranchCsrData
                                        ) &&
                                        this.state.searchBranchCsrData.length > 0 &&
                                        this.state.searchBranchCsrData.map(
                                          (branchCSRData) => {
                                            return (
                                              <li
                                                key={branchCSRData.id}
                                                className="result-item"
                                                onClick={(e) => {
                                                  e.preventDefault();
                                                  e.stopPropagation();
                                                  this.setState({
                                                    branchID: branchCSRData.id,
                                                    branchIDType: branchCSRData.type.replace(/\s/g, ''),
                                                    // locationSearch: "",
                                                    // searchBranchCsrData: [],
                                                    customList: [],
                                                    branchIDName: null,
                                                    branchIDFirstName: null,
                                                    branchIDAvgRating: null,
                                                    branchIDStatOverAll: null,
                                                  }, () => {
                                                    branchCSRData.set_item = true;
                                                    this.FetchArrayListClick(branchCSRData.id)
                                                    this.props.clear_search_branch_csr(branchCSRData);
                                                    this.handleSearchDropDown(branchCSRData.id);
                                                    this.props.handleMapMarkerClick();
                                                  });
                                                }
                                                }
                                              >
                                                <div className="result-link">
                                                  <div className="d-flex">
                                                    <div>
                                                      <img
                                                        className="result-icon"
                                                        src={
                                                          branchCSRData.profile_pic
                                                            ? branchCSRData.profile_pic
                                                            : require("../../assets/images/icons/placeholder-img-alt.jpg")
                                                        }
                                                        onError={(error) =>
                                                          (error.target.src = require("../../assets/images/icons/placeholder-img-alt.jpg"))
                                                        }
                                                        alt=""
                                                      />
                                                    </div>
                                                    <div>
                                                      {branchCSRData &&
                                                        branchCSRData.name
                                                        ? branchCSRData.name
                                                        : ""}

                                                      <span>
                                                        {branchCSRData.address1
                                                          ? ", " + branchCSRData.address1 + ", "
                                                          : " "}

                                                        {branchCSRData.city
                                                          ? branchCSRData.city +
                                                          ", "
                                                          : " "}
                                                        {branchCSRData.state
                                                          ? branchCSRData.state +
                                                          ", "
                                                          : " "}
                                                        {branchCSRData.country
                                                          ? branchCSRData.country +
                                                          ", "
                                                          : " "}
                                                        {branchCSRData.zipcode}
                                                      </span>
                                                      <br />
                                                      <span>
                                                        {branchCSRData.distance ?
                                                          branchCSRData.country === 'United States' || branchCSRData.country === 'USA' ?
                                                            (branchCSRData.distance * 0.621371).toFixed(2) + " miles away" :
                                                            (branchCSRData.distance).toFixed(2) + " km away" : ""}
                                                      </span>
                                                    </div>
                                                  </div>
                                                </div>
                                              </li>
                                            );
                                          }
                                        )
                                      )}
                                </ul>
                              </FormGroup>
                            </div>
                            <div className="px-2 d-flex flex-column flex-sm-row align-items-center">
                              {this.state.branchID !== '' && this.state.branchIDName !== null ?
                                <div className="px-2" style={{ width: '350px' }} >
                                  <div className="bg-white text-dark p-2">
                                    <div className="d-flex mx-n1">
                                      <div className="px-1 d-flex align-items-center">
                                        <Badge color={
                                          this.state.branchIDStatOverAll &&
                                            this.state.branchIDStatOverAll?.red > 0 ?
                                            'danger' : this.state.branchIDStatOverAll?.orange > 0 ? 'warning' : 'success'
                                        }
                                          className="badge-empty rounded-circle">
                                          &nbsp;
                                      </Badge>
                                        <div className="ff-headings fs-23 ml-3">{this.state.branchIDName} {this.state.branchIDZipcode ? <span>: {this.state.branchIDZipcode}</span> : ""}</div>
                                      </div>
                                      <div className="px-1 ml-auto d-flex align-items-center">
                                        <img className="d-inline-block"
                                          src={`https://stagingdatawikireviews.s3.amazonaws.com/images/star/blue/${this.state.branchIDAvgRating}`} alt="5 Rating"
                                          onError={(error) =>
                                            (error.target.src = 'https://stagingdatawikireviews.s3.amazonaws.com/images/star/blue/norating.png')
                                          }
                                        />
                                        <span className="d-inline-block text-secondary ff-base fs-35 ml-2" role="button" style={{ lineHeight: '1' }}
                                          onClick={(e) => {
                                            e.preventDefault();
                                            this.setState({
                                              searchBranchCsrData: [],
                                              locationSearch: '',
                                              branchID: '',
                                              branchIDName: ''
                                            }, () => {
                                              this.props.clear_search_branch_csr([]);
                                              // this.setInitialAtFirst() 
                                            });
                                          }}>x</span>
                                      </div>
                                    </div>
                                  </div>
                                </div> :
                                <>
                                  <div className="px-2 step-9">
                                    <span
                                      role="button"
                                      onClick={() => {
                                        this.setInitialAtFirst();
                                        this.setState({
                                          circleClickfilter: false,
                                          circleClickfilteredData: [],
                                          isToggleList: false,
                                        });
                                      }}
                                    >{`${branchCount &&
                                      branchCount.branch &&
                                      branchCount.branch.count
                                      ? branchCount.branch.count
                                      : 0
                                      } BRANCHES`}</span>
                                  </div>

                                  {isAdmin ? (
                                    <div className=" px-2">
                                      <div className="mr-lg-4 text-center text-lg-right">
                                        <span className="text-nowrap">
                                          <span className="px-2 d-inline-flex align-items-center">
                                            <span
                                              className="badge badge-success rounded-circle fixed-width cursor-pointer"
                                              onClick={() => {
                                                this.setState({
                                                  onColorClick: true
                                                });
                                                this.handleColorSelect({ color: "green" })
                                              }
                                              }
                                            >
                                              <span>{`${brnachesStats?.overall?.green
                                                ? brnachesStats?.overall?.green
                                                : 0
                                                }`}</span>
                                            </span>
                                          </span>
                                          <span className="px-2 d-inline-flex align-items-center">
                                            <span
                                              className="badge badge-warning rounded-circle fixed-width cursor-pointer"
                                              onClick={() => {
                                                this.setState({
                                                  onColorClick: true
                                                })
                                                this.handleColorSelect({
                                                  color: "orange",
                                                })
                                              }
                                              }
                                            >
                                              <span>{`${brnachesStats?.overall?.orange
                                                ? brnachesStats?.overall?.orange
                                                : 0
                                                } `}</span>
                                            </span>
                                          </span>
                                          <span className="px-2 d-inline-flex align-items-center">
                                            <span
                                              className="badge badge-danger rounded-circle fixed-width cursor-pointer"
                                              onClick={() => {
                                                this.setState({
                                                  onColorClick: true
                                                })
                                                this.handleColorSelect({ color: "red" })
                                              }
                                              }
                                            >
                                              <span>{`${brnachesStats?.overall?.red
                                                ? brnachesStats?.overall?.red
                                                : 0
                                                }`}</span>
                                            </span>
                                          </span>
                                        </span>
                                      </div>
                                    </div>
                                  ) : (
                                      <div className="ml-sm-auto px-2">
                                        <UncontrolledDropdown className="mr-lg-4 text-center text-lg-right">
                                          <DropdownToggle
                                            color="dark"
                                            size="sm"
                                            className="py-0"
                                            caret
                                          >
                                            <span>
                                              <span
                                                className="px-2 d-inline-flex align-items-center cursor-pointer"
                                                onClick={() =>
                                                  this.updateReviews({ color: "green" })
                                                }
                                              >
                                                <span className="badge badge-success rounded-circle badge-empty "></span>
                                                <span className="p-2">{`${branchCount &&
                                                  branchCount.branch &&
                                                  branchCount.branch.green
                                                  ? branchCount.branch.green
                                                  : 0
                                                  }`}</span>
                                              </span>
                                              <span
                                                className="px-2 d-inline-flex align-items-center cursor-pointer"
                                                onClick={() =>
                                                  this.updateReviews({
                                                    color: "orange",
                                                  })
                                                }
                                              >
                                                <span className="badge badge-warning rounded-circle badge-empty"></span>
                                                <span className="p-2">{`${branchCount &&
                                                  branchCount.branch &&
                                                  branchCount.branch.orange
                                                  ? branchCount.branch.orange
                                                  : 0
                                                  } `}</span>
                                              </span>
                                              <span
                                                className="px-2 d-inline-flex align-items-center cursor-pointer"
                                                onClick={() =>
                                                  this.updateReviews({ color: "red" })
                                                }
                                              >
                                                <span className="badge badge-danger rounded-circle badge-empty"></span>
                                                <span className="p-2">{`${branchCount &&
                                                  branchCount.branch &&
                                                  branchCount.branch.red
                                                  ? branchCount.branch.red
                                                  : 0
                                                  }`}</span>
                                              </span>
                                            </span>
                                          </DropdownToggle>
                                          <DropdownMenu right>
                                            {branchCount &&
                                              Array.isArray(branchCount.branch_counts) &&
                                              branchCount.branch_counts.length > 0
                                              ? branchCount.branch_counts.map(
                                                (item, index) => (
                                                  <DropdownItem
                                                    key={index}
                                                    className="mb-2"
                                                  >
                                                    <div className="d-flex align-items-center">
                                                      <span className="mr-4">
                                                        {item.name}
                                                      </span>
                                                      <span className="ml-auto">
                                                        <span className="px-2">
                                                          <span className="badge badge-success rounded-circle fixed-width">
                                                            {item.green}
                                                          </span>
                                                        </span>
                                                        <span className="px-2">
                                                          <span className="badge badge-warning rounded-circle fixed-width">
                                                            {item.orange}
                                                          </span>
                                                        </span>
                                                        <span className="px-2">
                                                          <span className="badge badge-danger rounded-circle fixed-width">
                                                            {item.red}
                                                          </span>
                                                        </span>
                                                      </span>
                                                    </div>
                                                  </DropdownItem>
                                                )
                                              )
                                              : ""}
                                          </DropdownMenu>
                                        </UncontrolledDropdown>
                                      </div>
                                    )}
                                  {isAdmin ? (
                                    <div className="ml-sm-auto px-2">
                                      <UncontrolledDropdown className="mr-lg-4 text-center text-lg-right">

                                        <DropdownToggle
                                          color="dark"
                                          size="sm"
                                          className="d-flex align-items-center mx-n2"
                                          caret
                                          style={{ maxWidth: '290px' }}
                                        >
                                          {this.state.selectedRoleName !== '' ?
                                            <div className="px-2 text-truncate">
                                              <span title="some random long title...">
                                                {`${this.state.selectedRoleData.people.length || ''} ${this.state.selectedRoleName}`}
                                              </span>
                                            </div> :
                                            <span>{`${userAssignedRoles?.total_people || ''}`}</span>
                                          }
                                          <div className="px-2">
                                            <div className="text-center text-lg-right">
                                              <span className="text-nowrap">
                                                <span className="px-2 d-inline-flex align-items-center">
                                                  <span
                                                    className="badge badge-success rounded-circle fixed-width cursor-pointer"
                                                  // onClick={() =>
                                                  //   this.updateReviews({
                                                  //     color: "green",
                                                  //   })
                                                  // }
                                                  >
                                                    <span>{`${this.state.selectedRoleCount !== null
                                                      ? this.state.selectedRoleCount.green
                                                      : userAssignedRoles?.total_count?.green || 0}`}</span>
                                                  </span>
                                                </span>
                                                <span className="px-2 d-inline-flex align-items-center">
                                                  <span
                                                    className="badge badge-warning rounded-circle fixed-width cursor-pointer"
                                                  // onClick={() =>
                                                  //   this.updateReviews({
                                                  //     color: "orange",
                                                  //   })
                                                  // }
                                                  >
                                                    <span>{`${this.state.selectedRoleCount !== null
                                                      ? this.state.selectedRoleCount.orange
                                                      : userAssignedRoles?.total_count?.orange || 0
                                                      } `}</span>
                                                  </span>
                                                </span>
                                                <span className="px-2 d-inline-flex align-items-center">
                                                  <span
                                                    className="badge badge-danger rounded-circle fixed-width cursor-pointer"
                                                  // onClick={() =>
                                                  //   this.updateReviews({ color: "red" })
                                                  // }
                                                  >
                                                    <span>{`${this.state.selectedRoleCount !== null
                                                      ? this.state.selectedRoleCount.red
                                                      : userAssignedRoles?.total_count?.red || 0
                                                      }`}</span>
                                                  </span>
                                                </span>
                                              </span>
                                            </div>
                                          </div>
                                        </DropdownToggle>
                                        <DropdownMenu right className="force-down">
                                          {userAssignedRoles &&
                                            userAssignedRoles.result &&
                                            Array.isArray(userAssignedRoles.result) &&
                                            userAssignedRoles.result.length > 0
                                            ? userAssignedRoles.result.map(
                                              (item, index) => {
                                                if (item.id !== "") {
                                                  return (
                                                    <DropdownItem
                                                      key={index}
                                                      className="mb-2"
                                                      onClick={async () => {
                                                        await this.setState({
                                                          selectedRoleData: item,
                                                          selectedRoleName: item.name,
                                                          selectedRoleCount: item.count,
                                                        }, () => {
                                                          this.setState({
                                                            onColorClick: true
                                                          });
                                                          this.handleRoleSelect(item.id);
                                                        })
                                                      }}
                                                    >
                                                      <div className="d-flex align-items-center">
                                                        <span
                                                          className="mr-4"
                                                        >
                                                          {item.name}
                                                        </span>
                                                        <span className="ml-auto">
                                                          <span className="px-2">
                                                            <span
                                                              className="badge badge-success rounded-circle fixed-width"
                                                            // onClick={async () => {
                                                            //   await this.setState({
                                                            //     selectedRoleData: item,
                                                            //     selectedRoleName: item.name,
                                                            //     selectedRoleCount: item.count,
                                                            //   })
                                                            //   this.handleRoleSelect('green');
                                                            // }}
                                                            >
                                                              {item?.count?.green || 0}
                                                            </span>
                                                          </span>
                                                          <span className="px-2">
                                                            <span
                                                              className="badge badge-warning rounded-circle fixed-width"
                                                            // onClick={async () => {
                                                            //   await this.setState({
                                                            //     selectedRoleData: item,
                                                            //     selectedRoleName: item.name,
                                                            //     selectedRoleCount: item.count,
                                                            //   })
                                                            //   this.handleRoleSelect('orange');
                                                            // }}
                                                            >
                                                              {item?.count?.orange || 0}
                                                            </span>
                                                          </span>
                                                          <span className="px-2">
                                                            <span
                                                              className="badge badge-danger rounded-circle fixed-width"
                                                            // onClick={async () => {
                                                            //   await this.setState({
                                                            //     selectedRoleData: item,
                                                            //     selectedRoleName: item.name,
                                                            //     selectedRoleCount: item.count,
                                                            //   })
                                                            //   this.handleRoleSelect('red');
                                                            // }}
                                                            >
                                                              {item?.count?.red || 0}
                                                            </span>
                                                          </span>
                                                        </span>
                                                      </div>
                                                    </DropdownItem>
                                                  )
                                                }
                                              }
                                            )
                                            : ""}
                                        </DropdownMenu>
                                      </UncontrolledDropdown>
                                    </div>

                                  ) : (
                                      ""
                                    )}
                                </>}
                            </div>
                          </div>
                        </div>
                        <div className="ml-auto">
                          <Button
                            className="btn-collapse mt-2"
                            onClick={this.toggleMapListCollapse}
                          >
                            <span className="collapse-icon"></span>
                          </Button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>

            {/* List view new design */}
            <div className="bg-white p-3" hidden>
              <Row>
                <Col lg="3">
                  Take Stats design from default list view
                </Col>
                <Col lg="6">
                  <div className="text-center py-3">
                    <div>
                      <img className="img-circle _100x100" src="https://stagingdatawikireviews.s3.amazonaws.com/media/media/content/Automation-Testing-Market.8bf875d5c697bd48ca8de9b60614c4ec306604ab.jpg" alt="" />
                    </div>
                    <div className="fs-50 text-dark ff-headings">Thomas Taylor</div>
                    <UncontrolledDropdown>
                      <DropdownToggle
                        className="d-inline-block text-center text-dark"
                        color="transparent"
                        size="sm"
                        caret
                      >
                        <span className="text-secondary-dark fs-14 font-weight-bold d-inline-block"
                          style={{ minWidth: '100px' }}>
                          3 Branches
                        </span>
                      </DropdownToggle>
                      <DropdownMenu
                        className="dropdown-list text-left"
                      >
                        <DropdownItem>
                          <div className="d-flex align-items-center">
                            <div className="text-secondary ff-headings fs-20 mr-2">
                              Apiria Branch Tyler, TX
                            </div>
                            <div className="ml-auto">
                              <img className="mr-2" title="4.9" src="https://stagingdatawikireviews.s3.amazonaws.com/images/star/blue/5rating.png" alt="" />
                              <Badge color="danger" className="badge-empty rounded-circle">&nbsp;</Badge>
                            </div>
                          </div>
                        </DropdownItem>
                        <DropdownItem>
                          <div className="d-flex align-items-center">
                            <div className="text-secondary ff-headings fs-20 mr-2">
                              Apria Branch Chicago, IL
                            </div>
                            <div className="ml-auto">
                              <img className="mr-2" title="4.9" src="https://stagingdatawikireviews.s3.amazonaws.com/images/star/blue/5rating.png" alt="" />
                              <Badge color="danger" className="badge-empty rounded-circle">&nbsp;</Badge>
                            </div>
                          </div>
                        </DropdownItem>
                        <DropdownItem>
                          <div className="d-flex align-items-center">
                            <div className="text-secondary ff-headings fs-20 mr-2">
                              Apria Branch Orange County, CA
                            </div>
                            <div className="ml-auto">
                              <img className="mr-2" title="4.9" src="https://stagingdatawikireviews.s3.amazonaws.com/images/star/blue/5rating.png" alt="" />
                              <Badge color="success" className="badge-empty rounded-circle">&nbsp;</Badge>
                            </div>
                          </div>
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </div>
                </Col>
                <Col lg="3">
                  <div className="fs-14 font-weight-bold text-center text-lg-left">
                    <div className="text-secondary-dark mb-2">Signed up on <span className="text-dark">Feb 2017</span></div>
                    <div className="text-secondary-dark mb-2">Thomas’ Backups: <br /> <span className="text-primary"><a href="#" target="_blank">Bessie Logan</a>, <a href="#" target="_blank">Mike Fence</a>, <a href="#" target="_blank">Joe D’Anna</a></span></div>
                    <div className="text-secondary-dark mb-2">Thomas is backup for: <br /> <span className="text-primary"><a href="#" target="_blank">Peter Falco</a>, <a href="#" target="_blank">Tim Donovan</a></span></div>
                    <div className="text-secondary-dark mb-2">Private Notes: <br /> <span className="text-dark">lorem ipsum lorei </span></div>
                    <div className="text-center text-lg-right mt-3">
                      <Button color="primary" size="sm">Contact Thomas</Button>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </Container>
        </section>
      </React.Fragment >
    );
  }
}

const mapState = (state) => {
  return {
    branchID: state.user.branch_id,
    map_cordinates: state.user.map_cordinates,
    map_cordinates_file: state.user.map_cordinates_file,
    profileData: state.user.current_user,
    current_role_and_permissions: state.user.current_role_and_permissions,
    search_branch_csr_data: state.user.search_branch_csr_data,
    search_branch_csr_data_loading: state.user.search_branch_csr_data_loading,
    isAdmin: state.user.isAdmin,
    top_bar_stats: state.user.top_bar_stats,
    all_branches: state.user.all_branches,
    admin_stats: state.user.admin_stats,
    selectedBranchesData: state.circleClick.selected_Branches_Data,
    filtered_stats_count: state.user.filtered_stats_count,

  };
};

const mapProps = (dispatch) => {
  return {
    get_map_cordinates: () => dispatch(get_map_cordinates()),
    get_map_cordinates_file: () => dispatch(get_map_cordinates_file()),
    current_user_profile: () => dispatch(current_user_profile()),
    get_search_branch_csr: (params, search) => dispatch(get_search_branch_csr(params, search)),
    get_review_data_by_circle_click: ({ color, type, days }) => dispatch(get_review_data_by_circle_click({ color, type, days })),
    get_branch_data_by_circle_click: ({ color, type, days, queryType, branchID }) =>
      dispatch(get_branch_data_by_circle_click({ color, type, days, queryType, branchID })),
    update_on_list_view_branch: ({ entry }) => dispatch(update_on_list_view_branch({ entry })),
    get_branch_message_count_: (data) => dispatch(get_branch_message_count_(data)),
    get_reviews_list_entity_id: (id) => dispatch(get_reviews_list_entity_id(id)),
    set_branch_ID_local_storage: (id) => dispatch(set_branch_ID_local_storage(id)),
    clear_search_branch_csr: (data) => dispatch(clear_search_branch_csr(data)),
    get_data_by_role: (roleID, days) => dispatch(get_data_by_role(roleID, days)),
    get_data_by_color: (color, days) => dispatch(get_data_by_color(color, days)),
  };
};

export default withRouter(connect(mapState, mapProps)(Map));
