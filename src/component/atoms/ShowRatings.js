import React from "react";

export const Ratings = ({ ratings }) => {
  return (
    <>
      <span className="d-inline-block hoverable-rating" title="4.0">
        {ratings?.avg_rating?.length > 0 ? (
          <img
            src={`https://userdatawikireviews.s3.amazonaws.com/images/star/blue/${ratings.avg_rating[0]}`}
            alt=""
          />
        ) : (
          <img
            src="https://userdatawikireviews.s3.amazonaws.com/images/star/blue/5rating.png"
            alt=""
          />
        )}
        {
          <div className="detail-rating-onhover">
            <div className="text-dark font-weight-bold mb-2">
              Detailed Star Rating
            </div>
            {ratings &&
              Object.entries(ratings).map(([key, value]) => {
                return (
                  <div key={key}>
                    <div className="d-flex flex-nowrap align-items-center">
                      <img
                        title={value[1]}
                        className="mr-1"
                        src={`https://userdatawikireviews.s3.amazonaws.com/images/star/blue/${value[0]}`}
                        alt="3.5 Rating"
                      />
                      <span className="ff-alt">{key}</span>
                    </div>
                  </div>
                );
              })}
          </div>
        }
      </span>
    </>
  );
};
