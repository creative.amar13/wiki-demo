import React, { Component } from "react";
import {
  FormGroup,
  Input,
  Badge,
  Label,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { connect } from "react-redux";
import {

  get_branch_data_by_circle_click
} from "../../actions/user";

class Filteration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: this.props.dotSelectedColor === 'red' ? 'Red Alert' :
        this.props.dotSelectedColor === 'orange' ? 'Orange Alert' :
          this.props.dotSelectedColor === 'green' ? 'Good Standing' : 'Red Alert',
	sortBy:"Latest"	  
    };
  }
 
  handleOnClickAlert = (filterName, filterValue) => {
    const { dotSelectedType, dotSelectedColor, dotSelectedName, dotSelectedDays, dotSelectedqueryType, dotSelectedBranchId } = this.props;
    let data_type = { 'reviews': '4', 'message': '5', 'qa': '7', 'feedback': '6', 'disputed_Review': '4' }
    this.setState({ selected: filterName }, () => {
      this.props.get_branch_data_by_circle_click({
        color: filterValue,
        type: dotSelectedType,
        days: dotSelectedDays,
        queryType: dotSelectedqueryType,
        branchID: dotSelectedBranchId,
      });
    })
  }
  sortDataBytime = (e) => {
    this.setState({
		sortBy:e.target.value
	}, () => {
		this.props.sortbyfunction(this.state.sortBy);
	});	
	
    // let byCount = this.state.messagesList.slice(0);
    //   byCount.sort(function (a, b) {
    //     return b.count - a.count;
    //   });
  }
  render() {
    const { mapBranchClickData, dotSelectedColor } = this.props;
    const { selected } = this.state;
    return (
      <React.Fragment>

        {/* New Design */}
        <div >
          {/* Filters */}
          <div

            className="form-inline mb-3">
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label className="mr-sm-2 text-secondary-dark" size="sm">Sorted By</Label>
              <Input className="light" type="select" name="select" bsSize="sm"
			   selected={this.state.sortBy}
                value={this.state.sortBy}
                onChange={this.sortDataBytime}
              >
                <option value="Latest">Latest</option>
                <option value="Oldest">Oldest</option>
              </Input>
            </FormGroup>
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label className="mr-sm-2 text-secondary-dark" size="sm">Showing</Label>
              {/* <Input className="light" type="select" name="select" bsSize="sm">
                <option>All</option>
                <option>Red Alert</option>
                <option>Orange Alert</option>
                <option>Good Standing</option>
              </Input> */}
              <UncontrolledDropdown>
                <DropdownToggle
                  className="d-inline-block text-left text-dark bg-white"
                  color="white"
                  size="sm"
                  caret
                >
                  <span className="text-dark fs-12 d-inline-block"
                    style={{ minWidth: '100px' }}>
                    {selected}
                  </span>
                </DropdownToggle>
                <DropdownMenu
                  className="dropdown-list"
                >
                  <DropdownItem>
                    <div className="d-flex align-items-center"
                      onClick={() => this.handleOnClickAlert("Red Alert", "red")}
                    >
                      <div className="text-danger font-weight-bold mr-2">
                        Red Alert
                      </div>
                      <div className="ml-auto">
                        <Badge color="danger" className="badge-empty rounded-circle">&nbsp;</Badge>
                      </div>
                    </div>
                  </DropdownItem>
                  <DropdownItem>
                    <div className="d-flex align-items-center"
                      onClick={() => this.handleOnClickAlert("Orange Alert", "orange")}
                    >
                      <div className="text-warning font-weight-bold mr-2">
                        Orange Alert
                      </div>
                      <div className="ml-auto">
                        <Badge color="warning" className="badge-empty rounded-circle">&nbsp;</Badge>
                      </div>
                    </div>
                  </DropdownItem>
                  <DropdownItem>
                    <div className="d-flex align-items-center"
                      onClick={() => this.handleOnClickAlert("Good Standing", "green")}
                    >
                      <div className="text-success font-weight-bold mr-2">
                        Good Standing
                      </div>
                      <div className="ml-auto">
                        <Badge color="success" className="badge-empty rounded-circle">&nbsp;</Badge>
                      </div>
                    </div>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </FormGroup>
          </div>

        </div>
      </React.Fragment>
    );
  }
}


const mapProps = {
  get_branch_data_by_circle_click
};

export default connect(null, mapProps)(Filteration);
