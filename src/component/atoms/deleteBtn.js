import React from "react";
import blueIcon from '../../assets/images/icons/delete-mypost.png';
import goldIcon from '../../assets/images/icons/delete-history.png';
import lightIcon from '../../assets/images/icons/delete-tip.png';

export default class DeleteBtn extends React.Component  {
    render() {
        const { onClick, color } = this.props;
        let srcIMG = blueIcon;
        let iconWidth = 16;

        if (color === "gold") {
            srcIMG = goldIcon;
        } else if (color === "light") {
            srcIMG = lightIcon;
            iconWidth = 10;
        } else {
            srcIMG = blueIcon;
            iconWidth = "";
        }
        return (
            <span className="d-inline-block p-1" role="button" title="Delete" onClick={onClick}>
                <img width={iconWidth} src={srcIMG} alt="Delete" alt="" />
            </span>
        )
    }
}