import React, { useState } from "react";
import { Button, Collapse } from 'reactstrap';

const CollapseBasic = (props) => {
    const [isOpen, setIsOpen] = useState(props.isOpen);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div className={"d-flex flex-column flex-column-reverse mb-2 " + (!props.transparent ? 'bg-white ' : ' ') + (props.noPadding ? 'p-0 ' : (props.size === 'sm' ? 'p-1 ' : 'p-3 ')) + (props.containerClass ? props.containerClass : '')} >
            <Collapse isOpen={isOpen}>
                <div className={(props.bodyClass ? props.bodyClass : 'text-secondary-dark')}>
                    {props.children}
                </div>
            </Collapse>
            <div className={"collapse-header" + ' ' + (props.size)}>
                <div className="d-flex">
                    {props.titleImg ? (<div className="mr-2">
                        <img src={props.titleImg} height="20" />
                    </div>) : ''}
                    {props.size === 'sm' ? (
                        <span className="mr-2 font-weight-semi-bold">{props.title}</span>
                    ) : (
                            <h2 className="mr-2">{props.title}</h2>
                        )}
                    <div className="ml-auto">
                        <Button color="" className="btn-collapse" onClick={toggle}>
                            <span className="collapse-icon"></span>
                        </Button>
                    </div>
                </div>
                {props.noHr ? '' : <hr className={"separator " + (props.size === 'sm' ? 'my-2' : '')} />}
            </div>
        </div>
    );
};

export default CollapseBasic;
