import Highcharts from "highcharts";
const highchartOptions = {
    chart: {
        style: {
            fontFamily: 'Arial, Helvetica, sans-serif',
            fontSize: '14px'
        }
    },
    title: {
        style: {
            fontSize: '16px',
            color: 'var(--secondary-dark)',
            fontWeight: 'medium',
            fontFamily: 'Museo, Arial, Helvetica, sans-serif',
        }
    },
    subtitle: {
        style: {
            fontSize: '14px',
            color: '#828282',
            fontWeight: 500
        }
    },
    yAxis: {
        title: {
            style: {
                fontWeight: 500
            }
        }
    },
    xAxis: {
        title: {
            style: {
                fontWeight: 500
            }
        }
    },
    legend: {
        layout: 'horizontal',
        align: 'center',
        verticalAlign: 'top',
        itemStyle: {
            color: '#000000',
            fontSize: '14px',
            fontWeight: 'normal',
        },
        title: {
            style: {
                color: '#000000',
                fontSize: '14px',
                fontWeight: 'normal'
            }
        }
    },
    credits: {
        enabled: false
    },
    lang: {
        thousandsSep: ','
    },
    tooltip: {
        backgroundColor: "rgba(255,255,255,1)"
    },
    colors: ['#d1bb78', '#2b8efa', '#5F7C8A', '#8bc34a', '#FEC007', '#ff5722', '#2196f2', '#795548', '#00bcd3', '#e91e63', '#673ab7'],
    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
};

Highcharts.setOptions(highchartOptions);

export const barChart = (title, subtitle, yAxisTitle, xAxisTitle, tooltip, categories, series) => {
    return {
        chart: {
            type: "bar",
        },
        title: {
            text: title,
        },
        subtitle: {
            text: subtitle,
        },
        xAxis: {
            categories: categories,
            title: {
                text: xAxisTitle,
            },
        },
        yAxis: {
            min: 0,
            title: {
                text: yAxisTitle,
            }
        },
        tooltip: {
            valueSuffix: ' ' + tooltip
        },
        series: series
    };
};

export const pieChart = (title, subtitle, series) => {
    return {
        chart: {
            type: "pie",
        },
        title: {
            text: title,
        },
        subtitle: {
            text: subtitle
        },
        accessibility: {
            announceNewData: {
                enabled: true,
            },
            point: {
                valueSuffix: "%",
            },
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: false,
                    formatter: function () {
                        return this.point.name + ' ' + ((this.y / this.total) * 100).toFixed(0) + '%'
                    }
                },
                showInLegend: true
            },
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat:
                '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>',
        },
        series: series
    };
};


export const lineColChart = (type, title, subtitle, yAxisTitle, xAxisTitle, tooltip, categories, series) => {
    return {
        chart: {
            type: type || 'line',
        },
        title: {
            text: title
        },

        subtitle: {
            text: subtitle
        },

        yAxis: {
            title: {
                text: yAxisTitle
            }
        },
        xAxis: {
            categories: categories,
            title: {
                text: xAxisTitle,
            },
        },
        tooltip: {
            valueSuffix: ' ' + tooltip
        },
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                }
            }
        },
        series: series
    }
}