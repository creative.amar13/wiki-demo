import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import Footer from "../../footer";


class Press extends Component {

    render() {
        return (           
            <React.Fragment>
                <Container className="fs-14 static-page">
                    <section className="bg-dark p-5 text-center text-white">
                        <h1 className="m-0">Media And Press</h1>
                    </section>
                    <section className="bg-white p-4">
                        <Row className="justify-content-between">
                            <Col md={3}>
                                <div className="position-sticky top-FIX">
                                    <h2 className="text-uppercase mb-3">Press</h2>
                                    <div className="scrollable">
                                        <h5 className="mb-2"> For Press Inquiries Contact WikiReviews</h5>
                                        <p className="mb-1">Email: <a href="mailto:press@wikireviews.com">press@wikireviews.com</a></p>
                                        <p>Phone: <a href="tel:+13109936486">1 (310)993-6486</a></p>
                                        <h5 className="mb-2"> Overview</h5>
                                        <ul className="listing-titles mb-3">
                                            <li><Link to="/">WikiReviews Factsheet</Link> </li>
                                            <li><Link to="/">Company anouncement</Link></li>
                                            <li><Link to="/">Brand Guidelines </Link></li>
                                            <li><Link to="/">WikiReviews Blog</Link> </li>
                                        </ul>
                                        <h5 className="mb-2"> Toolkit</h5>
                                        <ul className="listing-titles mb-3">
                                            <li><Link to="/">Print Page</Link> </li>
                                            <li><Link to="/">Email Page</Link></li>
                                            <li><Link to="/">Contact PR</Link></li>
                                        </ul>
                                        <ul className="listing-titles mb-3">
                                            <li><a href="#mobile-app">Mobile App</a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </Col>
                            <Col md={8}>
                                <div className="faq-content mb-4">
                                    <h2 className="text-uppercase text-dark mb-3">
                                        Overview
                                    </h2>
                                    <h4 className="mb-2">What is WikiReviews?  </h4>
                                    <p>
                                        WikiReviews is an online, community-driven social review website that helps people find, write and read reviews of local and nationwide (USA & Canada) businesses, products, services and films. WikiReviews is also a social site where users are encouraged to recommend things they like so that this can easily be shared with their friends and followers on the site. WikiReviews also allows the community to post their own personal projects that they want the community to review. WikiReviews, goal is to be the largest online social site for reviews about just about anything and everything in the world. We can only accomplish this with the help of WikiCommunity volunteers all over the world.
                                    </p>
                                    <h4 className="mb-2">Benefits of WikiReviews</h4>
                                    <p>
                                        A Wiki is a website where users can add, modify or delete content via their web browser.
                                    </p>
                                    <p>
                                        First, we would love for you to contribute to the reviews of products, services, businesses and movies that we have on our site. If you don’t see a particular item on our site, please add it to our database so that others can add their reviews. More info is always better, so be sure to include everything you know, whether that’s a business address, phone number, movie title or product name. If you really enjoy helping, you can also add images and videos or rate other users’ reviews. This can all be done from the Participate Section at the bottom menu bar of our site.
                                    </p>
                                    <p>
                                        No! WikiReviews is completely free to use for all consumer users and business owners.
                                    </p>
                                    <p>
                                        We make money through users clicking on the advertisements shown on the various pages of our site. Also, unlike other review sites, we do not solicit businesses listed on our site for advertising money nor can businesses pay us money to show up higher on our search results page. And most importantly, businesses cannot pay us to modify their star ratings. Star ratings are controlled by users and the community. This money will help us cover our web hosting costs as well as other administrative costs required to keep the site operating. The bulk of our workforce is made up of volunteers working hard to make our site the most comprehensive review site on the web.
                                    </p>
                                    <p>
                                        You will see that we have a very diverse user base of folks who use our site. You'll find a wide range of people on WikiReviews, including locals who are "in the know" about what's cool and happening in their city, visitors who want to get an insider's local perspective, and anyone trying to find a great local business. In our Community Review Projects section, you will find all different types of users from business owners who want feedback on potential logos to musicians wanting to know about their music to even artists.
                                    </p>
                                    <p>
                                        Studies show that people enjoy reviewing things they like and things they do not like. People like to give kudos to those things they really enjoyed and similarly, people like to caution others about their experiences. WikiReviews is not just a site full of complaints but rather a balanced site with reviews of all types.
                                    </p>

                                    <ul className="list-normal">
                                        <li>
                                            
                                            Authenticating users to prevent fake accounts.
                                            
                                        </li>
                                        <li>
                                            
                                            Only allowing movie reviews to be written after the date of release.
                                            
                                        </li>
                                        <li>
                                            
                                            Offering Detailed Star Ratings so users can get much deeper insight.
                                            
                                        </li>
                                        <li>
                                            
                                            Making it easy to create reviews by offering speech to text dictation reviews.
                                            
                                        </li>
                                        <li>
                                            
                                            Allowing reviewers to create 5-minute long video reviews…great for products.
                                            
                                        </li>
                                        <li>
                                            
                                            Allowing reviewers to review individuals at businesses where the person’s review weighs heavily over the actual business review…such as hairdressers, pet groomers, lawyers, salon technicians and even police officers.
                                            
                                        </li>
                                        <li>
                                            
                                            We will be the first review site to allow actor reviews…so you can even see how actors rate in specific genres!
                                            
                                        </li>
                                        <li>
                                            
                                            We allow businesses to dispute fake and irrelevant reviews. So, when a business gets a bad review because the reviewer got a speeding ticket on the way to the business, those can get removed.
                                            
                                        </li>
                                        <li>
                                            
                                            Our site is social so you can be kept updated on all the things your friends review and what they recommend.
                                            
                                        </li>
                                        <li>
                                            
                                            Use our UPC product barcode scanner to easily find and create reviews.
                                            
                                        </li>
                                        <li>
                                            
                                            Use our Buyers Guide to learn the vocabulary associated with things such as buying a new camera and learning Press f-stops, aperture settings, etc. and even things like different types of pastas and food. Also, we have a How to Buy Section that anyone can read so they have proper guidance before making a new purchase.
                                            
                                        </li>
                                        <li>
                                            
                                            Finally, we are turning reviews upside down by allowing anyone to upload their own “personal projects” to be uploaded for the community to review.
                                            
                                        </li>
                                        <li>
                                            
                                        The community help to “govern” WikiReviews through adding, patrolling, voting and improving the content on the site.
                                            
                                        </li>
                                        <li>
                                            
                                            Allowing reviewers to mark their reviews if they contain “movie spoilers” so that people who have not seen the movie do not accidentally read those reviews.
                                            
                                        </li>
                                        <li>
                                            
                                            We found that most people choose movies to see based upon what is playing at their “favorite” theater. So we allow users to put in their favorite theater in their profile and then we have a specific section called “User’s Theater” that they can go to and easily find the movies and showtimes and trailers for all the movies playing at their favorite theater.
                                            
                                        </li>
                                    </ul>
                                </div>
                                <h2 className="text-uppercase text-dark mb-3" id="mobile-app">WikiReviews Mobile App</h2>
                                <p>
                                    WikiReviews will soon to be the largest community-run business, movie, product, professional and actor review site. At WikiReviews the community is in charge, from disputing reviews to voting on whether reviews should be deleted or filtered. Be a part of the largest social reviews network.
                                </p>
                                <p>
                                    <strong className="ff-base">WikiReviews Features:</strong>
                                    <br/>
                                    Users can create reviews for just about anything by writing, dictating or recording reviews. Users can also create their own projects to be reviewed by the community.
                                </p>
                                <ul className="list-normal">
                                    <li>
                                        Discover trending businesses, products & movies in the USA & Canada
                                    </li>
                                    <li>
                                        Easily create reviews by writing, dictating or recording reviews
                                    </li>
                                    <li>
                                        Upload your own personal projects to get reviewed by the community.
                                    </li>
                                    <li>
                                        Discover great local businesses in any category
                                    </li>
                                    <li>
                                        Watch movie trailers. Lookup movie showtimes and buy tickets.
                                    </li>
                                    <li>
                                        Connect with your friends and be kept up to date on their reviews and recommendations
                                    </li>
                                    <li>
                                        Compare products to find out what reviewers like best.
                                    </li>
                                    <li>
                                        Watch Video Reviews that give you a much clearer picture before you purchase anything.
                                    </li>
                                    <li>
                                        Scan in a barcode of products you like and quickly read the reviews.
                                    </li>
                                    <li>
                                        Find the best-rated menu items in a restaurant to order.
                                    </li>
                                    <li>
                                        Peel behind general five-star ratings by getting detailed star ratings for every listing.
                                    </li>
                                    <li>
                                        Set your favorite Local Theater as your Home Theater and easily get showtimes, tickets and trailers for every movie playing there.
                                    </li>
                                    <li>
                                        Ask questions and problems and post answers and solutions for common issues with products, movies and businesses.
                                    </li>
                                    <li>
                                        Look up a business address, phone number, email, website, hours of operation and more.
                                    </li>
                                </ul>
                                <p>
                                    Download the App Today and See What Everyone's Talking About!
                                </p>
                            </Col>
                        </Row>
                    </section>
                </Container>

                <Footer />
            </React.Fragment>
        )
    }
}



export default (Press);