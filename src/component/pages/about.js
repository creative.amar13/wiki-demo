import React, { Component } from "react";
import { Container, Jumbotron, Row, Col } from "reactstrap";
import { Link } from "react-router-dom";
import Footer from "../../footer";

class About extends Component {
  render() {
    return (
      <React.Fragment>
        <Container className="fs-14 static-page">
          <section className="bg-dark p-5 text-center text-white">
            <h1 className="m-0">About WikiReviews</h1>
          </section>
          <section className="bg-white p-4">
            <Row className="justify-content-between">
              <Col md={3}>
                <div className="position-sticky top-FIX">
                  <h2 className="text-uppercase mb-3">Menu</h2>
                  <div className="scrollable">
                    <ul className="listing-titles">
                      <li>
                        <Link to="/terms">Terms & Conditions</Link>{" "}
                      </li>
                      <li>
                        <Link to="/privacy-policy">Privacy Policy </Link>
                      </li>
                      <li>
                        <Link to="/faq">FAQ </Link>
                      </li>
                      <li>
                        <Link to="/guidelines">Guidelines</Link>{" "}
                      </li>
                    </ul>
                  </div>
                </div>
              </Col>
              <Col md={9}>
                <div className="mb-4">
                  <h2 className="text-uppercase text-dark mb-3">About Us</h2>
                  <p>
                    WikiReviews, Inc is based in Irvine, Calfifornia and is a
                    privately owned company not funded by venture capitalists
                    but investors who believe that the “review space” is broken
                    and needs fixing. Sunil Wagle is the founder and he created
                    WikiReviews after himself experiencing issues with reviews
                    for his e-commerce business. While exploring the reviews
                    industry, it was brought to his attention that the “wiki”
                    model of business in the reviews space could be a good idea.
                    As Sunil did more and more research on this concept, he
                    quickly understood that the power of the community cannot be
                    beaten. Moreover, he identified a plethora of big problems in the review space including fake reviews, “paid for” reviews and even the inability of business owners to do anything about fake or not relevant reviews.
                  </p>
                  <p>
                    He also spent time researching issues with other “Wiki”
                    sites including that most contributors are younger males who
                    once they get married, stopped contributing. Also, he
                    learned that contributors had to understand HTML
                    programming, otherwise they could not contribute. Finally,
                    he also learned that whenever there is a lot of
                    something…like things to review, or how-to articles or an
                    encyclopedia or even reporting accidents and police on our
                    roadways…the best and truly the only successful method to 
                    do this is by using a “wiki” model,
                    where the community contributes, edits and patrols the
                    content on the site. Only with the power of the community,
                    can organizations focused on these things succeed.
                  </p>
                  <p>
                    Sunil was convinced that he had to make WikiReviews user
                    friendly to appeal to a much broader audience of people who
                    can contribute, and edit and patrol the content. This
                    entailed a lot of work, time and money in determining user-friendly ways for anyone to contribute to WikiReviews.
                  </p>
                  <p>
                    Also, he developed solutions to every single issue he found
                    with today’s existing review sites which{" "}
                    <strong className="ff-base">sets our site apart</strong>{" "}
                    from all other review sites and these include the following:
                  </p>

                  <ul className="list-normal">
                    <li>Authenticating users to prevent fake accounts.</li>
                    <li>
                      Only allowing movie reviews to be written after the date
                      of release.
                    </li>
                    <li>
                      Offering Detailed Star Ratings so users can get much
                      deeper insight.
                    </li>
                    <li>
                      Making it easy to create reviews by offering speech to
                      text dictation reviews.
                    </li>
                    <li>
                      Allowing reviewers to create 5-minute long video
                      reviews…great for products.
                    </li>
                    <li>
                      Allowing reviewers to review individuals at businesses
                      where the person’s review weighs heavily over the actual
                      business review…such as hairdressers, pet groomers,
                      lawyers, salon technicians and even police officers.
                    </li>
                    <li>
                      We will be the first review site to allow actor reviews…so
                      you can even see how actors rate in specific genres!
                    </li>
                    <li>
                      We allow businesses to dispute fake and irrelevant
                      reviews. So, when a business gets a bad review because the
                      reviewer got a speeding ticket on the way to the business,
                      those can get removed.
                    </li>
                    <li>
                      Our site is social so you can be kept updated on all the
                      things your friends review and what they recommend.
                    </li>
                    <li>
                      Use our UPC product barcode scanner to easily find and
                      create reviews.
                    </li>
                    <li>
                      Use our Buyers Guide to learn the vocabulary associated
                      with things such as buying a new camera and learning about
                      f-stops, aperture settings, etc. and even things like
                      different types of pastas and food. Also, we have a How to
                      Buy Section that anyone can read so they have proper
                      guidance before making a new purchase.
                    </li>
                    <li>
                      Finally, we are turning reviews upside down by allowing
                      anyone to upload their own “personal projects” to be
                      uploaded for the community to review.
                    </li>
                    <li>
                      The community help to “govern” WikiReviews through adding,
                      patrolling, voting and improving the content on the site.
                    </li>
                    <li>
                      Allowing reviewers to mark their reviews if they contain
                      “movie spoilers” so that people who have not seen the
                      movie do not accidentally read those reviews.
                    </li>
                    <li>
                      We found that most people choose movies to see based upon
                      what is playing at their “favorite” theater. So we allow
                      users to put in their favorite theater in their profile
                      and then we have a specific section called “User’s
                      Theater” that they can go to and easily find the movies,
                       showtimes and trailers for all the movies playing at
                      their favorite theater.
                    </li>
                  </ul>
                </div>

                <div>
                  <h2 className="mb-3" id="history">
                    History, Launch & Long-Term Goals
                  </h2>
                  <p>
                    WikiReviews was founded in 2010 and after many years of
                    perfecting the soon to be largest review site on the world
                    wide web, WikiReviews has finally launched and we couldn’t
                    be more excited for our community to grow.
                  </p>
                  <p>
                    WikiReviews strives to provide honest and thorough reviews
                    about businesses, products, and movies. And soon, we hope to
                    expand into every single type of review imaginable…from
                    reviewing college athletic programs to boards of directors
                    to money managers and even school principals….the categories
                    to review are endless and we hope to one day cover as many
                    as possible.
                  </p>
                  <p>
                    WikiReviews was created{" "}
                    <strong className="ff-base">for </strong>the community and{" "}
                    <strong className="ff-base">by</strong> the community. Sign
                    up for WikiReviews and become a part of the world’s largest
                    (and most honest) reviews community!
                  </p>
                </div>
              </Col>
            </Row>
          </section>
        </Container>
       
        <Footer />
      </React.Fragment>
    );
  }
}

export default About;
