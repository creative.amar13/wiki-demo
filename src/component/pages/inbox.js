import React, { Component } from 'react';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import { withRouter } from "react-router";
import {
    Nav,
    NavItem,
    NavLink,
    TabContent,
    TabPane,
    Row,
    Col,
    Media,
    Input,
    Button,
    Form,
    Modal,
    ModalHeader,
    ModalBody,
    FormGroup,
    Label,
    ModalFooter,
    Container,
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AppHeader from '../../app-header';
import DeleteBtn from '../atoms/deleteBtn';
import { current_user_profile } from "../../actions/user";

class UserInbox extends Component {
    constructor(props) {
        super(props)

        this.state = {
            viewMessagesType: 'inbox',
        }
    }
    componentDidMount() {
        this.props.current_user_profile();
    }
    render() {
        return (
            <>
                <AppHeader />
                <section className="py-4 mt-5">
                    <Container>
                        <div className="fs-14 text-dark">
                            <div className="d-flex mx-n2 mb-3">
                                <div className="px-2">
                                    <Nav tabs className="non-decorated-alt">
                                        <NavItem>
                                            <NavLink href="#"
                                                active={this.state.viewMessagesType === 'inbox'}
                                                onClick={() => { this.setState({ viewMessagesType: 'inbox' }) }}>
                                                Inbox <span className="count">0</span>
                                            </NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink href="#"
                                                active={this.state.viewMessagesType === 'draft'}
                                                onClick={() => { this.setState({ viewMessagesType: 'draft' }) }}>
                                                Draft <span className="count">1</span>
                                            </NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink href="#"
                                                active={this.state.viewMessagesType === 'trash'}
                                                onClick={() => { this.setState({ viewMessagesType: 'trash' }) }}>
                                                Trash <span className="count">3</span>
                                            </NavLink>
                                        </NavItem>
                                    </Nav>
                                </div>
                                <div className="ml-auto col-auto px-2">
                                    <div className="d-flex">
                                        <Button color="primary" size="sm" onClick={() => this.setState({ composeNewMessageModal: true })}>Compose</Button>
                                        <Input className="bg-search search-block-input primary ml-3" type="search" bsSize="sm" placeholder="Search Here..." />
                                    </div>
                                </div>
                            </div>
                            <TabContent activeTab={this.state.viewMessagesType}>
                                <TabPane tabId="inbox">
                                    <Row>
                                        <Col md={4}>
                                            <ul className="list-unstyled">
                                                <li className="chat-room active" role="button">
                                                    <Media>
                                                        <Media className="mr-2" left href="#">
                                                            <Media className="img-circle _50x50" object src={'https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/1594323905763.b61278a7aaab346f8337a4cbbeded879146441a5.jpg'} alt="User" />
                                                        </Media>
                                                        <Media body>
                                                            <div className="d-flex mx-n2 mb-2">
                                                                <Media className="chat-title px-2" heading>
                                                                    Hamko Distribution, LLC
                                                            </Media>
                                                                <div className="col-auto px-2 ml-auto">
                                                                    <div className="text-faded mb-2">
                                                                        Apr 09
                                                                </div>
                                                                    <div className="text-right">
                                                                        <DeleteBtn />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Media>
                                                    </Media>
                                                </li>
                                                <li className="chat-room" role="button">
                                                    <Media>
                                                        <Media className="mr-2" left href="#">
                                                            <Media className="img-circle _50x50" object src={'https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/1594323905763.b61278a7aaab346f8337a4cbbeded879146441a5.jpg'} alt="User" />
                                                        </Media>
                                                        <Media body>
                                                            <div className="d-flex mx-n2 mb-2">
                                                                <Media className="chat-title px-2" heading>
                                                                    Addison Bryant
                                                            </Media>
                                                                <div className="col-auto px-2 ml-auto">
                                                                    <div className="text-faded mb-2">
                                                                        Dec 16
                                                                </div>
                                                                    <div className="text-right">
                                                                        <DeleteBtn />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Media>
                                                    </Media>
                                                </li>
                                                <li className="chat-room" role="button">
                                                    <Media>
                                                        <Media className="mr-2" left href="#">
                                                            <Media className="img-circle _50x50" object src={'https://userdatawikireviews.s3.amazonaws.com/images/group-icon.png'} alt="User" />
                                                        </Media>
                                                        <Media body>
                                                            <div className="d-flex mx-n2 mb-2">
                                                                <Media className="chat-title px-2" heading>
                                                                    Addison Bryant, Jayne McGinnis, Amy Andonian
                                                            </Media>
                                                                <div className="col-auto px-2 ml-auto">
                                                                    <div className="text-faded mb-2">
                                                                        Jul 13
                                                                </div>
                                                                    <div className="text-right">
                                                                        <DeleteBtn />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Media>
                                                    </Media>
                                                </li>
                                            </ul>
                                        </Col>
                                        <Col md={8}>
                                            <div className="mb-2 text-dark font-weight-bold">
                                                Conversations
                                        </div>

                                            <div className="bg-white p-3 mb-2">
                                                <div className="text-center mb-2">
                                                    Jan 20, 2020
                                            </div>
                                                <ul className="list-unstyled">
                                                    <li className="chat-list">
                                                        <Media>
                                                            <Media className="mr-2" left href="#">
                                                                <Media className="img-circle _40x40" object src={'https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/1594323905763.b61278a7aaab346f8337a4cbbeded879146441a5.jpg'} alt="User" />
                                                            </Media>
                                                            <Media body>
                                                                <div className="d-flex mx-n2 mb-1">
                                                                    <Media className="chat-title px-2" heading>
                                                                        <a href="#" className="text-reset">WikiReviews Investor</a>
                                                                    </Media>
                                                                    <div className="text-faded col-auto px-2 ml-auto">
                                                                        Jan 20, 2020 8:59 AM
                                                                </div>
                                                                    <div className="px-2">
                                                                        <DeleteBtn />
                                                                    </div>
                                                                </div>
                                                            What just happen
                                                        </Media>
                                                        </Media>
                                                    </li>
                                                    <li className="chat-list">
                                                        <Media>
                                                            <Media className="mr-2" left href="#">
                                                                <Media className="img-circle _40x40" object src={'https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/1594323905763.b61278a7aaab346f8337a4cbbeded879146441a5.jpg'} alt="User" />
                                                            </Media>
                                                            <Media body>
                                                                <div className="d-flex mx-n2 mb-1">
                                                                    <Media className="chat-title px-2" heading>
                                                                        <a href="#" className="text-reset">WikiReviews Investor</a>
                                                                    </Media>
                                                                    <div className="text-faded col-auto px-2 ml-auto">
                                                                        Jan 20, 2020 8:59 AM
                                                                </div>
                                                                    <div className="px-2">
                                                                        <DeleteBtn />
                                                                    </div>
                                                                </div>
                                                            What just happen
                                                        </Media>
                                                        </Media>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="bg-white p-3 mb-2">
                                                <div className="text-center mb-2">
                                                    Jan 20, 2020
                                            </div>
                                                <ul className="list-unstyled">
                                                    <li className="chat-list">
                                                        <Media>
                                                            <Media className="mr-2" left href="#">
                                                                <Media className="img-circle _40x40" object src={'https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/1594323905763.b61278a7aaab346f8337a4cbbeded879146441a5.jpg'} alt="User" />
                                                            </Media>
                                                            <Media body>
                                                                <div className="d-flex mx-n2 mb-1">
                                                                    <Media className="chat-title px-2" heading>
                                                                        <a href="#" className="text-reset">WikiReviews Investor</a>
                                                                    </Media>
                                                                    <div className="text-faded col-auto px-2 ml-auto">
                                                                        Jan 20, 2020 8:59 AM
                                                                </div>
                                                                    <div className="px-2">
                                                                        <DeleteBtn />
                                                                    </div>
                                                                </div>
                                                            What just happen
                                                        </Media>
                                                        </Media>
                                                    </li>
                                                    <li className="chat-list">
                                                        <Media>
                                                            <Media className="mr-2" left href="#">
                                                                <Media className="img-circle _40x40" object src={'https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/1594323905763.b61278a7aaab346f8337a4cbbeded879146441a5.jpg'} alt="User" />
                                                            </Media>
                                                            <Media body>
                                                                <div className="d-flex mx-n2 mb-1">
                                                                    <Media className="chat-title px-2" heading>
                                                                        <a href="#" className="text-reset">WikiReviews Investor</a>
                                                                    </Media>
                                                                    <div className="text-faded col-auto px-2 ml-auto">
                                                                        Jan 20, 2020 8:59 AM
                                                                </div>
                                                                    <div className="px-2">
                                                                        <DeleteBtn />
                                                                    </div>
                                                                </div>
                                                            What just happen
                                                        </Media>
                                                        </Media>
                                                    </li>
                                                </ul>
                                            </div>

                                            <Form>
                                                <div className="bg-white mt-4 p-3 mb-3">
                                                    <Input className="primary add-comment-input not-trash bg-white" type="text" placeholder="Write Something..." />
                                                </div>

                                                {/* Uploaded media shown here */}
                                                <div>
                                                    <Row xs={2} sm={3} md={4} lg={5} form>
                                                        <Col className="mb-3">
                                                            <div className="d-flex pr-3 pt-3" role="button" onClick={() => this.setState({ viewMyPhotosModal: true })}>
                                                                <div>
                                                                    <img className="img-fluid img-thumbnail" src={'https://stagingdatawikireviews.s3.amazonaws.com/media/content/images.82805121a31b59ba88646a19ca8044f136f8855c.png'} alt="Uploaded media" />
                                                                </div>
                                                                <div className="mx-n3 mt-n3">
                                                                    <Button color="dark" size="sm" title="Remove Media"><FontAwesomeIcon icon="minus" /> </Button>
                                                                </div>
                                                            </div>
                                                        </Col>
                                                        <Col className="mb-3">
                                                            <div className="d-flex pr-3 pt-3" role="button" onClick={() => this.setState({ viewMyPhotosModal: true })}>
                                                                <div>
                                                                    <img className="img-fluid img-thumbnail" src={'https://stagingdatawikireviews.s3.amazonaws.com/media/content/images.82805121a31b59ba88646a19ca8044f136f8855c.png'} alt="Uploaded media" />
                                                                </div>
                                                                <div className="mx-n3 mt-n3">
                                                                    <Button color="dark" size="sm" title="Remove Media"><FontAwesomeIcon icon="minus" /> </Button>
                                                                </div>
                                                            </div>
                                                        </Col>
                                                        <Col className="mb-3">
                                                            <div className="d-flex pr-3 pt-3" role="button" onClick={() => this.setState({ viewMyPhotosModal: true })}>
                                                                <div>
                                                                    <img className="img-fluid img-thumbnail" src={'https://stagingdatawikireviews.s3.amazonaws.com/media/content/images.82805121a31b59ba88646a19ca8044f136f8855c.png'} alt="Uploaded media" />
                                                                </div>
                                                                <div className="mx-n3 mt-n3">
                                                                    <Button color="dark" size="sm" title="Remove Media"><FontAwesomeIcon icon="minus" /> </Button>
                                                                </div>
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </div>

                                                <div className="mb-3">
                                                    <div className="d-flex mx-n2">
                                                        <div className="px-2">
                                                            <span role="button" title="Upload media">
                                                                <img src={require('../../assets/images/icons/feed-cam.png')} alt="" />
                                                            </span>
                                                        </div>
                                                        <div className="px-2 ml-auto">
                                                            <Button color="transparent" type="button" className="text-dark">Clear</Button>
                                                            <Button color="primary" type="submit">Reply</Button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Form>
                                        </Col>
                                    </Row>
                                </TabPane>
                                <TabPane tabId="draft">
                                    <Row>
                                        <Col md={4}>
                                            <ul className="list-unstyled">
                                                <li className="chat-room active" role="button">
                                                    <Media>
                                                        <Media className="mr-2" left href="#">
                                                            <Media className="img-circle _50x50" object src={'https://userdatawikireviews.s3.amazonaws.com/images/group-icon.png'} alt="User" />
                                                        </Media>
                                                        <Media body>
                                                            <div className="d-flex mx-n2 mb-2">
                                                                <Media className="chat-title px-2" heading>
                                                                    Addison Bryant, Jayne McGinnis, Amy Andonian
                                                            </Media>
                                                                <div className="col-auto px-2 ml-auto">
                                                                    <div className="text-faded mb-2">
                                                                        Jul 13
                                                                </div>
                                                                    <div className="text-right">
                                                                        <DeleteBtn />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Media>
                                                    </Media>
                                                </li>
                                            </ul>
                                        </Col>
                                        <Col md={8}>
                                            <div className="mb-2 text-dark font-weight-bold">
                                                Conversations
                                        </div>
                                            <div className="bg-white p-3 mb-2">
                                                <div className="text-center mb-2">
                                                    Jan 20, 2020
                                            </div>
                                                <ul className="list-unstyled">
                                                    <li className="chat-list">
                                                        <Media>
                                                            <Media className="mr-2" left href="#">
                                                                <Media className="img-circle _40x40" object src={'https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/1594323905763.b61278a7aaab346f8337a4cbbeded879146441a5.jpg'} alt="User" />
                                                            </Media>
                                                            <Media body>
                                                                <div className="d-flex mx-n2 mb-1">
                                                                    <Media className="chat-title px-2" heading>
                                                                        <a href="#" className="text-reset">WikiReviews Investor</a>
                                                                    </Media>
                                                                    <div className="text-faded col-auto px-2 ml-auto">
                                                                        Jan 20, 2020 8:59 AM
                                                                </div>
                                                                </div>
                                                            What just happen
                                                        </Media>
                                                        </Media>
                                                    </li>
                                                    <li className="chat-list">
                                                        <Media>
                                                            <Media className="mr-2" left href="#">
                                                                <Media className="img-circle _40x40" object src={'https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/eec181c6d57700abb2feb82d706759a8--chicago-cubs-logo-chicago-cubs-baseball.c8d294b1520442508eac3f8ebb265daf344d157e.jpg'} alt="User" />
                                                            </Media>
                                                            <Media body>
                                                                <div className="d-flex mx-n2 mb-1">
                                                                    <Media className="chat-title px-2" heading>
                                                                        <a href="#" className="text-reset">Addison Bryant</a>
                                                                    </Media>
                                                                    <div className="text-faded col-auto px-2 ml-auto">
                                                                        Jan 20, 2020 8:59 AM
                                                                </div>
                                                                </div>
                                                            Test Post
                                                        </Media>
                                                        </Media>
                                                    </li>
                                                </ul>
                                            </div>

                                            <Form>
                                                <div className="bg-white mt-4 p-3 mb-3">
                                                    <Input className="primary add-comment-input not-trash bg-white" type="text" defaultValue="Test Post" placeholder="Write Something..." />
                                                </div>

                                                <div className="mb-3">
                                                    <div className="d-flex mx-n2">
                                                        <div className="px-2">
                                                            <span role="button" title="Upload media">
                                                                <img src={require('../../assets/images/icons/feed-cam.png')} alt="" />
                                                            </span>
                                                            <Button color="transparent" type="button" className="text-dark ml-2">Clear</Button>
                                                        </div>
                                                        <div className="px-2 ml-auto">
                                                            <Button color="primary" type="button">Save Draft</Button>
                                                            <Button color="primary" type="submit">Send</Button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Form>
                                        </Col>
                                    </Row>
                                </TabPane>
                                <TabPane tabId="trash">
                                    <Row>
                                        <Col md={4}>
                                            <ul className="list-unstyled">
                                                <li className="chat-room active" role="button">
                                                    <Media>
                                                        <Media className="mr-2" left href="#">
                                                            <Media className="img-circle _50x50" object src={'https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/PP.a8488daa1cbc7e67aedf12b970c19d453f3b4100.jpg'} alt="User" />
                                                        </Media>
                                                        <Media body>
                                                            <div className="d-flex mx-n2 mb-2">
                                                                <div className="px-2">
                                                                    <Media className="chat-title mb-2" heading>
                                                                        Jayne McGinnis
                                                                </Media>

                                                                    <div className="font-weight-bold">
                                                                        [Draft]
                                                                </div>
                                                                </div>
                                                                <div className="col-auto px-2 ml-auto">
                                                                    <div className="text-faded mb-1">
                                                                        Dec 16
                                                                </div>
                                                                    <div className="text-right">
                                                                        <div className="text-faded" role="button" title="Restore">
                                                                            <FontAwesomeIcon icon="history" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Media>
                                                    </Media>
                                                </li>
                                                <li className="chat-room" role="button">
                                                    <Media>
                                                        <Media className="mr-2" left href="#">
                                                            <Media className="img-circle _50x50" object src={'https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/test.a23c7b40eadb4044c04290074a9946627ff8f2d7.jpg'} alt="User" />
                                                        </Media>
                                                        <Media body>
                                                            <div className="d-flex mx-n2 mb-2">
                                                                <div className="px-2">
                                                                    <Media className="chat-title mb-2" heading>
                                                                        Sunil Wagle
                                                                </Media>
                                                                    <div className="font-weight-bold">
                                                                        [Inbox]
                                                                </div>
                                                                </div>
                                                                <div className="col-auto px-2 ml-auto">
                                                                    <div className="text-faded mb-1">
                                                                        Dec 06
                                                                </div>
                                                                    <div className="text-right">
                                                                        <div className="text-faded" role="button" title="Restore">
                                                                            <FontAwesomeIcon icon="history" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </Media>
                                                    </Media>
                                                </li>
                                            </ul>
                                        </Col>
                                        <Col md={8}>
                                            <div className="mb-2 text-dark font-weight-bold">
                                                Conversations
                                        </div>
                                            <div className="bg-white p-3 mb-2">
                                                <div className="text-center mb-2">
                                                    Jan 20, 2020
                                            </div>
                                                <ul className="list-unstyled">
                                                    <li className="chat-list">
                                                        <Media>
                                                            <Media className="mr-2" left href="#">
                                                                <Media className="img-circle _40x40" object src={'https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/1594323905763.b61278a7aaab346f8337a4cbbeded879146441a5.jpg'} alt="User" />
                                                            </Media>
                                                            <Media body>
                                                                <div className="d-flex mx-n2 mb-1">
                                                                    <Media className="chat-title px-2" heading>
                                                                        <a href="#" className="text-reset">WikiReviews Investor</a>
                                                                    </Media>
                                                                    <div className="text-faded col-auto px-2 ml-auto">
                                                                        Jan 20, 2020 8:59 AM
                                                                </div>
                                                                </div>
                                                            What just happen
                                                        </Media>
                                                        </Media>
                                                    </li>
                                                    <li className="chat-list">
                                                        <Media>
                                                            <Media className="mr-2" left href="#">
                                                                <Media className="img-circle _40x40" object src={'https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/eec181c6d57700abb2feb82d706759a8--chicago-cubs-logo-chicago-cubs-baseball.c8d294b1520442508eac3f8ebb265daf344d157e.jpg'} alt="User" />
                                                            </Media>
                                                            <Media body>
                                                                <div className="d-flex mx-n2 mb-1">
                                                                    <Media className="chat-title px-2" heading>
                                                                        <a href="#" className="text-reset">Addison Bryant</a>
                                                                    </Media>
                                                                    <div className="text-faded col-auto px-2 ml-auto">
                                                                        Jan 20, 2020 8:59 AM
                                                                </div>
                                                                </div>
                                                            Test Post
                                                        </Media>
                                                        </Media>
                                                    </li>
                                                </ul>
                                            </div>
                                        </Col>
                                    </Row>
                                </TabPane>
                            </TabContent>

                            {/* Compose Message Modal */}
                            <Modal
                                centered
                                isOpen={this.state.composeNewMessageModal}
                                toggle={() => this.setState({ composeNewMessageModal: !this.state.composeNewMessageModal })}
                            >
                                <ModalHeader className="px-0" toggle={() => this.setState({ composeNewMessageModal: !this.state.composeNewMessageModal })}>
                                    COMPOSE NEW MESSAGE
                            </ModalHeader>
                                <ModalBody>
                                    <div >
                                        <Row className="mb-3" style={{ maxHeight: '120px', overflowY: 'auto' }}>
                                            <Col md={6}>
                                                <FormGroup check>
                                                    <Label className="fs-14 text-dark font-weight-bold" check>
                                                        <Input type="checkbox" />{' '}
                                                Addison Bryant
                                            </Label>
                                                </FormGroup>
                                            </Col>
                                            <Col md={6}>
                                                <FormGroup check>
                                                    <Label className="fs-14 text-dark font-weight-bold" check>
                                                        <Input type="checkbox" />{' '}
                                                Amy Andonian
                                            </Label>
                                                </FormGroup>
                                            </Col>
                                            <Col md={6}>
                                                <FormGroup check>
                                                    <Label className="fs-14 text-dark font-weight-bold" check>
                                                        <Input type="checkbox" />{' '}
                                                Andrew Summers
                                            </Label>
                                                </FormGroup>
                                            </Col>
                                            <Col md={6}>
                                                <FormGroup check>
                                                    <Label className="fs-14 text-dark font-weight-bold" check>
                                                        <Input type="checkbox" />{' '}
                                                Darshan Shah
                                            </Label>
                                                </FormGroup>
                                            </Col>
                                            <Col md={6}>
                                                <FormGroup check>
                                                    <Label className="fs-14 text-dark font-weight-bold" check>
                                                        <Input type="checkbox" />{' '}
                                                Jayne McGinnis
                                            </Label>
                                                </FormGroup>
                                            </Col>
                                            <Col md={6}>
                                                <FormGroup check>
                                                    <Label className="fs-14 text-dark font-weight-bold" check>
                                                        <Input type="checkbox" />{' '}
                                                Leela Kuttemperoor
                                            </Label>
                                                </FormGroup>
                                            </Col>
                                            <Col md={6}>
                                                <FormGroup check>
                                                    <Label className="fs-14 text-dark font-weight-bold" check>
                                                        <Input type="checkbox" />{' '}
                                                Leela Kuttemperoor
                                            </Label>
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                    </div>
                                    <FormGroup>
                                        <Input className="primary" type="textarea" name="write_message" rows="4" placeholder="Write something..." />
                                    </FormGroup>

                                    <div className="mb-2">
                                        <span className="d-inline-block" role="button" title="Upload media">
                                            <img src={require('../../assets/images/icons/feed-cam.png')} alt="" />
                                        </span>
                                    </div>

                                    {/* Uploaded media shown here */}
                                    <div>
                                        <Row xs={2} sm={3} md={4} lg={5} form>
                                            <Col className="mb-3">
                                                <div className="d-flex pr-3 pt-3" role="button" onClick={() => this.setState({ viewMyPhotosModal: true })}>
                                                    <div>
                                                        <img className="img-fluid img-thumbnail" src={'https://stagingdatawikireviews.s3.amazonaws.com/media/content/images.82805121a31b59ba88646a19ca8044f136f8855c.png'} alt="Uploaded media" />
                                                    </div>
                                                    <div className="mx-n3 mt-n3">
                                                        <Button color="dark" size="sm" title="Remove Media"><FontAwesomeIcon icon="minus" /> </Button>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col className="mb-3">
                                                <div className="d-flex pr-3 pt-3" role="button" onClick={() => this.setState({ viewMyPhotosModal: true })}>
                                                    <div>
                                                        <img className="img-fluid img-thumbnail" src={'https://stagingdatawikireviews.s3.amazonaws.com/media/content/images.82805121a31b59ba88646a19ca8044f136f8855c.png'} alt="Uploaded media" />
                                                    </div>
                                                    <div className="mx-n3 mt-n3">
                                                        <Button color="dark" size="sm" title="Remove Media"><FontAwesomeIcon icon="minus" /> </Button>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col className="mb-3">
                                                <div className="d-flex pr-3 pt-3" role="button" onClick={() => this.setState({ viewMyPhotosModal: true })}>
                                                    <div>
                                                        <img className="img-fluid img-thumbnail" src={'https://stagingdatawikireviews.s3.amazonaws.com/media/content/images.82805121a31b59ba88646a19ca8044f136f8855c.png'} alt="Uploaded media" />
                                                    </div>
                                                    <div className="mx-n3 mt-n3">
                                                        <Button color="dark" size="sm" title="Remove Media"><FontAwesomeIcon icon="minus" /> </Button>
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </ModalBody>
                                <ModalFooter className="justify-content-start px-0">
                                    <Button
                                        color="link"
                                        className="text-white font-weight-bold text-decoration-none"
                                    >
                                        Clear
                                </Button>
                                    <Button color="primary" className="mw ml-auto">
                                        Save Draft
                                </Button>
                                    <Button color="primary" className="mw">
                                        Send
                                </Button>
                                </ModalFooter>
                            </Modal>
                        </div>
                    </Container>
                </section>
            </>
        )
    }
}

const mapState = (state) => ({
    profileData: state.user.current_user,
    get_notification: state.user.get_notification,
});

const mapProps = (dispatch) => ({
    current_user_profile: () => dispatch(current_user_profile()),
});
export default withRouter(connect(mapState, mapProps)(UserInbox));