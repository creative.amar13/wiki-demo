import React, { Component } from 'react';
import { Container, Row, Col, FormGroup, Input, Label, CustomInput, Button } from 'reactstrap';
import AppHeader from "../../app-header";
export default class SignUp extends Component {
    render() {
        return (
            <React.Fragment>
                <AppHeader />
                <section className="py-4 mt-5">
                    <Container>
                        <Row className="justify-content-center">
                            <Col lg={8}>
                                <div className="bg-white p-3">
                                    <div className="text-dark font-weight-bold fs-18 mb-3">Administrator Info</div>
                                    <Row xs={1} md={2} form>
                                        <Col>
                                            <FormGroup>
                                                <Label className="text-dark font-weight-normal">First Name</Label>
                                                <Input className="primary" type="text" />
                                            </FormGroup>
                                        </Col>
                                        <Col>
                                            <FormGroup>
                                                <Label className="text-dark font-weight-normal">Last Name</Label>
                                                <Input className="primary" type="text" />
                                            </FormGroup>
                                        </Col>
                                        <Col>
                                            <FormGroup>
                                                <Label className="text-dark font-weight-normal">Email Address</Label>
                                                <Input className="primary" type="email" />
                                            </FormGroup>
                                        </Col>
                                        <Col lg={12}>
                                            <FormGroup>
                                                <Label className="text-dark font-weight-normal">Phone Number</Label>
                                                <div className="d-flex mx-n2">
                                                    <div className="col-sm-3 px-2">
                                                        <Input className="primary" type="number" placeholder="(555)" />
                                                    </div>
                                                    <div className="col-sm-3 px-2">
                                                        <Input className="primary" type="number" placeholder="555" />
                                                    </div>
                                                    <div className="col-sm-6 px-2">
                                                        <Input className="primary" type="number" placeholder="5555" />
                                                    </div>
                                                </div>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row form>
                                        <Label lg={6}>Company Name</Label>
                                        <Col lg={6}>
                                            <FormGroup>
                                                <Input className="primary" type="text" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row form>
                                        <Label lg={6}>Add Listings from search or Excel file?</Label>
                                        <Col lg={6}>
                                            <FormGroup>
                                                <Input className="primary" type="select">
                                                    <option>Search</option>
                                                    <option>Excel File</option>
                                                </Input>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <hr />
                                    <Row form>
                                        <Label xs={12}>Search for a Business</Label>
                                        <Col xs={12}>
                                            <FormGroup>
                                                <Input className="primary" type="search" />
                                                <div className="shadow-sm" style={{ maxHeight: "400px", overflowY: "auto" }}>
                                                    <ul className="list-unstyled mb-2 p-2">
                                                        <li className="d-flex">
                                                            <CustomInput
                                                                className="mr-2"
                                                                type="checkbox"
                                                                id={"branch_" + 1}
                                                                name="select_multiple_companies"
                                                            />
                                                            <Label
                                                                for={"branch_" + 1}
                                                                className="flex-fill fs-14 text-dark font-weight-normal"
                                                            >
                                                                Company Branch name 1
                                        </Label>
                                                        </li>
                                                        <li className="d-flex">
                                                            <CustomInput
                                                                className="mr-2"
                                                                type="checkbox"
                                                                id={"branch_" + 2}
                                                                name="select_multiple_companies"
                                                            />
                                                            <Label
                                                                for={"branch_" + 2}
                                                                className="flex-fill fs-14 text-dark font-weight-normal"
                                                            >
                                                                Company Branch name 2
                                        </Label>
                                                        </li>
                                                        <li className="d-flex">
                                                            <CustomInput
                                                                className="mr-2"
                                                                type="checkbox"
                                                                id={"branch_" + 3}
                                                                name="select_multiple_companies"
                                                            />
                                                            <Label
                                                                for={"branch_" + 3}
                                                                className="flex-fill fs-14 text-dark font-weight-normal"
                                                            >
                                                                Company Branch name 3
                                        </Label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <div className="text-right">
                                        <Button color="primary">Submit</Button>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </React.Fragment>
        )
    }
}
