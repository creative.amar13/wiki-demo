import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { Button, Container, Row, Col, ButtonGroup, CardBody, Card, Nav, NavItem, NavLink, TabContent, TabPane, Badge, } from "reactstrap";
import AppHeader from "../../app-header";
import classnames from "classnames";
import Chart from "react-google-charts";
import Map from "./../oragnisms/map";
import {
  get_chart_stats,
  get_branch_count_data,
  post_business_update,
  corporate_id,
  corporate_id_details,
  current_user_profile,
  get_user_role_and_permissions,
  getallbranch,
  get_admin_stats
} from "../../actions/user";
import { get_roles_list } from '../../actions/admin-view/admin';
import {
  get_review_count,
  get_message_count,
  get_feedback_count,
  get_qa_count
} from '../../actions/counts';
import { connect } from "react-redux";
import ReactJoyride, { CallBackProps, STATUS } from "react-joyride";
import SideBar from "./../oragnisms/sidebar";
import OverallData from "./../molecules/oerallInfo";
import Feed from "./../molecules/tabs/feed";
import MyPosts from "./../molecules/tabs/myPosts";
import Reviews from "./../molecules/tabs/reviews";
import Messages from "./../molecules/tabs/messages";
import MessagesNewDesign from "./../molecules/tabs/messagesNewDesign";
import Feedback from "./../molecules/tabs/feedback";
import MediaTab from "./../molecules/tabs/media";
import QA from "./../molecules/tabs/qa";
import Employees from "./../molecules/tabs/employees";
import AccountSettings from "./../molecules/tabs/accountSettings";
// import Loader from "react-loader-spinner";
import { loaderExludedRoutes } from "../../router";
import moment from "moment";
import Loaderimg from "./../../assets/images/w-brand-black.jpg";
import { act } from "react-dom/test-utils";

class Dashboard extends Component {
  mapRef = React.createRef();

  constructor(props) {
    super(props);

    // history.replace('', null);

    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    document.body.scrollTop = 0; // For Safari
    this.state = {
      dotSelectedType: '',
      dotSelectedColor: '',
      dotSelectedName: '',
      dotSelectedDays: '',
      dotSelectedqueryType: '',

      assignedUserRoles: null,
      isMapListCollapseOpen: true,
      activeTab: "1",
      mapBranchClickData: false,
      mapBranchClickDataCount: '',
      branchMessageCount: '',
      statView: "30_days",
      lng: -129.891,
      lat: 35.7608,
      zoom: 3.6,
      addmodal: false,
      menuItemsModal: false,
      newServiceItemModal: false,
      isOpen: false,
      map_box_item: [],
      map_cordinates: null,
      profileData: null,
      userData: null,
      position: null,
      corporateID: null,
      phoneDiary: [],
      AdressDiary: [],
      webSet: [],
      emailSet: [],
      payOptions: [],
      categoryList: [],
      HoursOfOperation: [],
      chartStats: [],
      branchCount: [],
      grapghViewStat: "day_graph_stat",
      profession_data: null,
      currentRolePermission: null,
      isLoader: "false",
      run: false,
      tabName: null,
      steps: [
        {
          target: ".step-1",
          title: `Hello ${this.props.profileData && this.props.profileData.user && this.props.profileData.user.first_name !== null ? this.props.profileData.user.first_name : 'xyz'} and welcome to WikiReviews!`,
          content:
            "This is a brief tour that will show you how to setup your corporate account. This includes how to set up users, roles and branches.  You will also learn how to fill out company information so it is properly displayed for each location.  Finally, you will learn how to create posts, engage with your customers and respond to and dispute reviews.",
          disableBeacon: true,
          disableOverlayClose: true,
          hideCloseButton: true,
          placement: 'center',
          spotlightClicks: true,
          styles: {
            options: {
              zIndex: 1000,
            },
          },
        },
        {
          target: ".step-2",
          title: 'Statistics',
          content:
            "You can easily see a dashboard summary of all the locations monthly statistics as well as the number of (put in red symbol) overdue, (put in yellow symbol) pending and (put in green symbol) completed tasks.",
          placement: "right",
          disableBeacon: true,
        },
        {
          target: ".step-3",
          content:
            "Click on colored circles and tabs with numbers to filter the results.",
          placement: "center",
        },
        {
          target: ".step-4",
          content:
            "Modify the dashboard statistics by time frame by using the pulldowns here",
          placement: "left",
        },
        {
          target: ".step-5",
          content:
            "The listing owner guide is a quick reference guide to use at any time.  This is simply a template which can be used or it can be edited to suit your needs.",
          placement: "left",
        },
        // {
        //   target: ".step-6",
        //   content:
        //     "The listing owner guide is a quick reference guide to use at any time.  This is simply a template which can be used or it can be edited to suit your needs.",
        //   placement: "left",
        // },
        {
          target: ".step-7",
          content:
            "Search here for a specific branch by city, state or zip or by customer service representative name.  This search will filter results on the map as well as all the data below the map.",
          placement: "top",
        },
        {
          target: ".step-8",
          content:
            "The information shown here can be filtered by the search box above.",
          placement: "top",
        },
        // {
        //   target: ".step-9",
        //   content:
        //     "The data shown here reflects the filters from the search box.",
        //   placement: "top",
        // },
        {
          target: ".step-10",
          content:
            "The data shown here reflects the filters from the search box.",
          placement: "left",
        },
        {
          target: ".step-11",
          content:
            "From here you can add or remove employees who will be responding to customer messages, reviews and inquiries.  Once an employee’s email is added, they will receive an emailed link to create their own password.  You will also assign each user to a specific role which gives users permissions to do various tasks.",
          placement: "left",
        },
        {
          target: ".step-12",
          content:
            "Here you can assign the branch location(s) to a specific employee and even assign backup branches to employees in case one is sick.",
          placement: "left",
        },
      ],
      corporateReviewCount: null,
      contribution_count: null,
      feedback_counts: null,
      qa_counts: null,
      review_counts: null,
      statistics_owner_tab_count: null,
    };
  }

  handleClick = (e) => {
    e.preventDefault();
    this.setState({ run: true, });
  };

  componentWillReceiveProps(nextProps) {
    const { statistics_owner_tab_count, userRoles, } = nextProps;
    //get stat count of tabs
    if (statistics_owner_tab_count !== null && Object.keys(statistics_owner_tab_count).length > 0) {
      this.setState({ statistics_owner_tab_count: statistics_owner_tab_count });
      statistics_owner_tab_count.map(async (element) => {
        await this.setState({
          contribution_count: element && element.contribution_count,
          feedback_counts: element && element.feedback_counts,
          qa_counts: element && element.qa_counts,
          review_counts: element && element.review_counts,
        });
      });
    }
    if (userRoles) {
      this.setState({
        assignedUserRoles: userRoles
      })
    }
    if (nextProps.current_role_and_permissions) {
      this.setState({
        currentRolePermission: nextProps.current_role_and_permissions
      });
    }


    // get_chart_stats
    if (Array.isArray(nextProps.chart_stats) && nextProps.chart_stats.length > 0) {
      this.setState({ chartStats: nextProps.chart_stats, statView: "30_days", grapghViewStat: "day_graph_stat", });
    }

    if (nextProps.branch_count && Object.keys(nextProps.branch_count).length > 0) {
      this.setState({ branchCount: nextProps.branch_count });
    }

    if (nextProps.corporate_review_count && Object.keys(nextProps.corporate_review_count).length > 0) {
      this.setState({ corporateReviewCount: nextProps.corporate_review_count });
    }
  }

  componentWillUnmount() {
    clearInterval(this.intervalCheck);
  }

  UNSAFE_componentWillMount() {
    window.scrollTo(0, 0);
  }

  loaderInterval = () => {
    let this_keep = this;

    if (!loaderExludedRoutes.includes(this.props.history.location.pathname)) {
      this.intervalCheck = setInterval(() => {
        let isRedirectTrue = localStorage.getItem('redirect');

        if (isRedirectTrue == "true") {
          this.props.history.push('/corporatesettings');
          localStorage.removeItem('redirect');
        }

        let { isLoader } = this_keep.state;
        let localValue = localStorage.getItem("loader");
        if (isLoader !== localValue) {
          this_keep.setState({ isLoader: localStorage.getItem("loader") });
        }
      }, 1500);
    }

    if (loaderExludedRoutes.includes(this.props.history.location.pathname)) {
      clearInterval(this.intervalCheck);
    }
  };

  componentDidMount() {
    this.fetchRequests();
    let getBd = document.getElementsByTagName("BODY")[0];
    getBd.className = "";
    getBd.style.paddingRight = 0;
    this.loaderInterval();
    // this.props.get_corporate_review_count();
    // this.props.get_owner_message_count();
    // this.props.get_owner_feedback_count();
    // this.props.get_owner_qa_count();
    if (this.props.branch_id === null) {
      Promise.all([
        this.props.get_review_count(),
        this.props.get_message_count(),
        this.props.get_qa_count(),
        this.props.get_feedback_count(),
        this.props.getallbranch()
      ]).then(() => {
        if (this?.props?.history?.location?.state?.tour == true) {
          this.props.history.replace('', null);
          this.props.clickTourStart();
        }
      })
    }
  }

  fetchRequests = () => {
    let { currentRolePermission } = this.state;
    if (currentRolePermission == null) {
      this.props.get_user_role_and_permissions();
    }
    this.props.get_corporate_id();
    this.props.get_chart_stats();
    this.props.current_user_profile();
  };

  componentDidUpdate(prevProps) {
    if (prevProps.isAdmin !== this.props.isAdmin) {
      let userRoleType = this.props.isAdmin ? 'admin' : 'nonadmin';
      this.props.get_branch_count_data(userRoleType);
      if (userRoleType === 'admin') {
        this.props.get_roles_list();
      }
    }

    if (prevProps.branchMessageCount !== this.props.branchMessageCount) {
      this.setState({
        branchMessageCount: this.props.branchMessageCount
      });
    }
  }
  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({ activeTab: tab });
    }
  };

  statsContribution = (isTrue) => {
    let { chartStats } = this.state;
    let result = [];
    if (chartStats && chartStats[0] && chartStats[0].contribution_stats) {
      let contributionStats = chartStats[0].contribution_stats;
      contributionStats.forEach((item) => {
        if (isTrue) {
          result.push({ name: item[0], y: item[1] });
        } else {
          result.push({ name: item[0], data: [item[1]] });
        }
      });
    }
    return result;
  };

  ageGraph = (isTrue) => {
    let { chartStats } = this.state;
    let result = [];
    if (chartStats && chartStats[0] && chartStats[0].age_graph_stat) {
      let ageGraphStat = chartStats[0].age_graph_stat;
      ageGraphStat.forEach((item) => {
        if (item[0] !== null) {
          if (isTrue) {
            result.push(item[0]);
          } else {
            result.push(item[1]);
          }
        }
      });
    }
    return result;
  };

  handlePageViews = (toFromDate) => {
    let { chartStats, statView } = this.state;
    let pageViews = "";

    const fromDate = toFromDate[0];
    const toDate = toFromDate[toFromDate.length - 1];

    if (chartStats && chartStats[0] && chartStats[0].page_views) {
      let pageViewStat = chartStats[0].page_views;
      switch (statView) {
        case "30_days":
          pageViews = pageViewStat["30_days"];
          // this.setState({ grapghViewStat: 'day_graph_stat' })
          break;
        case "12_months":
          pageViews = pageViewStat["12_months"];
          // this.setState({ grapghViewStat: 'monthly_graph_stat' })
          break;
        case "24_months":
          pageViews = pageViewStat["24_months"];
          // this.setState({ grapghViewStat: 'yearly_graph_stat' })
          break;
        default:
          break;
      }
    }

    let from_date = "";
    let to_date = "";
    if (statView == "30_days") {
      let isTrue = moment(fromDate, "MM-DD-YYYY", true).isValid();
      if (isTrue) {
        from_date = fromDate
          ? moment(fromDate, "MM-DD-YYYY").format("MMMM DD, YYYY")
          : "";
        to_date = toDate
          ? moment(toDate, "MM-DD-YYYY").format("MMMM DD, YYYY")
          : "";
      } else {
        from_date = fromDate
          ? moment(fromDate, "YYYY-DD-MM").format("MMMM DD, YYYY")
          : "";
        to_date = toDate
          ? moment(toDate, "YYYY-DD-MM").format("MMMM DD, YYYY")
          : "";
      }
    } else {
      from_date = fromDate;
      to_date = toDate;
    }

    return (
      <div className="d-sm-flex mb-4">
        <div className="mr-2">
          <h2 className="text-secondary-dark">{pageViews} Page Views</h2>
        </div>
        <div className="ml-auto align-self-end">
          <div className="text-secondary fs-14">
            <span>from</span>
            <span className="font-weight-semi-bold mx-1">{from_date}</span>
            <span>to</span>
            <span className="font-weight-semi-bold mx-1">{to_date}</span>
          </div>
        </div>
      </div>
    );
  };

  handleHighestLowestPageView = (array2, array1) => {
    const highestPageView = array2 && array2.length > 0 ? Math.max.apply(null, array2) : 0;
    const lowestPageView = array2 && array2.length > 0 ? Math.min.apply(null, array2) : 0;
    const indexOfLowestValue = array2 && array2.length > 0 ? array2.indexOf(Math.min.apply(null, array2)) : 0;
    const lowestPageViewDate = array1 && array1.length > 0 ? array1[indexOfLowestValue] : 0;
    return (
      <div className="d-sm-flex mb-4">
        <div className="ml-auto align-self-end">
          <div className="text-secondary fs-14">
            <span>Highest:</span>
            <span className="font-weight-semi-bold mx-1">
              {highestPageView} views/day
            </span>
            <span>&nbsp; Lowest:</span>
            <span className="font-weight-semi-bold mx-1">
              {lowestPageViewDate} {lowestPageView} views/day
            </span>
          </div>
        </div>
      </div>
    );
  };

  checkIsLoader = (isLoader) => {
    if (isLoader && isLoader == "true") {
      return (
        <div className="loader_div">
          <div className="inner-loader">
            <img src={Loaderimg} alt="" />
          </div>
        </div>
      );
    } else {
      return null;
    }
  };

  renderSvg = () => {
    return (
      <svg
        role="img"
        alt="Usersnap"
        aria-label="Usersnap"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 140 140"
        style={{ fill: "currentColor", pointerEvents: "none", height: "18px" }}>
        <path
          d="M136.9,53.5C127.9,16,90.5-6.2,53.6,2.7C35.6,7.1,20.2,18.1,10.8,34S-1.4,68.5,3.1,86.4c7.3,31.3,35.8,52.9,66.7,52.9
        c5.7,0,11.4-0.8,16.2-2.1C123.5,127.5,146.2,90.5,136.9,53.5z M40.5,118.1C13.7,101.9,5.2,66.9,21,39.7c7.7-13,20.7-22.4,34.9-25.6
        c4.5-1.2,8.9-1.6,13.9-2.1c31.3,0,57.3,25.6,57.3,57.3c0,26-17.9,48.8-43.6,55.3C69,128.7,53.1,126.7,40.5,118.1z M107.2,59.2
        c-1.6-3.7-6.9-3.3-7.7,0.4c-6.9,20.3-29.7,30.5-49.6,23.6c-3.7-1.6-7.3-3.3-11-6.1c-1.6-1.2-3.7-1.2-4.9,0.8
        c-0.8,0.8-0.8,2.1-0.8,2.5c0,2.5,0.8,4.5,1.6,6.9c6.5,17.5,25.2,28.5,43.6,25.2c20.7-3.7,34.9-24,30.9-45.1c0-0.8-0.4-1.6-0.4-2.5
        C108,62.9,107.6,61.2,107.2,59.2z"
        ></path>
      </svg>
    );
  };

  setTabReviews = (TabNo = "4", mapBranchClickDataProp, count) => {
    if (mapBranchClickDataProp === true && TabNo != 1) {
      this.setState({ activeTab: TabNo, mapBranchClickData: mapBranchClickDataProp, mapBranchClickDataCount: count });
    } else {
      this.setState({ activeTab: TabNo });
    }
  };
  handleMapMarkerClick = async () => {
    await this.setState({ activeTab: '1' });
  };

  renderActiveItems = ({ active, renderItem }) => {
    let { activeTab } = this.state;
    if (activeTab == active) {

      return (
        (<TabPane tabId={active} >
          {renderItem}
        </TabPane>)
      )
    }
    return null;
  }

  handleTabName = async (title) => {
    await this.setState({
      tabName: title
    });
  }

  renderStatsTab = ({ array1, array2, newArray, ageGraphData, pageViewsGraph, genderGraphData }) => {
    return (
      <div className="text-secondary">
        <div className="mb-3">
          <ButtonGroup className="type-filter flex-wrap" size="sm">
            <div className="item d-flex align-items-center">
              <Button
                color="transparent"
                onClick={() => this.setState({ statView: "30_days", grapghViewStat: "day_graph_stat", })}
                active={this.state.statView === "30_days"}>
                {'30 Days'}
              </Button>
            </div>
            <div className="item d-flex align-items-center">
              <Button
                color="transparent"
                onClick={() =>
                  this.setState({ statView: "12_months", grapghViewStat: "yearly_graph_stat", })}
                active={this.state.statView === "12_months"}>
                {'12 Months'}
              </Button>
            </div>
            <div className="item d-flex align-items-center">
              <Button
                color="transparent"
                onClick={() => this.setState({ statView: "24_months", grapghViewStat: "monthly_graph_stat", })}
                active={this.state.statView === "24_months"}>
                {'24 Months'}
              </Button>
            </div>
          </ButtonGroup>
        </div>

        <div className="bg-white p-3">
          {this.handlePageViews(array1)} {'Page Views'}
          <hr />
          <div>
            <Card className="shadow-sm">
              <CardBody>
                <Chart
                  width={"100%"}
                  height={"400px"}
                  chartType="LineChart"
                  loader={<div>Loading Chart</div>}
                  data={pageViewsGraph}
                  options={{
                    vAxis: {
                      ticks: [0, 50, 150, 250, 350, 450, 600],
                      viewWindowMode: "explicit",
                      viewWindow: {
                        min: 0,
                      },
                      minValue: 0,
                    },
                    chartArea: { width: "80%" },
                    legend: { position: "top" },
                  }}
                />
              </CardBody>
              {this.handleHighestLowestPageView(array2, array1)}
            </Card>
          </div>
          <hr />
          <Row>
            <Col xs={12}>
              <h4 className="ff-base fs-18 mb-3">
                Category Statistics
            </h4>
            </Col>
            <Col xl={6}>
              <Card className="mb-3 shadow-sm">
                <CardBody>
                  <Chart
                    chartType="BarChart"
                    loader={<div>Loading Chart</div>}
                    data={newArray}
                    options={{
                      title:
                        "Distribution of Approvals/Reverts",
                      legend: { position: "none" },
                    }}
                  />
                </CardBody>
              </Card>
            </Col>
            <Col xl={6}>
              <Card className="shadow-sm">
                <CardBody>
                  <Chart
                    chartType="PieChart"
                    loader={<div>Loading Chart</div>}
                    data={newArray}
                    options={{
                      title:
                        "Distribution of Approvals/Reverts",
                      legend: { position: "top" },
                    }}
                    rootProps={{ "data-testid": "1" }}
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
          <hr />
          <Row>
            <Col xs={12}>
              <h4 className="ff-base fs-18 mb-3">
                {"Viewers Statistics"}
              </h4>
            </Col>
            <Col xl={6}>
              <Card className="shadow-sm mb-3">
                <CardBody>
                  <Chart
                    chartType="LineChart"
                    loader={<div>Loading Chart</div>}
                    data={ageGraphData}
                    options={{
                      vAxis: {
                        viewWindowMode: "explicit",
                        viewWindow: {
                          min: 0,
                        },
                        minValue: 0,
                      },
                      title: "Age Graph",
                      legend: { position: "top" },
                    }}
                  />
                </CardBody>
              </Card>
            </Col>
            <Col xl={6}>
              <Card className="shadow-sm">
                <CardBody>
                  <Chart
                    chartType="PieChart"
                    loader={<div>Loading Chart</div>}
                    data={genderGraphData}
                    options={{
                      vAxis: {
                        viewWindowMode: "explicit",
                        viewWindow: {
                          min: 0,
                        },
                        minValue: 0,
                      },
                      title: "Gender Graph",
                      legend: { position: "top" },
                    }}
                    rootProps={{ "data-testid": "1" }}
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    )
  }

  handleJoyrideCallback = (CallBackProps) => {
    const { status, type } = CallBackProps;
    const finishedStatuses = [STATUS.FINISHED, STATUS.SKIPPED];

    if (finishedStatuses.includes(status)) {
      this.setState({ run: false });
    }

  };
  renderReactJoyRide = ({ Tooltip }) => {
    return (
      <ReactJoyride
        callback={this.handleJoyrideCallback}
        steps={this.state.steps}
        run={this.state.run}
        continuous
        tooltipComponent={Tooltip}
        showProgress
        hideBackButton={false}
        disableOverlayClose={true}
        locale={{ close: 'Close' }}
        styles={{
          options: {
            arrowColor: "#fff",
            backgroundColor: "#fff",
            overlayColor: "rgba(0, 0, 0, 0.6)",
            primaryColor: "mediumaquamarine",
            textColor: "#333",
            zIndex: 10000,
          },
        }}
      />
    )
  }

  renderNavTabs = ({ active, title, count, isAdmin }) => {
    return (
      <NavItem>
        <NavLink
          className={classnames({ active: this.state.activeTab === active })}
          onClick={() => {
            this.toggle(active);
            this.handleTabName(title);
          }}>
          <div className="d-flex align-items-center">
            <div>
              {title}
            </div>
            <div>
              {count && count > 0 ? <Badge className="ml-1" color="primary"> {count}</Badge> : null}
            </div>
          </div>
        </NavLink>
      </NavItem>
    )
  }


  handleSetDotTypeColor = (type, color, tabName, stats_duration, queryType) => {
    this.setState(
      {
        dotSelectedType: type,
        dotSelectedColor: color,
        dotSelectedName: tabName,
        dotSelectedDays: stats_duration,
        dotSelectedqueryType: queryType,
      })
  }
  render() {
    let { branchCount, chartStats, currentRolePermission, isLoader } = this.state;
    const { profileData } = this.props;
    let dailyGraph = [];
    let businesOwner = profileData?.business_owner === true ? true : false
    if (this.state.grapghViewStat === "day_graph_stat") {
      dailyGraph = chartStats && chartStats[0] && chartStats[0].day_graph_stat;
    } else if (this.state.grapghViewStat === "monthly_graph_stat") {
      dailyGraph =
        chartStats && chartStats[0] && chartStats[0].monthly_graph_stat;
    } else if (this.state.grapghViewStat === "yearly_graph_stat") {
      dailyGraph =
        chartStats && chartStats[0] && chartStats[0].yearly_graph_stat;
    }

    let xaxis = [];
    let data = [];
    xaxis = dailyGraph && Object.keys(dailyGraph).map((element) => {
      return dailyGraph[element].length > 0 ? dailyGraph[element][0] : true;
    });

    const pageViewsGraph = dailyGraph && Object.keys(dailyGraph).map((element) => {
      return dailyGraph[element].length > 0
        ? [
          dailyGraph[element][0],
          dailyGraph[element][1],
          dailyGraph[element][2],
          dailyGraph[element][3],
        ]
        : true;
    });

    pageViewsGraph && pageViewsGraph.splice(0, 0, ["Time", "Pageviews", "Recommend List", "Want List",]);
    const contributionGraph = chartStats && chartStats[0] && chartStats[0].contribution_stats;

    let newArray = [];

    if (contributionGraph !== undefined && contributionGraph.length === 3) {
      newArray = [["Contribution", "Stat"]].concat(contributionGraph);
      // [contributionGraph].forEach(item => filterItems.push(item));

      // let newContributionGraph = contributionGraph;
      // newArray = newContributionGraph.splice(0, 0, ["Contribution", "Stat"]);
    } else {
      newArray = contributionGraph;
    }

    const ageGraphData = chartStats && chartStats[0] && chartStats[0].age_graph_stat;
    let newAgeGraphArray = [];

    if (ageGraphData !== undefined && ageGraphData.length === 5) {
      let newAgeGraph = ageGraphData;
      newAgeGraphArray = newAgeGraph.splice(0, 0, ["Age-Group", "Age"]);
    } else {
      newAgeGraphArray = ageGraphData;
    }

    const genderGraphData = chartStats && chartStats[0] && chartStats[0].gender_graph_stat;
    let newGenderGraphArray = [];

    if (genderGraphData !== undefined && genderGraphData.length === 2) {
      let newAgeGraph = genderGraphData;
      newGenderGraphArray = newAgeGraph.splice(0, 0, ["Gender", "Age"]);
    } else {
      newGenderGraphArray = genderGraphData;
    }


    let array1 = [];
    let array2 = [];
    let array3 = [];
    let array4 = [];

    data = dailyGraph && Object.keys(dailyGraph).map((element) => {
      dailyGraph[element].forEach((e, i) => {
        if (i === 0) {
          array1.push(e);
        }
        if (i === 1) {
          array2.push(e);
        }
        if (i === 2) {
          array3.push(e);
        }
        if (i === 3) {
          array4.push(e);
        }
      });
    });

    const Tooltip = ({
      continuous,
      index,
      isLastStep,
      step,
      backProps,
      closeProps,
      primaryProps,
      skipProps,
      tooltipProps, }) => {

      if (index === "2" || index === 2) {

        return (<div {...tooltipProps}>
          {/* {step.title && <span>{step.title}</span>} */}
          {/* <span className="description">{step.content}</span> */}
          <div>
            <div className="stat-bubble with-line" data-type="overall">
              &nbsp;
                  </div>
            <div className="stat-bubble with-line" data-type="reviews">
              &nbsp;
                  </div>
            <div className="stat-bubble with-line" data-type="branches">
              &nbsp;
                  </div>
          </div>
          <div className="intro-block" data-type="stat-intro">
            {step.content}
          </div>

          {continuous && (
            <Button {...primaryProps} color="primary" className="bt-btn">
              {'Got It'}
            </Button>
          )}
          {/* <div>
            {index > 0 && (
              <Button {...backProps}>
                {'1'}
              </Button>
            )}
            {continuous && (
              <Button {...primaryProps}>
                {'2'}
              </Button>
            )}
            {!continuous && (
              <Button {...closeProps}>
                {'3'}
              </Button>
            )}
          </div> */}
        </div>)

      }
      else {
        return (
          <div className="cs-toolpop" {...tooltipProps}>
            {step.title && <h3>{step.title}</h3>}
            {step.content && <div>{step.content}

            </div>}
            <div className="d-flex mt-4 mb-3">

              {/* {!isLastStep || index === 0 ? <Button {...skipProps} spacer={true}>
              Skipasdas
              </Button> : ""
            } */}
              {/* {index > 0 && (
              <Button {...backProps}>
                Back
              </Button>
            )} */}
              <Button {...primaryProps} color="primary">
                Ok
            </Button>
              <Button {...closeProps} color="link" className="ml-auto" onClick={() => {
                this.setState({
                  run: false
                })
              }}>
                End Tour
            </Button>
            </div>
            {index === 0 ?
              <Link
                to="/corporateprofile"
                onClick={() => {
                  this.setState({
                    run: false
                  })
                }}
              >
                Skip this part and I’ll explore on my own
              </Link> :
              ""}

          </div>

        )
      }
    };


    const { feedback_count, qa_count, review_count, message_count, branch_id } = this.props;
    return (
      <React.Fragment>
        {this.renderReactJoyRide({ Tooltip })}
        {this.checkIsLoader(isLoader)}
        {/* <button onClick={this.handleClick} class="fixed-button-right">{'test'}</button> */}
        {/* <button className="fixed-button-right"><span className="mr-2">{this.renderSvg()}</span>{"Report Issues"}</button> */}
        <AppHeader clickTourStart={this.props.clickTourStart} />
        <div className="user-main tour1">
          <OverallData
            setTabReviews={this.setTabReviews}
            handleSetDotTypeColor={this.handleSetDotTypeColor}
          />
          <Map
            setTabReviews={this.setTabReviews}
            handleMapMarkerClick={this.handleMapMarkerClick}
            branchCount={branchCount}
            assignedUserRoles={this.state.assignedUserRoles}
            dotSelectedType={this.state.dotSelectedType}
            dotSelectedColor={this.state.dotSelectedColor}
            dotSelectedName={this.state.dotSelectedName}
            dotSelectedDays={this.state.dotSelectedDays}
            dotSelectedqueryType={this.state.dotSelectedqueryType}

          />
          <section>
            <Container className="mt-20">
              <Row id="scrollTillTab">
                <Col md="4">
                  <SideBar
                    post_business_update={this.props.post_business_update}
                    business_update={this.props.business_update}
                    profile_data={this.props.profession_data}
                  />
                  {/* Business Info Collapse */}
                </Col>
                <Col md="8">
                  <div>
                    <Nav tabs className="main-tabs">
                      {this.renderNavTabs({ active: "1", title: 'Stats', count: 0 })}
                      {this.renderNavTabs({ active: "2", title: 'Feed', count: 0 })}
                      {this.renderNavTabs({ active: "3", title: 'My Posts', count: 0 })}
                      {this.renderNavTabs({
                        active: "4",
                        title: 'Reviews',
                        count: branch_id !== null ?
                          this.state.review_counts :
                          review_count.reviews_count
                      })}
                      {this.renderNavTabs({
                        active: "5",
                        title: 'Messages',
                        // count: this.state.mapBranchClickData ? this.state.mapBranchClickDataCount : message_count.total_count
                        count: this.state.branchMessageCount?.total_count ? this.state.branchMessageCount.total_count : message_count.total_count
                        // count: this.state.mapBranchClickData ? this.state.mapBranchClickDataCount : ''
                      })}
                      {this.renderNavTabs({
                        active: "6",
                        title: 'Feedback',
                        count: branch_id !== null ?
                          this.state.feedback_counts :
                          `${feedback_count.feedback_answered_count + feedback_count.feedback_pending_count}`
                      })}
                      {this.renderNavTabs({
                        active: "7",
                        title: 'Q&A',
                        count: branch_id !== null ?
                          this.state.qa_counts :
                          qa_count.total_count
                      })}
                      {this.renderNavTabs({ active: "10", title: 'Media', count: 0, isAdmin: currentRolePermission && currentRolePermission.role == "" })}
                      {this.renderNavTabs({ active: "8", title: 'Employees', count: 0 })}
                      {this.renderNavTabs({ active: "9", title: 'Account Settings', count: 0 })}
                    </Nav>
                    <TabContent activeTab={this.state.activeTab}>
                      {this.renderActiveItems({ active: '1', renderItem: (this.renderStatsTab({ array1, array2, newArray, ageGraphData, pageViewsGraph, genderGraphData })) })}
                      {this.renderActiveItems({ active: '2', renderItem: (<Feed />) })}
                      {this.renderActiveItems({ active: '3', renderItem: (<MyPosts corporateId={this.props.corporate_details?.id} />) })}
                      {this.renderActiveItems({
                        active: '4',
                        renderItem: (<Reviews
                          get_admin_stats={this.props.get_admin_stats}
                          mapBranchClickData={this.state.mapBranchClickData}
                          dotSelectedType='new'
                          dotSelectedColor={this.state.dotSelectedColor}
                          dotSelectedName='reviews'
                          dotSelectedDays={this.state.dotSelectedDays}
                          dotSelectedqueryType='corporatereviewsnew'
                          dotSelectedBranchId={this.props.branch_id}
                        />)
                      })}
                      {/* {this.renderActiveItems({ active: '5', renderItem: (<Messages />) })} */}
                      {this.renderActiveItems({
                        active: '5',
                        renderItem: (
                          <MessagesNewDesign
                            get_admin_stats={this.props.get_admin_stats}
                            businesOwner={businesOwner}
                            mapBranchClickData={this.state.mapBranchClickData}
                            dotSelectedType='message'
                            dotSelectedColor={this.state.dotSelectedColor}
                            dotSelectedName='message'
                            dotSelectedDays={this.state.dotSelectedDays}
                            dotSelectedqueryType='corporatemessage'
                            dotSelectedBranchId={this.props.branch_id}
                          />
                        )
                      })}
                      {this.renderActiveItems({
                        active: '6',
                        renderItem: (<Feedback
                          get_admin_stats={this.props.get_admin_stats}
                          tabName={this.state.tabName}
                          mapBranchClickData={this.state.mapBranchClickData}
                          dotSelectedType='pending'
                          dotSelectedColor={this.state.dotSelectedColor}
                          dotSelectedName='feedback'
                          dotSelectedDays={this.state.dotSelectedDays}
                          dotSelectedqueryType='corporatefeedbackcount'
                          dotSelectedBranchId={this.props.branch_id}
                        />)
                      })}
                      {this.renderActiveItems({
                        active: '7',
                        renderItem: (
                          <QA
                            get_admin_stats={this.props.get_admin_stats}
                            tabName={this.state.tabName}
                            mapBranchClickData={this.state.mapBranchClickData}
                            dotSelectedType='pending'
                            dotSelectedColor={this.state.dotSelectedColor}
                            dotSelectedName='qa'
                            dotSelectedDays={this.state.dotSelectedDays}
                            dotSelectedqueryType='corporatequestions'
                            dotSelectedBranchId={this.props.branch_id}
                          />
                        )
                      })}
                      {this.renderActiveItems({ active: '8', renderItem: (<Employees />) })}
                      {this.renderActiveItems({ active: '9', renderItem: (<AccountSettings />) })}
                      {this.renderActiveItems({ active: '10', renderItem: (<MediaTab />) })}
                    </TabContent>
                  </div>
                </Col>
              </Row>
            </Container>
          </section>
        </div>
      </React.Fragment>
    );
  }
}

const mapState = (state) => {
  return {
    profileData: state.user.current_user,
    chart_stats: state.user.chart_stats,
    branch_count: state.user.branch_count,
    business_update: state.user.business_update,
    corporate_id: state.user.corporate_id,
    corporate_details: state.user.corporate_id_details,
    profession_data: state.user.employee_list,
    current_role_and_permissions: state.user.current_role_and_permissions,
    corporate_review_count: state.user.corporate_review_count,
    statistics_owner_tab_count: state.user.statistics_owner_tab_count,
    feedback_count: state.count.feedback,
    review_count: state.count.review,
    qa_count: state.count.qa,
    message_count: state.count.message,
    isAdmin: state.user.isAdmin,
    // owner_message_count: state.user.owner_message_count,
    // owner_qa_count: state.user.owner_qa_count,
    // owner_feedback_count: state.user.owner_feedback_count,
    // branch_message_count: state.user.branch_message_count,
    branch_id: state.user.branch_id,
    userRoles: state.admin.roles_list,
    branchMessageCount: state.user.branch_message_count,
  };
};

const mapProps = (dispatch) => ({
  get_admin_stats: () => dispatch(get_admin_stats()),
  get_user_role_and_permissions: () => dispatch(get_user_role_and_permissions()),
  current_user_profile: () => dispatch(current_user_profile()),
  get_corporate_id: () => dispatch(corporate_id()),
  get_corporate_id_details: (id) => dispatch(corporate_id_details(id)),
  get_chart_stats: () => dispatch(get_chart_stats()),
  get_branch_count_data: (data) => dispatch(get_branch_count_data(data)),
  get_roles_list: () => dispatch(get_roles_list()),
  post_business_update: (data) => dispatch(post_business_update(data)),
  get_review_count: () => dispatch(get_review_count()),
  get_feedback_count: () => dispatch(get_feedback_count()),
  get_message_count: () => dispatch(get_message_count()),
  get_qa_count: () => dispatch(get_qa_count()),
  getallbranch: () => dispatch(getallbranch())
});

export default connect(mapState, mapProps)(Dashboard);