import React, { Component } from "react";
import AppHeader from "../../../app-header";
import {
  Container,
  Row,
  Col,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import BranchCSR from "../../../component/molecules/branchCSR";
import classnames from "classnames";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import AddUser from "../../../component/molecules/admin-view/addUser";
import AdminRoles from "../../molecules/admin-view/adminRoles";
import UsersList from "../../../component/molecules/admin-view/usersList";
import AdminBranches from "../../../component/molecules/admin-view/adminBranches";
import AdminSettings from "../../../component/molecules/admin-view/settings";
import { corporate_id } from "../../../actions/user";

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "people",
      isEditUserFormVisible: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.active_branch_tab) {
      this.setState({ activeTab: 'branches' });
    }

    if (nextProps && nextProps.active_roles_tab) {
      this.setState({ activeTab: 'roles' });
    }

    if (nextProps && nextProps.active_settings_tab) {
      this.setState({ activeTab: 'settings' });
    }
  }

  toggleMainTabs = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({ activeTab: tab });
    }
  };

  componentDidMount() {
    if (this.props.corporate_details == null) {
      this.props.get_corporate_id();
    }
  }

  render() {
    let headerImage = '';
    const {corporate_details} = this.props;
    if (this.props && this.props.corporate_details && Array.isArray(this.props.corporate_details.listing_profileimage) && this.props.corporate_details.listing_profileimage.length > 0) {
      headerImage = this.props.corporate_details.listing_profileimage[0].url
    }
    let listingImage = corporate_details && corporate_details.listing_logo_pic && corporate_details.listing_logo_pic[0] && corporate_details.listing_logo_pic[0].location;
    let listing_name = corporate_details && corporate_details.name;
    // console.log({props:this.props})
    return (
      <React.Fragment>
        <AppHeader></AppHeader>
        <div className="user-main">
          <header className="app-header mb-3" style={{ backgroundImage: `url(${headerImage})`, backgroundSize: 'cover' }}>
            <Container>
              <div className="app-header-block d-flex flex-column">
                <Row className="item-prev-btn">
                  <Col md="3">&nbsp;</Col>
                  <Col md="6" style={{ minHeight: "400px" }}>
                    <div className="text-center p-4">
                      <div className="header-profile">
                        <div className="header-profile-block mr-2">
                          <img
                            src={listingImage}
                            alt={listing_name}
                          />
                        </div>
                      </div>
                      <h1 className="text-shadow">{listing_name}</h1>
                    </div>
                  </Col>
                  <Col md="3">
                    <div className="d-flex h-100 flex-column">
                      <div className="text-md-right text-center mt-auto">
                        <Link
                          to={"/corporateprofile"}
                          className="text-uppercase font-weight-bold text-decoration-none text-white px-1"
                        >
                          <FontAwesomeIcon icon="arrow-left" /> Back to Homepage
                        </Link>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
            </Container>
          </header>

          <section>
            <Container>
              <Row>
                <Col md="4">
                  <BranchCSR></BranchCSR>
                </Col>
                <Col md="8">
                  <div>
                    <Nav
                      tabs
                      justified
                      className="scrollbar text-center align-items-end flex-nowrap overflow-y-hidden overflow-x-auto"
                    >
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: this.state.activeTab === "people",
                          })}
                          onClick={() => {
                            this.toggleMainTabs("people");
                          }}
                        >
                          People
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: this.state.activeTab === "branches",
                          })}
                          onClick={() => {
                            this.toggleMainTabs("branches");
                          }}
                        >
                          Branches
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: this.state.activeTab === "roles",
                          })}
                          onClick={() => {
                            this.toggleMainTabs("roles");
                          }}
                        >
                          Roles
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: this.state.activeTab === "settings",
                          })}
                          onClick={() => {
                            this.toggleMainTabs("settings");
                          }}
                        >
                          Settings
                        </NavLink>
                      </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.activeTab}>
                      <TabPane tabId="people">
                        <div className="bg-white p-3 mb-3 step-11">
                          <h3 className="text-secondary mb-3">Assign New</h3>
                          <AddUser />
                        </div>
                        <UsersList />
                      </TabPane>

                      <TabPane tabId="branches">
                          {/* <h3 className="text-secondary mb-3">Branches New</h3> */}
                          <AdminBranches />
                      </TabPane>

                      {/* <TabPane tabId="roles">
                        <div className="bg-white p-3 mb-3">
                          <h3 className="text-secondary mb-4">
                            Existing Roles
                          </h3>                     
                       </div>
                        
                      </TabPane> */}
                      <TabPane tabId="roles">
                        <AdminRoles />
                      </TabPane>
                      <TabPane tabId="settings">
                        <AdminSettings />
                      </TabPane>
                    </TabContent>
                  </div>
                </Col>
              </Row>
            </Container>
          </section>
        </div>
      </React.Fragment>
    );
  }
}

const mapState = (state) => ({
  profileData: state.user.current_user,
  corporate_details: state.user.corporate_id_details,
  current_role_and_permissions: state.user.current_role_and_permissions,
  top_bar_stats: state.user.top_bar_stats,
  filtered_stats_count: state.user.filtered_stats_count,
  getbranch_copy: state.user.getbranch_copy,
  getbranch_data: state.user.getbranch_data,
  all_branches: state.user.all_branches,
  admin_stats: state.user.admin_stats,
  active_branch_tab: state.user.active_branch_tab,
  active_roles_tab: state.user.active_roles_tab,
  active_settings_tab: state.user.active_settings_tab,
});

const mapProps = (dispatch) => ({
  get_corporate_id: () => dispatch(corporate_id())
})

export default connect(mapState, mapProps)(Settings);
