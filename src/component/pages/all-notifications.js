/* eslint eqeqeq: 0 */
/* eslint-disable jsx-a11y/anchor-is-valid */
/*eslint-disable eqeqeq*/
/*eslint eqeqeq: "off"*/
/* eslint-disable-line */
/* eslint-disable-next-line */
import React, { Component } from 'react';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import { Container, } from 'reactstrap';
import { withRouter } from "react-router";
// import AppHeader from '../oragnisms/dashboard/appHeader';
import AppHeader from "../../app-header";

import { fetch_notification_data, current_user_profile } from "../../actions/user";

class AllNotifications extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fetchNotification: [],
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.get_notification && nextProps.get_notification.results && Array.isArray(nextProps.get_notification.results) && nextProps.get_notification.results.length > 0) {
      this.setState({ fetchNotification: nextProps.get_notification?.results });

    }
  }

  componentDidMount() {
    let page = 1;
    this.props.fetch_notification_data(page);
    this.props.current_user_profile();
  }


  render() {
    let { fetchNotification } = this.state;
    return (
      <div>
        {/* <AppHeader isEnableCover={false} /> */}
        <AppHeader clickTourStart={this.props.clickTourStart} />
        <div className="pb-3" style={{ marginTop: '70px' }}>
          <Container>
            <h1 className="notification-heading">Your Notifications</h1>
            <div style={{ maxWidth: '800px' }}>

              {fetchNotification && Array.isArray(fetchNotification) && fetchNotification.length > 0 ?
                fetchNotification.map((itemFeed, indexFeed) => (
                  <div key={indexFeed} className="bg-white fs-14 text-tertiary p-3 mt-2 position-relative" role="button">
                    <div className="d-flex mx-n1">
                      <div className="px-1">
                        <a href="#">
                          <img className="img-circle _50x50"
                            src={itemFeed?.notification?.actor?.current_profile_pic}
                            onError={(error) => { error.target.src = require('../../assets/images/icons/user-circle.png') }}
                            alt={'no-image'} />
                        </a>
                      </div>
                      <div className="px-1 align-self-center flex-fill">
                        <Link to={{ pathname: `/people/${itemFeed?.notification?.actor.username}` }} target="_blank" className="mr-2">
                          <span className="font-weight-bold text-dark">
                            {itemFeed?.notification?.actor?.first_name} {itemFeed?.notification?.actor?.last_name}
                          </span>
                        </Link>
                        <span>
                          {Object.keys(itemFeed.notification.target).length > 0 && itemFeed.notification.target.category ?
                            <Link to={{ pathname: `/${itemFeed.notification.target.category}/${itemFeed.notification.target.slug && itemFeed.notification.target.slug != "" ? itemFeed.notification.target.slug : null}`, state: { id: itemFeed?.notification?.target?.userentry_id } }}>{itemFeed?.notification?.verb}</Link>
                            :
                            <Link to="#!">{itemFeed?.notification?.verb}</Link>
                          }
                        </span>
                        <span>{" "}{itemFeed?.notification?.time_stamp}</span>
                      </div>
                    </div>
                  </div>
                ))
                : <div className="px-3 mt-2">
                  <span>No New Notifications</span>
                </div>}
            </div>
          </Container>
        </div>
      </div>
    )
  }
}

const mapState = (state) => ({
  profileData: state.user.current_user,
  get_notification: state.user.get_notification,
});

const mapProps = (dispatch) => ({
  fetch_notification_data: (page) => dispatch(fetch_notification_data(page)),
  current_user_profile: () => dispatch(current_user_profile()),
});
export default withRouter(connect(mapState, mapProps)(AllNotifications));
