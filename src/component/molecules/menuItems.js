import React, { Component } from 'react';
import CollapseBasic from '../atoms/collapse';
import { FormGroup, Modal, ModalHeader, ModalBody, ModalFooter, InputGroupText, Label, Input, InputGroup, InputGroupAddon, Button, Row, Col, Progress, } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
    get_map_cordinates,
    corporate_id_details,
    corporate_id,
    add_service_item,
    get_album_types_list,
    get_album_type_data,
    delete_selected_gallery_media_Upload,
    add_service_media,
    set_branch_ID_local_storage
} from '../../actions/user';
import { TabContent, TabPane, Nav, NavItem, NavLink, } from 'reactstrap';
import classnames from 'classnames';
import { callApi } from "../../utils/apiCaller";
import MenuContent from "./MenuContent";



class MenuItems extends Component {
    mapRef = React.createRef();

    constructor(props) {
        super(props);
        this.state = {
            menuItemsModalToggle: false,
            newServiceItemModalToggle: false,
            menuData: [],
            corporateId: null,
            exploreMenuItems: [],
            activeTab: "1",
            setActiveTab: null,
            addServiceItem: {
                menu_type_name: '',
                section_name: '',
                menu_name: '',
                price: 0,
                menu_description: '',
                error: ''
            },
            uploadMediaModal: false,
            selectedUpMediaType: "upload",
            showGalleryType: "images",
            uploadMedia: {
                mediaType: "image",
                albumType: "",
                pageNo: 1,
                albumTypesList: [],
                albumTypeData: {},
                selectedMedia: [],
                selectedMediaIds: [],
                embedLinks: {},
                uploadFiles: [],
                uploadedFiles: [],
                progress: 0,
            },
            menuItems: {},
            currentElement: 1,
            MenuContent: null,
            activeMenuItemIndex: 0,
            animatingMenuItem: false,
        };
    }

    toggle = tab => {
        if (this.state.activeTab !== tab) { this.setState({ activeTab: tab }) };
    }

    componentWillReceiveProps(nextProps) {
        // console.log({ nextProps })
        if (nextProps.map_cordinates && nextProps.map_cordinates.data) {
            // console.log("MENU-ITEMS1111",nextProps.map_cordinates.data);

            //let items = nextProps.menu_items.branch_counts;


            this.setState({
                menuData: nextProps.map_cordinates.data
            })
            // console.log("MENU-ITEMS",this.state.menuData);
        }

        if (nextProps.menu_items && nextProps.menu_items.results) {
            //console.log("MEU", nextProps.menu_items.results);
            this.setState({ exploreMenuItems: nextProps.menu_items.results })
        }

        if (nextProps.corporate_id && nextProps.corporate_id.id) {

            this.setState({
                corporateId: nextProps.corporate_id.id
            })
        }

        if (nextProps.album_types_list && nextProps.album_types_list.length > 0) {
            this.setState({
                ...this.state,
                uploadMedia: {
                    ...this.state.uploadMedia,
                    albumTypesList: nextProps.album_types_list,
                },
                //uploadMediaModal: true,
                //selectedUpMediaType: "upload",
                //showGalleryType: "images",
            });
        }

        if (
            nextProps.album_type_data &&
            Object.keys(nextProps.album_type_data).length > 0
        ) {
            this.setState({
                ...this.state,
                //selectedUpMediaType: "gallery",
                uploadMedia: {
                    ...this.state.uploadMedia,
                    albumTypeData: nextProps.album_type_data,
                },
            });
        }

        if (nextProps.menu_content && nextProps.menu_content.results) {

            //console.log("MEU", nextProps.menu_content.results);

            this.setState({
                ...this.state,
                menuContent: {
                    ...this.state.menuContent,
                    menuData: nextProps.menu_content.results
                },
                //selectedUpMediaType: "gallery",

            });
            //console.log("STATE", this.state.menuContent);
        }

    }

    handleOnFileUploadChange = (event) => {
        let uploadFiles = event.target.files;
        let showFiles = [];
        // for (const key of Object.keys(uploadFiles)) {
        //     showFiles.push({ id: "", url: URL.createObjectURL(uploadFiles[key]) });
        // }
        for (const key of Object.keys(uploadFiles)) {
            let itemType = uploadFiles[key].type.split("/");
            let extName = itemType[0];
            showFiles.push({ id: "", url: extName === "image" ?  URL.createObjectURL(uploadFiles[key]) : require("../../assets/images/blank.png") });
          }
        this.setState({
            ...this.state,
            uploadMedia: {
                ...this.state.uploadMedia,
                uploadFiles: showFiles,
                progress: 0,
            },
        });
        let progressPart = 100 / showFiles.length;
        let progress = 0;
        for (const key of Object.keys(uploadFiles)) {
            let data = new FormData();
            data.append("file", uploadFiles[key]);

            callApi(
                `/upload/multiuploader/?album=&instance=Add_Menu%20Image`,
                "POST",
                data,
                true
            ).then((response) => {
                this.handleOnClickSelectGalleryMedia(response);
                if (showFiles.length === 1 || key === showFiles.length - 1) {
                    progress = 100;
                } else {
                    progress = progress + progressPart;
                }
                showFiles[key].id = response.id;
                this.setState({
                    ...this.state,
                    uploadMedia: {
                        ...this.state.uploadMedia,
                        progress: progress,
                        uploadedFiles: [...this.state.uploadMedia.uploadedFiles, response],
                        uploadedFiles: showFiles,
                    },
                });
            });
        }
    };
    handleOnClickRemoveSelectedMedia = (id) => () => {
        this.props.delete_selected_gallery_media_Upload(id);
        let uploadFiles = this.state.uploadMedia.uploadFiles.filter(
            (file) => file.id !== id
        );
        let uploadedFiles = this.state.uploadMedia.uploadedFiles.filter(
            (file) => file.id !== id
        );
        let selectedMediaIds = this.state.uploadMedia.selectedMediaIds.filter(
            (item) => item !== id
        );
        let selectedMedia = this.state.uploadMedia.selectedMedia.filter(
            (file) => file.id !== id
        );
        this.setState({
            ...this.state,
            uploadMedia: {
                ...this.state.uploadMedia,
                selectedMedia: selectedMedia,
                selectedMediaIds: selectedMediaIds,
                uploadedFiles: uploadedFiles,
                uploadFiles: uploadFiles,
            },
        });
    };
    handleOnClickRemoveSelectedGalleryMedia = (media) => {
        let index = this.state.uploadMedia.selectedMediaIds.indexOf(media.id);
        let newSelectedMediaIds;
        let newSelectedMedia;
        if (index !== -1) {
            newSelectedMediaIds = this.state.uploadMedia.selectedMediaIds.filter(
                (item) => item !== media.id
            );
            newSelectedMedia = this.state.uploadMedia.selectedMedia.filter(
                (item) => item.id !== media.id
            );
            this.props.delete_selected_gallery_media_Upload(media.id);
            this.setState({
                ...this.state,
                uploadMedia: {
                    ...this.state.uploadMedia,
                    selectedMedia: newSelectedMedia,
                    selectedMediaIds: newSelectedMediaIds,
                },
            });
        }
    };
    handleOnClickSelectGalleryMedia = (media) => {
        let index = this.state.uploadMedia.selectedMediaIds.indexOf(media.id);
        let newSelectedMediaIds;
        let newSelectedMedia;
        if (index !== -1) {
            newSelectedMediaIds = this.state.uploadMedia.selectedMediaIds.filter(
                (item) => item !== media.id
            );
            newSelectedMedia = this.state.uploadMedia.selectedMedia.filter(
                (item) => item.id !== media.id
            );
        } else {
            newSelectedMediaIds = [
                ...this.state.uploadMedia.selectedMediaIds,
                media.id,
            ];
            newSelectedMedia = [...this.state.uploadMedia.selectedMedia, media];
        }
        this.setState({
            ...this.state,
            uploadMedia: {
                ...this.state.uploadMedia,
                selectedMedia: newSelectedMedia,
                selectedMediaIds: newSelectedMediaIds,
            },
        });
    };
    handleOnClickGalleryType = (type) => {
        let mediaType = "";
        if (type === "images") {
            mediaType = "image";
        } else if (type === "videos") {
            mediaType = "video";
        }
        this.props.get_album_type_data(mediaType, "", 1);
        this.setState({
            ...this.state,
            selectedUpMediaType: "gallery",
            showGalleryType: type,
            uploadMedia: {
                ...this.state.uploadMedia,
                mediaType: mediaType,
                albumType: "",
                pageNo: 1,
                //selectedMedia: [],
                //selectedMediaIds: [],
            },
        });
    };
    handleOnClickAlbumTypeChange = (e) => {
        this.props.get_album_type_data("image", e.target.value, 1);
        this.setState({
            ...this.state,
            selectedUpMediaType: "gallery",
            showGalleryType: "images",
            uploadMedia: {
                ...this.state.uploadMedia,
                mediaType: "image",
                albumType: e.target.value,
                pageNo: 1,
                //selectedMedia: [],
                //selectedMediaIds: [],
            },
        });
    };
    handleOnClickSelectedUploadMediaType = (type) => {
        if (type === "gallery") {
            this.props.get_album_type_data("image", "", 1);
            this.setState({
                ...this.state,
                selectedUpMediaType: type,
                showGalleryType: "images",
                uploadMedia: {
                    ...this.state.uploadMedia,
                    mediaType: "image",
                    albumType: "",
                    pageNo: 1,
                    //selectedMedia: [],
                    //selectedMediaIds: [],
                    embedLinks: {},
                    progress: 0,
                },
            });
        } else if (type === "upload") {
            this.setState({
                ...this.state,
                selectedUpMediaType: type,
                uploadMedia: {
                    ...this.state.uploadMedia,
                    //selectedMedia: [],
                    //selectedMediaIds: [],
                    embedLinks: {},
                    progress: 0,
                },
            });
        } else if (type === "embed") {
            let embedLinks = {
                0: "",
                1: "",
            };
            this.setState({
                ...this.state,
                selectedUpMediaType: type,
                uploadMedia: {
                    ...this.state.uploadMedia,
                    selectedMedia: [],
                    selectedMediaIds: [],
                    embedLinks: embedLinks,
                },
            });
        }
    };

    handleOnClickUploadMedia = () => {
        this.props.get_album_types_list();
        this.setState({
            ...this.state,
            uploadMedia: {
                ...this.state.uploadMedia,
                mediaType: "image",
                albumType: "",
                pageNo: 1,
                albumTypeData: {},
                embedLinks: {},
                progress: 0,
                uploadFiles: [],
                uploadedFiles: [],
            },
            uploadMediaModal: true,
            selectedUpMediaType: "upload",
            showGalleryType: "images",
        });
    };
    uploadMediaModalToggle = () => {
        this.setState({
            ...this.state,
            uploadMediaModal: !this.state.uploadMediaModal,
            uploadMedia: {
                ...this.state.uploadMedia,
                selectedMedia: [],
                selectedMediaIds: [],
                mediaType: "image",
                albumType: "",
                pageNo: 1,
                albumTypeData: {},
                //albumTypesList: [],
                embedLinks: {},
                uploadedFiles: [],
                uploadFiles: [],
            },
            selectedUpMediaType: "upload",
            showGalleryType: "images",
        });
    };

    truncate = (filenameString) => {
        // let split = filenameString.split(".");
        let filename = filenameString.substr(0, filenameString.lastIndexOf("."));
        let extension = filenameString.substr(
            filenameString.lastIndexOf("."),
            filenameString.length - 1
        );
        let partial = filename.substring(filename.length - 3, filename.length);
        filename = filename.substring(0, 15);
        return filename + "..." + partial + extension;
    };

    menuItemsModalToggle = () => {
        this.setState({ menuItemsModal: !this.state.menuItemsModal });
        this.getMenuContent();

        if (this.state.corporateId !== null) {
            this.get_menu_list(this.state.corporateId);
        }
    }

    get_menu_list = async (id) => {
        try {
            localStorage.setItem('loader', true);
            let data = await callApi(`/api/getmenu/?${id}`, "GET");
            //console.log({ data })
            localStorage.setItem('loader', false);
            this.setState({ exploreMenuItems: data && data.results })
        } catch (e) {
            console.log(e);
        }
    }

    get_menu_content = async (id, element) => {
        try {
            let { menuItems } = this.state;
            localStorage.setItem('loader', true);
            let qa = window.unescape(`/api/getmenu/?${id}/&menuType=${element}`);
            let data = await callApi(qa, "GET")
            localStorage.setItem('loader', false);
            if (data != undefined) {
                //onsole.log({ data, element });
                menuItems[element] = data.results;
                this.setState({ menuItems, currentElement: element });
            }
        } catch (e) {
            console.log(e);
        }
    }

    newServiceItemModalToggle = () => {
        this.setState({
            ...this.state,
            newServiceItemModal: !this.state.newServiceItemModal,
            addServiceItem: {
                menu_type_name: '',
                section_name: '',
                menu_name: '',
                price: 0,
                menu_description: '',
                error: ''
            },
            uploadMediaModal: false,
            uploadMedia: {
                ...this.state.uploadMedia,
                selectedMedia: [],
                selectedMediaIds: [],
                mediaType: "image",
                albumType: "",
                pageNo: 1,
                albumTypeData: {},
                //albumTypesList: [],
                embedLinks: {},
                uploadedFiles: [],
                uploadFiles: [],
            },
            selectedUpMediaType: "upload",
            showGalleryType: "images",
        });
    }

    handleOnChangeServicePrice = (e) => {
        let NumberValue = e.target.value;
        if(NumberValue >= 0){this.setState({
            ...this.state,
            addServiceItem: {
                ...this.state.addServiceItem,
                [e.target.name]: NumberValue,
                error: ''
            }
        })}
    }
    
    handleOnChangeServiceItem = (e) => {
        this.setState({
            ...this.state,
            addServiceItem: {
                ...this.state.addServiceItem,
                [e.target.name]: e.target.value,
                error: ''
            }
        })
    }

    handleOnAddServiceSubmit = (e) => {
        e.preventDefault();
        let id = this.props.corporate_id.id;
        if (this.state.addServiceItem.menu_name === '') {
            this.setState({
                ...this.state,
                addServiceItem: {
                    ...this.state.addServiceItem,
                    error: 'This field is required'
                }
            })
        } else if (id && this.state.addServiceItem.menu_name !== '') {
            this.props.add_service_item(this.state.addServiceItem, id);
            if (this.state.uploadMedia.selectedMedia.length > 0) {
                let data = {
                    type: 'media',
                    entries: id,
                    userentries_id: id,
                    taxonomy_id: '',
                    multiuploaderfile: this.state.uploadMedia.selectedMediaIds
                }
                this.props.add_service_media(data);
            }
            this.newServiceItemModalToggle();
        }
    }

    getMenuContent = async () => {
        let items = await this.state.exploreMenuItems;
        let id = await this.state.corporateId;
        //console.log("itemsitems", items);
        if (items && items.length > 0) {
            let element = items[1];
            this.get_menu_content(id, element);
        }
    }

    getAllMenuContent = async (element) => {
        let id = await this.state.corporateId;
        this.get_menu_content(id, element);
    }

    // nav body
    // render - onclick//
    // 

    renderActiveItems = ({ active }) => {
        let { activeTab, currentElement, menuItems } = this.state;
        //console.log(activeTab,'---')
        //console.log("CURRETELEMENT",menuItems[currentElement]);
        if (activeTab !== null && activeTab > 0) {

            let data = menuItems[currentElement];
            //this.setState({MenuContent: menuItems[currentElement]})
            if (data && data !== null || data !== undefined) {

                //console.log("DATADATADATA",menuItems[currentElement]);
                return (
                    (<TabPane tabId={activeTab}>
                        <MenuContent content={data} name={currentElement} updateMenu={this.updateMenu} deleteMenu={this.deleteMenu} />
                    </TabPane>)
                )
            }
        }
        return null;
    }

    getBranchData = async (event) => {
        let id = event.target.value;
        await this.props.set_branch_ID_local_storage(id);
    }

    componentDidMount() {
        this.get_menu_list(this.state.corporateId);
    }

    updateMenu = (data) => {
        const {sectionId, section_name, price, menu_description , menu_type_name, menu_name} = data
        callApi(
            `/api/getmenu/?id=${sectionId}`,
            "PUT",
            {
                "menu_name":menu_name,
                "price":price,
                "menu_type_name":menu_type_name,
                "section_name":section_name,
                "menu_description":menu_description
            },
            true
        ).then((response) => {
            this.get_menu_list(this.state.corporateId);
        });
    }

    deleteMenu = (id, name) => {
        callApi(
            `/api/getmenu/?id=${id}&menuType=${name}`,
            "DELETE",
            true
        ).then((response) => {
            this.get_menu_list(this.state.corporateId);
        });
    }



    render() {
        const { menuData, exploreMenuItems, menuContent } = this.state;
        return (

            <React.Fragment>

                <CollapseBasic title="Menu Items" isOpen={true}>
                    <FormGroup>
                        <Input type="select" name="selectBranch" className="bg-white" onChange={this.getBranchData}>
                            <option disabled>Select Branch</option>
                            {menuData &&
                                Array.isArray(menuData) &&
                                menuData.length > 0
                                ? menuData.map((item, index) => (

                                    <option key={index} value={item.entries_id}>{item.name} - {item.address1} ({item.country})</option>
                                )) :
                                "No Items"
                            }


                        </Input>
                    </FormGroup>

                    {/* Trigger Modal Pop up on below action */}
                    <div>
                        <div className="d-flex">
                            <span className="actionable mr-auto" onClick={this.menuItemsModalToggle}>Explore Menu of Services</span>
                        </div>
                    </div>

                    {/* Trigger Modal Pop up on below action */}
                    <div>
                        <div className="d-flex">
                            <span className="actionable ml-auto" onClick={this.newServiceItemModalToggle}><FontAwesomeIcon icon="plus" /> Add Services</span>
                        </div>
                    </div>
                </CollapseBasic>


                {/* Explore Menu Items Modal */}
                <Modal isOpen={this.state.menuItemsModal} toggle={this.menuItemsModalToggle} size="lg">
                    <ModalHeader toggle={this.menuItemsModalToggle}>Menu Items</ModalHeader>
                    <ModalBody>
                        <Nav tabs>
                            {exploreMenuItems && exploreMenuItems.length > 0 ?
                                exploreMenuItems.map((item, index) => {
                                    {
                                        return index > 0 && (
                                            <NavItem 
                                                key={index}
                                            onClick={() => {
                                                this.toggle(`${index}`);
                                                this.getAllMenuContent(item);
                                            }}>
                                                <NavLink
                                                    className={classnames({ active: this.state.activeTab === `${index}` })}

                                                >
                                                    {item}
                                                </NavLink>
                                            </NavItem>
                                        )
                                    }
                                }) : ''}
                        </Nav>

                        <div>
                            <TabContent activeTab={this.state.activeTab}>



                                {this.renderActiveItems({ active: '1' })}
                                {/* {menuContent && menuContent.length > 0 ?
                                    menuContent.map((item, index) => {
                                        {
                                            return index > 0 && this.state.activeTab == `${index}` && (
                                                // {return index > 0 &&(

                                                <TabPane tabId={index} className={classnames({ active: this.state.activeTab === `${index}` })}>
                                                    <Row>
                                                        <Col sm="12">
                                                            <h4>{index}</h4>
                                                        </Col>
                                                    </Row>
                                                </TabPane>
                                            )
                                        }
                                        // )}
                                    }) : ''} */}
                                {/* {this.state.activeTab == '1' && (
                            <TabPane tabId="1">
                              <Row>
                                <Col sm="12">
                                  <h4>Tab 1 Contents</h4>
                                </Col>
                              </Row>
                            </TabPane>)}
                           {this.state.activeTab == '2' && <TabPane tabId="2">
                              <Row>
                                <Col sm="6">
                                  <Card body>
                                    <CardTitle>Special Title Treatment</CardTitle>
                                    <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                    <Button>Go somewhere</Button>
                                  </Card>
                                </Col>
                                <Col sm="6">
                                  <Card body>
                                    <CardTitle>Special Title Treatment</CardTitle>
                                    <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                    <Button>Go somewhere</Button>
                                  </Card>
                                </Col>
                              </Row>
                            </TabPane>} */}
                            </TabContent>
                        </div>
                        {/* : 
                            <div className="text-dark text-center fs-20">No Menu Items Listed</div>
                    } */}

                    </ModalBody>
                </Modal>

                {/* Add New Service Item Modal */}
                <Modal isOpen={this.state.newServiceItemModal} toggle={this.newServiceItemModalToggle} size="lg">
                    <ModalHeader toggle={this.newServiceItemModalToggle}>Add New Service Item</ModalHeader>
                    <ModalBody>
                        <Row className="justify-content-center">
                            <Col lg={10}>
                                <FormGroup row>
                                    <Label sm={4} className="text-dark" size="sm">Service Menu</Label>
                                    <Col sm={8}>
                                        <Input type="text" name="menu_type_name" value={this.state.addServiceItem.menu_type_name} onChange={this.handleOnChangeServiceItem} placeholder="Name the Service Menu" className="primary" />
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={4} className="text-dark" size="sm">Section Name</Label>
                                    <Col sm={8}>
                                        <Input type="text" name="section_name" value={this.state.addServiceItem.section_name} onChange={this.handleOnChangeServiceItem} placeholder="Name the Section" className="primary" />
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={4} className="text-dark" size="sm">Service Name</Label>
                                    <Col sm={8}>
                                        <Input type="text" name="menu_name" value={this.state.addServiceItem.menu_name} onChange={this.handleOnChangeServiceItem} placeholder="Name the Service" className="primary" />
                                        {this.state.addServiceItem.error && (<p className="text-danger small">This field is required</p>)}
                                    </Col>

                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={4} className="text-dark" size="sm">Service Description</Label>
                                    <Col sm={8}>
                                        <Input type="textarea" row="3" name="menu_description" placeholder="Description" value={this.state.addServiceItem.menu_description} onChange={this.handleOnChangeServiceItem} className="primary" />
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label sm={4} className="text-dark" size="sm">Price</Label>
                                    <Col sm={8}>
                                        <InputGroup>
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText className="primary">$</InputGroupText>
                                            </InputGroupAddon>
                                            <Input type="number" name="price" value={this.state.addServiceItem.price} onChange={this.handleOnChangeServicePrice} placeholder="Price" className="primary" />
                                        </InputGroup>
                                    </Col>
                                </FormGroup>
                                <FormGroup>
                                    <Button color="white" className="text-dark" size="sm" onClick={() => this.handleOnClickUploadMedia()}><FontAwesomeIcon icon="camera" className="mr-2" /> Add Image</Button>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row
                            className="mb-3"
                            form
                            hidden={
                                this.state.uploadMedia.selectedMedia.length > 0
                                    ? false
                                    : true
                            }
                        >
                            {this.state.uploadMedia.selectedMedia.length > 0 &&
                                this.state.uploadMedia.selectedMedia.map((item) => {
                                    return (
                                        <Col xs="auto" className="mb-3">
                                            <div className="d-flex pr-3 pt-3">
                                                <div>
                                                    {(item.type === "image" ||
                                                        item.media_type === "image" ||
                                                        item.type === "video" ||
                                                        item.media_type === "video") && (
                                                            <div className="gallery-media">
                                                                <img
                                                                    src={
                                                                        item.file ? item.file : item.url
                                                                    }
                                                                    onError={(e) => (e.target.src = require("../../assets/images/blank.png"))}

                                                                    alt={
                                                                        item.filename
                                                                            ? item.filename.length < 20
                                                                                ? item.filename
                                                                                : this.truncate(item.filename)
                                                                            : item.name
                                                                                ? item.name.length < 20
                                                                                    ? item.name
                                                                                    : this.truncate(item.name)
                                                                                : ""
                                                                    }
                                                                />
                                                            </div>
                                                        )}
                                                </div>
                                                <div className="mx-n3 mt-n3">
                                                    <Button
                                                        color="dark"
                                                        size="sm"
                                                        title="Remove Media"
                                                        onClick={() =>
                                                            this.handleOnClickRemoveSelectedGalleryMedia(
                                                                item
                                                            )
                                                        }
                                                    >
                                                        <FontAwesomeIcon icon="minus" />{" "}
                                                    </Button>
                                                </div>
                                            </div>
                                        </Col>
                                    );
                                })}
                        </Row>
                    </ModalBody>
                    <ModalFooter className="p-0">
                        <div className="text-right py-2 m-0">
                            <Button color="primary" onClick={this.newServiceItemModalToggle}>Cancel</Button>
                            <Button type="submit" color="primary" onClick={this.handleOnAddServiceSubmit}>Submit</Button>
                        </div>
                    </ModalFooter>
                </Modal>

                {/* Upload Media Modal */}
                <Modal
                    isOpen={this.state.uploadMediaModal}
                    toggle={this.uploadMediaModalToggle}
                    size="lg"
                >
                    <ModalHeader toggle={this.uploadMediaModalToggle}>
                        Upload Media
                    </ModalHeader>
                    <ModalBody className="p-3">
                        <Row>
                            <Col xs={3}>
                                <Nav pills className="flex-column">
                                    <NavItem className="text-center">
                                        <NavLink
                                            href="#"
                                            active={this.state.selectedUpMediaType === "upload"}
                                            onClick={() => {
                                                this.handleOnClickSelectedUploadMediaType("upload");
                                            }}
                                        >
                                            Upload
                                        </NavLink>
                                    </NavItem>
                                    <NavItem className="text-center">
                                        <NavLink
                                            href="#"
                                            active={this.state.selectedUpMediaType === "gallery"}
                                            onClick={() => {
                                                this.handleOnClickSelectedUploadMediaType("gallery");
                                            }}
                                        >
                                            Gallery
                                        </NavLink>
                                    </NavItem>

                                </Nav>
                            </Col>
                            <Col xs={9}>
                                <TabContent activeTab={this.state.selectedUpMediaType}>
                                    <TabPane tabId="upload">
                                        <div
                                            className="mb-3 type-file-block"
                                            hidden={
                                                this.state.uploadMedia.uploadFiles &&
                                                    this.state.uploadMedia.uploadFiles.length > 0
                                                    ? true
                                                    : false
                                            }
                                        >
                                            <Input
                                                type="file"
                                                name="upload_media_file"
                                                id="uploadFilesFromSystem"
                                                accept="image/*, video/*"
                                                onChange={this.handleOnFileUploadChange}
                                                multiple
                                            />
                                            <Label for="uploadFilesFromSystem">
                                                <FontAwesomeIcon
                                                    icon="upload"
                                                    size="2x"
                                                    className="text-primart"
                                                />
                                                <br />
                                                Upload Files From Computer
                                                <br />
                                                <small className="text-muted">
                                                    (or Drag files here)
                                                </small>
                                            </Label>
                                        </div>
                                        {this.state.uploadMedia.uploadFiles.length > 0 && (
                                            <Row className="mb-3" form>
                                                <Col xs={12}>
                                                    <div
                                                        style={{
                                                            maxWidth: "120px",
                                                            margin: "0 1rem 1rem auto",
                                                        }}
                                                    >
                                                        <div className="text-center mb-1 small">
                                                            {this.state.uploadMedia.progress === 100 ? (
                                                                <div className="text-success">
                                                                    <FontAwesomeIcon
                                                                        icon="check-circle"
                                                                        className="mr-1"
                                                                    />{" "}
                                                                    Uploaded
                                                                </div>
                                                            ) : (
                                                                    <div>
                                                                        Uploading{" "}
                                                                        <span className="text-success font-weight-bold ff-base">
                                                                            {this.state.uploadMedia.progress.toFixed(0)}
                                                                            %
                                                                        </span>
                                                                    </div>
                                                                )}
                                                        </div>
                                                        <Progress
                                                            value={this.state.uploadMedia.progress}
                                                            style={{ height: "8px" }}
                                                        ></Progress>
                                                    </div>
                                                </Col>
                                                {this.state.uploadMedia.uploadFiles.map((file) => {
                                                    return (
                                                        <Col xs="auto">
                                                            <div className="d-flex pr-3 pt-3">
                                                                <div>
                                                                    <div
                                                                        className="selectable-media"
                                                                        style={{ cursor: "default" }}
                                                                    >
                                                                        <div className="gallery-media">
                                                                            <img src={file.url} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="mx-n3 mt-n3">
                                                                    <Button
                                                                        color="dark"
                                                                        size="sm"
                                                                        title="Remove Media"
                                                                        hidden={file.id === "" ? true : false}
                                                                        onClick={this.handleOnClickRemoveSelectedMedia(
                                                                            file.id
                                                                        )}
                                                                    >
                                                                        <FontAwesomeIcon icon="minus" />{" "}
                                                                    </Button>
                                                                </div>
                                                            </div>
                                                        </Col>
                                                    );
                                                })}
                                                <Col xs="auto">
                                                    <div className="d-flex pt-3">
                                                        <div className="selectable-media" hidden>
                                                            <Label
                                                                for="uploadFilesFromSystemMini"
                                                                className="gallery-media"
                                                                style={{
                                                                    borderStyle: "dashed",
                                                                    cursor: "pointer",
                                                                }}
                                                            >
                                                                <div className="d-flex h-100 align-items-center justify-content-center">
                                                                    <span className="fs-14">Upload More</span>
                                                                </div>
                                                            </Label>
                                                            <Input
                                                                type="file"
                                                                name="upload_media_file"
                                                                id="uploadFilesFromSystemMini"
                                                                accept="image/*, video/*"
                                                                onChange={this.handleOnFileUploadChange}
                                                                multiple
                                                                style={{ display: "none" }}
                                                            />
                                                        </div>
                                                    </div>
                                                </Col>
                                            </Row>
                                        )}
                                        <div className="d-flex mx-n2">
                                            <div className="px-2">
                                                <Button
                                                    color="primary"
                                                    size="sm"
                                                    className="mw"
                                                    onClick={this.uploadMediaModalToggle}
                                                >
                                                    Cancel
                                                </Button>
                                            </div>
                                            <div className="px-2 ml-auto">
                                                <Button
                                                    color="primary"
                                                    size="sm"
                                                    className="mw"
                                                    onClick={() => {
                                                        this.setState({
                                                            ...this.state,
                                                            uploadMediaModal: false,
                                                        });
                                                    }}
                                                >
                                                    Insert
                                                </Button>
                                            </div>
                                        </div>
                                    </TabPane>
                                    <TabPane tabId="gallery">
                                        <div className="mb-2">
                                            <Nav tabs className="d-inline-flex mb-0">
                                                <NavItem>
                                                    <NavLink
                                                        href="#"
                                                        active={this.state.showGalleryType === "images"}
                                                        onClick={() => {
                                                            this.handleOnClickGalleryType("images");
                                                        }}
                                                    >
                                                        Image Gallery
                                                    </NavLink>
                                                </NavItem>
                                                <NavItem>
                                                    <NavLink
                                                        href="#"
                                                        active={this.state.showGalleryType === "videos"}
                                                        onClick={() => {
                                                            this.handleOnClickGalleryType("videos");
                                                        }}
                                                    >
                                                        Video Gallery
                                                    </NavLink>
                                                </NavItem>
                                            </Nav>
                                        </div>
                                        <TabContent activeTab={this.state.showGalleryType}>
                                            <TabPane tabId="images">
                                                <div>
                                                    <FormGroup className="form-row justify-content-end">
                                                        <Col xs="auto">
                                                            <Label className="small" for="selectAlbumType">
                                                                Album Type:
                                                            </Label>
                                                        </Col>
                                                        <Col xs="auto">
                                                            <Input
                                                                bsSize="sm"
                                                                type="select"
                                                                name="album_type_select"
                                                                id="selectAlbumType"
                                                                defaultValue={this.state.uploadMedia.albumType}
                                                                onChange={this.handleOnClickAlbumTypeChange}
                                                            >
                                                                <option value="">All</option>
                                                                {this.state.uploadMedia.albumTypesList.length >
                                                                    0 &&
                                                                    this.state.uploadMedia.albumTypesList.map(
                                                                        (type) => (
                                                                            <option value={type.category}>
                                                                                {type.category}
                                                                            </option>
                                                                        )
                                                                    )}
                                                            </Input>
                                                        </Col>
                                                    </FormGroup>
                                                </div>
                                                <div
                                                    className="px-3 my-3"
                                                    style={{ maxHeight: "400px", overflow: "auto" }}
                                                >
                                                    {this.state.uploadMedia.albumTypeData &&
                                                        this.state.uploadMedia.albumTypeData.count > 0 ? (
                                                            <Row xs={2} md={3} lg={4}>
                                                                {this.state.uploadMedia.albumTypeData.results.map(
                                                                    (media) => {
                                                                        return (
                                                                            <Col className="mb-3" key={media.id}>
                                                                                {media.type === "image" && (
                                                                                    <div
                                                                                        className={`selectable-media ${
                                                                                            this.state.uploadMedia.selectedMediaIds.indexOf(
                                                                                                media.id
                                                                                            ) !== -1
                                                                                                ? "selected"
                                                                                                : ""
                                                                                            }`}
                                                                                        onClick={() =>
                                                                                            this.handleOnClickSelectGalleryMedia(
                                                                                                media
                                                                                            )
                                                                                        }
                                                                                    >
                                                                                        <div className="gallery-media">
                                                                                            <img
                                                                                                src={media.file}
                                                                                                alt={
                                                                                                    media.filename.length < 20
                                                                                                        ? media.filename
                                                                                                        : this.truncate(
                                                                                                            media.filename
                                                                                                        )
                                                                                                }
                                                                                            />
                                                                                        </div>
                                                                                        <div className="font-weight-bold fs-12 text-secondary-dark mb-0 text-truncate">
                                                                                            {media.category
                                                                                                ? media.category
                                                                                                : "No Category"}
                                                                                        </div>
                                                                                        <div className="font-weight-normal fs-15 d-block">
                                                                                            {media.caption
                                                                                                ? media.caption
                                                                                                : "No Caption"}
                                                                                        </div>
                                                                                    </div>
                                                                                )}
                                                                            </Col>
                                                                        );
                                                                    }
                                                                )}
                                                            </Row>
                                                        ) : (
                                                            <div className="bg-white p-3">
                                                                <h2 className="text-secondary-dark">No images</h2>
                                                            </div>
                                                        )}
                                                </div>
                                                <div className="d-flex mx-n2">
                                                    <div className="px-2">
                                                        <Button
                                                            color="primary"
                                                            size="sm"
                                                            className="mw"
                                                            onClick={this.uploadMediaModalToggle}
                                                        >
                                                            Cancel
                                                        </Button>
                                                    </div>
                                                    <div className="px-2 ml-auto">
                                                        <Button
                                                            color="primary"
                                                            size="sm"
                                                            className="mw"
                                                            onClick={() => {
                                                                this.setState({
                                                                    ...this.state,
                                                                    uploadMediaModal: false,
                                                                });
                                                            }}
                                                        >
                                                            Insert
                                                        </Button>
                                                    </div>
                                                </div>
                                            </TabPane>
                                            <TabPane tabId="videos">
                                                <div
                                                    className="px-3 my-3"
                                                    style={{ maxHeight: "400px", overflow: "auto" }}
                                                >
                                                    {this.state.uploadMedia.albumTypeData &&
                                                        this.state.uploadMedia.albumTypeData.count > 0 ? (
                                                            <Row xs={2} md={3} lg={4}>
                                                                {this.state.uploadMedia.albumTypeData.results.map(
                                                                    (media) => {
                                                                        return (
                                                                            <Col className="mb-3" key={media.id}>
                                                                                {media.type === "video" && (
                                                                                    <div
                                                                                        className={`selectable-media ${
                                                                                            this.state.uploadMedia.selectedMediaIds.indexOf(
                                                                                                media.id
                                                                                            ) !== -1
                                                                                                ? "selected"
                                                                                                : ""
                                                                                            }`}
                                                                                        onClick={() =>
                                                                                            this.handleOnClickSelectGalleryMedia(
                                                                                                media
                                                                                            )
                                                                                        }
                                                                                    >
                                                                                        <div className="gallery-media">
                                                                                            <img
                                                                                                src={media.thumbnail}
                                                                                                alt={
                                                                                                    media.filename.length < 20
                                                                                                        ? media.filename
                                                                                                        : this.truncate(
                                                                                                            media.filename
                                                                                                        )
                                                                                                }
                                                                                            />
                                                                                        </div>
                                                                                        <div className="font-weight-bold fs-12 text-secondary-dark mb-0 text-truncate">
                                                                                            {media.category
                                                                                                ? media.category
                                                                                                : "No Category"}
                                                                                        </div>
                                                                                        <div className="font-weight-normal fs-15 d-block">
                                                                                            {media.caption
                                                                                                ? media.caption
                                                                                                : "No Caption"}
                                                                                        </div>
                                                                                    </div>
                                                                                )}
                                                                            </Col>
                                                                        );
                                                                    }
                                                                )}
                                                            </Row>
                                                        ) : (
                                                            <div className="bg-white p-3">
                                                                <h2 className="text-secondary-dark">No videos</h2>
                                                            </div>
                                                        )}
                                                </div>
                                                <div className="d-flex mx-n2">
                                                    <div className="px-2">
                                                        <Button
                                                            color="primary"
                                                            size="sm"
                                                            className="mw"
                                                            onClick={this.uploadMediaModalToggle}
                                                        >
                                                            Cancel
                                                        </Button>
                                                    </div>
                                                    <div className="px-2 ml-auto">
                                                        <Button
                                                            color="primary"
                                                            size="sm"
                                                            className="mw"
                                                            onClick={() => {
                                                                this.setState({
                                                                    ...this.state,
                                                                    uploadMediaModal: false,
                                                                });
                                                            }}
                                                        >
                                                            Insert
                                                        </Button>
                                                    </div>
                                                </div>
                                            </TabPane>
                                        </TabContent>
                                    </TabPane>

                                </TabContent>
                            </Col>
                        </Row>
                    </ModalBody>
                </Modal>

            </React.Fragment>
        )
    }
}



const mapState = (state) => {
    return {
        menu_items: state.user.menuItems,
        map_cordinates: state.user.map_cordinates,
        corporate_id: state.user.corporate_id,
        menu_content: state.user.menu_items_content,

        album_types_list: state.user.album_types_list,
        album_type_data: state.user.album_type_data,
    }
}

const mapProps = (dispatch) => {
    return {

        // get_menu_list: (id) => dispatch(get_menu_list(id)),
        add_service_item: (item, id) => dispatch(add_service_item(item, id)),
        // get_menu_content: (id, element) => dispatch(get_menu_content(id, element)),
        get_album_type_data: (type, value, page) => dispatch(get_album_type_data(type, value, page)),
        get_album_types_list: () => dispatch(get_album_types_list()),
        delete_selected_gallery_media_Upload: (id) => dispatch(delete_selected_gallery_media_Upload(id)),
        add_service_media: (data) => dispatch(add_service_media(data)),
        set_branch_ID_local_storage: (id) => dispatch(set_branch_ID_local_storage(id)),
    }
}
export default withRouter(connect(mapState, mapProps)(MenuItems));