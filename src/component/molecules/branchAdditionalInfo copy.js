import React, { Component, } from 'react';
import CollapseBasic from '../atoms/collapse';
import { Input, Button, ButtonGroup, Badge, Row, Col } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import Switch from 'react-input-switch';
import {
    update_additional_info,
    delete_additional_info,
    get_amenities_options
} from "../../actions/user";
import { callApi } from "../../utils/apiCaller";
import EditBtn from "../atoms/editBtn";
import DeleteBtn from "../atoms/deleteBtn";


class BranchAdditionalInfo extends Component {
    constructor(props) {
        super(props)
        this.myRef = React.createRef()
        this.state = {
            isToogleAcceptCreditCards: false,
            additionInfo: [],
            allAdditionalInfo: [],
            all_additional_fields: [],
            selectedCategory: '',
            isEditVisible: {},
            CategoryValue: 'None',
            CategoryName: '',
            CategoryId: '',
            corporateId: '',
            existing_info_count: 0,
            add_info_map: '',
            sub_category_list: [],
            amenities_list: '',
            saved_amenities_list: [],
            amenityId: '',
            additional_info_all_categories: [],
            res_data_by_category: [],
            received_additional_info: false,
            isOpenAdditional: {},
            newID: 0
        }
    }

    componentWillReceiveProps(nextProps) {
        const { received_additional_info } = this.state
        if (nextProps && nextProps.corporate_details && nextProps.corporate_details.additional_info) {
            let item = nextProps.corporate_details.additional_info;
            let result = this.sortObject(item);
            let newMergeresult = result.concat(nextProps?.corporate_details?.all_additional_fields)
            newMergeresult.forEach((nitem, index) => {
                if (nitem) { newMergeresult[index]["uniquId"] = index; }
            });
            this.props.handleSelectedAdditionalInfo(newMergeresult)
            this.setState({
                additionInfo: newMergeresult,
                existing_info_count: result.length,
                corporateId: nextProps.corporate_details.id,
                all_additional_fields: nextProps?.corporate_details?.all_additional_fields,
            })
        }

        // Getting all additional info - all categories
        if (nextProps && nextProps.corporate_details && nextProps.corporate_details.taxonomy_dict) {
            let item = nextProps.corporate_details.taxonomy_dict
            let result = this.sortObject(item);
            // this.setState({ additionInfo: result , existing_info_count: result.length, corporateId: nextProps.corporate_details.id })

            // additional-attributes-by-id/?category=471 
            let res_data = []
            if (result.length > 0 && !received_additional_info) {
                result.map((res) => {
                    callApi(
                        `/api/additional-attributes-by-id/?category=${res.value.id}&userentry=${nextProps?.corporate_details?.id}`,
                        "GET",
                    ).then((response) => {
                        if (response && response.code === 200) {
                            let resp = response
                            if (resp.length > 1) {
                                resp.map((r) => {
                                    res_data.push(r)
                                    let additional_info_all_categories = [...this.state.additional_info_all_categories];

                                    // Add item to it
                                    additional_info_all_categories.push(r);

                                    // Set state
                                    this.setState({ additional_info_all_categories });
                                })
                            }
                        }
                    })
                })
                this.setState({ received_additional_info: true })

            }

        }


        let data = {}

        if (nextProps && nextProps.corporate_details && nextProps.corporate_details.all_additional_fields) {
            let items = nextProps.corporate_details.all_additional_fields;
            let result = this.sortData(items)

            this.setState({ allAdditionalInfo: result })

            nextProps.corporate_details &&
                Array.isArray(nextProps.corporate_details.additional_info_map) &&
                nextProps.corporate_details.additional_info_map.length > 0 &&
                nextProps.corporate_details.additional_info_map.map((dt) => {
                    data[dt.name] = dt.id
                });

            this.setState({ add_info_map: data })

        }

        if (nextProps && nextProps.sub_category_data) {


            this.setState({ sub_category_list: nextProps.sub_category_data })

            // map data for already saved additional info
            let final_list = []
            nextProps && nextProps.sub_category_data.map((data) => {
                this.state.additionInfo && this.state.additionInfo.map((add_data) => {
                    if (data.label === add_data.name) {
                        add_data && add_data.value.map((val) => {
                            if (val && val.id === data.id) {
                                let found = 0
                                final_list && final_list.map((lst) => {
                                    if (lst && lst.name === data.label) {
                                        lst['value'].push(val)
                                        found = 1
                                    }
                                })
                                if (found === 0) {
                                    let dict = { 'name': data.label, 'value': [val] }
                                    final_list.push(dict)
                                }
                            }
                        })
                    }
                })
            })
            this.setState({ saved_amenities_list: final_list })

        }

        if (nextProps && nextProps.amenities_data) {
            this.setState({
                amenities_list: nextProps.amenities_data,
                CategoryId: nextProps.amenities_data &&
                    nextProps.amenities_data[0] &&
                    nextProps.amenities_data[0].reviews_additionalattributes
            })

        }
    }

    handleChange = (e) => {
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value,
        });
    };

    sortObject = (items) => {
        let { isEditVisible } = this.state;
        let filteredItems = [];
        var myObj = items,
            keys = Object.keys(myObj),
            i, k, len = keys.length;

        let filteredKeys = items &&
            Array.isArray(items) &&
            items.filter(item => (item?.name)?.search('_') < 0);
        filteredKeys = filteredKeys && Array.isArray(filteredKeys) && filteredKeys.length > 0 && filteredKeys.sort((a, b) => {
            let aItems = a.toLowerCase();
            let bItems = b.toLowerCase();
            if (aItems < bItems) { return -1; }
            if (aItems > bItems) { return 1; }
            return 0;
        });

        for (i = 0; i < len; i++) {
            k = filteredKeys[i];
            if (k !== undefined) {
                // isEditVisible[i] = false;
                filteredItems.push({ "name": k, "value": myObj[k] })
            }
        }
        // this.setState({ isEditVisible })
        return filteredItems;
    }

    sortData = (items) => {
        let filteredKeys = items && Array.isArray(items) && items.length > 0 && items.filter(item => (item?.name)?.search('_') < 0);
        filteredKeys = filteredKeys &&
            Array.isArray(filteredKeys) &&
            filteredKeys.length > 0 &&
            filteredKeys.sort((a, b) => {
                let aItems = (a.name).toLowerCase();
                let bItems = (b.name).toLowerCase();
                if (aItems < bItems) { return -1; }
                if (aItems > bItems) { return 1; }
                return 0;
            });
        return filteredKeys;
    }

    toggleAcceptCreditCards = () => {
        this.setState((prevstate) => ({ isToogleAcceptCreditCards: !prevstate.isToogleAcceptCreditCards }));
    }

    toggleDeleteItems = (item, title) => {
        let param = {}
        param["delete_add_info"] = true
        param["name"] = title
        param["info"] = item.value

        this.props.delete_additional_info(param, this.state.corporateId)


    }

    renderValues = (value, title) => {
        let itemsRender = []
        if (value && Array.isArray(value) && value.length > 0) {
            value.map((item, index) => {
                let currentIndex = index + 1;
                if (item.value && typeof (item.value) == "string") {
                    itemsRender.push(
                        <React.Fragment key={index}>
                            <Badge color="default" className="mr-1 fs-12">
                                <div className="d-flex align-items-center">
                                    <span className="mr-1 font-weight-normal">{item.value}</span>
                                    <button onClick={() => this.toggleDeleteItems(item, title)} type="button" className="close btn-sm" aria-label="Close">
                                        <FontAwesomeIcon icon="times-circle" />
                                    </button>
                                </div>
                            </Badge>
                        </React.Fragment>
                    )
                }
                return null;
            })
        }
        return (
            itemsRender
        )
    }


    renderItems = ({ title, value, edit, trash, index, item, }, new_info = false) => {
        let { isEditVisible, CategoryValue, existing_info_count, isOpenAdditional } = this.state;
        if (new_info) {
            index = existing_info_count + index
        }
        if (title !== 'specialties' && title != 'undefined' && title != undefined && title != "") {
            return (
                <li key={index}>
                    <div className="d-flex flex-wrap">
                        <span className="font-weight-bold mb-1">{title}</span>

                        {/* Hide when editing */}
                        <span className="ml-auto">
                            <div className="d-flex">
                                <div className="text-right mb-2">
                                    {value != "undefined" ? this.renderValues(value, title) : ""}
                                </div>
                                <div className="ml-2 interactive col-auto px-0">
                                    <div className="interactive-appear">
                                        <div className="text-nowrap">
                                            {value == "undefined" ?
                                                <button type="button" className="btn-sm"
                                                    onClick={
                                                        () => this.toggleEditState({ idx: index, item })
                                                    }>
                                                    {"+ Add"}
                                                </button>
                                                :
                                                <EditBtn
                                                    onClick={async () => {
                                                        await this.setState({ isOpenAdditional: {} });
                                                        await this.toggleEditState({ idx: index, item });
                                                    }
                                                    }

                                                />
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                    {/* Show when editing */}
                    <div className="mb-4 py-2 shadow-sm bg-white px-2" hidden={isOpenAdditional && isOpenAdditional[item.uniquId] ? false : true}>
                        <div className="mb-2">

                            <Row form>

                                {this.state.newID != 0 && this.state.newID == (item && item.id ? item.id : item && item.value && item.value[0] && item.value[0].id) ?
                                    <>
                                        {this.state.amenities_list &&
                                            Array.isArray(this.state.amenities_list) &&
                                            this.state.amenities_list.length > 0 &&
                                            this.state.amenities_list.map((amenity, amenityIndex) => (
                                                <Col xs="auto" key={index}>
                                                    <Switch
                                                        value={this.userExists(value, amenity.option_value) ? 1 : 0}
                                                        name={amenity.option_value}
                                                        id={"amenityIndex_" + amenity.option_value}
                                                        onChange={(e) => this.handleEdit(e, index, item, amenity.option_value)}
                                                    />  {amenity.option_value}
                                                </Col>
                                            ))
                                        }
                                    </>
                                    : ""
                                }
                            </Row>
                        </div>

                    </div>
                </li>
            )
        }
    }

    userExists = (arr, val) => {
        let data = false
        let { newID } = this.state
        if (Array.isArray(arr) && arr.length > 0) {
            arr.map((el, index) => {
                if (el.value == val) {
                    data = true
                }
            })
            return data
        }
    }

    addAdditionalFields = ({ name, edit, index }) => {
        return (
            <li key={index} className="mb-2">
                <div className="d-flex mx-n1">
                    <div className="col-6 px-1"><span className="font-weight-bold">{name}</span></div>
                    <div className="col-6 px-1 text-right"><span className="actionable"><FontAwesomeIcon icon="plus" /> Add</span></div>
                </div>
            </li>
        )
    }

    changeCategories = (e) => {
        let { name, value } = e.target;
        this.setState({ [name]: value })
    }

    toggleEditState = ({ idx, del, item }) => {
        let { isEditVisible, additionInfo, isOpenAdditional } = this.state;
        //isEditVisible.pop();
        let lastkey = Object.keys(isOpenAdditional)[Object.keys(isOpenAdditional).length - 1];
        let newID;
        if (del) {
            isEditVisible[idx] = false;
        } else if (isOpenAdditional[idx]) {
            isOpenAdditional[idx] = false
        } else {
            //newID = item && item.id ? item && item.id : item && item.value && item.value[0] && item.value[0].id;
            newID = item && item.id ? item.id : item && item.value && item.value[0] && item.value[0].id;
            //isEditVisible[lastkey] = false;
            isEditVisible[idx] = true;
            isOpenAdditional[idx] = true
            this.setState({ amenities_list: '', newID: newID }, () => this.props.get_amenities_options(newID));


        }
        this.setState({ isEditVisible, CategoryName: item && item.name, CategoryId: newID, isOpenAdditional })

    }


    handleEdit = (e, index, itemData, valueType) => {
        const { add_info_map, isEditVisible, isOpenAdditional } = this.state
        if (e === 1) {

            let param = {}
            param["name"] = this.state.CategoryId
            param["info"] = this.state.CategoryName
            param["value"] = [valueType]
            param["multiple"] = true

            //isEditVisible[index] = false;
            isOpenAdditional[index] = false;
            this.setState({ isOpenAdditional })
            this.props.update_additional_info(param, this.state.corporateId)
        } else if (e === 0) {
            let param = {}
            param["delete_add_info"] = true
            param["name"] = itemData.name
            param["info"] = valueType

            this.props.delete_additional_info(param, this.state.corporateId)
            // this.toggleDeleteItems(valueType,itemData.name, )
        }
    }


    render() {
        let { all_additional_fields, isToogleAcceptCreditCards, additionInfo, allAdditionalInfo, additional_info_all_categories, saved_amenities_list, isEditVisible, sub_category_list, res_data_by_category } = this.state;


        return (
            <React.Fragment>
                <CollapseBasic title="Additional Information" isOpen={true}>
                    <div className="mb-2">

                        {/* Show If no data */}
                        <div hidden>
                            <span className="small"><i>{'No Info added so far.'}</i></span>
                        </div>

                        {/* Show If data */}
                        <div style={{ 'maxHeight': '300px', 'overflowY': 'auto' }}>
                            <ul className="list-unstyled mb-2 fs-14">
                                {additionInfo && Array.isArray(additionInfo) && additionInfo.length > 0 ?
                                    additionInfo.map((item, index) => {
                                        return this.renderItems({
                                            title: item.name,
                                            value: item.value,
                                            index,
                                            item,
                                            edit: this.toggleAcceptCreditCards,
                                            trash: this.toggleAcceptCreditCards
                                        },
                                            false)
                                    }) : null}
                            </ul>
                        </div>
                    </div>


                </CollapseBasic>
            </React.Fragment>
        )
    }
}


const mapState = (state) => {
    return {
        corporate_id: state.user.corporate_id,
        corporate_details: state.user.corporate_id_details,
        business_category: state.user.business_category,
        business_sub_category: state.user.business_sub_category,
        payment_update: state.user.payment_update,
        updated_additional_data: state.user.update_additional_info,
        sub_category_data: state.user.sub_category_data,
        amenities_data: state.user.amenities_data,
    }
}

const mapProps = (dispatch) => ({
    update_additional_info: (data, id) => dispatch(update_additional_info(data, id)),
    delete_additional_info: (data, id) => dispatch(delete_additional_info(data, id)),
    get_amenities_options: (data) => dispatch(get_amenities_options(data)),
});

export default connect(mapState, mapProps)(BranchAdditionalInfo);

// export default withRouter(connect(mapState)(branchAdditionalInfo));