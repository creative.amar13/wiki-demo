import React, { Component } from "react";
import CollapseBasic from "../atoms/collapse";
import {
  FormGroup,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  Button,
} from "reactstrap";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";
import { current_user_profile, add_name, add_position_value, add_password_value } from "../../actions/user";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { AvForm, AvField, } from 'availity-reactstrap-validation';
import { callApi } from "../../utils/apiCaller";
class BranchCSR extends Component {
  mapRef = React.createRef();

  constructor(props) {
    super(props);
    this.refNameForm = React.createRef();
    this.refPositionForm = React.createRef();
    this.refrefPassForm = React.createRef();
    this.state = {
      fullName: "",
      position: "",
      userEmail: null,
      profilePic: null,
      editName: true,
      fName: "",
      lName: "",
      isValidFullName: true,
      isTogglePosition: true,
      isValidPosition: true,
      profession: "",
      userID: null,
      isTogglePass: true,
      isValidPass: true,
      password1: "",
      password2: "",
      disablePass: true,
      branchName: '',
    };
    this.handleOnFileUploadChange = this.handleOnFileUploadChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.profileData) {
      let profileData = nextProps.profileData;
      let userData = profileData.user;

      if (userData) {
        this.setState({
          profileData,
          fullName: (profileData.user && profileData.user.first_name) + " " + (profileData.user && profileData.user.last_name),
          userEmail: profileData.user.email,
          profilePic: profileData.current_profile_file,
          fName: profileData.user.first_name,
          lName: profileData.user.last_name,
          position: profileData?.professional[0]?.profession.title,
          userID: profileData?.professional[0]?.profession.id
        });
      }
    }

    if (nextProps.newName) {
      let newNAme = nextProps.newName;
      let userDetail = newNAme && newNAme.data && newNAme.data.user;
      this.setState({
        fullName: (userDetail && newNAme.data.user.first_name)
          + " " + (userDetail && newNAme.data.user.last_name),
        fName: userDetail && newNAme.data.user.first_name,
        lName: userDetail && newNAme.data.user.last_name,
      });
    }

    if (nextProps.newPosition && nextProps.newPosition.data) {
      let data = nextProps.newPosition.data;
      this.setState({
        position: data.professional[0].profession.title,
      });
    }
    if (nextProps && nextProps.corporate_details) {

      this.setState({
        branchName: nextProps.corporate_details?.name,
      });
    }
  }

  EditName = () => {
    let editName = this.state.editName;
    editName = !editName;

    if (!editName) {
      this.setState({ isValidFullName: false });
    }
    this.setState({ editName });
  };

  handleChange = (e) => {
    let hasError = this.refNameForm.current._inputs['fname'].context.FormCtrl.hasError()
    this.setState({
      fName: e.target.value,
    }, () => { this.checkValidate(hasError) });
  };

  handleChange2 = (e) => {
    let hasError = this.refNameForm.current._inputs['lname'].context.FormCtrl.hasError()
    this.setState({
      lName: e.target.value,
    }, () => { this.checkValidate(hasError) });
  };

  checkValidate = (hasError) => {
    let firstName = this.state.fName;
    let lastName = this.state.lName;
    let isValidFullName = this.state.isValidFullName;

    if (firstName !== '' && lastName !== '' && firstName.match('^[a-zA-Z ]*$') !== null && lastName.match('^[a-zA-Z ]*$') !== null) {
      this.setState({ isValidFullName: false })
    } else {
      if (isValidFullName !== true) {
        this.setState({ isValidFullName: true })
      }
    }

  }

  saveName = () => {
    let firstName = this.state.fName;
    let lastName = this.state.lName;

    if (firstName !== '' && lastName !== '') {
      let data = { "preFill": true, "user": { "last_name": lastName, "first_name": firstName, "email": this.state.userEmail } };
      this.props.add_name(data);
    }

    this.setState((state) => ({
      editName: !state.editName,
    }));

  }

  savePosition = () => {
    let position = this.state.position;
    let id = this.state.userID;


    if (position && position !== '') {
      let data = { "position": { "key": position, "id": id, "label": "title" } };
      this.props.add_position_value(data);
    }
    this.setState({ isTogglePosition: true })

  }

  handlePosition = (e) => {
    let hasError = this.refPositionForm.current._inputs['position'].context.FormCtrl.hasError();
    this.setState({
      position: e.target.value,
    }, () => { this.checkPositionValidate(hasError) });
  };

  checkPositionValidate = (hasError) => {
    let { position, isValidPosition } = this.state;

    if (position !== '' && position.match('^[a-zA-Z ]*$') !== null) {
      this.setState({ isValidPosition: false })
    } else {
      if (isValidPosition !== true) {
        this.setState({ isValidPosition: true })
      }
    }
  }

  EditPosition = () => {
    let { isTogglePosition, position } = this.state;

    isTogglePosition = !isTogglePosition;

    if (!isTogglePosition) {
      this.setState({ isTogglePosition, isValidPosition: true })
    }
    //this.setState({ isTogglePosition, isValidPosition: false, position: profileData.professional[0].profession.title, });
    this.setState({ isTogglePosition, isValidPosition: false, position: position, });
  }

  EditPassword = () => {
    let { isTogglePass } = this.state;
    isTogglePass = !isTogglePass;

    if (isTogglePass) {
      this.setState({ isTogglePass: true })
    }
    else {
      this.setState({ isTogglePass: false })
    }

  }

  handlePassword = async (e) => {
    const value = e.target.value;
    await this.setState({
      ...this.state,
      [e.target.name]: value,
      // isTogglePass: true
    });
    if (this.state.password1 === "") {
      this.setState({ disablePass: true, isValidPass: true })
    }
    else if (this.state.password1 !== "") {
      this.setState({ disablePass: false })
    }
    if (this.state.password2 !== "") {
      if (this.state.password1 !== this.state.password2) {
        this.setState({ isValidPass: true })
      }
      else if (this.state.password1 !== "" && this.state.password2 !== "" && this.state.password1 === this.state.password2) {
        this.setState({ isValidPass: false })
      }
    }
  };

  savePassword = () => {
    let pass1 = this.state.password1;
    let pass2 = this.state.password2;

    if (pass1 !== '' && pass2 !== '' && pass1 === pass2) {

      let data = { name: "password", pk: 1, password1: pass1, password2: pass2 };
      this.props.add_password_value(data);
      this.setState({ isValidPass: false, isTogglePass: true })
    }


  }
  passValidation = () => {
    return <p>Enter Correct Password</p>
  }
  handleOnFileUploadChange = (event) => {
    let uploadFile = event.target.files;
    let extName = "";
    for (const key of Object.keys(uploadFile)) {
      let itemType = uploadFile[key].type.split("/");
      extName = itemType[0];
    }
    if (extName === "image") {
      var i = 0;
      let data = new FormData();
      data.append("file", uploadFile[i]);
      callApi(
        `/upload/multiuploader/?album=profile&corp=1`,
        "POST",
        data,
        true
      ).then((response) => {
        this.props.current_user_profile();
      });
    } else {
      // console.log("Image is invalid")
      toast(`Image is invalid`, {
        autoClose: 2500,
        className: "toast-container",
        toastClassName: "dark-toast",
        bodyClassName: "red-hunt",
        progressClassName: "cc",
      });
    }
  };


  render() {
    let { fullName, userEmail, profilePic, isTogglePosition, position, isTogglePass, profileData } = this.state;
    let { current_role_and_permissions } = this.props;

    return (
      <React.Fragment>
        <CollapseBasic title={current_role_and_permissions && current_role_and_permissions.role === '' ? "Corporate Admin" : this.state.branchName} isOpen={true}>
          <div>
            {/* Profile Image */}
            <div className="text-center">
              <div className="profile-pic-holder">
                <img
                  className="profile-pic"
                  src={profilePic}
                  alt="Profile Image"
                />
                <Label for="newProfilePhoto" className="upload-file-block">
                  <div className="text-center">
                    <div className="mb-2">
                      <FontAwesomeIcon icon="camera" size="2x" />
                    </div>
                    <div className="text-uppercase">
                      Change <br /> Profile Photo
                    </div>
                  </div>
                </Label>
                <Input
                  type="file"
                  name="profile_pic"
                  id="newProfilePhoto"
                  accept="image/*"
                  style={{ display: "none" }}
                  onChange={this.handleOnFileUploadChange}
                />
              </div>

              {/* Show Name, hide when updating */}
              <div className="mt-2">
                <span
                  className="text-secondary-dark font-weight-bold actionable"
                  title="Update"
                >
                  {fullName && fullName != null ? fullName : "Provide Your First and Last Name"}
                </span>
              </div>

              <div>
                <div className="d-flex text-center justify-content-center">
                  <span className="actionable" onClick={this.EditName}>
                    Update
                  </span>
                </div>
              </div>

              {/* Update Name field, show when updating */}

              <div className={this.state.editName ? "d-none" : "mt-2"}>
                <AvForm ref={this.refNameForm} onValidSubmit={this.saveName}>
                  <AvField
                    className="mr-2"
                    bsSize="sm"
                    type="text"
                    name="fname"
                    placeholder="First Name"
                    value={this.state.fName}
                    onChange={(e) => {
                      this.handleChange(e);
                    }}
                    validate={{
                      pattern: { value: '^[a-zA-Z ]*$' }
                    }}
                  />

                  <AvField
                    bsSize="sm"
                    type="text"
                    name="lname"
                    placeholder="Last Name"
                    value={this.state.lName}
                    onChange={(e) => {
                      this.handleChange2(e);
                    }}
                    validate={{
                      pattern: { value: '^[a-zA-Z ]*$' }
                    }}
                  />
                  <div className="text-right">
                    <Button disabled={this.state.isValidFullName} size="sm" className="btn-icon-split mr-2" title="Save">
                      <span className="text">Save</span>
                      <span className="icon">
                        <FontAwesomeIcon icon="check" />
                      </span>
                    </Button>
                    <Button onClick={this.EditName} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                      <span className="text">
                        {'Cancel'}
                      </span>
                      <span className="icon">
                        <FontAwesomeIcon icon="times" />
                      </span>
                    </Button>
                  </div>
                  {/* <InputGroup className="mb-2">
                    <InputGroupAddon addonType="append">
                      <Button disabled={this.state.isValidFullName} className="py-0">
                        <FontAwesomeIcon icon="arrow-right" />{" "}
                      </Button>
                    </InputGroupAddon>
                  </InputGroup>
                  <div className="text-right">
                    <Button color="dark" size="sm" onClick={this.EditName}>
                      Cancel
                  </Button>
                  </div> */}
                </AvForm>
              </div>
            </div>

            {/* User Info */}
            <div>
              <FormGroup>
                <Label size="sm" className="p-0 mb-1">
                  Position
                </Label>
                {/* If not provide ever */}
                <div hidden>
                  <span className="is-empty">Please provide your position</span>
                </div>
                {/* If Provided */}
                <div>
                  <div className="d-flex">
                    <span className="mr-2 wb-break-word">{position ? position : "Add your position"}</span>
                    <span className="actionable ml-auto" onClick={this.EditPosition}>{position ? "Update" : "Add"}</span>
                  </div>
                </div>
                {/* When clicked on Update show below */}
                <div className={isTogglePosition ? "d-none" : "mt-3"}>
                  <AvForm ref={this.refPositionForm} onValidSubmit={this.savePosition}>
                    <AvField
                      bsSize="sm"
                      type="text"
                      name="position"
                      placeholder="Enter Position..."
                      value={current_role_and_permissions && current_role_and_permissions.role === '' ? position : position}
                      onChange={(e) => {
                        this.handlePosition(e);
                      }}
                      validate={{
                        pattern: { value: '^[a-zA-Z ]*$' }
                      }}
                    />

                    <div className="text-right">
                      <Button disabled={this.state.isValidPosition} size="sm" className="btn-icon-split mr-2" title="Save">
                        <span className="text">Save</span>
                        <span className="icon">
                          <FontAwesomeIcon icon="check" />
                        </span>
                      </Button>
                      <Button onClick={() => {
                        this.setState({
                          position: profileData.professional && profileData.professional[0] && profileData.professional[0].profession && profileData.professional[0].profession.title, isTogglePosition: true
                        })
                      }} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                        <span className="text">
                          {'Cancel'}
                        </span>
                        <span className="icon">
                          <FontAwesomeIcon icon="times" />
                        </span>
                      </Button>
                    </div>

                  </AvForm>
                </div>
              </FormGroup>

              <FormGroup>
                <Label size="sm" className="p-0 mb-1">
                  Email
                </Label>
                <div hidden>
                  <span className="is-empty">Please provide your Email</span>
                </div>
                <div>
                  <div className="d-flex">
                    <span className="mr-2">{userEmail}</span>{" "}

                  </div>
                </div>
                <div hidden>
                  <InputGroup className="mb-2">
                    <Input
                      bsSize="sm"
                      type="text"
                      name="email"
                      placeholder="Enter Email..."
                    />
                    <InputGroupAddon addonType="append">
                      <Button className="py-0">
                        <FontAwesomeIcon icon="arrow-right" />{" "}
                      </Button>
                    </InputGroupAddon>
                  </InputGroup>
                  <div className="text-right">
                    <Button color="dark" size="sm">
                      Cancel
                    </Button>
                  </div>
                </div>
              </FormGroup>

              <FormGroup>
                <Label size="sm" className="p-0 mb-1">
                  Password
                </Label>
                <div>
                  <div className="d-flex">
                    <span className="mr-2">********</span>{" "}
                    <span className="actionable ml-auto" onClick={this.EditPassword}>Update</span>
                  </div>
                </div>
                <div className={isTogglePass ? "d-none" : "mt-3"}>
                  <AvForm ref={this.refPassForm} onValidSubmit={this.savePassword}>
                    <FormGroup className="mb-2">
                      <AvField
                        bsSize="sm"
                        type="password"
                        name="password1"
                        placeholder="New Password"
                        value={this.state.password1}
                        onChange={(e) => {
                          this.handlePassword(e);
                        }}

                      />
                    </FormGroup>
                    <FormGroup className="mb-2">
                      <AvField
                        bsSize="sm"
                        type="password"
                        name="password2"
                        placeholder="Re-Enter Password"
                        value={this.state.password2}
                        onChange={(e) => {
                          this.handlePassword(e);
                        }}
                        disabled={this.state.disablePass}
                      />
                    </FormGroup>

                    <div className="text-right">
                      <Button
                        size="sm"
                        className="btn-icon-split mr-2"
                        title="Save"
                        disabled={this.state.isValidPass}
                      >
                        {" "}
                        <span className="text">Save</span>{" "}
                        <span className="icon">
                          {" "}
                          <FontAwesomeIcon icon="check" />{" "}
                        </span>
                      </Button>
                      <Button
                        size="sm"
                        title="Cancel"
                        color="dark"
                        className="btn-icon-split ml-0"
                        onClick={this.EditPassword}
                      >
                        {" "}
                        <span className="text">Cancel</span>{" "}
                        <span className="icon">
                          <FontAwesomeIcon icon="times" />
                        </span>{" "}
                      </Button>
                    </div>
                  </AvForm>
                </div>
              </FormGroup>
            </div>
          </div>
        </CollapseBasic>
      </React.Fragment>
    );
  }
}

const mapState = (state) => {
  return {
    profileData: state.user.current_user,
    newName: state.user.new_name,
    newPosition: state.user.new_position,
    current_role_and_permissions: state.user.current_role_and_permissions,
    corporate_details: state.user.corporate_id_details,
    // profession_data: state.user
  };
};

const mapProps = (dispatch) => {
  return {
    current_user_profile: () => dispatch(current_user_profile()),
    add_name: (data) => dispatch(add_name(data)),
    add_position_value: (data) => dispatch(add_position_value(data)),
    add_password_value: (data) => dispatch(add_password_value(data))
  };
};

export default withRouter(connect(mapState, mapProps)(BranchCSR));
