import React, { Component, } from 'react';
import CollapseBasic from '../atoms/collapse';
import { Badge, Row, Col } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import Switch from 'react-input-switch';
import {
    update_additional_info,
    delete_additional_info,
    get_amenities_options
} from "../../actions/user";
import EditBtn from "../atoms/editBtn";
class BranchAdditionalInfo extends Component {
    constructor(props) {
        super(props)
        this.myRef = React.createRef()
        this.state = {
            additionInfo: [],
            allAdditionalInfo: [],
            all_additional_fields: [],
            isEditVisible: {},
            CategoryName: '',
            CategoryId: '',
            corporateId: '',
            existing_info_count: 0,
            amenities_list: '',
            isOpenAdditional: {},
            isOpenCategory: {},
            newID: 0
        }
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps && nextProps.corporate_details && nextProps.corporate_details.all_additional_fields_value) {
            let item = nextProps.corporate_details.all_additional_fields_value;
            // let result = this.sortObject(item);
            let result = item;
            let newMergeresult = result.concat(nextProps?.corporate_details?.all_additional_fields)
            // let newMergeresult = nextProps?.corporate_details?.all_additional_fields
            newMergeresult &&
                Array.isArray(newMergeresult) &&
                newMergeresult.length > 0 &&
                newMergeresult.forEach((nitem, index) => {
                    /*if (nitem) {
                        newMergeresult[index]["uniquId"] = index;
                    }
                    if (nitem.value) {
                       newMergeresult[index]["children"] = this.sortObject(nitem.value)
                    }*/
					if (nitem) {
                        newMergeresult[index]["uniquId"] = index;
                    }
                    if (nitem.value) {
                        // this.sortObject(nitem.value)
                        newMergeresult[index]["children"] = this.sortObject(nitem.value)
                        newMergeresult[index]["type"] = 'Edit'
                    } else {
                        newMergeresult[index]["type"] = 'Add'

                    }
                });
            this.setState({
                additionInfo: newMergeresult,
                existing_info_count: result.length,
                corporateId: nextProps.corporate_details.id,
                all_additional_fields: nextProps?.corporate_details?.all_additional_fields,
            },()=>{
				this.props.handleSelectedAdditionalInfo(this.state.additionInfo);
			})
        }

        if (nextProps && nextProps.amenities_data) {
            this.setState({
                amenities_list: nextProps.amenities_data,
                CategoryId: nextProps.amenities_data &&
                    nextProps.amenities_data[0] &&
                    nextProps.amenities_data[0].reviews_additionalattributes
            })

        }
    }

    sortObjectOld = (items) => {
        let { isEditVisible } = this.state;
        let filteredItems = [];
        var myObj = items,
            keys = Object.keys(myObj),
            i, k, len = keys.length;

        let filteredKeys = items &&
            Array.isArray(items) &&
            items.filter(item => (item?.name)?.search('_') < 0);
        filteredKeys = filteredKeys && Array.isArray(filteredKeys) && filteredKeys.length > 0 && filteredKeys.sort((a, b) => {
            let aItems = a.toLowerCase();
            let bItems = b.toLowerCase();
            if (aItems < bItems) { return -1; }
            if (aItems > bItems) { return 1; }
            return 0;
        });

        for (i = 0; i < len; i++) {
            k = filteredKeys[i];
            if (k !== undefined) {
                isEditVisible[i] = false;
                filteredItems.push({ "name": k, "value": myObj[k] })
            }
        }
        this.setState({ isEditVisible })
        return filteredItems;
    }
    sortObject = (items) => {
        console.log('item', items)
        let { isEditVisible } = this.state;
        let filteredItems = [];
        var myObj = items,
            keys = Object.keys(myObj),
            i, k, len = keys.length;

        let filteredKeys = keys.filter(item => item.search('_') < 0);
        filteredKeys = filteredKeys && Array.isArray(filteredKeys) && filteredKeys.sort((a, b) => {
            let aItems = a.toLowerCase();
            let bItems = b.toLowerCase();
            if (aItems < bItems) { return -1; }
            if (aItems > bItems) { return 1; }
            return 0;
        });
        for (i = 0; i < len; i++) {
            k = filteredKeys[i];
            if (k !== undefined) {
                isEditVisible[i] = false;
                filteredItems.push({ "name": k, "value": myObj[k] })
            }
        }
        this.setState({ isEditVisible })
        return filteredItems;
    }

    sortData = (items) => {
        let filteredKeys = items && Array.isArray(items) && items.length > 0 && items.filter(item => (item?.name)?.search('_') < 0);
        filteredKeys = filteredKeys &&
            Array.isArray(filteredKeys) &&
            filteredKeys.length > 0 &&
            filteredKeys.sort((a, b) => {
                let aItems = (a.name).toLowerCase();
                let bItems = (b.name).toLowerCase();
                if (aItems < bItems) { return -1; }
                if (aItems > bItems) { return 1; }
                return 0;
            });
        return filteredKeys;
    }

    toggleDeleteItems = (item, title) => {
        let param = {}
        param["delete_add_info"] = true
        param["name"] = title
        param["info"] = item.value
        this.props.delete_additional_info(param, this.state.corporateId)
    }

    renderValues = (value, title) => {
        let itemsRender = []
        if (value && Array.isArray(value) && value.length > 0) {
            value.map((item, index) => {
                let currentIndex = index + 1;
                if (item.value && typeof (item.value) == "string") {
                    itemsRender.push(
                        <React.Fragment key={index}>
                            <Badge color="default" className="mr-1 fs-12">
                                <div className="d-flex align-items-center">
                                    <span className="mr-1 font-weight-normal">{item.value}</span>
                                    <button onClick={() => this.toggleDeleteItems(item, title)} type="button" className="close btn-sm" aria-label="Close">
                                        <FontAwesomeIcon icon="times-circle" />
                                    </button>
                                </div>
                            </Badge>
                        </React.Fragment>
                    )
                }
                return null;
            })
        }
        return (
            itemsRender
        )
		
    }


    renderItems = ({ title, value, index, item, parentId, parentName }, new_info = false) => {
        let { existing_info_count, isOpenAdditional } = this.state;
        if (new_info) {
            index = existing_info_count + index
        }
        if (title !== 'specialties' && title != 'undefined' && title != undefined && title != "") {
            return (
                <li key={index}>
                    <div className="d-flex flex-wrap">
                        <span className="font-weight-bold mb-1">{title}</span>

                        {/* Hide when editing */}
                        <span className="ml-auto">
                            <div className="d-flex">
                                <div className="text-right mb-2">
                                    {value != "undefined" ? this.renderValues(value, title) : ""}
                                </div>
                                <div className="ml-2 interactive col-auto px-0">
                                    <div className="interactive-appear">
                                        <div className="text-nowrap">
                                            {value == "undefined" ?
                                                <button type="button" className="btn btn-sm text-secondary-dark"
                                                    onClick={async () => {
                                                        await this.setState({ isOpenAdditional: {} });
                                                        await this.toggleEditState({ idx: item?.id, item });
                                                    }
                                                    }
                                                >
                                                    <FontAwesomeIcon icon="plus" /> {"Add"}
                                                </button>
                                                :
                                                <EditBtn
                                                    onClick={async () => {
                                                        await this.setState({ isOpenAdditional: {} });
                                                        await this.toggleEditState({ idx: index, item });
                                                    }
                                                    }
                                                />
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                    {/* Show when editing */}
                    <div className="mb-4 py-2 shadow-sm bg-white px-2" hidden={isOpenAdditional && isOpenAdditional[item.id] ? false : true}>
                        <div className="mb-2">

                            <Row form>

                                {this.state.newID != 0 && this.state.newID == (item && item.id ? item.id : item && item.value && item.value[0] && item.value[0].id) ?
                                    <>
                                        {this.state.amenities_list &&
                                            Array.isArray(this.state.amenities_list) &&
                                            this.state.amenities_list.length > 0 &&
                                            this.state.amenities_list.map((amenity, amenityIndex) => (
                                                <Col xs="auto" key={index}>
                                                    <Switch
                                                        value={this.userExists(value, amenity.option_value) ? 1 : 0}
                                                        name={amenity.option_value}
                                                        id={"amenityIndex_" + amenity.option_value}
                                                        onChange={(e) => this.handleEdit(e, index, item, amenity.option_value, parentId, parentName)}
                                                    />  {amenity.option_value}
                                                </Col>
                                            ))
                                        }
                                    </>
                                    : ""
                                }
                            </Row>
                        </div>

                    </div>
                </li>
            )
        }
    }

    userExists = (arr, val) => {
        let data = false
        let { newID } = this.state
        if (Array.isArray(arr) && arr.length > 0) {
            arr.map((el, index) => {
                if (el.value == val) {
                    data = true
                }
            })
            return data
        }
    }

    toggleEditState = ({ idx, del, item }) => {
        let { isEditVisible, isOpenAdditional } = this.state;
        let lastkey = Object.keys(isOpenAdditional)[Object.keys(isOpenAdditional).length - 1];
        let newID;
        if (del) {
            isEditVisible[idx] = false;
        } else if (isOpenAdditional[item?.id]) {
            isOpenAdditional[item?.id] = false
        } else {
            newID = item && item.id ? item.id : item && item.value && item.value[0] && item.value[0].id;
            isEditVisible[idx] = true;
            isOpenAdditional[item?.id] = true
            this.setState({ amenities_list: '', newID: newID }, () => this.props.get_amenities_options(newID));
        }
        this.setState({ isEditVisible, CategoryName: item && item.name, CategoryId: newID, isOpenAdditional })
    }
    handleOnExpand = (expand, id) => {
        let { isOpenCategory } = this.state;
        isOpenCategory[id] = expand;
        this.setState({ isOpenCategory });
    }


    handleEdit = (e, index, itemData, valueType, parentId, parentName) => {
        const { isOpenAdditional } = this.state
        if (e === 1) {
            let param = {}
            param["name"] = this.state.CategoryId
            param["info"] = this.state.CategoryName
            param["value"] = [valueType]
            param["multiple"] = true
            param["parentId"] = parentId ? parentId : "";
            param["parentName"] = parentName ? parentName : "";

            isOpenAdditional[index] = false;
            this.setState({ isOpenAdditional })
            this.props.update_additional_info(param, this.state.corporateId)
        } else if (e === 0) {
            let param = {}
            param["delete_add_info"] = true
            param["name"] = itemData.name
            param["info"] = valueType

            this.props.delete_additional_info(param, this.state.corporateId)
            // this.toggleDeleteItems(valueType,itemData.name, )
        }
    }


    render() {
        let { isOpenCategory, additionInfo, } = this.state;
        

        return (
            <React.Fragment>
                <CollapseBasic title="Additional Information" isOpen={true}>
                    <div className="mb-2">

                        {/* Show If no data */}
                        <div hidden>
                            <span className="small"><i>{'No Info added so far.'}</i></span>
                        </div>

                        {/* Show If data */}
                        <div style={{ 'maxHeight': '300px', 'overflowY': 'auto' }}>
                            {/* <ul className="list-unstyled mb-2 fs-14">
                                {additionInfo && Array.isArray(additionInfo) && additionInfo.length > 0 ?
                                    additionInfo.map((item, index) => {
                                        return this.renderItems({
                                            title: item.name,
                                            value: item.value,
                                            index,
                                            item,
                                            edit: this.toggleAcceptCreditCards,
                                            trash: this.toggleAcceptCreditCards
                                        },
                                            false)
                                    }) : null}
                            </ul> */}
                            <ul className="list-unstyled mb-2 fs-14">
                                {additionInfo && Array.isArray(additionInfo) && additionInfo.length > 0 ?
                                    additionInfo.map((item, index) => {
                                        if (item.parentId) {
                                            return (
                                                <li key={item.uniquId}>
                                                    <div className="border-bottom">
                                                        <div className={`d-flex flex-nowrap align-items-start py-2 px-1 add_info_type_${item.type.toLowerCase()}`}>
                                                            <div className="font-weight-bold flex-grow-1 mr-2" key={item.uniquId}>{item.parentName}</div>
                                                            {isOpenCategory && isOpenCategory[item?.uniquId] ?
                                                                <div className="flex-shrink-0">
                                                                    <span className="d-inline-block" role="button" tabIndex={0} onClick={async () => {
                                                                        this.handleOnExpand(false, item.uniquId);
                                                                    }}>
                                                                        <FontAwesomeIcon icon="minus" />
                                                                    </span>
                                                                </div>
                                                                : <div className="flex-shrink-0">
                                                                    <span className="d-inline-block" role="button" tabIndex={0} onClick={async () => {
                                                                        this.handleOnExpand(true, item.uniquId);
                                                                    }}>
                                                                        <span className="mr-2">{item.type}</span><FontAwesomeIcon icon="plus" />
                                                                    </span>
                                                                </div>
                                                            }
                                                        </div>
                                                        {isOpenCategory && isOpenCategory[item.uniquId] ?
                                                            <ul className="list-unstyled mb-2 fs-14 pl-3">
                                                                {item.children && Array.isArray(item.children) && item.children.length > 0 ?
                                                                    item.children.map((itemSubcategory, indexSub) => {
                                                                        return (
                                                                            this.renderItems({
                                                                                title: itemSubcategory.name,
                                                                                value: itemSubcategory.value,
                                                                                index: indexSub,
                                                                                item: itemSubcategory,
                                                                                parentId: item.parentId,
                                                                                parentName: item.parentName,
                                                                            },
                                                                                false)
                                                                        )
                                                                    }) : null}
                                                            </ul>
                                                            : ""}
                                                    </div>
                                                </li>
                                            )
                                        }
                                    }) : null}
                            </ul>
                        </div>
                    </div>


                </CollapseBasic>
            </React.Fragment>
        )
    }
}


const mapState = (state) => {
    return {
        corporate_id: state.user.corporate_id,
        corporate_details: state.user.corporate_id_details,
        business_category: state.user.business_category,
        business_sub_category: state.user.business_sub_category,
        payment_update: state.user.payment_update,
        updated_additional_data: state.user.update_additional_info,
        sub_category_data: state.user.sub_category_data,
        amenities_data: state.user.amenities_data,
    }
}

const mapProps = (dispatch) => ({
    update_additional_info: (data, id) => dispatch(update_additional_info(data, id)),
    delete_additional_info: (data, id) => dispatch(delete_additional_info(data, id)),
    get_amenities_options: (data) => dispatch(get_amenities_options(data)),
});

export default connect(mapState, mapProps)(BranchAdditionalInfo);

// export default withRouter(connect(mapState)(branchAdditionalInfo));