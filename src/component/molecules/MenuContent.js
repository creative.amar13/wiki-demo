import React, { Component } from 'react';
import CollapseBasic from '../atoms/collapse';
import { FormGroup, Modal, ModalHeader, ModalBody, ModalFooter, InputGroupText, Label, Input, InputGroup, InputGroupAddon, Button, Row, Col, Progress, ButtonGroup, Carousel, CarouselControl, CarouselItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

// import { get_map_cordinates, get_menu_list, corporate_id_details, corporate_id, add_service_item, get_menu_content } from '../../actions/user';
import {
    get_map_cordinates,
    // get_menu_list,
    corporate_id_details, corporate_id, add_service_item, get_album_types_list,
    get_album_type_data,
    delete_selected_gallery_media_Upload,
    add_service_media,
    // get_menu_content,
    delete_menu_item
} from '../../actions/user';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, CardTitle, CardText } from 'reactstrap';
import classnames from 'classnames';
import { callApi } from "../../utils/apiCaller";
import { stat } from 'fs';

const menuItemImagesList = [
    {
        id: 5,
        src: 'https://userdatawikireviews.s3.amazonaws.com/media/content/1576217453093.8fc87b9e96593149a0843e5113e157381f812f9e.jpg',
        altText: 'Uploaded Photo 1'
    },
    {
        id: 2,
        src: 'https://userdatawikireviews.s3.amazonaws.com/media/content/1576206413032.87eefd1076f12330a707133670b9b40f59ff2d7d.jpg',
        altText: 'Uploaded Photo 2'
    },
    {
        id: 3,
        src: 'https://userdatawikireviews.s3.amazonaws.com/media/content/maxresdefault.4d660be44ce3bb284961971326c40fcad6d7f61e.jpg',
        altText: 'Uploaded Photo 3'
    }
];



class MenuContent extends Component {
    mapRef = React.createRef();

    constructor(props) {
        super(props);
        this.state = {
            activeMenuItemIndex: 0,
            animatingMenuItem: false,
            editMenuItemSection: false,
            menuItemImages: menuItemImagesList,
            menu_item_values:{}
        };
    }

    nextMenuItem = () => {
        const {menuItemImages} = this.state
        if (this.state.animatingMenuItem) return;
        const nextIndex = this.state.activeMenuItemIndex === menuItemImages.length - 1 ? 0 : this.state.activeMenuItemIndex + 1;
        this.setState({ activeMenuItemIndex: nextIndex })
    }

    previousMenuItem = () => {
        const {menuItemImages} = this.state
        if (this.state.animatingMenuItem) return;
        const nextIndex = this.state.activeMenuItemIndex === 0 ? menuItemImages.length - 1 : this.state.activeMenuItemIndex - 1;
        this.setState({ activeMenuItemIndex: nextIndex })
    }

    handleDelete = (item, id) => {
        const {menuItemImages} = this.state
        // menuItemImages
        menuItemImages.splice(id,1)
        this.setState({
            menuItemImages,
        })
        // this.props.delete_menu_item(item)
    }

    handleEdit = (dtValue, dtKey) => {
        // const [] = dtValue
        const {name, content} = this.props
        this.setState({editMenuItemSection: true,
            sectionId : dtValue[4],
            section_name: dtKey[0] ? dtKey[0] : "",
            price: dtValue[7] ? Number.parseInt(dtValue[7], 10) : 0, 
            menu_type_name: Object.keys(content)[0] ? Object.keys(content)[0] : '', 
            menu_name : name,
            menu_description: dtValue[2] ? dtValue[2] : "",
            menu_item_values: dtValue})

    }

//     menuItemSlides = () => {
//         const {menuItemImages} = this.state
//         menuItemImages.map((item) => {
//         return (
//             <CarouselItem
//                 onExiting={() => this.setState({ animatingMenuItem: true })}
//                 onExited={() => this.setState({ animatingMenuItem: false })}
//                 key={item.id}
//             >
//                 <div className="text-center">
//                     <img className="img-fluid" src={item.src} alt={item.altText} />
//                 </div>
//             </CarouselItem>
//         )
//     });
// }


handleDeleteList = (item,key) => {
    const {deleteMenu} = this.props
    deleteMenu(item[4], key[0])
    // const [image] = item
}

    subItems =(item) => {
        const { menuItemImages } = this.state
        
        if(item && Array.isArray(item) && item.length > 0){
            return item.map(dt => {
                
                let dtKey = Object.keys(dt);
                let dtValue = dt[dtKey];
                //console.log("ITEMITEMITEM", dtValue);
                // console.log({item ,dt})
                // for(let {key, value} in dt){
                    //console.log("ddddd",{key, value});
                    return (
                        <React.Fragment>
                    {/* <span>{`${dtKey} ${dtValue[0]}`}</span> */}
                    <Row>

                                    {/* Repeat this for items */}
                                    {/* Use lg={6} default and lg={12} when viewing images below column*/}
                                    <Col className="mb-3" lg={12}>
                                        <Row noGutters>
                                            <Col xs={12} lg={6}>
                                                <div className="d-flex flex-nowrap mx-n2">
                                                    <div className="px-2">
                                                        <img width="50" src={'https://stagingdatawikireviews.s3.amazonaws.com/images/placeholder.png'} alt="Image" />
                                                    </div>
                                                    <div className="px-2">
                                                        <div className="d-flex">
                                                            <div className="mr-2">
                                                                <a className="text-secondary-dark mr-2 font-weight-bold" href="#">{`${dtKey}`} </a>

                                                                <div className="text-secondary">
                                                                    <img src={`${dtValue[0]}`} alt={`${dtValue[1]}`} className="mr-2" />
                                                                    <span>{`${dtValue[3]}`} reviews</span>
                                                                </div>
                                                            </div>

                                                            <div className="px-0 col-auto">
                                                                <div className="interactive">
                                                                    <div className="interactive-appear">
                                                                        <div className="text-nowrap">
                                                                            <ButtonGroup>
                                                                                <Button
                                                                                    onClick={() => this.handleEdit(dtValue, dtKey, )}
                                                                                    size="sm" color="dark" title="Edit">
                                                                                    <FontAwesomeIcon icon="edit" />
                                                                                </Button>
                                                                                <Button
                                                                                    size="sm" color="danger" title="Delete" onClick={() => this.handleDeleteList(dtValue, dtKey)}>
                                                                                    <FontAwesomeIcon icon="trash-alt" />
                                                                                </Button>
                                                                            </ButtonGroup>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="text-secondary-dark">
                                                            <p className="fs-14 mb-0">
                                                            {`${dtValue[2]}`}
                                                            </p>
                                                            <div className="mt-1">
                                                                <span>${`${dtValue[7]}`}</span>
                                                            </div>
                                                        </div>
                                                        <div className="mt-1">
                                                            <Button color="link" className="text-secondary-dark fs-14 font-weight-bold p-0">
                                                                <FontAwesomeIcon icon="camera" className="mr-2" />
                                                                Add Image
                                                            </Button>
                                                            <Button color="link" className="text-secondary-dark fs-14 font-weight-bold p-0">
                                                                View Images
                                                            </Button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>

                                            {/* Show when clicked on view images */}
                                            <Col xs={12} lg={6}>
                                                <div className="pl-3 mt-3 mt-lg-0">
                                                    <div className="d-flex flex-nowrap mx-n1">
                                                        <div className="px-1">
                                                            <Carousel
                                                                activeIndex={this.state.activeMenuItemIndex}
                                                                next={this.nextMenuItem}
                                                                previous={this.previousMenuItem}
                                                            >
                                                                {menuItemImages.map((item) => (
                                                                        <CarouselItem
                                                                            onExiting={() => this.setState({ animatingMenuItem: true })}
                                                                            onExited={() => this.setState({ animatingMenuItem: false })}
                                                                            key={item.id}
                                                                        >
                                                                            <div className="text-center">
                                                                                <img className="img-fluid" src={item.src} alt={item.altText} />
                                                                            </div>
                                                                        </CarouselItem>
                                                                ))
                                                            }
                                                                
                                                                {/* {this.menuItemSlides} */}
                                                                <CarouselControl className="bg-dark" direction="prev" directionText="Previous" onClickHandler={this.previousMenuItem} />
                                                                <CarouselControl className="bg-dark" direction="next" directionText="Next" onClickHandler={this.nextMenuItem} />
                                                            </Carousel>
                                                        </div>

                                                        <div className="text-right px-1 d-flex flex-column">
                                                            <div>
                                                                <Button color="transparent" title="Close View">
                                                                    <FontAwesomeIcon icon="times" className="text-secondary" />
                                                                </Button>
                                                            </div>
                                                            <div className="mt-auto">
                                                                <Button color="transparent" title="Delete Image" onClick={()=>this.handleDelete(dtKey, this.state.activeMenuItemIndex)}>
                                                                    <FontAwesomeIcon icon="trash-alt" className="text-danger" />
                                                                </Button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                                </React.Fragment>
                    )
                // }
            })
        }
    }

    renderItems = (data) =>{
        let dataItems = [];
        if(Object.keys(data).length > 0){
            Object.keys(data).forEach((element,index) => {
                let item = data[element];
                
                dataItems.push(
                    <CollapseBasic title={element} key={index}>
                       
                        {this.subItems(item)}
                    </CollapseBasic>
                )
            })
        }  else {
            dataItems.push( "Nothing to Display")
        }
        return dataItems;
    }

    menuItemsModalToggle = () => {
        const { editMenuItemSection } = this.state
        this.setState({ editMenuItemSection: !editMenuItemSection });
    }

    handleOnChangeServiceItem = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    onClickSubmitForm = () => {
        const {updateMenu} = this.props
        // const {} = this.state
        updateMenu(this.state)

    }

    render() {
        const {editMenuItemSection,section_name, price, menu_description , menu_type_name, menu_name} =this.state
        
        const data = this.props.content;
        //console.log("MENUPROPSPROPS", data)
        const name = this.props.name;
        //let item = Object.values(data);
        //console.log("OBJECTKEYS", Object.keys(data))
        return (

            <React.Fragment>
                {this.renderItems(data)} 


                {/* Edit Details */}
                <Modal
                    isOpen={editMenuItemSection}
                    size="lg"
                    >
                        <ModalHeader toggle={this.menuItemsModalToggle}>Edit Service Item</ModalHeader>
                        <ModalBody className="p-3">
                        <Row className="justify-content-center">
                            <Col lg={10}>
                                <FormGroup row>
                                <Label sm={4} className="text-dark" size="sm">Menu Name</Label>
                                    <Col sm={8}>
                                        <Input className="primary" type="text" name="menu_name" value={menu_name} onChange={this.handleOnChangeServiceItem} placeholder="Name the Service Menu" className="primary" />
                                    </Col>
                                    </FormGroup>
                                <FormGroup row>
                                <Label sm={4} className="text-dark" size="sm">Menu Type</Label>
                                    <Col sm={8}>
                                        <Input className="primary" type="text" name="menu_type_name" value={menu_type_name} onChange={this.handleOnChangeServiceItem} placeholder="Name the Service Menu" className="primary" />
                                    </Col>
                                    </FormGroup>
                                <FormGroup row>
                                <Label sm={4} className="text-dark" size="sm">Section Name</Label>
                                    <Col sm={8}>
                                        <Input className="primary" type="text" name="section_name" value={section_name} onChange={this.handleOnChangeServiceItem} placeholder="Name the Service Menu" className="primary" />
                                    </Col>
                                    </FormGroup>
                                <FormGroup row>
                                    <Label sm={4} className="text-dark" size="sm">Price</Label>
                                    <Col sm={8}>
                                        <Input className="primary" type="text" name="price" value={price} onChange={this.handleOnChangeServiceItem} placeholder="Name the Service Menu" className="primary" />
                                    </Col>
                                    </FormGroup>
                                <FormGroup row>
                                    <Label sm={4} className="text-dark" size="sm">Description</Label>
                                    <Col sm={8}>
                                        <Input className="primary" type="text" name="menu_description" value={menu_description} onChange={this.handleOnChangeServiceItem} placeholder="Description" className="primary" />
                                    </Col>
                                </FormGroup>
                                    <div className="mt-3 text-right">
                                    <Button color="primary" size="sm" onClick={() => this.onClickSubmitForm()}>Update</Button>
                                    </div>

                            </Col>
                        </Row>

                        </ModalBody>
                </Modal>    



            </React.Fragment>
        )
    }
}



const mapState = (state) => {
    return {
        menu_items: state.user.menuItems,
        delete_menu_item: state.user.delete_menu_item,
    }
}

const mapProps = (dispatch) => {
    return {

        // get_menu_list: (id) => dispatch(get_menu_list(id)),
        delete_menu_item: (data) => dispatch(delete_menu_item(data))
    }
}
export default withRouter(connect(mapState, mapProps)(MenuContent));