import React, { Component } from 'react';
import CollapseBasic from '../atoms/collapse';
import { FormGroup, Label, Input, Button, } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { corporate_id, corporate_id_details, add_additional_value } from '../../actions/user';



class AboutBusiness extends Component {
    constructor(props) {
        super(props);
        this.state = {
            corporateID: null,
            specialities: [],
            meetOwner: [],
            businessOwnerhistory: [],
            editSpec: true,
            editValue: "",
            editHisSpec: true,
            editHistory: "",
            EditBussOwn: "",
            editBusiness: true,
            isToggleBusiness: false,
            isToggleHistory: false,
            isToggleMeet: false
        };
    }

    async componentWillReceiveProps(nextProps) {



        if (nextProps.corporate_id && nextProps.corporate_id.id) {
            let corporate_id = nextProps.corporate_id.id;
            this.setState({ corporateID: corporate_id });
        }

        if (nextProps.corporate_details && nextProps.corporate_details.additional_info) {

            await this.setState({
                specialities: nextProps.corporate_details.additional_info.specialties,
                meetOwner: nextProps.corporate_details.additional_info.meet_the_business_owner,
                businessOwnerhistory: nextProps.corporate_details.additional_info.business_owner_history,

            });

            /*if(this.state.specialities === undefined || this.state.specialities === null){
                this.setState({isToggleBusiness: true})
            }
            if(this.state.meetOwner === undefined || this.state.meetOwner === null){
                this.setState({isToggleMeet: true})
            }
            if(this.state.businessOwnerhistory === undefined || this.state.businessOwnerhistory === null){
                this.setState({isToggleHistory: true})
            }*/

            if (this.state.specialities === null) {
                this.setState({ isToggleBusiness: true })
            }
            if (this.state.meetOwner === null) {
                this.setState({ isToggleMeet: true })
            }
            if (this.state.businessOwnerhistory === null) {
                this.setState({ isToggleHistory: true })
            }

        }



        if (nextProps.additional_data && nextProps.additional_data.specialties) {
            let NewData = nextProps.additional_data.specialties;

            this.setState({
                editSpec: true,
                specialities: NewData,
            },
                () => {
                    this.bizpecialities();
                }
            );
        }

        if (nextProps.additional_data && nextProps.additional_data.business_owner_history) {
            this.setState({
                editHisSpec: true,
                businessOwnerhistory: nextProps.additional_data.business_owner_history,
            },
                () => {
                    this.Ownerhistory();
                }
            );
        }

        if (nextProps.additional_data && nextProps.additional_data.meet_the_business_owner) {
            this.setState({
                editBusiness: true,
                meetOwner: nextProps.additional_data.meet_the_business_owner,
            },
                () => {
                    this.ownerMeet();
                }
            );
        }
    }

    getCorporateDetails = (id) => {
        if (id) {
            this.props.get_corporate_id_details(id);
        }
    }

    bizpecialities = () => {
        let bizpecialities = this.state.specialities;

        if (bizpecialities && Array.isArray(bizpecialities) && bizpecialities.length > 0 && bizpecialities[0].value !== "") {

            return (
                <span>
                    {bizpecialities[0].value}
                </span>)

        } else if ((bizpecialities && Array.isArray(bizpecialities) && bizpecialities.length > 0 && bizpecialities[0].value === "") || bizpecialities === undefined) {
            return (
                <span>
                    <i>Please enter Specialities about your business</i>
                </span>)
        }
        else if (bizpecialities === null) {

            return (
                <span>
                    <i>No Data Available</i>
                </span>)
        }
    }



    ownerMeet = () => {
        let ownerMeet = this.state.meetOwner;
        // if (ownerMeet && Array.isArray(ownerMeet) && ownerMeet.length > 0) {
        //     return ownerMeet.map((item, index) =>
        //         <span key={index}>
        //             {item.value.bio}
        //         </span>
        //     )
        // }



        if (ownerMeet && Array.isArray(ownerMeet) && ownerMeet.length > 0 && ownerMeet[0].value.bio !== "") {

            return (
                <span>
                    {ownerMeet[0].value.bio}
                </span>)

        }
        else if ((ownerMeet && Array.isArray(ownerMeet) && ownerMeet.length > 0 && ownerMeet[0].value.bio === "") || ownerMeet === undefined) {
            return (
                <span>
                    <i>Please enter history of your business</i>
                </span>)
        }
        else if (ownerMeet === null) {

            return (
                <span>
                    <i>No Data Available</i>
                </span>)
        }

    }

    Ownerhistory = () => {
        // let Ownerhistory = this.state.businessOwnerhistory;
        // if(Ownerhistory.length > 0 ){
        //     return Ownerhistory.map((item,index) => 
        //     <span key={index}>
        //         {item.value.history}
        //     </span>
        // )
        // }

        let Ownerhistory = this.state.businessOwnerhistory;

        if (Ownerhistory && Array.isArray(Ownerhistory) && Ownerhistory.length > 0 && Ownerhistory[0].value.history !== "") {

            return (
                <span>
                    {Ownerhistory[0].value.history}
                </span>)

        }
        else if ((Ownerhistory && Array.isArray(Ownerhistory) && Ownerhistory.length > 0 && Ownerhistory[0].value.history === "") || Ownerhistory === undefined) {
            return (
                <span>
                    <i>Please enter history of your business</i>
                </span>)
        }
        else if (Ownerhistory === null) {

            return (
                <span>
                    <i>No Data Available</i>
                </span>)
        }

    }


    EditSpec = () => {

        let editValue = this.state.specialities;

        this.setState(state => ({
            editSpec: !state.editSpec,
            editValue: editValue == undefined ? '' : editValue[0].value
        }));
    }

    handleChange = (e) => {
        this.setState({
            editValue: e.target.value,
            // editHistory: e.target.value
        });
    }

    saveVaule = () => {
        let value = this.state.editValue;
        let id = this.state.corporateID;

        let data = { "type": "specialties", "value": value, "update_additional_info": true };
        this.props.add_additional_value(id, data);
        this.EditSpec()
    }

    EditHistory = () => {
        let editHistoryData = this.state.businessOwnerhistory;

        this.setState(state => ({
            editHisSpec: !state.editHisSpec,
            editHistory: editHistoryData == undefined ? '' : editHistoryData[0].value.history
        }));
    }

    handleHistory = (e) => {
        this.setState({
            editHistory: e.target.value
        });
    }

    saveHistVaule = () => {
        let value = this.state.editHistory;
        let id = this.state.corporateID;

        let data = { "type": "business_owner_history", "value": value, "update_additional_info": true };
        this.props.add_additional_value(id, data);
        this.EditHistory()
    }


    EditBussOwn = () => {
        let ownerMeet = this.state.meetOwner;

        this.setState(state => ({
            editBusiness: !state.editBusiness,
            EditBussOwn: ownerMeet == undefined ? '' : ownerMeet[0].value.bio
        }));
    }

    saveBussOwn = () => {
        let value = this.state.EditBussOwn;
        let id = this.state.corporateID;

        let data = { "type": "meet_the_business_owner", "value": value, "update_additional_info": true };
        this.props.add_additional_value(id, data);
        this.EditBussOwn();
    }

    handleBussOwn = (e) => {
        this.setState({
            EditBussOwn: e.target.value
        });
    }




    render() {
        const { isToggleBusiness, isToggleMeet, isToggleHistory } = this.state;
        return (

            <React.Fragment>
                <CollapseBasic title="About this Business" isOpen={true}>
                    <FormGroup>
                        <Label size="sm" className="p-0 mb-1">Specialities</Label>

                        {/* Data, hide when editing */}
                        <div>
                            <div className="d-flex mb-2">
                                <div className="mr-2 small">
                                    {/* Show if no data */}

                                    {/* Show if data */}
                                    {this.bizpecialities()}
                                </div>
                                <div className={isToggleBusiness ? "d-none" : "ml-auto"}>
                                    <span className="actionable text-dark" onClick={this.EditSpec}><FontAwesomeIcon icon="pencil-alt" size="sm" /> </span>
                                </div>
                            </div>
                        </div>

                        {/* Show when editing */}
                        <div className={this.state.editSpec ? "d-none" : ""}>
                            <div className="mb-2">
                                <Input onChange={(e) => { this.handleChange(e) }} bsSize="sm" className="mb-2" type="textarea" name="specialities" placeholder="Enter specialities" value={this.state.editValue} />
                                <div className="text-right">
                                    <div className="text-right">
                                        <Button size="sm" className="btn-icon-split mr-2" title="Save" onClick={this.saveVaule}> <span className="text">Save</span> <span className="icon"> <FontAwesomeIcon icon="check" /> </span></Button>
                                        <Button size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0" onClick={this.EditSpec}> <span className="text">Cancel</span> <span className="icon"><FontAwesomeIcon icon="times" /></span> </Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </FormGroup>

                    <FormGroup>
                        <Label size="sm" className="p-0 mb-1">History</Label>

                        {/* Data, hide when editing */}
                        <div>
                            <div className="d-flex mb-2">
                                <div className="mr-2 small">
                                    {/* Show if no data */}
                                    <span hidden>
                                        <i>Please enter history about your business</i>
                                    </span>

                                    {/* Show if data */}
                                    {this.Ownerhistory()}
                                </div>
                                <div className={isToggleHistory ? "d-none" : "ml-auto"}>
                                    <span className="actionable text-dark" onClick={this.EditHistory}><FontAwesomeIcon icon="pencil-alt" size="sm" /> </span>
                                </div>
                            </div>
                        </div>

                        {/* Show when editing */}
                        <div className={this.state.editHisSpec ? "d-none" : ""}>
                            <div className="mb-2">
                                <Input onChange={(e) => { this.handleHistory(e) }} bsSize="sm" className="mb-2" type="textarea" name="history" placeholder="Enter history" value={this.state.editHistory} />
                                <div className="text-right">
                                    <div className="text-right">
                                        <Button size="sm" className="btn-icon-split mr-2" title="Save" onClick={this.saveHistVaule}> <span className="text">Save</span> <span className="icon"> <FontAwesomeIcon icon="check" /> </span></Button>
                                        <Button size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0" onClick={this.EditHistory}> <span className="text">Cancel</span> <span className="icon"><FontAwesomeIcon icon="times" /></span> </Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </FormGroup>

                    <FormGroup>
                        <Label size="sm" className="p-0 mb-1">Meet the Business Owner</Label>

                        {/* Data, hide when editing */}
                        <div>
                            <div className="d-flex mb-2">
                                <div className="mr-2 small">
                                    {/* Show if no data */}
                                    <span hidden>
                                        <i>Please enter owner detail</i>
                                    </span>

                                    {/* Show if data */}
                                    {this.ownerMeet()}
                                </div>
                                <div className={isToggleMeet ? "d-none" : "ml-auto"}>
                                    <span className="actionable text-dark"><FontAwesomeIcon icon="pencil-alt" size="sm" onClick={this.EditBussOwn} /> </span>
                                </div>
                            </div>
                        </div>

                        {/* Show when editing */}
                        <div className={this.state.editBusiness ? "d-none" : ""}>
                            <div className="mb-2">
                                <Input onChange={(e) => { this.handleBussOwn(e) }} bsSize="sm" className="mb-2" type="textarea" name="b_owner" placeholder="Enter Business..." value={this.state.EditBussOwn} />
                                <div className="text-right">
                                    <div className="text-right">
                                        <Button size="sm" className="btn-icon-split mr-2" title="Save"> <span className="text" onClick={this.saveBussOwn}>Save</span> <span className="icon"> <FontAwesomeIcon icon="check" /> </span></Button>
                                        <Button size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0" onClick={this.EditBussOwn}> <span className="text">Cancel</span> <span className="icon" ><FontAwesomeIcon icon="times" /></span> </Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </FormGroup>
                </CollapseBasic>

            </React.Fragment>
        )
    }
}

const mapState = (state) => {
    return {
        corporate_id: state.user.corporate_id,
        corporate_details: state.user.corporate_id_details,
        additional_data: state.user.additional_data
    }
}

const mapProps = (dispatch) => {
    return {
        get_corporate_id: () => dispatch(corporate_id()),
        get_corporate_id_details: (id) => dispatch(corporate_id_details(id)),
        add_additional_value: (id, data) => dispatch(add_additional_value(id, data))
    }
}

export default withRouter(connect(mapState, mapProps)(AboutBusiness));