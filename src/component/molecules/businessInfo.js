import React, { Component } from 'react';
import CollapseBasic from '../atoms/collapse';
import { Modal, ModalHeader, ModalBody, FormGroup, Label, Input, Button, Row, Col, Badge, } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
    corporate_id,
    corporate_id_details,
    post_business_update,
    delete_business_record,
    get_business_category_data,
    update_payment_options,
    post_business_category_data,
    delete_business_categories,
    delete_payment_option,
    post_hours_of_operation,
    put_hours_of_operation,
    delete_hours_of_operation,
    get_sub_categories
} from '../../actions/user';
import moment from 'moment';
import { AvForm, AvField, } from 'availity-reactstrap-validation';
import Switch from 'react-input-switch';
import { emailRegex, websiteRegex } from "../../utils/validation";
import EditBtn from "../atoms/editBtn";
import DeleteBtn from "../atoms/deleteBtn";

class BusinessInfo extends Component {
    mapRef = React.createRef();

    constructor(props) {
        super(props);
        this.refWebsiteForm = React.createRef()
        this.refBusinessEmailForm = React.createRef()
        this.state = {
            corporateId: null,
            phoneDiary: [],
            AdressDiary: [],
            webSet: [],
            emailSet: [],
            payOptions: [],
            categoryList: [],
            HoursOfOperation: [],
            modalToggle: false,
            isToggleAddress: false,
            isEdit: false,
            isTogglePhone: false, // phone fields
            isEditPhone: false,
            addPhone: { ph1: '', ph2: '', ph3: '', label: '' },
            isPhoneValid: true,

            isToggleWebsite: false, // Website fields
            isEditWebsite: false,
            addWebsite: { website: "", website_type: null },
            isWebsiteValid: true,

            isToggleHours: false, // Hours fields
            isEditHours: false,
            addHours: { day_of_week: 0, end_time: "", start_time: "", entries: "", id: "", info: "", "next_day": 0 },
            isHoursValid: false,
            isToggleDeleteHours: false,

            isToggleDeletePayment: false,

            isEditAddress: false,
            addAddress: { address1: '', address2: '', state: '', city: '', zipcode: '', country: '', latitude: '', longitude: '' },

            isToggleDeleteCategory: false,

            isToggleBusinessEmail: false, // BusinessEmail fields
            isEditBusinessEmail: false,
            addBusinessEmail: { email: "", email_type: null },
            isBusinessEmailValid: true,
            clonePaymentSwitch: {},
            paymentSwitch: {},
            isTogglePayment: false,
            deletePop: false,
            editRecordId: false,
            editRecordType: false,
            businessCategory: [],
            businessSubCategory: [],
            totalSubCategories: [1],
            anotherCategories: [],
            anotherSubCategories: [],
            postCategoryList: {},
            seletedCategories: [],
            business_specialities: '',
            address1: '',
            subCategoryZero: '',
            selectedCategory: '',

        };
        this.handleChangeAddress = this.handleChangeAddress.bind(this);

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.corporate_details) {
            let payOptions = nextProps.corporate_details.paymentoptions_set;
            let addressData = nextProps.corporate_details.address;
            let HoursOfOperation = nextProps.corporate_details && nextProps.corporate_details.hoursofoperation_set
            let setDaysOfWeek = 1;
            HoursOfOperation = HoursOfOperation || [];
            let filteredData = HoursOfOperation && HoursOfOperation.sort((a, b) => {
                return (a.id < b.id) ? -1 : (a.id > b.id) ? 1 : 0;
            });

            if (filteredData.length > 0) {
                let lastItem = filteredData[filteredData.length - 1];
                if (lastItem.day_of_week == '7') {
                    setDaysOfWeek = 1;
                } else {
                    setDaysOfWeek = (lastItem.day_of_week + 1);
                }
            }

            if (HoursOfOperation && Array.isArray(HoursOfOperation) && HoursOfOperation.length > 0) {
                let lastElement = HoursOfOperation[HoursOfOperation.length - 1];
                this.setState({ addHours: { day_of_week: setDaysOfWeek, end_time: lastElement.end_time, start_time: lastElement.start_time, entries: "", id: "", info: "" } })
                // console.log("SETSTATETIME",this.state.addHours)
            }

            this.setState({
                //addHours: { day_of_week: setDaysOfWeek, end_time: this.state.end_time, start_time: this.state.start_time, entries: "", id: "", info: "" },
                isHoursValid: false,
                corporateId: nextProps.corporate_details.id,
                phoneDiary: nextProps.corporate_details.phone_set,
                AdressDiary: nextProps.corporate_details.address,
                webSet: nextProps.corporate_details.website_set,
                emailSet: nextProps.corporate_details.email_set,
                payOptions: payOptions,
                categoryList: nextProps.corporate_details.taxonomy_dict,
                HoursOfOperation: filteredData
            }, () => {
                let paymentSwitch = this.state.paymentSwitch;
                payOptions && payOptions.forEach(item => {

                    if (item.paymentoptions == "Apple Pay") {
                        paymentSwitch['apple'] = true
                    }

                    if (item.paymentoptions == "Mastercard") {
                        paymentSwitch['mastercard'] = true
                    }

                    if (item.paymentoptions == "American Express") {
                        paymentSwitch['american'] = true
                    }

                    if (item.paymentoptions == "Cash") {
                        paymentSwitch['cash'] = true
                    }

                    if (item.paymentoptions == "Check") {
                        paymentSwitch['check'] = true
                    }

                    if (item.paymentoptions == "Discover") {
                        paymentSwitch['discover'] = true
                    }

                    if (item.paymentoptions == "Visa") {
                        paymentSwitch['visa'] = true
                    }

                    if (item.paymentoptions == "Google Wallet") {
                        paymentSwitch['google'] = true
                    }

                    if (item.paymentoptions == "Cryptocurrency") {
                        paymentSwitch['cryptocurrency'] = true
                    }

                    if (item.paymentoptions == "Debit Card") {
                        paymentSwitch['debit'] = true
                    }
                })
                this.setState({ paymentSwitch, clonePaymentSwitch: paymentSwitch })
                if (this.state.categoryList && this.state.categoryList[0] && this.state.categoryList[0].id) {
                    this.setState({ selectedCategory: this.state.categoryList[0].id })
                }
            });
        }

        if (nextProps.business_category) {
            this.setState({
                businessCategory: nextProps.business_category
            })
        }

        if (nextProps.business_sub_category) {
            let fetchSubCategories = nextProps.business_sub_category;
            let { anotherCategories, businessSubCategory, subCategoryZero } = this.state;
            if (subCategoryZero && subCategoryZero == "zero") {
                businessSubCategory[0] = fetchSubCategories
            } else {
                if (anotherCategories.length > 0) {
                    businessSubCategory[anotherCategories.length] = fetchSubCategories
                } else {
                    businessSubCategory[0] = fetchSubCategories
                }
            }
            this.setState({ businessSubCategory })
        }

        if (nextProps.update_pay_option) {
            this.setState({ payOptions: nextProps.update_pay_option })
        }
    }


    setPaymentReset = () => {
        let { payOptions } = this.state;
        let paymentSwitch = {};
        if (payOptions && payOptions.length > 0) {

            payOptions.forEach(item => {
                if (item.paymentoptions == "Cash") {
                    paymentSwitch['cash'] = true
                }

                if (item.paymentoptions == "Check") {
                    paymentSwitch['check'] = true
                }

                if (item.paymentoptions == "Visa") {
                    paymentSwitch['visa'] = true
                }

                if (item.paymentoptions == "Mastercard") {
                    paymentSwitch['mastercard'] = true
                }

                if (item.paymentoptions == "Discover") {
                    paymentSwitch['discover'] = true
                }

                if (item.paymentoptions == "American Express") {
                    paymentSwitch['american'] = true
                }

                if (item.paymentoptions == "Apple Pay") {
                    paymentSwitch['apple'] = true
                }

                if (item.paymentoptions == "Google Wallet") {
                    paymentSwitch['google'] = true
                }

                if (item.paymentoptions == "Cryptocurrency") {
                    paymentSwitch['cryptocurrency'] = true
                }

                if (item.paymentoptions == "Debit Card") {
                    paymentSwitch['debit'] = true
                }
            })
            this.setState({ paymentSwitch, clonePaymentSwitch: paymentSwitch })
        }
    }

    modalToggle = () => {
        this.setState({ addmodal: !this.state.addmodal }, () => {
            if (this.state.addmodal) {
                this.props.get_business_category_data('Business');
            } else {
                this.setState({
                    businessCategory: [],
                    businessSubCategory: [],
                    totalSubCategories: [1],
                    anotherCategories: [],
                    anotherSubCategories: [],
                    postCategoryList: {},
                    seletedCategories: [],
                    business_specialities: ''
                });
            }
        });
    }

    deleteToggle = (id, type) => {
        this.setState({
            isDeleteToggle: !this.state.isDeleteToggle,
            editRecordId: id ? id : false,
            editRecordType: type ? type : false
        });

    }

    deleteBusinessRecord = () => {
        let { corporateId, editRecordId, editRecordType } = this.state;
        this.props.delete_business_record(editRecordType, editRecordId, corporateId);
        this.deleteToggle();
    }

    PhoneData = () => {
        let phonelist = this.state.phoneDiary;
        if (phonelist.length > 0) {
            return phonelist.map((item, index) => {
                item.ph1 = (item.phone).slice(0, 3)
                item.ph2 = (item.phone).slice(3, 6)
                item.ph3 = (item.phone).slice(6, 10)
                return (
                    <div className="d-flex mb-2 interactive" key={index}>
                        <span className="mr-2 small text-truncate mt-1">
                            {item.formated_phone}
                            {` [${item.label == "Tel" ? "Telephone" : item.label == "Mob" ? "Mobile" : item.label == "Support" ? "Support" : ""}]`} </span>
                        <div className="ml-auto interactive-appear">
                            {/* <ButtonGroup>
                                <Button onClick={() => this.toggleEditPhone(item)} size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button>
                                <Button onClick={() => this.deleteToggle(item.id, 'phone')} size="sm" color="danger" title="Delete"><FontAwesomeIcon icon="trash-alt" /></Button>
                            </ButtonGroup> */}
                            <EditBtn
                                onClick={() => this.toggleEditPhone(item)}
                            />
                            <DeleteBtn
                                onClick={() => this.deleteToggle(item.id, 'phone')}
                                color="gold"
                            />
                        </div>
                    </div>
                )
            }
            )
        }
    }

    adressData = () => {
        let addressList = this.state.AdressDiary;

        if (addressList && addressList.length > 0) {
            return addressList.map((item, index) =>

                <div className="d-flex mb-2 interactive" key={index}>
                    <span className="mr-2 small text-truncate mt-1">{item.address1} {item.city} {item.state} {item.country}</span>
                    <div className="ml-auto interactive-appear">
                        {/* <ButtonGroup>
                            <Button size="sm" color="dark" title="Edit" onClick={() => this.toggleEditAddress(item)}><FontAwesomeIcon icon="edit" /> </Button>
                            <Button size="sm" color="danger" title="Delete" onClick={() => this.deleteToggle(item.id, 'address')}><FontAwesomeIcon icon="trash-alt" /></Button>
                        </ButtonGroup> */}
                        <EditBtn
                            onClick={() => this.toggleEditAddress(item)}
                        />
                        <DeleteBtn
                            onClick={() => this.deleteToggle(item.id, 'address')}
                            color="gold" />
                    </div>
                </div>
            )
        }
    }

    websiteData = () => {
        let webList = this.state.webSet;
        if (webList && webList.length > 0) {
            return webList.map((item, index) =>
                <div className="d-flex mb-2 interactive" key={index}>
                    <span className="mr-2 small text-truncate mt-1">{item.website} [{item.website_type}]</span>
                    <div className="ml-auto interactive-appear">
                        {/* <ButtonGroup>
                            <Button onClick={() => this.toggleEditWebsite(item)} size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button>
                            <Button onClick={() => this.deleteToggle(item.id, 'website')} size="sm" color="danger" title="Delete"><FontAwesomeIcon icon="trash-alt" /></Button>
                        </ButtonGroup> */}
                        <EditBtn
                            onClick={() => this.toggleEditWebsite(item)}
                        />
                        <DeleteBtn
                            onClick={() => this.deleteToggle(item.id, 'website')}
                            color="gold"
                        />
                    </div>
                </div>
            )
        }
    }

    emailList = () => {
        let emailList = this.state.emailSet;

        if (emailList && emailList.length > 0) {

            return emailList.map((item, index) =>

                <div className="d-flex mb-2 interactive" key={index}>
                    <span className="mr-2 small text-truncate mt-1">{item.email} [{item.email_type}]</span>
                    <div className="ml-auto interactive-appear">
                        {/* <ButtonGroup>
                            <Button onClick={() => this.toggleEditBusinessEmail(item)} size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button>
                            <Button onClick={() => this.deleteToggle(item.id, 'email')} size="sm" color="danger" title="Delete"><FontAwesomeIcon icon="trash-alt" /></Button>
                        </ButtonGroup> */}
                        <EditBtn
                            onClick={() => this.toggleEditBusinessEmail(item)}
                        />
                        <DeleteBtn
                            onClick={() => this.deleteToggle(item.id, 'email')}
                            color="gold"
                        />
                    </div>
                </div>

            )
        }
    }

    payOptions = () => {
        let payOptions = this.state.payOptions;
        if (payOptions && payOptions.length > 0) {
            return payOptions.map((item, index) => {
                if (item.paymentoptions !== "Credit Card") {
                    return (
                        <span className="px-1 fs-20" key={index}>
                            <Badge color="secondary" className="px-2 font-weight-normal">
                                <span className="mr-1 font-weight-normal text-truncate">{item.paymentoptions}</span>
                                <button
                                    type="button" className="close btn-sm" aria-label="Close"
                                    onClick={() => this.toggleDeletePayment(item)}
                                >
                                    <span><FontAwesomeIcon icon="times-circle" /> </span>
                                </button>
                            </Badge>
                        </span>

                    )
                }
            })
        }
        else {
            return <p className="mr-2 small text-truncate">Add Payment Option</p>
        }
    }
    handleCategory = (item) => {
        this.props.get_sub_categories(item)
    }

    categoryList = () => {
        let categoryList = this.state.categoryList;

        if (categoryList && categoryList.length > 0) {
            return categoryList.map((item, index) => {
                return (
                    <span className="px-1 mb-2 mw-100" key={index}>
                        <Badge color="secondary" className="d-flex align-items-center">
                            <span className="mr-1 font-weight-normal text-truncate" onClick={() => this.handleCategory(item)}>{item.category}</span>
                            <button onClick={() => this.toggleDeleteCategory(item.id)} type="button" className="close btn-sm" aria-label="Close">
                                <span><FontAwesomeIcon icon="times-circle" /> </span>
                            </button>
                        </Badge>
                    </span>
                )
            }
            )
        }
    }

    HoursOfOperation = () => {
        let HoursOfOperation = this.state.HoursOfOperation;
        let weekDays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        if (HoursOfOperation && Array.isArray(HoursOfOperation) && HoursOfOperation.length > 0) {
            // let filteredData = HoursOfOperation.sort((a, b) => {
            //     return (a.id < b.id) ? -1 : (a.id > b.id) ? 1 : 0;
            // });

            return HoursOfOperation.map((item, index) => {

                return (
                    <li className="mb-2" key={index}>
                        {/* <span className="font-weight-bold">{weekDays[item.day_of_week - 1]}</span>
                        <span className="ml-auto font-weight-bold">
                            <span>
                                {item.start_time ? moment(item.start_time, 'hh:mm:ss').format('hh:mm A') : `[Closed]`}
                            </span>
                            {'-'}<span> {item.start_time ? moment(item.end_time, 'hh:mm:ss').format('hh:mm A') : ''} </span>
                        </span>
                        {item.info && <span className="font-weight-bold"> {`[${item.info}]`} </span>}

                       
                        <span className="interactive-appear">
                            <div className="ml-2 text-nowrap">
                                <ButtonGroup>
                                    <Button onClick={() => this.toggleEditHours(item)} size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button>
                                    <Button onClick={() => this.toggleDeleteHours(item.id)} size="sm" color="danger" title="Delete"><FontAwesomeIcon icon="trash-alt" /></Button>
                                </ButtonGroup>
                            </div>
                        </span> */}
                        <div className="d-sm-flex d-md-block d-xxl-flex flex-wrap mx-n2">
                            <span className="font-weight-bold px-2 mr-auto">{weekDays[item.day_of_week - 1]}</span>
                            <div className="px-2">
                                <div className="d-flex">
                                    <div className="flex-fill mr-2">
                                        <div className="d-flex flex-wrap">
                                            <span className="font-weight-bold ">
                                                <span>{item.start_time ? moment(item.start_time, 'HH:mm:ss').format('hh:mm A') : `[Closed]`} </span>
                                                {'-'}
                                                <span> {item.end_time ? moment(item.end_time, 'HH:mm:ss').format('hh:mm A') : ''}</span>
                                                <span> {item.next_day ? "(next day)" : ""} </span>
                                            </span>
                                            <span className="font-weight-bold ml-1"> {item.info && <span className="font-weight-bold"> {`[${item.info}]`} </span>} </span>
                                        </div>
                                        {/* <span>
                {`${weekDays[item.day_of_week - 1]} ${moment(item.start_time, 'hh:mm:ss').format('hh:mm A')}  ${moment(item.end_time, 'hh:mm:ss').format('hh:mm A')} [${item.info}]`}
                </span> */}
                                    </div>

                                    <div className="ml-auto interactive col-auto px-0">
                                        <div className="interactive-appear">
                                            <div className="ml-2 text-nowrap">
                                                {/* <ButtonGroup>
                                                    <Button onClick={() => this.toggleEditHours(item)} size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button>
                                                    <Button onClick={() => this.toggleDeleteHours(item.id)} size="sm" color="danger" title="Delete"><FontAwesomeIcon icon="trash-alt" /></Button>
                                                </ButtonGroup> */}
                                                {/* <EditBtn color="gold" /> */}
                                                <DeleteBtn
                                                    onClick={() => this.toggleDeleteHours(item.id)}
                                                    color="gold"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                )
            }
            )
        }
    }


    //toggleEditAddress = () => this.setState(prevState => ({ isToggleAddress: !prevState.isToggleAddress })); // Edit Address

    toggleEditAddress = (isTrue) => {

        if (isTrue && isTrue.id) {
            let { addAddress } = this.state;
            addAddress['address1'] = isTrue.address1;
            addAddress['address2'] = isTrue.address2;
            addAddress['city'] = isTrue.city;
            addAddress['state'] = isTrue.state;
            addAddress['country'] = isTrue.country;
            addAddress['zipcode'] = isTrue.zipcode;
            addAddress['latitude'] = isTrue.latitude;
            addAddress['longitude'] = isTrue.longitude;
            this.setState({ addAddress });
        }
        this.setState({ isEditAddress: isTrue && isTrue.id ? isTrue : false, isToggleAddress: !this.state.isToggleAddress })
    }
    //togglePayment = () => this.setState(prevState => ({ isTogglePayment: !prevState.isTogglePayment })); // Edit Address
    //toggleEditAddress = () => this.setState(prevState => ({ isToggleAddress: !prevState.isToggleAddress })); // Edit Address
    togglePayment = () => {
        let { isTogglePayment, paymentSwitch } = this.state;
        this.setState({ isTogglePayment: !isTogglePayment })
    } // Edit Address

    toggleEditBusinessEmail = (isTrue) => {

        if (isTrue && isTrue.id) {
            let { addBusinessEmail } = this.state;
            addBusinessEmail['email'] = isTrue.email;
            addBusinessEmail['email_type'] = isTrue.email_type;
            this.setState({ addBusinessEmail, isBusinessEmailValid: false }, () => this.businessEmailValidation(addBusinessEmail));
        }

        this.setState({ isEditBusinessEmail: isTrue && isTrue.id ? isTrue : false, isToggleBusinessEmail: !this.state.isToggleBusinessEmail })
    }

    toggleEditWebsite = (isTrue) => {

        if (isTrue && isTrue.id) {
            let { addWebsite } = this.state;
            addWebsite['website'] = isTrue.website;
            addWebsite['website_type'] = isTrue.website_type;
            this.setState({ addWebsite, isWebsiteValid: false }, () => this.websiteValidation(addWebsite));
        }

        this.setState({ isEditWebsite: isTrue && isTrue.id ? isTrue : false, isToggleWebsite: !this.state.isToggleWebsite })
    }


    toggleEditPhone = (isTrue) => {
        if (isTrue && isTrue.id) {
            let { addPhone } = this.state;
            addPhone['ph1'] = isTrue.ph1
            addPhone['ph2'] = isTrue.ph2
            addPhone['ph3'] = isTrue.ph3
            addPhone['label'] = isTrue.label
            this.setState({ addPhone, isPhoneValid: false }, () => this.phoneValidation(addPhone));
        }
        this.setState({ isEditPhone: isTrue && isTrue.id ? isTrue : false, isTogglePhone: !this.state.isTogglePhone })
    }


    toggleEditHours = (isTrue) => {
        if (isTrue && isTrue.id) {
            let { addHours } = this.state;
            addHours['day_of_week'] = isTrue.day_of_week
            addHours['end_time'] = isTrue.end_time
            addHours['start_time'] = isTrue.start_time
            addHours['entries'] = isTrue.entries
            addHours['id'] = isTrue.id
            addHours['info'] = isTrue.info
            this.setState({ addHours, isHoursValid: false }, () => this.hoursValidation(addHours));
        }
        this.setState({ isEditHours: isTrue && isTrue.id ? isTrue : false, isToggleHours: !this.state.isToggleHours })
    }

    setInitialPhone = () => {
        this.toggleEditPhone();
        this.setState({ addPhone: { ph1: '', ph2: '', ph3: '', label: '' }, isPhoneValid: true })
    }
    setInitialAddress = () => {
        this.toggleEditAddress();
        this.setState({ addAddress: { address1: '', address2: '', city: '', state: '', country: '', zipcode: '', latitude: '', longitude: '' } })
    }

    setInitialWebsite = () => {
        this.toggleEditWebsite();
        this.setState({ addWebsite: { website: '', website_type: '' }, isWebsiteValid: true });
    }

    setInitialBusinessEmail = () => {
        this.toggleEditBusinessEmail();
        this.setState({ addBusinessEmail: { email: '', email_type: '' }, isBusinessEmailValid: true });
    }

    setInitialHours = (startTime, endTime) => {
        this.toggleEditHours();
        this.setState({ addHours: { day_of_week: 1, end_time: endTime, start_time: startTime, entries: "", id: "", info: "" }, isHoursValid: false });
    }

    hoursValidation = (addHours) => {
        let { isHoursValid } = this.state;
        let start_time = moment(addHours.start_time, "HH:mm:ss").format("HH:mm");
        let end_time = moment(addHours.end_time, "HH:mm:ss").format("HH:mm");

        // if (addHours.start_time !== '' && addHours.day_of_week !== '') {
        //     this.setState({ isHoursValid: false });
        // }
        if (moment(end_time, 'HH:mm').isBefore(moment(start_time, 'HH:mm')) && addHours['next_day'] === '0') {

            this.setState({ isHoursValid: true });
        }
        else {
            if (isHoursValid !== false) {
                this.setState({ isHoursValid: false });
            }
        }


        // else {
        //         this.setState({ isHoursValid: false });
        // }

    }

    phoneValidation = (addPhone) => {
        let { isPhoneValid } = this.state;

        let labels = ['Tel', 'Mob', 'Support'];
        if (addPhone['ph1'].length == 3 && addPhone['ph2'].length == 3 && addPhone['ph3'].length == 4 && labels.includes(addPhone['label'])) {
            this.setState({ isPhoneValid: false });
        } else {
            if (isPhoneValid !== true) {
                this.setState({ isPhoneValid: true });
            }
        }
    }

    businessEmailValidation = (addBusinessEmail, hasError) => {
        let { isBusinessEmailValid } = this.state;
        let labels = ['Sales', 'Reserve', 'General'];

        if (addBusinessEmail.email !== '' && labels.includes(addBusinessEmail.email_type) && hasError !== true) {
            this.setState({ isBusinessEmailValid: false });
        } else {
            if (isBusinessEmailValid !== true) {
                this.setState({ isBusinessEmailValid: true });
            }
        }
    }

    websiteValidation = (addWebsite, hasError) => {
        let { isWebsiteValid } = this.state;
        let labels = ['Main', 'Facebook', 'Google+', 'Twitter', 'LinkedIn', 'Instagram'];

        if (addWebsite.website !== '' && labels.includes(addWebsite.website_type) && hasError !== true) {
            this.setState({ isWebsiteValid: false });
        } else {
            if (isWebsiteValid !== true) {
                this.setState({ isWebsiteValid: true });
            }
        }
    }

    handleChangePhone = (event) => {
        let { name, value } = event.target;
        let { addPhone } = this.state;
        addPhone[name] = value;
        this.setState({ addPhone }, () => this.phoneValidation(addPhone));
    }

    handleChangeAddress = (event) => {
        let { name, value } = event.target;
        let { addAddress } = this.state;
        addAddress[name] = value;
        this.setState({ addAddress });
    }

    handleChangeWebsite = (event) => {
        let { name, value } = event.target;
        let hasError = this.refWebsiteForm.current._inputs[name].context.FormCtrl.hasError()
        let { addWebsite } = this.state;
        addWebsite[name] = value;
        this.setState({ addWebsite }, () => this.websiteValidation(addWebsite, hasError));
    }

    handleChangeBusinessEmail = (event) => {
        let { name, value } = event.target;
        let hasError = this.refBusinessEmailForm.current._inputs[name].context.FormCtrl.hasError()
        let { addBusinessEmail } = this.state;
        addBusinessEmail[name] = value;
        this.setState({ addBusinessEmail }, () => this.businessEmailValidation(addBusinessEmail, hasError));
    }

    handleChangeHours = (event) => {
        let { name, value } = event.target;
        let { addHours, isEditHours } = this.state;
        if (value[value.length - 1] === "1") {
            addHours["next_day"] = value[value.length - 1]
            this.setState({ addHours })
        }
        else {
            addHours["next_day"] = "0";
            this.setState({ addHours })
        }
        addHours[name] = value;
        this.setState({ addHours, isEditHours }, () => this.hoursValidation(addHours));
    }


    handleSubmitHours = async () => {
        let { isEditHours, corporateId, HoursOfOperation, addHours, addHours: { day_of_week, end_time, start_time, entries, id, info, next_day } } = this.state;
        // let getData = HoursOfOperation.filter(data => data.day_of_week == day_of_week);

        //let isAdd = true;
        let data = {
            day_of_week,
            end_time: moment(end_time, "HH:mm:ss").format("HH:mm"),
            start_time: moment(start_time, "HH:mm:ss").format("HH:mm"),
            info,
            entries_id: corporateId,
            next_day: parseInt(addHours['next_day'])
        }

        let brunchStart = '09:59';
        let brunchEnd = '11:31';

        let lunchStart = '11:59';
        let lunchEnd = '15:01'; // 3:00 pm

        let dinnerStart = '17:59'; // 6:00 pm
        let dinnerEnd = '22:01';  // 10:00 pm



        if (
            moment(data.start_time, 'HH:mm').isBetween(moment(brunchStart, 'HH:mm'), moment(brunchEnd, 'HH:mm')) && moment(data.end_time, 'HH:mm').isBetween(moment(brunchStart, 'HH:mm'), moment(brunchEnd, 'HH:mm'))
        ) {
            this.setState({ isEditHours: false });

            data.info = "Brunch Time";

            await this.props.post_hours_of_operation(data, corporateId);
            this.toggleEditHours();
            this.setInitialHours(data.start_time, data.end_time);
        }


        else if (moment(data.start_time, 'HH:mm').isBetween(moment(lunchStart, 'HH:mm'), moment(lunchEnd, 'HH:mm')) && moment(data.end_time, 'HH:mm').isBetween(moment(lunchStart, 'HH:mm'), moment(lunchEnd, 'HH:mm'))) {
            this.setState({ isEditHours: false });

            data.info = "Lunch Time";
            await this.props.post_hours_of_operation(data, corporateId);
            this.toggleEditHours();
            this.setInitialHours(data.start_time, data.end_time);
        }

        else if (moment(data.start_time, 'HH:mm').isBetween(moment(dinnerStart, 'HH:mm'), moment(dinnerEnd, 'HH:mm')) && moment(data.end_time, 'HH:mm').isBetween(moment(dinnerStart, 'HH:mm'), moment(dinnerEnd, 'HH:mm'))) {
            this.setState({ isEditHours: false });

            data.info = "Dinner Time";
            await this.props.post_hours_of_operation(data, corporateId);
            this.toggleEditHours();
            this.setInitialHours(data.start_time, data.end_time);
        }

        else {

            data.info = "";
            await this.props.post_hours_of_operation(data, corporateId);
            this.toggleEditHours();
            this.setInitialHours(data.start_time, data.end_time);
        }



        // if (isEditHours && isEditHours.id) {
        //     isAdd = false;

        //     this.props.put_hours_of_operation(data, id, corporateId)
        //     //this.setInitialHours()
        //     this.toggleEditHours();
        // }
        //  else {
        //     data.entries_id = corporateId;
        //     await this.props.post_hours_of_operation(data, corporateId);
        //     // this.toggleEditHours();
        //     // this.setInitialHours()
        // }
    }

    handleSubmitBusinessEmail = () => {
        let { isEditBusinessEmail, corporateId, addBusinessEmail: { email, email_type } } = this.state;
        let time = moment().format('HH:mm');
        let isAdd = true;
        let data = {
            "email": email,
            "email_type": email_type,
            "type": "email_form",
            "id": corporateId,
            "start_time": time,
            "end_time": time
        }

        if (isEditBusinessEmail && isEditBusinessEmail.id) {
            isAdd = false;
            data.editform = true;
            data.entityid = isEditBusinessEmail.id
        }

        this.props.post_business_update(data, isAdd, corporateId);
        this.toggleEditBusinessEmail();
    }

    handleSubmitPhone = () => {
        let { isEditPhone, corporateId, addPhone: { ph1, ph2, ph3, label } } = this.state;
        let time = moment().format('HH:mm');
        let isAdd = true;
        let data = {
            "end_time": time,
            "id": corporateId,
            "label": label,
            "ph1": ph1,
            "ph2": ph2,
            "ph3": ph3,
            "phone": `${ph1}${ph2}${ph3}`,
            "start_time": time,
            "type": "phone_form"
        }

        if (isEditPhone && isEditPhone.id) {
            isAdd = false;
            data.editform = true;
            data.entityid = isEditPhone.id
        }

        this.props.post_business_update(data, isAdd, corporateId);
        this.toggleEditPhone();
    }

    handleSubmitAddress = (event, errors, values) => {
        let { isEditAddress, corporateId, addAddress: { address1, address2, city, country, state, zipcode, latitude, longitude } } = this.state;
        let time = moment().format('HH:mm');
        let isAdd = true;
        let data = {
            "end_time": time,
            "id": corporateId,
            "address1": address1,
            "address2": address2,
            "city": city,
            "country": country,
            "state": state,
            "zipcode": zipcode,
            "start_time": time,
            "type": "address_form",
            "latitude": latitude,
            "longitude": longitude,
        }

        if (isEditAddress && isEditAddress.id) {
            isAdd = false;
            data.editform = true;
            data.entityid = isEditAddress.id
        }
        this.setState({ errors, values });
        if (this.state.errors.length == 0) {
            this.props.post_business_update(data, isAdd, corporateId);
            this.toggleEditAddress();
        }
    }


    handleSubmitWebsite = () => {
        let { isEditWebsite, corporateId, addWebsite: { website, website_type } } = this.state;
        let time = moment().format('HH:mm');
        let isAdd = true;
        let data = {
            "website": website,
            "website_type": website_type,
            "type": "website_form",
            "id": corporateId,
            "start_time": time,
            "end_time": time
        }

        if (isEditWebsite && isEditWebsite.id) {
            isAdd = false;
            data.editform = true;
            data.entityid = isEditWebsite.id
        }

        this.props.post_business_update(data, isAdd, corporateId);
        this.toggleEditWebsite();
    }

    handleSubmitPayments = () => {
        let { paymentSwitch, corporateId } = this.state;
        let data = { selected_pay: [], not_selected: [] };

        [paymentSwitch].forEach(item => {
            if (item.cash == true) {
                data.selected_pay.push('Cash')
            } else {
                data.not_selected.push('Cash')
            }

            if (item.check == true) {
                data.selected_pay.push('Check')
            } else {
                data.not_selected.push('Check')
            }

            if (item.visa == true) {
                data.selected_pay.push('Visa')
            } else {
                data.not_selected.push('Visa')
            }

            if (item.mastercard == true) {
                data.selected_pay.push('Mastercard')
            } else {
                data.not_selected.push('Mastercard')
            }


            if (item.discover == true) {
                data.selected_pay.push('Discover')
            } else {
                data.not_selected.push('Discover')
            }


            if (item.american == true) {
                data.selected_pay.push('American Express')
            } else {
                data.not_selected.push('American Express')
            }

            if (item.apple == true) {
                data.selected_pay.push('Apple Pay')
            } else {
                data.not_selected.push('Apple Pay')
            }

            if (item.google == true) {
                data.selected_pay.push('Google Wallet')
            } else {
                data.not_selected.push('Google Wallet')
            }

            if (item.debit == true) {
                data.selected_pay.push('Debit Card')
            } else {
                data.not_selected.push('Debit Card')
            }

            if (item.cryptocurrency == true) {
                data.selected_pay.push('Cryptocurrency')
            } else {
                data.not_selected.push('Cryptocurrency')
            }
        });

        this.togglePayment();
        this.props.update_payment_options(corporateId, data);
    }

    handleSwitch = (value, name) => {
        let { paymentSwitch } = this.state;
        paymentSwitch[name] = !paymentSwitch[name];
        this.setState({ paymentSwitch });
    }

    handleChangeCategory = (e) => {
        let { name, value } = e.target;
        let { postCategoryList, seletedCategories } = this.state;
        postCategoryList[[value]] = [];
        seletedCategories.push(value)
        var val = e.target.getAttribute("data-id");
        this.setState({ postCategoryList, seletedCategories, subCategoryZero: val }, () => this.props.get_business_category_data(value))
    }

    addSubCategories = () => {
        let { totalSubCategories, businessSubCategory } = this.state;
        if (businessSubCategory[0] && businessSubCategory[0].length > 0) {
            totalSubCategories.push(1);
            this.setState({ totalSubCategories })
        }
    }

    removeSubCateItems = (index) => {
        let { totalSubCategories, postCategoryList, seletedCategories } = this.state;
        if (index > -1) {
            let categoryId = seletedCategories[0];
            postCategoryList[categoryId].splice(index, 1);
            totalSubCategories.splice(index, 1);
            this.setState({ totalSubCategories })
        }
    }

    addAnotherCategories = () => {
        let { anotherCategories } = this.state;
        let index = anotherCategories.length;
        anotherCategories[index] = [];
        this.setState({ anotherCategories })
    }


    addAnotherSubCategories = (index) => {
        let { anotherCategories, businessSubCategory } = this.state;
        if (businessSubCategory[index + 1] && businessSubCategory[index + 1].length > 0) {
            anotherCategories[index].push(1);
            this.setState({ anotherCategories })
        }
    }

    removeSubCategoryItem = (index, subIndex) => {
        let { anotherCategories, postCategoryList, seletedCategories } = this.state;
        let categoryId = seletedCategories[index];
        postCategoryList[categoryId].splice(subIndex, 1);
        anotherCategories[index - 1].splice(subIndex, 1);
        this.setState({ anotherCategories });
    }

    changeSubCategory = (e, index, subIndex) => {
        let { name, value } = e.target;
        let { postCategoryList, seletedCategories } = this.state;

        if (index !== 'undefined') {
            let categoryId = seletedCategories[index];
            postCategoryList[categoryId][subIndex] = value
        } else {
            // undefined
            let categoryId = seletedCategories[0];
            postCategoryList[categoryId][subIndex] = value
        }
        this.setState({ postCategoryList })
    }


    handleSubmitCategories = () => {
        let { postCategoryList, corporateId, business_specialities } = this.state;
        let data = {
            "add_category": true,
            "categoryList": postCategoryList,
            "specialties": business_specialities,
            "listing_id": corporateId
        }

        this.props.post_business_category_data(data, corporateId);
        this.modalToggle();
    }

    handleChange = (e) => {
        let { name, value } = e.target;
        this.setState({ [name]: value })
    }

    toggleDeleteCategory = (id) => {
        this.setState({
            isToggleDeleteCategory: !this.state.isToggleDeleteCategory,
            editRecordId: id ? id : false,
        });
    }
    toggleDeletePayment = (data) => {
        this.setState({
            isToggleDeletePayment: !this.state.isToggleDeletePayment,
            editRecordId: data?.id ? data.id : false,
        });
    }
    deletePayment = () => {
        let { editRecordId, corporateId } = this.state;
		this.setState({
			paymentSwitch:{},
		},
        () => {
          this.props.delete_payment_option(editRecordId, corporateId)
			 this.toggleDeletePayment();
        }
      );		
    }

    toggleDeleteHours = (id) => {
        this.setState({
            isToggleDeleteHours: !this.state.isToggleDeleteHours,
            editRecordId: id ? id : false,
        });
    }


    deleteCategory = () => {
        let { editRecordId, corporateId } = this.state;
        this.props.delete_business_categories(editRecordId, corporateId)
        this.toggleDeleteCategory();
    }

    deleteHoursOfOperations = () => {
        let { editRecordId, corporateId } = this.state;
        this.props.delete_hours_of_operation(editRecordId, corporateId);
        this.toggleDeleteHours();
    }

    // componentDidMount() {
    //     let { HoursOfOperation } = this.state;
    //     this.setInitialHours();
    // }


    render() {
        let {
            isTogglePhone,
            isToggleAddress,
            isEditPhone,
            phoneDiary,
            addPhone,
            isPhoneValid,
            isTogglePayment,
            isEditWebsite,
            addWebsite,
            isToggleWebsite,
            webSet,
            isWebsiteValid,
            isToggleBusinessEmail,
            isEditBusinessEmail,
            addBusinessEmail,
            isBusinessEmailValid,
            emailSet,
            businessCategory,
            paymentSwitch,
            businessSubCategory,
            totalSubCategories,
            anotherCategories,
            business_specialities,
            isToggleDeleteCategory,
            HoursOfOperation,
            isToggleHours,
            isEditHours,
            addHours,
            isHoursValid,
            isToggleDeleteHours,
            isToggleDeletePayment,
            clonePaymentSwitch,
            isEditAddress,
            addAddress } = this.state;
        return (
            <React.Fragment>
                <CollapseBasic title="Business Info" isOpen={true}>
                    <FormGroup className="mb-2">
                        <Label size="sm" className="p-0 mb-1">Address</Label>

                        <div hidden>
                            <span className="small"><i>No Address Saved</i></span>
                        </div>
                        {/* Show Address */}
                        <div>
                            {this.adressData()}
                        </div>

                        {/* Add Address Field */}
                        <div className={isToggleAddress ? "" : "d-none"}>
                            <AvForm onSubmit={this.handleSubmitAddress}>
                                <div className="mb-2">
                                    <AvField bsSize="sm" className="mb-2" value={isEditAddress ? isEditAddress.address1 : addAddress.address1} onChange={this.handleChangeAddress} type="text" name="address1" placeholder="Address 1" errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                    <AvField bsSize="sm" className="mb-2" value={isEditAddress ? isEditAddress.address2 : addAddress.address2} onChange={this.handleChangeAddress} type="text"
                                        name="address2"
                                        placeholder="Address 2"
                                    // errorMessage="This Field is required" 
                                    // validate={{ required: { value: true } }}
                                    />
                                    <AvField bsSize="sm" className="mb-2" value={isEditAddress ? isEditAddress.city : addAddress.city} type="text" name="city" onChange={this.handleChangeAddress} placeholder="City" errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                    <AvField bsSize="sm" className="mb-2" value={isEditAddress ? isEditAddress.state : addAddress.state} type="text" name="state" onChange={this.handleChangeAddress} placeholder="State" errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                    <Input bsSize="sm" className="mb-2" value={isEditAddress ? isEditAddress.country : addAddress.country} onChange={this.handleChangeAddress} type="select" name="country" id="exampleSelect" errormessage="This Field is required" validate={{ required: { value: true } }}>
                                        <option value="United States">USA</option>
                                        <option value="CANADA">CANADA</option>
                                    </Input>
                                    <AvField bsSize="sm" className="mb-2" value={isEditAddress ? isEditAddress.zipcode : addAddress.zipcode} onChange={this.handleChangeAddress} type="text" name="zipcode" placeholder="Zip Code /  Postal Code" errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                    <div className="text-right">
                                        <Button size="sm" className="btn-icon-split mr-2" title="Save">
                                            <span className="text">Save</span>
                                            <span className="icon">
                                                <FontAwesomeIcon icon="check" />
                                            </span>
                                        </Button>
                                        <Button onClick={this.toggleEditAddress} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                            <span className="text">
                                                {'Cancel'}
                                            </span>
                                            <span className="icon">
                                                <FontAwesomeIcon icon="times" />
                                            </span>
                                        </Button>
                                    </div>
                                    <hr />
                                </div>
                            </AvForm>
                        </div>

                        {/* Hide when User adding */}
                        <div>
                            <div className="d-flex">
                                <span className="actionable ml-auto" onClick={this.setInitialAddress}><FontAwesomeIcon icon="plus" /> Add Address</span>
                            </div>
                        </div>
                    </FormGroup>

                    <FormGroup className="mb-2">
                        <Label size="sm" className="p-0 mb-1">Phone</Label>
                        {phoneDiary && phoneDiary.length > 0
                            ? (<div>{this.PhoneData()} </div>)
                            : (<div hidden><span className="small"><i>No Phone added</i></span></div>)
                        }

                        <div className={isTogglePhone ? "" : "d-none"}>
                            <AvForm>
                                <div className="mb-2">
                                    <Row form className="mb-2">
                                        <Col>
                                            <AvField
                                                className="mr-3"
                                                name="ph1"
                                                type="text"
                                                placeholder="xxx"
                                                bsSize="sm"
                                                onChange={this.handleChangePhone}
                                                value={isEditPhone ? isEditPhone.ph1 : addPhone.ph1}
                                                validate={{
                                                    pattern: { value: '^[0-9]+$' },
                                                    minLength: { value: 3 },
                                                    maxLength: { value: 3 },
                                                }}
                                            />
                                        </Col>
                                        <Col>
                                            <AvField
                                                className="mr-3"
                                                name="ph2"
                                                type="text"
                                                placeholder="xxx"
                                                bsSize="sm"
                                                onChange={this.handleChangePhone}
                                                value={isEditPhone ? isEditPhone.ph2 : addPhone.ph2}
                                                validate={{
                                                    pattern: { value: '^[0-9]+$' },
                                                    minLength: { value: 3 },
                                                    maxLength: { value: 3 },
                                                }}
                                            />
                                        </Col>
                                    </Row>
                                    <AvField
                                        className="mr-3"
                                        name="ph3"
                                        type="text"
                                        placeholder="xxxx"
                                        bsSize="sm"
                                        onChange={this.handleChangePhone}
                                        value={isEditPhone ? isEditPhone.ph3 : addPhone.ph3}
                                        validate={{
                                            pattern: { value: '^[0-9]+$' },
                                            minLength: { value: 4 },
                                            maxLength: { value: 4 },
                                        }}
                                    />
                                    <AvField onChange={this.handleChangePhone} className="mb-2" bsSize="sm" type="select" value={isEditPhone ? isEditPhone.label : addPhone.label} name="label">
                                        {!isEditPhone && <option>Choose Phone Type</option>}
                                        <option value="Tel">Telephone</option>
                                        <option value="Mob">Mobile</option>
                                        <option value="Support">Support</option>
                                    </AvField>
                                    <div className="text-right">
                                        <Button onClick={this.handleSubmitPhone} size="sm" className="btn-icon-split mr-2" title="Save" disabled={isPhoneValid} >
                                            <span className="text">Save</span>
                                            <span className="icon">
                                                <FontAwesomeIcon icon="check" />
                                            </span>
                                        </Button>
                                        <Button onClick={this.toggleEditPhone} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                            <span className="text">Cancel</span>
                                            <span className="icon"><FontAwesomeIcon icon="times" /></span>
                                        </Button>
                                    </div>
                                    <hr />
                                </div>
                            </AvForm>
                        </div>

                        {/* Hide when User adding */}
                        <div className={isTogglePhone ? "d-none" : ""} onClick={this.setInitialPhone}>
                            <div className="d-flex">
                                <span className="actionable ml-auto"><FontAwesomeIcon icon="plus" /> Add Phone Number</span>
                            </div>
                        </div>
                    </FormGroup>

                    <FormGroup className="mb-2">
                        <Label size="sm" className="p-0 mb-1">Websites</Label>
                        {webSet && webSet.length > 0 ?
                            (
                                <div>
                                    {this.websiteData()}
                                </div>
                            ) : (
                                <div>
                                    <span className="small"><i>No Website Saved</i></span>
                                </div>
                            )}

                        <div className={isToggleWebsite ? "" : "d-none"}>
                            <AvForm ref={this.refWebsiteForm} onValidSubmit={this.handleSubmitWebsite}>
                                <div className="mb-2">
                                    <AvField
                                        bsSize="sm"
                                        className="mb-2"
                                        type="textarea"
                                        name="website"
                                        value={isEditWebsite ? isEditWebsite.website : addWebsite.website}
                                        onChange={this.handleChangeWebsite}
                                        placeholder="Web URL"
                                        validate={{
                                            pattern: { value: websiteRegex },
                                        }}
                                    />
                                    <AvField
                                        onFocus={this.handleChangeWebsite}
                                        onChange={this.handleChangeWebsite}
                                        value={isEditWebsite ? isEditWebsite.website_type : addWebsite.website_type}
                                        className="mb-2"
                                        bsSize="sm"
                                        type="select"
                                        name="website_type"
                                    >
                                        <option value="">Choose website type</option>
                                        <option value="Main">Main Website</option>
                                        <option value="Facebook">Facebook Website</option>
                                        <option value="Google+">Google Website</option>
                                        <option value="Twitter">Twitter Website</option>
                                        <option value="LinkedIn">LinkedIn Website</option>
                                        <option value="Instagram">Instagram Website</option>
                                    </AvField>
                                    <div className="text-right">
                                        <Button size="sm" className="btn-icon-split mr-2" title="Save" disabled={isWebsiteValid} >
                                            <span className="text">Save</span>
                                            <span className="icon">
                                                <FontAwesomeIcon icon="check" />
                                            </span>
                                        </Button>
                                        <Button onClick={this.toggleEditWebsite} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                            <span className="text">Cancel</span>
                                            <span className="icon"><FontAwesomeIcon icon="times" /></span>
                                        </Button>
                                    </div>
                                    <hr />
                                </div>
                            </AvForm>
                        </div>

                        {/* Hide when User adding */}
                        <div className={isToggleWebsite ? "d-none" : ""} onClick={this.setInitialWebsite}>
                            <div className="d-flex">
                                <span className="actionable ml-auto"><FontAwesomeIcon icon="plus" /> Add Website</span>
                            </div>
                        </div>
                    </FormGroup>


                    <FormGroup className="mb-2">
                        <Label size="sm" className="p-0 mb-1">Email</Label>

                        {emailSet && emailSet.length > 0 ? (<div>{this.emailList()}</div>) : (<div>  <span className="small"><i>{'No Email Registered'}</i></span>  </div>)}


                        <div className={isToggleBusinessEmail ? "" : "d-none"}>
                            <div className="mb-2">
                                <AvForm ref={this.refBusinessEmailForm} onValidSubmit={this.handleSubmitBusinessEmail}>
                                    <AvField
                                        onFocus={this.handleChangeBusinessEmail}
                                        onChange={this.handleChangeBusinessEmail}
                                        value={isEditBusinessEmail ? isEditBusinessEmail.email : addBusinessEmail.email}
                                        bsSize="sm"
                                        className="mb-2"
                                        type="email"
                                        name="email"
                                        placeholder="Email..."
                                        validate={{
                                            pattern: { value: emailRegex },
                                        }}
                                    />
                                    <AvField
                                        onFocus={this.handleChangeBusinessEmail}
                                        onChange={this.handleChangeBusinessEmail}
                                        value={isEditBusinessEmail ? isEditBusinessEmail.email_type : addBusinessEmail.email_type}
                                        className="mb-2"
                                        bsSize="sm"
                                        type="select"
                                        name="email_type">
                                        <option>Choose Email Type</option>
                                        <option value="Sales">Sales mail</option>
                                        <option value="Reserve">Reserve mail</option>
                                        <option value="General">General mail</option>
                                    </AvField>
                                    <div className="text-right">
                                        <Button size="sm" className="btn-icon-split mr-2" title="Save" disabled={isBusinessEmailValid}>
                                            <span className="text">Save</span>
                                            <span className="icon">
                                                <FontAwesomeIcon icon="check" />
                                            </span>
                                        </Button>
                                        <Button onClick={this.toggleEditBusinessEmail} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                            <span className="text">Cancel</span>
                                            <span className="icon"><FontAwesomeIcon icon="times" /></span>
                                        </Button>
                                    </div>
                                </AvForm>
                                <hr />
                            </div>
                        </div>

                        {/* Hide when User adding */}
                        <div className={isToggleBusinessEmail ? "d-none" : ""} onClick={this.setInitialBusinessEmail}>
                            <div className="d-flex">
                                <span className="actionable ml-auto"><FontAwesomeIcon icon="plus" /> Add Business Email</span>
                            </div>
                        </div>
                    </FormGroup>

                    <FormGroup className="mb-2">
                        <Label size="sm" className="p-0 mb-1">Payment Options</Label>
                        {/* Show If no data */}
                        <div hidden>
                            <span className="small"><i>No Payment Method added.</i></span>
                        </div>

                        {/* Show if data */}
                        <div>
                            <div className="d-flex  flex-wrap mx-n1 mb-2">
                                {this.payOptions()}
                            </div>
                        </div>

                        {/* Show when adding/ editing */}
                        <div className={isTogglePayment ? "" : "d-none"}>
                            <div className="mb-2">
                                <Label size="sm" className="p-0 mb-1">Select Payment Method</Label>
                                <div className="mb-2">
                                    <Row form>

                                        <Col xs="auto">
                                            <Switch
                                                value={paymentSwitch.cash ? 1 : 0}
                                                name="cash"
                                                onChange={(e) => this.handleSwitch(e, 'cash')}
                                            />  {` Cash `}
                                        </Col>

                                        <Col xs="auto">
                                            <Switch
                                                value={paymentSwitch.check ? 1 : 0}
                                                name="check"
                                                onChange={(e) => this.handleSwitch(e, 'check')}
                                            /> {` Check `}
                                        </Col>

                                        <Col xs="auto">
                                            <Switch
                                                value={paymentSwitch.discover ? 1 : 0}
                                                name="discover"
                                                onChange={(e) => this.handleSwitch(e, 'discover')}
                                            /> {` Discover `}
                                        </Col>
                                        <Col xs="auto">
                                            <Switch
                                                value={paymentSwitch.visa ? 1 : 0}
                                                name="visa"
                                                onChange={(e) => this.handleSwitch(e, 'visa')}
                                            /> {` Visa `}
                                        </Col>

                                        <Col xs="auto">
                                            <Switch
                                                value={paymentSwitch.mastercard ? 1 : 0}
                                                name="mastercard"
                                                onChange={(e) => this.handleSwitch(e, 'mastercard')}
                                            /> {` Mastercard `}
                                        </Col>

                                        <Col xs="auto">

                                            <Switch
                                                value={paymentSwitch.apple ? 1 : 0}
                                                name="apple"
                                                onChange={(e) => this.handleSwitch(e, 'apple')}
                                            /> {` Apple Pay `}
                                        </Col>

                                        <Col xs="auto">

                                            <Switch
                                                value={paymentSwitch.debit ? 1 : 0}
                                                name="Debit"
                                                onChange={(e) => this.handleSwitch(e, 'debit')}
                                            /> {` Debit Card `}
                                        </Col>
                                        <Col xs="auto">
                                            <Switch
                                                value={paymentSwitch.american ? 1 : 0}
                                                name="american"
                                                onChange={(e) => this.handleSwitch(e, 'american')}
                                            /> {` American Express `}
                                        </Col>
                                        <Col xs="auto">
                                            <Switch
                                                value={paymentSwitch.google ? 1 : 0}
                                                name="google"
                                                onChange={(e) => this.handleSwitch(e, 'google')}
                                            /> {` Google Wallet `}
                                        </Col>
                                        <Col xs="auto">
                                            <Switch
                                                value={paymentSwitch.cryptocurrency ? 1 : 0}
                                                name="cryptocurrency"
                                                onChange={(e) => this.handleSwitch(e, 'cryptocurrency')}
                                            /> {` Cryptocurrency `}
                                        </Col>
                                    </Row>

                                </div>
                                <div className="text-right">
                                    <Button onClick={this.handleSubmitPayments} size="sm" className="btn-icon-split mr-2" title="Submit">
                                        <span className="text">{'Submit'}</span>
                                        <span className="icon">
                                            <FontAwesomeIcon icon="check" />
                                        </span>
                                    </Button>
                                    <Button onClick={() => { this.setState({ isTogglePayment: false }, () => this.setPaymentReset()) }} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                        <span className="text">{'Cancel'}</span>
                                        <span className="icon">
                                            <FontAwesomeIcon icon="times" />
                                        </span>
                                    </Button>
                                </div>
                                <hr />
                            </div>
                        </div>
                        {/* Hide when adding/ editing */}
                        <div>
                            <div className="d-flex">
                                <span className="actionable ml-auto" onClick={this.togglePayment}><FontAwesomeIcon icon="plus" /> Add Payment Method</span>
                            </div>
                        </div>
                    </FormGroup>

                    <FormGroup className="mb-2">
                        <Label size="sm" className="p-0 mb-1">Categories</Label>

                        {/* Show If no data */}
                        <div hidden>
                            <span className="small"><i>No Categories added.</i></span>
                        </div>

                        {/* Show if data */}
                        <div>
                            <div className="d-flex flex-wrap mx-n1 mb-2">
                                {this.categoryList()}

                            </div>
                        </div>

                        {/* Trigger Modal Pop up on below action */}
                        <div>
                            <div className="d-flex">
                                <span className="actionable ml-auto" onClick={this.modalToggle}><FontAwesomeIcon icon="plus" /> Add Categories</span>
                            </div>
                        </div>
                    </FormGroup>

                    <FormGroup className="mb-2">
                        <Label size="sm" className="p-0 mb-1">Hours of Operation</Label>
                        {HoursOfOperation && Array.isArray(HoursOfOperation) && HoursOfOperation.length > 0 ?
                            (
                                <div>
                                    <ul className="list-unstyled mb-2 fs-14">
                                        {this.HoursOfOperation()}
                                    </ul>
                                </div>
                            )
                            : (
                                <div>
                                    <span className="small"><i>No Hours added.</i></span>
                                </div>
                            )
                        }

                        <div>
                            {/* <div className={isToggleHours ? "" : "d-none"}> */}
                            <div className="mb-2">
                                <AvForm>
                                    <Row className="form-row">
                                        <Col sm={addHours.start_time !== "Closed" ? '4' : '6'} md={12} className="col-xxl-4">
                                            <div className="mb-2">
                                                <AvField onChange={this.handleChangeHours} bsSize="sm" type="select" name="day_of_week" value={isEditHours ? isEditHours.day_of_week : addHours.day_of_week}>
                                                    <option disabled>Select Day</option>
                                                    <option value="1">Monday</option>
                                                    <option value="2" >Tuesday</option>
                                                    <option value="3" >Wednesday</option>
                                                    <option value="4">Thursday</option>
                                                    <option value="5">Friday</option>
                                                    <option value="6">Saturday</option>
                                                    <option value="7">Sunday</option>
                                                </AvField>
                                            </div>
                                        </Col>
                                        <Col sm={addHours.start_time !== "Closed" ? '4' : '6'} md={12} className="col-xxl-4">
                                            <div className="mb-2">
                                                <AvField onChange={this.handleChangeHours} bsSize="sm" type="select" name="start_time"
                                                    value={
                                                        isEditHours ? moment(isEditHours.start_time, 'HH:mm:ss').format('HH:mm') :
                                                            addHours.start_time !== "Closed" ? moment(addHours.start_time, 'HH:mm:ss').format('HH:mm') : addHours.start_time}>
                                                    <option disabled>Select Opening Hours</option>
                                                    {/* <option value="Closed">Closed</option> */}
                                                    <option value="00:00">12:00 am (midnight)</option>
                                                    <option value="00:30">12:30 am</option>
                                                    <option value="01:00">1:00 am</option>
                                                    <option value="01:30">1:30 am</option>
                                                    <option value="02:00">2:00 am</option>
                                                    <option value="02:30">2:30 am</option>
                                                    <option value="03:00">3:00 am</option>
                                                    <option value="03:30">3:30 am</option>
                                                    <option value="04:00">4:00 am</option>
                                                    <option value="04:30">4:30 am</option>
                                                    <option value="05:00">5:00 am</option>
                                                    <option value="05:30">5:30 am</option>
                                                    <option value="06:00">6:00 am</option>
                                                    <option value="06:30">6:30 am</option>
                                                    <option value="07:00">7:00 am</option>
                                                    <option value="07:30">7:30 am</option>
                                                    <option value="08:00">8:00 am</option>
                                                    <option value="08:30">8:30 am</option>
                                                    <option value="09:00">9:00 am</option>
                                                    <option value="09:30">9:30 am</option>
                                                    <option value="10:00">10:00 am</option>
                                                    <option value="10:30">10:30 am</option>
                                                    <option value="11:00">11:00 am</option>
                                                    <option value="11:30">11:30 am</option>
                                                    <option value="12:00">12:00 pm</option>
                                                    <option value="12:30">12:30 pm</option>
                                                    <option value="13:00">1:00 pm</option>
                                                    <option value="13:30">1:30 pm</option>
                                                    <option value="14:00">2:00 pm</option>
                                                    <option value="14:30">2:30 pm</option>
                                                    <option value="15:00">3:00 pm</option>
                                                    <option value="15:30">3:30 pm</option>
                                                    <option value="16:00">4:00 pm</option>
                                                    <option value="16:30">4:30 pm</option>
                                                    <option value="17:00">5:00 pm</option>
                                                    <option value="17:30">5:30 pm</option>
                                                    <option value="18:00">6:00 pm</option>
                                                    <option value="18:30">6:30 pm</option>
                                                    <option value="19:00">7:00 pm</option>
                                                    <option value="19:30">7:30 pm</option>
                                                    <option value="20:00">8:00 pm</option>
                                                    <option value="20:30">8:30 pm</option>
                                                    <option value="21:00">9:00 pm</option>
                                                    <option value="21:30">9:30 pm</option>
                                                    <option value="22:00">10:00 pm</option>
                                                    <option value="22:30">10:30 pm</option>
                                                    <option value="23:00">11:00 pm</option>
                                                    <option value="23:30">11:30 pm</option>
                                                </AvField>
                                            </div>
                                        </Col>
                                        <Col sm="4" md={12} className="col-xxl-4">
                                            <div className={addHours.start_time !== "Closed" ? 'mb-2' : 'd-none'}>
                                                <AvField onChange={this.handleChangeHours} bsSize="sm" type="select" name="end_time"
                                                    value={isEditHours ? moment(isEditHours.end_time, 'HH:mm:ss').format('HH:mm') : moment(addHours.end_time, 'HH:mm:ss').format('HH:mm')}>
                                                    <option disabled>Select Closing Hours</option>
                                                    <option value="00:30">12:30 am</option>
                                                    <option value="01:00">1:00 am</option>
                                                    <option value="01:30">1:30 am</option>
                                                    <option value="02:00">2:00 am</option>
                                                    <option value="02:30">2:30 am</option>
                                                    <option value="03:00">3:00 am</option>
                                                    <option value="03:30">3:30 am</option>
                                                    <option value="04:00">4:00 am</option>
                                                    <option value="04:30">4:30 am</option>
                                                    <option value="05:00">5:00 am</option>
                                                    <option value="05:30">5:30 am</option>
                                                    <option value="06:00">6:00 am</option>
                                                    <option value="06:30">6:30 am</option>
                                                    <option value="07:00">7:00 am</option>
                                                    <option value="07:30">7:30 am</option>
                                                    <option value="08:00">8:00 am</option>
                                                    <option value="08:30">8:30 am</option>
                                                    <option value="09:00">9:00 am</option>
                                                    <option value="09:30">9:30 am</option>
                                                    <option value="10:00">10:00 am</option>
                                                    <option value="10:30">10:30 am</option>
                                                    <option value="11:00">11:00 am</option>
                                                    <option value="11:30">11:30 am</option>
                                                    <option value="12:00">12:00 pm (noon)</option>
                                                    <option value="12:30">12:30 pm</option>
                                                    <option value="13:00">1:00 pm</option>
                                                    <option value="13:30">1:30 pm</option>
                                                    <option value="14:00">2:00 pm</option>
                                                    <option value="14:30">2:30 pm</option>
                                                    <option value="15:00">3:00 pm</option>
                                                    <option value="15:30">3:30 pm</option>
                                                    <option value="16:00">4:00 pm</option>
                                                    <option value="16:30">4:30 pm</option>
                                                    <option value="17:00">5:00 pm</option>
                                                    <option value="17:30">5:30 pm</option>
                                                    <option value="18:00">6:00 pm</option>
                                                    <option value="18:30">6:30 pm</option>
                                                    <option value="19:00">7:00 pm</option>
                                                    <option value="19:30">7:30 pm</option>
                                                    <option value="20:00">8:00 pm</option>
                                                    <option value="20:30">8:30 pm</option>
                                                    <option value="21:00">9:00 pm</option>
                                                    <option value="21:30">9:30 pm</option>
                                                    <option value="22:00">10:00 pm</option>
                                                    <option value="22:30">10:30 pm</option>
                                                    <option value="23:00">11:00 pm</option>
                                                    <option value="23:30">11:30 pm</option>
                                                    <option value="00:00 1">12:00 am (midnight next day)</option>
                                                    <option value="00:30 1">12:30 am (next day)</option>
                                                    <option value="01:00 1">1:00 am (next day)</option>
                                                    <option value="01:30 1">1:30 am (next day)</option>
                                                    <option value="02:00 1">2:00 am (next day)</option>
                                                    <option value="02:30 1">2:30 am (next day)</option>
                                                    <option value="03:00 1">3:00 am (next day)</option>
                                                    <option value="03:30 1">3:30 am (next day)</option>
                                                    <option value="04:00 1">4:00 am (next day)</option>
                                                    <option value="04:30 1">4:30 am (next day)</option>
                                                    <option value="05:00 1">5:00 am (next day)</option>
                                                    <option value="05:30 1">5:30 am (next day)</option>
                                                    <option value="06:00 1">6:00 am (next day)</option>
                                                </AvField>
                                            </div>
                                        </Col>
                                        {/* <Col sm={addHours.start_time !== "Closed" ? "6" : "12"}>
                                            <div className="mb-2">
                                                <AvField
                                                    onChange={this.handleChangeHours}
                                                    value={isEditHours ? isEditHours.info : addHours.info}
                                                    bsSize="sm"
                                                    type="text"
                                                    name="info"
                                                    placeholder="i.e. Brunch"
                                                />
                                            </div>
                                        </Col> */}
                                    </Row>

                                    <div className="text-right">
                                        <Button onClick={this.handleSubmitHours} size="sm" className="btn-icon-split mr-0" title="Submit" disabled={isHoursValid}>
                                            <span className="text">{'Add Hours'}</span>
                                            <span className="icon">
                                                <FontAwesomeIcon icon="check" />
                                            </span>
                                        </Button>
                                        {/* <Button onClick={this.toggleEditHours} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                            <span className="text">{'Cancel'}</span>
                                            <span className="icon">
                                                <FontAwesomeIcon icon="times" />
                                            </span>
                                        </Button> */}
                                    </div>
                                    <hr />
                                </AvForm>
                            </div>
                        </div>

                        {/* Hide when adding/ editing */}
                        {/* <div>
                            <div className="d-flex">
                                <span onClick={this.setInitialHours} className="actionable ml-auto"><FontAwesomeIcon icon="plus" /> Add more hours</span>
                            </div>
                        </div> */}
                    </FormGroup>
                </CollapseBasic>


                <Modal isOpen={this.state.addmodal} toggle={this.modalToggle}>
                    <ModalHeader toggle={this.modalToggle}></ModalHeader>
                    <ModalBody className="bg-dark text-white text-center">
                        <h2 className="mb-3">Categories &amp; Specialities</h2>
                        <p className="small">{'Select the right category for your business'}</p>

                        <div className="pt-4">
                            {/* Always visible */}
                            <div className="mb-3">
                                <Row form className="justify-content-between">
                                    <Col md={6}>
                                        <FormGroup className="mb-3">
                                            <Input onChange={this.handleChangeCategory} bsSize="sm" type="select" name="choose_category" data-id="zero" className="transparent">
                                                <option value="">Choose Category</option>
                                                {businessCategory && Array.isArray(businessCategory) && businessCategory.length > 0 ?
                                                    businessCategory.map((item, idx_1) => {
                                                        return (<option key={idx_1} value={item.id}>{item.category}</option>)
                                                    }) : ''}
                                            </Input>
                                        </FormGroup>
                                    </Col>
                                    {/* hide subcategories by default */}
                                    {totalSubCategories && Array.isArray(totalSubCategories) && totalSubCategories.length > 0 ?
                                        totalSubCategories.map((subItem, index) => {
                                            return (
                                                <React.Fragment key={index}>
                                                    {index !== 0 && <Col md={6}></Col>}
                                                    <Col key={index} md={6} className={businessSubCategory && businessSubCategory[0] && businessSubCategory[0].length > 0 ? "" : "d-none"}>
                                                        {/* Closable Sub Categories block */}
                                                        <div className="d-flex mr-n2">
                                                            <FormGroup className="mb-3 flex-grow-1">
                                                                <Input onChange={(e) => this.changeSubCategory(e, 'undefined', index)} bsSize="sm" type="select" name="choose_sub_category" className="transparent">
                                                                    <option value="">Choose Subcategory</option>
                                                                    {businessSubCategory && businessSubCategory[0] && Array.isArray(businessSubCategory[0]) && businessSubCategory[0].length > 0 ?
                                                                        businessSubCategory[0].map(item => {
                                                                            return (<option value={item.id}>{item.category}</option>)
                                                                        }) : ''}
                                                                </Input>
                                                            </FormGroup>
                                                            {index !== 0 && <Button
                                                                onClick={() => { this.removeSubCateItems(index) }}
                                                                color="circle"
                                                                size="xs"
                                                                className="bg-primary align-self-start ml-n2 mt-n2"
                                                                title="Remove"
                                                            >
                                                                <FontAwesomeIcon icon="times" />
                                                            </Button>}
                                                        </div>
                                                    </Col>
                                                </React.Fragment>
                                            )
                                        }
                                        ) : null}

                                    <Col md={6} className="ml-auto">
                                        <div className="text-right">
                                            <Button
                                                hidden={businessSubCategory && businessSubCategory[0] && businessSubCategory[0].length == 0 ? true : false}
                                                onClick={this.addSubCategories}
                                                color="link"
                                                size="sm"
                                                className="text-primary">
                                                <FontAwesomeIcon icon="plus" size="sm" />
                                                add subcategory
                                                </Button>
                                        </div>
                                    </Col>
                                </Row>
                                <hr className="bg-primary" />
                            </div>

                            {/* On Click off Add Category, Repeat below */}
                            {anotherCategories && anotherCategories.length > 0 ?
                                anotherCategories.map((item, index) => {
                                    return (
                                        <div key={index} className="mb-3" >
                                            <Row form className="justify-content-between">
                                                <Col md={6}>
                                                    <FormGroup className="mb-3">
                                                        <Input onChange={this.handleChangeCategory} bsSize="sm" type="select" name="choose_category" className="transparent">
                                                            <option value="">Choose Category</option>
                                                            {businessCategory && Array.isArray(businessCategory) && businessCategory.length > 0 ?
                                                                businessCategory.map(item => {
                                                                    return (<option value={item.id}>{item.category}</option>)
                                                                }) : ''}
                                                        </Input>
                                                    </FormGroup>
                                                </Col>
                                                {/* {'sub-categories'} */}
                                                {anotherCategories && anotherCategories[index] && Array.isArray(anotherCategories[index]) && anotherCategories[index].length > -1 ?
                                                    anotherCategories[index].map((subItems, subIndex) => {
                                                        return (
                                                            <React.Fragment>
                                                                {subIndex !== 0 && <Col key={subIndex} md={6}></Col>}
                                                                <Col key={subIndex} md={6} className={businessSubCategory && businessSubCategory[index + 1] && businessSubCategory[index + 1].length > 0 ? "" : "d-none"}>
                                                                    {/* Closable Sub Categories block */}
                                                                    <div className="d-flex mr-n2">
                                                                        <FormGroup className="mb-3 flex-grow-1">
                                                                            <Input onChange={(e) => this.changeSubCategory(e, index + 1, subIndex)} bsSize="sm" type="select" name="choose_sub_category" className="transparent">
                                                                                <option value="">Choose Subcategory</option>
                                                                                {businessSubCategory && businessSubCategory[index + 1] && Array.isArray(businessSubCategory[index + 1]) && businessSubCategory[index + 1].length > 0 ?
                                                                                    businessSubCategory[index + 1].map((item, idx_3) => {
                                                                                        return (<option key={idx_3} value={item.id}>{item.category}</option>)
                                                                                    }) : ''}
                                                                            </Input>
                                                                        </FormGroup>
                                                                        {subIndex !== 0 && <Button
                                                                            onClick={() => { this.removeSubCategoryItem(index + 1, subIndex) }}
                                                                            color="circle"
                                                                            size="xs"
                                                                            className="bg-primary align-self-start ml-n2 mt-n2"
                                                                            title="Remove"
                                                                        >
                                                                            <FontAwesomeIcon icon="times" />
                                                                        </Button>}
                                                                    </div>
                                                                </Col>
                                                            </React.Fragment>
                                                        )
                                                    }) : null
                                                }

                                                <Col md={6} className="ml-auto">
                                                    <div className="text-right">
                                                        <Button onClick={() => this.addAnotherSubCategories(index)} color="link" size="sm" className="text-primary">
                                                            <FontAwesomeIcon icon="plus" size="sm" />
                                                            {'add subcategory'}
                                                        </Button>
                                                    </div>
                                                </Col>


                                            </Row>
                                            <hr className="bg-primary" />
                                        </div>
                                    )
                                }) : null}

                            <div className="text-right mb-3">
                                <Button onClick={this.addAnotherCategories} color="link" size="sm" className="text-primary">
                                    <FontAwesomeIcon icon="plus" size="sm" /> add another category
                                </Button>
                            </div>

                            <div>
                                <FormGroup>
                                    <Label className="d-block text-center text-white font-weight-normal">What your business specialities are?</Label>
                                    <Input onChange={this.handleChange} bsSize="sm" className="transparent" type="textarea" rows="5" name="business_specialities" placeholder="Explain what gets you stand out from your competitors.What do customers love about your business?" value={business_specialities} />
                                </FormGroup>
                            </div>
                            <div>
                                <Button onClick={this.handleSubmitCategories} size="lg" block color="primary">Save &amp; Continue</Button>
                                <Button size="sm" block color="link" onClick={this.modalToggle}>Skip this part</Button>
                            </div>
                        </div>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.isDeleteToggle} toggle={this.deleteToggle}>
                    <ModalHeader toggle={this.deleteToggle}></ModalHeader>
                    <ModalBody className="text-center">
                        <h2 className="mb-3">Confirmation</h2>
                        <p className="small">Are you sure you want to delete?</p>

                        <div className="pt-4">
                            <div>
                                <Button onClick={this.deleteToggle} size="md" color="primary">Cancel</Button>
                                <Button onClick={this.deleteBusinessRecord} size="md" color="primary">Ok</Button>
                            </div>
                        </div>
                    </ModalBody>
                </Modal>

                <Modal isOpen={isToggleDeletePayment} toggle={this.toggleDeletePayment}>
                    <ModalHeader toggle={this.toggleDeletePayment}></ModalHeader>
                    <ModalBody className="text-center">
                        <h2 className="mb-3">Confirmation</h2>
                        <p className="small">Are you sure you want to delete?</p>

                        <div className="pt-4">
                            <div>
                                <Button onClick={this.toggleDeletePayment} size="md" color="primary">Cancel</Button>
                                <Button onClick={this.deletePayment} size="md" color="primary">Ok</Button>
                            </div>
                        </div>
                    </ModalBody>
                </Modal>
                <Modal isOpen={isToggleDeleteCategory} toggle={this.toggleDeleteCategory}>
                    <ModalHeader toggle={this.toggleDeleteCategory}></ModalHeader>
                    <ModalBody className="text-center">
                        <h2 className="mb-3">Confirmation</h2>
                        <p className="small">Are you sure you want to delete?</p>

                        <div className="pt-4">
                            <div>
                                <Button onClick={this.toggleDeleteCategory} size="md" color="primary">Cancel</Button>
                                <Button onClick={this.deleteCategory} size="md" color="primary">Ok</Button>
                            </div>
                        </div>
                    </ModalBody>
                </Modal>

                <Modal isOpen={isToggleDeleteHours} toggle={this.toggleDeleteHours}>
                    <ModalHeader toggle={this.toggleDeleteHours}></ModalHeader>
                    <ModalBody className="text-center">
                        <h2 className="mb-3">Confirmation</h2>
                        <p className="small">Are you sure you want to delete?</p>

                        <div className="pt-4">
                            <div>
                                <Button onClick={this.toggleDeleteHours} size="md" color="primary">Cancel</Button>
                                <Button onClick={this.deleteHoursOfOperations} size="md" color="primary">Ok</Button>
                            </div>
                        </div>
                    </ModalBody>
                </Modal>
            </React.Fragment >
        )
    }
}

const mapState = (state) => {
    return {
        corporate_id: state.user.corporate_id,
        corporate_details: state.user.corporate_id_details,
        business_category: state.user.business_category,
        business_sub_category: state.user.business_sub_category,
        payment_update: state.user.payment_update,
        update_pay_option: state.user.update_pay_option

    }
}

const mapProps = (dispatch) => {
    return {
        get_corporate_id: () => dispatch(corporate_id()),
        get_corporate_id_details: (id) => dispatch(corporate_id_details(id)),
        post_business_update: (data, isAdd, userId) => dispatch(post_business_update(data, isAdd, userId)),
        delete_business_record: (entity, deleteId, userId) => dispatch(delete_business_record(entity, deleteId, userId)),
        get_business_category_data: (type) => dispatch(get_business_category_data(type)),
        post_business_category_data: (data, userId) => dispatch(post_business_category_data(data, userId)),
        update_payment_options: (userId, data) => dispatch(update_payment_options(userId, data)),
        delete_business_categories: (entityid, userId) => dispatch(delete_business_categories(entityid, userId)),
        delete_payment_option: (entityid, userentry) => dispatch(delete_payment_option(entityid, userentry)),
        post_hours_of_operation: (data, userId) => dispatch(post_hours_of_operation(data, userId)),
        put_hours_of_operation: (data, id, userId) => dispatch(put_hours_of_operation(data, id, userId)),
        delete_hours_of_operation: (id, userId) => dispatch(delete_hours_of_operation(id, userId)),
        get_sub_categories: (item) => dispatch(get_sub_categories(item))
    }
}

export default withRouter(connect(mapState, mapProps)(BusinessInfo));