import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Row,
  Col,
  FormGroup,
  Input,
  Button,
  Label,
  CustomInput,
} from "reactstrap";
import {
  get_settings_list,
  update_settings,
} from "../../../actions/admin-view/admin";

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      settingsList: [],
      error: "",
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.settings_list.length > 0) {
      let red = nextProps.settings_list.filter((obj) => obj.colour_id == 1);
      let orange = nextProps.settings_list.filter((obj) => obj.colour_id == 2);
      this.setState({
        settingsList: [...red, ...orange],
        error: "",
      });
    }
  }
  componentDidMount() {
    this.props.get_settings_list();
  }
  handleOnChangeSettingHours = (e) => {
    let newSetting = [];
    if (e.target.id.includes("red")) {
      
      newSetting = this.state.settingsList.map((setting) => {
        if (setting.colour_id == 1) {
          setting.hours = parseInt(e.target.value);
        }
        return setting;
      });
    } else if (e.target.id.includes("orange")) {
      let settingID = 1;
      
      newSetting = this.state.settingsList.map((setting) => {
        if (setting.colour_id == 2) {
          setting.minutes = 0;
          setting.setting_id = settingID;
          setting.hours = parseInt(e.target.value);
        }
        return setting;
      });
    }
    this.setState({
      settingsList: newSetting,
    });
   
  };
  handleOnChangeSettingMinutes = (e) => {
    let newSetting = [];
    if (e.target.id.includes("red")) {
      newSetting = this.state.settingsList.map((setting) => {
        if (setting.colour_id == 1) {
          setting.minutes = parseInt(e.target.value);
        }
        return setting;
      });
    } else if (e.target.id.includes("orange")) {
      let settingID = 1;
      newSetting = this.state.settingsList.map((setting) => {
        if (setting.colour_id == 2) {
          setting.setting_id = settingID;
          setting.minutes = parseInt(e.target.value);
        }
        return setting;
      });
    }
    this.setState({
      settingsList: newSetting,
    });
  };
  handleSubmit = () => {
    let settings = this.state.settingsList;
    let setting = {
      red: `${settings[0].setting_id},${settings[0].hours}:${settings[0].minutes}`,
      orange: `${settings[1].setting_id},${settings[1].hours}:${settings[1].minutes}`,
    };
    if (settings[0].hours < settings[1].hours) {
      this.setState({
        error: "Red Alert time must be greater than Orange Alert time",
      });
    } else if (
      settings[0].hours == settings[1].hours &&
      settings[0].minutes < settings[1].minutes
    ) {
      this.setState({
        error: "Red Alert time must be greater than Orange Alert time",
      });
    } else {
      this.props.update_settings(setting);
      this.setState({
        error: "",
      });
    }
  };
  handleOnCancel = () => {
    this.props.get_settings_list();
    this.setState({
      error: "",
    });
  };

  handleOnChangeSettingId = (e) => {
    let newSetting = [];
    if (e.target.id.includes("red")) {
      newSetting = this.state.settingsList.map((setting) => {
        if (setting.colour_id == 1) {
          setting.setting_id = parseInt(e.target.value);
          setting.hours = 0;
          setting.minutes = 0;
        }
        return setting;
      });
    } else if (e.target.id.includes("orange")) {
      let settingID = 1;
      newSetting = this.state.settingsList.map((setting) => {
        if (setting.colour_id == 2) {
          setting.setting_id = settingID;
          setting.hours = 0;
          setting.minutes = 0;
        }
        return setting;
      });
    }
    this.setState({
      settingsList: newSetting,
    });
  };

  render() {
    const { settingsList } = this.state;
    const { settings_list } = this.props;

   

    return (
      <div className="bg-white p-3 mb-3">
        <h3 className="text-secondary mb-4">Assign Alert Parameters</h3>
        <div className="text-primary-dark fs-14">
          <p className="ff-base mb-2">
            The below colors reflect the time you designate for a customer
            response during business hours.
          </p>
          <p className="ff-base mb-2">
            <span className="badge badge-danger badge-empty rounded-circle align-middle"></span>{" "}
            &nbsp; Red - Overdue
          </p>
          <p className="ff-base mb-2">
            <span className="badge badge-warning badge-empty rounded-circle align-middle"></span>{" "}
            &nbsp; Orange - Pending
          </p>
          <p className="ff-base mb-2">
            <span className="badge badge-success badge-empty rounded-circle align-middle"></span>{" "}
            &nbsp; Green - Completed
          </p>
        </div>
        <hr className="bg-secondary my-4" />

        <div className="step-16">
          <Row form>
            <Col xs="auto">
              <div className="h-100 bg-danger" style={{ width: '10px' }}></div>
            </Col>
            <Col>
              <h5 className="text-danger mb-3 ff-base">Red Alert</h5>
              <div className="mb-4">
                <div className="mb-3">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="red_noResponseWithin"
                    name="red_setting_id"
                    onChange={this.handleOnChangeSettingId}
                    label="Anything not responded to within:"
                    value="1"
                    checked={
                      settingsList.length > 0 &&
                      settingsList[0].setting_id === 1
                    }
                  />
                </div>
                <div>
                    <Row form>
                      <Col xs="auto">
                        <FormGroup>
                        <div className="d-flex flex-nowrap align-items-center px-4">
						<Input
                           disabled = { settingsList.length > 0 &&
                            settingsList[0].setting_id === 1 ? false :true}
                            bsSize="lg"
                            type="select"
                            name="hours"
                            className="mr-3"
                            onChange={this.handleOnChangeSettingHours}
                            id="red_noResponseWithin_hours1"
                            value={
                              settingsList.length > 0 &&
                              settingsList[0].setting_id === 1 &&
                              settingsList[0].hours
                                ? settingsList[0].hours
                                : 0
                            }
                          >
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                          </Input>

                          <Label className="text-primary-dark font-weight-normal mb-0">
                            Hours
                          </Label>

                        </div>
                        </FormGroup>
                      </Col>
                      <Col xs="auto">
                        <FormGroup>
                        <div className="d-flex flex-nowrap align-items-center px-4">
                          <Input
                           disabled = { settingsList.length > 0 &&
                            settingsList[0].setting_id === 1 ? false :true}
                            bsSize="lg"
                            type="select"
                            name="minutes"
                            className="mr-3"
                            onChange={this.handleOnChangeSettingMinutes}
                            id="red_noResponseWithin_minutes1"
                            value={
                              settingsList.length > 0 &&
                              settingsList[0].setting_id === 1 &&
                              settingsList[0].minutes
                                ? settingsList[0].minutes
                                : 0
                            }
                          >
                            <option value="0">0</option>
                            <option value="5">05</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                            <option value="35">35</option>
                            <option value="40">40</option>
                            <option value="45">45</option>
                            <option value="50">50</option>
                            <option value="55">55</option>
                          </Input>

                          <Label className="text-primary-dark font-weight-normal mb-0">
                            Minutes
                          </Label>
                          
                        </div>
                        </FormGroup>
                      </Col>
                    </Row>
                </div>
              </div>

              <div className="mb-4">
                <div className="mb-3">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="red_hrsBeforeClosing"
                    name="red_setting_id"
                    label="The number of hours before close of business:"
                    value="2"
                    onChange={this.handleOnChangeSettingId}
                    checked={
                      settingsList.length > 0 &&
                      settingsList[0].setting_id === 2
                    }
                  />
                </div>
                <div>
                    <Row form>
                      <Col xs="auto">
                        <FormGroup>
                        <div className="d-flex flex-nowrap align-items-center px-4">
                          <Input
                          disabled = { settingsList.length > 0 &&
                            settingsList[0].setting_id === 2 ? false :true}
                            bsSize="lg"
                            type="select"
                            name="hours"
                            className="mr-3"
                            onChange={this.handleOnChangeSettingHours}
                            id="red_hrsBeforeClosing_hours2"
                            value={
                              settingsList.length > 0 &&
                              settingsList[0].setting_id === 2 &&
                              settingsList[0].hours
                                ? settingsList[0].hours
                                : 0
                            }
                          >
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                          </Input>

                          <Label className="text-primary-dark font-weight-normal mb-0">
                            Hours
                          </Label>
                          
                        </div>
                        </FormGroup>
                      </Col>
                      <Col xs="auto">
                        <FormGroup>
                        <div className="d-flex flex-nowrap align-items-center px-4"> 
                          <Input
                           disabled = { settingsList.length > 0 &&
                            settingsList[0].setting_id === 2 ? false :true}
                            bsSize="lg"
                            type="select"
                            name="minutes"
                            className="mr-3"
                            onChange={this.handleOnChangeSettingMinutes}
                            id="red_hrsBeforeClosing_minutes2"
                            value={
                              settingsList.length > 0 &&
                              settingsList[0].setting_id === 2 &&
                              settingsList[0].minutes
                                ? settingsList[0].minutes
                                : 0
                            }
                          >
                            <option value="0">0</option>
                            <option value="5">05</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                            <option value="35">35</option>
                            <option value="40">40</option>
                            <option value="45">45</option>
                            <option value="50">50</option>
                            <option value="55">55</option>
                          </Input>

                          <Label className="text-primary-dark font-weight-normal mb-0">
                            Minutes
                          </Label>
                          
                        </div>
                        </FormGroup>
                      </Col>
                    </Row>
                </div>
              </div>

              <div className="mb-4">
                <div className="mb-3">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="red_noRespBusHrs"
                    name="red_setting_id"
                    value="3"
                    onChange={this.handleOnChangeSettingId}
                    label="Anything not responded to during business hours:"
                    checked={
                      settingsList.length > 0 &&
                      settingsList[0].setting_id === 3
                    }
                  />
                </div>
              </div>
              <p className="text-primary-dark fs-14 ff-base">
                <span>
                All the Red alerts are overdue tasks still pending that and that were not completed within the desired timeframe.

                </span>
              </p>
            </Col>
          </Row>
          
          <hr className="bg-secondary" />
        </div>

        <div>
          <Row form>
            <Col xs="auto">
              <div className="h-100 bg-warning" style={{ width: '10px' }}>&nbsp;</div>
            </Col>
            <Col>
              <h5 className="text-warning mb-3 ff-base">Orange Alert</h5>
              <div className="mb-4">
                {/* <div className="mb-3">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="orange_noResponseWithin"
                    name="setting_id"
                    value="1"
                    onChange={this.handleOnChangeSettingId}
                    label="Anything not responded to within:"
                    checked={
                      settingsList.length > 0 &&
                      settingsList[1].setting_id === 1
                    }
                  />
                </div> */}
                <div>

                <p className="ff-base">
                <span>
                Anything not responded to within:

                </span>
				</p>
                    <Row form>
                      <Col xs="auto">
                        <FormGroup>
                        <div className="d-flex flex-nowrap align-items-center">  
                          <Input
                            bsSize="lg"
                            type="select"
                            name="hours"
                            className="mr-3"
                            onChange={this.handleOnChangeSettingHours}
                            id="orange_noResponseWithin_hours1"
                            value={
                              settingsList.length > 0 &&
                              settingsList[1].setting_id === 1 &&
                              settingsList[1].hours
                                ? settingsList[1].hours
                                : 0 
                            }
                          >
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                          </Input>

                          <Label className="text-primary-dark font-weight-normal mb-0">
                            Hours
                          </Label>
                          
                        </div>
                        </FormGroup>
                      </Col>
                      <Col xs="auto">
                        <FormGroup>
                        <div className="d-flex flex-nowrap align-items-center px-4">
                          <Input
                            bsSize="lg"
                            type="select"
                            name="minutes"
                            className="mr-3"
                            onChange={this.handleOnChangeSettingMinutes}
                            id="orange_noResponseWithin_minutes1"
                            value={
                              settingsList.length > 0 &&
                              settingsList[1].setting_id === 1 &&
                              settingsList[1].minutes
                                ? settingsList[1].minutes
                                : 0
                            }
                          >
                            <option value="0">0</option>
                            <option value="5">05</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                            <option value="35">35</option>
                            <option value="40">40</option>
                            <option value="45">45</option>
                            <option value="50">50</option>
                            <option value="55">55</option>
                          </Input>

                          <Label className="text-primary-dark font-weight-normal mb-0">
                            Minutes
                          </Label>
                          
                        </div>
                        </FormGroup>
                      </Col>
                    </Row>
                  <p className="text-primary-dark fs-14 ff-base">
                <span>
                All Orange alerts are pending tasks that still need to be completed within the desired timeframe.

                </span>
              </p>
                </div>
              </div>

              {/* <div className="mb-4">
                <div className="mb-3">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="orange_hrsBeforeClosing"
                    name="setting_id"
                    label="The number of hours before close of business:"
                    onChange={this.handleOnChangeSettingId}
                    value="2"
                    checked={
                      settingsList.length > 0 &&
                      settingsList[1].setting_id === 2
                    }
                  />
                </div>
                <div>
                    <Row form>
                      <Col xs="auto">
                        <FormGroup>
                        <div className="d-flex flex-nowrap align-items-center px-4">
                          <Input
                            bsSize="lg"
                            type="select"
                            name="hours"
                            className="mr-3"
                            onChange={this.handleOnChangeSettingHours}
                            id="orange_hrsBeforeClosing_hours2"
                            value={
                              settings_list.length > 0 &&
                              settings_list[1].setting_id === 2 &&
                              settings_list[1].hours
                                ? settings_list[1].hours
                                : 0
                            }
                          >
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                          </Input>

                          <Label className="text-primary-dark font-weight-normal mb-0">
                            Hours
                          </Label>
                        </div>
                        </FormGroup>
                      </Col>
                      <Col xs="auto">
                        <FormGroup>
                        <div className="d-flex flex-nowrap align-items-center px-4">
                          <Input
                            bsSize="lg"
                            type="select"
                            name="minutes"
                            className="mr-3"
                            onChange={this.handleOnChangeSettingMinutes}
                            id="orange_hrsBeforeClosing_minutes2"
                            value={
                              settings_list.length > 0 &&
                              settings_list[1].setting_id === 2 &&
                              settings_list[1].minutes
                                ? settings_list[1].minutes
                                : 0
                            }
                          >
                            <option value="0">0</option>
                            <option value="05">05</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                            <option value="35">35</option>
                            <option value="40">40</option>
                            <option value="45">45</option>
                            <option value="50">50</option>
                            <option value="55">55</option>
                          </Input>

                          <Label className="text-primary-dark font-weight-normal mb-0">
                            Minutes
                          </Label>
                        </div>
                        </FormGroup>
                      </Col>
                    </Row>
                </div>
              </div> */}

              {/* <div className="mb-4">
                <div className="mb-3">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="orange_noRespBusHrs"
                    name="setting_id"
                    label="Anything not responded to during business hours:"
                    value="3"
                    onChange={this.handleOnChangeSettingId}
                    checked={
                      settingsList.length > 0 &&
                      settingsList[1].setting_id === 3
                    }
                  />
                </div>
              </div> */}
            </Col>
          </Row>

          <hr className="bg-secondary" />
        </div>

        <div>
          <Row form>
            <Col xs="auto">
              <div className="h-100 bg-success" style={{ width: '10px' }}>&nbsp;</div>
            </Col>
            <Col>
              <h5 className="text-success mb-3 ff-base">Green Alert</h5>
              <p className="text-primary-dark fs-14 ff-base">
                <span>
                All Green alerts are completed tasks.
                </span>
              </p>
            </Col>
          </Row>

          <hr className="bg-secondary" />
        </div>

        <div className="text-right">
          <p style={{ color: "red" }} hidden={this.state.error === ""}>
            {this.state.error}
          </p>
          <Button
            color="light"
            className="mw"
            onClick={() => this.handleOnCancel()}
          >
            Cancel
          </Button>
          <Button
            color="primary"
            className="mw"
            onClick={() => this.handleSubmit()}
          >
            Save
          </Button>
        </div>
      </div>
    );
  }
}
const mapState = (state) => ({
  settings_list: state.admin.settings_list,
});
const mapDispatch = {
  get_settings_list,
  update_settings,
};

export default connect(mapState, mapDispatch)(Settings);