export const roleOptions = [
  {
    id: "upload_remove_logo",
    intID: 2,
    name: "Upload/Remove Logo for Assigned Business",
  },
  {
    id: "upload_remove_images",
    intID: 3,
    name: "upload/Remove Image for Assigned Business",
  },
  {
    id: "upload_remove_videos",
    intID: 4,
    name: "upload/Remove Video for Assigned Business",
  },
  {
    id: "modify_listing_owner_guide",
    intID: 6,
    name: "Modify listing Owner Guide",
  },
  {
    id: "modify_core_lising_info",
    intID: 8,
    name: "Modify Core Listing info including hours etc.",
  },
  {
    id: "change_additional_info",
    intID: 7,
    name: "Mhange additional Info Section",
  },
  {
    id: "change_about_business",
    intID: 9,
    name:
      "Change about this business:specialities,history,meet the business owner",
  },
  {
    id: "modify_menu_item",
    intID: 10,
    name: "Modify Menu items and Prices",
  },
  {
    id: "other_offers",
    intID: 11,
    name: "Other offer",
  },
  {
    id: "add_remove_employee",
    intID: 12,
    name: "Add/Remove Employees",
  },
  {
    id: "create_post",
    intID: 14,
    name: "Create Posts",
  },
  {
    id: "respond_reviews",
    intID: 15,
    name: "Respond to Reviews",
  },
  {
    id: "respond_disputed_reviews",
    intID: 16,
    name: "Respond to Disputed Reviews",
  },
  {
    id: "respond_message",
    intID: 17,
    name: "Respond to Message",
  },
  {
    id: "respond_q&a",
    intID: 18,
    name: "Respond to Q&A",
  },
  {
    id: "respond_feedback",
    intID: 19,
    name: "Responding to Feedback",
  },
  {
    id: "call_to_action",
    intID: 20,
    name: "Create/Delete Call To Action",
  },
];
