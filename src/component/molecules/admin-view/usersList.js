import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
  Row,
  Col,
  FormGroup,
  Input,
  Button,
  Badge,
  InputGroup,
  InputGroupAddon,
  Label,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import {
  get_people_list,
  get_people_data,
  search_people,
  get_roles_list,
  get_countries_list,
  get_branches_list,
  get_states_list,
  change_active_status,
  edit_user,
} from "../../../actions/admin-view/admin";
import { current_user_profile } from "../../../actions/user";

class UsersList extends Component {
  constructor(props) {
    super(props);

    this.editFormRef = React.createRef();
    this.wrapperRef = React.createRef();
    this.wrapperRef2 = React.createRef();

    this.state = {
      editFormVisible: false,
      confirmDeleteModal: false,
      searchString: "",
      people_data: {},
      selectedBranches: [],
      selectedCsrBackups: [],
      dropdownBranchesVisible: false,
      dropdownBackupsVisible: false,
      branchesList: [],
      peopleList: {},
      country: "",
      state: "",
      branchText: ""
    };
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.people_data &&
      Object.keys(nextProps.people_data).length > 0
    ) {
      let selectedCsrBackups = [];
      if (
        nextProps.people_list &&
        nextProps.people_list.result.people_list.length > 0 &&
        Object.keys(nextProps.people_list).length > 0 &&
        nextProps.people_data.result.details[0].csrbackup_count > 0
      ) {
        selectedCsrBackups = nextProps.people_list.result.people_list.filter(
          (people) => {
            return nextProps.people_data.result.details[0].csr_backup.includes(
              people.csr_id.toString()
            );
          }
        );
      }

      let selectedBranches = [];
      if (
        nextProps.people_data.result.details[0].branch_count > 0 &&
        nextProps.branches_list &&
        nextProps.branches_list.length > 0
      ) {
        this.setState({
          state: nextProps.people_data[0]?.state,
          country: nextProps.people_data[0]?.country,
        });
        selectedBranches = nextProps.branches_list.filter((branch) => {
          return nextProps.people_data.result.details[0].branches.includes(
            branch.admin_entries_id.toString()
          );
        });
      }
      this.setState({
        ...this.state,
        selectedCsrBackups: selectedCsrBackups,
        selectedBranches: selectedBranches,
        people_data: nextProps.people_data,
        peopleList: nextProps.people_list,
        branchesList: nextProps.branches_list,
      });
    }
  }
  componentDidMount() {
    this.props.get_people_list();
    this.props.current_user_profile();
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  confirmDeleteModalToggle = () => {
    this.setState({ confirmDeleteModal: !this.state.confirmDeleteModal });
  };

  handleClickOutside = (event) => {
    let { dropdownBranchesVisible, dropdownBackupsVisible } = this.state;
    if (
      this.wrapperRef.current &&
      this.wrapperRef.current.contains(event.target)
    ) {
      if (dropdownBranchesVisible !== true) {
        this.setState({ dropdownBranchesVisible: true });
      }
    } else {
      if (dropdownBranchesVisible !== false) {
        this.setState({ dropdownBranchesVisible: false });
      }
    }

    if (
      this.wrapperRef2.current &&
      this.wrapperRef2.current.contains(event.target)
    ) {
      if (dropdownBackupsVisible !== true) {
        this.setState({ dropdownBackupsVisible: true });
      }
    } else {
      if (dropdownBackupsVisible !== false) {
        this.setState({ dropdownBackupsVisible: false });
      }
    }
  };

  handleOnDeleteConfirmation = () => {
    if (
      this.state.people_data &&
      this.state.people_data.result.details[0].csr_id
    ) {
      this.props.change_active_status(
        this.state.people_data.result.details[0].csr_id,
        1
      );
      this.setState({
        editFormVisible: false,
        searchString: "",
        people_data: {},
        selectedBranches: [],
        selectedCsrBackups: [],
        dropdownBranchesVisible: false,
        dropdownBackupsVisible: false,
        branchesList: [],
        peopleList: {},
        country: "",
        state: "",
        confirmDeleteModal: false
      });
    }
  };
  handleOnChangeBackups = (e) => {
    let peopleList = { ...this.props.people_list };

    let value = (e.target.value).toLowerCase();

    if (value) {
      let newPeopleList = this.props.people_list.result.people_list.filter(
        (people) => {
          let firstName = (people.first_name).toLowerCase();
          let lastName = (people.last_name).toLowerCase();

          if (
            firstName.includes(value) ||
            lastName.includes(value)
          ) {
            return true;
          } else {
            return false;
          }
        }
      );
      this.setState({
        ...this.state,
        peopleList: {
          ...this.state.peopleList,
          result: {
            ...this.state.peopleList.result,
            people_list: newPeopleList,
          },
        },
      });
    } else {
      this.setState({
        ...this.state,
        peopleList: { ...this.props.people_list },
      });
    }
  };
  handleOnChangeCountryState = (e) => {
    if (e.target.name === "country") {
      this.props.get_states_list(e.target.value);
    }
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        this.filterBranchViaLocation();
      }
    );
  };
  filterBranchViaLocation = () => {
    let branchesList = [...this.props.branches_list];
    if (this.state.country !== "") {
      if (this.state.state === "") {
        branchesList = this.props.branches_list.filter((branch) => {
          return branch.country === this.state.country;
        });
      } else {
        branchesList = this.props.branches_list.filter((branch) => {
          return (
            branch.country === this.state.country &&
            branch.state === this.state.state
          );
        });
      }
    } else if (this.state.country === "") {
      branchesList = [];
    }
    this.setState({
      ...this.state,
      branchesList: branchesList,
    });
  };
  handleOnChangeBranches = (e) => {
    let branchesList = [...this.props.branches_list];
    let value = (e.target.value).toLowerCase();
    if (value) {
      branchesList = this.props.branches_list.filter((branch) => {
        let branchString = (`${branch.name} - ${branch.address1}, ${branch.state}, ${branch.city}, ${branch.country}, ${branch.zipcode}`).toLowerCase();
        if (branchString.includes(value)) {
          return true;
        } else {
          return false;
        }
      });
      this.setState({
        ...this.state,
        branchesList: branchesList,
        branchText: e.target.value
      });
    } else {
      this.setState({
        ...this.state,
        branchesList: [...branchesList],
        branchText: e.target.value
      });
    }
  };

  handleOnClickEditUser = (id) => {
    this.props.get_people_data(id);
    this.props.get_countries_list();
    this.props.get_branches_list();
    this.setState(
      {
        ...this.state,
        editFormVisible: true,
      },
      () => {
        window.scrollTo(0, this.editFormRef.current.offsetTop + 360);
      }
    );
  };
  handleOnChangeEdit = (e) => {
    let newPeople = {
      ...this.state.people_data,
    };
    newPeople.result.details[0][e.target.name] = e.target.value;
    this.setState({
      ...this.state,
      people_data: newPeople,
    });
  };
  handleSubmitEdit = () => {
    let data = {
      role: this.state.people_data.result.details[0].role,
      first_name: this.state.people_data.result.details[0].first_name,
      last_name: this.state.people_data.result.details[0].last_name,
      country: this.state.people_data?.result?.details[0]?.country !== null ?
        this.state.people_data.result.details[0].country :
        this.state.country ? this.state.country : "",
      state: this.state.people_data?.result?.details[0]?.state !== null ?
        this.state.people_data.result.details[0].state :
        this.state.state ? this.state.state : "",
      backups: this.state.selectedCsrBackups
        .map((people) => people.csr_id)
        .join(","),
      branches: this.state.selectedBranches
        .map((branch) => branch.id)
        .join(","),
    };
    if (data.role !== "" && data.first_name !== "" && data.last_name !== "") {
      this.props.edit_user(
        this.state.people_data.result.details[0].csr_id,
        data
      );
      this.setState({
        editFormVisible: false,
        searchString: "",
        people_data: {},
        selectedBranches: [],
        selectedCsrBackups: [],
        dropdownBranchesVisible: false,
        dropdownBackupsVisible: false,
        branchesList: [],
        peopleList: {},
        country: "",
        state: "",
        branchText: "",
      });
    }
  };

  toggleDropdownVisibility = (value, type) => {
    if (type === "branches") {
      this.setState({
        dropdownBranchesVisible: value,
      }, () => console.log({ branchesList: this.state.branchesList }));
    } else if (type === "backups") {
      this.setState({
        dropdownBackupsVisible: value,
      }, () => console.log({ branchesList: this.state.branchesList }));
    }
  };

  removeFromSelected = (item, type) => {
    let newItems = [];
    if (type === "branch") {
      newItems = this.state.selectedBranches.filter(
        (branch) => branch.admin_entries_id !== item.admin_entries_id
      );
      this.setState({
        ...this.state,
        selectedBranches: newItems,
      });
    } else if (type === "backup") {
      newItems = this.state.selectedCsrBackups.filter(
        (people) => people.csr_id !== item.csr_id
      );
      this.setState({
        ...this.state,
        selectedCsrBackups: newItems,
      });
    }
  };
  handleSelectFromDropdown = (item, type) => {
    let newItems = [];
    let index;
    if (type === "branch") {
      index = this.state.selectedBranches
        .map((branch) => branch.admin_entries_id)
        .includes(item.admin_entries_id);

      if (index) {
        newItems = this.state.selectedBranches.filter(
          (branch) => branch.admin_entries_id !== item.admin_entries_id
        );
      } else {
        newItems = [...this.state.selectedBranches, item];
      }

      this.setState({
        ...this.state,
        selectedBranches: newItems,
      });
    } else if (type === "backup") {
      index = this.state.selectedCsrBackups
        .map((people) => people.csr_id)
        .includes(item.csr_id);

      if (index) {
        newItems = this.state.selectedCsrBackups.filter(
          (people) => people.csr_id !== item.csr_id
        );
      } else {
        newItems = [...this.state.selectedCsrBackups, item];
      }

      this.setState({
        ...this.state,
        selectedCsrBackups: newItems,
      });
    }
  };
  // handleOnChangeCountry = (e) => {
  //   this.props.get_states_list(e.target.value);
  // };

  handleOnChangeSearch = (e) => {
    this.props.search_people(e.target.value);
    this.setState({
      ...this.state,
      searchString: e.target.value,
    });
  };

  handleOnClickSearchSubmit = (e) => {
    e.preventDefault();
    this.props.search_people(this.state.searchString);
  };

  handleOnClickChangeActiveStatus = (type) => (e) => {
    e.preventDefault();
    this.props.change_active_status(
      this.state.people_data.result.details[0].csr_id,
      type
    );
    this.setState({
      editFormVisible: false,
      searchString: "",
      people_data: {},
      selectedBranches: [],
      selectedCsrBackups: [],
      dropdownBranchesVisible: false,
      dropdownBackupsVisible: false,
      branchesList: [],
      peopleList: {},
      country: "",
      state: "",
    });
  };
  render() {
    let {
      editFormVisible,
      searchString,
      selectedBranches,
      selectedCsrBackups,
      peopleList,
      branchesList,
      profileData,
    } = this.state;
    const {
      people_list,
      profile_data,
      people_data,
      roles,
      countries_list,
      branches_list,
      states_list,
    } = this.props;

    if (this.state.country !== "" && this.state.state == "") {
      if (branchesList && Array.isArray(branchesList) && branchesList.length) {
        branchesList = branchesList.filter(item => item.country == this.state.country);
      }

      if (selectedBranches && Array.isArray(selectedBranches) && selectedBranches.length) {
        selectedBranches = selectedBranches.filter(item => item.country == this.state.country);
      }
    }


    if (this.state.country !== "" && this.state.state !== "") {
      if (branchesList && Array.isArray(branchesList) && branchesList.length) {
        branchesList = branchesList.filter(item => (item?.country == this.state.country && item?.state_fullname == this.state.state));
      }

      if (selectedBranches && Array.isArray(selectedBranches) && selectedBranches.length) {
        selectedBranches = selectedBranches.filter(item => (item?.country == this.state.country && item?.state_fullname == this.state.state));
      }
    }

    return (
      <>
        <div className="bg-white p-3 mb-3">
          <div className="d-flex flex-wrap flex-column flex-sm-row align-items-center mx-n2 mb-4">
            <h3 className="text-secondary px-2 flex-grow-1 mb-2">
              Existing People(
              <span>
                {people_list &&
                  people_list.result &&
                  (people_list.result.csr_count
                    ? people_list.result.csr_count
                    : 0) + 1}
              </span>
              )
            </h3>
            <div className="px-2">
              <InputGroup>
                <Input
                  bsSize="sm"
                  type="search"
                  name="name"
                  value={searchString}
                  onChange={this.handleOnChangeSearch}
                  placeholder="Type a name to filter"
                />
                <InputGroupAddon addonType="append">
                  <Button
                    className="py-0"
                    onClick={this.handleOnClickSearchSubmit}
                  >
                    <FontAwesomeIcon icon="search" />{" "}
                  </Button>
                </InputGroupAddon>
              </InputGroup>
            </div>
          </div>

          <div>
            <div className="mb-3">
              <span className="text-dark font-weight-bold">Admin (1)</span>
            </div>

            {/* Loop through admins */}
            <div className="mb-3">
              <Row className="align-items-center">
                <Col xs={6} lg={5}>
                  <div className="d-flex align-items-center">
                    <div className="mr-3">
                      <img
                        className="img-circle _70x70"
                        width="70"
                        height="70"
                        src={
                          profile_data && profile_data.current_profile_file
                            ? profile_data.current_profile_file
                            : require("../../../assets/images/user-admin.jpg")
                        }
                        alt="Admin Image"
                      />
                    </div>
                    <div>
                      <span className="font-weight-bold text-dark d-block">
                        {`${profile_data && profile_data.user
                          ? profile_data.user.first_name
                            .charAt(0)
                            .toUpperCase() +
                          profile_data.user.first_name.slice(1)
                          : "Admin"
                          } ${profile_data && profile_data.user
                            ? profile_data.user.last_name
                              .charAt(0)
                              .toUpperCase() +
                            profile_data.user.last_name.slice(1)
                            : "User"
                          }`}
                      </span>
                    </div>
                  </div>
                </Col>
                <Col xs={6} lg={4}>
                  <div className="text-uppercase text-right text-lg-left text-secondary-dark mb-0 fs-14 wb-break-word">
                    {profile_data && profile_data.professional[0]
                      ? profile_data.professional[0].profession.title
                      : "Admin"}
                  </div>
                </Col>
                {/* <Col lg={3} className="mt-3 mt-lg-0">&nbsp;</Col> */}
              </Row>
              <hr className="bg-secondary" />
            </div>
          </div>

          <div>
            <div className="mb-3">
              <span className="text-dark font-weight-bold">
                People(
                {people_list &&
                  people_list.result &&
                  people_list.result.csr_count
                  ? people_list.result.csr_count
                  : 0}
                )
              </span>
            </div>

            {/* Loop through people */}
            {people_list &&
              people_list.result &&
              people_list.result.people_list.length > 0 &&
              people_list.result.people_list.map((people) => {
                return (
                  <div className="mb-3" key={people.csr_id}>
                    <Row className="align-items-center">
                      <Col xs={6} lg={5}>
                        <div className="d-flex align-items-center step-12">
                          <div className="mr-3">
                            <img
                              className="img-circle _70x70"
                              width="70"
                              height="70"
                              src={
                                people.current_profile_pic_id
                                  ? people.current_profile_pic_id.url
                                  : require("../../../assets/images/user-circle.png")
                              }
                              alt=""
                            />
                          </div>
                          <div>
                            <span className="font-weight-bold text-dark d-block">
                              {`${people.first_name.charAt(0).toUpperCase() +
                                people.first_name.slice(1)
                                } ${people.last_name.charAt(0).toUpperCase() +
                                people.last_name.slice(1)
                                }`}
                            </span>
                            {people.branches && people.branches.length > 0 ? (
                              <div>
                                <UncontrolledDropdown>
                                  <DropdownToggle
                                    color="transparent"
                                    size="sm"
                                    caret
                                  >
                                    <span className="font-weight-bold text-primary fs-12 mr-2">
                                    {people.branches
									  ? people.branches.length
									  : 0} Branches
                                    </span>
                                  </DropdownToggle>
                                  <DropdownMenu
                                    className="dropdown-list"
                                    right
                                    style={{
                                      maxHeight: "200px",
                                      overflowY: "auto",
                                    }}
                                  >
                                    {people.branches.map((branch, index) => (
                                      <div
                                        className="dropdown-item"
                                        key={index}
                                      >
                                        {branch}
                                      </div>
                                    ))}
                                  </DropdownMenu>
                                </UncontrolledDropdown>
                              </div>
                            ) : (
                                <span className="font-weight-bold text-primary d-block fs-12 actionable">
                                  No Branches
                                </span>
                              )}
                            {people.csrbackup_count > 0 ? (
                              <div>
                                <UncontrolledDropdown>
                                  <DropdownToggle
                                    color="transparent"
                                    size="sm"
                                    caret
                                  >
                                    <span className="font-weight-bold text-primary fs-12 mr-2">
                                      CSR Backup {people.csr_backup
									  ? people.csr_backup.length
									  : 0} CSR
                                    </span>
                                  </DropdownToggle>
                                  <DropdownMenu
                                    className="dropdown-list"
                                    right
                                    style={{
                                      maxHeight: "200px",
                                      overflowY: "auto",
                                    }}
                                  >
                                    {people.csr_backup.map((item, index) => (
                                      <div key={index} className="dropdown-item">
										  <input type="radio" name="CSR" value={item}/> {item}
                                      </div>
                                    ))}
                                  </DropdownMenu>
                                </UncontrolledDropdown>
                              </div>
                            ) : (
                                <span className="font-weight-bold text-primary d-block fs-12 actionable">
                                  No Backup
                                </span>
                              )}
                          </div>
                        </div>
                      </Col>
                      <Col xs={6} lg={5}>
                        <Row>
                          <Col xs={12} lg={6}>
                            <div className="text-uppercase text-center text-lg-left text-secondary-dark fs-14">
                              {people.role}
                            </div>
                          </Col>
                          <Col xs={12} lg={6}>
                            <div className="font-weight-bold text-center text-lg-left text-warning fs-14">
                              {people.status === "active" ? "Active" : "Disabled"}
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={2} className="mt-3 mt-lg-0">
                        <Button
                          color="primary"
                          block
                          onClick={() => {
                            this.props.get_states_list(people?.country ? people?.country : "United States")
                            this.handleOnClickEditUser(people.csr_id);
                          }}
                          className="step-13"
                        >
                          Edit
                        </Button>
                      </Col>
                    </Row>
                    <hr className="bg-secondary" />
                  </div>
                );
              })}
          </div>
        </div>

        {editFormVisible && (
          <div className="p-3 bg-white step-14">
            <div
              className="bg-body border border-secondary p-3 mb-3"
              ref={this.editFormRef}
            >
              <h3 className="text-secondary mb-4">Editing User</h3>
              <div className="">
                <Row form>
                  <Col lg={4}>
                    <FormGroup>
                      <Input
                        type="text"
                        name="first_name"
                        value={
                          this.state.people_data.result &&
                            this.state.people_data.result.details[0].first_name
                            ? this.state.people_data.result.details[0].first_name
                            : ""
                        }
                        onChange={this.handleOnChangeEdit}
                        placeholder="First Name"
                      />
                    </FormGroup>
                  </Col>
                  <Col lg={4}>
                    <FormGroup>
                      <Input
                        type="text"
                        name="last_name"
                        onChange={this.handleOnChangeEdit}
                        value={
                          this.state.people_data.result &&
                            this.state.people_data.result.details[0].last_name
                            ? this.state.people_data.result.details[0].last_name
                            : ""
                        }
                        placeholder="Last Name"
                      />
                    </FormGroup>
                  </Col>
                  <Col lg={4}>
                    <FormGroup>
                      {/* <Label for="editUserRole">Select Role</Label> */}
                      <Input
                        type="select"
                        name="role"
                        id="editUserRole"
                        onChange={this.handleOnChangeEdit}
                      >
                        {roles.map((role) => (
                          <option
                            value={role.id}
                            key={role.id}
                            selected={
                              people_data && people_data.result.details[0]
                                ? people_data.result.details[0].role == role.id
                                  ? true
                                  : false
                                : false
                            }
                          >
                            {role.name.toUpperCase()}
                          </option>
                        ))}
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <Row form>
                  <Col md={6}>
                    <FormGroup>
                      {/* <Label for="editUserCountry">Select Country</Label> */}
                      <Input
                        type="select"
                        name="country"
                        onChange={
                          this.state.people_data.result &&
                            this.state.people_data.result.details[0].country
                            ? this.handleOnChangeEdit
                            : this.handleOnChangeCountryState}
                        id="editUserCountry"
                        // value={this.state.country}
                        value={
                          this.state.people_data.result &&
                            this.state.people_data.result.details[0].country
                            ? this.state.people_data?.result?.details[0]?.country
                            : this.state.country ? this.state.country : ""
                        }
                      >
                        <option value="">Select Country</option>
                        {countries_list.map((country, index) => (
                          <option value={country.country} key={index}>
                            {country.country}
                          </option>
                        ))}
                      </Input>

                    </FormGroup>
                  </Col>
                  <Col md={6}>
                    <FormGroup>
                      {/* <Label for="editUserState">Select State</Label> */}
                      <Input
                        type="select"
                        name="state"
                        // onChange={this.handleOnChangeCountryState}
                        onChange={
                          this.state.people_data.result &&
                            this.state.people_data?.result?.details[0]?.state
                            ? this.handleOnChangeEdit
                            : this.handleOnChangeCountryState}

                        value={this.state.state}
                        value={
                          this.state.people_data.result &&
                            this.state.people_data.result.details[0].state
                            ? this.state.people_data.result.details[0].state
                            : this.state.state ? this.state.state : ""
                        }
                        id="editUserState"
                      >
                        <option value="">Select State</option>
                        { states_list && Array.isArray(states_list) &&
                          states_list.length > 0 &&
                          states_list.map((state) => {
                            return (
                              <option value={state.state}>{`${state.state.charAt(0).toUpperCase() +
                                state.state.slice(1)
                                }`}</option>
                            );
                          })}
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <Row form>
                  <Col xs={12} className="mt-5">
                    <FormGroup row>
                      <Col sm={2}>
                        <Label className="text-dark">Branches</Label>
                      </Col>
                      <Col sm={10}>
                        <Input
                          type="text"
                          name="user_branches"
                          value={this.state.branchText}
                          onChange={this.handleOnChangeBranches}
                          placeholder="Type a branch name to assign it"
                          onFocus={() => this.toggleDropdownVisibility(true, "branches")}
                        />
                        <div className="mt-2" style={{ maxHeight: '200px', overflowY: 'auto' }}>
                          <div>
                            {selectedBranches.length > 0 &&
                              selectedBranches.map((branch) => {
                                return (
                                  <span className="d-block mb-2 w-100 fs-18">
                                    <Badge
                                      color="primary"
                                      className="d-flex align-items-center"
                                    >
                                      <span className="mr-1 font-weight-normal text-truncate">
                                        {`${branch.name} - ${branch.address1}, ${branch.state}, ${branch.city}, ${branch.country}, ${branch.zipcode}`}
                                      </span>
                                      <button
                                        type="button"
                                        className="close btn-sm ml-auto"
                                        aria-label="Close"
                                        onClick={() =>
                                          this.removeFromSelected(
                                            branch,
                                            "branch"
                                          )
                                        }
                                      >
                                        <span>
                                          <FontAwesomeIcon icon="times-circle" />{" "}
                                        </span>
                                      </button>
                                    </Badge>
                                  </span>
                                );
                              })}
                          </div>
                        </div>
                        <div className="position-relative mt-2" ref={this.wrapperRef}
                          hidden={!this.state.dropdownBranchesVisible}>
                          <hr />
                          <div
                            className="w-100"
                            style={{ zIndex: 9 }}
                          >
                            <ul
                              className="list-unstyled bg-white border shadow-sm"
                              style={{ maxHeight: "200px", overflowY: "auto" }}
                            >
                              {branchesList && Array.isArray(branchesList) && branchesList.length > 0 ? (
                                branchesList.map((branch) => {
                                  return (
                                    <li
                                      className={`selectable-item ${selectedBranches
                                        .map((item) => item.admin_entries_id)
                                        .includes(branch.admin_entries_id)
                                        ? "active"
                                        : ""
                                        }`}
                                      onClick={() =>
                                        this.handleSelectFromDropdown(
                                          branch,
                                          "branch"
                                        )
                                      }
                                    >{`${branch.name} - ${branch.address1}, ${branch.state}, ${branch.city}, ${branch.country}, ${branch.zipcode}`}</li>
                                  );
                                })
                              ) : (<li>{'No results found'} </li>)}
                            </ul>
                          </div>
                        </div>

                      </Col>
                    </FormGroup>
                  </Col>
                  <Col xs={12} className="mt-3">
                    <FormGroup row>
                      <Col sm={2}>
                        <Label className="text-dark">Backup</Label>
                      </Col>
                      <Col sm={10}>
                        <Input
                          type="text"
                          name="user_backup"
                          placeholder="Type a CSR name to assign it"
                          onChange={this.handleOnChangeBackups}
                          onFocus={() => this.toggleDropdownVisibility(true, "backups")}
                        />
                        <div className="mt-2">
                          <div className="d-flex flex-wrap mx-n1">
                            {selectedCsrBackups.length > 0 &&
                              selectedCsrBackups.map((people) => {
                                return (
                                  <span className="px-1 mb-2 mw-100 fs-18">
                                    <Badge
                                      color="dark"
                                      className="d-flex align-items-center"
                                    >
                                      <span className="mr-2 font-weight-normal text-truncate">
                                        {`${people.first_name
                                          .charAt(0)
                                          .toUpperCase() +
                                          people.first_name.slice(1)
                                          } ${people.last_name
                                            .charAt(0)
                                            .toUpperCase() +
                                          people.last_name.slice(1)
                                          }`}
                                      </span>
                                      <button
                                        type="button"
                                        className="close btn-sm"
                                        aria-label="Close"
                                        onClick={() =>
                                          this.removeFromSelected(
                                            people,
                                            "backup"
                                          )
                                        }
                                      >
                                        <span>
                                          <FontAwesomeIcon
                                            icon="times"
                                            color="white"
                                          />{" "}
                                        </span>
                                      </button>
                                    </Badge>
                                  </span>
                                );
                              })}
                          </div>
                        </div>
                        <div className="position-relative mt-2" ref={this.wrapperRef2}
                          hidden={!this.state.dropdownBackupsVisible}>
                          <hr />
                          <div
                            className="w-100 mb-3"
                            style={{ zIndex: 9 }}
                          >
                            <ul
                              className="list-unstyled bg-white border shadow-sm"
                              style={{ maxHeight: "200px", overflowY: "auto" }}
                            >
                              {peopleList &&
                                peopleList.result &&
                                peopleList.result.people_list.length > 0 ? (
                                  peopleList.result.people_list.map(
                                    (people, index) => {
                                      if (this.state.people_data.result &&
                                        this.state.people_data.result.details[0].csr_id !== people.csr_id) {
                                        return (
                                          <li
                                            className={`selectable-item ${selectedCsrBackups
                                              .map((item) => item.csr_id)
                                              .includes(people.csr_id)
                                              ? "active"
                                              : ""
                                              }`}
                                            onClick={() =>
                                              this.handleSelectFromDropdown(
                                                people,
                                                "backup"
                                              )
                                            }
                                            key={index}
                                          >
                                            {`${people.first_name
                                              .charAt(0)
                                              .toUpperCase() +
                                              people.first_name.slice(1)
                                              } ${people.last_name
                                                .charAt(0)
                                                .toUpperCase() +
                                              people.last_name.slice(1)
                                              }`}
                                          </li>
                                        )
                                      };
                                    }
                                  )
                                ) : (
                                  <li>No results found</li>
                                )}
                            </ul>
                          </div>
                        </div>

                      </Col>
                    </FormGroup>
                  </Col>
                </Row>
                <Row form className="mt-3">
                  <Col xs={6} md={3}>
                    <Button
                      color="link"
                      block
                      className="text-danger font-weight-bold text-decoration-none my-2"
                      onClick={this.confirmDeleteModalToggle}
                    >
                      Delete
                  </Button>
                  </Col>
                  <Col xs={6} md={3}>
                    <Button
                      color="link"
                      block
                      className="text-warning font-weight-bold text-decoration-none my-2"
                      onClick={this.handleOnClickChangeActiveStatus(
                        people_data &&
                          people_data.result.details[0].status === "active"
                          ? 2
                          : 3
                      )}
                    >
                      {people_data &&
                        people_data.result.details[0].status === "active"
                        ? "Disable"
                        : "Activate"}
                    </Button>
                  </Col>
                  <Col xs={6} md={3}>
                    <Button
                      color="light"
                      className="my-2"
                      block
                      onClick={() => {
                        this.setState({
                          ...this.state,
                          editFormVisible: false,
                          branchText: "",
                          searchString: "",
                          people_data: {},
                          selectedBranches: [],
                          selectedCsrBackups: [],
                          dropdownBranchesVisible: false,
                          dropdownBackupsVisible: false,
                          branchesList: [],
                          peopleList: {},
                          country: "",
                          state: "",
                        });
                      }}
                    >
                      Cancel
                  </Button>
                  </Col>
                  <Col xs={6} md={3}>
                    <Button
                      color="primary"
                      className="my-2"
                      block
                      onClick={() => this.handleSubmitEdit()}
                    >
                      Save
                  </Button>
                  </Col>
                </Row>
              </div>
            </div>
          </div>
        )}

        {/* Delete Confirmation Modal */}
        <Modal
          isOpen={this.state.confirmDeleteModal}
          toggle={this.confirmDeleteModalToggle}
          className="text-center"
        >
          <div className="bg-white modal-header">
            <h5 className="modal-title mx-auto text-dark">Confirmation</h5>
            <button
              className="btn btn-sm"
              aria-label="Close"
              onClick={this.confirmDeleteModalToggle}
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <ModalBody className="text-dark">
            Are you sure you want to delete?
          </ModalBody>
          <ModalFooter className="bg-white">
            <div className="text-center w-100">
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.confirmDeleteModalToggle}
              >
                Cancel
              </Button>
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.handleOnDeleteConfirmation}
              >
                Ok
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}

const mapState = (state) => {
  return {
    people_list: state.admin.people_list,
    people_data: state.admin.people_data,
    profile_data: state.user.current_user,
    countries_list: state.admin.countries_list,
    states_list: state.admin.states_list,
    roles: state.admin?.roles_list?.result,
    branches_list: state.admin.branches_list,
    current_role_and_permissions: state.user.current_role_and_permissions,
  };
};

const mapDispatch = {
  get_people_list,
  get_people_data,
  search_people,
  get_roles_list,
  get_countries_list,
  get_branches_list,
  get_states_list,
  change_active_status,
  edit_user,
  current_user_profile,
};

export default withRouter(connect(mapState, mapDispatch)(UsersList));
