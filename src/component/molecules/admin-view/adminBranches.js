import React, { Component } from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Row,
  Col,
  FormGroup,
  FormFeedback,
  Input,
  Button,
  InputGroup,
  InputGroupAddon,
} from "reactstrap";

import {
  getBranch,
  getStates,
  updateBranch,
  deleteBranch,
  addBranch,
  get_branches_list,
  get_states_list,
} from "../../../actions/admin-view/admin";

const initialState = {
  errors: {},
};

class AdminBranches extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState,
      branches_data: [],
      branches_count: 0,
      branches: [],
      adminEntriesId: '',
      branch_data: "",
      editBranchView: false,
      branch_id: "",
      branch_name: "",
      branch_address: "",
      branch_city: "",
      branch_state: "",
      branch_country: "",
      branch_zipcode: "",
      branch_states: [],
      isDeleteToggle: false,
      id: "",
      current_page: 1,
      current_branches: [],
      search_name: "",
      setattr: false,
      isCanada: false,
    };
  }

  componentDidMount() {
    this.props.get_branches_list();
  }

  componentWillReceiveProps(nextProps) {
    const { current_page } = this.state;
    const { branches_data, branch_states } = nextProps;
    if (
      branches_data &&
      Array.isArray(branches_data) &&
      branches_data.length > 0
    ) {
      this.setState({
        branches_data: branches_data,
        branches_count: branches_data.length,
      });
      let data = [];
      for (let i = 10 * (current_page - 1); i <= current_page * 10; i++) {
        data.push(branches_data[i]);
      }
      this.setState({ current_branches: data });
    }

    if (
      branch_states &&
      Array.isArray(branch_states) &&
      branch_states.length > 0
    ) {
      let data = [];
      for (let i = 0; i < branch_states.length; i++) {
        data.push(branch_states[i].state);
      }
      data = data.sort();
      this.setState({ branch_states: data });
    }
    if (
      branch_states &&
      Array.isArray(branch_states) &&
      branch_states.length === 0
    ) {
      this.setState({ branch_states: [] });
    }

  }

  componentDidUpdate = () => {
    const { admin } = this.props;

    if (admin && admin.TYPE === "ADMIN_GET_BRANCH") {
      let data = admin.items && admin.items.data && admin.items.data.result && admin.items.data.result[0];
      if (data) {
        this.setState({
          branch_id: data.id,
          branch_name: data.name,
          branch_address: data.address1,
          branch_city: data.city,
          branch_country: data.country,
          branch_state: data.state,
          branch_zipcode: data.zipcode,
          editBranchView: true,
        });
        if (data.country === 'Canada') {
          this.setState({ isCanada: true })
        }
        else {
          this.setState({ isCanada: false })
        }
        this.props.get_states_list(data.country);
      }
      admin.TYPE = "";
    }

    if (admin && admin.TYPE === "ADMIN_UPDATE_STATE") {
      let data = admin.items.data && admin.items.data;
      if (data) {
        this.setState({
          branch_id: data.id,
          branch_name: data.name,
          branch_address: data.address1,
          branch_city: data.city,
          branch_country: data.country,
          branch_state: data.state,
          branch_zipcode: data.zipcode,
          editBranchView: true,
        });
        this.props.get_states_list(data.country);
      }
      admin.TYPE = "";
      toast(`Branch Updated!`, {
        autoClose: 2500,
        className: "black-background",
        bodyClassName: "red-hunt",
        progressClassName: "cc",
      });
    }

    if (admin && admin.TYPE === "ADMIN_DELETE_STATE") {
      let data = admin.items.data && admin.items.data;
      if (data) {
        this.setState({ search_name: "" });
        this.props.get_branches_list();
      }
      window.scrollTo(100, 500);
      admin.TYPE = "";
      this.setState({
        editBranchView: false,
        branch_city: "",
        branch_country: "",
        branch_zipcode: "",
        branch_address: "",
        branch_name: "",
      });
      toast(`Branch Deleted!`, {
        autoClose: 2500,
        className: "black-background",
        bodyClassName: "red-hunt",
        progressClassName: "cc",
      });
    }

    if (admin && admin.TYPE === "ADMIN_ADD_BRANCH") {
      let data = admin.items.data && admin.items.data;
      if (data) {
        this.setState({ search_name: "" });
        this.props.get_branches_list();
        window.scrollTo(100, 500);
      }
      admin.TYPE = "";
      this.setState({
        editBranchView: false,
        branch_city: "",
        branch_country: "",
        branch_zipcode: "",
        branch_address: "",
        branch_name: "",
      });
      toast(`New Branch Created!`, {
        autoClose: 2500,
        className: "black-background",
        bodyClassName: "red-hunt",
        progressClassName: "cc",
      });
    }
  };

  handleBranchUpdate = (id, branch) => {
    this.setState({
      search_name: "",
      adminEntriesId: branch.admin_entries_id
    });
    window.scrollTo(100, 2000);
    this.props.getBranch({ id: id });
  };

  handleChange = (e) => {
    let errors = this.state.errors;
    if (e.target.value !== "") {
      delete errors[e.target.name];
    }
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
      errors: errors,
    });

    if (e.target.name === "branch_country") {
      if (e.target.value === 'Canada') {
        this.setState({ isCanada: true })
      }
      else {
        this.setState({ isCanada: false })
      }
      this.props.get_states_list(e.target.value);
    }
  };

  checkFields = () => {
    const body = { ...this.state };
    let errors = {};
    if (body.branch_name === "") {
      errors["branch_name"] = "This field is required";
    }
    if (body.branch_address === "") {
      errors["branch_address"] = "This field is required";
    }
    if (body.branch_city === "") {
      errors["branch_city"] = "This field is required";
    }
    if (body.branch_zipcode === "") {
      errors["branch_zipcode"] = "This field is required";
    }
    if (body.branch_zipcode.length > 6 && this.state.isCanada) {
      errors["branch_zipcode"] = "Invalid Postal Code";
    }
    if (body.branch_country === "") {
      errors["branch_country"] = "This field is required";
    }
    if (body.branch_state === "") {
      errors["branch_state"] = "This field is required";
    }
    if (Object.keys(errors).length > 0) {
      this.setState({
        ...this.state,
        errors: errors,
      });
      return true;
    } else {
      return false;
    }
  };

  handleUpdate = (id) => {
    const {
      branch_name,
      branch_state,
      branch_zipcode,
      branch_city,
      branch_address,
      branch_country,
      adminEntriesId
    } = this.state;
    const body = { ...this.state };
    let errors = {};

    if (!this.checkFields()) {
      this.props.updateBranch({
        id: id,
        name: branch_name,
        address: branch_address,
        city: branch_city,
        state: branch_state,
        country: branch_country,
        zipcode: branch_zipcode,
        adminEntriesId: adminEntriesId,
      });
      window.scrollTo(100, 500);
      this.handleAddBranch();
    }
  };


  hasErrorFor = (field) => {
    return !!this.state.errors[field];
  };

  handleAdd = (e) => {
    const {
      branch_name,
      branch_state,
      branch_zipcode,
      branch_city,
      branch_address,
      branch_country,
    } = this.state;
    e.preventDefault();

    if (!this.checkFields()) {
      this.props.addBranch({
        name: branch_name,
        address: branch_address,
        city: branch_city,
        state: branch_state,
        country: branch_country,
        zipcode: branch_zipcode,
      });
    }
  };

  handleDelete = (id) => {
    this.setState({ isDeleteToggle: true, id: id });
  };
  deleteBranch = () => {
    this.props.deleteBranch({ id: this.state.id });
    this.setState({
      isDeleteToggle: !this.state.isDeleteToggle,
      editBranchView: false,
    });
  };

  handleAddBranch = () => {
	this.setState({
      editBranchView: false,
      branch_city: "",
      branch_country: "",
      branch_zipcode: "",
      branch_address: "",
      branch_name: "",
    });
	window.scrollTo(100, 500);
  };

  deleteToggle = () => {
    this.setState({ isDeleteToggle: !this.state.isDeleteToggle });
  };

  handlepagination = async (btn) => {
    const { branches_data, current_page, current_branches } = this.state;
    let data = [];

    if (btn === "previous" && this.state.current_page > 1) {
      for (let i = 10 * (current_page - 1); i <= current_page * 10; i++) {
        if (branches_data[i] && branches_data[i].name) {
          await data.push(branches_data[i]);
        }
      }
      await this.setState({
        current_page: this.state.current_page - 1,
        current_branches: data
      });
    }
    if (btn === "next" &&
      this.state.current_page < this.state.branches_count / 10
    ) {
      for (let i = 10 * (current_page - 1); i <= current_page * 10; i++) {
        if (branches_data[i] && branches_data[i].name) {
          await data.push(branches_data[i]);
        }
      }
      await this.setState({
        current_page: this.state.current_page + 1,
        current_branches: data
      });
    }
    // for (let i = 10 * (current_page - 1); i <= current_page * 10; i++) {
    //   if (branches_data[i] && branches_data[i].name) {
    //     await data.push(branches_data[i]);
    //   }
    // }
    // await this.setState({ current_branches: data });
  };

  handleSearch = async (e) => {
    const { branches_data, current_page } = this.state;
    this.setState({ [e.target.name]: e.target.value });
    let searched_branches = [];
    for (let i = 0; i < branches_data.length; i++) {
      if (branches_data[i].name.includes(e.target.value)) {
        searched_branches.push(branches_data[i]);
      }
    }
    await this.setState({
      branches_count: searched_branches.length,
      current_page: 1,
    });

    let data = [];
    for (let i = 10 * (current_page - 1); i <= current_page * 10; i++) {
      if (searched_branches[i] && searched_branches[i].name) {
        data.push(searched_branches[i]);
      }
    }
    await this.setState({ current_branches: data });
  };

  resetForm = (e) => {
    e.preventDefault();
    this.setState({
      ...this.state,
      ...initialState,
    });
  };
  renderErrorFor = (field) => {
    if (this.hasErrorFor(field)) {
      return <FormFeedback invalid>{this.state.errors[field]}</FormFeedback>;
    }
  };

  render() {
    const {
      isEditUserFormVisible,
      branches_count,
      current_branches,
      branch_data,
      editBranchView,
      branch_id,
      branch_name,
      branch_address,
      branch_city,
      branch_country,
      branch_state,
      branch_zipcode,
      branch_states,
      current_page,
      search_name,
      isCanada
    } = this.state;

    return (
      <div>
        <div className="bg-white mb-3 p-3">
          <div className="d-flex flex-wrap flex-column flex-sm-row align-items-center mx-n2 mb-4">
            <h3 className="text-secondary px-2 flex-grow-1 mb-2">
              Existing Branches (<span>{branches_count}</span>)
            </h3>
            <div className="px-2">
              <InputGroup>
                <Input
                  bsSize="sm"
                  type="search"
                  name="search_name"
                  placeholder="Type a branch name to filter"
                  onChange={this.handleSearch}
                  value={search_name}
                />
                <InputGroupAddon addonType="append">
                  <Button className="py-0">
                    <FontAwesomeIcon icon="search" />{" "}
                  </Button>
                </InputGroupAddon>
              </InputGroup>
            </div>
          </div>
          <hr className="bg-secondary" />
          <div>
            {current_branches &&
              current_branches.map((branch, index) => {
                if (branch) {
                  return (
                    <div key={index}>
                      <div className="d-flex flex-column flex-sm-row mx-n2 mb-3">
                        <div className="px-2 flex-grow-1">
                          <div className="mb-1">
                            <Link
                              to="/"
                              className="h2 text-secondary text-decoration-none"
                            >
                              {branch?.name}
                            </Link>
                          </div>
                          <div className="text-secondary-dark small">
                            <span className="d-block">{branch?.address1}</span>
                            <span className="d-block">
                              {branch?.city}, {branch?.state} {branch?.zipcode}
                            </span>
                          </div>
                          <div>
                            <img
                              // src={require("../../../assets/images/norating.png")}
                              src={branch && branch.rating ? `https://stagingdatawikireviews.s3.amazonaws.com/images/star/blue/${branch.rating}` : `https://stagingdatawikireviews.s3.amazonaws.com/images/star/blue/0rating.png`}
                              alt="rating"
                            />
                          </div>
                        </div>
                        <div className="px-2 ml-auto">
                          <Button
                            color="primary"
                            className={index == 0 ? `step-14 mw` : "mw"}
                            onClick={() => this.handleBranchUpdate(`${branch && branch.id}`, branch)}
                          >
                            Edit
                      </Button>
                        </div>
                      </div>
                      <hr className="bg-secondary" />
                    </div>
                  )
                }
              })}
          </div>
          {/* Pagination buttons */}
          <div className="text-center">
            <Button
              color="primary"
              className="mw mr-1"
              onClick={(btn) => this.handlepagination("previous")}
            >
              Previous
          </Button>
          &nbsp; &nbsp;
          {current_page}/{Math.ceil(branches_count / 10)}
            <Button
              color="primary"
              className="mw"
              onClick={(btn) => this.handlepagination("next")}
            >
              Next
          </Button>
          </div>
        </div>
        {editBranchView ? (
          <div className="text-right mb-3" onClick={this.handleAddBranch}>
            <Button color="primary" className="mw">
              <FontAwesomeIcon icon="plus" /> Add New Branch
            </Button>
          </div>
        ) : (
            ""
          )}

        {/* Update branch form, show when updating */}
        {editBranchView ? (
          <div className="bg-white p-3 mb-3">
            <div className="bg-body p-3 border border-secondary">
              <h3 className="text-secondary mb-3">Update Branch</h3>

              <FormGroup>
                <Input
                  type="text"
                  name="branch_name"
                  className="primary"
                  placeholder="Branch Name"
                  value={branch_name}
                  onChange={this.handleChange}
                  invalid={this.hasErrorFor("branch_name")}
                />
                {this.renderErrorFor("branch_name")}
              </FormGroup>


              <Row form>
                <Col md={8}>
                  <FormGroup>
                    <Input
                      type="text"
                      name="branch_address"
                      className="primary"
                      placeholder="Address"
                      value={branch_address}
                      onChange={this.handleChange}
                      invalid={this.hasErrorFor("branch_address")}
                    />
                    {this.renderErrorFor("branch_address")}

                  </FormGroup>
                </Col>

                <Col md={4}>
                  <FormGroup>
                    <Input
                      type="text"
                      name="branch_city"
                      className="primary"
                      placeholder="City"
                      value={branch_city}
                      onChange={this.handleChange}
                      invalid={this.hasErrorFor("branch_city")}
                    />
                    {this.renderErrorFor("branch_city")}
                  </FormGroup>
                </Col>
              </Row>


              <Row form>
                <Col md={4}>
                  <FormGroup>
                    <Input
                      type="select"
                      name="branch_country"
                      className="primary"
                      value={branch_country}
                      onChange={this.handleChange}
                      invalid={this.hasErrorFor("branch_country")}
                    >
                      <option>Select Country</option>
                      <option value="United States">United States</option>
                      <option value="Canada">Canada</option>
                    </Input>
                    {this.renderErrorFor("branch_country")}
                  </FormGroup>
                </Col>

                <Col md={4}>
                  <FormGroup>
                    <Input
                      type={isCanada ? "text" : "number"}
                      name="branch_zipcode"
                      className="primary"
                      placeholder={isCanada ? "Postal Code" : "Zipcode"}
                      value={branch_zipcode}
                      onChange={this.handleChange}
                      invalid={this.hasErrorFor("branch_zipcode")}
                    />
                    {this.renderErrorFor("branch_zipcode")}
                  </FormGroup>
                </Col>

                <Col md={4}>
                  <FormGroup>
                    <Input
                      type="select"
                      name="branch_state"
                      className="primary"
                      value={branch_state}
                      onChange={this.handleChange}
                      invalid={this.hasErrorFor("branch_state")}
                    >
                      {isCanada ?
                        <option>Select Province</option>
                        :
                        <option>Select State</option>
                      }
                      {branch_states &&
                        branch_states.map((state, index) => (
                          <option key={index} value={state}>{state}</option>
                        ))}
                      {/* <option value="CL">California</option>
                    <option value="TX">Texas</option>
                    <option value="MI">Michigan</option> */}
                    </Input>
                    {this.renderErrorFor("branch_state")}
                  </FormGroup>
                </Col>
              </Row>

              <div className="d-flex">
                <div>
                  <Button
                    color="transparent"
                    className="mw text-danger"
                    onClick={(id) => this.handleDelete(`${branch_id}`)}
                  >
                    Delete Branch
                </Button>
                </div>
                <div className="ml-auto">
                  <Button
                    color="light"
                    className="mw"
                    onClick={this.handleAddBranch}
                  >
                    Cancel
                </Button>
                  <Button
                    color="primary"
                    className="mw"
                    onClick={(id) => this.handleUpdate(`${branch_id}`)}
                  >
                    Save
                </Button>
                </div>
              </div>
            </div>
          </div>
        ) : (
            <div className="bg-white p-3 mb-3">
              <h3 className="text-secondary mb-3">Add New Branch</h3>

              <FormGroup>
                <Input
                  type="text"
                  name="branch_name"
                  className="primary"
                  placeholder="Branch Name"
                  value={branch_name}
                  onChange={this.handleChange}
                  invalid={this.hasErrorFor("branch_name")}
                />
                {this.renderErrorFor("branch_name")}
              </FormGroup>


              <Row form>
                <Col md={8}>
                  <FormGroup>
                    <Input
                      type="text"
                      name="branch_address"
                      className="primary"
                      placeholder="Address"
                      value={branch_address}
                      onChange={this.handleChange}
                      invalid={this.hasErrorFor("branch_address")}
                    />
                    {this.renderErrorFor("branch_address")}
                  </FormGroup>
                </Col>

                <Col md={4}>
                  <FormGroup>
                    <Input
                      type="text"
                      name="branch_city"
                      className="primary"
                      placeholder="City"
                      value={branch_city}
                      onChange={this.handleChange}
                      invalid={this.hasErrorFor("branch_city")}
                    />
                    {this.renderErrorFor("branch_city")}
                  </FormGroup>
                </Col>
              </Row>

              <Row form>
                <Col md={4}>
                  <FormGroup>
                    <Input
                      type="select"
                      name="branch_country"
                      className="primary"
                      value={branch_country}
                      onChange={this.handleChange}
                      invalid={this.hasErrorFor("branch_country")}
                    >
                      <option>Select Country</option>
                      <option value="United States">United States</option>
                      <option value="Canada">Canada</option>
                    </Input>
                    {this.renderErrorFor("branch_country")}
                  </FormGroup>
                </Col>

                <Col md={4}>
                  <FormGroup>
                    <Input
                      type={isCanada ? "text" : "number"}
                      name="branch_zipcode"
                      className="primary"
                      placeholder={isCanada ? "Postal Code" : "Zipcode"}
                      value={branch_zipcode}
                      onChange={this.handleChange}
                      invalid={this.hasErrorFor("branch_zipcode")}
                    />
                    {this.renderErrorFor("branch_zipcode")}
                  </FormGroup>
                </Col>

                <Col md={4}>
                  <FormGroup>
                    <Input
                      type="select"
                      name="branch_state"
                      className="primary"
                      value={branch_state}
                      onChange={this.handleChange}
                      invalid={this.hasErrorFor("branch_state")}
                    >
                      {isCanada ?
                        <option>Select Province</option>
                        :
                        <option>Select State</option>
                      }
                      {branch_states &&
                        branch_states.map((state) => (
                          <option value={state}>{state}</option>
                        ))}
                      {/* <option value="CL">California</option>
                    <option value="TX">Texas</option>
                    <option value="MI">Michigan</option> */}
                    </Input>
                    {this.renderErrorFor("branch_state")}
                  </FormGroup>
                </Col>
              </Row>

              <div className="d-flex">
                <div className="ml-auto">
                  <Button color="light" className="mw" onClick={this.handleAddBranch}>
                    Cancel
                </Button>
                  <Button color="primary" className="mw" onClick={this.handleAdd}>
                    Save
                </Button>
                </div>
              </div>
            </div>
          )}
        <Modal isOpen={this.state.isDeleteToggle} toggle={this.deleteToggle}>
          <ModalHeader toggle={this.deleteToggle}></ModalHeader>
          <ModalBody className="text-center">
            <h2 className="mb-3">Confirmation</h2>
            <p className="small">
              Are you sure you want to delete this Branch ?
            </p>

            <div className="pt-4">
              <div>
                <Button onClick={this.deleteToggle} size="md" color="primary">
                  Cancel
                </Button>
                <Button onClick={this.deleteBranch} size="md" color="primary">
                  Ok
                </Button>
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

const mapState = (state) => {
  return {
    admin: state.admin,
    branches_data: state.admin.branches_list,
    branch_states: state.admin.states_list,
  };
};

const mapDispatch = {
  get_branches_list,
  getBranch,
  updateBranch,
  deleteBranch,
  addBranch,
  get_states_list,
};

// const mapDispatch = {
//   get_branches_list,
//   getBranch,
//   getStates,
//   updateBranch,
//   deleteBranch,
//   addBranch,
// };
export default connect(mapState, mapDispatch)(AdminBranches);
