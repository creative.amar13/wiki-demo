import React, { Component } from "react";
import { connect } from "react-redux";

import {
  Row,
  Col,
  FormGroup,
  FormFeedback,
  Input,
  Button,
  Label,
} from "reactstrap";

import { get_roles_list, add_user } from "../../../actions/admin-view/admin";

const initialState = {
  first_name: "",
  last_name: "",
  email: "",
  role: "",
  errors: {},
};

class AddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState,
    };
  }

  componentDidMount() {
    this.props.get_roles_list();
  }

  handleChange = (e) => {
    let regexChar = /^[A-Za-z0-9 ]+$/;
    let errors = this.state.errors;
    if (e.target.value !== "") {
      delete errors[e.target.name];
    }
    let value = e.target.value;
    let name = e.target.name;

    if (name === "first_name" || name === "last_name") {
      let isvalid = regexChar.test(value);
      if (isvalid) {
        this.setState({
          ...this.state,
          [name]: value,
          errors: errors,
        });
      } else {
        const body = { ...this.state };
        let errors = {};
        errors[name] = "Special charcters not Allowed";
      }
    } else {
      this.setState({
        ...this.state,
        [name]: value,
        errors: errors,
      });
    }

  };

  handleSubmit = (e) => {
    e.preventDefault();
    const body = { ...this.state };
    let errors = {};
    if (body.role === "") {
      errors["role"] = "This field is required";
    }
    if (body.first_name === "") {
      errors["first_name"] = "This field is required";
    }
    if (body.last_name === "") {
      errors["last_name"] = "This field is required";
    }
    if (body.email === "") {
      errors["email"] = "This field is required";
    } else {
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!re.test(String(body.email).toLowerCase())) {
        errors["email"] = "Please enter a valid email";
      }
    }
    //delete body.roles;

    if (Object.keys(errors).length > 0) {
      this.setState({
        ...this.state,
        errors: errors,
      });
    } else {
      delete body.errors;
      this.props.add_user(body);
      this.setState({
        ...this.state,
        ...initialState,
      });
    }
  };

  resetForm = (e) => {
    e.preventDefault();
    this.setState({
      ...this.state,
      ...initialState,
    });
  };

  hasErrorFor = (field) => {
    return !!this.state.errors[field];
  };

  renderErrorFor = (field) => {
    if (this.hasErrorFor(field)) {
      return <FormFeedback invalid>{this.state.errors[field]}</FormFeedback>;
    }
  };

  render() {
    const { first_name, last_name, email, role } = this.state;
    const { roles } = this.props;

    return (
      <div>
        <Row form>
          <Col md={6}>
            <FormGroup>
              <Input
                invalid={this.hasErrorFor("first_name")}
                type="text"
                name="first_name"
                className="primary"
                placeholder="First Name"
                value={first_name}
                onChange={this.handleChange}
                required={true}
				maxLength="40"
              />
              {this.renderErrorFor("first_name")}
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Input
                invalid={this.hasErrorFor("last_name")}
                type="text"
                name="last_name"
                className="primary"
                placeholder="Last Name"
                value={last_name}
                onChange={this.handleChange}
                required={true}
				maxLength="40"
              />
              {this.renderErrorFor("last_name")}
            </FormGroup>
          </Col>
        </Row>
        <Row form>
          <Col md={6}>
            <FormGroup>
              <Input
                invalid={this.hasErrorFor("email")}
                type="email"
                name="email"
                className="primary"
                placeholder="Email Address"
                value={email}
                onChange={this.handleChange}
                required={true}
              />
              {this.renderErrorFor("email")}
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              {/* <Label for="selectRole">Select Role</Label> */}
              <Input
                invalid={this.hasErrorFor("role")}
                type="select"
                name="role"
                id="selectRole"
                className="primary colorbox"
                value={role}
                onChange={this.handleChange}
                required={true}
				
              >
                {roles && Array.isArray(roles) &&
                  roles.length > 0 && roles.map((role) => (
                    <option value={role.id} key={role.id}>
                      {role.name.toUpperCase()}
                    </option>
                  ))}
              </Input>
              {this.renderErrorFor("role")}
            </FormGroup>
          </Col>
        </Row>
        <FormGroup className="text-right">
          <Button color="light" className="mw" onClick={this.resetForm}>
            Cancel
          </Button>
          <Button color="primary" className="mw" onClick={this.handleSubmit}>
            Add
          </Button>
        </FormGroup>
      </div>
    );
  }
}
const mapState = (state) => ({
  roles: state.admin?.roles_list?.result,
});

const mapDispatch = {
  get_roles_list,
  add_user,
};

export default connect(mapState, mapDispatch)(AddUser);
