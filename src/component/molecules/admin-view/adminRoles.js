import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import {
  Row,
  Col,
  FormGroup,
  CustomInput,
  Input,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import {
  get_roles_list,
  get_roleview,
  add_new_role,
  delete_role,
  edit_save_role,
} from "../../../actions/admin-view/admin";
const initialSettings = [
  {
    id: "upload_remove_logo",
    intID: 2,
    name: "Upload/Remove Logo for Assigned Business",
  },
  {
    id: "upload_remove_images",
    intID: 3,
    name: "upload/Remove Image for Assigned Business",
  },
  {
    id: "upload_remove_videos",
    intID: 4,
    name: "upload/Remove Video for Assigned Business",
  },
  {
    id: "modify_listing_owner_guide",
    intID: 6,
    name: "Modify listing Owner Guide",
  },
  {
    id: "modify_core_lising_info",
    intID: 8,
    name: "Modify Core Listing info including hours etc.",
  },
  {
    id: "change_additional_info",
    intID: 7,
    name: "Mhange additional Info Section",
  },
  {
    id: "change_about_business",
    intID: 9,
    name:
      "Change about this business:specialities,history,meet the business owner",
  },
  {
    id: "modify_menu_item",
    intID: 10,
    name: "Modify Menu items and Prices",
  },
  {
    id: "other_offers",
    intID: 11,
    name: "Other offer",
  },
  {
    id: "add_remove_employee",
    intID: 12,
    name: "Add/Remove Employees",
  },
  {
    id: "create_post",
    intID: 14,
    name: "Create Posts",
  },
  {
    id: "respond_reviews",
    intID: 15,
    name: "Respond to Reviews",
  },
  {
    id: "respond_disputed_reviews",
    intID: 16,
    name: "Respond to Disputed Reviews",
  },
  {
    id: "respond_message",
    intID: 17,
    name: "Respond to Message",
  },
  {
    id: "respond_q&a",
    intID: 18,
    name: "Respond to Q&A",
  },
  {
    id: "respond_feedback",
    intID: 19,
    name: "Responding to Feedback",
  },
  {
    id: "call_to_action",
    intID: 20,
    name: "Create/Delete Call To Action",
  },
];
class AdminRoles extends Component {
  constructor(props) {
    super(props);
    this.roleFormRef = React.createRef()  
    this.state = {
      roles: [],
      textBox: "Add Role",
      editRoleId: "",
      role_name: "",
      noflag: "",
      yesflag: "",
      confirmDeleteModal: false,
      settings: [
        {
          id: "upload_remove_logo",
          intID: 2,
          name: "Upload/Remove Logo for Assigned Business",
        },
        {
          id: "upload_remove_images",
          intID: 3,
          name: "upload/Remove Image for Assigned Business",
        },
        {
          id: "upload_remove_videos",
          intID: 4,
          name: "upload/Remove Video for Assigned Business",
        },
        {
          id: "modify_listing_owner_guide",
          intID: 6,
          name: "Modify listing Owner Guide",
        },
        {
          id: "modify_core_lising_info",
          intID: 8,
          name: "Modify Core Listing info including hours etc.",
        },
        {
          id: "change_additional_info",
          intID: 7,
          name: "Mhange additional Info Section",
        },
        {
          id: "change_about_business",
          intID: 9,
          name:
            "Change about this business:specialities,history,meet the business owner",
        },
        {
          id: "modify_menu_item",
          intID: 10,
          name: "Modify Menu items and Prices",
        },
        {
          id: "other_offers",
          intID: 11,
          name: "Other offer",
        },
        {
          id: "add_remove_employee",
          intID: 12,
          name: "Add/Remove Employees",
        },
        {
          id: "create_post",
          intID: 14,
          name: "Create Posts",
        },
        {
          id: "respond_reviews",
          intID: 15,
          name: "Respond to Reviews",
        },
        {
          id: "respond_disputed_reviews",
          intID: 16,
          name: "Respond to Disputed Reviews",
        },
        {
          id: "respond_message",
          intID: 17,
          name: "Respond to Message",
        },
        {
          id: "respond_q&a",
          intID: 18,
          name: "Respond to Q&A",
        },
        {
          id: "respond_feedback",
          intID: 19,
          name: "Responding to Feedback",
        },
        {
          id: "call_to_action",
          intID: 20,
          name: "Create/Delete Call To Action",
        },
      ],
    };
  }

  componentWillReceiveProps(nextProps) {
    const { roles, roleview } = nextProps;
    if (roleview && roleview.yesflag) {
      this.setState({ yesflag: "true" });
    } else if (roleview && !roleview.yesflag) {
      this.setState({ yesflag: "false" });
    }

    if (roleview && roleview.noflag) {
      this.setState({ noflag: "true" });
    } else if (roleview && !roleview.noflag) {
      this.setState({ noflag: "false" });
    }

    if (
      roleview &&
      roleview.Media &&
      Array.isArray(roleview.Media) &&
      roleview.Media.length > 0
    ) {
      let test = [];
      roleview.Media.map((element1) => {
        test = this.state.settings.map((element2) => {
          if (element1.id === element2.intID) {
            element2.value = element1.selected;
          }
          return element2;
        });
      });
    }
    if (
      roleview &&
      roleview["Managing community content"] &&
      Array.isArray(roleview["Managing community content"]) &&
      roleview["Managing community content"].length > 0
    ) {
      let test = [];
      roleview["Managing community content"].map((element1) => {
        test = this.state.settings.map((element2) => {
          if (element1.id === element2.intID) {
            element2.value = element1.selected;
          }
          return element2;
        });
      });
    }
    if (
      roleview &&
      Array.isArray(roleview["Managing listing content"]) &&
      roleview["Managing listing content"] &&
      roleview["Managing listing content"].length > 0
    ) {
      let test = [];
      roleview["Managing listing content"].map((element1) => {
        test = this.state.settings.map((element2) => {
          if (element1.id === element2.intID) {
            element2.value = element1.selected;
          }
          return element2;
        });
      });
    }
    if (roles && Array.isArray(roles) && roles.length > 0) {
      this.setState({
        roles: roles,
      });
    }
  }

  componentDidMount() {
    this.props.get_roles_list();
  }

  handleChange = async (event) => {
    await this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleOptionsChange = async (e) => {
    if (e.target.value === "false") {
      this.setState({ yesflag: "false" });
    } else {
      this.setState({ noflag: "false" });
    }
    const { settings } = this.state;
    const roleName = e.target.name;
    const roleValue = e.target.value;
    settings.map(async (element) => {
      if (element.id === roleName) {
        await this.setState((prevState) => ({
          settings: prevState.settings.map((obj) =>
            obj.id === roleName ? Object.assign(obj, { value: roleValue }) : obj
          ),
        }));
      }
    });
    let select_count_true = 0;
    let select_count_false = 0;
    this.state.settings.map((data) => {
      if (data.value === "true") {
        select_count_true += 1;
      } else {
        select_count_false += 1;
      }
    });
    if (select_count_true === 17) {
      this.setState({ yesflag: "true", noflag: "false" });
    }
    if (select_count_false === 17) {
      this.setState({ noflag: "true", yesflag: "false" });
    }
  };

  handleSelectAll = (e) => {
    const { settings } = this.state;
    const roleName = e.target.name;
    const roleValue = e.target.value;
    settings.map(async (element) => {
      if (roleValue === "true") {
        await this.setState((prevState) => ({
          settings: prevState.settings.map((obj) =>
            Object.assign(obj, { value: "true" })
          ),
          yesflag: "true",
          noflag: "false",
        }));
      } else if (roleValue === "false") {
        await this.setState((prevState) => ({
          settings: prevState.settings.map((obj) =>
            Object.assign(obj, { value: "false" })
          ),
          noflag: "true",
          yesflag: "false",
        }));
      }
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const body = { ...this.state };
    //delete body.roles;
    delete body.errors;
  };

  handleEditRole = async (id) => {
    const { get_roleview } = this.props;

    await this.setState({
      textBox: "Edit Role",
      editRoleId: id,
    });
    get_roleview(this.state.editRoleId);
  };

  handleSaveEditedRole = async (id) => {
    const { edit_save_role } = this.props;
    const data = {};
    data["name"] = this.state.role_name;
    const editSettingsData = await this.state.settings.filter((element) => {
      return element.value !== undefined && element.value !== "none";
    });
    data["settings"] = await editSettingsData.map((element) => {
      if (element.value !== undefined && element.value !== "none") {
        return {
          id: element.intID,
          value: element.value === "true" ? "1" : "0",
        };
      }
    });

    if (data.settings && data.settings.length > 0) {
      edit_save_role(this.state.editRoleId, data);
    }
    this.setState({
      ...this.state,
      settings: [
        {
          id: "upload_remove_logo",
          intID: 2,
          name: "Upload/Remove Logo for Assigned Business",
        },
        {
          id: "upload_remove_images",
          intID: 3,
          name: "upload/Remove Image for Assigned Business",
        },
        {
          id: "upload_remove_videos",
          intID: 4,
          name: "upload/Remove Video for Assigned Business",
        },
        {
          id: "modify_listing_owner_guide",
          intID: 6,
          name: "Modify listing Owner Guide",
        },
        {
          id: "modify_core_lising_info",
          intID: 8,
          name: "Modify Core Listing info including hours etc.",
        },
        {
          id: "change_additional_info",
          intID: 7,
          name: "Mhange additional Info Section",
        },
        {
          id: "change_about_business",
          intID: 9,
          name:
            "Change about this business:specialities,history,meet the business owner",
        },
        {
          id: "modify_menu_item",
          intID: 10,
          name: "Modify Menu items and Prices",
        },
        {
          id: "other_offers",
          intID: 11,
          name: "Other offer",
        },
        {
          id: "add_remove_employee",
          intID: 12,
          name: "Add/Remove Employees",
        },
        {
          id: "create_post",
          intID: 14,
          name: "Create Posts",
        },
        {
          id: "respond_reviews",
          intID: 15,
          name: "Respond to Reviews",
        },
        {
          id: "respond_disputed_reviews",
          intID: 16,
          name: "Respond to Disputed Reviews",
        },
        {
          id: "respond_message",
          intID: 17,
          name: "Respond to Message",
        },
        {
          id: "respond_q&a",
          intID: 18,
          name: "Respond to Q&A",
        },
        {
          id: "respond_feedback",
          intID: 19,
          name: "Responding to Feedback",
        },
        {
          id: "call_to_action",
          intID: 20,
          name: "Create/Delete Call To Action",
        },
      ],
      textBox: "Add Role",
      editRoleId: "",
      role_name: "",
      noflag: "false",
      yesflag: "false",
    });
    // this.forceUpdate();
  };

  handleAddRole = async (id) => {
    const { add_new_role, get_roles_list } = this.props;
    const data = {};

    data["name"] = this.state.role_name;
    data["settings"] = await this.state.settings.map((element) => {
      if (element.value !== undefined) {
        return {
          id: element.intID,
          value: element.value === "true" ? 1 : 0,
        };
      } else {
        return {
          id: element.intID,
          value: "none",
        };
      }
    });

    const unSelectedRadio = await data.settings.filter((element) => {
      return element.value === "none";
    });

    if (unSelectedRadio.length > 0) {
      this.handleSelectAllRadio();
    } else {
      await add_new_role(data);
      this.setState({
        textBox: "Add Role",
        editRoleId: "",
        role_name: "",
        noflag: "false",
        yesflag: "false",
        settings: [
          {
            id: "upload_remove_logo",
            intID: 2,
            name: "Upload/Remove Logo for Assigned Business",
          },
          {
            id: "upload_remove_images",
            intID: 3,
            name: "upload/Remove Image for Assigned Business",
          },
          {
            id: "upload_remove_videos",
            intID: 4,
            name: "upload/Remove Video for Assigned Business",
          },
          {
            id: "modify_listing_owner_guide",
            intID: 6,
            name: "Modify listing Owner Guide",
          },
          {
            id: "modify_core_lising_info",
            intID: 8,
            name: "Modify Core Listing info including hours etc.",
          },
          {
            id: "change_additional_info",
            intID: 7,
            name: "Mhange additional Info Section",
          },
          {
            id: "change_about_business",
            intID: 9,
            name:
              "Change about this business:specialities,history,meet the business owner",
          },
          {
            id: "modify_menu_item",
            intID: 10,
            name: "Modify Menu items and Prices",
          },
          {
            id: "other_offers",
            intID: 11,
            name: "Other offer",
          },
          {
            id: "add_remove_employee",
            intID: 12,
            name: "Add/Remove Employees",
          },
          {
            id: "create_post",
            intID: 14,
            name: "Create Posts",
          },
          {
            id: "respond_reviews",
            intID: 15,
            name: "Respond to Reviews",
          },
          {
            id: "respond_disputed_reviews",
            intID: 16,
            name: "Respond to Disputed Reviews",
          },
          {
            id: "respond_message",
            intID: 17,
            name: "Respond to Message",
          },
          {
            id: "respond_q&a",
            intID: 18,
            name: "Respond to Q&A",
          },
          {
            id: "respond_feedback",
            intID: 19,
            name: "Responding to Feedback",
          },
          {
            id: "call_to_action",
            intID: 20,
            name: "Create/Delete Call To Action",
          },
        ],
      });
      get_roles_list();
    }
  };

  handleDeleteRole = async () => {
    const { delete_role } = this.props;
    await delete_role(this.state.editRoleId);
    await this.setState({
      ...this.state,
      settings: initialSettings,
      textBox: "Add Role",
      editRoleId: "",
      role_name: "",
      noflag: "false",
      yesflag: "false",
    });
  };
  handleSelectAllRadio = () => {
    this.setState({ selectAllRadio: !this.state.selectAllRadio });
  };

  executeScroll = () => {
    this.roleFormRef.current.scrollIntoView()
  };

  render() {
    const { first_name, last_name, email, roles } = this.state;
    const { error } = this.props;
    if (error) {
      return <p>Error! {error.message}</p>;
    }
    return (
      <div>
        <div className="bg-white p-3 mb-3">
          <h3 className="text-secondary mb-4">Existing Roles</h3>
          <Modal
            isOpen={this.state.selectAllRadio}
            toggle={this.handleSelectAllRadio}
            className="text-center"
          >
            <div className="bg-white modal-header">
              <h5 className="modal-title mx-auto text-dark">Alert</h5>
              <button
                className="btn btn-sm"
                aria-label="Close"
                onClick={this.handleSelectAllRadio}
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <ModalBody className="text-dark">Select All Roles</ModalBody>
            <ModalFooter className="bg-white">
              <div className="text-center w-100">
                <Button
                  color="primary"
                  size="sm"
                  className="mw"
                  onClick={this.handleSelectAllRadio}
                >
                  Ok
                </Button>
              </div>
            </ModalFooter>
          </Modal>
          {/* <div>
              <Row>
                <Col xs>
                  <span className="text-dark font-weight-bold">Admin</span>
                </Col>
                <Col xs="auto">
                  <span className="text-muted font-weight-bold d-block text-center fs-14">
                    Default Role
                  </span>
                </Col>
              </Row>
              <hr className="bg-secondary" />
            </div> */}
          {roles &&
            Array.isArray(roles) &&
            roles.length > 0 &&
            roles.map((element, index) => {
              return (
                <div className="px-3" key={index}>
                  <Row className="align-items-center">
                    <Col xs>
                      <span className="text-dark font-weight-bold">
                        {element.id === "" ? "Admin Role" : element.name}
                      </span>
                    </Col>
                    <Col xs="auto">
                      {element.id === "" ? (
                        <span className="text-muted font-weight-bold d-block text-center fs-14">
                          Default Role
                        </span>
                      ) : (
                        <Button
                          color="primary"
                          className="mw"
                          onClick={() => {
                            this.executeScroll();
                            this.handleEditRole(element.id);
                            this.setState({
                              role_name: element.name,
                            });
                          }}
                        >
                          Edit
                        </Button>
                      )}
                    </Col>
                  </Row>
                  <hr className="bg-secondary" />
                </div>
              );
            })}
        </div>

        <div ref={this.roleFormRef} className="bg-white p-3 mb-3 step-15">
          <div className="bg-body p-3 border border-secondary">
            <h3 className="text-secondary mb-3">
              {this.state.textBox === "Edit Role"
                ? "Update Role"
                : "Create a Role"}
            </h3>
            <div className="mb-3">
              <FormGroup row>
                <Col md={8}>
                  <Input
                    type="text"
                    name="role_name"
                    className="primary mb-2"
                    placeholder="Type a role name here"
                    value={this.state.role_name}
                    onChange={this.handleChange}
                  />
                </Col>
                <Col xs="auto" className="ml-md-auto">
                  {this.state.textBox === "Edit Role" ? (
                    ""
                  ) : (
                      <Button
                        color="primary"
                        className="mw"
                        disabled={this.state.role_name === "" ? true : false}
                        onClick={this.handleAddRole}
                      >
                        Add
                      </Button>
                    )}
                </Col>
              </FormGroup>
            </div>
            <div className="">
              <Row className="align-items-center mb-2">
                {/* <Col md={8}>&nbsp;</Col> */}
                <Col xs="auto" className="ml-md-auto">
                  <CustomInput
                    type="radio"
                    className="lg square"
                    id="yesflag"
                    name="selectAll"
                    label="All&nbsp;"
                    value="true"
                    checked={this.state.yesflag === "true"}
                    onChange={this.handleSelectAll}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg square"
                    id="noflag"
                    name="selectAll"
                    label="All&nbsp;"
                    value="false"
                    checked={this.state.noflag === "true"}
                    onChange={this.handleSelectAll}
                    inline
                  />
                </Col>
              </Row>
            </div>
            <div className="mb-3">
              <h4 className="text-dark mb-2">Media</h4>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">
                    • Upload/Remove Logo for Assigned Business
                </span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="upload_remove_logo_Yes"
                    name="upload_remove_logo"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[0].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="upload_remove_logo_No"
                    name="upload_remove_logo"
                    label="No"
                    value="false"
                    checked={this.state.settings[0].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">
                    • Upload/Remove Images for Assigned Business
                </span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="upload_remove_images_Yes"
                    name="upload_remove_images"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[1].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="upload_remove_images_No"
                    name="upload_remove_images"
                    label="No"
                    value="false"
                    checked={this.state.settings[1].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">
                    • Upload/Remove Videos for Assigned Business
                </span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="upload_remove_videos_Yes"
                    name="upload_remove_videos"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[2].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="upload_remove_videos_No"
                    name="upload_remove_videos"
                    label="No"
                    value="false"
                    checked={this.state.settings[2].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
            </div>
            <div className="mb-3">
              <h4 className="text-dark mb-2">Managing Listing Content</h4>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">
                    • Modify listing Owner Guide (Editable)
                </span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="modify_listing_owner_guide_Yes"
                    name="modify_listing_owner_guide"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[3].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="modify_listing_owner_guide_No"
                    name="modify_listing_owner_guide"
                    label="No"
                    value="false"
                    checked={this.state.settings[3].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">
                    • Modify Core Listing Info including hours, etc.
                </span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="modify_core_lising_info_Yes"
                    name="modify_core_lising_info"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[4].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="modify_core_lising_info_No"
                    name="modify_core_lising_info"
                    label="No"
                    value="false"
                    checked={this.state.settings[4].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">
                    • Change Additional Info Section
                </span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="change_additional_info_Yes"
                    name="change_additional_info"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[5].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="change_additional_info_No"
                    name="change_additional_info"
                    label="No"
                    value="false"
                    checked={this.state.settings[5].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">
                    • Change About This Businesses: <br />{" "}
                    <span>Specialities, History, Meet the business Owner</span>
                  </span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="change_about_business_Yes"
                    name="change_about_business"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[6].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="change_about_business_No"
                    name="change_about_business"
                    label="No"
                    value="false"
                    checked={this.state.settings[6].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">
                    • Modify Menu Items & Prices
                </span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="modify_menu_item_Yes"
                    name="modify_menu_item"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[7].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="modify_menu_item_No"
                    name="modify_menu_item"
                    label="No"
                    value="false"
                    checked={this.state.settings[7].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">• Other Offers</span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="other_offers_Yes"
                    name="other_offers"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[8].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="other_offers_No"
                    name="other_offers"
                    label="No"
                    value="false"
                    checked={this.state.settings[8].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">• Add/Remove Employees </span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="add_remove_employee_Yes"
                    name="add_remove_employee"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[9].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="add_remove_employee_No"
                    name="add_remove_employee"
                    label="No"
                    value="false"
                    checked={this.state.settings[9].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
            </div>
            <div className="mb-3">
              <h4 className="text-dark mb-2">Managing Community Content</h4>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">• Create Posts</span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="create_post_Yes"
                    name="create_post"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[10].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="create_post_No"
                    name="create_post"
                    label="No"
                    value="false"
                    checked={this.state.settings[10].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">• Respond to Reviews</span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="respond_reviews_Yes"
                    name="respond_reviews"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[11].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="respond_reviews_No"
                    name="respond_reviews"
                    label="No"
                    value="false"
                    checked={this.state.settings[11].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">
                    • Respond to Disputed Reviews
                </span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="respond_disputed_reviews_Yes"
                    name="respond_disputed_reviews"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[12].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="respond_disputed_reviews_No"
                    name="respond_disputed_reviews"
                    label="No"
                    value="false"
                    checked={this.state.settings[12].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">• Respond to Messages</span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="respond_message_Yes"
                    name="respond_message"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[13].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="respond_message_No"
                    name="respond_message"
                    label="No"
                    value="false"
                    checked={this.state.settings[13].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">• Respond to Q&A</span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="respond_q&a_Yes"
                    name="respond_q&a"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[14].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="respond_q&a_No"
                    name="respond_q&a"
                    label="No"
                    value="false"
                    checked={this.state.settings[14].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">
                    • Responding to Feedback
                </span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="respond_feedback_Yes"
                    name="respond_feedback"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[15].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="respond_feedback_No"
                    name="respond_feedback"
                    label="No"
                    value="false"
                    checked={this.state.settings[15].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
              <Row className="align-items-center mb-2">
                <Col xs={12} className="col-md">
                  <span className="text-dark small">
                    • Create/Delete Call To Action{" "}
                  </span>
                </Col>
                <Col xs="auto">
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="call_to_action_Yes"
                    name="call_to_action"
                    label="Yes"
                    value="true"
                    checked={this.state.settings[16].value === "true"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                  <CustomInput
                    type="radio"
                    className="lg"
                    id="call_to_action_No"
                    name="call_to_action"
                    label="No"
                    value="false"
                    checked={this.state.settings[16].value === "false"}
                    onChange={this.handleOptionsChange}
                    inline
                  />
                </Col>
              </Row>
            </div>
            {this.state.textBox === "Edit Role" ? (
              <div className="mb-3">
                <FormGroup className="justify-content-center justify-content-lg-end" row>
                  <Col xs="12" sm="auto">
                    <Button
                      color="link"
                      className="text-danger font-weight-bold text-decoration-none btn-block btn-sm-inline-block"
                      onClick={this.handleDeleteRole}
                    >
                      Delete
                  </Button>
                  </Col>
                  <Col xs="12" sm="auto">
                    <Button
                      color="primary"
                      className="mw btn-block btn-sm-inline-block mb-3 mb-sm-0"
                      onClick={() => {
                        this.setState({
                          ...this.state,
                          settings: initialSettings,
                          textBox: "Add Role",
                          editRoleId: "",
                          role_name: "",
                          noflag: "false",
                          yesflag: "false",
                        });
                      }}
                    >
                      Cancel
                  </Button>
                  </Col>
                  <Col xs="12" sm="auto">
                    <Button
                      color="primary"
                      className="mw btn-block btn-sm-inline-block mb-3 mb-sm-0"
                      onClick={this.handleSaveEditedRole}
                    >
                      Save
                  </Button>
                  </Col>
                </FormGroup>
              </div>
            ) : (
                ""
              )}
          </div>
        </div>
      </div>
    );
  }
}
const mapState = (state) => {
  return {
    roles: state.admin?.roles_list?.result,
    error: state.adminViewRoles.error,
    roleview: state.admin.roleview,
  };
};

const mapDispatch = {
  get_roles_list,
  get_roleview,
  add_new_role,
  delete_role,
  edit_save_role,
};

export default connect(mapState, mapDispatch)(AdminRoles);
