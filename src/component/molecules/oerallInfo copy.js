import { Formik, FieldArray } from "formik";
import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { connect } from "react-redux";
import CollapseBasic from "../atoms/collapse";
import EditBtn from "../atoms/editBtn";
import DeleteBtn from "../atoms/deleteBtn";
import { toast } from "react-toastify";
import BranchAdditionalInfo from "./branchAdditionalInfo";
import PhoneInput from 'react-phone-number-input/input'

// import EditableLabel from 'react-inline-edit';
import {
  FormGroup,
  Modal,
  ModalHeader,
  ModalBody,
  Input,
  Button,
  Container,
  Row,
  Col,
  Badge,
  // ButtonGroup,
  Progress,
  ModalFooter
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import {
  get_top_bar_counts,
  get_stats_filtered_data,
  get_branch_copy,
  get_branch_data,
  update_listing_owner_guide,
  copy_add_branch,
  fresh_add_branch,
  upload_corporate_media,
  get_admin_stats,
  get_review_data_by_circle_click,
  corporate_copy_details,
  get_corporate_call_list,
  post_business_update,
  delete_business_record,
  get_business_category_data,
  update_payment_options,
  post_business_category_data,
  delete_business_categories,
  post_hours_of_operation,
  put_hours_of_operation,
  delete_hours_of_operation,
  get_sub_categories,
  post_special_offer,
  update_special_offer,
  delete_offer,
  add_additional_value,
  add_name,
  add_position_value,
  update_additional_info,
  delete_additional_info,
  get_amenities_options,
  get_specific_branch_corporate_call_list,
  corporate_id_details,
  upload_move_set_profile_media,
  delete_payment_option,
  delete_corporate_call,
  get_biz_callon_type,
  corporate_call_to_action,
} from "../../actions/user";
import {
  get_roles_list_filter_data
} from "../../actions/admin-view/admin";
import Label from "reactstrap/lib/Label";
import CustomInput from "reactstrap/lib/CustomInput";
import { callApi } from "../../utils/apiCaller";
import moment from 'moment';
import listingGuide from './tabs/rawJsonData/listingGuide.json';
import AvForm from "availity-reactstrap-validation/lib/AvForm";
import AvField from "availity-reactstrap-validation/lib/AvField";
import { emailRegex, websiteRegex } from "../../utils/validation";
import Switch from "react-input-switch";
import { AvInput, AvFeedback, AvGroup } from "availity-reactstrap-validation";
import Form from "reactstrap/lib/Form";
import FormText from "reactstrap/lib/FormText";
// import AvFeedback from "availity-reactstrap-validation/lib/AvFeedback";

class OverallData extends Component {
  constructor(props) {
    super(props);
    this.refWebsiteForm = React.createRef()
    this.refBusinessEmailForm = React.createRef()
    this.refNameForm = React.createRef();
    this.refPositionForm = React.createRef();

    // this.refrefPassForm = React.createRef();
    this.state = {
      hoverImage: 'call_on_static1.png',
      typeService: '',
      linked_url: '',
      headline: '',
      response_time: null,
      sub_headline: '',
      previewImage: '',
      setup: true,
      selectValueCalltoAction: [],
      getStarted: false,
      isToggleDeletePayment: false,
      dotSelectedType: '',
      dotSelectedColor: '',
      dotSelectedName: '',
      dotSelectedDays: '',
      dotSelectedqueryType: '',
      isImage: false,
      relX: 0,
      relY: 0,
      x: -85,
      y: -45,
      left: 0,
      top: 0,
      uploadMedia: {
        selectedMedia: [],
        selectedMediaIds: [],
        uploadFile: [],
        progress: 0,
      },
      formType: '',
      errorFirstName: '',
      errorLastName: '',
      errorPosition: '',
      errorEmail: '',
      selectallEmails: false,
      selectallWebsites: false,
      selectallCategories: false,
      selectallPayment: false,
      selectallAddress: false,
      listingOwnerGuideModal: false,
      checkAllBusinessInfo: false,
      freshprofilePicUrl: "",
      freshProfilePicId: "",
      freshprofileBranchLogoUrl: "",
      freshProfileBranchLogoId: "",
      freshProfileBranchBannerUrl: "",
      freshProfileBranchBannerId: "",
      corporateDetails: null,
      topBarStats: null,
      stats_duration: "30",
      getallBranchCopy: {},
      branches: [],
      newBranches: [],
      pendingBranches: [],
      countryList: [],
      cState: {},
      country: "",
      stateName: "",
      countryName: "",
      countryListingName: "",
      provinceName: "",
      branchesData: [],
      countryState: [],
      isdisabled: true,
      getBranchData: '',
      branchId: 0,
      rowState: [],
      allBranches: [],
      withoutfilterBranches: [],
      checkAll: false,
      checkAllaction: false,
      checkAllOffer: false,
      isCheckedFullName: false,
      isCheckedEmail: false,
      isCheckedPosition: false,
      isCheckedProfile: false,
      islogoChecked: false,
      ischeckedSpecial: false,
      ischeckedHistory: false,
      ischeckedMBO: false,
      website: {},
      phoneNo: {},
      selectValue: [],
      selectBranches: [],
      checkAllAditional: false,
      isCheckedAllOwner: false,
      rowStateAditional: [],
      selectValueAditional: [],
      checkAllAddress: false,
      isVideoChecked: false,
      isChecked: false,
      selectValueAddress: [],
      isError: false,
      checkAllBusinessAll: false,
      isdisabledsave: true,
      mediaId: 0,
      getAdminStats: {},
      ratings: [],
      specialities: [],
      meetOwner: [],
      businessOwnerhistory: [],
      specialofferSet: [],
      phoneDiary: [],
      AdressDiary: [],
      webSet: [],
      emailSet: [],
      payOptions: [],
      categoryList: [],
      HoursOfOperation: [],
      profileSet: [],
      professionSet: [],
      MediaData: "",
      branch_logo_pic: "",
      branch_profile_pic: "",
      fullName: "",
      position: "",
      userEmail: null,
      profilePic: null,
      listing_id: '',
      corporateCallList: [],
      previewAditional: [],
      selectedPhone: [],
      selectedWebsite: [],
      selectedEmail: [],
      selectedCategory: [],
      selectedCalltoAction: [],
      selectedSpecialOffers: [],
      selectedPayment: [],
      selectedHour: [],
      selectedAbout: [],
      //selectedBusiness: [{ email: "", profile: "", position: ""}],
      selectedBusiness: { email: "", profile: "", position: "" },
      listingOwnerGuide: [],
      ownerProfile: "",
      ownerEmail: "",
      ownerPosition: "",
      video_to_copy: "",
      logo_to_copy: "",
      listingImage: '',
      listing_name: '',
      faqEditID: '',
      faqEditClick: false,
      nonFaqEditID: '',
      nonFaqEditClick: false,
      preferredMethod: 'freshData',

      addPhone: { ph1: '', ph2: '', ph3: '', label: '' },
      isEditPhone: false,
      isTogglePhone: false,
      isPhoneValid: true,

      isToggleBusinessEmail: false, // BusinessEmail fields
      isEditBusinessEmail: false,
      addBusinessEmail: { email: "", email_type: null },
      isBusinessEmailValid: true,

      isToggleWebsite: false, // Website fields
      isEditWebsite: false,
      addWebsite: { website: "", website_type: null },
      isWebsiteValid: true,

      specialofferSet: [], //offer set
      isToggleOffer: false,
      isToggleDeleteOffers: false,
      isEditOffer: false,
      addOffer: { special_offer: "", offer_type: "", from_date: "", to_date: "", claim_deal_link: "" },
      claim_deal_link_ur: "",

      editRecordId: false, // edit record
      editRecordType: false,

      clonePaymentSwitch: {}, // payment
      paymentSwitch: {},
      isTogglePayment: false,

      businessCategory: [], // Business category
      businessSubCategory: [],
      totalSubCategories: [1],
      anotherCategories: [],
      anotherSubCategories: [],
      postCategoryList: {},
      seletedCategories: [],
      seletedCategoriesNames: [],
      business_specialities: '',
      address1: '',
      subCategoryZero: '',
      selectedPayCategory: '',

      isToggleHours: false, // Hours fields
      isEditHours: false,
      addHours: { day_of_week: 0, end_time: "", start_time: "", entries: "", id: "", info: "", "next_day": 0 },
      isHoursValid: false,
      isToggleDeleteHours: false,

      isEditAddress: false,
      addAddress: { address1: '', address2: '', state: '', city: '', zipcode: '', country: '', latitude: '', longitude: '' },

      specialities: [], //abbut the business
      meetOwner: [],
      businessOwnerhistory: [],
      editSpec: true,
      editValue: "",
      editHisSpec: true,
      editHistory: "",
      EditBussOwn: "",
      editBusiness: true,
      isToggleBusiness: false,
      isToggleHistory: false,
      isToggleMeet: false,

      fullName: "", // Branch Info
      position: "",
      userEmail: null,
      profilePic: null,
      editName: true,
      fName: "",
      lName: "",
      isValidFullName: true,
      isTogglePosition: true,
      isValidPosition: true,
      profession: "",
      userID: null,

      isToogleAcceptCreditCards: false, // additional information
      additionInfo: [],
      allAdditionalInfo: [],
      all_additional_fields: [],
      selectedAddCategory: '',
      isEditVisible: {},
      CategoryValue: 'None',
      CategoryName: '',
      CategoryId: '',
      corporateId: '',
      existing_info_count: 0,
      add_info_map: '',

      sub_category_list: [],
      amenities_list: '',
      saved_amenities_list: [],
      amenityId: '',
      additional_info_all_categories: [],
      received_additional_info: false,
      isOpenAdditional: {},
      newID: 0,
      selectallPhone: false,

      freshData: {
        freshFirstName: "",
        freshLastName: "",
        freshPosition: "",
        freshPersonEmail: "",
        freshSpecialities: '',
        freshHistory: '',
        freshBusinessOwner: "",
        freshWROffer: [
          {
            freshOfferText: '',
            freshOfferType: '',
            freshOfferStartDATE: '',
            freshOfferEndDate: "",
            freshOfferWebsiteLink: "",
          }
        ],
        freshAddress: [
          {
            freshAddress1: '',
            freshAddress2: '',
            freshAddressCity: '',
            freshAddressState: '',
            freshAddressCountry: "",
            freshAddressZipcode: "",
          }
        ],
        freshPhone: [
          {
            phoneFresh: '',
            freshPhoneType: '',
          }
        ],
        freshWebsite: [
          {
            freshWebsiteURL: '',
            freshWebsiteType: '',
          }
        ],
        freshHOP: [
          {
            freshHOPDay: '',
            freshHOPText: '',
            freshHOPStartTime: '',
            freshHOPEndTime: '',
          }
        ],
        freshEmail: [
          {
            freshemailText: "",
            freshEmailType: "",
          }
        ]
      },
      freshWROffer: [
        {
          freshOfferText: '',
          freshOfferType: '',
          freshOfferStartDATE: '',
          freshOfferEndDate: "",
          freshOfferWebsiteLink: "",
        }
      ],
      freshAddress: [
        {
          freshAddress1: '',
          freshAddress2: '',
          freshAddressCity: '',
          freshAddressState: '',
          freshAddressCountry: "",
          freshAddressZipcode: "",
        }
      ],
      freshPhone: [
        {
          phoneFresh: '',
          freshPhoneType: '',
        }
      ],
      freshWebsite: [
        {
          freshWebsiteURL: '',
          freshWebsiteType: '',
        }
      ],
      freshHOP: [
        {
          freshHOPDay: '',
          freshHOPText: '',
          freshHOPStartTime: '',
          freshHOPEndTime: '',
        }
      ],
      freshEmail: [
        {
          freshemailText: "",
          freshEmailType: "",
        }
      ],
      freshFirstName: "",
      freshLastName: "",
      freshPosition: "",
      freshPersonEmail: "",
      freshSpecialities: '',
      freshHistory: '',
      freshBusinessOwner: "",
      isToggleFreshPayment: false,
      freshClonePaymentSwitch: {}, // payment
      freshPaymentSwitch: {},
      freshSelectedPayment: [],
      freshCategoryList: [],
      freshSelectAdditionalInfo: [],


    };
    this.filterList = this.filterList.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAllChecked = this.handleAllChecked.bind(this);
    this.handleAllCheckedAdditional = this.handleAllCheckedAdditional.bind(this);
    this.handleAllCheckedAddress = this.handleAllCheckedAddress.bind(this);
    this.handleChangedBranches = this.handleChangedBranches.bind(this);
    this.handleOnFileUploadChange = this.handleOnFileUploadChange.bind(this);
    this.handleAllCheckedBranch = this.handleAllCheckedBranch.bind(this);
    this.handleAllPhoneChecked = this.handleAllPhoneChecked.bind(this);
    this.handleCheckChieldPhone = this.handleCheckChieldPhone.bind(this);
    this.handleAllEmailChecked = this.handleAllEmailChecked.bind(this);
    this.handleCheckChieldEmail = this.handleCheckChieldEmail.bind(this);
    this.handleAllWebsiteChecked = this.handleAllWebsiteChecked.bind(this);
    this.handleCheckChieldWebsite = this.handleCheckChieldWebsite.bind(this);
    this.handleAllPaymentChecked = this.handleAllPaymentChecked.bind(this);
    this.handleCheckChieldPayment = this.handleCheckChieldPayment.bind(this);
    this.handleAllCategoryChecked = this.handleAllCategoryChecked.bind(this);
    this.handleCheckChieldCategory = this.handleCheckChieldCategory.bind(this);
    this.handleAllhoursChecked = this.handleAllhoursChecked.bind(this);
    this.handleCheckChieldHours = this.handleCheckChieldHours.bind(this);
    this.handleAllActionChecked = this.handleAllActionChecked.bind(this);
    this.handleCheckChieldAction = this.handleCheckChieldAction.bind(this);
    this.handleCheckChieldAbout = this.handleCheckChieldAbout.bind(this);
    this.handleAllOfferChecked = this.handleAllOfferChecked.bind(this);
    this.handleCheckChieldOffers = this.handleCheckChieldOffers.bind(this);
    this.handleAllCheckedBussiness = this.handleAllCheckedBussiness.bind(this);
    this.handleAllListingChecked = this.handleAllListingChecked.bind(this);
    this.handleAllOwnerChecked = this.handleAllOwnerChecked.bind(this);
    this.handleAllCheckedBusinessAll = this.handleAllCheckedBusinessAll.bind(this);
    this.setupCall = this.setupCall.bind(this);
    this.getStarted = this.getStarted.bind(this);

    this.gridX = props.gridX || 1;
    this.gridY = props.gridY || 1;
    this.onMouseDown = this.onMouseDown.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.onMouseUp = this.onMouseUp.bind(this);
    this.onTouchStart = this.onTouchStart.bind(this);
    this.onTouchMove = this.onTouchMove.bind(this);
    this.onTouchEnd = this.onTouchEnd.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { ratings, received_additional_info } = this.state;
    if (nextProps.corporate_details) {
      let data = nextProps.corporate_details;
      this.setState({
        corporateDetails: data,
        listingImage: data.listing_logo_pic && data.listing_logo_pic[0] && data.listing_logo_pic[0].location,
        listing_name: data.name,
        listingOwnerGuide: data.listing_owner_guide
      });
    }

    if (nextProps.profileData) {
      let profileData = nextProps.profileData;
      let userData = profileData.user;

      if (userData) {
        this.setState({
          profileData,
          fullName: (profileData.user && profileData.user.first_name) + " " + (profileData.user && profileData.user.last_name),
          userEmail: profileData.user.email,
          profilePic: profileData.current_profile_file,
          fName: profileData.user.first_name,
          lName: profileData.user.last_name,
          position: profileData?.professional[0]?.profession.title,
          userID: profileData?.professional[0]?.profession.id
        });
        this.setState({
          ownerProfile: profileData.current_profile_file ?
            profileData.current_profile_file : "",
          ownerEmail: profileData.user.email ?
            profileData.user.email : "",
          ownerPosition: profileData?.professional[0]?.profession.title ?
            profileData?.professional[0]?.profession.title : "",
        })
      }
    }
    //get all branch copy
    if (
      nextProps.getbranch_copy &&
      Object.keys(nextProps.getbranch_copy).length > 0
    ) {
      if (this.state.country == "") {
        this.setState({
          getallBranchCopy: nextProps.getbranch_copy,
          branches: nextProps.getbranch_copy.result,
          newBranches: nextProps.getbranch_copy.result,
          pendingBranches: nextProps.getbranch_copy.result,
          withoutfilterBranches: nextProps.getbranch_copy.result,
          countryList: nextProps.getbranch_copy.country_list,
          cState: nextProps.getbranch_copy.c_states,
          countryState: nextProps.getbranch_copy.country_state,
        });
        for (var i = 0; i < nextProps && nextProps.getbranch_copy && nextProps.getbranch_copy.result.length; i++) {
          this.state.rowState[i] = false;
        }
      }
    }

    //get all branch copy
    if (
      nextProps.getbranch_data &&
      Object.keys(nextProps.getbranch_data).length > 0
    ) {
      if (nextProps.branch_corporate_call_list &&
        nextProps.branch_corporate_call_list[0] &&
        Array.isArray(nextProps.branch_corporate_call_list[0]) &&
        nextProps.branch_corporate_call_list[0].length > 0
      ) {
        Object.keys(nextProps.branch_corporate_call_list[0]).forEach(
          (key) => {
            if (key != 'code') {
              nextProps.branch_corporate_call_list[0][key].isChecked = false;
            }
          }
        );
        let sortedList = nextProps.branch_corporate_call_list && nextProps.branch_corporate_call_list[0].sort((a, b) => b.id - a.id)

        this.setState({ corporateCallList: sortedList });
      }
    }

    //get admin stats
    if (nextProps.admin_stats) {
      let items = nextProps.admin_stats;
      let dataItems = {
        overall: items['overall'],
        review_count: items["review count"],
        dispute_review: items["respond to dispute review"],
        qa: items["respond to qa"],
        feedback: items["respond to feedback"],
        message: items["respond to message"],
      };


      nextProps.admin_stats.ratings && Array.isArray(nextProps.admin_stats.ratings) && nextProps.admin_stats.ratings.length > 0 && nextProps.admin_stats.ratings.map((rating) => {
        // let str = ''
        switch (rating.key) {
          case 'AssortmentofVehicles':
            rating.key = 'Assortment of Vehicles';
            break;
          case 'BathroomCleanliness':
            rating.key = 'Bathroom Cleanliness';
            break;
          case 'FoodQuality':
            rating.key = 'Food Quality';
            break;
          case 'PromptnessinServingMeal':
            rating.key = 'Promptness in ServingMeal';
            break;
          case 'QualityofAutomobiles':
            rating.key = 'Quality of Automobiles';
            break;
          case 'ReasonablyPriced':
            rating.key = 'Reasonably Priced';
            break;
          case 'RestaurantCleanliness':
            rating.key = 'Restaurant Cleanliness';
            break;
          case 'TurnaroundTime':
            rating.key = 'Turnaround Time';
            break;
          case 'WillingnesstoNegotiate':
            rating.key = 'Willingness to Negotiate';
            break;
          case 'WillingnesstoNegotiate':
            rating.key = 'Willingness to Negotiate';
            break;
          case 'AbilitytoObtainLoanForYou':
            rating.key = 'Ability to Obtain Loan For You';
            break;
          case 'avg_rating':
            rating.key = 'Avg Rating';
            break;
          case 'EthicallyOperateInRecession':
            rating.key = 'Ethically Operate In Recession';
            break;
          case 'LoanApprovalProcess':
            rating.key = 'Loan Approval Process';
            break;
          case 'Professionalism':
            rating.key = 'Professionalism';
            break;
          case 'QualityofLoan':
            rating.key = 'Quality of Loan';
            break;
          case 'Trustworthy':
            rating.key = 'Trustworthy';
            break;
          case 'TurnaroundTime':
            rating.key = 'Turn Around Time';
            break;
          case 'CleanStore':
            rating.key = 'Clean Store';
            break;
          case 'KnowledgeableStaff':
            rating.key = 'Knowledgeable Staff';
            break;
          case 'ProductAssortment':
            rating.key = 'Product Assortment';
            break;
          case 'ProductQuality':
            rating.key = 'Product Quality';
            break;
          default:
            break;

        }
      })

      this.setState({
        topBarStats: dataItems,
        getAdminStats: nextProps.admin_stats,
        ratings: nextProps.admin_stats.ratings,
      });

    }

    if (nextProps.additional_data && nextProps.additional_data.specialties) {
      let NewData = nextProps.additional_data.specialties;

      this.setState({
        editSpec: true,
        specialities: NewData,
      },
        () => {
          this.bizpecialities();
        }
      );
    }

    if (nextProps.additional_data && nextProps.additional_data.business_owner_history) {
      this.setState({
        editHisSpec: true,
        businessOwnerhistory: nextProps.additional_data.business_owner_history,
      },
        () => {
          this.Ownerhistory();
        }
      );
    }

    if (nextProps.additional_data && nextProps.additional_data.meet_the_business_owner) {
      this.setState({
        editBusiness: true,
        meetOwner: nextProps.additional_data.meet_the_business_owner,
      },
        () => {
          this.ownerMeet();
        }
      );
    }
    if (
      Array.isArray(nextProps.biz_callon_type) &&
      nextProps.biz_callon_type.length > 0
    ) {
      this.setState({ bizCallonType: nextProps.biz_callon_type });
    }

    if (nextProps.corporate_copy_details) {


      let HoursOfOperation = nextProps.corporate_copy_details.hoursofoperation_set
      let setDaysOfWeek = 1;
      HoursOfOperation = HoursOfOperation || [];
      let filteredData = HoursOfOperation.sort((a, b) => {
        return (a.id < b.id) ? -1 : (a.id > b.id) ? 1 : 0;
      });

      if (filteredData.length > 0) {
        let lastItem = filteredData[filteredData.length - 1];
        if (lastItem.day_of_week == '7') {
          setDaysOfWeek = 1;
        } else {
          setDaysOfWeek = (lastItem.day_of_week + 1);
        }
      }

      if (nextProps && nextProps?.corporate_copy_details && nextProps.corporate_copy_details?.additional_info) {
        let item = nextProps.corporate_copy_details.additional_info;
        let result = this.sortObject(item);
        let newMergeresult = result.concat(nextProps?.corporate_copy_details?.all_additional_fields)
        newMergeresult.forEach((nitem, index) => {
          if (nitem) { newMergeresult[index]["uniquId"] = index; }
        });
        this.setState({
          additionInfo: newMergeresult,
          existing_info_count: result.length,
          corporateId: nextProps.corporate_copy_details.id,
          all_additional_fields: nextProps?.corporate_copy_details?.all_additional_fields,
        })
      }

      // Getting all additional info - all categories
      if (nextProps && nextProps.corporate_copy_details && nextProps.corporate_copy_details?.taxonomy_dict) {
        let item = nextProps.corporate_copy_details.taxonomy_dict
        let result = this.sortObject(item);

        let res_data = []
        /*if (result.length > 0 && !received_additional_info) {
          result.map((res) => {
            callApi(
              `/api/additional-attributes-by-id/?category=${res.value.id}&userentry=${nextProps?.corporate_copy_details?.id}`,
              "GET",
            ).then((response) => {
              if (response && response.code === 200) {
                let resp = response
                if (resp.length > 1) {
                  resp.map((r) => {
                    res_data.push(r)
                    let additional_info_all_categories = [...this.state.additional_info_all_categories];

                    // Add item to it
                    additional_info_all_categories.push(r);

                    // Set state
                    this.setState({ additional_info_all_categories });
                  })
                }
              }
            })
          })
          this.setState({ received_additional_info: true })
        }*/
      }

      let data = {}

      if (nextProps && nextProps.corporate_copy_details && nextProps.corporate_copy_details?.all_additional_fields) {
        let items = nextProps.corporate_copy_details.all_additional_fields;
        let result = this.sortData(items)

        this.setState({ allAdditionalInfo: result })

        nextProps.corporate_copy_details &&
          Array.isArray(nextProps.corporate_copy_details.additional_info_map) &&
          nextProps.corporate_copy_details.additional_info_map.length > 0 &&
          nextProps.corporate_copy_details.additional_info_map.map((dt) => {
            data[dt.name] = dt.id
          });
        this.setState({ add_info_map: data })
      }
      if (nextProps && nextProps.sub_category_data) {

        this.setState({ sub_category_list: nextProps.sub_category_data })

        // map data for already saved additional info
        let final_list = []
        nextProps && nextProps.sub_category_data.map((data) => {
          this.state.additionInfo && this.state.additionInfo.map((add_data) => {
            if (data.label === add_data.name) {
              add_data && add_data.value.map((val) => {
                if (val && val.id === data.id) {
                  let found = 0
                  final_list && final_list.map((lst) => {
                    if (lst && lst.name === data.label) {
                      lst['value'].push(val)
                      found = 1
                    }
                  })
                  if (found === 0) {
                    let dict = { 'name': data.label, 'value': [val] }
                    final_list.push(dict)
                  }
                }
              })
            }
          })
        })
        this.setState({ saved_amenities_list: final_list })

      }

      if (nextProps && nextProps.amenities_data) {
        this.setState({
          amenities_list: nextProps.amenities_data,
          CategoryId: nextProps.amenities_data &&
            nextProps.amenities_data[0] &&
            nextProps.amenities_data[0].reviews_additionalattributes
        })

      }

      if (HoursOfOperation && Array.isArray(HoursOfOperation) && HoursOfOperation.length > 0) {
        let lastElement = HoursOfOperation[HoursOfOperation.length - 1];
        this.setState({ addHours: { day_of_week: setDaysOfWeek, end_time: lastElement.end_time, start_time: lastElement.start_time, entries: "", id: "", info: "" } })
      }

      if (
        nextProps.corporate_copy_details &&
        nextProps.corporate_copy_details.phone_set &&
        Object.keys(nextProps.corporate_copy_details.phone_set).length > 0
      ) {
        Object.keys(nextProps.corporate_copy_details.phone_set).forEach(
          (key) => {
            nextProps.corporate_copy_details.phone_set[key].isChecked = false;
          }
        );
      }
      if (
        nextProps.corporate_copy_details.address &&
        Object.keys(nextProps.corporate_copy_details.address).length > 0
      ) {
        Object.keys(nextProps.corporate_copy_details.address).forEach(
          (key) => {
            nextProps.corporate_copy_details.address[key].isChecked = false;
          }
        );
      }

      if (nextProps.corporate_copy_details && nextProps.corporate_copy_details?.additional_info) {

        this.setState({
          specialities: nextProps.corporate_copy_details?.additional_info.specialties,
          meetOwner: nextProps.corporate_copy_details?.additional_info.meet_the_business_owner,
          businessOwnerhistory: nextProps.corporate_copy_details?.additional_info.business_owner_history,

        });

        if (this.state.specialities === null) {
          this.setState({ isToggleBusiness: true })
        }
        if (this.state.meetOwner === null) {
          this.setState({ isToggleMeet: true })
        }
        if (this.state.businessOwnerhistory === null) {
          this.setState({ isToggleHistory: true })
        }

      }

      if (
        nextProps.corporate_copy_details.email_set &&
        Object.keys(nextProps.corporate_copy_details.email_set).length > 0
      ) {
        Object.keys(nextProps.corporate_copy_details.email_set).forEach(
          (key) => {
            nextProps.corporate_copy_details.email_set[key].isChecked = false;
          }
        );
      }
      if (
        nextProps.corporate_copy_details.paymentoptions_set &&
        Object.keys(nextProps.corporate_copy_details.paymentoptions_set).length > 0
      ) {
        Object.keys(nextProps.corporate_copy_details.paymentoptions_set).forEach(
          (key) => {
            nextProps.corporate_copy_details.paymentoptions_set[key].isChecked = false;
          }
        );
      }
      if (
        nextProps.corporate_copy_details.taxonomy_dict &&
        Object.keys(nextProps.corporate_copy_details.taxonomy_dict).length > 0
      ) {

        Object.keys(nextProps?.corporate_copy_details?.taxonomy_dict).forEach(
          (key) => {
            nextProps.corporate_copy_details.taxonomy_dict[key].isChecked = false;
          }
        );
      }
      if (
        filteredData &&
        Object.keys(filteredData).length > 0
      ) {
        Object.keys(filteredData).forEach(
          (key) => {
            filteredData[key].isChecked = false;
          }
        );
      }

      if (
        nextProps.corporate_copy_details.specialoffer_set &&
        Object.keys(nextProps.corporate_copy_details.specialoffer_set).length > 0
      ) {
        Object.keys(nextProps.corporate_copy_details.specialoffer_set).forEach(
          (key) => {
            nextProps.corporate_copy_details.specialoffer_set[key].isChecked = false;
          }
        );
      }

      if (
        nextProps.corporate_copy_details.additional_info &&
        nextProps.corporate_copy_details.additional_info.specialties &&
        Object.keys(nextProps.corporate_copy_details.additional_info.specialties).length > 0
      ) {
        Object.keys(nextProps.corporate_copy_details.additional_info.specialties).forEach(
          (key) => {
            nextProps.corporate_copy_details.additional_info.specialties[key].isChecked = false;
          }
        );
      }
      if (
        nextProps.corporate_copy_details &&
        nextProps.corporate_copy_details.additional_info &&
        nextProps.corporate_copy_details.additional_info.meet_the_business_owner &&
        Object.keys(nextProps.corporate_copy_details.additional_info.meet_the_business_owner).length > 0
      ) {
        Object.keys(nextProps.corporate_copy_details.additional_info.meet_the_business_owner).forEach(
          (key) => {
            nextProps.corporate_copy_details.additional_info.meet_the_business_owner[key].isChecked = false;
          }
        );
      }
      if (
        nextProps.corporate_copy_details.additional_info &&
        nextProps.corporate_copy_details.additional_info.business_owner_history &&
        Object.keys(nextProps.corporate_copy_details.additional_info.business_owner_history).length > 0
      ) {
        Object.keys(nextProps.corporate_copy_details.additional_info.business_owner_history).forEach(
          (key) => {
            nextProps.corporate_copy_details.additional_info.business_owner_history[key].isChecked = false;
          }
        );
      }
      this.setState({
        getBranchData: nextProps.corporate_copy_details.name,
        specialities: nextProps.corporate_copy_details.additional_info ? nextProps.corporate_copy_details.additional_info.specialties : [],
        meetOwner: nextProps.corporate_copy_details?.additional_info ? nextProps.corporate_copy_details.additional_info.meet_the_business_owner : [],
        businessOwnerhistory: nextProps.corporate_copy_details?.additional_info ? nextProps.corporate_copy_details.additional_info.business_owner_history : [],
        phoneDiary: nextProps.corporate_copy_details?.phone_set,
        AdressDiary: nextProps.corporate_copy_details?.address,
        webSet: nextProps.corporate_copy_details?.website_set,
        emailSet: nextProps.corporate_copy_details?.email_set,
        payOptions: nextProps.corporate_copy_details?.paymentoptions_set,
        //categoryList: nextProps.corporate_copy_details.taxonomy_list,
        categoryList: nextProps.corporate_copy_details?.taxonomy_dict,
        HoursOfOperation: filteredData,
        specialofferSet: nextProps.corporate_copy_details?.specialoffer_set,
        // MediaData: nextProps.corporate_copy_details?.media_dict ? nextProps.corporate_copy_details.media_dict[0] : [],
        branch_logo_pic: nextProps.corporate_copy_details?.listing_logo_pic ? nextProps.corporate_copy_details.listing_logo_pic[0] : [],
        branch_profile_pic: nextProps.corporate_copy_details?.listing_profileimage ? nextProps.corporate_copy_details.listing_profileimage[0] : [],
        profileSet: nextProps.corporate_copy_details?.profile_set,
        professionSet: nextProps.corporate_copy_details?.profession_set,
      }, () => {
        let paymentSwitch = this.state.paymentSwitch;
        this.state.payOptions && this.state.payOptions.forEach(item => {

          if (item.paymentoptions == "Apple Pay") {
            paymentSwitch['apple'] = true
          }

          if (item.paymentoptions == "Mastercard") {
            paymentSwitch['mastercard'] = true
          }

          if (item.paymentoptions == "American Express") {
            paymentSwitch['american'] = true
          }

          if (item.paymentoptions == "Cash") {
            paymentSwitch['cash'] = true
          }

          if (item.paymentoptions == "Check") {
            paymentSwitch['check'] = true
          }

          if (item.paymentoptions == "Discover") {
            paymentSwitch['discover'] = true
          }

          if (item.paymentoptions == "Visa") {
            paymentSwitch['visa'] = true
          }

          if (item.paymentoptions == "Google Wallet") {
            paymentSwitch['google'] = true
          }

          if (item.paymentoptions == "Cryptocurrency") {
            paymentSwitch['cryptocurrency'] = true
          }

          if (item.paymentoptions == "Debit Card") {
            paymentSwitch['debit'] = true
          }
        })
        this.setState({ paymentSwitch, clonePaymentSwitch: paymentSwitch })

      });
    }
    if (nextProps.business_category) {
      this.setState({
        businessCategory: nextProps.business_category
      })
    }

    if (nextProps.business_sub_category) {
      let fetchSubCategories = nextProps.business_sub_category;
      let { anotherCategories, businessSubCategory, subCategoryZero } = this.state;
      if (subCategoryZero && subCategoryZero == "zero") {
        businessSubCategory[0] = fetchSubCategories
      } else {
        if (anotherCategories.length > 0) {
          businessSubCategory[anotherCategories.length] = fetchSubCategories
        } else {
          businessSubCategory[0] = fetchSubCategories
        }
      }
      this.setState({ businessSubCategory })
    }

  }

  componentDidUpdate(prevProps) {
    if (
      prevProps?.branch_corporate_call_list !==
      this.props?.branch_corporate_call_list
    ) {
      if (this.props.branch_corporate_call_list &&
        this.props.branch_corporate_call_list[0] &&
        Array.isArray(this.props.branch_corporate_call_list[0]) &&
        this.props.branch_corporate_call_list[0].length > 0
      ) {
        Object.keys(this.props.branch_corporate_call_list[0]).forEach(
          (key) => {
            if (key != 'code') {
              this.props.branch_corporate_call_list[0][key].isChecked = false;
            }
          }
        );
        let sortedList = this.props.branch_corporate_call_list && this.props.branch_corporate_call_list[0].sort((a, b) => b.id - a.id)
        this.setState({
          corporateCallList: sortedList
        });
      }
    }

    if (prevProps.profileData !== this.props.profileData) {
      let profileData = this.props.profileData;
      let userData = profileData.user;

      if (userData) {
        this.setState({
          profileData,
          fullName: (profileData.user && profileData.user.first_name) + " " + (profileData.user && profileData.user.last_name),
          userEmail: profileData.user.email,
          profilePic: profileData.current_profile_file,
          fName: profileData.user.first_name,
          lName: profileData.user.last_name,
          position: profileData?.professional[0]?.profession.title,
          userID: profileData?.professional[0]?.profession.id
        });
        this.setState({
          ownerProfile: profileData.current_profile_file ?
            profileData.current_profile_file : "",
          ownerEmail: profileData.user.email ?
            profileData.user.email : "",
          ownerPosition: profileData?.professional[0]?.profession.title ?
            profileData?.professional[0]?.profession.title : "",
        })
      }
    }

  }

  listingOwnerGuideModalToggle = () => {
    this.setState({
      listingOwnerGuideModal: !this.state.listingOwnerGuideModal,
    });
  };

  handleAllCheckedBussiness = (event, checkType) => {
    this.setState({ checkAllBusinessInfo: !this.state.checkAllBusinessInfo }, () => {
      this.handleAllPhoneChecked(null, !checkType);
      this.handleAllEmailChecked(null, !checkType);
      this.handleAllWebsiteChecked(null, !checkType);
      this.handleAllhoursChecked(null, !checkType);
      this.handleAllPaymentChecked(null, !checkType);
      this.handleAllCategoryChecked(null, !checkType);
      this.handleAllCheckedAddress(null, !checkType);
    });
  }

  // Phone checked
  handleAllPhoneChecked = (event, checkType) => {
    let { selectedPhone } = this.state;
    let phoneDiary = this.state.phoneDiary
    phoneDiary.forEach((phoneD, index) => {
      phoneD.isChecked = event != null ? event.target.checked : checkType;

      if ((event?.target?.checked == true) || checkType == true) {
        let el = this.state.selectedPhone.find((itm) => itm === phoneD.id);
        if (el)
          this.state.selectedPhone.splice(
            this.state.selectedPhone.indexOf(el),
            1
          );
        selectedPhone.push(phoneD.id)
        this.setState({ selectallPhone: true })
      } else {
        let el = this.state.selectedPhone.find((itm) => itm === phoneD.id);
        if (el)
          this.state.selectedPhone.splice(
            this.state.selectedPhone.indexOf(el),
            1
          );
        this.setState({ selectallPhone: false })
      }
    }
    )
    this.setState(phoneDiary)
  }

  handleCheckChieldPhone = (event) => {
    let phoneDiary = this.state.phoneDiary
    let count = phoneDiary.length
    phoneDiary.forEach(phoneD => {
      if (phoneD.id === parseInt(event.target.value)) {
        phoneD.isChecked = event.target.checked
        if (event?.target?.checked == true) {
          this.state.selectedPhone.push(phoneD.id)
        } else {
          let el = this.state.selectedPhone.find(
            (itm) => itm === parseInt(event.target.value)
          );
          if (el)
            this.state.selectedPhone.splice(
              this.state.selectedPhone.indexOf(el),
              1
            );
          this.setState({ selectallPhone: false })
        }
      }
    })

    if (count == this.state.selectedPhone.length) {
      this.setState({ selectallPhone: true })
    } else {
      this.setState({ selectallPhone: false })
    }
    this.setState({ phoneDiary: phoneDiary })
  }

  // Business Owner checked
  handleAllOwnerChecked = (event) => {
    let { selectedBusiness } = this.state;
    if (event.target.checked) {
      selectedBusiness.email = this.state.ownerEmail;
      selectedBusiness.position = this.state.ownerPosition;
      selectedBusiness.profile = this.state.ownerProfile;
      selectedBusiness.fullName = this.state.fullName;
      this.setState({ checked: true, isCheckedFullName: true, isCheckedProfile: true, isCheckedEmail: true, isCheckedPosition: true, selectedBusiness, isCheckedAllOwner: true });
    } else {
      selectedBusiness.email = "";
      selectedBusiness.position = "";
      selectedBusiness.profile = "";
      selectedBusiness.fullName = "";
      this.setState({ checked: false, isCheckedFullName: false, isCheckedProfile: false, isCheckedEmail: false, isCheckedPosition: false, selectedBusiness, isCheckedAllOwner: false });
    }
  }

  handleAllOwnerAnthorChecked = (checktype) => {
    let { selectedBusiness } = this.state;
    if (checktype) {
      selectedBusiness.email = this.state.ownerEmail;
      selectedBusiness.position = this.state.ownerPosition;
      selectedBusiness.profile = this.state.ownerProfile;
      selectedBusiness.fullName = this.state.fullName;
      this.setState({ checked: true, isCheckedFullName: true, isCheckedProfile: true, isCheckedEmail: true, isCheckedPosition: true, selectedBusiness, isCheckedAllOwner: true });
    } else {
      selectedBusiness.email = "";
      selectedBusiness.position = "";
      selectedBusiness.profile = "";
      selectedBusiness.fullName = "";
      this.setState({ checked: false, isCheckedFullName: false, isCheckedProfile: false, isCheckedEmail: false, isCheckedPosition: false, selectedBusiness, isCheckedAllOwner: false });
    }


  }

  handleAllCheckedBusinessAll = (checkType) => {
    let specialities = this.state.specialities
    let meetOwner = this.state.meetOwner
    let businessOwnerhistory = this.state.businessOwnerhistory
    const { selectedAbout } = this.state;
    let aboutInfo = {}
    if (specialities && Array.isArray(specialities) && specialities.length > 0 && specialities[0].value !== "") {
      specialities[0].isChecked = !this.state.ischeckedSpecial
      if (!this.state.ischeckedSpecial == true) {
        aboutInfo.specialities = specialities[0].value
        selectedAbout.push(aboutInfo);
      } else {
        let el = this.state.selectedAbout.find((itm) => itm.specialities === specialities[0].value);
        if (el)
          this.state.selectedAbout.splice(
            this.state.selectedAbout.indexOf(el),
            1
          );
      }

    }
    if (meetOwner && Array.isArray(meetOwner) && meetOwner.length > 0 && meetOwner[0].value.bio !== "") {
      meetOwner[0].isChecked = !this.state.ischeckedSpecial
      if (!this.state.ischeckedSpecial == true) {
        aboutInfo.meetOwner = meetOwner[0].value.bio
        selectedAbout.push(aboutInfo);
      } else {
        let el = this.state.selectedAbout.find((itm) => itm.meetOwner === meetOwner[0].value.bio);
        if (el)
          this.state.selectedAbout.splice(
            this.state.selectedAbout.indexOf(el),
            1
          );
      }

    }
    if (businessOwnerhistory && Array.isArray(businessOwnerhistory) && businessOwnerhistory.length > 0 && businessOwnerhistory[0].value.history !== "") {
      businessOwnerhistory[0].isChecked = !this.state.ischeckedSpecial
      if (!this.state.ischeckedSpecial == true) {
        aboutInfo.businessOwnerhistory = businessOwnerhistory[0].value.history
        selectedAbout.push(aboutInfo);
      } else {
        let el = this.state.selectedAbout.find((itm) => itm.businessOwnerhistory === businessOwnerhistory[0].value.history);
        if (el)
          this.state.selectedAbout.splice(
            this.state.selectedAbout.indexOf(el),
            1
          );
      }
    }
    this.setState({ checkAllBusinessAll: !this.state.checkAllBusinessAll, ischeckedSpecial: !this.state.ischeckedSpecial, ischeckedHistory: !this.state.ischeckedHistory, ischeckedMBO: !this.state.ischeckedMBO, isCheckedAllOwner: this.state.ischeckedSpecial, businessOwnerhistory: businessOwnerhistory, meetOwner: meetOwner, specialities: specialities }, () => {
      this.handleAllOwnerAnthorChecked(!checkType);
    });

  }

  // Email checked
  handleAllEmailChecked = (event, checkType) => {
    let emailSet = this.state.emailSet
    let { selectedEmail } = this.state;
    emailSet.forEach((emailSe, index) => {
      emailSe.isChecked = event != null ? event.target.checked : checkType;
      if ((event?.target?.checked == true) || checkType == true) {
        let el = this.state.selectedEmail.find((itm) => itm === emailSe.id);
        if (el)
          this.state.selectedEmail.splice(
            this.state.selectedEmail.indexOf(el),
            1
          );
        selectedEmail.push(emailSe.id)
        this.setState({ selectallEmails: true })
      } else {
        let el = this.state.selectedEmail.find((itm) => itm === emailSe.id);
        if (el)
          this.state.selectedEmail.splice(
            this.state.selectedEmail.indexOf(el),
            1
          );
        this.setState({ selectallEmails: false })
      }
    }
    )
    this.setState(emailSet)
  }

  handlelistingOwnerGuideDataView = () => {
    let faq = this.state.listingOwnerGuide &&
      this.state.listingOwnerGuide.filter((element) => {
        return element.section === "Frequently Asked Questions"
      })
    let otherThanFAQ = this.state.listingOwnerGuide &&
      this.state.listingOwnerGuide.filter((element) => {
        return element.section !== "Frequently Asked Questions"
      })
    return (
      <>
        {otherThanFAQ &&
          Array.isArray(otherThanFAQ) &&
          otherThanFAQ.length > 0 &&
          otherThanFAQ.map((element, index) => {
            return <CollapseBasic
              title={element.section}
              isOpen={false}
              size="sm"
              key={index}
            >
              {
                this.state.nonFaqEditClick ?
                  <>
                    <textarea
                      className="form-control text-secondary-dark ff-alt"
                      // defaultValue={element.content}
                      rows="4"
                      value={element.content}
                      id={element.id}
                      onChange={this.handleListingUpdate}
                    >

                    </textarea>
                    <div className="mt-2 text-right">
                      <Button
                        color="tertiary"
                        size="sm"
                        title="Save"
                        onClick={async () => {
                          this.handleSubmitListingGuideUpdate(element.id, element.content)
                          await this.setState({
                            nonFaqEditClick: false,
                            nonFaqEditID: '',
                          });
                        }}
                      >
                        <FontAwesomeIcon icon="check" fixedWidth />
                      </Button>
                      <Button
                        className="ml-2"
                        color="tertiary"
                        size="sm"
                        title="Cancel"
                        onClick={() => {
                          this.setState({
                            nonFaqEditClick: false,
                            nonFaqEditID: '',
                          });
                        }}
                      >
                        <FontAwesomeIcon icon="times" fixedWidth />
                      </Button>
                    </div>
                  </>
                  :
                  <p className="hover-blue" title="Click to Edit"
                    onClick={() => {
                      this.setState({
                        nonFaqEditClick: true,
                        nonFaqEditID: element.id,
                      });
                    }}
                  >{element.content}</p>
              }
            </CollapseBasic>

          })}
        <CollapseBasic
          title='Frequently Asked Questions'
          isOpen={false}
          size="sm"
        >
          {
            faq &&
            Array.isArray(faq) &&
            faq.length > 0 &&
            faq.map((element, index) => {
              return (
                <div className="mb-2" key={index}>
                  {
                    false ?
                      <>
                        <div className="d-flex">
                          <div className="py-2 mr-1 fs-16 ff-base font-weight-bold">{index + 1 + ') '}</div>
                          <div className="flex-grow-1">
                            <textarea className="form-control text-secondary-dark ff-base font-weight-bold fs-16" defaultValue={element.sub_section} style={{ lineHeight: '1.2' }} ></textarea>
                          </div>
                        </div>

                        <div className="mt-2 text-right">
                          <Button color="tertiary" size="sm" title="Save">
                            <FontAwesomeIcon icon="check" fixedWidth />
                          </Button>
                          <Button className="ml-2" color="tertiary" size="sm" title="Cancel">
                            <FontAwesomeIcon icon="times" fixedWidth />
                          </Button>
                        </div>
                      </>
                      :
                      <div className="fs-16 ff-base font-weight-bold">{index + 1}{') '} {element.sub_section}</div>
                  }
                  <div>
                    {
                      this.state.faqEditClick &&
                        this.state.faqEditID === element.id
                        ?
                        <>
                          <textarea
                            className="form-control text-secondary-dark ff-alt"
                            value={element.content}
                            id={element.id}
                            onChange={this.handleListingUpdate}
                            rows="4" >
                          </textarea>
                          <div className="mt-2 text-right">
                            <Button
                              color="tertiary"
                              size="sm"
                              title="Save"
                              onClick={async () => {
                                this.handleSubmitListingGuideUpdate(element.id, element.content)
                                await this.setState({
                                  faqEditClick: false,
                                  faqEditID: '',
                                });
                              }}
                            >
                              <FontAwesomeIcon icon="check" fixedWidth />
                            </Button>
                            <Button
                              className="ml-2"
                              color="tertiary"
                              size="sm"
                              title="Cancel"
                              onClick={async () => {

                                await this.setState({
                                  faqEditClick: false,
                                  faqEditID: '',
                                });
                              }}
                            >
                              <FontAwesomeIcon icon="times" fixedWidth />
                            </Button>
                          </div>
                        </>
                        :
                        <p
                          className="hover-blue"
                          title="Click to Edit"
                          onClick={() => {
                            this.setState({
                              faqEditClick: true,
                              faqEditID: element.id
                            });
                          }}
                        >{element.content}</p>
                    }
                  </div>
                </div>
              )
            })}
        </CollapseBasic>
      </>
    )

  }

  handleListingUpdate = async (evt) => {
    const value = evt.target.value;
    const id = evt.target.id;
    const elementsIndex = this.state.listingOwnerGuide.findIndex(element => element.id == id)
    let newArray = [...this.state.listingOwnerGuide]

    newArray[elementsIndex] = {
      ...newArray[elementsIndex],
      content: value
    }
    await this.setState({
      listingOwnerGuide: newArray
    });
  }

  handleSubmitListingGuideUpdate = (id, value) => {
    const { update_listing_owner_guide } = this.props;
    const corporateID = this.props.corporate_id?.id
    const data = {
      "guide_id": id,
      "value": value,
      "update_listing_owner_guide": true
    }

    update_listing_owner_guide(data, corporateID)
  }

  handleCheckChieldEmail = (event) => {
    let emailSet = this.state.emailSet
    let count = emailSet.length
    emailSet.forEach(emailSe => {
      if (emailSe.id === parseInt(event.target.value)) {
        emailSe.isChecked = event.target.checked
        if (event?.target?.checked == true) {
          this.state.selectedEmail.push(emailSe.id)
        } else {
          let el = this.state.selectedEmail.find(
            (itm) => itm === parseInt(event.target.value)
          );
          if (el)
            this.state.selectedEmail.splice(
              this.state.selectedEmail.indexOf(el),
              1
            );
          this.setState({ selectallEmails: false })
        }
      }
    })

    if (count == this.state.selectedEmail.length) {
      this.setState({ selectallEmails: true })
    } else {
      this.setState({ selectallEmails: false })
    }
    this.setState({ emailSet: emailSet })

  }

  // Website checked
  handleAllWebsiteChecked = (event, checkType) => {
    let { selectedWebsite } = this.state;
    let webSet = this.state.webSet
    webSet.forEach((webSe, index) => {
      webSe.isChecked = event != null ? event.target.checked : checkType;
      if ((event?.target?.checked == true) || checkType == true) {
        let el = this.state.selectedWebsite.find((itm) => itm === webSe.id);
        if (el)
          this.state.selectedWebsite.splice(
            this.state.selectedWebsite.indexOf(el),
            1
          );
        selectedWebsite.push(webSe.id)
        this.setState({ selectallWebsites: true })
      } else {
        let el = this.state.selectedWebsite.find((itm) => itm === webSe.id);
        if (el)
          this.state.selectedWebsite.splice(
            this.state.selectedWebsite.indexOf(el),
            1
          );
        this.setState({ selectallWebsites: false })
      }
    }
    )
    this.setState(webSet)
  }

  handleCheckChieldWebsite = (event) => {
    let webSet = this.state.webSet
    let count = webSet.length;
    webSet.forEach(webSe => {
      if (webSe.id === parseInt(event.target.value)) {
        webSe.isChecked = event.target.checked
        if (event?.target?.checked == true) {
          this.state.selectedWebsite.push(webSe.id)

        } else {
          let el = this.state.selectedWebsite.find(
            (itm) => itm === parseInt(event.target.value)
          );
          if (el)
            this.state.selectedWebsite.splice(
              this.state.selectedWebsite.indexOf(el),
              1
            );
          this.setState({ selectallWebsites: false })
        }
      }
    })

    if (count == this.state.selectedWebsite.length) {
      this.setState({ selectallWebsites: true })
    } else {
      this.setState({ selectallWebsites: false })
    }
    this.setState({ webSet: webSet })
  }

  // Payment Option checked
  handleAllPaymentChecked = (event, checkType) => {
    let { selectedPayment } = this.state;
    let payOptions = this.state.payOptions
    payOptions.forEach((payOption, index) => {
      payOption.isChecked = event != null ? event.target.checked : checkType;
      if ((event?.target?.checked == true) || checkType == true) {
        let el = this.state.selectedPayment.find((itm) => itm === payOption.id);
        if (el)
          this.state.selectedPayment.splice(
            this.state.selectedPayment.indexOf(el),
            1
          );
        selectedPayment.push(payOption.id)
        this.setState({ selectallPayment: true })
      } else {
        let el = this.state.selectedPayment.find((itm) => itm === payOption.id);
        if (el)
          this.state.selectedPayment.splice(
            this.state.selectedPayment.indexOf(el),
            1
          );
        this.setState({ selectallPayment: false })
      }
    }
    )

    this.setState(payOptions)
  }

  handleCheckChieldPayment = (event) => {
    let payOptions = this.state.payOptions
    let count = payOptions.length

    payOptions.forEach(payOption => {
      if (payOption.id === parseInt(event.target.value)) {
        payOption.isChecked = event.target.checked
        if (event?.target?.checked == true) {
          this.state.selectedPayment.push(payOption.id)
        } else {
          let el = this.state.selectedPayment.find(
            (itm) => itm === parseInt(event.target.value)
          );
          if (el)
            this.state.selectedPayment.splice(
              this.state.selectedPayment.indexOf(el),
              1
            );
          this.setState({ selectallPayment: false })
        }
      }
    })
    if (count == this.state.selectedPayment.length) {
      this.setState({ selectallPayment: true })
    } else {
      this.setState({ selectallPayment: false })
    }
    this.setState({ payOptions: payOptions })
  }

  // Categories checked
  handleAllCategoryChecked = (event, checkType) => {
    let categoryList = this.state.categoryList
    let { selectedCategory } = this.state;
    let self = this;
    categoryList.forEach((category, index) => {
      category.isChecked = event != null ? event.target.checked : checkType;
      if ((event?.target?.checked == true) || checkType == true) {
        let el = this.state.selectedCategory && this.state.selectedCategory.find((itm) => itm === category.id);
        if (el)
          this.state.selectedCategory.splice(
            this.state.selectedCategory.indexOf(el),
            1
          );
        selectedCategory.push(category.id)
        this.setState({ selectallCategories: true })
      } else {
        let el = selectedCategory && this.state.selectedCategory.find((itm) => itm === category.id);
        if (el)
          this.state.selectedCategory.splice(
            this.state.selectedCategory.indexOf(el),
            1
          );
        this.setState({ selectallCategories: false })
      }
    }
    )

    this.setState(categoryList)
  }

  /**
    * Function to phone number format
    * 
    */

  formatPhoneNumber = (phoneNumberString) => {
    var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
    var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
    if (match) {
      return '(' + match[1] + ') ' + match[2] + '-' + match[3]
    }
    return null
  }

  handleCheckChieldCategory = (event) => {
    let categoryList = this.state.categoryList
    let count = categoryList.length
    categoryList.forEach(category => {
      if (category.id === parseInt(event.target.value)) {
        category.isChecked = event.target.checked
        if (event?.target?.checked == true) {
          this.state.selectedCategory && this.state.selectedCategory.push(category.id)
        } else {
          let el = this.state.selectedCategory.find(
            (itm) => itm === parseInt(event.target.value)
          );
          if (el)
            this.state.selectedCategory.splice(
              this.state.selectedCategory.indexOf(el),
              1
            );
          this.setState({ selectallCategories: false })
        }

      }
    })
    if (count == this.state.selectedCategory.length) {
      this.setState({ selectallCategories: true })
    } else {
      this.setState({ selectallCategories: false })
    }
    this.setState({ categoryList: categoryList })
  }

  // Hours of operation checked
  handleAllhoursChecked = (event, checkType) => {
    let HoursOfOperation = this.state.HoursOfOperation
    let { selectedHour } = this.state;
    HoursOfOperation.forEach((Hours, index) => {
      Hours.isChecked = event != null ? event.target.checked : checkType;
      if ((event?.target?.checked == true) || checkType == true) {
        let el = this.state.selectedHour.find((itm) => itm === Hours.id);
        if (el)
          this.state.selectedHour.splice(
            this.state.selectedHour.indexOf(el),
            1
          );
        selectedHour.push(Hours.id)
        this.setState({ selectallHours: true })
      } else {
        let el = this.state.selectedHour.find((itm) => itm === Hours.id);
        if (el)
          this.state.selectedHour.splice(
            this.state.selectedHour.indexOf(el),
            1
          );
        this.setState({ selectallHours: false })
      }
    }
    )

    this.setState(HoursOfOperation)

  }

  handleCheckChieldHours = (event) => {
    let HoursOfOperation = this.state.HoursOfOperation;
    let count = HoursOfOperation.length;
    HoursOfOperation.forEach(Hours => {
      if (Hours.id === parseInt(event.target.value)) {
        Hours.isChecked = event.target.checked
        if (event?.target?.checked == true) {
          this.state.selectedHour.push(Hours.id)
        } else {
          let el = this.state.selectedHour.find(
            (itm) => itm === parseInt(event.target.value)
          );
          if (el)
            this.state.selectedHour.splice(
              this.state.selectedHour.indexOf(el),
              1
            );
          this.setState({ selectallHours: false })
        }
      }
    })

    if (count == this.state.selectedHour.length) {
      this.setState({ selectallHours: true })
    } else {
      this.setState({ selectallHours: false })
    }
    this.setState({ HoursOfOperation: HoursOfOperation })
  }

  // Call to Action checked
  handleAllActionChecked = (event) => {
    let corporateCallList = this.state.corporateCallList
    var checkState = !this.state.checkAllaction;
    let { selectedCalltoAction } = this.state;
    corporateCallList.forEach((corporateCall, index) => {
      if (checkState == true) {
        corporateCall.isChecked = checkState;
        let el = this.state.selectedCalltoAction.find((itm) => itm === corporateCall.id);
        if (el)
          this.state.selectedCalltoAction.splice(
            this.state.selectedCalltoAction.indexOf(el),
            1
          );
        selectedCalltoAction.push(corporateCall.id)
      } else {
        corporateCall.isChecked = checkState;
        let el = this.state.selectedCalltoAction.find((itm) => itm === corporateCall.id);
        if (el)
          this.state.selectedCalltoAction.splice(
            this.state.selectedCalltoAction.indexOf(el),
            1
          );
      }
    });
    this.state.checkAllaction = checkState;
    this.setState({ corporateCallList: corporateCallList, checkAllaction: this.state.checkAllaction })
  }

  handleCheckChieldAction = (event) => {
    let corporateCallList = this.state.corporateCallList
    corporateCallList.forEach(corporateCall => {
      if (corporateCall.id === parseInt(event.target.value)) {
        corporateCall.isChecked = event.target.checked
        if (event?.target?.checked == true) {
          this.state.selectedCalltoAction.push(corporateCall.id)
        } else {
          let el = this.state.selectedCalltoAction.find(
            (itm) => itm === parseInt(event.target.value)
          );
          if (el)
            this.state.selectedCalltoAction.splice(
              this.state.selectedCalltoAction.indexOf(el),
              1
            );
        }
      }
    })
    this.setState({ corporateCallList: corporateCallList })
  }
  // About us checked
  handleCheckChieldAbout = (event) => {
    let specialities = this.state.specialities
    let meetOwner = this.state.meetOwner
    let businessOwnerhistory = this.state.businessOwnerhistory
    const { selectedAbout } = this.state;
    let aboutInfo = {}
    if (specialities && Array.isArray(specialities) && specialities.length > 0 && specialities[0].value !== "" && event.target.name == "selectSpeciality") {
      specialities[0].isChecked = event.target.checked
      if (event?.target?.checked == true) {
        aboutInfo.specialities = specialities[0].value
        selectedAbout.push(aboutInfo);
      } else {
        let el = this.state.selectedAbout.find((itm) => itm.specialities === specialities[0].value);
        if (el)
          this.state.selectedAbout.splice(
            this.state.selectedAbout.indexOf(el),
            1
          );
      }
      this.setState({ specialities: specialities, ischeckedSpecial: !this.state.ischeckedSpecial })
    } else if (event.target.name == "selectSpeciality") {
      this.setState({ specialities: specialities, ischeckedSpecial: !this.state.ischeckedSpecial })
    }
    if (meetOwner && Array.isArray(meetOwner) && meetOwner.length > 0 && meetOwner[0].value.bio !== "" && event.target.name == "selectHistory") {
      meetOwner[0].isChecked = event.target.checked
      if (event?.target?.checked == true) {
        aboutInfo.meetOwner = meetOwner[0].value.bio
        selectedAbout.push(aboutInfo);
      } else {
        let el = this.state.selectedAbout.find((itm) => itm.meetOwner === meetOwner[0].value.bio);
        if (el)
          this.state.selectedAbout.splice(
            this.state.selectedAbout.indexOf(el),
            1
          );
      }
      this.setState({ meetOwner: meetOwner, ischeckedHistory: !this.state.ischeckedHistory })
    }
    if (businessOwnerhistory && Array.isArray(businessOwnerhistory) && businessOwnerhistory.length > 0 && businessOwnerhistory[0].value.history !== "" && event.target.name == "selectMBO") {
      businessOwnerhistory[0].isChecked = event.target.checked
      if (event?.target?.checked == true) {
        aboutInfo.businessOwnerhistory = businessOwnerhistory[0].value.history
        selectedAbout.push(aboutInfo);
      } else {
        let el = this.state.selectedAbout.find((itm) => itm.businessOwnerhistory === businessOwnerhistory[0].value.history);
        if (el)
          this.state.selectedAbout.splice(
            this.state.selectedAbout.indexOf(el),
            1
          );
      }
      this.setState({ businessOwnerhistory: businessOwnerhistory, ischeckedMBO: !this.state.ischeckedMBO })
    }


  }

  // Special Offer checked
  handleAllOfferChecked = (event) => {
    let specialofferSet = this.state.specialofferSet
    var checkState = !this.state.checkAllOffer;
    let { selectedSpecialOffers } = this.state;
    specialofferSet.forEach((specialoffer, index) => {
      if (checkState == true) {
        specialoffer.isChecked = checkState;
        let el = this.state.selectedSpecialOffers.find((itm) => itm === specialoffer.id);
        if (el)
          this.state.selectedSpecialOffers.splice(
            this.state.selectedSpecialOffers.indexOf(el),
            1
          );
        selectedSpecialOffers.push(specialoffer.id)
      } else {
        specialoffer.isChecked = checkState;
        let el = this.state.selectedSpecialOffers.find((itm) => itm === specialoffer.id);
        if (el)
          this.state.selectedSpecialOffers.splice(
            this.state.selectedSpecialOffers.indexOf(el),
            1
          );
      }
    });
    this.state.checkAllOffer = checkState;
    this.setState({ specialofferSet: specialofferSet, checkAllOffer: this.state.checkAllOffer })
  }

  handleCheckChieldOffers = (event) => {
    let specialofferSet = this.state.specialofferSet
    specialofferSet.forEach(specialoffer => {
      if (specialoffer.id === parseInt(event.target.value)) {
        specialoffer.isChecked = event.target.checked
        if (event?.target?.checked == true) {
          this.state.selectedSpecialOffers.push(specialoffer.id)
        } else {
          let el = this.state.selectedSpecialOffers.find(
            (itm) => itm === parseInt(event.target.value)
          );
          if (el)
            this.state.selectedSpecialOffers.splice(
              this.state.selectedSpecialOffers.indexOf(el),
              1
            );
        }
      }
    })
    this.setState({ specialofferSet: specialofferSet })
  }

  componentDidMount() {
    let country = "";
    this.props.get_branch_copy(country);
    this.props.get_admin_stats();
  }

  componentWillUnmount() {
    clearTimeout(this.setTimeOutSlider);
  }

  selectBranchModalToggle = () => {
    this.setState({
      selectBranchModal: !this.state.selectBranchModal,
      mediaId: 0,
      country: "",
      stateName: "",
      branchId: 0,
    });
  };

  copyInfoToBranchModalToggle = () => {
    let branchId = this.state.branchId;
    //let branchId = 185423;
    this.props.get_branch_data(branchId);
    this.props.get_corporate_copy_details(branchId);
    this.props.get_specific_branch_corporate_call_list(branchId);
    let filteredArray = this.state.pendingBranches.filter(item => item.id != branchId);
    //this.setState({ copyInfoToBranchModal: !this.state.copyInfoToBranchModal }, () => this.props.get_branch_data(branchId));
    this.setState({ copyInfoToBranchModal: !this.state.copyInfoToBranchModal, pendingBranches: filteredArray });
  };

  cancelCopyOpConfirmed = () => {
    this.setState({
      cancelCopyOpModal: false,
      copyInfoToBranchModal: false,
      selectBranchModal: false,
      previewAditional: [],
      islogoChecked: false,
      isCheckedEmail: false,
      isCheckedPosition: false,
      isCheckedProfile: false,
      isVideoChecked: false
    });
    this.toggleCopyBranchToDefaultValues();
  }

  editCopiedSelectionModalToggle = () => {
    this.setState({ copyInfoToBranchModalStep2: false, previewAditional: [] });
  }

  handleOnFileUploadChange = (event, uploadType, uploadSubType) => {

    let uploadFile = event.target.files;
    let url = "";
    let id = this.props.corporate_details?.id
    if (uploadType === 'companyLogo') {
      url = `/upload/multiuploader/?img=corporate`;
    } else if (uploadType === 'freshProfilePic') {
      url = `/upload/multiuploader/?album=&instance=bizimage&image=undefined&doc=undefined`;
    } else if (uploadType === 'UploadBackgroundVideoOrImage') {
      url = `/upload/multiuploader/?album=profile&img=corporate`
    } else {
      url = `/upload/multiuploader/?album=profile&img=corporate`
    }

    var i = 0;
    let data = new FormData();
    data.append("file", uploadFile[i]);
    callApi(
      url,
      "POST",
      data,
      true
    ).then((response) => {

      if (response) {
        let media_id = response.id;
        this.setState({
          ...this.state,
          mediaId: media_id,
          // selectBranchModal: true,
        }, () => {
          if (uploadType === 'freshProfilePic') {
            if (uploadSubType === 'branchLogo') {
              this.setState({
                freshprofileBranchLogoUrl: response.url,
                freshProfileBranchLogoId: response.id
              })

            } else if (uploadSubType === 'branchBanner') {
              this.setState({
                freshProfileBranchBannerUrl: response.url,
                freshProfileBranchBannerId: response.id
              })

            } else {
              this.setState({
                freshprofilePicUrl: response.url,
                freshProfilePicId: response.id
              })
            }
          } else {
            const data = {
              "corporate_listing": [id],
              "media_id": `${this.state.mediaId}`
            }
            if (uploadType === 'companyLogo') {
              this.props.upload_corporate_media(data, 'companyLogo', id);
            } else if (uploadType === 'UploadBackgroundVideoOrImage') {
              this.props.upload_corporate_media(data, 'UploadBackgroundVideoOrImage', id);
            } else {
              this.props.upload_corporate_media(data);
            }
          }
        });
      }
    });
  };

  handleOnSelectedBranchLogoUpload = (event, uploadType) => {
    let uploadFile = event.target.files;
    let showFiles = [];

    for (const key of Object.keys(uploadFile)) {
      let itemType = uploadFile[key].type.split("/");
      let extName = itemType[0];
      showFiles.push({ id: "", url: extName !== "" ? URL.createObjectURL(uploadFile[key]) : require("../../assets/images/blank.png") });
    }
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        uploadFile: showFiles,
        progress: 0,
      },
    });
    let progressPart = 100 / showFiles.length;
    let progress = 0;


    let url = "";
    let id = this.state.branchId
    url = uploadType === 'companyLogo' ?
      `/upload/multiuploader/?img=corporate` :
      `/upload/multiuploader/?album=profile&img=corporate`;
    var i = 0;

    for (const key of Object.keys(uploadFile)) {
      let data = new FormData();
      data.append("file", uploadFile[key]);
      const options = {
        onUploadProgress: (progressEvent) => {
          const { loaded, total } = progressEvent;
          let Percent = Math.floor(loaded * 100 / total)

        }
      }
      callApi(
        url,
        "POST",
        data,
        true,
        options
      ).then((response) => {
        if (response) {
          if (showFiles.length === 1 || key === showFiles.length - 1) {
            progress = 100;
          } else {
            progress = progress + progressPart;
          }
          showFiles[key].id = response.id;

          let media_id = response.id;
          this.setState({
            ...this.state,
            mediaId: media_id,
            uploadMedia: {
              ...this.state.uploadMedia,
              progress: progress,
            },
            // selectBranchModal: true,
          }, () => {
            const data = {
              "corporate_listing": [id],
              "media_id": `${this.state.mediaId}`
            }
            if (this.state.uploadMedia.progress === 100) {
              this.setTimeOutSlider = setTimeout(() => {
                this.setState({
                  uploadMedia: {
                    selectedMedia: [],
                    selectedMediaIds: [],
                    uploadFile: [],
                    progress: 0,
                  }
                })
              }, 1500)

            }
            this.props.upload_corporate_media(data, 'copyBranch', id);
          });
        }
      });
    }
  };

  handleUploadcorporatemedia = (event) => {
    event.preventDefault();
    const { selectValue, mediaId } = this.state;
    let data = {
      corporate_listing: selectValue,
      media_id: mediaId
    }
    this.props.upload_corporate_media(data);
    let country = "";
    this.props.get_branch_copy(country)
    this.setState({ selectBranchModal: false, selectValue: [], mediaId: 0, country: "", state: "", rowState: [] });
  }

  updateReviews = ({ color }, tabType, tab, queryType, tabName) => {
    // let elem = document.getElementById("scrollTillTab");
    // elem.scrollIntoView();
    // this.props.setTabReviews(tab);
    // this.setState({ ViewReviewsType: "new" })
    this.setState({
      dotSelectedType: tabType,
      dotSelectedName: tabName,
      dotSelectedColor: color,
      dotSelectedDays: this.state.stats_duration,
      dotSelectedqueryType: queryType,

    }, () => {
      this.props.handleSetDotTypeColor(tabType, color, tabName, this.state.stats_duration, queryType)
      this.props.get_review_data_by_circle_click({
        color,
        type: tabType,
        days: this.state.stats_duration,
        // this.props && this.props.filtered_stats_count ? this.props.filtered_stats_count : 30,
        queryType,
        getBranches: 'yes',
      });
    })
  };


  renderItemsSpans = ({ data }, type, queryType, tab) => {
    let data_type = { 'reviews': '4', 'message': '5', 'qa': '7', 'feedback': '6', 'disputed_Review': '4' }
    return (
      <div className="d-flex flex-wrap">
        <span>{data.count}</span>
        <span className="ml-auto">
          {data.green > 0 && (
            <span className="px-2 d-inline-flex align-items-center">
              {type === 'overall' ? <span className="badge badge-success">{data.green}</span>
                : <span className="badge badge-success"
                  onClick={() =>
                    this.updateReviews({ color: "green" }, type, data_type[tab], queryType, tab)
                  }>{data.green}</span>}
            </span>
          )}
          {data.orange > 0 && (
            <span className="px-2 d-inline-flex align-items-center">
              {type === 'overall' ? <span className="badge badge-warning">{data.orange}</span>
                : <span className="badge badge-warning"
                  onClick={() =>
                    this.updateReviews({ color: "orange" }, type, data_type[tab], queryType, tab)
                  }>{data.orange} </span>}
            </span>
          )}
          {
            data.red > 0 && (
              <span className="px-2 d-inline-flex align-items-center">
                {type === 'overall' ? <span className="badge badge-danger">{data.red}</span>
                  : <span className="badge badge-danger"
                    onClick={() =>
                      this.updateReviews({ color: "red" }, type, data_type[tab], queryType, tab)
                    }
                  >{data.red}</span>}
              </span>
            )
          }
        </span>
      </div>
    )
  }

  handleChangeStats = (e) => {
    let { name, value } = e.target;
    this.setState({ [name]: value }, () => {

      this.props.get_stats_filtered_data(value)
      this.props.get_roles_list_filter_data(value)
    })
  }

  /**
   * Function for set state on change
   * @param event
   */

  handleChange = (event) => {
    const { cState } = this.state;
    let branchId = 0;
    let stateName = "";
    if (event.target.name == "select_country") {
      let country = event.target.value;
      var item;
      if (cState.hasOwnProperty(country)) {
        item = cState[country];
      }
      let newArray = [];
      for (var i = 0; i < this.state.branches.length; i++) {
        if (country == this.state.branches[i].country) {
          newArray.push(this.state.branches[i]);
        }
      }
      this.setState(
        {
          country: event.target.value,
          newBranches: newArray,
          countryState: item,
        },
        () => this.props.get_branch_copy(country)
      );
    }

    if (event.target.name == "select_state") {
      let state = event.target.value;
      let newArray = [];
      if (this.state.branches) {
        for (var i = 0; i < this.state.branches.length; i++) {
          if (
            this.state.country == this.state.branches[i].country &&
            state == this.state.branches[i].state
          ) {
            newArray.push(this.state.branches[i]);
          }
        }
        this.setState(
          { state: event.target.value, isdisabled: false, newBranches: newArray },
          () => this.props.get_branch_copy(this.state.country)
        );
      }
    }

    if (event.target.name == "select_branch" && event.target.type === "radio") {
      let branchId = event.target.value;
      this.setState({ branchId: branchId, isdisabled: false });
    }

    if (event.target.name == "select_state") {
      let stateName = event.target.value;
      this.setState({ stateName: stateName });
    }

    this.setState({ [event.target.name]: event.target.value });
  };

  /**
   * Function for set state on change
   * @param event
   */

  handlePreviewChange = (event) => {
    const { cState } = this.state;
    let branchId = 0;
    let stateName = "";
    if (event.target.name == "select_listing_country") {
      let country = event.target.value;
      if (country != "Select Country") {
        var item;
        if (cState.hasOwnProperty(country)) {
          item = cState[country];
        }
        let newArray = [];
        for (var i = 0; i < this.state.branches.length; i++) {
          if (country == this.state.branches[i].country) {
            newArray.push(this.state.branches[i]);
          }
        }
        this.setState(
          {
            countryName: event.target.value,
            pendingBranches: newArray,
            countryState: item,
          }
        );
      } else {
        this.setState({ pendingBranches: this.state.withoutfilterBranches, countryName: event.target.value });
      }
    }

    if (event.target.name == "select_listing_province") {
      let state = event.target.value;
      let newArray = [];
      if (this.state.branches) {
        for (var i = 0; i < this.state.branches.length; i++) {
          if (
            this.state.countryName == this.state.branches[i].country &&
            state == this.state.branches[i].state
          ) {
            newArray.push(this.state.branches[i]);
          }
        }
        this.setState(
          { provinceName: event.target.value, isdisabled: false, pendingBranches: newArray });
      }
    }

    this.setState({ [event.target.name]: event.target.value });
  };

  /**
   * Function for select all for branches
   * @param event
   */

  handleAllListingChecked = (value) => {
    var rowState = [];
    var nocheckAll = [];
    var checkState = value;
    for (var i = 0; i < this.state.pendingBranches.length; i++) {
      rowState[i] = checkState;
      if (checkState == true) {
        let el = this.state.selectValue.find(
          (itm) => itm === this.state.pendingBranches[i].id
        );
        if (el)
          this.state.selectValue.splice(this.state.selectValue.indexOf(el), 1);
        this.state.selectValue.push(this.state.pendingBranches[i].id);
      } else {
        let el = this.state.selectValue.find(
          (itm) => itm === this.state.pendingBranches[i].id
        );
        if (el)
          this.state.selectValue.splice(this.state.selectValue.indexOf(el), 1);
      }
    }
    this.state.checkAll = checkState;
    this.setState({
      rowState: rowState,
      checkAll: this.state.checkAll,
      isdisabledsave: false,
    });
  };

  /**
   * Function for select all for branches
   * @param event
   */

  handleAllChecked = (event) => {
    var rowState = [];
    var nocheckAll = [];
    var checkState = !this.state.checkAll;
    for (var i = 0; i < this.state.pendingBranches.length; i++) {
      rowState[i] = checkState;
      if (checkState == true) {
        let el = this.state.selectValue.find(
          (itm) => itm === this.state.pendingBranches[i].id
        );
        if (el)
          this.state.selectValue.splice(this.state.selectValue.indexOf(el), 1);
        this.state.selectValue.push(this.state.pendingBranches[i].id);
      } else {
        let el = this.state.selectValue.find(
          (itm) => itm === this.state.pendingBranches[i].id
        );
        if (el)
          this.state.selectValue.splice(this.state.selectValue.indexOf(el), 1);
      }
    }
    this.state.checkAll = checkState;
    this.setState({
      rowState: rowState,
      checkAll: this.state.checkAll,
      isdisabledsave: false,
    });
  };

  /**
   * Function for select all for branches for modal One
   * @param event
   */

  handleAllCheckedBranch = (event) => {
    var rowState = [];
    var nocheckAll = [];
    var checkState = !this.state.checkAll;
    for (var i = 0; i < this.state.newBranches.length; i++) {
      rowState[i] = checkState;
      if (checkState == true) {
        let el = this.state.selectValue.find(
          (itm) => itm === this.state.newBranches[i].id
        );
        if (el)
          this.state.selectValue.splice(this.state.selectValue.indexOf(el), 1);
        this.state.selectValue.push(this.state.newBranches[i].id);
      } else {
        let el = this.state.selectValue.find(
          (itm) => itm === this.state.newBranches[i].id
        );
        if (el)
          this.state.selectValue.splice(this.state.selectValue.indexOf(el), 1);
      }
    }
    this.state.checkAll = checkState;
    this.setState({
      rowState: rowState,
      checkAll: this.state.checkAll,
      isdisabledsave: false,
    });
  };

  /**
   * Function for search filter branch
   * @param event
   **/
  filterList(event) {
    let value = event.target.value;
    if (value == "") {
      this.setState({ pendingBranches: this.state.withoutfilterBranches });
    } else {
      let branches = this.state.pendingBranches;
      let pendingBranches = [];
      pendingBranches = branches.filter((branch) => {
        return branch.name.toLowerCase().search(value) != -1;
      });
      this.setState({ pendingBranches });
    }
  }

  /**
   * Function for select all for Additional Information
   * @param event
   */

  handleAllCheckedAdditional = (event) => {
    var rowStateAditional = [];
    var nocheckAll = [];
    let additionalinformation = {}
    var checkState = !this.state.checkAllAditional;
    Object.values(this.state.additionInfo).forEach(
      (key, index) => {
        rowStateAditional[index] = checkState;
        if (checkState == true) {
          let el = this.state.selectValueAditional.find((itm) => itm.name === key.name);
          if (el)
            this.state.selectValueAditional.splice(
              this.state.selectValueAditional.indexOf(el),
              1
            );
          this.state.selectValueAditional.push(key);
          this.state.previewAditional.push(this.state.additionInfo[index]);
        } else {
          let el = this.state.selectValueAditional.find((itm) => itm.name === key.name);
          if (el)
            this.state.selectValueAditional.splice(
              this.state.selectValueAditional.indexOf(el),
              1
            );
          this.state.previewAditional.splice(
            this.state.previewAditional.indexOf(el),
            1
          );
        }
      }
    );
    this.state.checkAllAditional = checkState;
    this.setState({
      rowStateAditional: rowStateAditional,
      checkAllAditional: this.state.checkAllAditional,
      isdisabledsave: false,
    });

  };

  handleAllCheckedAddress = (event, checkType) => {
    const { selectValueAddress } = this.state;
    let AdressDiary = this.state.AdressDiary
    AdressDiary.forEach((addressD, index) => {
      addressD.isChecked = event != null ? event.target.checked : checkType;
      if ((event?.target?.checked == true) || checkType == true) {
        let el = this.state.selectValueAddress.find((itm) => itm === addressD.id);
        if (el)
          this.state.selectValueAddress.splice(
            this.state.selectValueAddress.indexOf(el),
            1
          );
        selectValueAddress.push(addressD.id)
        this.setState({ selectallAddress: true })
      } else {
        let el = this.state.selectValueAddress.find((itm) => itm === addressD.id);
        if (el)
          this.state.selectValueAddress.splice(
            this.state.selectValueAddress.indexOf(el),
            1
          );
        this.setState({ selectallAddress: false })
      }
    }
    )

    this.setState(AdressDiary)
  };

  handleCheckfieldAddress = (event) => {
    let AdressDiary = this.state.AdressDiary
    let count = AdressDiary.length
    AdressDiary.forEach(addressD => {
      if (addressD.id === parseInt(event.target.value)) {
        addressD.isChecked = event.target.checked
        if (event?.target?.checked == true) {
          this.state.selectValueAddress.push(addressD.id)
        } else {
          let el = this.state.selectValueAddress.find(
            (itm) => itm === parseInt(event.target.value)
          );
          if (el)
            this.state.selectValueAddress.splice(
              this.state.selectValueAddress.indexOf(el),
              1
            );
          this.setState({ selectallAddress: false })
        }
      }
    })

    if (count == this.state.selectValueAddress.length) {
      this.setState({ selectallAddress: true })
    } else {
      this.setState({ selectallAddress: false })
    }
    this.setState({ AdressDiary: AdressDiary })
  }

  /**
   * Function for set state on change
   * @param event
   */

  handleChangedBranches = (event) => {
    let opts = [],
      opt;
    let { selectedBusiness } = this.state;
    if (
      (event.target.name == "select_multiple_companies" ||
        event.target.name == "select_multiple_branches") &&
      event.target.type === "checkbox"
    ) {
      let val = 0;
      if (event.target.name == "select_multiple_companies") {
        val = event.target.getAttribute("data-id");
      } else {
        val = event.target.getAttribute("data-idnew");
      }

      if (event.target.checked) {
        // Pushing the object into array
        this.state.rowState[val] = true;
        this.state.selectValue.push(parseInt(event.target.value));
        if (this.state.checkAll) {
          this.state.checkAll = !this.state.checkAll;
        }
        this.setState({
          rowState: this.state.rowState,
          checkAll: this.state.checkAll,
        });
      } else {
        this.state.rowState[val] = false;
        if (this.state.checkAll) {
          this.state.checkAll = this.state.checkAll;
        }
        this.setState({
          rowState: this.state.rowState,
          checkAll: this.state.checkAll,
        });
        let el = this.state.selectValue.find(
          (itm) => itm === parseInt(event.target.value)
        );
        if (el)
          this.state.selectValue.splice(this.state.selectValue.indexOf(el), 1);
      }
      this.setState({
        selectValue: this.state.selectValue,
        isdisabledsave: false,
      });
    }

    if (
      event.target.name == "selectAddress" &&
      event.target.type === "checkbox"
    ) {
      if (event.target.checked) {
        // Pushing the object into array
        this.state.selectValueAddress.push(event.target.value);
      } else {
        let el = this.state.selectValueAddress.find(
          (itm) => itm === parseInt(event.target.value)
        );
        if (el)
          this.state.selectValueAddress.splice(
            this.state.selectValueAddress.indexOf(el),
            1
          );
      }
      this.setState({
        selectValueAddress: this.state.selectValueAddress,
        isdisabledsave: false,
        isChecked: !this.state.isChecked,
      });
    }

    if (
      event.target.name == "selectEmail" &&
      event.target.type === "checkbox"
    ) {
      if (event.target.checked) {
        // Pushing the object into array
        //selectedBusiness[0]['email'] = event.target.value;
        selectedBusiness.email = event.target.value;
        this.setState({ selectedBusiness, isCheckedEmail: true });
      } else {
        let key = "email";
        //delete selectedBusiness[0][key];
        selectedBusiness.email = "";
        this.setState({ selectedBusiness, isCheckedAllOwner: false, isCheckedEmail: false });
      }
    }
    if (
      event.target.name == "selectProfilePic" &&
      event.target.type === "checkbox"
    ) {
      if (event.target.checked) {
        // Pushing the object into array
        //selectedBusiness[0]['profile'] = event.target.value;
        selectedBusiness.profile = event.target.value;
        this.setState({ selectedBusiness });
      } else {
        let key = "profile";
        //delete selectedBusiness[0][key];
        selectedBusiness.profile = "";
        this.setState({ selectedBusiness, isCheckedAllOwner: false });

      }
      this.setState({
        isCheckedProfile: !this.state.isCheckedProfile,
      });
    }
    if (
      event.target.name == "selectName" &&
      event.target.type === "checkbox"
    ) {
      if (event.target.checked) {
        // Pushing the object into array
        //selectedBusiness[0]['profile'] = event.target.value;
        selectedBusiness.fullName = event.target.value;
        this.setState({ selectedBusiness });
      } else {
        let key = "profile";
        //delete selectedBusiness[0][key];
        selectedBusiness.fullName = "";
        this.setState({ selectedBusiness, isCheckedAllOwner: false });

      }
      this.setState({
        isCheckedFullName: !this.state.isCheckedFullName,
      });
    }
    if (
      event.target.name == "selectPostion" &&
      event.target.type === "checkbox"
    ) {

      if (event.target.checked) {
        // Pushing the object into array
        //selectedBusiness[0]['position'] = event.target.value;
        selectedBusiness.position = event.target.value;
        this.setState({ selectedBusiness, isCheckedPosition: true });
      } else {
        let key = "position";
        //delete selectedBusiness[0][key];
        selectedBusiness.position = "";
        this.setState({ selectedBusiness, isCheckedPosition: false, isCheckedAllOwner: false });
      }
      /*this.setState({
        isCheckedPosition: !this.state.isCheckedPosition,
      });*/
    }
    if (
      event.target.name == "select_logo" &&
      event.target.type === "checkbox"
    ) {
      if (event.target.checked) {
        // Pushing the object into array
        var imgsrc = document.getElementById("logoUrl").src;
        this.setState({ logo_to_copy: imgsrc }, () => {
        });
      } else {
        this.setState({ logo_to_copy: "" });
      }
      this.setState({
        islogoChecked: !this.state.islogoChecked,
      });
    }

    if (
      event.target.name == "select_homepage_image" &&
      event.target.type === "checkbox"
    ) {
      if (event.target.checked) {
        // Pushing the object into array

        var videosrc = document.getElementById("videoUrl").src;
        this.setState({ video_to_copy: videosrc });
      } else {
        this.setState({ video_to_copy: "" });
      }
      this.setState({
        isVideoChecked: !this.state.isVideoChecked,
      });
    }
    if (
      event.target.name == "select_additional" &&
      event.target.type === "checkbox"
    ) {
      let val2 = event.target.getAttribute("data-idd");
      let objectaddtional = {}
      if (event.target.checked) {
        // Pushing the object into array
        this.state.rowStateAditional[val2] = true;
        this.state.selectValueAditional.push(event.target.value);
        Object.values(this.state.additionInfo).forEach(
          (key, index) => {
            if (key.name === event.target.value) {
              this.state.previewAditional.push(this.state.additionInfo[index]);
            }
          })
        if (this.state.checkAllAditional) {
          this.state.checkAllAditional = !this.state.checkAllAditional;
        }
        this.setState({
          rowStateAditional: this.state.rowStateAditional,
          checkAllAditional: this.state.checkAllAditional,
        });
      } else {
        this.state.rowStateAditional[val2] = false;
        if (this.state.checkAllAditional) {
          this.state.checkAllAditional = this.state.checkAllAditional;
        }
        this.setState({
          rowStateAditional: this.state.rowStateAditional,
          checkAllAditional: this.state.checkAllAditional,
        });
        let el = this.state.selectValueAditional.find(
          (itm) => itm === event.target.value
        );
        if (el)
          this.state.selectValueAditional.splice(
            this.state.selectValueAditional.indexOf(el),
            1
          );
        this.state.previewAditional.splice(
          this.state.previewAditional.indexOf(el),
          1
        );
      }
      this.setState({
        selectValueAditional: this.state.selectValueAditional,
        isdisabledsave: false,
      });

    }
  };

  handleSubmit = (event, errors, values) => {
    event.preventDefault();
    const {
      selectValue, selectValueAddress, selectValueAditional, branchId, selectedPhone, selectedWebsite, selectedEmail,
      selectedCategory, selectedCalltoAction, selectedSpecialOffers, selectedPayment, selectedHour, selectedAbout,
      selectedBusiness, logo_to_copy, video_to_copy, freshSelectedPayment, freshWROffer, freshAddress, freshPhone,
      freshWebsite, freshHOP, freshEmail, freshFirstName, freshLastName, freshPosition, freshPersonEmail,
      freshSpecialities, freshHistory, freshBusinessOwner, freshProfilePicId, freshProfileBranchLogoId,
      freshProfileBranchBannerId, seletedCategoriesNames, freshSelectAdditionalInfo
    } = this.state;
    let data = {};
    if (this.state.formType === 'fresh') {
      data = {
        freshData: true,
        about_to_copy: {
          specialities: freshSpecialities,
          businessOwnerhistory: freshHistory,
          meetOwner: freshBusinessOwner
        },
        // freshPersonEmail: freshPersonEmail,
        specialOffer_to_copy: freshWROffer,
        address_to_copy: freshAddress[0].freshAddress1 == "" ? [] : freshAddress,
        phone_to_copy: freshPhone,
        website_to_copy: freshWebsite,
        hours_to_copy: freshHOP,
        email_to_copy: freshEmail,
        additional_to_copy: freshSelectAdditionalInfo,
        // owner_to_copy: {
        //   specialities: freshSpecialities,
        //   businessOwnerhistory: freshHistory,
        //   meetOwner: freshBusinessOwner
        // },
        payment_to_copy: freshSelectedPayment,
        list_branches_copy: selectValue,
        category_to_copy: seletedCategoriesNames,
        // media: {},
      }
      if (this.state.freshProfilePicId != "") {
        data.profilePic = freshProfilePicId
      }
      if (this.state.freshProfileBranchLogoId != "") {
        data.logo_to_copy = freshProfileBranchLogoId
      }
      if (this.state.freshProfileBranchBannerId != "") {
        data.video_to_copy = freshProfileBranchBannerId
      }
      this.props.fresh_add_branch(data);

    } else {
      data = {
        list_branches_copy: selectValue,
        additional_to_copy: selectValueAditional,
        phone_to_copy: selectedPhone,
        website_to_copy: selectedWebsite,
        email_to_copy: selectedEmail,
        category_to_copy: selectedCategory,
        callToAction_to_copy: selectedCalltoAction,
        specialOffer_to_copy: selectedSpecialOffers,
        payment_to_copy: selectedPayment,
        hours_to_copy: selectedHour,
        about_to_copy: selectedAbout,
        listing_id: parseInt(branchId),
        owner_to_copy: [],
        logo_to_copy: logo_to_copy,
        video_to_copy: video_to_copy,
        data_to_copy: [],
        address_to_copy: selectValueAddress
      };
      this.props.copy_add_branch(data);
    }
    this.toggleCopyBranchToDefaultValues()
  };

  toggleCopyBranchToDefaultValues = async () => {

    await this.setState({
      corporateCallList: [],
      preferredMethod: 'freshData',
      freshSelectAdditionalInfo: [],
      copyInfoToBranchModal: false,
      selectCopyBranchMethodModal: false,
      enterNewBranchDataModal: false,
      selectBranchModal: false,
      selectValue: [],
      selectValueAddress: [],
      selectValueAditional: [],
      branchId: 0,
      country: "",
      stateName: "",
      countryName: "",
      countryListingName: "",
      provinceName: "",
      rowState: [],
      selectedPhone: [],
      selectedWebsite: [],
      selectallWebsites: [],
      selectedEmail: [],
      selectedCategory: [],
      selectedCalltoAction: [],
      selectedSpecialOffers: [],
      selectedPayment: [],
      selectedHour: [],
      selectedAbout: [],
      copyInfoToBranchModalStep2: false,
      rowStateAditional: [],
      selectallPhone: false,
      selectallEmails: false,
      selectallWebsites: false,
      selectallCategories: false,
      selectallPayment: false,
      selectallAddress: false,
      selectallHours: false,
      checkAll: false,
      checkAllAditional: false,
      isCheckedAllOwner: false,
      checkAllBusinessInfo: false,
      islogoChecked: false,
      ischeckedSpecial: false,
      ischeckedHistory: false,
      ischeckedMBO: false,
      isCheckedFullName: false,
      selectedBusiness: { email: "", profile: "", position: "" },
      logo_to_copy: "",
      video_to_copy: "",
      addPhone: { ph1: '', ph2: '', ph3: '', label: '' },
      isEditPhone: false,
      isTogglePhone: false,
      isPhoneValid: true,
      isVideoChecked: false,

      isToggleBusinessEmail: false, // BusinessEmail fields
      isEditBusinessEmail: false,
      addBusinessEmail: { email: "", email_type: null },
      isBusinessEmailValid: true,

      isToggleWebsite: false, // Website fields
      isEditWebsite: false,
      addWebsite: { website: "", website_type: null },
      isWebsiteValid: true,

      specialofferSet: [], //offer set
      isToggleOffer: false,
      isToggleDeleteOffers: false,
      isEditOffer: false,
      addOffer: { special_offer: "", offer_type: "", from_date: "", to_date: "", claim_deal_link: "" },
      claim_deal_link_ur: "",

      editRecordId: false, // edit record
      editRecordType: false,

      clonePaymentSwitch: {}, // payment
      paymentSwitch: {},
      isTogglePayment: false,

      businessCategory: [], // Business category
      businessSubCategory: [],
      totalSubCategories: [1],
      anotherCategories: [],
      anotherSubCategories: [],
      postCategoryList: {},
      seletedCategories: [],
      seletedCategoriesNames: [],
      business_specialities: '',
      address1: '',
      subCategoryZero: '',
      selectedCategory: '',

      isToggleHours: false, // Hours fields
      isEditHours: false,
      addHours: { day_of_week: 0, end_time: "", start_time: "", entries: "", id: "", info: "", "next_day": 0 },
      isHoursValid: false,
      isToggleDeleteHours: false,

      isEditAddress: false,
      addAddress: { address1: '', address2: '', state: '', city: '', zipcode: '', country: '', latitude: '', longitude: '' },

      specialities: [], //abbut the business
      meetOwner: [],
      businessOwnerhistory: [],
      editSpec: true,
      editValue: "",
      editHisSpec: true,
      editHistory: "",
      EditBussOwn: "",
      editBusiness: true,
      isToggleBusiness: false,
      isToggleHistory: false,
      isToggleMeet: false,

      fullName: "", // Branch Info
      position: "",
      userEmail: null,
      profilePic: null,
      editName: true,
      fName: "",
      lName: "",
      isValidFullName: true,
      isTogglePosition: true,
      isValidPosition: true,
      profession: "",
      userID: null,

      isToogleAcceptCreditCards: false, // additional information
      additionInfo: [],
      allAdditionalInfo: [],
      selectedCategory: '',
      isEditVisible: {},
      CategoryValue: 'None',
      CategoryName: '',
      CategoryId: '',
      corporateId: '',
      existing_info_count: 0,
      add_info_map: '',

      sub_category_list: [],
      amenities_list: '',
      saved_amenities_list: [],
      amenityId: '',
      additional_info_all_categories: [],
      received_additional_info: false,

      freshData: {
        freshFirstName: "",
        freshLastName: "",
        freshPosition: "",
        freshPersonEmail: "",
        freshSpecialities: '',
        freshHistory: '',
        freshBusinessOwner: "",
        freshWROffer: [
          {
            freshOfferText: '',
            freshOfferType: '',
            freshOfferStartDATE: '',
            freshOfferEndDate: "",
            freshOfferWebsiteLink: "",
          }
        ],
        freshAddress: [
          {
            freshAddress1: '',
            freshAddress2: '',
            freshAddressCity: '',
            freshAddressState: '',
            freshAddressCountry: "",
            freshAddressZipcode: "",
          }
        ],
        freshPhone: [
          {
            phoneFresh: '',
            freshPhoneType: '',
          }
        ],
        freshWebsite: [
          {
            freshWebsiteURL: '',
            freshWebsiteType: '',
          }
        ],
        freshHOP: [
          {
            freshHOPDay: '',
            freshHOPText: '',
            freshHOPStartTime: '',
            freshHOPEndTime: '',
          }
        ],
        freshEmail: [
          {
            freshemailText: "",
            freshEmailType: "",
          }
        ]
      },
      freshWROffer: [
        {
          freshOfferText: '',
          freshOfferType: '',
          freshOfferStartDATE: '',
          freshOfferEndDate: "",
          freshOfferWebsiteLink: "",
        }
      ],
      freshAddress: [
        {
          freshAddress1: '',
          freshAddress2: '',
          freshAddressCity: '',
          freshAddressState: '',
          freshAddressCountry: "",
          freshAddressZipcode: "",
        }
      ],
      freshPhone: [
        {
          phoneFresh: '',
          freshPhoneType: '',
        }
      ],
      freshWebsite: [
        {
          freshWebsiteURL: '',
          freshWebsiteType: '',
        }
      ],
      freshHOP: [
        {
          freshHOPDay: '',
          freshHOPText: '',
          freshHOPStartTime: '',
          freshHOPEndTime: '',
        }
      ],
      freshEmail: [
        {
          freshemailText: "",
          freshEmailType: "",
        }
      ],
      freshFirstName: "",
      freshLastName: "",
      freshPosition: "",
      freshPersonEmail: "",
      freshSpecialities: '',
      freshHistory: '',
      freshBusinessOwner: "",

      formType: '',
      errorFirstName: '',
      errorLastName: '',
      errorPosition: '',
      errorEmail: '',
      freshprofilePicUrl: "",
      freshProfilePicId: "",
      freshprofileBranchLogoUrl: "",
      freshProfileBranchLogoId: "",
      freshProfileBranchBannerUrl: "",
      freshProfileBranchBannerId: "",
      freshPaymentSwitch: {},
      freshSelectedPayment: [],
    })

  }

  toggleEditPhone = (isTrue) => {
    if (isTrue && isTrue.id) {
      let { addPhone } = this.state;
      addPhone['ph1'] = isTrue.ph1
      addPhone['ph2'] = isTrue.ph2
      addPhone['ph3'] = isTrue.ph3
      addPhone['label'] = isTrue.label
      this.setState({ addPhone, isPhoneValid: false }, () => this.phoneValidation(addPhone));
    }
    this.setState({
      isEditPhone: isTrue && isTrue.id ? isTrue : false,
      isTogglePhone: !this.state.isTogglePhone
    });
  }

  toggleEditHours = (isTrue) => {
    if (isTrue && isTrue.id) {
      let { addHours } = this.state;
      addHours['day_of_week'] = isTrue.day_of_week
      addHours['end_time'] = isTrue.end_time
      addHours['start_time'] = isTrue.start_time
      addHours['entries'] = isTrue.entries
      addHours['id'] = isTrue.id
      addHours['info'] = isTrue.info
      this.setState({ addHours, isHoursValid: false }, () => this.hoursValidation(addHours));
    }
    this.setState({ isEditHours: isTrue && isTrue.id ? isTrue : false, isToggleHours: !this.state.isToggleHours })
  }

  hoursValidation = (addHours) => {
    let { isHoursValid } = this.state;
    let start_time = moment(addHours.start_time, "HH:mm:ss").format("HH:mm");
    let end_time = moment(addHours.end_time, "HH:mm:ss").format("HH:mm");

    // if (addHours.start_time !== '' && addHours.day_of_week !== '') {
    //     this.setState({ isHoursValid: false });
    // }
    if (moment(end_time, 'HH:mm').isBefore(moment(start_time, 'HH:mm')) && addHours['next_day'] === '0') {

      this.setState({ isHoursValid: true });
    }
    else {
      if (isHoursValid !== false) {
        this.setState({ isHoursValid: false });
      }
    }

    // else {
    //         this.setState({ isHoursValid: false });
    // }

  }

  handleSubmitPhone = () => {
    let { isEditPhone, branchId, addPhone: { ph1, ph2, ph3, label } } = this.state;
    let time = moment().format('HH:mm');
    let isAdd = true;
    let data = {
      "end_time": time,
      "id": branchId,
      "label": label,
      "ph1": ph1,
      "ph2": ph2,
      "ph3": ph3,
      "phone": `${ph1}${ph2}${ph3}`,
      "start_time": time,
      "type": "phone_form"
    }

    if (isEditPhone && isEditPhone.id) {
      isAdd = false;
      data.editform = true;
      data.entityid = isEditPhone.id
    }
    this.props.post_business_update(data, isAdd, branchId, 'copyBranch');
    this.toggleEditPhone();
  }

  phoneValidation = (addPhone) => {
    let { isPhoneValid } = this.state;

    let labels = ['Tel', 'Mob', 'Support'];
    if (addPhone['ph1'].length == 3 && addPhone['ph2'].length == 3 && addPhone['ph3'].length == 4 && labels.includes(addPhone['label'])) {
      this.setState({ isPhoneValid: false });
    } else {
      if (isPhoneValid !== true) {
        this.setState({ isPhoneValid: true });
      }
    }
  }

  handleChangePhone = (event) => {
    let { name, value } = event.target;
    let { addPhone } = this.state;
    addPhone[name] = value;
    this.setState({ addPhone }, () => this.phoneValidation(addPhone));
  }

  deleteToggle = (id, type) => {
    this.setState({
      isDeleteToggle: !this.state.isDeleteToggle,
      editRecordId: id ? id : false,
      editRecordType: type ? type : false
    });

  }

  PhoneData = () => {
    let phonelist = this.state.phoneDiary;
    if (phonelist.length > 0) {
      return phonelist.map((item, index) => {
        if (item?.phone) {
          item.ph1 = (item?.phone).slice(0, 3)
          item.ph2 = (item?.phone).slice(3, 6)
          item.ph3 = (item?.phone).slice(6, 10)
          return (
            <li className="mb-1" key={index}>
              <div className="d-flex mx-n1">
                <div className="px-1 flex-grow-1">
                  <div className="d-flex align-items-start" onClick={this.handleCheckChieldPhone}>
                    <CustomInput type="checkbox" id={"bus_info_phone_" + index} name="selectPhoneNumber1" checked={item.isChecked} onChange={() => { }} value={item.id} />
                    <Label for={"bus_info_phone_" + index} className="flex-fill font-weight-bold mb-0">
                      <span>{item.formated_phone}{" "}</span>
                      <span>{`[${item.label == "Tel" ? "Telephone" : item.label == "Mob" ? "Mobile" : item.label == "Support" ? "Support" : ""}]`}</span>
                    </Label>
                  </div>
                </div>
                <div className="px-1 flex-shrink-0 interactive">
                  <div className="interactive-appear text-nowrap">
                    <EditBtn onClick={() => {
                      this.toggleEditPhone(item);
                    }} />
                    <DeleteBtn onClick={() => this.deleteToggle(item.id, 'phone')} color="gold" />
                  </div>
                </div>
              </div>
            </li>

          )
        }
      }
      )
    }
  }


  toggleEditBusinessEmail = (isTrue) => {

    if (isTrue && isTrue.id) {
      let { addBusinessEmail } = this.state;
      addBusinessEmail['email'] = isTrue.email;
      addBusinessEmail['email_type'] = isTrue.email_type;
      this.setState({ addBusinessEmail, isBusinessEmailValid: false }, () => this.businessEmailValidation(addBusinessEmail));
    }
    this.setState({ isEditBusinessEmail: isTrue && isTrue.id ? isTrue : false, isToggleBusinessEmail: !this.state.isToggleBusinessEmail })
  }

  businessEmailValidation = (addBusinessEmail, hasError) => {
    let { isBusinessEmailValid } = this.state;
    let labels = ['Sales', 'Reserve', 'General'];

    if (addBusinessEmail.email !== '' && labels.includes(addBusinessEmail.email_type) && hasError !== true) {
      this.setState({ isBusinessEmailValid: false });
    } else {
      if (isBusinessEmailValid !== true) {
        this.setState({ isBusinessEmailValid: true });
      }
    }
  }

  handleChangeBusinessEmail = (event) => {
    let { name, value } = event.target;
    let hasError = this.refBusinessEmailForm.current._inputs[name].context.FormCtrl.hasError()
    let { addBusinessEmail } = this.state;
    addBusinessEmail[name] = value;
    this.setState({ addBusinessEmail }, () => this.businessEmailValidation(addBusinessEmail, hasError));
  }

  handleSubmitBusinessEmail = () => {
    let { isEditBusinessEmail, branchId, addBusinessEmail: { email, email_type } } = this.state;
    let time = moment().format('HH:mm');
    let isAdd = true;
    let data = {
      "email": email,
      "email_type": email_type,
      "type": "email_form",
      "id": branchId,
      "start_time": time,
      "end_time": time
    }

    if (isEditBusinessEmail && isEditBusinessEmail.id) {
      isAdd = false;
      data.editform = true;
      data.entityid = isEditBusinessEmail.id
    }

    this.props.post_business_update(data, isAdd, branchId, 'copyBranch');
    this.toggleEditBusinessEmail();
  }

  emailList = () => {
    let emailList = this.state.emailSet;
    if (emailList && emailList.length > 0) {

      return emailList.map((item, index) =>
        <li className="mb-1" key={index}>
          <div className="d-flex mx-n1">
            <div className="px-1 flex-grow-1">
              <div className="d-flex align-items-start" onClick={this.handleCheckChieldEmail}>
                <CustomInput type="checkbox" id={"bus_info_email_" + index} name="selectEmail1" checked={item.isChecked} onChange={() => { }} value={item.id} />
                <Label for={"bus_info_email_" + index} className="flex-fill font-weight-bold mb-0">
                  <span>{item.email}{" "}</span>
                  <span>[{item.email_type}]</span>
                </Label>
              </div>
            </div>
            <div className="px-1 flex-shrink-0 interactive">
              <div className="interactive-appear text-nowrap">
                {/* <ButtonGroup>
                  <Button onClick={() => this.toggleEditBusinessEmail(item)} size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button>
                  <Button onClick={() => this.deleteToggle(item.id, 'email')} size="sm" color="danger" title="Delete"><FontAwesomeIcon icon="trash-alt" /></Button>
                </ButtonGroup> */}
                <EditBtn onClick={() => this.toggleEditBusinessEmail(item)} />
                <DeleteBtn onClick={() => this.deleteToggle(item.id, 'email')} color="gold" />
              </div>
            </div>
          </div>
        </li>
      )
    }
  }

  payOptions = () => {
    const { freshSelectedPayment, preferredMethod } = this.state;
    let payOptions = this.state.payOptions;
    if (preferredMethod === 'freshData') {
      if (freshSelectedPayment && freshSelectedPayment.length > 0) {
        return freshSelectedPayment.map((item, index) => {
          // if (item.paymentoptions !== "Credit Card") {
          return (
            <div key={index} className="mx-1 mb-2">
              <span className="px-1 fs-20">
                <Badge color="secondary" className="px-2 font-weight-normal"> {item.name}</Badge>
              </span>
            </div>
          )
          // }
        })
      }
    } else {
      if (payOptions && payOptions.length > 0) {
        return payOptions.map((item, index) => {
          // if (item.paymentoptions !== "Credit Card") {
          return (
            <li className="mb-1 px-2" key={index}>

              <div className="d-flex mx-n1">
                <span className="px-1 fs-20" key={index}>
                  <Badge color="secondary" className="px-2 font-weight-normal">
                    <span className="mr-1 font-weight-normal text-truncate">{item.paymentoptions}</span>
                    <button
                      type="button" className="close btn-sm" aria-label="Close"
                      onClick={() => this.toggleDeletePayment(item)}
                    >
                      <span><FontAwesomeIcon icon="times-circle" /> </span>
                    </button>
                  </Badge>
                </span>
                {/* <div className="px-1">
                  <div className="d-flex align-items-start"
                   onClick={this.handleCheckChieldPayment}
                   >
                    <CustomInput type="checkbox" id={"bus_info_payment" + index} name="selectPayment1" checked={item.isChecked} onChange={() => { }} value={item.id} />
                    <Label for={"bus_info_payment" + index} className="flex-fill font-weight-bold mb-0">
                      {item.paymentoptions}
                    </Label>
                  </div>
                </div> */}
              </div>
            </li>
          )
          // }
        })
      }
    }

  }

  toggleDeletePayment = (data) => {
    let { branchId } = this.state;
    let editRecordId = data.id;
    this.props.delete_payment_option(editRecordId, branchId, 'copyBranch');
  }

  categoryList = () => {
    let categoryList = this.state.categoryList;
    if (categoryList && categoryList.length > 0) {
      return categoryList.map((item, index) => {
        return (
          <li className="mb-2 px-2 mw-100" key={index}>
            <div className="d-flex mx-n1">
              <div className="px-1 flex-grow-1 mw-100">
                <div className="d-flex align-items-start" onClick={this.handleCheckChieldCategory}>
                  <CustomInput type="checkbox" id={"bus_info_category_" + index} name="selectCategory1" checked={item.isChecked} onChange={() => { }} value={item.id} />
                  {/* <Label for={"bus_info_category_" + index} className="flex-fill font-weight-bold mb-0">
                    {item.category}
                  </Label> */}
                  <Badge color="secondary" className="d-flex align-items-center">
                    <span className="mr-1 fs-12 font-weight-normal text-truncate"
                    // onClick={() => this.handleCategory(item)}
                    >{item.category}</span>
                    <button
                      onClick={() => this.toggleDeleteCategory(item.id)}
                      type="button" className="close btn-sm" aria-label="Close" title="Delete">
                      <span><FontAwesomeIcon icon="times-circle" /> </span>
                    </button>
                  </Badge>
                </div>
              </div>
            </div>
          </li>
        )
      }
      )
    }
  }

  HoursOfOperation = () => {
    let HoursOfOperation = this.state.HoursOfOperation;
    let weekDays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    if (HoursOfOperation && Array.isArray(HoursOfOperation) && HoursOfOperation.length > 0) {
      // let filteredData = HoursOfOperation.sort((a, b) => {
      //     return (a.id < b.id) ? -1 : (a.id > b.id) ? 1 : 0;
      // });

      return HoursOfOperation.map((item, index) => {
        return (
          <li className="mb-1" key={index}>
            <div className="d-flex mx-n1">
              <div className="px-1 flex-grow-1">
                <div className="d-flex align-items-start" onClick={this.handleCheckChieldHours}>
                  <CustomInput type="checkbox" value={item.id} id={"bus_info_hour_" + index} name="selectHOP1" checked={item.isChecked} onChange={() => { }} />
                  <Label for={"bus_info_hour_" + index} className="flex-fill font-weight-bold mb-0">
                    <span>{weekDays[item.day_of_week - 1]}{" "}</span>
                    <span>[{item.start_time ? moment(item.start_time, 'HH:mm:ss').format('hh:mm A') : `Closed`} - {item.start_time ? moment(item.end_time, 'HH:mm:ss').format('hh:mm A') : ''}]{" "}</span>
                    <span>{item.info && <span className="font-weight-bold"> {`[${item.info}]`} </span>}</span>
                  </Label>
                </div>
              </div>
              <div className="px-1 flex-shrink-0 interactive">
                <div className="interactive-appear text-nowrap">
                  {/* <ButtonGroup> */}
                  {/* <Button size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button> */}
                  {/* <Button onClick={() => this.toggleDeleteHours(item.id)} size="sm" color="danger" title="Delete"><FontAwesomeIcon icon="trash-alt" /></Button> */}
                  {/* </ButtonGroup> */}
                  {/* <EditBtn color="gold" /> */}
                  <DeleteBtn onClick={() => this.toggleDeleteHours(item.id)} color="gold" />
                </div>
              </div>
            </div>
          </li>

        )
      }
      )
    }
  }

  businessAddresses = () => {

    let addressList = this.state.AdressDiary;
    if (addressList && addressList.length > 0) {

      return addressList.map((item, index) => {
        return (
          <li className="mb-1" key={index}>
            <div className="d-flex mx-n1">

              <div className="px-1 flex-grow-1">
                <div className="d-flex align-items-start">
                  <CustomInput type="checkbox"
                    value={item.id}
                    id={"bus_info_addresses_" + index} name="selectHOP1" checked={item.isChecked} onChange={() => { }}
                    onClick={this.handleCheckfieldAddress} />

                  <Label for={"bus_info_addresses_" + index} className="flex-fill font-weight-bold mb-0">
                    <span>{item.address1} {item.city} {item.state} {item.country}</span>
                  </Label>
                </div>
              </div>
              <div className="px-1 flex-shrink-0 interactive">
                <div className="interactive-appear text-nowrap">
                  {/* <ButtonGroup>
                    <Button size="sm" color="dark" title="Edit" onClick={() => this.toggleEditAddress(item)}><FontAwesomeIcon icon="edit" /> </Button>
                    <Button size="sm" color="danger" title="Delete" onClick={() => this.deleteToggle(item.id, 'address')}><FontAwesomeIcon icon="trash-alt" /></Button>
                  </ButtonGroup> */}
                  <EditBtn onClick={() => this.toggleEditAddress(item)} />
                  <DeleteBtn onClick={() => this.deleteToggle(item.id, 'address')} color="gold" />
                </div>
              </div>
            </div>
          </li>
        )
      })
    }
  }

  handleSubmitAddress = (event, errors, values) => {
    let { isEditAddress, branchId, addAddress: { address1, address2, city, country, state, zipcode, latitude, longitude } } = this.state;
    let time = moment().format('HH:mm');
    let isAdd = true;
    let data = {
      "end_time": time,
      "id": branchId,
      "address1": address1,
      "address2": address2,
      "city": city,
      "country": country,
      "state": state,
      "zipcode": zipcode,
      "start_time": time,
      "type": "address_form",
      "latitude": latitude,
      "longitude": longitude,
    }

    if (isEditAddress && isEditAddress.id) {
      isAdd = false;
      data.editform = true;
      data.entityid = isEditAddress.id
    }
    this.setState({ errors, values });
    if (this.state.errors.length == 0) {
      this.props.post_business_update(data, isAdd, branchId, 'copyBranch');
      this.toggleEditAddress();
    }
  }

  bizpecialities = () => {
    let bizpecialities = this.state.specialities;
    if (bizpecialities && Array.isArray(bizpecialities) && bizpecialities.length > 0 && bizpecialities[0].value !== "") {
      return (
        <span>
          <span>{bizpecialities[0].value}</span>
          {/* <textarea className="text-secondary-dark ff-alt" defaultValue={bizpecialities[0].value} hidden></textarea> */}
        </span>
      )
    } else if ((bizpecialities && Array.isArray(bizpecialities) && bizpecialities.length > 0 && bizpecialities[0].value === "") || bizpecialities === undefined) {
      return (
        <span>
          <i>Please enter Specialities about your business</i>
        </span>
      )
    }
    else if (bizpecialities === null) {
      return (
        <span>
          <i>No Data Available</i>
        </span>
      )
    }

    // else {

    //   return (
    //     <span>
    //       <i className="hover-blue" title="Click to Edit">No Data Available</i>
    //       {/* <textarea className="text-secondary-dark ff-alt" defaultValue="" hidden></textarea> */}
    //     </span>)
    // }
  }



  ownerMeet = () => {
    let ownerMeet = this.state.meetOwner;

    if (ownerMeet && Array.isArray(ownerMeet) && ownerMeet.length > 0 && ownerMeet[0].value.bio !== "") {
      return (
        <span>
          <span>{ownerMeet[0].value.bio}</span>
          {/* <textarea className="text-secondary-dark ff-alt" defaultValue={ownerMeet[0].value.bio} hidden></textarea> */}
        </span>
      )
    }
    else if ((ownerMeet && Array.isArray(ownerMeet) && ownerMeet.length > 0 && ownerMeet[0].value.bio === "") || ownerMeet === undefined) {
      return (
        <span>
          <i>Please enter history of your business</i>
        </span>
      )
    }
    else if (ownerMeet === null) {
      return (
        <span>
          <i>No Data Available</i>
        </span>
      )
    }

    // else {
    //   return (
    //     <span>
    //       <i>No Data Available</i>
    //       {/* <textarea className="text-secondary-dark ff-alt" defaultValue="" hidden></textarea> */}
    //     </span>)
    // }

  }

  Ownerhistory = () => {

    let Ownerhistory = this.state.businessOwnerhistory;
    if (Ownerhistory && Array.isArray(Ownerhistory) && Ownerhistory.length > 0 && Ownerhistory[0].value.history !== "") {

      return (
        <span>
          <span>{Ownerhistory[0].value.history}</span>
          {/* <textarea className="text-secondary-dark ff-alt" defaultValue={Ownerhistory[0].value.history} hidden></textarea> */}
        </span>)

    } else if ((Ownerhistory && Array.isArray(Ownerhistory) && Ownerhistory.length > 0 && Ownerhistory[0].value.history === "") || Ownerhistory === undefined) {
      return (
        <span>
          <i>Please enter history of your business</i>
        </span>
      )
    }
    else if (Ownerhistory === null) {
      return (
        <span>
          <i>No Data Available</i>
        </span>
      )
    }
  }

  EditSpec = () => {

    let editValue = this.state.specialities;

    this.setState(state => ({
      editSpec: !state.editSpec,
      editValue: editValue == undefined ? '' : editValue[0].value
    }));
  }

  handleAboutChange = (e) => {
    this.setState({
      editValue: e.target.value,
      // editHistory: e.target.value
    });
  }

  saveVaule = () => {
    let value = this.state.editValue;
    let id = this.state.branchId;

    let data = { "type": "specialties", "value": value, "update_additional_info": true };
    this.props.add_additional_value(id, data, 'copyBranch');
    this.EditSpec()
  }

  EditHistory = () => {
    let editHistoryData = this.state.businessOwnerhistory;

    this.setState(state => ({
      editHisSpec: !state.editHisSpec,
      editHistory: editHistoryData == undefined ? '' : editHistoryData[0].value.history
    }));
  }

  handleHistory = (e) => {
    this.setState({
      editHistory: e.target.value
    });
  }

  saveHistVaule = () => {
    let value = this.state.editHistory;
    let id = this.state.branchId;

    let data = { "type": "business_owner_history", "value": value, "update_additional_info": true };
    this.props.add_additional_value(id, data, 'copyBranch');
    this.EditHistory()
  }


  EditBussOwn = () => {
    let ownerMeet = this.state.meetOwner;

    this.setState(state => ({
      editBusiness: !state.editBusiness,
      EditBussOwn: ownerMeet == undefined ? '' : ownerMeet[0].value.bio
    }));
  }

  saveBussOwn = () => {
    let value = this.state.EditBussOwn;
    let id = this.state.branchId;

    let data = { "type": "meet_the_business_owner", "value": value, "update_additional_info": true };
    this.props.add_additional_value(id, data, 'copyBranch');
    this.EditBussOwn();
  }

  handleBussOwn = (e) => {
    this.setState({
      EditBussOwn: e.target.value
    });
  }

  toggleEditWebsite = (isTrue) => {

    if (isTrue && isTrue.id) {
      let { addWebsite } = this.state;
      addWebsite['website'] = isTrue.website;
      addWebsite['website_type'] = isTrue.website_type;
      this.setState({ addWebsite, isWebsiteValid: false }, () => this.websiteValidation(addWebsite));
    }

    this.setState({ isEditWebsite: isTrue && isTrue.id ? isTrue : false, isToggleWebsite: !this.state.isToggleWebsite })
  }

  websiteValidation = (addWebsite, hasError) => {
    let { isWebsiteValid } = this.state;
    let labels = ['Main', 'Facebook', 'Google+', 'Twitter', 'LinkedIn', 'Instagram'];

    if (addWebsite.website !== '' && labels.includes(addWebsite.website_type) && hasError !== true) {
      this.setState({ isWebsiteValid: false });
    } else {
      if (isWebsiteValid !== true) {
        this.setState({ isWebsiteValid: true });
      }
    }
  }

  handleChangeWebsite = (event) => {
    let { name, value } = event.target;
    let hasError = this.refWebsiteForm.current._inputs[name].context.FormCtrl.hasError()
    let { addWebsite } = this.state;
    addWebsite[name] = value;
    this.setState({ addWebsite }, () => this.websiteValidation(addWebsite, hasError));
  }

  handleChangeAddress = (event) => {
    let { name, value } = event.target;
    let { addAddress } = this.state;
    addAddress[name] = value;
    this.setState({ addAddress });
  }

  handleSubmitWebsite = () => {
    let { isEditWebsite, branchId, addWebsite: { website, website_type } } = this.state;
    let time = moment().format('HH:mm');
    let isAdd = true;
    let data = {
      "website": website,
      "website_type": website_type,
      "type": "website_form",
      "id": branchId,
      "start_time": time,
      "end_time": time
    }

    if (isEditWebsite && isEditWebsite.id) {
      isAdd = false;
      data.editform = true;
      data.entityid = isEditWebsite.id
    }

    this.props.post_business_update(data, isAdd, branchId, 'copyBranch');
    this.toggleEditWebsite();
  }

  websiteData = () => {
    let webList = this.state.webSet;
    const { isEditWebsite, addWebsite, isWebsiteValid, } = this.state;
    if (webList && webList.length > 0) {
      return webList.map((item, index) => {

        return (
          <li className="mb-1" key={index}>
            <div className="d-flex mx-n1">
              <div className="px-1 flex-grow-1">
                <div className="d-flex align-items-start" onClick={this.handleCheckChieldWebsite}>
                  <CustomInput type="checkbox" id={"bus_info_website_" + index} name="selectWebsite1" checked={item.isChecked} value={item.id} onChange={() => { }} />
                  <Label for={"bus_info_website_" + index} className="flex-fill font-weight-bold mb-0">
                    <span>{item.website}{" "}</span>
                    <span>[{item.website_type}]</span>
                  </Label>
                </div>
              </div>
              <div className="px-1 flex-shrink-0 interactive">
                <div className="interactive-appear text-nowrap">
                  {/* <ButtonGroup>
                    <Button onClick={() => this.toggleEditWebsite(item)} size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button>
                    <Button onClick={() => this.deleteToggle(item.id, 'website')} size="sm" color="danger" title="Delete"><FontAwesomeIcon icon="trash-alt" /></Button>
                    
                  </ButtonGroup> */}
                  <EditBtn onClick={() => this.toggleEditWebsite(item)} />
                  <DeleteBtn onClick={() => this.deleteToggle(item.id, 'website')} color="gold" />
                </div>
              </div>
            </div>
          </li>
        )
      })
    }
  }

  calltoactionData = () => {
    let corCallList = this.state.corporateCallList;
    if (corCallList && corCallList.length > 0) {
      return corCallList.map((item, index) =>
        <li
          hidden={item.corp_code === null ? true : false}
          className="mb-2" key={index}>
          <div className="mb-2 d-flex mx-n1">
            <div className="px-1 flex-grow-1">
              <div className="d-flex" onClick={this.handleCheckChieldAction}>
                <CustomInput type="checkbox" id={"select_c2a_" + index} name="selectC2A1" checked={item.isChecked} onChange={() => { }} value={item.id || ''} />

                <Label for={"select_c2a_" + index} className="font-weight-bold mb-0 flex-fill">
                  {/* {item.selected_type} */}
                  <div className="text-primary-dark font-weight-normal fs-14">
                    <span>Your current Call to Action button is <strong className="font-weight-bold">{item.selected_type}</strong></span>
                    <br />
                    <strong>Last update:</strong>
                    <span> {moment(item.last_update).format('MMMM DD YYYY')}</span>
                  </div>
                  <div className="bg-primary p-2">
                    <div className="d-flex mx-n2">
                      {/* <div className="px-2">
                        <img width="50" src={"https://enterprise.staging.wikireviews.com/" + item?.image_link} alt="" />
                      </div> */}
                      <div className="px-2 flex-fill">
                        <div className="text-center">
                          <h3 className="mb-2 text-dark">{item.headline}</h3>
                          <h4 className="mb-2 text-white">{item.sub_headline}</h4>

                          <Button color="dark">{item.selected_type}</Button>
                        </div>
                      </div>
                    </div>
                  </div>
                </Label>
              </div>
            </div>

            <div className="px-1 flex-shrink-0 interactive">
              <div className="interactive-appear text-nowrap">
                {/* <EditBtn /> */}
                <DeleteBtn
                  onClick={() => {
                    this.deleteCallToAction(item)
                  }}
                  color="gold" />
              </div>
            </div>
          </div>
          <ul hidden className="pl-4 fs-12 list-unstyled">
            <li className="mb-2">

              <div className="d-flex mx-n1">
                <div className="px-1 flex-grow-1">
                  <Label className="font-weight-bold mb-0">
                    Headline: <span className="hover-blue" title="Click to Edit">{item.headline}</span>
                    <textarea className="text-secondary-dark ff-alt" defaultValue={item.headline} hidden ></textarea>
                  </Label>
                </div>
                {/* unhide for edit functionality */}
                <div className="ml-auto col-auto px-1" hidden>
                  <div className="interactive">
                    <div className="interactive-appear text-nowrap">
                      {/* <ButtonGroup>
                        <Button size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button>
                      </ButtonGroup> */}
                      <EditBtn color="gold" />
                      {/* <DeleteBtn color="gold" /> */}
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li className="mb-2">
              <div className="d-flex mx-n1">
                <div className="px-1 flex-grow-1">
                  <Label className="font-weight-bold mb-0">
                    Subheadline: <span className="hover-blue" title="Click to Edit">{item.sub_headline}</span>
                    <textarea className="text-secondary-dark ff-alt" defaultValue={item.sub_headline} hidden></textarea>
                  </Label>
                </div>
                {/* unhide for edit functionality */}
                <div className="ml-auto col-auto px-1" hidden>
                  <div className="interactive">
                    <div className="interactive-appear text-nowrap">
                      {/* <ButtonGroup>
                        <Button size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button>
                      </ButtonGroup> */}
                      <EditBtn color="gold" />
                      {/* <DeleteBtn color="gold" /> */}
                    </div>
                  </div>
                </div>
              </div>
            </li>
          </ul>

        </li>
      )
    }
  }

  deleteCallToAction = (data) => {
    const { delete_corporate_call } = this.props;
    const { branchId, corporateCallList } = this.state;
    delete_corporate_call(data.corp_code, branchId, 'copyBranch');
    if (corporateCallList.length === 1) {
      this.setState({
        corporateCallList: [],
        setup: true,
        getStarted: false,
        isOpenRow: false,
        headline: '',
        sub_headline: ''
      });
    }
  }

  // Preview Functions 
  previewpayOptions = () => {
    const { freshSelectedPayment } = this.state;

    let payOptions = this.state.payOptions;
    if (this.state.preferredMethod === "freshData") {
      if (freshSelectedPayment && freshSelectedPayment.length > 0) {
        return freshSelectedPayment.map((item, index) => {
          return (
            <li className="mb-1 px-1" key={index}>
              {item.name}
              <span>&nbsp;|</span>
            </li>
          )
        })
      } else {
        return (<div>  <span className="small"><i>{'No Payment Option'}</i></span>  </div>)
      }
    } else {
      if (payOptions && payOptions.length > 0) {
        return payOptions.map((item, index) => {
          if (item.isChecked == true) {
            return (
              <li className="mb-1 px-1" key={index}>
                {item.paymentoptions}
                <span>&nbsp;|</span>
              </li>
            )
          }
        })
      } else {
        return (<div>  <span className="small"><i>{'No Payment Option'}</i></span>  </div>)
      }
    }

  }

  previewHours = () => {
    let HoursOfOperation = this.state.HoursOfOperation;
    let weekDays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    if (HoursOfOperation && Array.isArray(HoursOfOperation) && HoursOfOperation.length > 0) {
      return HoursOfOperation.map((item, index) => {
        if (item.isChecked == true && item?.start_time) {
          return (
            <li className="mb-1" key={index}>
              <div className="d-flex flex-wrap">
                <div className="mr-2">{weekDays[item.day_of_week - 1]} </div>
                <div>
                  <div className="d-flex flex-wrap">
                    <span className="mr-2">{item.start_time ? moment(item.start_time, 'HH:mm:ss').format('hh:mm A') : `[Closed]`} - {item.start_time ? moment(item.end_time, 'HH:mm:ss').format('hh:mm A') : ''}</span><span>{item.info && <span className="font-weight-bold"> {`[${item.info}]`} </span>}</span>
                  </div>
                </div>
              </div>
            </li>
          )
        } else if (item?.freshHOPDay) {
          return (
            <li className="mb-1" key={index}>
              <div className="d-flex flex-wrap">
                <div className="mr-2">{weekDays[item.freshHOPDay - 1]} </div>
                <div>
                  <div className="d-flex flex-wrap">
                    <span className="mr-2">{item.freshHOPStartTime ? moment(item.freshHOPStartTime, 'HH:mm:ss').format('hh:mm A') : `[Closed]`} - {item.freshHOPStartTime ? moment(item.freshHOPEndTime, 'HH:mm:ss').format('hh:mm A') : ''}</span><span>{item.freshHOPText && <span className="font-weight-bold"> {`[${item.freshHOPText}]`} </span>}</span>
                  </div>
                </div>
              </div>
            </li>
          )

        }
      })
    } else {
      return (<div>  <span className="small"><i>{'No Hours'}</i></span>  </div>)
    }

  }

  previewSpecialOffer = () => {
    let specialofferSet = this.state.specialofferSet;
    if (specialofferSet && specialofferSet.length > 0) {
      return specialofferSet.map((item, index) => {
        if (item.isChecked == true && item.special_offer) {
          return (
            <li className="mb-2" key={index}>
              <FontAwesomeIcon icon="check-circle" fixedWidth className="mr-2 text-primary" />
              {item.special_offer}
              <div className="pl-4 mt-1 fs-12">
                Valid until {moment(item.to_date).format('MMMM DD YYYY')}
              </div>
            </li>
          )
        } else if (item?.freshOfferText) {

          return (
            <li className="mb-2" key={index}>
              <FontAwesomeIcon icon="check-circle" fixedWidth className="mr-2 text-primary" />
              {item.freshOfferText}
              <div className="pl-4 mt-1 fs-12">
                Valid until {moment(item.freshOfferEndDate).format('MMMM DD YYYY')}
              </div>
            </li>
          )
        }
      })
    } else {
      return (<div>  <span className="small"><i>{'No Special Offer'}</i></span>  </div>)
    }

  }

  previewcalltoactionData = () => {
    let corCallList = this.state.corporateCallList;
    if (corCallList && corCallList.length > 0) {
      return corCallList.map((item, index) => {
        if (item.isChecked == true) {
          return (
            <li className="mb-2" key={index}>
              <FontAwesomeIcon icon="check-circle" fixedWidth className="mr-2 text-primary" />
				  Type of Call to Action: {item.selected_type}
              <ul className="pl-4 fs-12 list-unstyled">
                <li className="mb-2">
                  <FontAwesomeIcon icon="fas-none" fixedWidth className="mr-2 text-primary" />
					  Headline: <span> {item.headline}</span>
                </li>
                <li className="mb-2">
                  <FontAwesomeIcon icon="fas-none" fixedWidth className="mr-2 text-primary" />
					  Subheadline: <span>{item.sub_headline}</span>
                </li>
                <li className="mb-2">
                  <FontAwesomeIcon icon="fas-none" fixedWidth className="mr-2 text-primary" />
					  Linked URL: <span>{item.linked_url}</span>
                </li>
              </ul>
            </li>)
        }
      })
    }
  }

  previewAdditional = () => {
    // let previewAditional = this.state.previewAditional;
    let previewAditional = this.state.preferredMethod === 'freshData' ?
      this.state.freshSelectAdditionalInfo : this.state.previewAditional;
    if (previewAditional.length > 0) {
      return previewAditional.map((item, index) => {
        if (item?.value[0]?.value !== '') {
          return (
            <li className="mb-2" key={index}>
              <FontAwesomeIcon icon="check-circle" fixedWidth className="mr-2 text-primary" />
              <span>{item?.name}:</span>
              <span>&nbsp;</span>
              <span>{item?.value[0]?.value}</span>
            </li>
          )
        }
      })
    } else {
      return (<div>  <span className="small"><i>{'No Additonal Information'}</i></span>  </div>)
    }

  }

  previewCategory = () => {
    let categoryList = this.state.categoryList;
    if (categoryList && categoryList.length > 0) {
      return categoryList.map((item, index) => {
        if (item.isChecked == true) {
          return (
            <li className="mb-1 px-1" key={index}>
              {item.category}
              <span>&nbsp;|</span>
            </li>
          )
        }
      })
    } else {
      return (<div>  <span className="small"><i>{'No Category'}</i></span>  </div>)
    }

  }

  previewEmail = () => {
    let emailList = this.state.emailSet;
    if (emailList && emailList.length > 0) {
      return emailList.map((item, index) => {
        if (item.isChecked == true && item?.email) {
          return (
            <li className="mb-1" key={index}>
              {item.email} [{item.email_type}]
            </li>
          )
        } else if (item?.freshemailText) {
          return (
            <li className="mb-1" key={index}>
              {item.freshemailText} [{item.freshEmailType}]
            </li>
          )
        }
      })
    } else {
      return (<div>  <span className="small"><i>{'No Email'}</i></span>  </div>)
    }

  }

  previewPhone = () => {
    let phonelist = this.state.phoneDiary;
    if (phonelist && phonelist.length > 0) {
      return phonelist.map((item, index) => {
        if (item.isChecked == true && item?.phone) {
          item.ph1 = (item.phone).slice(0, 3)
          item.ph2 = (item.phone).slice(3, 6)
          item.ph3 = (item.phone).slice(6, 10)
          return (
            <li className="mb-1" key={index}>
              {item.formated_phone} {` [${item.label == "Tel" ? "Telephone" : item.label == "Mob" ? "Mobile" : item.label == "Support" ? "Support" : ""}]`}
            </li>
          )
        } else if (item?.phoneFresh) {
          return (
            <li className="mb-1" key={index}>
              {this.formatPhoneNumber((item.phoneFresh).replace("+1", ""))} {` [${item.freshPhoneType == "Tel" ? "Telephone" : item.freshPhoneType == "Mob" ? "Mobile" : item.freshPhoneType == "Support" ? "Support" : ""}]`}
            </li>
          )
        }
      })
    } else {
      return (<div>  <span className="small"><i>{'No Phone Number'}</i></span>  </div>)
    }

  }

  previewCategory = () => {
    let categoryList = this.state.categoryList;
    if (categoryList && categoryList.length > 0) {
      return categoryList.map((item, index) => {
        if (item.isChecked == true) {
          return (
            <li className="mb-1 px-1" key={index}>
              {item.category}
              <span>&nbsp;|</span>
            </li>
          )
        }
      })
    } else {
      return (<div>  <span className="small"><i>{'No Category'}</i></span>  </div>)
    }

  }

  Previewbizpecialities = () => {
    let bizpecialities = this.state.specialities;
    let freshSpecialities = this.state.freshSpecialities;
    if (bizpecialities && Array.isArray(bizpecialities) && bizpecialities.length > 0 && bizpecialities[0].value !== "") {
      if (bizpecialities[0].isChecked == true) {
        return (
          <div className="pl-4 mt-1 fs-12">
            {bizpecialities[0].value}
          </div>)
      }

    } else if (freshSpecialities && freshSpecialities != "") {
      return (
        <div className="pl-4 mt-1 fs-12">
          {freshSpecialities}
        </div>)
    } else {

      return (
        <div>  <span className="small"><i>{'No Data Available'}</i></span>  </div>)
    }
  }



  PreviewownerMeet = () => {
    let ownerMeet = this.state.meetOwner;
    let freshBusinessOwner = this.state.freshBusinessOwner;
    if (ownerMeet && Array.isArray(ownerMeet) && ownerMeet.length > 0 && ownerMeet[0].value.bio !== "") {
      if (ownerMeet[0].isChecked == true) {
        return (
          <div className="pl-4 mt-1 fs-12">
            <span>{ownerMeet[0].value.bio}</span>
          </div>
        )
      }
    } else if (freshBusinessOwner && freshBusinessOwner != "") {
      return (
        <div className="pl-4 mt-1 fs-12">
          {freshBusinessOwner}
        </div>)
    } else {

      return (
        <div>  <span className="small"><i>{'No Data Available'}</i></span>  </div>)
    }

  }

  PreviewOwnerhistory = () => {

    let Ownerhistory = this.state.businessOwnerhistory;
    let freshHistory = this.state.freshHistory;
    if (Ownerhistory && Array.isArray(Ownerhistory) && Ownerhistory.length > 0 && Ownerhistory[0].value.history !== "") {
      if (Ownerhistory[0].isChecked == true) {
        return (
          <div className="pl-4 mt-1 fs-12">
            <span>{Ownerhistory[0].value.history}</span>
          </div>
        )
      }
    } else if (freshHistory && freshHistory != "") {
      return (
        <div className="pl-4 mt-1 fs-12">
          {freshHistory}
        </div>)
    } else {

      return (
        <div>  <span className="small"><i>{'No Data Available'}</i></span>  </div>)
    }

  }

  previewWebsite = () => {
    let webList = this.state.webSet;
    if (webList && webList.length > 0) {
      return webList.map((item, index) => {
        if (item.isChecked == true && item?.website) {
          return (
            <li className="mb-1" key={index}>
              {item.website} [{item.website_type}]
            </li>
          )
        } else if (item?.freshWebsiteURL) {
          return (
            <li className="mb-1" key={index}>
              {item.freshWebsiteURL} [{item.freshWebsiteType}]
            </li>
          )
        }
      })
    } else {
      return (<div>  <span className="small"><i>{'No Website'}</i></span>  </div>)
    }

  }

  previewAddress = () => {
    let AdressDiary = this.state.AdressDiary;
    if (AdressDiary && AdressDiary.length > 0) {
      return AdressDiary.map((item, index) => {
        if (item.isChecked == true && item?.address1) {
          return (
            <li className="mb-1" key={index}>
              <div className="d-flex mx-n1">
                <div className="px-1 flex-grow-1">
                  <div className="d-flex align-items-start">
                    <Label for={"bus_info_addresses_" + index} className="flex-fill font-weight-bold mb-0">
                      <span>{item.address1} {item.city} {item.state} {item.country} {item.zipcode}</span>
                    </Label>
                  </div>
                </div>
              </div>
            </li>
          )
        } else if (item?.freshAddress1) {
          return (
            <li className="mb-1" key={index}>
              <div className="d-flex mx-n1">
                <div className="px-1 flex-grow-1">
                  <div className="d-flex align-items-start">
                    <Label for={"bus_info_addresses_" + index} className="flex-fill font-weight-bold mb-0">
                      <span>{item.freshAddress1} {item.freshAddress2} {item.freshAddressCity} {item.freshAddressState} {item.freshAddressCountry} {item.freshAddressZipcode}</span>
                    </Label>
                  </div>
                </div>
              </div>
            </li>
          )

        }
      })
    } else {
      return (<div>  <span className="small"><i>{'No Address Found'}</i></span>  </div>)
    }

  }

  previewOwnerInfo = () => {
    let item = this.state.selectedBusiness;
    let { freshFirstName, freshLastName, freshPosition, freshPersonEmail, freshprofilePicUrl } = this.state;
    //let selectedBusiness = this.state.selectedBusiness;
    //if (selectedBusiness && selectedBusiness.length > 0) {
    if (freshFirstName != "" && freshLastName != "" && freshPosition != "") {
      return (<ul className="pl-4 list-unstyled mt-1 fs-12">
        <li className="mb-1">
          Profile Pic: <img width="80"
            src={freshprofilePicUrl && freshprofilePicUrl != "" ? freshprofilePicUrl : require("../../assets/images/icons/user-circle.png")}
            alt={freshFirstName} />
        </li>
        <li className="mb-1">
          Name : <span>{freshFirstName}</span>
        </li>
        <li className="mb-1">
          Position: <span>{freshPosition}</span>
        </li>
      </ul>
      )
    } else if (item) {
      //return selectedBusiness.map((item, index) => {
      return (
        <ul className="pl-4 list-unstyled mt-1 fs-12">
          <li className="mb-1">
            Profile Pic: <img width="80" src={item.profile && item.profile != "" ? item.profile : "https://stagingdatawikireviews.s3.amazonaws.com/images/circle.png"} alt="Profile Pic" />
          </li>
          <li className="mb-1">
            Position: <span>{item.position && item.position != "" ? item.position : ""}</span>
          </li>
          <li className="mb-1">
            Email: <span>{item.email && item.email != "" ? item.email : ""}</span>
          </li>
        </ul>
      )
      //})  
    } else {
      return (<div>  <span className="small"><i>{'No Info Available'}</i></span>  </div>)
    }

  }

  toggleEditOffer = (isTrue) => {
    if (isTrue && isTrue.id) {
      let { addOffer } = this.state;
      addOffer['special_offer'] = isTrue.special_offer;
      addOffer['offer_type'] = isTrue.offer_type;
      addOffer['from_date'] = isTrue.from_date;
      addOffer['to_date'] = isTrue.to_date;
      addOffer['claim_deal_link'] = isTrue.claim_deal_link
      //addOffer['claim_deal_link'] = "None"
      this.setState({ addOffer });
    }
    this.setState({ isEditOffer: isTrue && isTrue.id ? isTrue : false, isToggleOffer: !this.state.isToggleOffer })
  }

  handleChangeOffer = (event) => {
    let { name, value } = event.target;
    let { addOffer } = this.state;
    addOffer[name] = value;
    this.setState({ addOffer });
  }

  handleSubmitOffer = (event, errors, values) => {
    let { isEditOffer, branchId, addOffer: { special_offer, offer_type, from_date, to_date, claim_deal_link }, user_id } = this.state;
    // let time = moment().format('HH:mm');
    let isAdd = true;
    let data = {
      "special_offer": special_offer,
      "offer_type": offer_type,
      "from_date": moment(from_date).format('MM-DD-YYYY'),
      "to_date": moment(to_date).format('MM-DD-YYYY'),
      "claim_deal_link": claim_deal_link,
      "name": "",
      "reviews_userentries": branchId,
      "created_by": user_id,
    }
    this.setState({ errors, values });
    if (isEditOffer && isEditOffer.id) {
      isAdd = false;
      data.editform = true;
      data.entityid = isEditOffer.id
      if (this.state.errors.length === 0) {
        this.props.update_special_offer(data, branchId, 'copyBranch');
        this.setState({
          addOffer: { special_offer: "", offer_type: "", from_date: "", to_date: "", claim_deal_link: "" },
        })
        this.toggleEditOffer();
      }
    } else {
      if (this.state.errors.length === 0) {
        this.props.post_special_offer(data, isAdd, branchId, 'copyBranch');
        this.setState({
          addOffer: { special_offer: "", offer_type: "", from_date: "", to_date: "", claim_deal_link: "" },
        })
        this.toggleEditOffer();
      }
    }
  }

  toggleDeleteOffers = (id) => {
    this.setState({
      isToggleDeleteOffers: !this.state.isToggleDeleteOffers,
      editRecordId: id ? id : false,
    });
  }

  deleteSpecialOffers = () => {
    let { editRecordId, branchId } = this.state;
    this.props.delete_offer(editRecordId, branchId, 'copyBranch');
    this.toggleDeleteOffers();
  }

  setInitialOffer = () => {
    this.toggleEditOffer();
    this.setState({ addOffer: { special_offer: '', offer_type: '', from_date: '', to_date: '', claim_deal_link: '' } });
  }
  setInitialPhone = () => {
    this.toggleEditPhone();
    this.setState({ addPhone: { ph1: '', ph2: '', ph3: '', label: '' }, isPhoneValid: true })
  }
  setInitialAddress = () => {
    this.toggleEditAddress();
    this.setState({ addAddress: { address1: '', address2: '', city: '', state: '', country: '', zipcode: '', latitude: '', longitude: '' } })
  }

  setInitialWebsite = () => {
    this.toggleEditWebsite();
    this.setState({ addWebsite: { website: '', website_type: '' }, isWebsiteValid: true });
  }

  setInitialBusinessEmail = () => {
    this.toggleEditBusinessEmail();
    this.setState({ addBusinessEmail: { email: '', email_type: '' }, isBusinessEmailValid: true });
  }

  setInitialHours = (startTime, endTime) => {
    this.toggleEditHours();
    this.setState({ addHours: { day_of_week: 1, end_time: endTime, start_time: startTime, entries: "", id: "", info: "" }, isHoursValid: false });
  }

  deleteBusinessRecord = () => {
    let { branchId, editRecordId, editRecordType } = this.state;
    this.props.delete_business_record(editRecordType, editRecordId, branchId, 'copyBranch');
    this.deleteToggle();
  }

  setPaymentReset = () => {
    let { payOptions } = this.state;
    let paymentSwitch = {};
    if (payOptions && payOptions.length > 0) {

      payOptions.forEach(item => {
        if (item.paymentoptions == "Cash") {
          paymentSwitch['cash'] = true
        }

        if (item.paymentoptions == "Check") {
          paymentSwitch['check'] = true
        }

        if (item.paymentoptions == "Visa") {
          paymentSwitch['visa'] = true
        }

        if (item.paymentoptions == "Mastercard") {
          paymentSwitch['mastercard'] = true
        }

        if (item.paymentoptions == "Discover") {
          paymentSwitch['discover'] = true
        }

        if (item.paymentoptions == "American Express") {
          paymentSwitch['american'] = true
        }

        if (item.paymentoptions == "Apple Pay") {
          paymentSwitch['apple'] = true
        }

        if (item.paymentoptions == "Google Wallet") {
          paymentSwitch['google'] = true
        }

        if (item.paymentoptions == "Cryptocurrency") {
          paymentSwitch['cryptocurrency'] = true
        }

        if (item.paymentoptions == "Debit Card") {
          paymentSwitch['debit'] = true
        }
      })
      this.setState({ paymentSwitch, clonePaymentSwitch: paymentSwitch })
    }
  }

  toggleEditAddress = (isTrue) => {

    if (isTrue && isTrue.id) {
      let { addAddress } = this.state;
      addAddress['address1'] = isTrue.address1;
      addAddress['address2'] = isTrue.address2;
      addAddress['city'] = isTrue.city;
      addAddress['state'] = isTrue.state;
      addAddress['country'] = isTrue.country;
      addAddress['zipcode'] = isTrue.zipcode;
      addAddress['latitude'] = isTrue.latitude;
      addAddress['longitude'] = isTrue.longitude;
      this.setState({ addAddress });
    }

    this.setState({ isEditAddress: isTrue && isTrue.id ? isTrue : false, isToggleAddress: !this.state.isToggleAddress })
  }

  togglePayment = () => {
    let { isTogglePayment, paymentSwitch } = this.state;
    this.setState({ isTogglePayment: !isTogglePayment })
  }
  handleToggleFreshPayment = () => {
    let { isToggleFreshPayment } = this.state;
    this.setPaymentReset();
    this.setState({ isToggleFreshPayment: !isToggleFreshPayment });
  }

  handleSubmitPayments = () => {
    let { paymentSwitch, branchId } = this.state;
    let data = { selected_pay: [], not_selected: [] };

    [paymentSwitch].forEach(item => {
      if (item.cash == true) {
        data.selected_pay.push('Cash')
      } else {
        data.not_selected.push('Cash')
      }

      if (item.check == true) {
        data.selected_pay.push('Check')
      } else {
        data.not_selected.push('Check')
      }

      if (item.visa == true) {
        data.selected_pay.push('Visa')
      } else {
        data.not_selected.push('Visa')
      }

      if (item.mastercard == true) {
        data.selected_pay.push('Mastercard')
      } else {
        data.not_selected.push('Mastercard')
      }


      if (item.discover == true) {
        data.selected_pay.push('Discover')
      } else {
        data.not_selected.push('Discover')
      }


      if (item.american == true) {
        data.selected_pay.push('American Express')
      } else {
        data.not_selected.push('American Express')
      }

      if (item.apple == true) {
        data.selected_pay.push('Apple Pay')
      } else {
        data.not_selected.push('Apple Pay')
      }

      if (item.google == true) {
        data.selected_pay.push('Google Wallet')
      } else {
        data.not_selected.push('Google Wallet')
      }

      if (item.debit == true) {
        data.selected_pay.push('Debit Card')
      } else {
        data.not_selected.push('Debit Card')
      }

      if (item.cryptocurrency == true) {
        data.selected_pay.push('Cryptocurrency')
      } else {
        data.not_selected.push('Cryptocurrency')
      }
    });

    this.togglePayment();
    this.props.update_payment_options(branchId, data, 'copyBranch');
  }

  handleSwitch = (value, name) => {
    let { paymentSwitch } = this.state;
    paymentSwitch[name] = !paymentSwitch[name];
    this.setState({ paymentSwitch });
  }

  handleFreshPaymentSwitch = async (value, name, label) => {
    let { freshPaymentSwitch, freshSelectedPayment } = this.state;
    let removeIndex = '';

    freshSelectedPayment.length > 0 && freshSelectedPayment.map((selected, index) => {
      if (selected.name === label && !freshPaymentSwitch[name] === false) {
        removeIndex = index;
      }
      return false;
    })
    if (removeIndex === '') {
      await freshSelectedPayment.push({
        name: label, value: !freshPaymentSwitch[name]
      })
    }
    else {
      freshSelectedPayment.splice(removeIndex, 1);
    }
    freshPaymentSwitch[name] = !freshPaymentSwitch[name];
    this.setState({ freshPaymentSwitch });
  }

  modalToggle = () => {
    this.setState({ CopyBranchAddmodal: !this.state.CopyBranchAddmodal }, () => {
      if (this.state.CopyBranchAddmodal) {
        this.props.get_business_category_data('Business');
      } else {
        this.setState({
          businessCategory: [],
          businessSubCategory: [],
          totalSubCategories: [1],
          anotherCategories: [],
          anotherSubCategories: [],
          postCategoryList: {},
          seletedCategories: [],
          business_specialities: ''
        });
      }
    });
  }

  handleChangeCategory = (e) => {
    let { name, value, label } = e.target;
    let { postCategoryList, seletedCategories, preferredMethod, seletedCategoriesNames } = this.state;
    postCategoryList[[value]] = [];
    // var selectedText =
    if (preferredMethod === 'freshData') {
      let index = e.nativeEvent.target.selectedIndex;
      let categoryLabel = e.nativeEvent.target[index].text
      seletedCategoriesNames.push({
        label: categoryLabel,
        value: value
      })
    }
    seletedCategories.push(value)
    var val = e.target.getAttribute("data-id");
    this.setState({
      postCategoryList,
      seletedCategories,
      seletedCategoriesNames,
      subCategoryZero: val
    }, () => {
      this.props.get_business_category_data(value)
    })
  }

  addSubCategories = () => {
    let { totalSubCategories, businessSubCategory } = this.state;
    if (businessSubCategory[0] && businessSubCategory[0].length > 0) {
      totalSubCategories.push(1);
      this.setState({ totalSubCategories })
    }
  }

  removeSubCateItems = (index) => {
    let { totalSubCategories, postCategoryList, seletedCategories, seletedCategoriesNames, preferredMethod } = this.state;
    if (index > -1) {
      let categoryId = seletedCategories[0];
      postCategoryList[categoryId].splice(index, 1);
      totalSubCategories.splice(index, 1);
      this.setState({ totalSubCategories })
    }
  }

  addAnotherCategories = () => {
    let { anotherCategories } = this.state;
    let index = anotherCategories.length;
    anotherCategories[index] = [];
    this.setState({ anotherCategories })
  }


  addAnotherSubCategories = (index) => {
    let { anotherCategories, businessSubCategory } = this.state;
    if (businessSubCategory[index + 1] && businessSubCategory[index + 1].length > 0) {
      anotherCategories[index].push(1);
      this.setState({ anotherCategories })
    }
  }

  removeSubCategoryItem = (index, subIndex) => {
    let { anotherCategories, postCategoryList, seletedCategories } = this.state;
    let categoryId = seletedCategories[index];
    postCategoryList[categoryId].splice(subIndex, 1);
    anotherCategories[index - 1].splice(subIndex, 1);
    this.setState({ anotherCategories });
  }

  changeSubCategory = (e, index, subIndex) => {
    let { name, value } = e.target;
    let { postCategoryList, seletedCategories, seletedCategoriesNames, preferredMethod } = this.state;

    if (preferredMethod === 'freshData') {
      let index = e.nativeEvent.target.selectedIndex;
      let categoryLabel = e.nativeEvent.target[index].text
      seletedCategoriesNames.push({
        label: categoryLabel,
        value: value
      })
    }

    if (index !== 'undefined') {
      let categoryId = seletedCategories[index];
      postCategoryList[categoryId][subIndex] = value
    } else {
      // undefined
      let categoryId = seletedCategories[0];
      postCategoryList[categoryId][subIndex] = value
    }
    this.setState({ postCategoryList, seletedCategoriesNames })
  }

  handleSubmitCategories = async () => {
    let { postCategoryList, branchId, business_specialities } = this.state;
    let data = {
      "add_category": true,
      "categoryList": postCategoryList,
      "specialties": business_specialities,
      "listing_id": branchId
    }
    if (this.state.preferredMethod === "freshData") {
      await this.setState({
        CopyBranchAddmodal: !this.state.CopyBranchAddmodal,
        businessCategory: [],
        businessSubCategory: [],
        totalSubCategories: [1],
        anotherCategories: [],
        anotherSubCategories: [],
        postCategoryList: {},
        seletedCategories: [],
        business_specialities: ''
      });
    } else {
      this.props.post_business_category_data(data, branchId, 'copyBranch');
      this.modalToggle();
    }
  }

  toggleDeleteCategory = (id) => {
    this.setState({
      isToggleDeleteCategory: !this.state.isToggleDeleteCategory,
      editRecordId: id ? id : false,
    });
  }

  toggleDeleteHours = (id) => {
    this.setState({
      isToggleDeleteHours: !this.state.isToggleDeleteHours,
      editRecordId: id ? id : false,
    });
  }


  deleteCategory = () => {
    let { editRecordId, branchId, preferredMethod, seletedCategoriesNames } = this.state;
    if (preferredMethod === 'freshData') {
      let removeIndex = '';
      seletedCategoriesNames.length > 0 && seletedCategoriesNames.map((selected, index) => {
        if (selected.value === editRecordId) {
          removeIndex = index;
        }
        return false;
      })
      seletedCategoriesNames.splice(removeIndex, 1);
      this.toggleDeleteCategory();
    } else {
      this.props.delete_business_categories(editRecordId, branchId, 'copyBranch')
      this.toggleDeleteCategory();
    }
  }

  deleteHoursOfOperations = () => {
    let { editRecordId, branchId } = this.state;
    this.props.delete_hours_of_operation(editRecordId, branchId, 'copyBranch');
    this.toggleDeleteHours();
  }

  handleChangeHours = (event) => {
    let { name, value } = event.target;
    let { addHours, isEditHours } = this.state;
    if (value[value.length - 1] === "1") {
      addHours["next_day"] = value[value.length - 1]
      this.setState({ addHours })
    }
    else {
      addHours["next_day"] = "0";
      this.setState({ addHours })
    }
    addHours[name] = value;
    this.setState({ addHours, isEditHours }, () => this.hoursValidation(addHours));
  }

  handleSubmitHours = async () => {
    let { isEditHours, branchId, HoursOfOperation, addHours, addHours: { day_of_week, end_time, start_time, entries, id, info, next_day } } = this.state;
    let data = {
      day_of_week,
      end_time: moment(end_time, "HH:mm:ss").format("HH:mm"),
      start_time: moment(start_time, "HH:mm:ss").format("HH:mm"),
      info: addHours?.info ? addHours.info : '',
      entries_id: branchId,
      next_day: parseInt(addHours['next_day'])
    }

    let brunchStart = '09:59';
    let brunchEnd = '11:31';

    let lunchStart = '11:59';
    let lunchEnd = '15:01'; // 3:00 pm

    let dinnerStart = '17:59'; // 6:00 pm
    let dinnerEnd = '22:01';  // 10:00 pm

    if (
      moment(data.start_time, 'HH:mm').isBetween(moment(brunchStart, 'HH:mm'), moment(brunchEnd, 'HH:mm')) && moment(data.end_time, 'HH:mm').isBetween(moment(brunchStart, 'HH:mm'), moment(brunchEnd, 'HH:mm'))
    ) {
      this.setState({ isEditHours: false });
      data.info = "Brunch Time";
      await this.props.post_hours_of_operation(data, branchId, 'copyBranch');
      this.toggleEditHours();
      this.setInitialHours(data.start_time, data.end_time);
    }


    else if (moment(data.start_time, 'HH:mm').isBetween(moment(lunchStart, 'HH:mm'), moment(lunchEnd, 'HH:mm')) && moment(data.end_time, 'HH:mm').isBetween(moment(lunchStart, 'HH:mm'), moment(lunchEnd, 'HH:mm'))) {
      this.setState({ isEditHours: false });
      data.info = "Lunch Time";
      await this.props.post_hours_of_operation(data, branchId, 'copyBranch');
      this.toggleEditHours();
      this.setInitialHours(data.start_time, data.end_time);
    }

    else if (moment(data.start_time, 'HH:mm').isBetween(moment(dinnerStart, 'HH:mm'), moment(dinnerEnd, 'HH:mm')) && moment(data.end_time, 'HH:mm').isBetween(moment(dinnerStart, 'HH:mm'), moment(dinnerEnd, 'HH:mm'))) {
      this.setState({ isEditHours: false });
      data.info = "Dinner Time";
      await this.props.post_hours_of_operation(data, branchId, 'copyBranch');
      this.toggleEditHours();
      this.setInitialHours(data.start_time, data.end_time);
    }

    else {
      data.info = "";
      await this.props.post_hours_of_operation(data, branchId, 'copyBranch');
      this.toggleEditHours();
      this.setInitialHours(data.start_time, data.end_time);
    }

  }

  EditName = () => {
    let editName = this.state.editName;
    editName = !editName;

    if (!editName) {
      this.setState({ isValidFullName: false });
    }
    this.setState({ editName });
  };
  handleNameChange = (e) => {
    let hasError = this.refNameForm.current._inputs['fname'].context.FormCtrl.hasError()
    this.setState({
      fName: e.target.value,
    }, () => { this.checkValidate(hasError) });
  };

  handleChange2 = (e) => {
    let hasError = this.refNameForm.current._inputs['lname'].context.FormCtrl.hasError()
    this.setState({
      lName: e.target.value,
    }, () => { this.checkValidate(hasError) });
  };
  checkValidate = (hasError) => {
    let firstName = this.state.fName;
    let lastName = this.state.lName;
    let isValidFullName = this.state.isValidFullName;

    if (firstName !== '' && lastName !== '' && firstName.match('^[a-zA-Z ]*$') !== null && lastName.match('^[a-zA-Z ]*$') !== null) {
      this.setState({ isValidFullName: false })
    } else {
      if (isValidFullName !== true) {
        this.setState({ isValidFullName: true })
      }
    }

  }

  saveName = () => {
    let firstName = this.state.fName;
    let lastName = this.state.lName;

    if (firstName !== '' && lastName !== '') {
      let data = { "preFill": true, "user": { "last_name": lastName, "first_name": firstName, "email": this.state.userEmail } };
      this.props.add_name(data, 'copyBranch');
    }

    this.setState((state) => ({
      editName: !state.editName,
    }));

  }

  savePosition = () => {
    let position = this.state.position;
    let id = this.state.userID;


    if (position && position !== '') {
      let data = { "position": { "key": position, "id": id, "label": "title" } };
      this.props.add_position_value(data, 'copyBranch');
    }
    this.setState({ isTogglePosition: true })

  }

  handlePosition = (e) => {
    let hasError = this.refPositionForm.current._inputs['position'].context.FormCtrl.hasError();
    this.setState({
      position: e.target.value,
    }, () => { this.checkPositionValidate(hasError) });
  };

  checkPositionValidate = (hasError) => {
    let { position, isValidPosition } = this.state;

    if (position !== '' && position.match('^[a-zA-Z ]*$') !== null) {
      this.setState({ isValidPosition: false })
    } else {
      if (isValidPosition !== true) {
        this.setState({ isValidPosition: true })
      }
    }
  }

  EditPosition = () => {
    let { isTogglePosition, position } = this.state;

    isTogglePosition = !isTogglePosition;

    if (!isTogglePosition) {
      this.setState({ isTogglePosition, isValidPosition: true })
    }
    //this.setState({ isTogglePosition, isValidPosition: false, position: profileData.professional[0].profession.title, });
    this.setState({ isTogglePosition, isValidPosition: false, position: position, });
  }

  handleCategoryChange = (e) => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
    });
  };

  sortObject = (items) => {
    let { isEditVisible } = this.state;
    let filteredItems = [];
    var myObj = items,
      keys = Object.keys(myObj),
      i, k, len = keys.length;

    let filteredKeys = keys.filter(item => item?.search('_') < 0);
    filteredKeys = filteredKeys && filteredKeys.sort((a, b) => {
      let aItems = a.toLowerCase();
      let bItems = b.toLowerCase();
      if (aItems < bItems) { return -1; }
      if (aItems > bItems) { return 1; }
      return 0;
    });

    for (i = 0; i < len; i++) {
      k = filteredKeys[i];
      if (k !== undefined) {
        // isEditVisible[i] = false;
        filteredItems.push({ "name": k, "value": myObj[k] })
      }
    }
    // this.setState({ isEditVisible })
    return filteredItems;
  }

  sortData = (items) => {
    let filteredKeys = items &&
      Array.isArray(items) &&
      items.filter(item => (item?.name)?.search('_') < 0);
    filteredKeys = filteredKeys && filteredKeys.length > 0 && filteredKeys.sort((a, b) => {
      let aItems = (a.name).toLowerCase();
      let bItems = (b.name).toLowerCase();
      if (aItems < bItems) { return -1; }
      if (aItems > bItems) { return 1; }
      return 0;
    });
    return filteredKeys;
  }

  toggleAcceptCreditCards = () => {
    this.setState((prevstate) => ({ isToogleAcceptCreditCards: !prevstate.isToogleAcceptCreditCards }));
    // this.setState({ isToogleAcceptCreditCards: true }, () => {
    //     if (this.myRef.current.offsetTop > 0) {
    //         window.scrollTo({ behavior: 'smooth', top: this.myRef.current.offsetTop + 500 })
    //     }
    // });
  }

  toggleDeleteItems = (item, title) => {
    let param = {}
    param["delete_add_info"] = true
    param["name"] = title
    param["info"] = item.value

    this.props.delete_additional_info(param, this.state.branchId, 'copyBranch');
  }

  renderValues = (value, title) => {
    let itemsRender = []
    if (value && Array.isArray(value) && value.length > 0) {
      value.map((item, index) => {
        let currentIndex = index + 1;
        if (item.value && typeof (item.value) == "string") {
          itemsRender.push(
            <React.Fragment key={index}>
              <Badge color="default" className="mr-1 fs-12">
                <div className="d-flex align-items-center">
                  <span className="mr-1 font-weight-normal">{item.value}</span>
                  <button onClick={() => this.toggleDeleteItems(item, title)} type="button" className="close btn-sm" aria-label="Close">
                    <FontAwesomeIcon icon="times-circle" />
                  </button>
                </div>
              </Badge>
            </React.Fragment>
          )
        }
        return null;
      })
    }
    return (
      itemsRender
    )
  }


  renderItems = ({ title, value, edit, trash, index, item }, new_info = false) => {
    // let { isEditVisible, CategoryValue, existing_info_count } = this.state;
    let { isEditVisible, CategoryValue, existing_info_count, isOpenAdditional } = this.state;
    if (new_info) {
      index = existing_info_count + index
    }

    if (title !== 'specialties' && title != 'undefined' && title != undefined && title != "") {
      return (
        <li key={index}>
          <div className="d-flex flex-wrap">
            {/* {(value && value.length > 0) && ( */}
            <CustomInput
              className="mr-3"
              type="checkbox"
              id={"add_info_" + index}
              checked={this.state.rowStateAditional[index]}
              data-idd={index}
              onChange={this.handleChangedBranches}
              name="select_additional"
              value={title}
            />
            {/* )} */}
            <span className="font-weight-bold mb-1">{title}</span>

            {/* Hide when editing */}
            <span className="ml-auto">
              <div className="d-flex">
                <div className="text-right mb-2">
                  {value != "undefined" ? this.renderValues(value, title) : ""}
                </div>
                <div className="ml-2 interactive col-auto px-0">
                  <div className="interactive-appear text-nowrap">
                    <div className="text-nowrap">
                      {value == "undefined" ?
                        <button type="button" className="btn-sm"
                          onClick={
                            () => this.toggleEditState({ idx: index, item })
                          }>
                          {"+ Add"}
                        </button>
                        :
                        <EditBtn
                          onClick={async () => {
                            // await this.setState({ isOpenAdditional: {} });
                            await this.toggleEditState({ idx: index, item });
                          }
                          }

                        />
                      }
                    </div>
                  </div>
                </div>
              </div>
            </span>
          </div>

          {/* Show when editing */}
          <div className="mb-4 py-2 shadow-sm bg-white px-2" hidden={isOpenAdditional && isOpenAdditional[item.uniquId] ? false : true}>
            <div className="mb-2">

              <Row form>

                {this.state.newID != 0 && this.state.newID == (item && item.id ? item.id : item && item.value && item.value[0] && item.value[0].id) ?
                  <>
                    {this.state.amenities_list &&
                      Array.isArray(this.state.amenities_list) &&
                      this.state.amenities_list.length > 0 &&
                      this.state.amenities_list.map((amenity, amenityIndex) => (
                        <Col xs="auto" key={index}>
                          <Switch
                            value={this.userExists(value, amenity.option_value) ? 1 : 0}
                            name={amenity.option_value}
                            id={"amenityIndex_" + amenity.option_value}
                            onChange={(e) => this.handleEdit(e, index, item, amenity.option_value)}
                          />  {amenity.option_value}
                        </Col>
                      ))
                    }
                  </>
                  : ""
                }
              </Row>
            </div>
            {/* <div className="text-right">
              <Button size="sm" className="btn-icon-split mr-2" title="Add" onClick={() => this.handleEdit(index)}>
                <span className="text">Save</span>
                <span className="icon">
                  <FontAwesomeIcon icon="check" />
                </span>
              </Button>
              <Button onClick={() => this.toggleEditState({ idx: index, del: true })} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                <span className="text">Cancel</span>
                <span className="icon">
                  <FontAwesomeIcon icon="times" />
                </span>
              </Button>
            </div> */}

          </div>
        </li>
      )
    }
  }

  userExists = (arr, val) => {
    let data = false
    let { newID } = this.state
    if (Array.isArray(arr) && arr.length > 0) {
      arr.map((el, index) => {
        if (el.value == val) {
          data = true
        }
      })
      return data
    }
  }

  addAdditionalFields = ({ name, edit, index }) => {
    return (
      <li key={index} className="mb-2">
        <div className="d-flex mx-n1">
          <div className="col-6 px-1"><span className="font-weight-bold">{name}</span></div>
          <div className="col-6 px-1 text-right"><span className="actionable"><FontAwesomeIcon icon="plus" /> Add</span></div>
        </div>
      </li>
    )
  }

  changeCategories = (e) => {
    let { name, value } = e.target;
    this.setState({ [name]: value })
  }

  toggleEditState = ({ idx, del, item }) => {
    let { isEditVisible, additionInfo, isOpenAdditional } = this.state;
    //isEditVisible.pop();
    let lastkey = Object.keys(isOpenAdditional)[Object.keys(isOpenAdditional).length - 1];
    let newID;
    if (del) {
      isEditVisible[idx] = false;
    } else if (isOpenAdditional[idx]) {
      isOpenAdditional[idx] = false
    } else {
      //newID = item && item.id ? item && item.id : item && item.value && item.value[0] && item.value[0].id;
      newID = item && item.id ? item.id : item && item.value && item.value[0] && item.value[0].id;
      //isEditVisible[lastkey] = false;
      isEditVisible[idx] = true;
      isOpenAdditional[idx] = true
      this.setState({ amenities_list: '', newID: newID }, () => this.props.get_amenities_options(newID));


    }
    this.setState({ isEditVisible, CategoryName: item && item.name, CategoryId: newID, isOpenAdditional })

  }

  handleEdit = (e, index, itemData, valueType) => {
    const { add_info_map, isEditVisible, branchId, isOpenAdditional } = this.state;
    if (e === 1) {
      let param = {}
      param["name"] = this.state.CategoryId
      // }
      param["info"] = this.state.CategoryName
      param["value"] = [valueType]
      param["multiple"] = true

      //isEditVisible[index] = false;
      isOpenAdditional[index] = false;
      this.setState({ isOpenAdditional })
      this.props.update_additional_info(param, this.state.corporateId, 'copyBranch');
    } else if (e === 0) {
      let param = {}
      param["delete_add_info"] = true
      param["name"] = itemData.name
      param["info"] = valueType

      this.props.delete_additional_info(param, this.state.corporateId)
      // this.toggleDeleteItems(valueType,itemData.name, )
    }
    //this.handleAfterSubmit(itemData)
  }

  handleFreshDataChange = (evt) => {
    let name = evt.target.name;
    let value = evt.target.value;
    this.setState({
      [name]: value
    })
    if (name == "freshFirstName") {
      this.setState({
        errorFirstName: ""
      });
    } else if (name == "freshLastName") {
      this.setState({
        errorLastName: ""
      });
    } else if (name == "freshPosition") {
      this.setState({
        errorPosition: ""
      });
    }
    // else if (name == "freshPersonEmail") {
    //   this.setState({
    //     errorEmail: ""
    //   });
    // }
  }

  handlePhoneChangeCast = (value, index, FieldType) => {

    const {
      freshPhone,
    } = this.state
    if (FieldType === 'AddPhone') {
      freshPhone[index]['phoneFresh'] = value
      this.setState({ freshPhone })
    }
  }

  handleChangeCast = (e, index, FieldType) => {
    const {
      freshData,
      freshWROffer,
      freshAddress,
      freshPhone,
      freshWebsite,
      freshHOP,
      freshEmail,
    } = this.state
    const { name, value } = e.target

    if (FieldType === 'WRoffer' && freshWROffer.length <= index) {
      freshWROffer.push(
        {
          freshOfferText: '',
          freshOfferType: '',
          freshOfferStartDATE: '',
          freshOfferEndDate: "",
          freshOfferWebsiteLink: "",
        }
      )
      this.setState({ freshWROffer })
    } else if (FieldType === 'AddAddress' && freshAddress.length <= index) {
      freshAddress.push(
        {
          freshAddress1: '',
          freshAddress2: '',
          freshAddressCity: '',
          freshAddressState: '',
          freshAddressCountry: "",
          freshAddressZipcode: "",
        }
      )
      this.setState({ freshAddress })
    } else if (FieldType === 'AddPhone' && freshPhone.length <= index) {
      freshPhone.push(
        {
          phoneFresh: '',
          freshPhoneType: '',
        }
      )
      this.setState({ freshPhone })
    }
    else if (FieldType === 'adddWebsite' && freshWebsite.length <= index) {
      freshWebsite.push(
        {
          freshWebsiteURL: '',
          freshWebsiteType: '',
        }
      )
      this.setState({ freshWebsite })
    }
    else if (FieldType === 'addHOP' && freshHOP.length <= index) {
      freshHOP.push(
        {
          freshHOPDay: '',
          freshHOPText: '',
          freshHOPStartTime: '',
          freshHOPEndTime: '',
        }
      )
      this.setState({ freshHOP })
    } else if (FieldType === 'addEmail' && freshEmail.length <= index) {
      freshEmail.push(
        {
          freshemailText: "",
          freshEmailType: "",
        }
      )
      this.setState({ freshEmail })
    }

    if (FieldType === 'WRoffer') {
      freshWROffer[index][name] = value
      this.setState({ freshWROffer }, () => {
      })
    } else if (FieldType === 'AddAddress') {
      freshAddress[index][name] = value
      this.setState({ freshAddress })
    } else if (FieldType === 'AddPhone') {
      freshPhone[index][name] = value
      this.setState({ freshPhone })
    } else if (FieldType === 'adddWebsite') {
      freshWebsite[index][name] = value
      this.setState({ freshWebsite })
    } else if (FieldType === 'addHOP') {
      freshHOP[index][name] = value
      this.setState({ freshHOP })
    } else if (FieldType === 'addEmail') {
      freshEmail[index][name] = value
      this.setState({ freshEmail })
    }


  }

  handleNextFreshSubmit = async () => {
    let { freshWROffer, freshAddress, freshPhone, freshWebsite, freshHOP, freshEmail, freshFirstName,
      freshLastName, freshPosition, freshPersonEmail, } = this.state

    let params = {}
    let media = []
    // this.state.mediaSet?.map(med => {
    //   media.push(med.id)
    // })
    //let intItem = parseInt(this.state.category_id)
    if (freshFirstName === '' && freshLastName === "" && freshPosition === "") {

      this.setState({
        errorFirstName:
          "Required",
        errorLastName:
          "Required",
        errorPosition:
          "Required",
      });
    } else if (freshFirstName == "") {
      this.setState({
        errorFirstName:
          "Required",
      });
    } else if (freshLastName == "") {
      this.setState({
        errorLastName:
          "Required",
      });
    } else if (freshPosition == "") {
      this.setState({
        errorPosition:
          "Required",
      });
    } else {
      await this.setState({
        specialofferSet: freshWROffer,
        AdressDiary: freshAddress,
        phoneDiary: freshPhone,
        webSet: freshWebsite,
        HoursOfOperation: freshHOP,
        emailSet: freshEmail,
        copyInfoToBranchModalStep2: true,
        formType: 'fresh'
      })

    }
  }

  handleremoveCast = (option, idx) => {
    const {
      freshData,
      freshPhone,
      freshData: {
        freshWROffer,
        freshAddress,
        // freshPhone,
        freshWebsite,
        freshHOP,
        freshEmail
      }
    } = this.state
    if (option === 'freshWROffer') {
      freshWROffer.pop()
      this.setState({ freshWROffer })
    } else if (option === 'freshAddress') {
      freshWROffer.pop()
      this.setState({ freshAddress })
    } else if (option === 'freshPhone') {

      freshPhone.splice(idx, 1);
      // freshPhone.pop()
      this.setState({ freshPhone })
    } else if (option === 'freshWebsite') {
      freshWebsite.pop()
      this.setState({ freshWebsite })
    } else if (option === 'freshEmail') {
      freshEmail.pop()
      this.setState({ freshEmail })
    } else if (option === 'freshEmail') {
      freshEmail.pop()
      this.setState({ freshEmail })
    }
  }
  handleSelectedAdditionalInfo = (data) => {
    let filteredAdditionalInfo = data && data.filter((element) => {
      return element.name !== "specialties"
    })
    this.setState({
      freshSelectAdditionalInfo: filteredAdditionalInfo
    })
  }

  reposition = (event) => {
    this.setState({ isImage: true });
  }

  getImage = (event) => {
    this.setState({ isImage: false });
    let data = {}
    data.left = this.state.left;
    data.top = this.state.top;
    data.x = this.state.x;
    data.y = this.state.y;
    this.props.upload_move_set_profile_media(data);
  }

  onStart(e) {
    let { handle } = this.state;
    let ref = ReactDOM.findDOMNode(handle);
    const box = ref?.getBoundingClientRect();
    if (box) {
      this.setState({
        relX: box?.left ? box?.left : 0,
        relY: box?.top ? box?.top : 0
      });
    }
  }

  onMove(e) {
    const left = Math.trunc((this.state.relX - e.pageX) / this.gridX) * this.gridX;
    const top = Math.trunc((this.state.relY - e.pageY) / this.gridY) * this.gridY;
    if (left !== this.state.left || top !== this.state.top) {
      this.setState({
        left,
        top
      }, () => {
        localStorage.setItem('left', this.state.left);
        localStorage.setItem('top', this.state.top);
      });
      this.props.onMove && this.props.onMove(this.state.left, this.state.top);
    }
  }

  onMouseDown(e) {
    if (e.button !== 0) return;
    this.onStart(e);
    document.addEventListener('mousemove', this.onMouseMove);
    document.addEventListener('mouseup', this.onMouseUp);
    e.preventDefault();
  }

  onMouseUp(e) {
    document.removeEventListener('mousemove', this.onMouseMove);
    document.removeEventListener('mouseup', this.onMouseUp);
    this.props.onStop && this.props.onStop(this.state.left, this.state.top);
    e.preventDefault();
  }

  onMouseMove(e) {
    this.onMove(e);
    e.preventDefault();
  }

  onTouchStart(e) {
    this.onStart(e.touches[0]);
    document.addEventListener('touchmove', this.onTouchMove, { passive: false });
    document.addEventListener('touchend', this.onTouchEnd, { passive: false });
    e.preventDefault();
  }

  onTouchMove(e) {
    this.onMove(e.touches[0]);
    e.preventDefault();
  }

  onTouchEnd(e) {
    document.removeEventListener('touchmove', this.onTouchMove);
    document.removeEventListener('touchend', this.onTouchEnd);
    this.props.onStop && this.props.onStop(this.state.left, this.state.top);
    e.preventDefault();
  }
  setHandle = (item) => {
    if (!this.state.handle) {
      this.setState({ handle: item });
    }
  }

  onHover = (activeType) => {
    let hoverimag = "";
    if (activeType == "Book Now") {
      hoverimag = "call_on_static1.png"
    } else if (activeType == "Buy Tickets") {
      hoverimag = "on-call-button-buy-ticket-new.png"
    } else if (activeType == "Contact Us") {
      hoverimag = "on-call-button-biz-contact-us-new.png"
    } else if (activeType == "Order Online") {
      hoverimag = "call_on_order.png"
    } else if (activeType == "Request A Consultation") {
      hoverimag = "on-call-button-biz-consult-with-us-new.png"
    } else if (activeType == "Request A Quote") {
      hoverimag = "on-call-button-biz-request-now.png"
    } else if (activeType == "Reserve Now") {
      hoverimag = "on-call-button-biz-reserve-new.png"
    } else if (activeType == "Schedule Appointment") {
      hoverimag = "on-call-button-biz-schedule-now.png"
    } else {
      hoverimag = "call_on_learn_more-new.png"
    }
    this.setState({ hoverImage: hoverimag })
  }
  // Set up Call
  setupCall = () => {
    this.setState({ setup: true })
  }
  // Get Started
  getStarted = () => {
    this.setState({ getStarted: true });
    // this.props.getallbranch();
  }
  handleChangeCallToAction = (event) => {
    let opts = [], opt;

    if (event.target.name == "type_of_service") {
      let imagename = "";
      if (event.target.value == "Book Now" || event.target.value == "Schedule Appointment") {
        imagename = "calendar.png"
      } else if (event.target.value == "Buy Tickets") {
        imagename = "buytickets.png"
      } else if (event.target.value == "Contact Us") {
        imagename = "contactus.png"
      } else if (event.target.value == "Order Online") {
        imagename = "OrderOnline.png"
      } else if (event.target.value == "Request A Consultation") {
        imagename = "RequestAConsultation.png"
      } else if (event.target.value == "Request A Quote") {
        imagename = "RequestAQuote.png"
      } else if (event.target.value == "Reserve Now") {
        imagename = "ReserveNow.png"
      } else {
        imagename = "LearnMore.png"
      }
      this.setState({ typeService: event.target.value, isOpenRow: true, previewImage: imagename, headline: '', sub_headline: '' });
    }

    this.setState({ [event.target.name]: event.target.value })
  }

  handleSubmitCallToAction = (event, errors, values) => {
    event.preventDefault();
    const { selectValueCalltoAction, headline, sub_headline, typeService, linked_url, response_time, branchId } = this.state;
    selectValueCalltoAction.push(branchId)
    if (selectValueCalltoAction.length != 0) {
      let data = {
        ent: selectValueCalltoAction,
        selected_type: typeService,
        headline: headline,
        subheadline: sub_headline,
        response_time: response_time,
        selected: true,
        linkedurl: linked_url,
      }
      this.setState({ errors, values });


      this.setState({ visible: true, message: "Add Successfully", isOpenRow: false })
      this.props.corporate_call_to_action(data, branchId, 'copyBranch');
      // this.props.get_corporate_call_list(this.state.listing_id);
      // this.props.get_specific_branch_corporate_call_list(branchId);
      this.refs.form.reset();
      setTimeout(
        function () {
          this.handleOnBiz("c2a");
          this.onDismiss();
        }
          .bind(this),
        3000
      );
    }
  }
  onDismiss = () => {
    this.setState({ visible: false })
  }

  handleOnBiz = () => {
    this.setState({
      setup: false,
      getStarted: false,
      isOpenRow: false,
      headline: '',
      sub_headline: ''
    })

    // this.props.get_biz_callon_details(data1);
    // this.props.get_corporate_call_list(listing_id);
    // this.refs.form.reset();
  }

  render() {
    const { profileData, current_role_and_permissions, admin_stats, filtered_stats_count } = this.props;
    const {
      bizCallonType,
      setup,
      typeService,
      linked_url,
      headline,
      sub_headline,
      previewImage,
      hoverImage,
      response_time,
      getStarted,
      isToggleDeletePayment,
      freshData,
      freshHOP,
      freshprofilePicUrl,
      freshprofileBranchLogoUrl,
      freshProfileBranchBannerUrl,
      country,
      countryList,
      countryState,
      stateName,
      getBranchData,
      newBranches,
      pendingBranches,
      mediaId,
      ratings,
      corporateDetails,
      specialofferSet,
      phoneDiary,
      emailSet,
      payOptions,
      categoryList,
      HoursOfOperation,
      countryName,
      provinceName,
      webSet,
      branch_logo_pic,
      branch_profile_pic,
      corporateCallList,
      branchId,
      ownerEmail,
      ownerPosition,
      ownerProfile,
      logo_to_copy,
      video_to_copy,
      isEditOffer,
      isToggleDeleteOffers,
      addOffer,
      claim_deal_link_ur,
      isToggleOffer,
      isEditPhone,
      addPhone,
      isPhoneValid,
      isTogglePhone,
      isEditWebsite,
      addWebsite,
      isWebsiteValid,
      isToggleWebsite,
      isEditBusinessEmail,
      addBusinessEmail,
      isBusinessEmailValid,
      isToggleBusinessEmail,
      paymentSwitch,
      isTogglePayment,
      businessCategory,
      businessSubCategory,
      totalSubCategories,
      anotherCategories,
      business_specialities,
      isToggleDeleteCategory,
      isToggleDeleteHours,
      addHours,
      isEditHours,
      isHoursValid,
      isEditAddress,
      addAddress,
      isToggleAddress,
      isToggleBusiness,
      isToggleHistory,
      isToggleMeet,
      fullName,
      position,
      userEmail,
      profilePic,
      editName,
      fName,
      lName,
      isValidFullName,
      isTogglePosition,
      isValidPosition,
      profession,
      userID,
      topBarStats,
      stats_duration,
      additionInfo,
      additional_info_all_categories,
      freshLastName,
      freshPosition,
      freshPersonEmail,
      freshSpecialities,
      freshHistory,
      freshBusinessOwner,
      freshPhone,
      freshPaymentSwitch,
      isToggleFreshPayment,
      freshSelectedPayment
    } = this.state;
    let style = {};
    let self = this;
    let style1 = {};
    let style2 = {};
    if (!setup) {
      style.display = 'none'
      style2.display = 'none'

    } else {
      style1.display = 'none'
      style2.display = 'none'
    }
    if (this.state.getStarted) {
      style2.display = 'block'
      style1.display = 'none'
      style.display = 'none'
    }
    let user_name = "";
    if (profileData && profileData.user) {
      if (
        profileData.user &&
        profileData.user.first_name &&
        profileData.user.last_name
      ) {
        user_name = `${profileData.user.first_name} ${profileData.user.last_name}`;
      }
    }

    let headerImage = '';
    let headerMediaType = '';

    if (corporateDetails && Array.isArray(corporateDetails.listing_profileimage) && corporateDetails.listing_profileimage.length > 0) {
      headerImage = corporateDetails.listing_profileimage[0].url
      headerMediaType = corporateDetails.listing_profileimage[0].media_type
    }

    return (
      <React.Fragment>
        <header className="app-header"
          style={headerMediaType === "video" ? {} : { backgroundImage: `url(${headerImage})` }}
        >
          {headerMediaType === "video" ? <video className="app-header-video" width="100%" height="100%" autoPlay={true} muted={true} loop={true}>
            <source
              src='https://www.w3schools.com/html/mov_bbb.mp4'
              src={headerImage}
              type="video/mp4"
            />
            Your browser does not support the video tag.
          </video> : ""}
          {/* Stats Intro block */}
          {/* <div>
            <div>
              <div className="stat-bubble with-line" data-type="overall">
                &nbsp;
                  </div>
              <div className="stat-bubble with-line" data-type="reviews">
                &nbsp;
                  </div>
              <div className="stat-bubble with-line" data-type="branches">
                &nbsp;
                  </div>
            </div>
            <div className="intro-block" data-type="stat-intro">
              Click on the coloured circles and tabs with numbers to filter the results.
                </div>
          </div> */}
          {/* Stats Intro block ends */}
          <Container>
            <div className="app-header-block">
              <Row>
                <Col xs={12}>
                  <div className="text-md-right text-center">
                    <FormGroup className="d-md-inline-block styled-select position-relative step-4">
                      <Input
                        onChange={this.handleChangeStats}
                        value={stats_duration}
                        type="select"
                        name="stats_duration"
                        className="light"
                      >
                        {/* <option>Last 30 days</option> */}
                        <option value="0">Today</option>
                        <option value="1">Yesterday</option>
                        <option value="7">Last 7 Days</option>
                        <option value="30">Last 30 Days</option>
                        <option value="90">Quarter</option>
                        <option value="365">Year</option>
                      </Input>
                    </FormGroup>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col lg="3" >
                  <div className="mb-3">
                    <span className="font-weight-bold fs-20">{user_name}</span>
                  </div>

                  {/* Stats */}
                  <div className="step-2">
                    <div className="stat overall step-3">
                      <div className="count">
                        {topBarStats && topBarStats.overall
                          ? this.renderItemsSpans({ data: topBarStats.overall }, 'overall')
                          : 0}
                      </div>
                      <div className="type">{" Overall"}</div>
                    </div>
                    <div className="stat">
                      <div className="count">
                        {topBarStats && topBarStats.review_count
                          ? this.renderItemsSpans({
                            data: topBarStats.review_count,
                          }, 'new', 'corporatereviewsnew', 'reviews')
                          : 0}
                      </div>
                      <div className="type">{"Reviews"}</div>
                    </div>
                    <div className="stat">
                      <div className="count">
                        {topBarStats && topBarStats.dispute_review
                          ? this.renderItemsSpans({
                            data: topBarStats.dispute_review,
                          }, 'disputed', 'corporatereviewsnew', 'disputed_Review')
                          : 0}
                      </div>
                      <div className="type">Disputed Reviews</div>
                    </div>
                    <div className="stat">
                      <div className="count">
                        {topBarStats && topBarStats.message
                          ? this.renderItemsSpans({ data: topBarStats.message }, 'message', 'corporatemessage', 'message')
                          : 0}
                      </div>
                      <div className="type">Respond to Messages</div>
                    </div>
                    <div className="stat">
                      <div className="count">
                        {topBarStats && topBarStats.qa
                          ? this.renderItemsSpans({ data: topBarStats.qa }, 'pending', 'corporatequestions', 'qa')
                          : 0}
                      </div>
                      <div className="type">Respond to Q&amp;A</div>
                    </div>
                    <div className="stat">
                      <div className="count">
                        {topBarStats && topBarStats.feedback
                          ? this.renderItemsSpans({ data: topBarStats.feedback }, 'pending', 'corporatefeedbackcount', 'feedback')
                          : 0}
                      </div>
                      <div className="type">Respond to Feedback</div>
                    </div>
                  </div>
                </Col>
                <Col lg="6">
                  {/* <div className="text-center p-4">
                    <div className="header-profile step-1">
                      <div className="header-profile-block mr-2">
                        {this.state.listingImage ?
                          <img
                            className="border-0"
                            // src={this.state.listingImage ? this.state.listingImage : require("../../assets/images/user.png")}
                            src={this.state.listingImage}

                            alt={this.state.listing_name ? this.state.listing_name : ""}
                            onError={(error) =>
                              (error.target.src = require("../../assets/images/user.png"))
                            }
                          /> : ""}
                      </div>
                      <div className="mt-2">
                        <Input
                          type="file"
                          name="logo_image"
                          id="branch_logo_image"
                          accept="image/*"
                          style={{ display: "none" }}
                          onChange={(event) => this.handleOnFileUploadChange(event, 'companyLogo')}
                        // onChange={ this.handleOnFileUploadChange}
                        />
                        <Label
                          for="branch_logo_image"
                          title="Upload Company Logo"
                          role="button"
                        >
                          <FontAwesomeIcon icon="camera" />
                        </Label>
                      </div>
                    </div>

                    <h1 className="text-shadow">{this.state.listing_name} </h1>
                    <div className="my-4">
                      {(typeof ratings !== 'undefined' && ratings.length > 0) ? ratings.filter(dt => dt.key == "avg_rating").map((dropdown, index) => {
                        return <img src={`https://stagingdatawikireviews.s3.amazonaws.com/images/star/large_white/${dropdown.rating}`} key={index} alt=" " className="ml-1" />
                      }) : (<li> </li>)}
                    </div>
                  </div> */}
                  <div className="text-center p-4">
                    <div
                      className={this.state.isImage ? "pancontainer dragaa" : "d-none"}
                      id="pan_id"
                      data-orient="center"
                      data-canzoom="no"
                      onMouseDown={this.onMouseDown}
                      onTouchStart={this.onTouchStart}
                      ref={(rf) => { this.setHandle(rf); }} >
                      <img
                        src={this.state.listingImage}
                        alt="User"
                        style={{
                          "width": '320px',
                          "maxWidth": '320px',
                          "height": '240px',
                          "position": 'relative',
                          "left": this.state.left,
                          "top": this.state.top
                        }}
                      />

                    </div>
                    <span className={this.state.isImage ? "save-image" : "d-none"} onClick={this.getImage}>Save</span>
                    <div className={!this.state.isImage ? "header-profile step-1" : "d-none"}>

                      <div
                        className={`avatars ${this.state.listingImage ? this.state.listingImage : 'silver'}`}
                      >
                        <div className="header-profile-block mr-2">
                          {this.state.listingImage ?
                            <img
                              style={{
                                "width": '320px',
                                "maxWidth": '320px',
                                "height": '240px',
                                "position": 'relative',
                                left: `${localStorage.getItem('left') ? localStorage.getItem('left') : '-85'}px`,
                                top: `${localStorage.getItem('top') ? localStorage.getItem('top') : '-85'}px`,
                              }}
                              className="border-0"
                              src={this.state.listingImage}
                              // src={this.state.listingImage ? this.state.listingImage : require("../../assets/images/user.png")}
                              alt={this.state.listing_name ? this.state.listing_name : ""}
                            /> :
                            <img

                              className="border-0"
                              src={require("../../assets/images/user.png")}
                              // src={this.state.listingImage ? this.state.listingImage : require("../../assets/images/user.png")}
                              alt={this.state.listing_name ? this.state.listing_name : ""}

                            />
                          }
                        </div>

                      </div>


                    </div>
                    <div
                      hidden={this.state.isImage ? "d-none" : ""}
                      className="position-relative text-center" style={{ zIndex: 2, marginTop: -30 }}>
                      <div className="mx-auto" style={{ maxWidth: '130px' }}>

                        <div className="d-flex justify-content-between">
                          <div className="px-1" onClick={this.reposition}>
                            <div role="button" title="Reposition">
                              <FontAwesomeIcon icon="arrows-alt" />
                            </div>
                          </div>
                          <div className="px-1">

                            <Input
                              type="file"
                              name="logo_image"
                              id="branch_logo_image"
                              accept="image/*"
                              style={{ display: "none" }}
                              onChange={(event) => this.handleOnFileUploadChange(event, 'companyLogo')}
                            // onChange={ this.handleOnFileUploadChange}
                            />
                            <Label
                              for="branch_logo_image"
                              title="Upload Company Logo"
                              role="button"
                            >
                              <FontAwesomeIcon icon="camera" />
                            </Label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <h1 className="text-shadow">{this.state.listing_name} </h1>
                    <div className="my-4">
                      {(typeof ratings !== 'undefined' && ratings.length > 0) ? ratings.filter(dt => dt.key == "avg_rating").map((dropdown, index) => {
                        return <img src={`https://stagingdatawikireviews.s3.amazonaws.com/images/star/large_white/${dropdown.rating}`} key={index} alt=" " className="ml-1" />
                      }) : (<li> </li>)}
                    </div>
                  </div>
                </Col>
                <Col lg="3">

                  <div className="d-flex h-100 flex-column">
                    {(
                      <ul className="mt-lg-5 mb-4 mb-lg-0 list-unstyled text-right rating-list">
                        {(typeof ratings !== 'undefined' && ratings.length > 0) ? ratings.filter(dt => dt.key !== "avg_rating").map((dropdown, index) => {
                          return <li className="mb-2" key={index}>
                            <Row noGutters>
                              <Col xs={6}>
                                <div className="px-2 text-left text-lg-right rating-name">
                                  {dropdown.key}
                                </div>
                              </Col>
                              <Col xs={6}>
                                <div className="px-2 text-right text-lg-left">
                                  <img className="img-fluid" src={`https://stagingdatawikireviews.s3.amazonaws.com/images/star/large_white/${dropdown.rating}`} alt="Rating" />
                                </div>
                              </Col>
                            </Row>
                          </li>
                        }) : (<li> </li>)}
                      </ul>
                    )}
                    <div className="text-lg-right text-center mt-auto">
                      <Button color="gray" className="fs-20 ff-headings step-5" size="sm" onClick={this.listingOwnerGuideModalToggle}>Listing Owner Guide <Badge className="ml-3 ff-base fs-14" color="primary">OPEN</Badge> </Button>

                      {/* Admin Links */}

                      <div>
                        {this.props.isAdmin && (
                          <div className="mt-3 mx-n1">
                            <Link to={'/corporatesettings'} className="text-uppercase font-weight-bold text-decoration-none text-white px-1">Admin View</Link>
                            <Link to={'/corporatesettings'} className="text-uppercase font-weight-bold text-decoration-none text-white px-1" title="Profile Settings"><FontAwesomeIcon icon="cog" className="step-10" /></Link>
                            <Button color="link" className="text-uppercase font-weight-bold text-decoration-none text-white px-1" title="Select Branch" onClick={() => this.setState({ selectCopyBranchMethodModal: true })}><FontAwesomeIcon icon="copy" className="step-6" /></Button>
                          </div>
                        )}
                        <div className="mt-2">
                          <Input
                            type="file"
                            name="banner_image"
                            id="branch_banner_image"
                            accept="image/*,video/*"
                            style={{ display: "none" }}
                            onChange={(event) => this.handleOnFileUploadChange(event, 'UploadBackgroundVideoOrImage')}
                          />
                          <Label for="branch_banner_image" title="Upload Background Video or Image" role="button">
                            <FontAwesomeIcon icon="camera" />
                          </Label>
                        </div>
                      </div>

                    </div>

                  </div>
                </Col>
              </Row>
            </div>
          </Container>
        </header>

        {/* Select Branch Modal */}
        <Modal size="lg" isOpen={this.state.selectBranchModal} toggle={this.selectBranchModalToggle}>
          <ModalHeader toggle={this.selectBranchModalToggle}>Select Branches</ModalHeader>
          <ModalBody className="bg-white">
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label for="selectCountry">Select Country</Label>
                  <Input className="primary" type="select" value={country} name="select_country" onChange={this.handleChange} id="selectCountry">
                    <option>--Select--</option>
                    {(typeof countryList !== 'undefined' && countryList.length > 0) ? countryList.map((dropdown, index) => {
                      return <option key={index} dropdown={dropdown} value={dropdown}>
                        {dropdown}</option>;
                    }) : (<option> </option>)
                    }
                  </Input>
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="selectState">Select State</Label>
                  <Input className="primary" type="select" name="select_state" value={stateName} id="selectState" onChange={this.handleChange}>
                    <option>--Select--</option>
                    {(typeof countryState !== 'undefined' && countryState !== 'AvePhiladelphia' && countryState.length > 0) ? countryState.map((dropdown, index) => {
                      return <option key={index} dropdown={dropdown} value={dropdown}>
                        {dropdown}</option>;
                    }) : (<option> </option>)
                    }
                  </Input>
                </FormGroup>
              </Col>
            </Row>

            <div>
              <div className="mt-3 mx-n1">
                <Link
                  to={"/corporatesettings"}
                  className="text-uppercase font-weight-bold text-decoration-none text-white px-1"
                >
                  {'Admin View'}
                </Link>
                <Link
                  to={"/corporatesettings"}
                  className="text-uppercase font-weight-bold text-decoration-none text-white px-1"
                  title="Profile Settings"
                >
                  <FontAwesomeIcon icon="cog" />
                </Link>
                <Button
                  color="link"
                  className="text-uppercase font-weight-bold text-decoration-none text-white px-1"
                  title="Select Branch"
                  onClick={() => this.setState({ selectCopyBranchMethodModal: true })}
                >
                  <FontAwesomeIcon icon="copy" />
                </Button>
              </div>
              <div className="mt-2">
                <Input
                  type="file"
                  name="banner_image"
                  id="branch_banner_image"
                  accept="image/*"
                  style={{ display: "none" }}
                  onChange={this.handleOnFileUploadChange}
                />
                <Label
                  for="branch_banner_image"
                  title="Update Background Image"
                  role="button"
                >
                  <FontAwesomeIcon icon="camera" />
                </Label>
              </div>
            </div>
          </ModalBody>
        </Modal>
        {/* )}
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </Container>
        </header> */}

        {/* Listing Owner Guide Modal */}
        <Modal
          isOpen={this.state.listingOwnerGuideModal}
          toggle={this.listingOwnerGuideModalToggle}
        >
          <ModalHeader toggle={this.listingOwnerGuideModalToggle}></ModalHeader>
          <ModalBody className="bg-white">
            <h2 className="text-secondary">Listing Owner Guide</h2>
            <hr />
            {this.handlelistingOwnerGuideDataView()}
          </ModalBody>
        </Modal>

        {/* Select Branch Modal */}
        <Modal
          size="lg"
          isOpen={this.state.selectBranchModal}
          toggle={this.selectBranchModalToggle}
        >
          <ModalHeader toggle={this.selectBranchModalToggle}>
            Select Branches
          </ModalHeader>
          <ModalBody className="bg-white">
            <p className="text-muted fs-14 mb-1">Select One Branch that has data that you would like to copy over to other listings</p>
            <p className="text-secondary-dark fs-14">Please note: After selecting a branch, any changes made will also apply to that branch.</p>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label for="selectCountry">Select Country</Label>
                  <Input
                    className="primary"
                    type="select"
                    value={country}
                    name="select_country"
                    onChange={this.handleChange}
                    id="selectCountry"
                  >
                    <option>--Select--</option>
                    {typeof countryList !== "undefined" &&
                      countryList.length > 0 ? (
                        countryList.map((dropdown, index) => {
                          return (
                            <option
                              key={index}
                              dropdown={dropdown}
                              value={dropdown}
                            >
                              {dropdown}
                            </option>
                          );
                        })
                      ) : (
                        <option> </option>
                      )}
                  </Input>
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="selectState">Select State</Label>
                  <Input
                    className="primary"
                    type="select"
                    name="select_state"
                    value={stateName}
                    id="selectState"
                    onChange={this.handleChange}
                  >
                    <option>--Select--</option>
                    {typeof countryState !== "undefined" &&
                      countryState.length > 0 ? (
                        countryState.map((dropdown, index) => {
                          return (
                            <option
                              key={index}
                              dropdown={dropdown}
                              value={dropdown}
                            >
                              {dropdown}
                            </option>
                          );
                        })
                      ) : (
                        <option> </option>
                      )}
                  </Input>
                </FormGroup>
              </Col>
            </Row>

            <div>
              {mediaId == 0 ? (
                <div
                  className="p-2 mb-4"
                  style={{ maxHeight: "calc(100vh - 330px", overflowY: "auto" }}
                >
                  {newBranches &&
                    Array.isArray(newBranches) &&
                    newBranches.length > 0 ? (
                      newBranches.map((item, index) => (
                        <CustomInput
                          type="radio"
                          id={"select_branch_" + index}
                          name="select_branch"
                          key={index}
                          onChange={this.handleChange}
                          value={item.id}
                        >
                          <Label
                            for={"select_branch_" + index}
                            className="text-dark font-weight-normal fs-14"
                          >
                            {item.name +
                              " ( " +
                              item.address1 +
                              ", " +
                              item.city +
                              ", " +
                              item.state +
                              ", " +
                              item.country +
                              ")"}
                          </Label>
                        </CustomInput>
                      ))
                    ) : (
                      <div className="bg-white p-3">
                        <h2 className="text-secondary-dark">
                          No Company to Display
                      </h2>
                      </div>
                    )}
                </div>
              ) : (
                  <div
                    className="p-2 mb-4"
                    style={{ maxHeight: "calc(100vh - 300px", overflowY: "auto" }}
                  >
                    <div className="ml-auto">
                      <span
                        role="button"
                        className="text-primary"
                        name="selectallBranches"
                        onClick={this.handleAllCheckedBranch}
                        checked={this.state.checkAll}
                      >
                        Select All
                    </span>
                    </div>
                    {newBranches &&
                      Array.isArray(newBranches) &&
                      newBranches.length > 0 ? (
                        newBranches.map((item, index) => (
                          <CustomInput
                            type="checkbox"
                            id={"select_all_branch_" + index}
                            key={index}
                            checked={this.state.rowState[index]}
                            data-idnew={index}
                            value={item.id}
                            onChange={this.handleChangedBranches}
                            name="select_multiple_branches"
                          >
                            <Label
                              for={"select_all_branch_" + index}
                              className="text-dark font-weight-normal fs-14"
                            >
                              {item.name +
                                " ( " +
                                item.address1 +
                                ", " +
                                item.city +
                                ", " +
                                item.state +
                                ", " +
                                item.country +
                                ")"}
                            </Label>
                          </CustomInput>
                        ))
                      ) : (
                        <div className="bg-white p-3">
                          <h2 className="text-secondary-dark">
                            No Company to Display
                      </h2>
                        </div>
                      )}
                  </div>
                )}
              <div className="text-right">
                {mediaId != 0 ? (
                  <Button
                    color="primary"
                    className="mw"
                    disabled={this.state.isdisabled}
                    onClick={this.handleUploadcorporatemedia}
                  >
                    Ok
                  </Button>
                ) : (
                    <div>
                      <Button
                        color="light"
                        className="mw"
                        onClick={
                          () => {
                            this.setState({
                              enterNewBranchDataModal: false,
                            })
                            this.selectBranchModalToggle()
                          }
                        }
                      >
                        <i className="fa fa-angle-left"></i> Back
                      </Button>
                      <Button
                        color="primary"
                        className="mw"
                        disabled={this.state.branchId !== 0 ? false : true}
                        // disabled={this.state.isdisabled}
                        onClick={this.copyInfoToBranchModalToggle}
                      >
                        Ok
                    </Button>
                    </div>
                  )}
              </div>
            </div>
          </ModalBody>
        </Modal>

        {/* Copy Info to branch Modal */}
        <Modal
          size="lg"
          scrollable
          isOpen={this.state.copyInfoToBranchModal}
        // toggle={this.copyInfoToBranchModalToggle}
        >
          <div className="modal-header bg-light text-dark text-center fs-16">
            <div className="flex-grow-1 p-2">
              <button
                type="button"
                className="close text-dark"
                onClick={this.copyInfoToBranchModalToggle}
                style={{ float: "initial" }}
              >
                ×
              </button>
              <br />
              <h4 className="modal-title">Select Branch Data</h4>
              <p className="ff-base mb-0">
                Check all the info you want to copy to the branches
              </p>
            </div>
          </div>
          <ModalBody>
            <Row form>
              <Col xs={12}>
                <div className="bg-white">
                  <div className="p-3">
                    <h2>{getBranchData !== '' ? getBranchData : ""}</h2>
                    <hr className="bg-secondary mb-0" />
                    {this.state.uploadMedia.uploadFile.length > 0 && (<div
                      style={{
                        maxWidth: "120px",
                        margin: "0 1rem 1rem auto",
                      }}
                    >
                      <div className="text-center mb-1 small">
                        {this.state.uploadMedia.progress === 100 ? (
                          <div className="text-success">
                            <FontAwesomeIcon
                              icon="check-circle"
                              className="mr-1"
                            />{" "}
                                          Uploaded
                          </div>
                        ) : (
                            <div>
                              Uploading{" "}
                              <span className="text-success font-weight-bold ff-base">
                                {this.state.uploadMedia.progress.toFixed(0)} %
                                            </span>
                            </div>
                          )}
                      </div>
                      <Progress
                        value={this.state.uploadMedia.progress}
                        style={{ height: "8px" }}
                      ></Progress>
                    </div>)}
                  </div>

                  <div>
                    <div className="p-3">
                      <Row>
                        <Col lg={6}>
                          <div className="mb-3">
                            <div>
                              <h3>Logo</h3>
                              <hr />
                            </div>

                            <div>
                              <div className="d-flex">
                                <CustomInput
                                  className="mr-3"
                                  type="checkbox"
                                  id="select_logo"
                                  name="select_logo"
                                  checked={this.state.islogoChecked}
                                  onChange={this.handleChangedBranches}
                                />
                                <Label for="select_logo" className="flex-fill">
                                  <img width="120" className="img-thumbnail" id="logoUrl"
                                    src={branch_logo_pic && branch_logo_pic.url ? branch_logo_pic.url : require('../../assets/images/icons/placeholder-img-alt.jpg')}
                                    alt="Logo"
                                    onError={(error) =>
                                      (error.target.src = require("../../assets/images/icons/placeholder-img-alt.jpg"))
                                    }
                                  />
                                  <div className="text-left mt-2">
                                    <Label className="text-dark hover-blue fs-14" role="button" for="updateLogoImage">
                                      {branch_profile_pic && branch_profile_pic.url ? 'Update' : "Add Image"}
                                    </Label>
                                    <input
                                      type="file"
                                      id="updateLogoImage"
                                      accept="image/x-png,image/gif,image/jpeg"
                                      onChange={(event) => this.handleOnSelectedBranchLogoUpload(event, 'companyLogo')}
                                      hidden
                                    />
                                    {/* <span className="mx-2">&middot;</span> */}
                                    <span hidden className="text-dark hover-blue fs-14" role="button">
                                      Reset
                                    </span>
                                  </div>
                                </Label>
                              </div>
                            </div>
                          </div>
                        </Col>
                        <Col lg="6">

                          <div className="mb-3">
                            <div>
                              <h3>Homepage Image/Video</h3>
                              <hr />
                            </div>

                            <div>
                              <div className="d-flex">
                                <CustomInput
                                  className="mr-3"
                                  type="checkbox"
                                  id="select_homepage_image"
                                  name="select_homepage_image"
                                  checked={this.state.isVideoChecked}
                                  onChange={this.handleChangedBranches}
                                />
                                <Label for="select_homepage_image" className="flex-fill">

                                  <img width="120" className="img-thumbnail" id="videoUrl"
                                    src={
                                      branch_profile_pic && branch_profile_pic.type == 'video' ?
                                        branch_profile_pic.thumbnail : branch_profile_pic && branch_profile_pic.url ?
                                          branch_profile_pic.url : require('../../assets/images/icons/placeholder-img-alt.jpg')}
                                    onError={(error) =>
                                      (error.target.src = require("../../assets/images/icons/placeholder-img-alt.jpg"))
                                    }
                                    alt="Image" />
                                  <div className="text-left mt-2">
                                    <Label className="text-dark hover-blue fs-14" role="button" for="updateHomeBannerImageVideo">
                                      {branch_profile_pic && branch_profile_pic.url ? 'Update' : "Add Image"}
                                    </Label>
                                    <input
                                      type="file"
                                      id="updateHomeBannerImageVideo"
                                      accept="image/x-png,image/gif,image/jpeg,video/*"
                                      onChange={(event) => {

                                        this.handleOnSelectedBranchLogoUpload(event, 'companyProfileLogo')
                                      }
                                      }
                                      hidden
                                    />
                                    {/* <span className="mx-2">&middot;</span> */}
                                    <span hidden className="text-dark hover-blue fs-14" role="button">
                                      Reset
                                    </span>
                                  </div>
                                </Label>
                              </div>
                            </div>

                          </div>

                        </Col>
                        <Col lg="6">

                          <div className="mb-3">
                            <div className="d-flex">
                              <span className="mr-2" onClick={(e) => { this.handleAllCheckedBussiness(e, this.state.checkAllBusinessInfo) }}>
                                <Button
                                  color="link"
                                  className="text-primary text-decoration-none px-0 mr-2"
                                >
                                  <FontAwesomeIcon
                                    icon="file"
                                  />{" "}
                                  All
                                </Button>
                              </span>
                              <div className="flex-fill">
                                <h3>Business Information</h3>
                                <hr />
                              </div>
                            </div>

                            <div>
                              <ul className="list-unstyled mb-2 text-secondary-dark scrollable-list">
                                <li className="mb-2">
                                  <div className="mb-2">
                                    <div className="d-flex">
                                      <CustomInput type="checkbox" id="bus_info_phone_all_1" name="selectPhoneNumbers" checked={this.state.selectallPhone} onClick={this.handleAllPhoneChecked} />
                                      <Label for="bus_info_phone_all_1" className="font-weight-bold mb-0">
                                        Phone Number
                                      </Label>
                                    </div>
                                  </div>
                                  <ul className="pl-4 fs-12 list-unstyled">
                                    {/* {phoneDiary && phoneDiary.length > 0
                                      ?  */}
                                    <>{this.PhoneData()}</>

                                    {/*   : (<li><span className="fs-12"><i>No Phone added</i></span></li>)
                                     } */}
                                  </ul>

                                  <div className="pl-4 mt-2">
                                    {
                                      isTogglePhone ?
                                        <AvForm>
                                          <Row form >
                                            <Col>
                                              <AvField
                                                className="mr-3"
                                                name="ph1"
                                                type="text"
                                                placeholder="xxx"
                                                bsSize="sm"
                                                onChange={this.handleChangePhone}
                                                value={isEditPhone ? isEditPhone.ph1 : addPhone.ph1}
                                                validate={{
                                                  pattern: { value: '^[0-9]+$' },
                                                  minLength: { value: 3 },
                                                  maxLength: { value: 3 },
                                                }}
                                              />
                                            </Col>
                                            <Col>
                                              <AvField
                                                className="mr-3"
                                                name="ph2"
                                                type="text"
                                                placeholder="xxx"
                                                bsSize="sm"
                                                onChange={this.handleChangePhone}
                                                value={isEditPhone ? isEditPhone.ph2 : addPhone.ph2}
                                                validate={{
                                                  pattern: { value: '^[0-9]+$' },
                                                  minLength: { value: 3 },
                                                  maxLength: { value: 3 },
                                                }}
                                              />
                                            </Col>
                                          </Row>
                                          <AvField
                                            className="mr-3"
                                            name="ph3"
                                            type="text"
                                            placeholder="xxxx"
                                            bsSize="sm"
                                            onChange={this.handleChangePhone}
                                            value={isEditPhone ? isEditPhone.ph3 : addPhone.ph3}
                                            validate={{
                                              pattern: { value: '^[0-9]+$' },
                                              minLength: { value: 4 },
                                              maxLength: { value: 4 },
                                            }}
                                          />
                                          <AvField
                                            onChange={this.handleChangePhone}
                                            className="mb-2" bsSize="sm" type="select"
                                            value={isEditPhone ? isEditPhone.label : addPhone.label}
                                            name="label">
                                            {true && <option>Choose Phone Type</option>}
                                            <option value="Tel">Telephone</option>
                                            <option value="Mob">Mobile</option>
                                            <option value="Support">Support</option>
                                          </AvField>
                                          <div className="text-right">
                                            <Button
                                              onClick={this.handleSubmitPhone}
                                              size="sm" className="btn-icon-split mr-2" title="Save"
                                              disabled={isPhoneValid}
                                            >
                                              <span className="text">Save</span>
                                              <span className="icon">
                                                <FontAwesomeIcon icon="check" />
                                              </span>
                                            </Button>
                                            <Button
                                              onClick={this.toggleEditPhone}
                                              size="sm"
                                              title="Cancel"
                                              color="dark"
                                              className="btn-icon-split ml-0"
                                            >
                                              <span className="text">Cancel</span>
                                              <span className="icon"><FontAwesomeIcon icon="times" /></span>
                                            </Button>
                                          </div>
                                          <hr />
                                        </AvForm>
                                        :
                                        <div className="d-flex">
                                          <span className="actionable fs-12 ml-auto" onClick={this.setInitialPhone}><FontAwesomeIcon icon="plus" /> Add Phone Number</span>
                                        </div>
                                    }
                                  </div>
                                </li>

                                <li className="mb-2">
                                  <div className="mb-2">
                                    <div className="d-flex">
                                      <CustomInput type="checkbox" id="bus_info_email_all_2" name="selectEmails" checked={this.state.selectallEmails} onClick={this.handleAllEmailChecked} />
                                      <Label for="bus_info_2" className="font-weight-bold mb-0">
                                        Email
                                      </Label>
                                    </div>
                                  </div>
                                  <ul className="pl-4 fs-12 list-unstyled">
                                    {emailSet && emailSet.length > 0 ? <>{this.emailList()}</> : (<li>  <span className="fs-12"><i>{'No Email Registered'}</i></span>  </li>)}
                                  </ul>

                                  <div className="pl-4 mt-2">
                                    <div className={isToggleBusinessEmail ? "" : "d-none"}>
                                      <AvForm
                                        ref={this.refBusinessEmailForm}
                                        onValidSubmit={this.handleSubmitBusinessEmail}
                                      >
                                        <AvField
                                          onFocus={this.handleChangeBusinessEmail}
                                          onChange={this.handleChangeBusinessEmail}
                                          value={isEditBusinessEmail ? isEditBusinessEmail.email : addBusinessEmail.email}
                                          bsSize="sm"
                                          className="mb-2"
                                          type="email"
                                          name="email"
                                          placeholder="Email..."
                                          validate={{
                                            pattern: { value: emailRegex },
                                          }}
                                        />
                                        <AvField
                                          onFocus={this.handleChangeBusinessEmail}
                                          onChange={this.handleChangeBusinessEmail}
                                          value={isEditBusinessEmail ? isEditBusinessEmail.email_type : addBusinessEmail.email_type}
                                          className="mb-2"
                                          bsSize="sm"
                                          type="select"
                                          name="email_type">
                                          <option>Choose Email Type</option>
                                          <option value="Sales">Sales mail</option>
                                          <option value="Reserve">Reserve mail</option>
                                          <option value="General">General mail</option>
                                        </AvField>
                                        <div className="text-right">
                                          <Button size="sm" className="btn-icon-split mr-2" title="Save"
                                            disabled={isBusinessEmailValid}
                                          >
                                            <span className="text">Save</span>
                                            <span className="icon">
                                              <FontAwesomeIcon icon="check" />
                                            </span>
                                          </Button>
                                          <Button
                                            onClick={this.toggleEditBusinessEmail}
                                            size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                            <span className="text">Cancel</span>
                                            <span className="icon"><FontAwesomeIcon icon="times" /></span>
                                          </Button>
                                        </div>
                                      </AvForm>
                                    </div>
                                    <div className={isToggleBusinessEmail ? "d-none" : ""} onClick={this.setInitialBusinessEmail}>
                                      <div className="d-flex">
                                        <span className="actionable fs-12 ml-auto"><FontAwesomeIcon icon="plus" /> Add Business Email</span>
                                      </div>
                                    </div>
                                  </div>
                                </li>

                                <li className="mb-2">
                                  <div className="mb-2">
                                    <div className="d-flex">
                                      <CustomInput type="checkbox" id="bus_info_payment_all_3" name="selectPayments" checked={this.state.selectallPayment} onClick={this.handleAllPaymentChecked} />
                                      <Label for="bus_info_3" className="font-weight-bold mb-0">
                                        Payment Options
                                      </Label>
                                    </div>
                                  </div>
                                  <ul className="pl-4 fs-12 list-unstyled d-flex flex-wrap mx-n2">
                                    {payOptions && payOptions.length > 0 ? <>{this.payOptions()}</> :
                                      (<li>  <span className="small"><i>{'No Payment Option'}</i></span>  </li>)}
                                  </ul>

                                  <div className="pl-4 mt-2">
                                    <div className={isTogglePayment ? "" : "d-none"}>
                                      <div>
                                        <div className="mb-2">
                                          <Label size="sm" className="p-0 font-weight-bold mb-1">Select Payment Method</Label>
                                          <div className="mb-2">
                                            <Row form>

                                              <Col xs="auto">
                                                <Switch
                                                  value={paymentSwitch.cash ? 1 : 0}
                                                  name="cash"
                                                  onChange={(e) => this.handleSwitch(e, 'cash')}
                                                />  {` Cash `}
                                              </Col>

                                              <Col xs="auto">
                                                <Switch
                                                  value={paymentSwitch.check ? 1 : 0}
                                                  name="check"
                                                  onChange={(e) => this.handleSwitch(e, 'check')}
                                                /> {` Check `}
                                              </Col>

                                              <Col xs="auto">
                                                <Switch
                                                  value={paymentSwitch.discover ? 1 : 0}
                                                  name="discover"
                                                  onChange={(e) => this.handleSwitch(e, 'discover')}
                                                /> {` Discover `}
                                              </Col>
                                              <Col xs="auto">
                                                <Switch
                                                  value={paymentSwitch.visa ? 1 : 0}
                                                  name="visa"
                                                  onChange={(e) => this.handleSwitch(e, 'visa')}
                                                /> {` Visa `}
                                              </Col>

                                              <Col xs="auto">
                                                <Switch
                                                  value={paymentSwitch.mastercard ? 1 : 0}
                                                  name="mastercard"
                                                  onChange={(e) => this.handleSwitch(e, 'mastercard')}
                                                /> {` Mastercard `}
                                              </Col>

                                              <Col xs="auto">

                                                <Switch
                                                  value={paymentSwitch.apple ? 1 : 0}
                                                  name="apple"
                                                  onChange={(e) => this.handleSwitch(e, 'apple')}
                                                /> {` Apple Pay `}
                                              </Col>

                                              <Col xs="auto">

                                                <Switch
                                                  value={paymentSwitch.debit ? 1 : 0}
                                                  name="Debit"
                                                  onChange={(e) => this.handleSwitch(e, 'debit')}
                                                /> {` Debit Card `}
                                              </Col>
                                              <Col xs="auto">
                                                <Switch
                                                  value={paymentSwitch.american ? 1 : 0}
                                                  name="american"
                                                  onChange={(e) => this.handleSwitch(e, 'american')}
                                                /> {` American Express `}
                                              </Col>
                                              <Col xs="auto">
                                                <Switch
                                                  value={paymentSwitch.google ? 1 : 0}
                                                  name="google"
                                                  onChange={(e) => this.handleSwitch(e, 'google')}
                                                /> {` Google Wallet `}
                                              </Col>
                                              <Col xs="auto">
                                                <Switch
                                                  value={paymentSwitch.cryptocurrency ? 1 : 0}
                                                  name="cryptocurrency"
                                                  onChange={(e) => this.handleSwitch(e, 'cryptocurrency')}
                                                /> {` Cryptocurrency `}
                                              </Col>
                                            </Row>

                                          </div>
                                          <div className="text-right">
                                            <Button
                                              onClick={this.handleSubmitPayments}
                                              size="sm" className="btn-icon-split mr-2" title="Submit">
                                              <span className="text">{'Submit'}</span>
                                              <span className="icon">
                                                <FontAwesomeIcon icon="check" />
                                              </span>
                                            </Button>
                                            <Button
                                              onClick={() => { this.setState({ isTogglePayment: false }, () => this.setPaymentReset()) }}
                                              size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                              <span className="text">{'Cancel'}</span>
                                              <span className="icon">
                                                <FontAwesomeIcon icon="times" />
                                              </span>
                                            </Button>
                                          </div>
                                          <hr />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="d-flex">
                                      <span className="actionable fs-12 ml-auto" onClick={this.togglePayment}><FontAwesomeIcon icon="plus" /> Add Payment Options</span>
                                    </div>

                                  </div>
                                </li>

                                <li className="mb-2">
                                  <div className="mb-2">
                                    <div className="d-flex">
                                      <CustomInput type="checkbox" id="bus_info_category_all_4" name="selectCategories" checked={this.state.selectallCategories} onClick={this.handleAllCategoryChecked} />
                                      <Label for="bus_info_4" className="font-weight-bold mb-0">
                                        Categories
                                      </Label>
                                    </div>
                                  </div>
                                  <ul className="pl-4 fs-12 list-unstyled d-flex flex-wrap mx-n2">
                                    {categoryList && categoryList.length > 0 ? <>{this.categoryList()}</> : (<li>  <span className="fs-12"><i>{'No Category'}</i></span>  </li>)}
                                  </ul>

                                  <div className="pl-4 mt-2">
                                    {/* Hide when User adding */}
                                    <div className="d-flex">
                                      <span className="actionable fs-12 ml-auto" onClick={this.modalToggle}><FontAwesomeIcon icon="plus" /> Add Categories</span>
                                    </div>
                                  </div>
                                </li>

                                <li className="mb-2">
                                  <div className="mb-2">
                                    <div className="d-flex">
                                      <CustomInput type="checkbox" id="bus_info_website_all_5" name="selectWebsites" checked={this.state.selectallWebsites} onClick={this.handleAllWebsiteChecked} />
                                      <Label for="bus_info_5" className="font-weight-bold mb-0">
                                        Website
                                      </Label>
                                    </div>
                                  </div>
                                  <ul className="pl-4 fs-12 list-unstyled">
                                    {webSet && webSet.length > 0 ?
                                      <>{this.websiteData()}</>
                                      : (
                                        <li>
                                          <span className="fs-12"><i>No Website Saved</i></span>
                                        </li>
                                      )}
                                  </ul>

                                  <div className="pl-4 mt-2">
                                    <div className={isToggleWebsite ? "" : "d-none"}>
                                      <AvForm
                                        ref={this.refWebsiteForm}
                                        onValidSubmit={this.handleSubmitWebsite}
                                      >
                                        <div className="mb-2">
                                          <AvField
                                            bsSize="sm"
                                            className="mb-2"
                                            type="textarea"
                                            name="website"
                                            value={isEditWebsite ? isEditWebsite.website : addWebsite.website}
                                            onChange={this.handleChangeWebsite}
                                            placeholder="Web URL"
                                            validate={{
                                              pattern: { value: websiteRegex },
                                            }}
                                          />
                                          <AvField
                                            onFocus={this.handleChangeWebsite}
                                            onChange={this.handleChangeWebsite}
                                            value={isEditWebsite ? isEditWebsite.website_type : addWebsite.website_type}
                                            className="mb-2"
                                            bsSize="sm"
                                            type="select"
                                            name="website_type"
                                          >
                                            <option value="">Choose website type</option>
                                            <option value="Main">Main Website</option>
                                            <option value="Facebook">Facebook Website</option>
                                            <option value="Google+">Google Website</option>
                                            <option value="Twitter">Twitter Website</option>
                                            <option value="LinkedIn">LinkedIn Website</option>
                                            <option value="Instagram">Instagram Website</option>
                                          </AvField>
                                          <div className="text-right">
                                            <Button size="sm" className="btn-icon-split mr-2" title="Save"
                                              disabled={isWebsiteValid}
                                            >
                                              <span className="text">Save</span>
                                              <span className="icon">
                                                <FontAwesomeIcon icon="check" />
                                              </span>
                                            </Button>
                                            <Button
                                              // onClick={this.toggleEditWebsite} 
                                              size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                              <span className="text">Cancel</span>
                                              <span className="icon"><FontAwesomeIcon icon="times" /></span>
                                            </Button>
                                          </div>
                                          <hr />
                                        </div>
                                      </AvForm>
                                    </div>

                                    <div className={isToggleWebsite ? "d-none" : ""} onClick={this.setInitialWebsite}>
                                      <div className="d-flex">
                                        <span className="actionable fs-12 ml-auto"><FontAwesomeIcon icon="plus" /> Add Website</span>
                                      </div>
                                    </div>
                                  </div>
                                </li>

                                <li className="mb-2" hidden>
                                  <div className="mb-2">
                                    <div className="d-flex">
                                      <CustomInput type="checkbox" id="bus_info_6" name="selectSocialMedias" />
                                      <Label for="bus_info_6" className="font-weight-bold mb-0">
                                        Social Media
                                      </Label>
                                    </div>
                                  </div>
                                  <ul className="pl-4 fs-12 list-unstyled d-flex flex-wrap mx-n2">
                                    <li className="mb-2 px-2">
                                      <div className="d-flex mx-n1">
                                        <div className="px-1">
                                          <div className="d-flex align-items-start">
                                            <CustomInput type="checkbox" id="bus_info_61" name="selectSocialMedia1" />
                                            <Label for="bus_info_61" className="flex-fill font-weight-bold mb-0">
                                              <FontAwesomeIcon size="lg" icon={['fab', 'facebook-f']} />
                                            </Label>
                                          </div>
                                        </div>
                                      </div>
                                    </li>

                                    <li className="mb-2 px-2">
                                      <div className="d-flex mx-n1">
                                        <div className="px-1">
                                          <div className="d-flex align-items-start">
                                            <CustomInput type="checkbox" id="bus_info_62" name="selectSocialMedia2" />
                                            <Label for="bus_info_62" className="flex-fill font-weight-bold mb-0">
                                              <FontAwesomeIcon size="lg" icon={['fab', 'twitter']} />
                                            </Label>
                                          </div>
                                        </div>
                                      </div>
                                    </li>
                                    <li className="mb-2 px-2">
                                      <div className="d-flex mx-n1">
                                        <div className="px-1">
                                          <div className="d-flex align-items-start">
                                            <CustomInput type="checkbox" id="bus_info_63" name="selectSocialMedia3" />
                                            <Label for="bus_info_63" className="flex-fill font-weight-bold mb-0">
                                              <FontAwesomeIcon size="lg" icon={['fab', 'instagram']} />
                                            </Label>
                                          </div>
                                        </div>
                                      </div>
                                    </li>
                                    <li className="mb-2 px-2">
                                      <div className="d-flex mx-n1">
                                        <div className="px-1">
                                          <div className="d-flex align-items-start">
                                            <CustomInput type="checkbox" id="bus_info_64" name="selectSocialMedia4" />
                                            <Label for="bus_info_64" className="flex-fill font-weight-bold mb-0">
                                              <FontAwesomeIcon size="lg" icon={['fab', 'pinterest']} />
                                            </Label>
                                          </div>
                                        </div>
                                      </div>
                                    </li>
                                    <li className="mb-2 px-2">
                                      <div className="d-flex mx-n1">
                                        <div className="px-1">
                                          <div className="d-flex align-items-start">
                                            <CustomInput type="checkbox" id="bus_info_65" name="selectSocialMedia4" />
                                            <Label for="bus_info_65" className="flex-fill font-weight-bold mb-0">
                                              <FontAwesomeIcon size="lg" icon={['fab', 'linkedin']} />
                                            </Label>
                                          </div>
                                        </div>
                                      </div>
                                    </li>
                                    <li className="mb-2 px-2">
                                      <div className="d-flex mx-n1">
                                        <div className="px-1">
                                          <div className="d-flex align-items-start">
                                            <CustomInput type="checkbox" id="bus_info_66" name="selectSocialMedia4" />
                                            <Label for="bus_info_66" className="flex-fill font-weight-bold mb-0">
                                              <FontAwesomeIcon size="lg" icon={['fab', 'whatsapp']} />
                                            </Label>
                                          </div>
                                        </div>
                                      </div>
                                    </li>
                                  </ul>
                                </li>

                                <li className="mb-2">
                                  <div className="mb-2">
                                    <div className="d-flex">
                                      <CustomInput type="checkbox" id="bus_info_hours_all_7" checked={this.state.selectallHours} name="selectHOPs" onClick={this.handleAllhoursChecked} />
                                      <Label for="bus_info_7" className="font-weight-bold mb-0">
                                        Hours of Operation
                                      </Label>
                                    </div>
                                  </div>
                                  <ul className="pl-4 fs-12 list-unstyled">
                                    {HoursOfOperation && HoursOfOperation.length > 0 ?
                                      <>{this.HoursOfOperation()}</> :
                                      (<li>  <span className="fs-12"><i>{'No Hours of Operation'}</i></span>  </li>)}
                                  </ul>

                                  <div className="pl-4 mt-2">
                                    <AvForm>
                                      <Row className="form-row">
                                        <Col
                                          sm={addHours.start_time !== "Closed" ? '4' : '6'}
                                          md={12} className="col-xxl-4">
                                          <div className="mb-2">
                                            <AvField onChange={this.handleChangeHours} bsSize="sm" type="select" name="day_of_week"
                                              value={isEditHours ? isEditHours.day_of_week : addHours.day_of_week}
                                            >
                                              <option disabled>Select Day</option>
                                              <option value="1">Monday</option>
                                              <option value="2" >Tuesday</option>
                                              <option value="3" >Wednesday</option>
                                              <option value="4">Thursday</option>
                                              <option value="5">Friday</option>
                                              <option value="6">Saturday</option>
                                              <option value="7">Sunday</option>
                                            </AvField>
                                          </div>
                                        </Col>
                                        <Col
                                          sm={addHours.start_time !== "Closed" ? '4' : '6'}
                                          md={12} className="col-xxl-4">
                                          <div className="mb-2">
                                            <AvField onChange={this.handleChangeHours} bsSize="sm" type="select" name="start_time"
                                              value={
                                                isEditHours ? moment(isEditHours.start_time, 'HH:mm:ss').format('HH:mm') :
                                                  addHours.start_time !== "Closed" ? moment(addHours.start_time, 'HH:mm:ss').format('HH:mm') : addHours.start_time}
                                            >
                                              <option disabled>Select Opening Hours</option>
                                              {/* <option value="Closed">Closed</option> */}
                                              <option value="00:00">12:00 am (midnight)</option>
                                              <option value="00:30">12:30 am</option>
                                              <option value="01:00">1:00 am</option>
                                              <option value="01:30">1:30 am</option>
                                              <option value="02:00">2:00 am</option>
                                              <option value="02:30">2:30 am</option>
                                              <option value="03:00">3:00 am</option>
                                              <option value="03:30">3:30 am</option>
                                              <option value="04:00">4:00 am</option>
                                              <option value="04:30">4:30 am</option>
                                              <option value="05:00">5:00 am</option>
                                              <option value="05:30">5:30 am</option>
                                              <option value="06:00">6:00 am</option>
                                              <option value="06:30">6:30 am</option>
                                              <option value="07:00">7:00 am</option>
                                              <option value="07:30">7:30 am</option>
                                              <option value="08:00">8:00 am</option>
                                              <option value="08:30">8:30 am</option>
                                              <option value="09:00">9:00 am</option>
                                              <option value="09:30">9:30 am</option>
                                              <option value="10:00">10:00 am</option>
                                              <option value="10:30">10:30 am</option>
                                              <option value="11:00">11:00 am</option>
                                              <option value="11:30">11:30 am</option>
                                              <option value="12:00">12:00 pm</option>
                                              <option value="12:30">12:30 pm</option>
                                              <option value="13:00">1:00 pm</option>
                                              <option value="13:30">1:30 pm</option>
                                              <option value="14:00">2:00 pm</option>
                                              <option value="14:30">2:30 pm</option>
                                              <option value="15:00">3:00 pm</option>
                                              <option value="15:30">3:30 pm</option>
                                              <option value="16:00">4:00 pm</option>
                                              <option value="16:30">4:30 pm</option>
                                              <option value="17:00">5:00 pm</option>
                                              <option value="17:30">5:30 pm</option>
                                              <option value="18:00">6:00 pm</option>
                                              <option value="18:30">6:30 pm</option>
                                              <option value="19:00">7:00 pm</option>
                                              <option value="19:30">7:30 pm</option>
                                              <option value="20:00">8:00 pm</option>
                                              <option value="20:30">8:30 pm</option>
                                              <option value="21:00">9:00 pm</option>
                                              <option value="21:30">9:30 pm</option>
                                              <option value="22:00">10:00 pm</option>
                                              <option value="22:30">10:30 pm</option>
                                              <option value="23:00">11:00 pm</option>
                                              <option value="23:30">11:30 pm</option>
                                            </AvField>
                                          </div>
                                        </Col>
                                        <Col sm="4" md={12} className="col-xxl-4">
                                          <div className={
                                            addHours.start_time !== "Closed" ? 'mb-2' : 'd-none'}>
                                            <AvField onChange={this.handleChangeHours} bsSize="sm" type="select" name="end_time"
                                              value={isEditHours ? moment(isEditHours.end_time, 'HH:mm:ss').format('HH:mm') : moment(addHours.end_time, 'HH:mm:ss').format('HH:mm')}
                                            >
                                              <option disabled>Select Closing Hours</option>
                                              <option value="00:30">12:30 am</option>
                                              <option value="01:00">1:00 am</option>
                                              <option value="01:30">1:30 am</option>
                                              <option value="02:00">2:00 am</option>
                                              <option value="02:30">2:30 am</option>
                                              <option value="03:00">3:00 am</option>
                                              <option value="03:30">3:30 am</option>
                                              <option value="04:00">4:00 am</option>
                                              <option value="04:30">4:30 am</option>
                                              <option value="05:00">5:00 am</option>
                                              <option value="05:30">5:30 am</option>
                                              <option value="06:00">6:00 am</option>
                                              <option value="06:30">6:30 am</option>
                                              <option value="07:00">7:00 am</option>
                                              <option value="07:30">7:30 am</option>
                                              <option value="08:00">8:00 am</option>
                                              <option value="08:30">8:30 am</option>
                                              <option value="09:00">9:00 am</option>
                                              <option value="09:30">9:30 am</option>
                                              <option value="10:00">10:00 am</option>
                                              <option value="10:30">10:30 am</option>
                                              <option value="11:00">11:00 am</option>
                                              <option value="11:30">11:30 am</option>
                                              <option value="12:00">12:00 pm (noon)</option>
                                              <option value="12:30">12:30 pm</option>
                                              <option value="13:00">1:00 pm</option>
                                              <option value="13:30">1:30 pm</option>
                                              <option value="14:00">2:00 pm</option>
                                              <option value="14:30">2:30 pm</option>
                                              <option value="15:00">3:00 pm</option>
                                              <option value="15:30">3:30 pm</option>
                                              <option value="16:00">4:00 pm</option>
                                              <option value="16:30">4:30 pm</option>
                                              <option value="17:00">5:00 pm</option>
                                              <option value="17:30">5:30 pm</option>
                                              <option value="18:00">6:00 pm</option>
                                              <option value="18:30">6:30 pm</option>
                                              <option value="19:00">7:00 pm</option>
                                              <option value="19:30">7:30 pm</option>
                                              <option value="20:00">8:00 pm</option>
                                              <option value="20:30">8:30 pm</option>
                                              <option value="21:00">9:00 pm</option>
                                              <option value="21:30">9:30 pm</option>
                                              <option value="22:00">10:00 pm</option>
                                              <option value="22:30">10:30 pm</option>
                                              <option value="23:00">11:00 pm</option>
                                              <option value="23:30">11:30 pm</option>
                                              <option value="00:00 1">12:00 am (midnight next day)</option>
                                              <option value="00:30 1">12:30 am (next day)</option>
                                              <option value="01:00 1">1:00 am (next day)</option>
                                              <option value="01:30 1">1:30 am (next day)</option>
                                              <option value="02:00 1">2:00 am (next day)</option>
                                              <option value="02:30 1">2:30 am (next day)</option>
                                              <option value="03:00 1">3:00 am (next day)</option>
                                              <option value="03:30 1">3:30 am (next day)</option>
                                              <option value="04:00 1">4:00 am (next day)</option>
                                              <option value="04:30 1">4:30 am (next day)</option>
                                              <option value="05:00 1">5:00 am (next day)</option>
                                              <option value="05:30 1">5:30 am (next day)</option>
                                              <option value="06:00 1">6:00 am (next day)</option>
                                            </AvField>
                                          </div>
                                        </Col>
                                        {/* <Col sm={addHours.start_time !== "Closed" ? "6" : "12"}> */}
                                        <Col sm="4" md={12} >
                                          <div className="mb-2">
                                            <AvField
                                              onChange={this.handleChangeHours}
                                              value={isEditHours ? isEditHours.info : addHours.info}
                                              bsSize="sm"
                                              type="text"
                                              name="info"
                                              placeholder="i.e. Brunch"
                                            />
                                          </div>
                                        </Col>
                                      </Row>

                                      <div className="text-right">
                                        <Button onClick={this.handleSubmitHours} size="sm" className="btn-icon-split mr-0" title="Submit"
                                          disabled={isHoursValid}
                                        >
                                          <span className="text">{'Add Hours'}</span>
                                          <span className="icon">
                                            <FontAwesomeIcon icon="check" />
                                          </span>
                                        </Button>
                                        {/* <Button onClick={this.toggleEditHours} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                              <span className="text">{'Cancel'}</span>
                                              <span className="icon">
                                                  <FontAwesomeIcon icon="times" />
                                              </span>
                                          </Button> */}
                                      </div>
                                      <hr />
                                    </AvForm>

                                    <div hidden className="d-flex">
                                      <span className="actionable fs-12 ml-auto"><FontAwesomeIcon icon="plus" /> Add Hours of Operation</span>
                                    </div>

                                  </div>
                                </li>


                                <li className="mb-2">
                                  <div className="mb-2">
                                    <div className="d-flex">
                                      <CustomInput type="checkbox" id="bus_info_addresses_all_7" checked={this.state.selectallAddress} name="selectaddresses"
                                        onClick={this.handleAllCheckedAddress}
                                      />
                                      <Label for="bus_info_addresses_all_7" className="font-weight-bold mb-0">
                                        Address
                                      </Label>
                                    </div>
                                  </div>
                                  <ul className="pl-4 fs-12 list-unstyled">
                                    {this.state.AdressDiary &&
                                      this.state.AdressDiary.length > 0 ?
                                      this.businessAddresses() :
                                      (<li>  <span className="fs-12"><i>{'No Address saved'}</i></span>  </li>)}
                                  </ul>

                                  <div className="pl-4 mt-2">
                                    <div className={isToggleAddress ? "" : "d-none"}>
                                      <AvForm
                                        onSubmit={this.handleSubmitAddress}
                                      >
                                        <div className="mb-2">
                                          <AvField bsSize="sm" className="mb-2"
                                            value={isEditAddress ? isEditAddress.address1 : addAddress.address1}
                                            onChange={this.handleChangeAddress}
                                            type="text" name="address1" placeholder="Address 1" errorMessage="This Field is required"
                                            validate={{ required: { value: true } }}
                                          />
                                          <AvField bsSize="sm" className="mb-2"
                                            value={isEditAddress ? isEditAddress.address2 : addAddress.address2} onChange={this.handleChangeAddress}
                                            type="text" name="address2" placeholder="Address 2" errorMessage="This Field is required"
                                            validate={{ required: { value: true } }}
                                          />
                                          <AvField bsSize="sm" className="mb-2"
                                            value={isEditAddress ? isEditAddress.city : addAddress.city}
                                            type="text" name="city"
                                            onChange={this.handleChangeAddress}
                                            placeholder="City" errorMessage="This Field is required"
                                            validate={{ required: { value: true } }}
                                          />
                                          <AvField bsSize="sm" className="mb-2"
                                            value={isEditAddress ? isEditAddress.state : addAddress.state}
                                            type="text" name="state"
                                            onChange={this.handleChangeAddress}
                                            placeholder="State" errorMessage="This Field is required"
                                            validate={{ required: { value: true } }}
                                          />
                                          <Input bsSize="sm" className="mb-2"
                                            value={isEditAddress ? isEditAddress.country : addAddress.country} onChange={this.handleChangeAddress}
                                            type="select" name="country" id="exampleSelect" errormessage="This Field is required"
                                            validate={{ required: { value: true } }}
                                          >
                                            <option value="United States">USA</option>
                                            <option value="CANADA">CANADA</option>
                                          </Input>
                                          <AvField bsSize="sm" className="mb-2"
                                            value={isEditAddress ? isEditAddress.zipcode : addAddress.zipcode} onChange={this.handleChangeAddress}
                                            type="text" name="zipcode" placeholder="Zip Code /  Postal Code" errorMessage="This Field is required"
                                            validate={{ required: { value: true } }}
                                          />
                                          <div className="text-right">
                                            <Button size="sm" className="btn-icon-split mr-2" title="Save">
                                              <span className="text">Save</span>
                                              <span className="icon">
                                                <FontAwesomeIcon icon="check" />
                                              </span>
                                            </Button>
                                            <Button
                                              onClick={this.toggleEditAddress}
                                              size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                              <span className="text">
                                                {'Cancel'}
                                              </span>
                                              <span className="icon">
                                                <FontAwesomeIcon icon="times" />
                                              </span>
                                            </Button>
                                          </div>
                                          <hr />
                                        </div>
                                      </AvForm>
                                    </div>

                                    <div className="d-flex">
                                      <span className="actionable fs-12 ml-auto" onClick={this.setInitialAddress}><FontAwesomeIcon icon="plus" /> Add Address</span>
                                    </div>

                                  </div>
                                </li>
                              </ul>
                            </div>
                          </div>

                        </Col>

                        <Col lg={6}>
                          <div className="mb-3">
                            <div className="d-flex">
                              <span className="mr-2" onClick={() => { this.handleAllCheckedBusinessAll(this.state.checkAllBusinessAll) }}
                                checked={this.state.checkAllBusinessAll}>
                                <Button
                                  color="link"
                                  className="text-primary text-decoration-none px-0 mr-2"
                                >
                                  <FontAwesomeIcon
                                    icon="file"
                                  />{" "}
                              All
                            </Button>
                              </span>
                              <div className="flex-fill">
                                <h3>About this Business</h3>
                                <hr />
                              </div>
                            </div>

                          </div>
                          <div>
                            <ul className="list-unstyled mb-2 text-secondary-dark scrollable-list">
                              <li className="mb-2">
                                <div className="mb-2">
                                  <div className="d-flex">
                                    <CustomInput type="checkbox" id="about_bus_1" onClick={this.handleCheckChieldAbout} name="selectSpeciality" checked={this.state.ischeckedSpecial} />
                                    <Label for="about_bus_1" className="font-weight-bold mb-0">
                                      Specialities
                                    </Label>
                                  </div>
                                  <div className="pl-4">
                                    <div className="d-flex mb-2">
                                      <div className="mr-2 small flex-grow-1">
                                        {this.bizpecialities()}
                                      </div>
                                      <div className={isToggleBusiness ? "d-none" : "ml-auto"}>
                                        <span className="actionable text-dark" onClick={this.EditSpec}><FontAwesomeIcon icon="pencil-alt" size="sm" /> </span>
                                      </div>
                                    </div>

                                    <div className={this.state.editSpec ? "d-none" : ""}>
                                      <div className="mb-2">
                                        <Input
                                          onChange={(e) => { this.handleAboutChange(e) }}
                                          bsSize="sm" className="mb-2" type="textarea" name="specialities" placeholder="Enter specialities"
                                          value={this.state.editValue}
                                        />
                                        <div className="text-right">
                                          <div className="text-right">
                                            <Button size="sm" className="btn-icon-split mr-2" title="Save"
                                              onClick={this.saveVaule}
                                            > <span className="text">Save</span> <span className="icon"> <FontAwesomeIcon icon="check" /> </span></Button>
                                            <Button size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0"
                                              onClick={this.EditSpec}
                                            > <span className="text">Cancel</span> <span className="icon"><FontAwesomeIcon icon="times" /></span> </Button>
                                          </div>
                                        </div>
                                      </div>
                                      <hr />
                                    </div>

                                  </div>
                                </div>
                              </li>
                              <li className="mb-2">
                                <div className="mb-2">
                                  <div className="d-flex">
                                    <CustomInput type="checkbox" id="about_bus_2" name="selectHistory" onClick={this.handleCheckChieldAbout} checked={this.state.ischeckedHistory} />
                                    <Label for="about_bus_2" className="font-weight-bold mb-0">
                                      History
                                    </Label>
                                  </div>
                                  <div className="pl-4">
                                    <div className="d-flex mb-2">
                                      <div className="mr-2 small flex-grow-1">
                                        {this.Ownerhistory()}
                                      </div>
                                      <div className={isToggleHistory ? "d-none" : "ml-auto"}>
                                        <span className="actionable text-dark" onClick={this.EditHistory}><FontAwesomeIcon icon="pencil-alt" size="sm" /> </span>
                                      </div>
                                    </div>

                                    <div className={this.state.editHisSpec ? "d-none" : ""}>
                                      <div className="mb-2">
                                        <Input
                                          onChange={(e) => { this.handleHistory(e) }}
                                          bsSize="sm" className="mb-2" type="textarea" name="history" placeholder="Enter history"
                                          value={this.state.editHistory}
                                        />
                                        <div className="text-right">
                                          <div className="text-right">
                                            <Button size="sm" className="btn-icon-split mr-2" title="Save"
                                              onClick={this.saveHistVaule}
                                            > <span className="text">Save</span> <span className="icon"> <FontAwesomeIcon icon="check" /> </span></Button>
                                            <Button size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0"
                                              onClick={this.EditHistory}
                                            > <span className="text">Cancel</span> <span className="icon"><FontAwesomeIcon icon="times" /></span> </Button>
                                          </div>
                                        </div>
                                      </div>
                                      <hr />
                                    </div>
                                  </div>
                                </div>
                              </li>
                              <li className="mb-2">
                                <div className="mb-2">
                                  <div className="d-flex">
                                    <CustomInput type="checkbox" id="about_bus_3" name="selectMBO" onClick={this.handleCheckChieldAbout} checked={this.state.ischeckedMBO} />
                                    <Label for="about_bus_3" className="font-weight-bold mb-0">
                                      Meet the business owner
                                    </Label>
                                  </div>
                                  <div className="pl-4">
                                    <div className="d-flex mb-2">
                                      <div className="mr-2 small flex-grow-1">
                                        {this.ownerMeet()}
                                      </div>
                                      <div className={isToggleMeet ? "d-none" : "ml-auto"}>
                                        <span className="actionable text-dark" onClick={this.EditBussOwn}><FontAwesomeIcon icon="pencil-alt" size="sm" /> </span>
                                      </div>
                                    </div>

                                    <div className={this.state.editBusiness ? "d-none" : ""}>
                                      <div className="mb-2">
                                        <Input
                                          onChange={(e) => { this.handleBussOwn(e) }}
                                          bsSize="sm" className="mb-2" type="textarea" name="b_owner" placeholder="Enter Business..."
                                          value={this.state.EditBussOwn}
                                        />
                                        <div className="text-right">
                                          <div className="text-right">
                                            <Button size="sm" className="btn-icon-split mr-2" title="Save"> <span className="text"
                                              onClick={this.saveBussOwn}
                                            >Save</span> <span className="icon"> <FontAwesomeIcon icon="check" /> </span></Button>
                                            <Button size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0"
                                              onClick={this.EditBussOwn}
                                            > <span className="text">Cancel</span> <span className="icon" ><FontAwesomeIcon icon="times" /></span> </Button>
                                          </div>
                                        </div>
                                      </div>
                                      <hr />
                                    </div>
                                  </div>
                                </div>
                              </li>
                              <li className="mb-2">
                                <div className="mb-2">
                                  <div className="d-flex">
                                    <CustomInput type="checkbox" id="about_bus_4" name="selectBusinessOwner" onClick={this.handleAllOwnerChecked} checked={this.state.isCheckedAllOwner} />
                                    <Label for="about_bus_4" className="font-weight-bold mb-0">
                                      Business Owner
                                </Label>
                                  </div>
                                </div>
                                <ul className="pl-4 fs-12 list-unstyled">
                                  <li className="mb-2">
                                    <div className="d-flex mx-n1">
                                      <div className="px-1">
                                        <div className="d-flex align-items-start">
                                          <CustomInput type="checkbox" id="about_bus_41" name="selectProfilePic" checked={this.state.isCheckedProfile} value={ownerProfile ? ownerProfile : "https://stagingdatawikireviews.s3.amazonaws.com/images/circle.png"} onChange={this.handleChangedBranches} />
                                          <Label for="about_bus_41" className="flex-fill font-weight-bold mb-0">
                                            <img width="80" src={profilePic ? profilePic : "https://stagingdatawikireviews.s3.amazonaws.com/images/circle.png"} alt="Profile Pic" />
                                            <div className="text-left mt-2">
                                              <Label className="text-dark hover-blue fs-14" role="button" for="updateOwnerImage">
                                                Update
                                              </Label>
                                              <input type="file" id="updateOwnerImage" accept="image/x-png,image/gif,image/jpeg" hidden />
                                              <span className="mx-2">&middot;</span>
                                              <span className="text-dark hover-blue fs-14" role="button">
                                                Reset
                                              </span>
                                            </div>
                                          </Label>
                                        </div>
                                      </div>

                                      {/* unhide for edit functionality */}
                                      <div className="ml-auto col-auto px-1" hidden>
                                        <div className="interactive">
                                          <div className="interactive-appear text-nowrap">
                                            {/* <ButtonGroup>
                                              <Button size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button>
                                            </ButtonGroup> */}
                                            <EditBtn color="gold" />
                                            {/* <DeleteBtn color="gold" /> */}
                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                  </li>
                                  <li className="mb-2">
                                    <div className="d-flex mx-n1">
                                      <div className="px-1 flex-fill">
                                        <div className="d-flex align-items-start">
                                          <CustomInput type="checkbox" id="about_bus_42" checked={this.state.isCheckedFullName} name="selectName" value={fullName && fullName != null ? fullName : ""} onChange={this.handleChangedBranches} />
                                          <Label for="about_bus_42" className="flex-fill font-weight-bold mb-0">
                                            Name: <span>{fullName ? fullName : "Provide Your First and Last Name"}</span>
                                          </Label>

                                        </div>
                                      </div>

                                      {/* unhide for edit functionality */}
                                      <div className="col-auto px-1">
                                        <span className="actionable ml-auto fs-12" onClick={this.EditName}> Update</span>
                                      </div>
                                    </div>

                                    <div className={this.state.editName ? "d-none" : "mt-2 pl-4"}>
                                      <AvForm ref={this.refNameForm} onValidSubmit={this.saveName}>
                                        <AvField
                                          className="mr-2"
                                          bsSize="sm"
                                          type="text"
                                          name="fname"
                                          placeholder="First Name"
                                          value={this.state.fName}
                                          onChange={(e) => {
                                            this.handleNameChange(e);
                                          }}
                                          validate={{
                                            pattern: { value: '^[a-zA-Z ]*$' }
                                          }}
                                        />

                                        <AvField
                                          bsSize="sm"
                                          type="text"
                                          name="lname"
                                          placeholder="Last Name"
                                          value={this.state.lName}
                                          onChange={(e) => {
                                            this.handleChange2(e);
                                          }}
                                          validate={{
                                            pattern: { value: '^[a-zA-Z ]*$' }
                                          }}
                                        />
                                        <div className="text-right">
                                          <Button disabled={this.state.isValidFullName} size="sm" className="btn-icon-split mr-2" title="Save">
                                            <span className="text">Save</span>
                                            <span className="icon">
                                              <FontAwesomeIcon icon="check" />
                                            </span>
                                          </Button>
                                          <Button onClick={this.EditName} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                            <span className="text">
                                              {'Cancel'}
                                            </span>
                                            <span className="icon">
                                              <FontAwesomeIcon icon="times" />
                                            </span>
                                          </Button>
                                        </div>
                                      </AvForm>
                                    </div>


                                  </li>
                                  <li className="mb-2">
                                    <div className="d-flex mx-n1">
                                      <div className="px-1 flex-fill">
                                        <div className="d-flex align-items-start">
                                          <CustomInput type="checkbox" id="about_bus_45" checked={this.state.isCheckedPosition} name="selectPostion" value={position && position != null ? position : ""} onChange={this.handleChangedBranches} />
                                          <Label for="about_bus_45" className="flex-fill font-weight-bold mb-0">
                                            Position: <span>{position ? position : "Add your position"}</span>
                                            <textarea className="text-secondary-dark ff-alt" defaultValue={position ? position : "Add your position"} hidden></textarea>
                                          </Label>
                                        </div>
                                      </div>

                                      {/* unhide for edit functionality */}
                                      <div className={"col-auto px-1"}>
                                        <span className="actionable ml-auto fs-12" onClick={this.EditPosition}>{position ? "Update" : "Add"}</span>
                                      </div>
                                    </div>

                                    {/* When clicked on Update show below */}
                                    <div className={isTogglePosition ? "d-none" : "mt-2 pl-4"}>
                                      <AvForm ref={this.refPositionForm} onValidSubmit={this.savePosition}>
                                        <AvField
                                          bsSize="sm"
                                          type="text"
                                          name="position"
                                          placeholder="Enter Position..."
                                          value={current_role_and_permissions && current_role_and_permissions.role === '' ? position : position}
                                          onChange={(e) => {
                                            this.handlePosition(e);
                                          }}
                                          validate={{
                                            pattern: { value: '^[a-zA-Z ]*$' }
                                          }}
                                        />

                                        <div className="text-right">
                                          <Button disabled={this.state.isValidPosition} size="sm" className="btn-icon-split mr-2" title="Save">
                                            <span className="text">Save</span>
                                            <span className="icon">
                                              <FontAwesomeIcon icon="check" />
                                            </span>
                                          </Button>
                                          <Button onClick={() => {
                                            this.setState({
                                              position: profileData.professional && profileData.professional[0] && profileData.professional[0].profession && profileData.professional[0].profession.title, isTogglePosition: true
                                            })
                                          }} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                            <span className="text">
                                              {'Cancel'}
                                            </span>
                                            <span className="icon">
                                              <FontAwesomeIcon icon="times" />
                                            </span>
                                          </Button>
                                        </div>

                                      </AvForm>
                                    </div>
                                  </li>

                                  <li className="mb-2" hidden>
                                    <div className="d-flex mx-n1">
                                      <div className="px-1 flex-fill">
                                        <div className="d-flex align-items-start">
                                          <CustomInput type="checkbox" id="about_bus_43" name="selectEmail" checked={this.state.isCheckedEmail} value={(userEmail ? userEmail : "")} onChange={this.handleChangedBranches} />
                                          <Label for="about_bus_43" className="flex-fill font-weight-bold mb-0">
                                            Email: <span>{(userEmail ? userEmail : "No Email")}</span>
                                            <textarea hidden className="text-secondary-dark ff-alt" defaultValue={(userEmail ? userEmail : "No Email")} hidden></textarea>
                                          </Label>
                                        </div>
                                      </div>

                                      {/* unhide for edit functionality */}
                                      <div hidden className="col-auto px-1">
                                        <span className="actionable ml-auto fs-12">{position ? "Update" : "Add"}</span>
                                      </div>

                                    </div>
                                  </li>
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </Col>

                        <Col lg="6">

                          <div className="mb-3">
                            <div className="d-flex">
                              <span className="mr-2" onClick={this.handleAllCheckedAdditional}
                                checked={this.state.checkAllAditional}>
                                <Button
                                  color="link"
                                  className="text-primary text-decoration-none px-0"
                                >
                                  <FontAwesomeIcon
                                    icon="file"
                                    className="mr-2"
                                    name="selectAdditional"

                                  />{" "}
        All
      </Button>
                              </span>
                              <div className="flex-fill">
                                <h3>Additional Information</h3>
                                <hr />
                              </div>
                            </div>

                            <div>
                              <ul className="list-unstyled mb-2 text-secondary-dark scrollable-list">
                                {additionInfo
                                  && Array.isArray(additionInfo)
                                  && additionInfo.length > 0 ?
                                  additionInfo.map((item, index) => {
                                    return this.renderItems({
                                      title: item?.name,
                                      value: item?.value,
                                      index,
                                      item,
                                      edit: this.toggleAcceptCreditCards,
                                      trash: this.toggleAcceptCreditCards
                                    },
                                      false)
                                  }) : null}
                              </ul>
                            </div>
                          </div>

                        </Col>


                        <Col lg="6">
                          <div className="mb-3">
                            <div className="d-flex">
                              <span className="mr-2" onClick={this.handleAllActionChecked}>
                                <Button
                                  color="link"
                                  className="text-primary text-decoration-none px-0 mr-2"

                                >
                                  <FontAwesomeIcon
                                    icon="file"
                                  />{" "}
                              All
                            </Button>
                              </span>
                              <div className="flex-fill">
                                <h3>Call to Action</h3>
                                <hr />
                              </div>
                            </div>
                            <div>
                              <ul className="list-unstyled mb-2 text-secondary-dark 
                              // scrollable-list 
                              fs-14">
                                {this.state.corporateCallList &&
                                  this.state.corporateCallList.length > 0 ?
                                  (<div>
                                    {this.calltoactionData()}
                                  </div>) :
                                  (
                                    <div className="bg-white mb-3" style={style}>
                                      <div className="p-2">
                                        <h2 className="text-secondary">Choose the action you want</h2>
                                        <hr className="bg-secondary" />
                                      </div>

                                      <img className="w-100"
                                        src={'https://stagingdatawikireviews.s3.amazonaws.com/images/' + hoverImage} alt="Call to action" />

                                      <div className="p-3">
                                        <div className="text-primary-dark">
                                          <p className="font-weight-bold">Available Actions</p>
                                          <Row md={3} className="text-center mb-3" noGutters>
                                            {bizCallonType && Array.isArray(bizCallonType) && bizCallonType.length > 0 ?
                                              bizCallonType.map(item => (
                                                <Col key={item.id}>
                                                  <div className="p-2 bg-light" onMouseEnter={() => this.onHover(item.action_type)}>
                                                    <span>{item.action_type}</span>
                                                  </div>
                                                </Col>
                                              )) : null}
                                          </Row>

                                          <Button block size="lg" color="primary" onClick={this.getStarted}>
                                            Get Started
                                     </Button>
                                        </div>
                                      </div>
                                    </div>

                                    //  <div>
                                    //     <span className="small"><i>{'No Call to Action Display'}</i></span>
                                    //   </div>
                                  )}

                                {/* Setting up the call block */}
                                <div className="bg-white mb-3" style={style2}>
                                  <div className="p-2">
                                    <h2 class="text-secondary">Setting up your call to action </h2>
                                    <hr className="bg-secondary" />

                                    <AvForm className="p-3" ref="form">
                                      <Row className="justify-content-center">
                                        <Col xs={12}>
                                          <FormGroup row>
                                            <Label sm={12} className="text-dark" size="sm">Choose type of action</Label>
                                            <Col sm={12}>
                                              <AvField type="select" value={typeService} onChange={this.handleChangeCallToAction} name="type_of_service" className="bg-white" id="exampleSelectMulti" errorMessage="This Field is required" validate={{ required: { value: true } }}>
                                                <option>Select Type</option>
                                                {(typeof bizCallonType !== 'undefined' && bizCallonType.length > 0) ? bizCallonType.map(dropdown => {
                                                  return <option key={dropdown.id} dropdown={dropdown} value={dropdown.action_type}>
                                                    {dropdown.action_type}</option>;
                                                }) : (<option> </option>)
                                                }
                                              </AvField>
                                            </Col>
                                          </FormGroup>
                                          <div>
                                            {this.state.typeService == "Request A Quote" ? (
                                              <FormGroup row>
                                                <Label sm={4} className="text-dark" size="sm">Response time</Label>
                                                <Col sm={8}>
                                                  <AvField type="text" value={response_time} onChange={this.handleChangeCallToAction} id="response_time" name="response_time" placeholder="i.e. 2 hours" className="bg-white" maxLength="15" errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                                  <FormText color="muted">Not more than 15 characters</FormText>
                                                </Col>
                                              </FormGroup>
                                            ) : ('')}
                                            <FormGroup row>
                                              <Label sm={4} className="text-dark" size="sm">Headline</Label>
                                              <Col sm={8}>
                                                <AvField type="text" value={headline} onChange={this.handleChangeCallToAction} id="headline" name="headline" placeholder="i.e. Save 20%" className="bg-white" maxLength="15" errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                                <FormText color="muted">Not more than 15 characters</FormText>
                                              </Col>
                                            </FormGroup>
                                            <FormGroup row>
                                              <Label sm={4} className="text-dark" size="sm">Sub Headline</Label>
                                              <Col sm={8}>
                                                <AvField type="text" value={sub_headline} onChange={this.handleChangeCallToAction} id="sub_headline" name="sub_headline" placeholder="i.e. if you book online" className="bg-white" maxLength="30" errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                                <FormText color="muted">Not more than 30 characters</FormText>
                                              </Col>
                                            </FormGroup>
                                            <FormGroup row>
                                              <Label sm={4} className="text-dark" size="sm">Linked URL</Label>
                                              <Col sm={8}>
                                                <AvField type="url" value={linked_url} onChange={this.handleChangeCallToAction} id="linked_url" name="linked_url" placeholder="ex: http://yourwebsite.com" className="bg-white" errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                              </Col>

                                            </FormGroup>
                                          </div>
                                          {this.state.headline != "" ? (
                                            <Row className="my-4">
                                              <Col md={5}>
                                                <div className="bg-primary p-4">
                                                  <div className="d-flex mx-n3">
                                                    <div className="px-3">
                                                      {previewImage !== '' ?
                                                        <img width="50" src={require('../../assets/images/' + previewImage)} alt="" /> :
                                                        ""}
                                                    </div>
                                                    <div className="px-3 flex-fill">
                                                      <div className="text-center">
                                                        <h3 className="mb-3 text-dark">{headline}</h3>
                                                        <h4 className="mb-3 text-white">{sub_headline}</h4>

                                                        <Button color="dark">{typeService}</Button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </Col>
                                            </Row>
                                          ) : ('')}
                                          <Row md={2} form>
                                            <Col>
                                              <Button block color="light" onClick={() => { this.handleOnBiz("c2a") }}>Cancel</Button>
                                            </Col>
                                            <Col>
                                              <Button block color="primary" onClick={this.handleSubmitCallToAction}>Done</Button>
                                            </Col>
                                          </Row>
                                        </Col>
                                      </Row>
                                    </AvForm>
                                  </div>
                                </div>
                              </ul>
                            </div>
                          </div>
                        </Col>
                        <Col lg="6">

                          <div className="mb-3">
                            <div className="d-flex">
                              <span className="mr-2" onClick={this.handleAllOfferChecked}>
                                <Button
                                  color="link"
                                  className="text-primary text-decoration-none px-0 mr-2"
                                >
                                  <FontAwesomeIcon
                                    icon="file"
                                  />{" "}
                              All
                            </Button>
                              </span>
                              <div className="flex-fill">
                                <h3>WikiReviews Special Offers</h3>
                                <hr />
                              </div>
                            </div>

                            <div>
                              <ul className="list-unstyled mb-2 text-secondary-dark scrollable-list">
                                <li className="mb-2">
                                  <ul className="pl-4 fs-12 list-unstyled">
                                    {specialofferSet && Array.isArray(specialofferSet) && specialofferSet.length > 0 ?
                                      specialofferSet.map((specialofferData, specialindex) => (
                                        <li className="mb-1" key={specialindex}>
                                          <div className="d-flex mx-n1">
                                            <div className="px-1 flex-grow-1">
                                              <div className="d-flex align-items-start" onClick={this.handleCheckChieldOffers}>
                                                <CustomInput type="checkbox" id={"wiki_sp_offer_" + specialindex} name="selectOffer1" checked={specialofferData.isChecked} onChange={() => { }} value={specialofferData.id} />
                                                <Label for={"wiki_sp_offer_" + specialindex} className="flex-fill font-weight-bold mb-0">
                                                  <FontAwesomeIcon icon="ticket-alt" />
                                                  <span>{" "}{specialofferData.special_offer}{" "}</span>
                                                </Label>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="d-flex mx-n1 pl-4">
                                            <div className="px-1 flex-grow-1">
                                              <div className="fs-12">
                                                Valid until <span>{moment(specialofferData.to_date).format('MMMM DD YYYY')}</span>
                                              </div>
                                            </div>
                                            <div className="px-1 flex-shrink-0 interactive">
                                              <div className="interactive-appear text-nowrap">
                                                {/* <ButtonGroup>
                                                  <Button onClick={() => this.toggleEditOffer(specialofferData)} size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button>
                                                  <Button onClick={() => this.toggleDeleteOffers(specialofferData.id)} size="sm" color="danger" title="Delete"><FontAwesomeIcon icon="trash-alt" /></Button>
                                                </ButtonGroup> */}
                                                <EditBtn onClick={() => this.toggleEditOffer(specialofferData)} />
                                                <DeleteBtn onClick={() => this.toggleDeleteOffers(specialofferData.id)} color="gold" />
                                              </div>
                                            </div>
                                          </div>
                                        </li>
                                      )) : <li className="bg-white p-3">
                                        <h2 className="text-secondary-dark">No Special Offers to Display</h2>
                                      </li>}
                                  </ul>

                                  <div className="pl-4">
                                    <div className={isToggleOffer ? "" : "d-none"}>
                                      <AvForm className="mt-2"
                                        onSubmit={this.handleSubmitOffer}
                                      >
                                        <div className="mb-2">
                                          <AvField
                                            bsSize="sm"
                                            className="mb-2"
                                            type="textarea"
                                            name="special_offer"
                                            placeholder="Add Offer"
                                            onChange={this.handleChangeOffer} value={isEditOffer ? isEditOffer.special_offer : addOffer.special_offer}
                                            errorMessage="This Field is required"
                                            validate={{ required: { value: true } }}
                                          />
                                          <AvField type="select"
                                            bsSize="sm"
                                            name="offer_type"
                                            className="mb-2"
                                            value={isEditOffer ? isEditOffer.offer_type : addOffer.offer_type}
                                            onChange={this.handleChangeOffer}
                                            errorMessage="This Field is required"
                                            validate={{ required: { value: true } }}
                                          >
                                            <option value="">Choose offer type</option>
                                            <option value="coupons">Coupons</option>
                                            <option value="special_discount">Special Discount</option>
                                          </AvField>
                                          <div className="mb-2">
                                            <Label className="d-block mb-0" size="sm">Start Date</Label>
                                            <AvField
                                              bsSize="sm"
                                              type="date"
                                              name="from_date"
                                              value={isEditOffer ? isEditOffer.from_date : addOffer.from_date}
                                              onChange={this.handleChangeOffer}
                                              errorMessage="This Field is required"
                                              validate={{ required: { value: true } }}
                                            ></AvField>
                                          </div>
                                          <div className="mb-2">
                                            <Label className="d-block mb-0" size="sm">End Date</Label>
                                            <AvField
                                              bsSize="sm"
                                              type="date"
                                              name="to_date"
                                              value={isEditOffer ? isEditOffer.to_date : addOffer.to_date}
                                              onChange={this.handleChangeOffer}
                                              errorMessage="This Field is required"
                                              validate={{ required: { value: true } }} min={moment().format("YYYY-MM-DD")}
                                            ></AvField>
                                          </div>
                                          <AvField
                                            bsSize="sm"
                                            className="mb-2"
                                            name="claim_deal_link"
                                            type="text"
                                            placeholder="Website to claim deal"
                                            value={
                                              claim_deal_link_ur ?
                                                "None" : addOffer.claim_deal_link
                                            }
                                            onChange={this.handleChangeOffer}
                                            errorMessage="This Field is required"
                                            validate={{
                                              required: { value: true }
                                            }}
                                          >
                                          </AvField>
                                          <div className="text-right">
                                            <Button size="sm" className="btn-icon-split mr-2" title="Save"> <span className="text">Save</span> <span className="icon"> <FontAwesomeIcon icon="check" /> </span></Button>
                                            <Button
                                              onClick={this.toggleEditOffer}
                                              size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0"> <span className="text">Cancel</span> <span className="icon"><FontAwesomeIcon icon="times" /></span> </Button>
                                          </div>
                                          <hr />
                                        </div>
                                      </AvForm>
                                    </div>
                                    <div className={isToggleOffer ? "d-none" : ""} onClick={this.setInitialOffer}>
                                      <div className="d-flex">
                                        <span className="actionable fs-12 ml-auto"><FontAwesomeIcon icon="plus" /> Add Offer</span>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter className="bg-white">
            <div className="text-right mt-2">
              <Button color="transparent" className="mw" size="lg" onClick={() => this.setState({ cancelCopyOpModal: true })}>Cancel</Button>
              <Button color="primary" className="mw" size="lg" onClick={() => { this.setState({ copyInfoToBranchModalStep2: true }) }}>Next</Button>
            </div>
          </ModalFooter>
        </Modal>

        {/* Copy Info to branch Modal Step 2 */}
        <Modal
          size="lg"
          isOpen={this.state.copyInfoToBranchModalStep2}
          toggle={() => this.setState({ copyInfoToBranchModalStep2: !this.state.copyInfoToBranchModalStep2 })}
        >
          <ModalBody className="bg-light">
            <div className="text-center">
              <button
                type="button"
                className="close"
                onClick={() => this.setState({ copyInfoToBranchModalStep2: !this.state.copyInfoToBranchModalStep2 })}
                style={{ float: "initial" }}
              >
                ×
              </button>
              <br />
              <h4 className="modal-title">Copy Info</h4>
              <p className="ff-base">
                Please review all data to be copied over to other listing
              </p>
            </div>

            <Row form>
              <Col xs={12}>
                <Button color="link text-primary p-0" size="sm" onClick={this.editCopiedSelectionModalToggle}>
                  <FontAwesomeIcon icon="angle-left" size="sm" />
                  {this.state.preferredMethod === 'freshData' ? ' Modify' : ' Back to selection'}
                </Button>
              </Col>
              <Col lg={6}>
                <div className="bg-white p-3" style={{ minHeight: '275px', maxHeight: 'calc(100vh - 300px)', overflowY: 'auto' }}>
                  <ul className="list-unstyled">
                    <li className="mb-2">
                      <div className="d-flex mx-n2">
                        <div className="px-2">
                          <span className="font-weight-bold">Branding</span>
                        </div>
                        {/* <div className="px-2 ml-auto">
                          <Button color="dark" title="Edit" size="sm" onClick={this.editCopiedSelectionModalToggle}><FontAwesomeIcon icon="pencil-alt" size="sm" /> </Button>
                        </div> */}
                      </div>
                      <ul className="list-unstyled mt-2 fs-14 text-secondary-dark font-weight-bold">
                        <li className={freshprofileBranchLogoUrl != "" ? "mb-2" : "d-none"}>
                          <FontAwesomeIcon icon="check-circle" fixedWidth className="mr-2 text-primary" /> Branch Logo
                          <div>
                            <img width="120" height="90" className="object-fit-contain" id="logoUrl"
                              src={freshprofileBranchLogoUrl && freshprofileBranchLogoUrl != "" ? freshprofileBranchLogoUrl : require("../../assets/images/blank-placeholder.png")}
                              alt="Logo" />
                          </div>
                        </li>
                        <li className={freshProfileBranchBannerUrl != "" ? "mb-2" : "d-none"}>
                          <FontAwesomeIcon icon="check-circle" fixedWidth className="mr-2 text-primary" /> Homepage Image
                          <div>
                            <img width="120" height="90" className="object-fit-contain" id="videoUrl"
                              src={freshProfileBranchBannerUrl && freshProfileBranchBannerUrl != "" ? freshProfileBranchBannerUrl : require("../../assets/images/blank-placeholder.png")}
                              alt="Image" />
                          </div>
                        </li>
                        <li className={logo_to_copy != "" ? "mb-2" : "d-none"}>
                          <FontAwesomeIcon icon="check-circle" fixedWidth className="mr-2 text-primary" /> Branch Logo
                          <img width="120" height="120" className="img-thumbnail" id="logoUrl"
                            src={logo_to_copy && logo_to_copy != "" ? logo_to_copy : require("../../assets/images/blank-placeholder.png")}
                            alt="Logo" />
                        </li>
                        <li className={video_to_copy != "" ? "mb-2" : "d-none"}>
                          <FontAwesomeIcon icon="check-circle" fixedWidth className="mr-2 text-primary" /> Homepage Image
                          <img width="240" className="img-thumbnail" id="videoUrl"
                            src={video_to_copy && video_to_copy != "" ? video_to_copy : require("../../assets/images/blank-placeholder.png")}

                            alt="Image" />
                        </li>
                      </ul>
                      <hr />
                    </li>
                    <li className="mb-2">
                      <div className="d-flex mx-n2">
                        <div className="px-2">
                          <span className="font-weight-bold">Additional Information</span>
                        </div>
                        {/* <div className="px-2 ml-auto">
                          <Button color="dark" title="Edit" size="sm" onClick={this.editCopiedSelectionModalToggle}><FontAwesomeIcon icon="pencil-alt" size="sm" /> </Button>
                        </div> */}
                      </div>
                      <ul className="list-unstyled mt-2 fs-14 text-secondary-dark font-weight-bold">
                        {this.state.copyInfoToBranchModalStep2 ? this.previewAdditional() : ""}
                      </ul>
                      <hr />
                    </li>
                    <li className="mb-2">
                      <div className="d-flex mx-n2">
                        <div className="px-2">
                          <span className="font-weight-bold">Business Information</span>
                        </div>
                        {/* <div className="px-2 ml-auto">
                          <Button color="dark" title="Edit" size="sm" onClick={this.editCopiedSelectionModalToggle}><FontAwesomeIcon icon="pencil-alt" size="sm" /> </Button>
                        </div> */}
                      </div>
                      <ul className="list-unstyled mt-2 fs-14 text-secondary-dark font-weight-bold">
                        <li className="mb-2">
                          <FontAwesomeIcon icon="check-circle" fixedWidth
                            className={this.state.preferredMethod === 'freshData' ?
                              this.state.phoneDiary && this.state.phoneDiary[0] && this.state.phoneDiary[0].phoneFresh !== '' ? "mr-2 text-primary" : "mr-2 text-danger"
                              : this.state.selectedPhone.length > 0 ? "mr-2 text-primary" : "mr-2 text-danger"}
                          />
                          Phone Number
                          <ul className="pl-4 list-unstyled mt-1 fs-12">
                            {this.state.preferredMethod === 'freshData' ?
                              this.state.phoneDiary &&
                                this.state.phoneDiary[0] &&
                                this.state.phoneDiary[0].phoneFresh !== '' ?
                                this.previewPhone() : ""
                              : this.state.selectedPhone.length > 0 ?
                                this.previewPhone() : ""}
                            {/* {this.previewPhone()} */}
                          </ul>
                        </li>
                        <li className="mb-2">
                          <FontAwesomeIcon icon="check-circle" fixedWidth
                            className={
                              this.state.preferredMethod === 'freshData' ?
                                this.state.emailSet && this.state.emailSet[0] && this.state.emailSet[0].freshemailText !== '' ? "mr-2 text-primary" : "mr-2 text-danger"
                                : this.state.selectedEmail.length > 0 ? "mr-2 text-primary" : "mr-2 text-danger"
                            }
                          />
                          Email
                          <ul className="pl-4 list-unstyled mt-1 fs-12">
                            {this.previewEmail()}
                          </ul>
                        </li>
                        {this.state.preferredMethod === 'freshData' ?
                          <li
                            className="mb-2">
                            <FontAwesomeIcon icon="check-circle" fixedWidth
                              className={
                                this.state.freshSelectedPayment.length > 0 ? "mr-2 text-primary" : "mr-2 text-danger"
                              }
                            />
                         Payment Options
                         <ul className="pl-4 list-unstyled mt-1 fs-12 d-flex flex-wrap mx-n1">
                              {this.previewpayOptions()}
                            </ul>
                          </li> : <li
                            className="mb-2">
                            <FontAwesomeIcon icon="check-circle" fixedWidth
                              className={
                                this.state.selectedPayment.length > 0 ? "mr-2 text-primary" : "mr-2 text-danger"
                              }
                            />
                          Payment Options
                          <ul className="pl-4 list-unstyled mt-1 fs-12 d-flex flex-wrap mx-n1">
                              {this.previewpayOptions()}
                            </ul>
                          </li>}
                        {this.state.preferredMethod === 'freshData' ? "" :
                          <li className="mb-2">
                            <FontAwesomeIcon icon="check-circle" fixedWidth
                              className={
                                this.state.selectedCategory.length > 0 ? "mr-2 text-primary" : "mr-2 text-danger"
                              }
                            />
                          Categories
                            <ul className="pl-4 list-unstyled mt-1 fs-12 d-flex flex-wrap mx-n1">
                              {this.previewCategory()}
                            </ul>
                          </li>}
                        <li className="mb-2">
                          <FontAwesomeIcon icon="check-circle" fixedWidth
                            className={
                              this.state.preferredMethod === 'freshData' ?
                                this.state.webSet && this.state.webSet[0] && this.state.webSet[0].freshWebsiteURL !== '' ? "mr-2 text-primary" : "mr-2 text-danger"
                                : this.state.selectedWebsite.length > 0 ? "mr-2 text-primary" : "mr-2 text-danger"
                            }
                          />
                          Website
                          <ul className="pl-4 list-unstyled mt-1 fs-12">
                            {this.previewWebsite()}
                          </ul>
                        </li>
                        <li hidden className="mb-2">
                          <FontAwesomeIcon icon="check-circle" fixedWidth className="mr-2 text-primary" />
                          Social Media
                          <ul className="pl-4 list-unstyled mt-1 fs-12 d-flex flex-wrap mx-n1">
                            <li className="mb-1 px-1">
                              Facebook
                              <span>&nbsp;|</span>
                            </li>
                            <li className="mb-1 px-1">
                              Twitter
                              <span>&nbsp;|</span>
                            </li>
                            <li className="mb-1 px-1">
                              Instagram
                              <span>&nbsp;|</span>
                            </li>
                            <li className="mb-1 px-1">
                              Pinterest
                              <span>&nbsp;|</span>
                            </li>
                            <li className="mb-1 px-1">
                              LinkedIn
                            </li>
                          </ul>
                        </li>
                        <li className="mb-2">
                          <FontAwesomeIcon icon="check-circle" fixedWidth
                            className={
                              this.state.preferredMethod === 'freshData' ?
                                this.state.HoursOfOperation && this.state.HoursOfOperation[0] && this.state.HoursOfOperation[0].freshHOPDay !== '' ? "mr-2 text-primary" : "mr-2 text-danger"
                                : this.state.selectedHour.length > 0 ? "mr-2 text-primary" : "mr-2 text-danger"
                            }
                          />
                          Hours of Operation
                          <ul className="pl-4 list-unstyled mt-1 fs-12">
                            {this.previewHours()}
                          </ul>
                        </li>
                        <li className="mb-2">
                          <FontAwesomeIcon icon="check-circle" fixedWidth
                            className={
                              this.state.preferredMethod === 'freshData' ?
                                this.state.AdressDiary && this.state.AdressDiary[0] && this.state.AdressDiary[0].freshAddress1 !== '' ? "mr-2 text-primary" : "mr-2 text-danger"
                                : this.state.selectValueAddress.length > 0 ? "mr-2 text-primary" : "mr-2 text-danger"
                            }
                          />
                          Address
                          <ul className="pl-4 list-unstyled mt-1 fs-12">
                            {this.previewAddress()}
                          </ul>
                        </li>
                      </ul>
                      <hr />
                    </li>
                    <li className="mb-2">
                      <div className="d-flex mx-n2">
                        <div className="px-2">
                          <span className="font-weight-bold">About this Business</span>
                        </div>
                        {/* <div className="px-2 ml-auto">
                          <Button color="dark" title="Edit" size="sm" onClick={this.editCopiedSelectionModalToggle}><FontAwesomeIcon icon="pencil-alt" size="sm" /> </Button>
                        </div> */}
                      </div>
                      <ul className="list-unstyled mt-2 fs-14 text-secondary-dark font-weight-bold">
                        <li className="mb-2">
                          <FontAwesomeIcon icon="check-circle" fixedWidth
                            className={this.state.preferredMethod === 'freshData' ?
                              this.state.freshSpecialities !== '' ? "mr-2 text-primary" : "mr-2 text-danger"
                              : this.state.selectedAbout &&
                                this.state.selectedAbout[0] &&
                                this.state.selectedAbout[0].specialities !== '' ?
                                "mr-2 text-primary" : "mr-2 text-danger"}
                          />
                          Specialities
                          {this.Previewbizpecialities()}
                        </li>
                        <li className="mb-2">
                          <FontAwesomeIcon icon="check-circle" fixedWidth
                            className={this.state.preferredMethod === 'freshData' ?
                              this.state.freshHistory !== '' ? "mr-2 text-primary" : "mr-2 text-danger"
                              : this.state.selectedAbout &&
                                this.state.selectedAbout[0] &&
                                this.state.selectedAbout[0].businessOwnerhistory !== '' ?
                                "mr-2 text-primary" : "mr-2 text-danger"}
                          />
                          History
                          {this.PreviewOwnerhistory()}
                        </li>
                        <li className="mb-2">
                          <FontAwesomeIcon icon="check-circle" fixedWidth
                            className={this.state.preferredMethod === 'freshData' ?
                              this.state.freshBusinessOwner !== '' ? "mr-2 text-primary" : "mr-2 text-danger"
                              : this.state.selectedAbout &&
                                this.state.selectedAbout[0] &&
                                this.state.selectedAbout[0].meetOwner !== '' ?
                                "mr-2 text-primary" : "mr-2 text-danger"}
                          />
                          Meet the business owner
						  {this.PreviewownerMeet()}

                        </li>
                        <li className="mb-2">
                          <FontAwesomeIcon icon="check-circle" fixedWidth
                            className={this.state.preferredMethod === 'freshData' ?
                              this.state.freshFirstName !== '' ? "mr-2 text-primary" : "mr-2 text-danger"
                              : this.state.selectedBusiness.profile !== '' ? "mr-2 text-primary" : "mr-2 text-danger"}
                          />
                          Business Owner
                          {this.previewOwnerInfo()}
                        </li>
                      </ul>
                      <hr />
                    </li>
                    <li className="mb-2">
                      <div className="d-flex mx-n2">
                        <div className="px-2">
                          <span className="font-weight-bold">WikiReviews Special Offers</span>
                        </div>
                        {/* <div className="px-2 ml-auto">
                          <Button color="dark" title="Edit" size="sm" onClick={this.editCopiedSelectionModalToggle}><FontAwesomeIcon icon="pencil-alt" size="sm" /> </Button>
                        </div> */}
                      </div>
                      <ul className="list-unstyled mt-2 fs-14 text-secondary-dark font-weight-bold">
                        {this.previewSpecialOffer()}
                      </ul>
                      <hr />
                    </li>
                    <li className="mb-2">
                      <div className="d-flex mx-n2">
                        <div className="px-2">
                          <span className="font-weight-bold">Call to Action</span>
                        </div>
                        {/* <div className="px-2 ml-auto">
                          <Button color="dark" title="Edit" size="sm" onClick={this.editCopiedSelectionModalToggle}><FontAwesomeIcon icon="pencil-alt" size="sm" /> </Button>
                        </div> */}
                      </div>
                      <ul className="list-unstyled mt-2 fs-14 text-secondary-dark font-weight-bold">
                        {this.previewcalltoactionData()}
                      </ul>
                      <hr />
                    </li>
                  </ul>
                </div>
              </Col>
              <Col lg={6}>
                <div className="bg-white p-3">
                  <FormGroup className="mb-2">
                    {/* <Label for="select_listing_type">Select Country</Label> */}
                    <Input bsSize="sm" className="primary" type="select" name="select_listing_country" id="select_listing_country" value={countryName} onChange={this.handlePreviewChange}>
                      <option>Select Country</option>
                      {(typeof countryList !== 'undefined' && countryList.length > 0) ? countryList.map((dropdown, index) => {
                        return <option key={index} dropdown={dropdown} value={dropdown}>
                          {dropdown}</option>;
                      }) : (<option> </option>)
                      }
                    </Input>
                  </FormGroup>
                  <FormGroup className="mb-2">
                    {/* <Label for="select_listing_type">Select State/Province</Label> */}
                    <Input bsSize="sm" className="primary" type="select" name="select_listing_province" id="select_listing_province" value={provinceName} onChange={this.handlePreviewChange}>
                      <option>Select State/Province</option>
                      {(typeof countryState !== 'undefined' && countryState.length > 0) ? countryState.map((dropdown, index) => {
                        return <option key={index} dropdown={dropdown} value={dropdown}>
                          {dropdown}</option>;
                      }) : (<option> </option>)
                      }
                    </Input>
                  </FormGroup>
                  <div className="d-flex mb-2">
                    <span className="mr-2">{pendingBranches && pendingBranches.length} Branches</span>
                    <div className="ml-auto">
                      <span
                        role="button"
                        className="text-primary"
                        name="selectBranches"
                        onClick={this.handleAllChecked}
                        checked={this.state.checkAll}
                      >
                        Select All
                      </span>
                    </div>
                  </div>
                  <Input
                    bsSize="sm"
                    className="bg-white primary"
                    type="text"
                    name="filterBranches"
                    placeholder="Type to filter branches"
                    onChange={this.filterList}
                  />

                  <div style={{ minHeight: '100px', maxHeight: 'calc(100vh - 475px)', overflowY: 'auto' }}>
                    <ul className="list-unstyled mb-2 p-2 fs-14">
                      {pendingBranches &&
                        Array.isArray(pendingBranches) &&
                        pendingBranches.length > 0 && pendingBranches.filter(item => item.id != branchId) ? (
                          pendingBranches.map((item, index) => (
                            <li className="d-flex" key={index}>
                              <CustomInput
                                className="mr-2"
                                type="checkbox"
                                id={"branch_" + index}
                                checked={this.state.rowState[index]}
                                data-id={index}
                                value={item.id}
                                onChange={this.handleChangedBranches}
                                name="select_multiple_companies"
                              />
                              <Label
                                for={"branch_" + index}
                                className="flex-fill"
                              >
                                {item.name +
                                  " ( " +
                                  item.address1 +
                                  ", " +
                                  item.city +
                                  ", " +
                                  item.state +
                                  ", " +
                                  item.country +
                                  ")"}
                              </Label>
                            </li>
                          ))
                        ) : (
                          <div className="bg-white p-3">
                            <h2 className="text-secondary-dark">
                              No Company to Display
                          </h2>
                          </div>
                        )}
                    </ul>
                  </div>

                </div>
              </Col>
            </Row>
            <div className=" mt-2">
              <p className="fs-14 text-dark flashing">
                <FontAwesomeIcon icon="info-circle" /> Once this is done it cannot by undone. Please be sure to fully review.
              </p>
              <div className="text-right">
                <Button
                  color="light"
                  size="lg"
                  onClick={() => this.setState({ copyInfoToBranchModalStep2: false })}
                >
                  Cancel
                </Button>

                <Button
                  color="primary"
                  size="lg"
                  disabled={this.state.isdisabledsave}
                  onClick={this.handleSubmit}
                >
                  Update Listing Data Now
                </Button>
              </div>
            </div>
          </ModalBody>
        </Modal>

        <Modal isOpen={this.state.cancelCopyOpModal} toggle={() => this.setState({ cancelCopyOpModal: !this.state.cancelCopyOpModal })}>
          <ModalHeader toggle={() => this.setState({ cancelCopyOpModal: !this.state.cancelCopyOpModal })}></ModalHeader>
          <ModalBody className="text-center">
            <h2 className="mb-3">Confirmation</h2>
            <p className="fs-14">
              Are you sure you want to Cancel this ?
              <br />
              All Template changes will be lost
            </p>

            <div className="pt-4">
              <div>
                <Button onClick={() => this.setState({ cancelCopyOpModal: false })} size="md" color="primary">
                  No
                </Button>
                <Button onClick={this.cancelCopyOpConfirmed} size="md" color="primary">
                  Yes
                </Button>
              </div>
            </div>
          </ModalBody>
        </Modal>

        {/* Select Copy Branch Method Modal */}
        <Modal
          size="lg"
          isOpen={this.state.selectCopyBranchMethodModal}
        // toggle={() =>
        //   this.setState({
        //     selectCopyBranchMethodModal: !this.state.selectCopyBranchMethodModal,
        //     preferredMethod: 'freshData'
        //   })
        // }
        >
          <ModalHeader
            toggle={() => this.setState({
              selectCopyBranchMethodModal: !this.state.selectCopyBranchMethodModal,
              preferredMethod: 'freshData'
            })}
          >Copy Branch Process</ModalHeader>
          <ModalBody>
            <FormGroup tag="fieldset">
              <legend className="ff-alt fs-16 text-dark mb-3">Select the preferred copy method</legend>
              <Row form>
                <Col>
                  <FormGroup check>
                    <Label className="text-primary" check>
                      <Input
                        type="radio"
                        name="copyMethod"
                        id="freshData"
                        checked={this.state.preferredMethod === 'freshData'}
                        onClick={() => {
                          this.setState({ preferredMethod: 'freshData' })
                        }}
                      />{' '}
                      Enter Fresh Data
                    </Label>
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup check>
                    <Label className="text-primary" check>
                      <Input
                        type="radio"
                        name="copyMethod"
                        id="copyBranch"
                        checked={this.state.preferredMethod === 'copyBranch'}
                        onClick={() => {
                          this.setState({ preferredMethod: 'copyBranch' })
                        }}
                      />{' '}
                      Select a Specific Branch
                    </Label>
                  </FormGroup>
                </Col>
              </Row>
            </FormGroup>
            <div className="fs-12 text-muted mt-4">
              <p>By Selecting a 'Enter Fresh data' option, you will be asked to enter the Branch info in the next step.
                <br /> By Selecting a 'Select a Specific Branch' option, you will be asked to choose the Branch in the next step.</p>
            </div>
          </ModalBody>
          <ModalFooter className="bg-white">
            <div className="text-right mt-2">
              <Button
                color="transparent"
                className="mw"
                size="lg"
                onClick={() => this.setState({
                  selectCopyBranchMethodModal: false,
                  preferredMethod: 'freshData'
                })
                }
              >
                Cancel
              </Button>
              {/* <Button color="primary" className="mw" size="lg" onClick={() => this.setState({ enterNewBranchDataModal: true })}>Next</Button> */}
              <Button
                color="primary"
                className="mw"
                size="lg"
                onClick={
                  this.state.preferredMethod === 'copyBranch' ?
                    () => {
                      this.props.get_biz_callon_type();
                      this.selectBranchModalToggle()
                    } : () => {
                      this.props.get_business_category_data('Business');
                      this.props.get_biz_callon_type();
                      this.setState({ enterNewBranchDataModal: true })
                    }}>Next</Button>
            </div>
          </ModalFooter>
        </Modal>

        {/* Enter Fresh Data Modal */}
        <Formik
          touched
          errors
          initialValues={freshData}
          // validationSchema={validate}
          // onSubmit={(values) => {
          //   this.props.onSubmit({
          //     ...values,
          //     taxonomy: [values.taxonomy],
          //   });
          // }}
          render={(props) => {
            const {
              values,
              errors,
              touched,
              handleSubmit,
              handleChange,
              handleBlur,
            } = props;
            return (
              <Modal
                scrollable
                size="lg"
                isOpen={this.state.enterNewBranchDataModal}
              // toggle={() => this.setState({ enterNewBranchDataModal: !this.state.enterNewBranchDataModal })}
              >
                <ModalHeader toggle={() =>
                  this.setState({
                    enterNewBranchDataModal: !this.state.enterNewBranchDataModal
                  }, () => {
                    this.toggleCopyBranchToDefaultValues();
                  })
                }
                >Enter New Branch Data</ModalHeader>
                <ModalBody>
                  <Row form>
                    <Col lg={6}>
                      <FormGroup>
                        <div className="ff-base fs-20 font-weight-bold text-dark mb-2">
                          Logo
                  </div>
                        <img width="120" height="120" className="img-thumbnail" id="logoUrl"
                          src={freshprofileBranchLogoUrl && freshprofileBranchLogoUrl != "" ? freshprofileBranchLogoUrl : require("../../assets/images/icons/placeholder-img-alt.jpg")}
                          alt="Logo" />
                        <div className="text-left mt-2">
                          <Label className="text-dark hover-blue fs-14" role="button" for="updateLogoImage">
                            Add/Update
                    </Label>
                          <input
                            type="file"
                            id="updateLogoImage"
                            accept="image/x-png,image/gif,image/jpeg"
                            onChange={(event) => this.handleOnFileUploadChange(event, 'freshProfilePic', 'branchLogo')}
                            hidden />
                        </div>
                      </FormGroup>
                    </Col>
                    <Col lg={6}>
                      <FormGroup>
                        <div className="ff-base fs-20 font-weight-bold text-dark mb-2">
                          Homepage Image/Video
                  </div>
                        <img width="120" className="img-thumbnail" id="videoUrl"
                          src={freshProfileBranchBannerUrl && freshProfileBranchBannerUrl != "" ? freshProfileBranchBannerUrl : require("../../assets/images/icons/placeholder-img-alt.jpg")}
                          alt="Image" />
                        <div className="text-left mt-2">
                          <Label className="text-dark hover-blue fs-14" role="button" for="updateHomeImageVideo">
                            Add/Update
                    </Label>
                          <input type="file" id="updateHomeImageVideo" accept="image/x-png,image/gif,image/jpeg,video/*"
                            onChange={(event) => this.handleOnFileUploadChange(event, 'freshProfilePic', 'branchBanner')}

                            hidden />
                        </div>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row form>
                    <Col lg={6}>
                      <CollapseBasic title="Branch Representative" containerClass="border" isOpen={true}>
                        <div>
                          {/* Profile Image */}
                          <div className="text-center">
                            <div className="profile-pic-holder">
                              <img
                                className="profile-pic"
                                src={freshprofilePicUrl && freshprofilePicUrl != "" ? freshprofilePicUrl : require("../../assets/images/icons/user-circle.png")}
                                alt="Profile Image"

                              />
                              <Label for="newfreshProfilePhoto" className="upload-file-block">
                                <div className="text-center">
                                  <div className="mb-2">
                                    <FontAwesomeIcon icon="camera" size="2x" />
                                  </div>
                                  <div className="text-uppercase">
                                    Add <br /> Profile Photo
                            </div>
                                </div>
                              </Label>
                              <Input
                                type="file"
                                name="profile_pic"
                                id="newfreshProfilePhoto"
                                accept="image/*"
                                style={{ display: "none" }}
                                onChange={(event) => this.handleOnFileUploadChange(event, 'freshProfilePic')}
                              />
                            </div>



                            {/* Update Name field, show when updating */}

                            <div className={false ? "d-none" : "mt-2"}>
                              <AvForm
                                className="text-left"
                              // ref={this.refNameForm} onValidSubmit={this.saveName}
                              >
                                <AvGroup>
                                  <AvInput
                                    className="mr-2"
                                    bsSize="sm"
                                    type="text"
                                    name="freshFirstName"
                                    placeholder="First Name"
                                    value={freshData.freshFirstName}
                                    onChange={(e) => {
                                      this.handleFreshDataChange(e);
                                    }}
                                    // errorMessage={this.state.errorFirstName}
                                    validate={{
                                      pattern: { value: '^[a-zA-Z ]*$' },
                                      maxLength: { value: 20 },
                                      required: true
                                    }}
                                  />
                                  <div className="invalid-feedback d-block">
                                    {this.state.errorFirstName}
                                  </div>
                                </AvGroup>
                                <AvGroup>
                                  <AvInput
                                    bsSize="sm"
                                    type="text"
                                    name='freshLastName'
                                    placeholder="Last Name"
                                    value={freshLastName}
                                    onChange={(e) => {
                                      this.handleFreshDataChange(e);
                                    }}
                                    validate={{
                                      pattern: { value: '^[a-zA-Z ]*$' },
                                      maxLength: { value: 20 },
                                      required: true
                                    }}
                                  />
                                  <div className="invalid-feedback d-block">{this.state.errorLastName}</div>
                                </AvGroup>
                              </AvForm>
                            </div>
                          </div>

                          {/* User Info */}
                          <div>
                            <FormGroup>
                              <Label size="sm" className="p-0 mb-1">
                                Position
                        </Label>
                              {/* If not provide ever */}
                              <div hidden>
                                <span className="is-empty">Please provide your position</span>
                              </div>

                              {/* When clicked on Update show below */}
                              <div className={false ? "d-none" : ""}>
                                <AvForm
                                // ref={this.refPositionForm} onValidSubmit={this.savePosition}
                                >
                                  <AvGroup>
                                    <AvInput
                                      bsSize="sm"
                                      type="text"
                                      name="freshPosition"
                                      placeholder="Enter Position..."
                                      value={freshPosition}
                                      onChange={(e) => {
                                        this.handleFreshDataChange(e);
                                      }}
                                      validate={{
                                        pattern: { value: '^[a-zA-Z ]*$' }
                                      }}
                                    />
                                    <div className="invalid-feedback d-block">{this.state.errorPosition}</div>
                                  </AvGroup>
                                </AvForm>
                              </div>
                            </FormGroup>

                            <FormGroup hidden>
                              <Label size="sm" className="p-0 mb-1">
                                Email
							</Label>

                              <div>
                                <Input
                                  bsSize="sm"
                                  type="text"
                                  name="freshPersonEmail"
                                  placeholder="Enter Email..."
                                  value={freshPersonEmail}
                                  onChange={(e) => {
                                    this.handleFreshDataChange(e);
                                  }}
                                />
                                <span style={{ color: 'red' }}>{this.state.errorEmail}</span>
                              </div>

                            </FormGroup>

                            <FormGroup hidden>
                              <Label size="sm" className="p-0 mb-1">
                                Password
                        </Label>
                              {
                                false ?
                                  <div>
                                    <div className="d-flex">
                                      <span className="mr-2">********</span>{" "}
                                      <span className="actionable ml-auto"
                                      // onClick={this.EditPassword}
                                      >Update</span>
                                    </div>
                                  </div>
                                  :
                                  ''
                              }
                              <div className={false ? "d-none" : ""}>
                                <AvForm
                                // ref={this.refPassForm} onValidSubmit={this.savePassword}
                                >
                                  <FormGroup className="mb-2">
                                    <AvField
                                      bsSize="sm"
                                      type="password"
                                      name="password1"
                                      placeholder="New Password"
                                    // value={this.state.password1}
                                    // onChange={(e) => {
                                    //   this.handlePassword(e);
                                    // }}

                                    />
                                  </FormGroup>
                                  <FormGroup className="mb-2">
                                    <AvField
                                      bsSize="sm"
                                      type="password"
                                      name="password2"
                                      placeholder="Re-Enter Password"
                                    // value={this.state.password2}
                                    // onChange={(e) => {
                                    //   this.handlePassword(e);
                                    // }}
                                    // disabled={this.state.disablePass}
                                    />
                                  </FormGroup>
                                </AvForm>
                              </div>
                            </FormGroup>
                          </div>
                        </div>
                      </CollapseBasic>

                      <CollapseBasic title="About this Business" containerClass="border" isOpen={true}>
                        <FormGroup>
                          <Label size="sm" className="p-0 mb-1">Specialities</Label>

                          {/* Data, hide when editing */}
                          <div>
                            <div className="d-flex mb-2">
                              <div className="mr-2 small">
                                {/* Show if data */}
                                <span hidden>
                                  {"Theses are the extra facilities provided here"}
                                </span>
                                {/* Show if no data */}
                                <span>
                                  <i>Please enter Specialities about your business</i>
                                </span>
                              </div>
                              <div className={false ? "d-none" : "ml-auto"}>
                                <span className="actionable text-dark"
                                // onClick={this.EditSpec}
                                ><FontAwesomeIcon icon="pencil-alt" size="sm" /> </span>
                              </div>
                            </div>
                          </div>

                          {/* Show when editing */}
                          <div className={false ? "d-none" : ""}>
                            <div className="mb-2">
                              <Input
                                onChange={(e) => { this.handleFreshDataChange(e) }}
                                bsSize="sm"
                                className="mb-2"
                                type="textarea"
                                name="freshSpecialities"
                                placeholder="Enter specialities"
                                value={freshSpecialities}
                              />
                            </div>
                          </div>
                          <hr />
                        </FormGroup>

                        <FormGroup>
                          <Label size="sm" className="p-0 mb-1">History</Label>

                          {/* Data, hide when editing */}
                          <div>
                            <div className="d-flex mb-2">
                              <div className="mr-2 small">
                                {/* Show if data */}
                                <span hidden>
                                  {"We started back in 1900 with a corner drug store"}
                                </span>
                                {/* Show if no data */}
                                <span>
                                  <i>Please enter history of your business</i>
                                </span>
                              </div>
                              <div className={false ? "d-none" : "ml-auto"}>
                                <span className="actionable text-dark" onClick={this.EditHistory}><FontAwesomeIcon icon="pencil-alt" size="sm" /> </span>
                              </div>
                            </div>
                          </div>

                          {/* Show when editing */}
                          <div className={false ? "d-none" : ""}>
                            <div className="mb-2">
                              <Input
                                onChange={(e) => { this.handleFreshDataChange(e) }}
                                bsSize="sm"
                                className="mb-2"
                                type="textarea"
                                name="freshHistory"
                                placeholder="Enter history"
                                value={freshHistory}
                              />
                            </div>
                          </div>
                          <hr />
                        </FormGroup>

                        <FormGroup>
                          <Label size="sm" className="p-0 mb-1">Meet the Business Owner</Label>

                          {/* Data, hide when editing */}
                          <div>
                            <div className="d-flex mb-2">
                              <div className="mr-2 small">
                                {/* Show if data */}
                                <span hidden>
                                  {"About Business Owner goes here"}
                                </span>
                                {/* Show if no data */}
                                <span>
                                  <i>Please enter about business Owner</i>
                                </span>
                              </div>
                              <div className={false ? "d-none" : "ml-auto"}>
                                <span className="actionable text-dark"><FontAwesomeIcon icon="pencil-alt" size="sm" onClick={this.EditBussOwn} /> </span>
                              </div>
                            </div>
                          </div>

                          {/* Show when editing */}
                          <div className={false ? "d-none" : ""}>
                            <div className="mb-2">
                              <Input
                                onChange={(e) => { this.handleFreshDataChange(e) }}
                                bsSize="sm"
                                className="mb-2"
                                type="textarea"
                                name="freshBusinessOwner"
                                placeholder="Enter Business..."
                                value={freshBusinessOwner}
                              />
                            </div>
                          </div>
                          <hr />
                        </FormGroup>
                      </CollapseBasic>
                      <BranchAdditionalInfo handleSelectedAdditionalInfo={this.handleSelectedAdditionalInfo} />
                      <CollapseBasic title="WR Special Offers" containerClass="border" isOpen={true}>
                        <FormGroup className="mb-2">
                          <Label size="sm" className="p-0 mb-1">Offer</Label>
                          {/* Show if data */}


                          {/* Show when adding/ editing */}
                          <FieldArray
                            name="freshWROffer"
                            render={(arrayHelpers) => (
                              <div>
                                {values.freshWROffer.map(
                                  (WROffer, index) => (

                                    <div key={index}>
                                      <h4 className="mb-2">Add Offer</h4>
                                      <AvForm>
                                        <div className="mb-2">
                                          <AvField
                                            bsSize="sm"
                                            className="mb-2"
                                            type="textarea"
                                            name="freshOfferText"
                                            placeholder="Add Offer"
                                            onChange={(e) => this.handleChangeCast(e, index, 'WRoffer')}
                                            // value={isEditOffer ? isEditOffer.special_offer : addOffer.special_offer}
                                            errorMessage="This Field is required"
                                          // validate={{ required: { value: true } }} 
                                          />
                                          <AvField
                                            type="select"
                                            bsSize="sm"
                                            name="freshOfferType"
                                            className="mb-2"
                                            // value={isEditOffer ? isEditOffer.offer_type : addOffer.offer_type} 
                                            onChange={(e) => this.handleChangeCast(e, index, 'WRoffer')}
                                            errorMessage="This Field is required"
                                          // validate={{ required: { value: true } }}
                                          >
                                            <option value="">Choose offer type</option>
                                            <option value="coupons">Coupons</option>
                                            <option value="special_discount">Special Discount</option>
                                          </AvField>
                                          <div className="mb-2">
                                            <Label className="d-block mb-0" size="sm">Start Date</Label>
                                            <Input
                                              bsSize="sm" type="date"
                                              name="freshOfferStartDATE"
                                              // value={isEditOffer ? isEditOffer.from_date : addOffer.from_date}
                                              onChange={(e) => this.handleChangeCast(e, index, 'WRoffer')}
                                              // errorMessage="This Field is required"
                                              max={moment(this.state.freshWROffer[index]?.freshOfferEndDate ?
                                                this.state.freshWROffer[index]?.freshOfferEndDate : "").format("YYYY-MM-DD")}
                                              min={moment().format("YYYY-MM-DD")}

                                            // validate={{ required: { value: true } }}
                                            ></Input>
                                          </div>
                                          <div className="mb-2">
                                            <Label className="d-block mb-0" size="sm">End Date</Label>
                                            <AvField bsSize="sm" type="date"
                                              name="freshOfferEndDate"
                                              // value={isEditOffer ? isEditOffer.to_date : addOffer.to_date} 
                                              onChange={(e) => this.handleChangeCast(e, index, 'WRoffer')}
                                              errorMessage="This Field is required"
                                              // validate={{ required: { value: true } }}
                                              min={moment().format("YYYY-MM-DD")}
                                            ></AvField>
                                          </div>
                                          <AvField
                                            bsSize="sm"
                                            className="mb-2"
                                            name="freshOfferWebsiteLink"
                                            type="text"
                                            placeholder="Website to claim deal"
                                            // value={
                                            //   claim_deal_link_ur ?
                                            //     "None" : addOffer.claim_deal_link
                                            // }
                                            onChange={(e) => this.handleChangeCast(e, index, 'WRoffer')}
                                            errorMessage="This Field is required"
                                          // validate={{
                                          //   required: { value: true }
                                          // }}
                                          >
                                          </AvField>
                                          {index === 0 ? "" :
                                            <div className="d-flex">
                                              <span
                                                className="actionable ml-auto"
                                                onClick={() => {
                                                  arrayHelpers.remove(index);
                                                  this.handleremoveCast('freshWROffer')
                                                }
                                                }
                                              ><FontAwesomeIcon icon="minus" /> Remove</span>
                                            </div>}
                                          <hr />
                                        </div>
                                      </AvForm>
                                    </div>
                                  )
                                )}

                                <div className="d-flex">
                                  <span className="actionable ml-auto"
                                    onClick={() =>
                                      arrayHelpers.push({
                                        freshOfferText: '',
                                        freshOfferType: '',
                                        freshOfferStartDATE: '',
                                        freshOfferEndDate: "",
                                        freshOfferWebsiteLink: "",
                                      })
                                    }
                                  ><FontAwesomeIcon icon="plus" /> Add More</span>
                                </div>
                              </div>

                            )}
                          />
                          {/* Hide when User adding */}

                        </FormGroup>
                      </CollapseBasic>
                    </Col>
                    <Col lg={6}>
                      <CollapseBasic title="Business Info" containerClass="border" isOpen={true}>
                        <FormGroup className="mb-2">
                          <Label size="sm" className="p-0 mb-1">Address</Label>

                          {/* Add Address Field */}
                          <FieldArray
                            name="freshAddress"
                            render={(arrayHelpers) => (
                              <div>
                                {values.freshAddress.map(
                                  (freshAddr, index) => (
                                    <div key={index}>
                                      <h4 className="mb-2">Add Address</h4>
                                      <AvForm>
                                        <div className="mb-2">
                                          <AvField
                                            bsSize="sm"
                                            className="mb-2"
                                            name="freshAddress1"
                                            onChange={(e) => this.handleChangeCast(e, index, 'AddAddress')}
                                            type="text"
                                            placeholder="Address 1"
                                            errorMessage="This Field is required"
                                          // validate={{ required: { value: true } }} 
                                          />
                                          <AvField bsSize="sm" className="mb-2"
                                            type="text"
                                            name="freshAddress2"
                                            onChange={(e) => this.handleChangeCast(e, index, 'AddAddress')}
                                            placeholder="Address 2"
                                            errorMessage="This Field is required"
                                          // validate={{ required: { value: true } }} 
                                          />
                                          <AvField bsSize="sm" className="mb-2"
                                            // value={isEditAddress ? isEditAddress.city : addAddress.city} 
                                            type="text"
                                            name="freshAddressCity"
                                            onChange={(e) => this.handleChangeCast(e, index, 'AddAddress')}
                                            placeholder="City"
                                            errorMessage="This Field is required"
                                          // validate={{ required: { value: true } }}
                                          />
                                          <AvField bsSize="sm" className="mb-2"
                                            // value={isEditAddress ? isEditAddress.state : addAddress.state} 
                                            type="text"
                                            name="freshAddressState"
                                            onChange={(e) => this.handleChangeCast(e, index, 'AddAddress')}
                                            placeholder="State"
                                            errorMessage="This Field is required"
                                          // validate={{ required: { value: true } }} 
                                          />
                                          <Input bsSize="sm" className="mb-2"
                                            // value={isEditAddress ? isEditAddress.country : addAddress.country} onChange={this.handleChangeAddress} 
                                            type="select"
                                            name="freshAddressCountry"
                                            onChange={(e) => this.handleChangeCast(e, index, 'AddAddress')}
                                            errormessage="This Field is required"
                                          // validate={{ required: { value: true } }}
                                          >
                                            <option value="">SELECT</option>
                                            <option value="United States">USA</option>
                                            <option value="CANADA">CANADA</option>
                                          </Input>
                                          <AvField bsSize="sm" className="mb-2"
                                            // value={isEditAddress ? isEditAddress.zipcode : addAddress.zipcode} onChange={this.handleChangeAddress} 
                                            type="text"
                                            name="freshAddressZipcode"
                                            onChange={(e) => this.handleChangeCast(e, index, 'AddAddress')}
                                            placeholder="Zip Code /  Postal Code"
                                            errorMessage="This Field is required"
                                          // validate={{ required: { value: true } }}
                                          />
                                          {
                                            index === 0 ? "" :
                                              <div className="d-flex" hidden>
                                                <span
                                                  className="actionable ml-auto"
                                                  onClick={() => {
                                                    arrayHelpers.remove(index);
                                                    this.handleremoveCast('freshAddress')
                                                  }
                                                  }
                                                ><FontAwesomeIcon icon="minus" /> Remove</span>
                                              </div>
                                          }
                                          <hr />
                                        </div>
                                      </AvForm>
                                    </div>
                                  )
                                )}
                                <div className="d-flex" hidden>
                                  <span hidden className="actionable ml-auto"
                                    onClick={() =>
                                      arrayHelpers.push({
                                        freshAddress1: '',
                                        freshAddress2: '',
                                        freshAddressCity: '',
                                        freshAddressState: '',
                                        freshAddressCountry: "",
                                        freshAddressZipcode: "",
                                      })
                                    }
                                  ><FontAwesomeIcon icon="plus" /> Add More</span>
                                </div>
                              </div>
                            )}
                          />
                          {/* Hide when User adding */}

                        </FormGroup>

                        <FormGroup className="mb-2">
                          <Label size="sm" className="p-0 mb-1">Phone</Label>

                          <FieldArray
                            name="freshPhone"
                            render={(arrayHelpers) => (
                              <div>
                                {values.freshPhone.map(
                                  (addphon, index) => (
                                    <div key={index}>
                                      <AvForm>
                                        <div className="mb-2">
                                          {/* <Input
                                            className="mr-3 mb-2"
                                            name="phoneFresh"
                                            type="text"
                                            placeholder="(xxx) xxx-xxxx"
                                            bsSize="sm"
                                            maxLength='14'
                                            onChange={(e) => this.handleChangeCast(e, index, 'AddPhone')}
                                            // value={freshPhone[index].phoneFresh}
                                            // errorMessage={false}
                                            value={this.formatPhoneNumber(freshPhone[index].phoneFresh)}
                                          // validate={{
                                          // pattern: { value: '^[0-9]+$' },
                                          //   minLength: { value: 10 },
                                          //   maxLength: { value: 10 },
                                          // }}
                                          /> */}
                                          <FormGroup>
                                            <PhoneInput
                                              country="US"
                                              className="form-control form-control-sm mb-2 "
                                              name="phoneFresh"
                                              bsSize="sm"
                                              // type="text"
                                              placeholder="(xxx) xxx-xxxx"
                                              onChange={(setValue) => this.handlePhoneChangeCast(setValue, index, 'AddPhone')}
                                              value={freshPhone && freshPhone[index] && freshPhone[index].phoneFresh}
                                              maxLength="14"
                                            />
                                          </FormGroup>
                                          <AvField
                                            onChange={(e) => this.handleChangeCast(e, index, 'AddPhone')}
                                            className="mb-2"
                                            bsSize="sm"
                                            type="select"
                                            name="freshPhoneType"
                                          >
                                            {true && <option>Choose Phone Type</option>}
                                            <option value="Tel">Telephone</option>
                                            <option value="Mob">Mobile</option>
                                            <option value="Support">Support</option>
                                          </AvField>
                                          {index === 0 ? "" :
                                            <div className="d-flex">
                                              <span
                                                className="actionable ml-auto"
                                                onClick={() => {
                                                  arrayHelpers.remove(index);
                                                  this.handleremoveCast('freshPhone', index)
                                                }
                                                }
                                              ><FontAwesomeIcon icon="minus" /> Remove</span>
                                            </div>}
                                          <hr />
                                        </div>
                                      </AvForm>
                                    </div>

                                  ))}<div className="d-flex">
                                  <span className="actionable ml-auto"
                                    onClick={() => {
                                      arrayHelpers.push({
                                        phoneFresh: '',
                                        freshPhoneType: '',
                                      })
                                      freshPhone.push({
                                        phoneFresh: '',
                                        freshPhoneType: '',
                                      })
                                    }
                                    }
                                  ><FontAwesomeIcon icon="plus" /> Add More</span>
                                </div>
                              </div>
                            )}
                          />

                        </FormGroup>

                        <FormGroup className="mb-2">
                          <Label size="sm" className="p-0 mb-1">Websites</Label>
                          <FieldArray
                            name="freshWebsite"
                            render={(arrayHelpers) => (
                              <div>
                                {values.freshWebsite.map(
                                  (addweb, index) => (
                                    <div key={index}>
                                      <AvForm
                                      // ref={this.refWebsiteForm} onValidSubmit={this.handleSubmitWebsite}
                                      >
                                        <div className="mb-2">
                                          <AvField
                                            bsSize="sm"
                                            className="mb-2"
                                            type="textarea"
                                            name="freshWebsiteURL"
                                            // value={isEditWebsite ? isEditWebsite.website : addWebsite.website}
                                            onChange={(e) => this.handleChangeCast(e, index, 'adddWebsite')}
                                            placeholder="Web URL"
                                            validate={{
                                              pattern: { value: websiteRegex },
                                            }}
                                          />
                                          <AvField
                                            // onFocus={this.handleChangeWebsite}
                                            className="mb-2"
                                            bsSize="sm"
                                            type="select"
                                            name="freshWebsiteType"
                                            onChange={(e) => this.handleChangeCast(e, index, 'adddWebsite')}
                                          // value={isEditWebsite ? isEditWebsite.website_type : addWebsite.website_type}
                                          >
                                            <option value="">Choose website type</option>
                                            <option value="Main">Main Website</option>
                                            <option value="Facebook">Facebook Website</option>
                                            <option value="Google+">Google Website</option>
                                            <option value="Twitter">Twitter Website</option>
                                            <option value="LinkedIn">LinkedIn Website</option>
                                            <option value="Instagram">Instagram Website</option>
                                          </AvField>
                                          {index === 0 ? "" :
                                            <div className="d-flex">
                                              <span
                                                className="actionable ml-auto"
                                                onClick={() => {
                                                  arrayHelpers.remove(index);
                                                  this.handleremoveCast('freshWebsite')
                                                }
                                                }
                                              ><FontAwesomeIcon icon="minus" /> Remove</span>
                                            </div>}
                                          <hr />
                                        </div>
                                      </AvForm>
                                    </div>
                                  ))}
                                <div className="d-flex">
                                  <span className="actionable ml-auto"
                                    onClick={() =>
                                      arrayHelpers.push({
                                        freshWebsite: '',
                                        freshWebsiteType: '',
                                      })
                                    }
                                  ><FontAwesomeIcon icon="plus" /> Add More</span>
                                </div>
                              </div>

                            )}
                          />
                          {/* Hide when User adding */}

                        </FormGroup>


                        <FormGroup className="mb-2">
                          <Label size="sm" className="p-0 mb-1">Email</Label>
                          <FieldArray
                            name="freshEmail"
                            render={(arrayHelpers) => (
                              <div>
                                {values.freshEmail.map(
                                  (addmail, index) => (
                                    <div key={index}>
                                      <div className="mb-2">
                                        <AvForm>
                                          <AvField
                                            // onFocus={this.handleChangeBusinessEmail}
                                            onChange={(e) => this.handleChangeCast(e, index, 'addEmail')}
                                            // value={isEditBusinessEmail ? isEditBusinessEmail.email : addBusinessEmail.email}
                                            bsSize="sm"
                                            className="mb-2"
                                            type="email"
                                            name="freshemailText"
                                            placeholder="Email..."
                                            validate={{
                                              pattern: { value: emailRegex },
                                            }}
                                          />
                                          <AvField
                                            // onFocus={this.handleChangeBusinessEmail}
                                            onChange={(e) => this.handleChangeCast(e, index, 'addEmail')}
                                            // value={isEditBusinessEmail ? isEditBusinessEmail.email_type : addBusinessEmail.email_type}
                                            className="mb-2"
                                            bsSize="sm"
                                            type="select"
                                            name="freshEmailType">
                                            <option>Choose Email Type</option>
                                            <option value="Sales">Sales mail</option>
                                            <option value="Reserve">Reserve mail</option>
                                            <option value="General">General mail</option>
                                          </AvField>

                                        </AvForm>
                                        {index === 0 ? "" :
                                          <div className="d-flex">
                                            <span
                                              className="actionable ml-auto"
                                              onClick={() => {
                                                arrayHelpers.remove(index);
                                                this.handleremoveCast('freshEmail')
                                              }
                                              }
                                            ><FontAwesomeIcon icon="minus" /> Remove</span>
                                          </div>}
                                        <hr />
                                      </div>
                                    </div>
                                  )
                                )}

                                <div className="d-flex">
                                  <span className="actionable ml-auto"
                                    onClick={() =>
                                      arrayHelpers.push({
                                        freshemailText: "",
                                        freshEmailType: "",
                                      })
                                    }
                                  ><FontAwesomeIcon icon="plus" /> Add More</span>
                                </div>
                              </div>

                            )}
                          />

                        </FormGroup>

                        <FormGroup className="mb-2">
                          <Label size="sm" className="p-0 mb-1">Payment Options</Label>
                          {/* Show If no data */}
                          <div hidden>
                            <span className="small"><i>No Payment Method added.</i></span>
                          </div>

                          {/* Show if data */}
                          {freshSelectedPayment && freshSelectedPayment.length > 0 ?
                            <div className="d-flex flex-wrap" >{this.payOptions()}</div> :
                            (<div >
                              <span className="small"><i>No Payment Method added.</i></span>
                            </div>)
                          }
                          {/* Show when adding/ editing */}
                          <div className={isToggleFreshPayment ? "" : "d-none"}>
                            <div className="mb-2">
                              <Label size="sm" className="p-0 mb-1">Select Payment Method</Label>
                              <div className="mb-2">
                                <Row form>

                                  <Col xs="auto">
                                    <Switch
                                      value={freshPaymentSwitch.cash ? 1 : 0}
                                      name="cash"
                                      onChange={(e) => this.handleFreshPaymentSwitch(e, 'cash', 'Cash')}
                                    />  {` Cash `}
                                  </Col>

                                  <Col xs="auto">
                                    <Switch
                                      value={freshPaymentSwitch.check ? 1 : 0}
                                      name="check"
                                      onChange={(e) => this.handleFreshPaymentSwitch(e, 'check', 'Check')}
                                    /> {` Check `}
                                  </Col>

                                  <Col xs="auto">
                                    <Switch
                                      value={freshPaymentSwitch.discover ? 1 : 0}
                                      name="discover"
                                      onChange={(e) => this.handleFreshPaymentSwitch(e, 'discover', 'Discover')}
                                    /> {` Discover `}
                                  </Col>
                                  <Col xs="auto">
                                    <Switch
                                      value={freshPaymentSwitch.visa ? 1 : 0}
                                      name="visa"
                                      onChange={(e) => this.handleFreshPaymentSwitch(e, 'visa', 'Visa')}
                                    /> {` Visa `}
                                  </Col>

                                  <Col xs="auto">
                                    <Switch
                                      value={freshPaymentSwitch.mastercard ? 1 : 0}
                                      name="mastercard"
                                      onChange={(e) => this.handleFreshPaymentSwitch(e, 'mastercard', 'Mastercard')}
                                    /> {` Mastercard `}
                                  </Col>

                                  <Col xs="auto">

                                    <Switch
                                      value={freshPaymentSwitch.apple ? 1 : 0}
                                      name="apple"
                                      onChange={(e) => this.handleFreshPaymentSwitch(e, 'apple', 'Apple Pay')}
                                    /> {` Apple Pay `}
                                  </Col>

                                  <Col xs="auto">

                                    <Switch
                                      value={freshPaymentSwitch.debit ? 1 : 0}
                                      name="Debit"
                                      onChange={(e) => this.handleFreshPaymentSwitch(e, 'debit', 'Debit Card')}
                                    /> {` Debit Card `}
                                  </Col>
                                  <Col xs="auto">
                                    <Switch
                                      value={freshPaymentSwitch.american ? 1 : 0}
                                      name="american"
                                      onChange={(e) => this.handleFreshPaymentSwitch(e, 'american', 'American Express')}
                                    /> {` American Express `}
                                  </Col>
                                  <Col xs="auto">
                                    <Switch
                                      value={freshPaymentSwitch.google ? 1 : 0}
                                      name="google"
                                      onChange={(e) => this.handleFreshPaymentSwitch(e, 'google', 'Google Wallet')}
                                    /> {` Google Wallet `}
                                  </Col>
                                  <Col xs="auto">
                                    <Switch
                                      value={freshPaymentSwitch.cryptocurrency ? 1 : 0}
                                      name="cryptocurrency"
                                      onChange={(e) => this.handleFreshPaymentSwitch(e, 'cryptocurrency', 'Cryptocurrency')}
                                    /> {` Cryptocurrency `}
                                  </Col>
                                </Row>

                              </div>
                              <div className="text-right">
                                <Button hidden onClick={this.handleSubmitPayments} size="sm" className="btn-icon-split mr-2" title="Submit">
                                  <span className="text">{'Submit'}</span>
                                  <span className="icon">
                                    <FontAwesomeIcon icon="check" />
                                  </span>
                                </Button>
                                <Button hidden onClick={() => { this.setState({ isTogglePayment: false }, () => this.setPaymentReset()) }} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0">
                                  <span className="text">{'Cancel'}</span>
                                  <span className="icon">
                                    <FontAwesomeIcon icon="times" />
                                  </span>
                                </Button>
                              </div>
                              <hr />
                            </div>
                          </div>
                          {/* Hide when adding/ editing */}
                          <div>
                            <div className="d-flex">
                              <span className="actionable ml-auto" onClick={this.handleToggleFreshPayment}><FontAwesomeIcon icon="plus" /> Add Payment Method</span>
                            </div>
                          </div>
                        </FormGroup>

                        <FormGroup className="mb-2">
                          <Label size="sm" className="p-0 mb-1">Categories</Label>

                          {/* Show if data */}
                          <div className="d-flex flex-wrap">
                            {
                              this.state.seletedCategoriesNames && this.state.seletedCategoriesNames.length > 0 ?

                                this.state.seletedCategoriesNames.map((item, index) => {

                                  return (
                                    <div className=" mx-1 mb-1">
                                      <span className=" mb-2 mw-100" key={index}>
                                        <Badge color="secondary" className="d-flex align-items-center">
                                          <span className="mr-1 font-weight-normal text-truncate"
                                          >{item.label}</span>
                                          <button
                                            onClick={() => this.toggleDeleteCategory(item.id)}
                                            type="button" className="close btn-sm" aria-label="Close">
                                            <span><FontAwesomeIcon icon="times-circle" /> </span>
                                          </button>
                                        </Badge>
                                      </span>
                                    </div>)
                                })
                                :
                                (<div >
                                  <span className="small"><i>No Categories added.</i></span>
                                </div>)
                            }
                          </div>

                          {/* Trigger Modal Pop up on below action */}
                          <div>
                            <div className="d-flex">
                              <span className="actionable ml-auto"
                                onClick={this.modalToggle}
                              ><FontAwesomeIcon icon="plus" /> Add Categories</span>
                            </div>
                          </div>
                        </FormGroup>


                        <FormGroup className="mb-2">
                          <Label size="sm" className="p-0 mb-1">Hours of Operation</Label>

                          <FieldArray
                            name="freshHOP"
                            render={(arrayHelpers) => (
                              <div>
                                {/* <div className={isToggleHours ? "" : "d-none"}> */}
                                {values.freshHOP.map(
                                  (WROffer, index) => (

                                    <div key={index} className="mb-2">
                                      <AvForm>
                                        <Row className="form-row">
                                          <Col
                                            // sm={addHours.start_time !== "Closed" ? '4' : '6'} 
                                            md={12} className="col-xxl-4">
                                            <div className="mb-2">
                                              <AvField
                                                onChange={(e) => this.handleChangeCast(e, index, 'addHOP')}
                                                bsSize="sm" type="select" name="freshHOPDay"
                                                //value={isEditHours ? isEditHours.day_of_week : addHours.day_of_week}
                                                //value = {index == 0 ? freshHOP[index].freshHOPDay : freshHOP[index + 1].freshHOPDay}
                                                value={index == 0 ? freshHOP[index].freshHOPDay : freshHOP[index - 1].freshHOPDay == 7 ? 1 : (parseInt(freshHOP[index - 1].freshHOPDay) + 1)}
                                              >
                                                <option>Select Day</option>
                                                <option value="1">Monday</option>
                                                <option value="2" >Tuesday</option>
                                                <option value="3" >Wednesday</option>
                                                <option value="4">Thursday</option>
                                                <option value="5">Friday</option>
                                                <option value="6">Saturday</option>
                                                <option value="7">Sunday</option>
                                              </AvField>
                                            </div>
                                          </Col>
                                          <Col
                                            // sm={addHours.start_time !== "Closed" ? '4' : '6'} 
                                            md={12} className="col-xxl-4">
                                            <div className="mb-2">
                                              <AvField
                                                onChange={(e) => this.handleChangeCast(e, index, 'addHOP')}
                                                bsSize="sm" type="select" name="freshHOPStartTime"
                                                // value={ isEditHours ? moment(isEditHours.start_time, 'HH:mm:ss').format('HH:mm') : addHours.start_time !== "Closed" ? moment(addHours.start_time, 'HH:mm:ss').format('HH:mm') : addHours.start_time}
                                                value={index == 0 ?
                                                  freshHOP[index].freshHOPStartTime :
                                                  freshHOP[index - 1].freshHOPStartTime == '23:30' ? '00:00' :
                                                    (freshHOP[index - 1].freshHOPStartTime)}
                                              >
                                                <option disabled>Select Opening Hours</option>
                                                {/* <option value="Closed">Closed</option> */}
                                                <option value="00:00">12:00 am (midnight)</option>
                                                <option value="00:30">12:30 am</option>
                                                <option value="01:00">1:00 am</option>
                                                <option value="01:30">1:30 am</option>
                                                <option value="02:00">2:00 am</option>
                                                <option value="02:30">2:30 am</option>
                                                <option value="03:00">3:00 am</option>
                                                <option value="03:30">3:30 am</option>
                                                <option value="04:00">4:00 am</option>
                                                <option value="04:30">4:30 am</option>
                                                <option value="05:00">5:00 am</option>
                                                <option value="05:30">5:30 am</option>
                                                <option value="06:00">6:00 am</option>
                                                <option value="06:30">6:30 am</option>
                                                <option value="07:00">7:00 am</option>
                                                <option value="07:30">7:30 am</option>
                                                <option value="08:00">8:00 am</option>
                                                <option value="08:30">8:30 am</option>
                                                <option value="09:00">9:00 am</option>
                                                <option value="09:30">9:30 am</option>
                                                <option value="10:00">10:00 am</option>
                                                <option value="10:30">10:30 am</option>
                                                <option value="11:00">11:00 am</option>
                                                <option value="11:30">11:30 am</option>
                                                <option value="12:00">12:00 pm</option>
                                                <option value="12:30">12:30 pm</option>
                                                <option value="13:00">1:00 pm</option>
                                                <option value="13:30">1:30 pm</option>
                                                <option value="14:00">2:00 pm</option>
                                                <option value="14:30">2:30 pm</option>
                                                <option value="15:00">3:00 pm</option>
                                                <option value="15:30">3:30 pm</option>
                                                <option value="16:00">4:00 pm</option>
                                                <option value="16:30">4:30 pm</option>
                                                <option value="17:00">5:00 pm</option>
                                                <option value="17:30">5:30 pm</option>
                                                <option value="18:00">6:00 pm</option>
                                                <option value="18:30">6:30 pm</option>
                                                <option value="19:00">7:00 pm</option>
                                                <option value="19:30">7:30 pm</option>
                                                <option value="20:00">8:00 pm</option>
                                                <option value="20:30">8:30 pm</option>
                                                <option value="21:00">9:00 pm</option>
                                                <option value="21:30">9:30 pm</option>
                                                <option value="22:00">10:00 pm</option>
                                                <option value="22:30">10:30 pm</option>
                                                <option value="23:00">11:00 pm</option>
                                                <option value="23:30">11:30 pm</option>
                                              </AvField>
                                            </div>
                                          </Col>
                                          <Col sm="4" md={12} className="col-xxl-4">
                                            <div
                                            // className={addHours.start_time !== "Closed" ? 'mb-2' : 'd-none'}
                                            >
                                              <AvField
                                                onChange={(e) => this.handleChangeCast(e, index, 'addHOP')}
                                                bsSize="sm" type="select" name="freshHOPEndTime"
                                                // value={isEditHours ? moment(isEditHours.end_time, 'HH:mm:ss').format('HH:mm') : moment(addHours.end_time, 'HH:mm:ss').format('HH:mm')}
                                                value={index == 0 ?
                                                  freshHOP[index].freshHOPEndTime :
                                                  freshHOP[index - 1].freshHOPEndTime == '23:30' ? '00:00' :
                                                    (freshHOP[index - 1].freshHOPEndTime)}
                                              >
                                                <option disabled>Select Closing Hours</option>
                                                <option value="00:30">12:30 am</option>
                                                <option value="01:00">1:00 am</option>
                                                <option value="01:30">1:30 am</option>
                                                <option value="02:00">2:00 am</option>
                                                <option value="02:30">2:30 am</option>
                                                <option value="03:00">3:00 am</option>
                                                <option value="03:30">3:30 am</option>
                                                <option value="04:00">4:00 am</option>
                                                <option value="04:30">4:30 am</option>
                                                <option value="05:00">5:00 am</option>
                                                <option value="05:30">5:30 am</option>
                                                <option value="06:00">6:00 am</option>
                                                <option value="06:30">6:30 am</option>
                                                <option value="07:00">7:00 am</option>
                                                <option value="07:30">7:30 am</option>
                                                <option value="08:00">8:00 am</option>
                                                <option value="08:30">8:30 am</option>
                                                <option value="09:00">9:00 am</option>
                                                <option value="09:30">9:30 am</option>
                                                <option value="10:00">10:00 am</option>
                                                <option value="10:30">10:30 am</option>
                                                <option value="11:00">11:00 am</option>
                                                <option value="11:30">11:30 am</option>
                                                <option value="12:00">12:00 pm (noon)</option>
                                                <option value="12:30">12:30 pm</option>
                                                <option value="13:00">1:00 pm</option>
                                                <option value="13:30">1:30 pm</option>
                                                <option value="14:00">2:00 pm</option>
                                                <option value="14:30">2:30 pm</option>
                                                <option value="15:00">3:00 pm</option>
                                                <option value="15:30">3:30 pm</option>
                                                <option value="16:00">4:00 pm</option>
                                                <option value="16:30">4:30 pm</option>
                                                <option value="17:00">5:00 pm</option>
                                                <option value="17:30">5:30 pm</option>
                                                <option value="18:00">6:00 pm</option>
                                                <option value="18:30">6:30 pm</option>
                                                <option value="19:00">7:00 pm</option>
                                                <option value="19:30">7:30 pm</option>
                                                <option value="20:00">8:00 pm</option>
                                                <option value="20:30">8:30 pm</option>
                                                <option value="21:00">9:00 pm</option>
                                                <option value="21:30">9:30 pm</option>
                                                <option value="22:00">10:00 pm</option>
                                                <option value="22:30">10:30 pm</option>
                                                <option value="23:00">11:00 pm</option>
                                                <option value="23:30">11:30 pm</option>
                                                <option value="00:00 1">12:00 am (midnight next day)</option>
                                                <option value="00:30 1">12:30 am (next day)</option>
                                                <option value="01:00 1">1:00 am (next day)</option>
                                                <option value="01:30 1">1:30 am (next day)</option>
                                                <option value="02:00 1">2:00 am (next day)</option>
                                                <option value="02:30 1">2:30 am (next day)</option>
                                                <option value="03:00 1">3:00 am (next day)</option>
                                                <option value="03:30 1">3:30 am (next day)</option>
                                                <option value="04:00 1">4:00 am (next day)</option>
                                                <option value="04:30 1">4:30 am (next day)</option>
                                                <option value="05:00 1">5:00 am (next day)</option>
                                                <option value="05:30 1">5:30 am (next day)</option>
                                                <option value="06:00 1">6:00 am (next day)</option>
                                              </AvField>
                                            </div>
                                          </Col>
                                          <Col sm="4" md={12}                                          >
                                            <div className="mb-2">
                                              <AvField
                                                onChange={(e) => this.handleChangeCast(e, index, 'addHOP')}
                                                // value={isEditHours ? isEditHours.info : addHours.info}
                                                value={(freshHOP && freshHOP[index] && freshHOP[index].freshHOPText)}
                                                bsSize="sm"
                                                type="text"
                                                name="freshHOPText"
                                                placeholder="i.e. Brunch"
                                              />
                                            </div>
                                          </Col>
                                        </Row>

                                        {index === 0 ? "" :
                                          <div className="d-flex">
                                            <span
                                              className="actionable ml-auto"
                                              onClick={() => {
                                                arrayHelpers.remove(index);
                                                this.handleremoveCast('freshHOP')
                                              }
                                              }
                                            ><FontAwesomeIcon icon="minus" /> Remove</span>
                                          </div>}
                                        <hr />
                                      </AvForm>
                                    </div>
                                  ))}
                                <div className="d-flex">
                                  <span className="actionable ml-auto"
                                    onClick={() => {
                                      arrayHelpers.push({
                                        freshHOPDay: '',
                                        freshHOPText: '',
                                        freshHOPStartTime: '',
                                        freshHOPEndTime: '',
                                      });
                                    }
                                    }
                                  ><FontAwesomeIcon icon="plus" /> Add More</span>
                                </div>
                              </div>

                            )}
                          />


                        </FormGroup>
                      </CollapseBasic>
                    </Col>
                  </Row>
                </ModalBody>
                <ModalFooter className="bg-white">
                  <div className="d-flex">
                    <div className="ml-auto">
                      <Button color="light" className="mw"
                        onClick={() => {
                          this.toggleCopyBranchToDefaultValues();
                          this.setState({ enterNewBranchDataModal: false, selectCopyBranchMethodModal: true })
                        }
                        }>
                        <i className="fa fa-angle-left"></i> Back
                </Button>
                      <Button
                        color="primary"
                        className="mw"
                        onClick={this.handleNextFreshSubmit}
                      >
                        Next
                </Button>
                    </div>
                  </div>
                </ModalFooter>
              </Modal>
            );
          }}
        />


        <Modal isOpen={this.state.isDeleteToggle} toggle={this.deleteToggle}>
          <ModalHeader toggle={this.deleteToggle}></ModalHeader>
          <ModalBody className="text-center">
            <h2 className="mb-3">Confirmation</h2>
            <p className="small">Are you sure you want to delete?</p>

            <div className="pt-4">
              <div>
                <Button onClick={this.deleteToggle} size="md" color="primary">Cancel</Button>
                <Button onClick={this.deleteBusinessRecord} size="md" color="primary">Ok</Button>
              </div>
            </div>
          </ModalBody>
        </Modal>
        <Modal isOpen={this.state.CopyBranchAddmodal} toggle={this.modalToggle}>
          <ModalHeader toggle={this.modalToggle}></ModalHeader>
          <ModalBody className="bg-dark text-white text-center">
            <h2 className="mb-3">Categories &amp; Specialities</h2>
            <p className="small">{'Select the right category for your business'}</p>

            <div className="pt-4">
              {/* Always visible */}
              <div className="mb-3">
                <Row form className="justify-content-between">
                  <Col md={6}>
                    <FormGroup className="mb-3">
                      <Input onChange={this.handleChangeCategory} bsSize="sm" type="select" name="choose_category" data-id="zero" className="transparent">
                        <option value="">Choose Category</option>
                        {businessCategory && Array.isArray(businessCategory) && businessCategory.length > 0 ?
                          businessCategory.map((item, idx_1) => {
                            return (<option key={idx_1} value={item.id} label={item.category}>{item.category}</option>)
                          }) : ''}
                      </Input>
                    </FormGroup>
                  </Col>
                  {/* hide subcategories by default */}
                  {totalSubCategories && Array.isArray(totalSubCategories) && totalSubCategories.length > 0 ?
                    totalSubCategories.map((subItem, index) => {
                      return (
                        <React.Fragment key={index}>
                          {index !== 0 && <Col md={6}></Col>}
                          <Col key={index} md={6} className={businessSubCategory && businessSubCategory[0] && businessSubCategory[0].length > 0 ? "" : "d-none"}>
                            {/* Closable Sub Categories block */}
                            <div className="d-flex mr-n2">
                              <FormGroup className="mb-3 flex-grow-1">
                                <Input onChange={(e) => this.changeSubCategory(e, 'undefined', index)} bsSize="sm" type="select" name="choose_sub_category" className="transparent">
                                  <option value="">Choose Subcategory</option>
                                  {businessSubCategory && businessSubCategory[0] && Array.isArray(businessSubCategory[0]) && businessSubCategory[0].length > 0 ?
                                    businessSubCategory[0].map(item => {
                                      return (<option value={item.id} label={item.category}>{item.category}</option>)
                                    }) : ''}
                                </Input>
                              </FormGroup>
                              {index !== 0 && <Button
                                onClick={() => { this.removeSubCateItems(index) }}
                                color="circle"
                                size="xs"
                                className="bg-primary align-self-start ml-n2 mt-n2"
                                title="Remove"
                              >
                                <FontAwesomeIcon icon="times" />
                              </Button>}
                            </div>
                          </Col>
                        </React.Fragment>
                      )
                    }
                    ) : null}

                  <Col md={6} className="ml-auto">
                    <div className="text-right">
                      <Button
                        hidden={businessSubCategory && businessSubCategory[0] && businessSubCategory[0].length == 0 ? true : false}
                        onClick={this.addSubCategories}
                        color="link"
                        size="sm"
                        className="text-primary">
                        <FontAwesomeIcon icon="plus" size="sm" />
                        add subcategory
                      </Button>
                    </div>
                  </Col>
                </Row>
                <hr className="bg-primary" />
              </div>

              {/* On Click off Add Category, Repeat below */}
              {anotherCategories && anotherCategories.length > 0 ?
                anotherCategories.map((item, index) => {
                  return (
                    <div key={index} className="mb-3" >
                      <Row form className="justify-content-between">
                        <Col md={6}>
                          <FormGroup className="mb-3">
                            <Input onChange={this.handleChangeCategory} bsSize="sm" type="select" name="choose_category" className="transparent">
                              <option value="">Choose Category</option>
                              {businessCategory && businessCategory.length > 0 ?
                                businessCategory.map(item => {
                                  return (<option value={item.id}>{item.category}</option>)
                                }) : ''}
                            </Input>
                          </FormGroup>
                        </Col>
                        {/* {'sub-categories'} */}
                        {anotherCategories && anotherCategories[index] && anotherCategories[index].length > -1 ?
                          anotherCategories[index].map((subItems, subIndex) => {
                            return (
                              <React.Fragment>
                                {subIndex !== 0 && <Col key={subIndex} md={6}></Col>}
                                <Col key={subIndex} md={6} className={businessSubCategory && businessSubCategory[index + 1] && businessSubCategory[index + 1].length > 0 ? "" : "d-none"}>
                                  {/* Closable Sub Categories block */}
                                  <div className="d-flex mr-n2">
                                    <FormGroup className="mb-3 flex-grow-1">
                                      <Input onChange={(e) => this.changeSubCategory(e, index + 1, subIndex)} bsSize="sm" type="select" name="choose_sub_category" className="transparent">
                                        <option value="">Choose Subcategory</option>
                                        {businessSubCategory && businessSubCategory[index + 1] && businessSubCategory[index + 1].length > 0 ?
                                          businessSubCategory[index + 1].map((item, idx_3) => {
                                            return (<option key={idx_3} value={item.id} label={item.category}>{item.category}</option>)
                                          }) : ''}
                                      </Input>
                                    </FormGroup>
                                    {subIndex !== 0 && <Button
                                      onClick={() => { this.removeSubCategoryItem(index + 1, subIndex) }}
                                      color="circle"
                                      size="xs"
                                      className="bg-primary align-self-start ml-n2 mt-n2"
                                      title="Remove"
                                    >
                                      <FontAwesomeIcon icon="times" />
                                    </Button>}
                                  </div>
                                </Col>
                              </React.Fragment>
                            )
                          }) : null
                        }

                        <Col md={6} className="ml-auto">
                          <div className="text-right">
                            <Button onClick={() => this.addAnotherSubCategories(index)} color="link" size="sm" className="text-primary">
                              <FontAwesomeIcon icon="plus" size="sm" />
                              {'add subcategory'}
                            </Button>
                          </div>
                        </Col>


                      </Row>
                      <hr className="bg-primary" />
                    </div>
                  )
                }) : null}

              <div className="text-right mb-3">
                <Button onClick={this.addAnotherCategories} color="link" size="sm" className="text-primary">
                  <FontAwesomeIcon icon="plus" size="sm" /> add another category
                                </Button>
              </div>

              <div>
                <FormGroup>
                  <Label className="d-block text-center text-white font-weight-normal">What your business specialities are?</Label>
                  <Input onChange={this.handleChange} bsSize="sm" className="transparent" type="textarea" rows="5" name="business_specialities" placeholder="Explain what gets you stand out from your competitors.What do customers love about your business?" value={business_specialities} />
                </FormGroup>
              </div>
              <div>
                <Button onClick={this.handleSubmitCategories} size="lg" block color="primary">Save &amp; Continue</Button>
                <Button size="sm" block color="link" onClick={this.modalToggle}>Skip this part</Button>
              </div>
            </div>
          </ModalBody>
        </Modal>
        <Modal isOpen={isToggleDeleteCategory} toggle={this.toggleDeleteCategory}>
          <ModalHeader toggle={this.toggleDeleteCategory}></ModalHeader>
          <ModalBody className="text-center">
            <h2 className="mb-3">Confirmation</h2>
            <p className="small">Are you sure you want to delete?</p>

            <div className="pt-4">
              <div>
                <Button onClick={this.toggleDeleteCategory} size="md" color="primary">Cancel</Button>
                <Button onClick={this.deleteCategory} size="md" color="primary">Ok</Button>
              </div>
            </div>
          </ModalBody>
        </Modal>

        <Modal isOpen={isToggleDeleteHours} toggle={this.toggleDeleteHours}>
          <ModalHeader toggle={this.toggleDeleteHours}></ModalHeader>
          <ModalBody className="text-center">
            <h2 className="mb-3">Confirmation</h2>
            <p className="small">Are you sure you want to delete?</p>

            <div className="pt-4">
              <div>
                <Button onClick={this.toggleDeleteHours} size="md" color="primary">Cancel</Button>
                <Button onClick={this.deleteHoursOfOperations} size="md" color="primary">Ok</Button>
              </div>
            </div>
          </ModalBody>
        </Modal>

        <Modal isOpen={isToggleDeleteOffers} toggle={this.toggleDeleteOffers}>
          <ModalHeader toggle={this.toggleDeleteOffers}></ModalHeader>
          <ModalBody className="text-center">
            <h2 className="mb-3">Confirmation</h2>
            <p className="small">Are you sure you want to delete?</p>

            <div className="pt-4">
              <div>
                <Button onClick={this.toggleDeleteOffers} size="md" color="primary">Cancel</Button>
                <Button onClick={this.deleteSpecialOffers} size="md" color="primary">Ok</Button>
              </div>
            </div>
          </ModalBody>
        </Modal>
        <Modal isOpen={isToggleDeletePayment} toggle={this.toggleDeletePayment}>
          <ModalHeader toggle={this.toggleDeletePayment}></ModalHeader>
          <ModalBody className="text-center">
            <h2 className="mb-3">Confirmation</h2>
            <p className="small">Are you sure you want to delete?</p>

            <div className="pt-4">
              <div>
                <Button onClick={this.toggleDeletePayment} size="md" color="primary">Cancel</Button>
                <Button onClick={this.deletePayment} size="md" color="primary">Ok</Button>
              </div>
            </div>
          </ModalBody>
        </Modal>
      </React.Fragment>
    );
  }
}


const mapState = (state) => {
  return ({
    profileData: state.user.current_user,
    corporate_details: state.user.corporate_id_details,
    current_role_and_permissions: state.user.current_role_and_permissions,
    top_bar_stats: state.user.top_bar_stats,
    filtered_stats_count: state.user.filtered_stats_count,
    getbranch_copy: state.user.getbranch_copy,
    getbranch_data: state.user.getbranch_data,
    admin_stats: state.user.admin_stats,
    corporate_copy_details: state.user.corporate_copy_details,
    branch_corporate_call_list: state.user.specific_branch_corporate_call_list,
    corporate_call: state.user.corporate_call,
    corporate_id: state.user.corporate_id,
    isAdmin: state.user.isAdmin,
    additional_data: state.user.additional_data,
    newName: state.user.new_name,
    newPosition: state.user.new_position,
    corporate_id: state.user.corporate_id,
    business_category: state.user.business_category,
    business_sub_category: state.user.business_sub_category,
    payment_update: state.user.payment_update,
    updated_additional_data: state.user.update_additional_info,
    sub_category_data: state.user.sub_category_data,
    amenities_data: state.user.amenities_data,
    biz_callon_type: state.user.biz_callon_type,

  });
}

const mapProps = (dispatch) => ({
  get_roles_list_filter_data: (value) => dispatch(get_roles_list_filter_data(value)),
  get_stats_filtered_data: (value) => dispatch(get_stats_filtered_data(value)),
  get_top_bar_counts: (id) => dispatch(get_top_bar_counts(id)),
  get_branch_copy: (country) => dispatch(get_branch_copy(country)),
  get_branch_data: (branchId) => dispatch(get_branch_data(branchId)),
  copy_add_branch: (data) => dispatch(copy_add_branch(data)),
  fresh_add_branch: (data) => dispatch(fresh_add_branch(data)),
  upload_corporate_media: (data, type, id) => dispatch(upload_corporate_media(data, type, id)),
  get_admin_stats: () => dispatch(get_admin_stats()),
  get_review_data_by_circle_click: ({ color, type, days, queryType, getbranches }) =>
    dispatch(get_review_data_by_circle_click({ color, type, days, queryType, getbranches })),
  get_corporate_id_details: (id) => dispatch(corporate_id_details(id)),
  get_corporate_copy_details: (id) => dispatch(corporate_copy_details(id)),
  get_specific_branch_corporate_call_list: (id) => dispatch(get_specific_branch_corporate_call_list(id)),
  get_corporate_call_list: (listing_id) => dispatch(get_corporate_call_list(listing_id)),
  update_listing_owner_guide: (data, listing_id) => dispatch(update_listing_owner_guide(data, listing_id)),
  post_business_update: (data, isAdd, userId, type) => dispatch(post_business_update(data, isAdd, userId, type)),
  delete_business_record: (entity, deleteId, userId, type) => dispatch(delete_business_record(entity, deleteId, userId, type)),
  get_business_category_data: (type) => dispatch(get_business_category_data(type)),
  post_business_category_data: (data, userId, type) => dispatch(post_business_category_data(data, userId, type)),
  update_payment_options: (userId, data, type) => dispatch(update_payment_options(userId, data, type)),
  delete_business_categories: (entityid, userId, type) => dispatch(delete_business_categories(entityid, userId, type)),
  post_hours_of_operation: (data, userId, type) => dispatch(post_hours_of_operation(data, userId, type)),
  put_hours_of_operation: (data, id, userId, type) => dispatch(put_hours_of_operation(data, id, userId, type)),
  delete_hours_of_operation: (id, userId, type) => dispatch(delete_hours_of_operation(id, userId, type)),
  get_sub_categories: (item) => dispatch(get_sub_categories(item)),
  post_special_offer: (data, isAdd, userId, type) => dispatch(post_special_offer(data, isAdd, userId, type)),
  update_special_offer: (data, userId, type) => dispatch(update_special_offer(data, userId, type)),
  delete_offer: (id, userId, type) => dispatch(delete_offer(id, userId, type)),
  add_additional_value: (id, data, type) => dispatch(add_additional_value(id, data, type)),
  add_name: (data, type) => dispatch(add_name(data, type)),
  add_position_value: (data, type) => dispatch(add_position_value(data, type)),
  update_additional_info: (data, id, type) => dispatch(update_additional_info(data, id, type)),
  delete_additional_info: (data, id, type) => dispatch(delete_additional_info(data, id, type)),
  get_amenities_options: (data) => dispatch(get_amenities_options(data)),
  upload_move_set_profile_media: (data) => dispatch(upload_move_set_profile_media(data)),
  delete_payment_option: (entityid, userentry, type) => dispatch(delete_payment_option(entityid, userentry, type)),
  delete_corporate_call: (corp_code, id, type) => dispatch(delete_corporate_call(corp_code, id, type)),
  get_biz_callon_type: () => dispatch(get_biz_callon_type()),
  corporate_call_to_action: (data, id, type) => dispatch(corporate_call_to_action(data, id, type)),

});

export default connect(mapState, mapProps)(OverallData);
