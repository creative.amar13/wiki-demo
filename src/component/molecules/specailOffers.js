import React, { Component } from 'react';
import CollapseBasic from '../atoms/collapse';
//import { FormGroup, Label, Input, Button, ButtonGroup, } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { corporate_id, corporate_id_details, post_special_offer, update_special_offer, delete_offer } from '../../actions/user';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { Modal, ModalHeader, ModalBody, FormGroup, Label, Button } from 'reactstrap';
import moment from 'moment';
import EditBtn from "../atoms/editBtn";
import DeleteBtn from "../atoms/deleteBtn";

class Specialities extends Component {
    mapRef = React.createRef();

    constructor(props) {
        super(props);
        this.state = {
            corporateId: null,
            phoneDiary: [],
            AdressDiary: [],
            webSet: [],
            emailSet: [],
            payOptions: [],
            categoryList: [],
            HoursOfOperation: [],
            specialofferSet: [],
            isToggleOffer: false,
            isEditOffer: false,
            addOffer: { special_offer: "", offer_type: "", from_date: "", to_date: "", claim_deal_link: "" },
            editRecordId: false,
            isToggleDeleteOffers: false,
            user_id: localStorage.getItem('profileId'),
            claim_deal_link_ur: "",

        };
        this.handleChangeOffer = this.handleChangeOffer.bind(this);
        this.toggleEditOffer = this.toggleEditOffer.bind(this);
        this.handleSubmitOffer = this.handleSubmitOffer.bind(this);

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.corporate_details) {
            this.setState({
                corporateId: nextProps.corporate_details.id,
                phoneDiary: nextProps.corporate_details.phone_set,
                AdressDiary: nextProps.corporate_details.address,
                webSet: nextProps.corporate_details.website_set,
                emailSet: nextProps.corporate_details.email_set,
                payOptions: nextProps.corporate_details.paymentoptions_set,
                categoryList: nextProps.corporate_details.taxonomy_list,
                HoursOfOperation: nextProps.corporate_details.hoursofoperation_set,
                specialofferSet: nextProps.corporate_details.specialoffer_set,
            }, () => {
                return (this.state.phoneDiary, this.state.AdressDiary, this.state.webSet, this.state.emailSet, this.state.categoryList, this.state.HoursOfOperation);
            });
        }
    }

    componentDidMount() {
        // this.props.get_corporate_id();
    }

    // getCorporateDetails = (id) => {
    //     if (id) {
    //         this.props.get_corporate_id_details(id);
    //     }
    // }

    specialofferData = () => {
        let specialofferList = this.state.specialofferSet;
        if (specialofferList && specialofferList.length > 0) {
            return specialofferList.map((item, index) =>
                <div className="d-flex mb-2 interactive" key={index}>
                    <span className="mr-2 small mt-1"><FontAwesomeIcon icon="ticket-alt" /> {item.special_offer} Valid until {moment(item.to_date).format('MMMM DD YYYY')}</span>
                    <div className="ml-auto interactive-appear">
                        {/* <ButtonGroup>
                            <Button onClick={() => this.toggleEditOffer(item)} size="sm" color="dark" title="Edit"><FontAwesomeIcon icon="edit" /> </Button>
                            <Button onClick={() => this.toggleDeleteOffers(item.id)} size="sm" color="danger" title="Delete"><FontAwesomeIcon icon="trash-alt" /></Button>
                        </ButtonGroup> */}
                        <EditBtn onClick={() => this.toggleEditOffer(item)} />
                        <DeleteBtn onClick={() => this.toggleDeleteOffers(item.id)} color="gold" />
                    </div>
                </div>

            )
        }
    }

    handleChangeOffer = (event) => {
        let { name, value } = event.target;
        let { addOffer } = this.state;
        addOffer[name] = value;
        this.setState({ addOffer });
    }

    toggleEditOffer = (isTrue) => {
        if (isTrue && isTrue.id) {
            let { addOffer } = this.state;
            addOffer['special_offer'] = isTrue.special_offer;
            addOffer['offer_type'] = isTrue.offer_type;
            addOffer['from_date'] = isTrue.from_date;
            addOffer['to_date'] = isTrue.to_date;
            addOffer['claim_deal_link'] = isTrue.claim_deal_link
            //addOffer['claim_deal_link'] = "None"
            this.setState({ addOffer });
        }
        this.setState({ isEditOffer: isTrue && isTrue.id ? isTrue : false, isToggleOffer: !this.state.isToggleOffer })
    }

    handleSubmitOffer = (event, errors, values) => {
        let { isEditOffer, corporateId, addOffer: { special_offer, offer_type, from_date, to_date, claim_deal_link }, user_id } = this.state;
        // let time = moment().format('HH:mm');
        let isAdd = true;
        let data = {
            "special_offer": special_offer,
            "offer_type": offer_type,
            "from_date": moment(from_date).format('MM-DD-YYYY'),
            "to_date": moment(to_date).format('MM-DD-YYYY'),
            "claim_deal_link": claim_deal_link,
            "name": "",
            "reviews_userentries": corporateId,
            "created_by": user_id,
        }
        this.setState({ errors, values });
        if (isEditOffer && isEditOffer.id) {
            isAdd = false;
            data.editform = true;
            data.entityid = isEditOffer.id
            if (this.state.errors.length === 0) {
                this.props.update_special_offer(data, corporateId);
                this.toggleEditOffer();
            }
        } else {
            if (this.state.errors.length === 0) {
                this.props.post_special_offer(data, isAdd, corporateId);
                this.toggleEditOffer();
            }
        }


    }

    setInitialOffer = () => {
        this.toggleEditOffer();
        this.setState({ addOffer: { special_offer: '', offer_type: '', from_date: '', to_date: '', claim_deal_link: '' } });
    }

    toggleDeleteOffers = (id) => {
        this.setState({
            isToggleDeleteOffers: !this.state.isToggleDeleteOffers,
            editRecordId: id ? id : false,
        });
    }

    deleteSpecialOffers = () => {
        let { editRecordId, corporateId } = this.state;
        this.props.delete_offer(editRecordId, corporateId);
        this.toggleDeleteOffers();
    }


    render() {
        let { specialofferSet, isToggleOffer, isEditOffer, addOffer, isToggleDeleteOffers, claim_deal_link_ur } = this.state;

        return (

            <React.Fragment>
                <CollapseBasic title="WR Special Offers" isOpen={true}>
                    <FormGroup className="mb-2">
                        <Label size="sm" className="p-0 mb-1">Offer</Label>
                        {/* Show if data */}

                        {specialofferSet && specialofferSet.length > 0 ?
                            (
                                <div>
                                    {this.specialofferData()}
                                </div>
                            ) : (
                                <div>
                                    <span className="small"><i>No Offer added</i></span>
                                </div>
                            )}

                        {/* Show when adding/ editing */}
                        <div className={isToggleOffer ? "" : "d-none"}>
                            <h4 className="mb-2">Add Offer</h4>
                            <AvForm onSubmit={this.handleSubmitOffer}>
                                <div className="mb-2">
                                    <AvField bsSize="sm" className="mb-2" type="textarea" name="special_offer" placeholder="Add Offer" onChange={this.handleChangeOffer} value={isEditOffer ? isEditOffer.special_offer : addOffer.special_offer} errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                    <AvField type="select" bsSize="sm" name="offer_type" className="mb-2" value={isEditOffer ? isEditOffer.offer_type : addOffer.offer_type} onChange={this.handleChangeOffer} errorMessage="This Field is required" validate={{ required: { value: true } }}>
                                        <option value="">Choose offer type</option>
                                        <option value="coupons">Coupons</option>
                                        <option value="special_discount">Special Discount</option>
                                    </AvField>
                                    <div className="mb-2">
                                        <Label className="d-block mb-0" size="sm">Start Date</Label>
                                        <AvField bsSize="sm" type="date" name="from_date" value={isEditOffer ? isEditOffer.from_date : addOffer.from_date} onChange={this.handleChangeOffer} errorMessage="This Field is required" validate={{ required: { value: true } }}></AvField>
                                    </div>
                                    <div className="mb-2">
                                        <Label className="d-block mb-0" size="sm">End Date</Label>
                                        <AvField bsSize="sm" type="date" name="to_date" value={isEditOffer ? isEditOffer.to_date : addOffer.to_date} onChange={this.handleChangeOffer} errorMessage="This Field is required" validate={{ required: { value: true } }} min={moment().format("YYYY-MM-DD")}></AvField>
                                    </div>
                                    <AvField
                                        bsSize="sm"
                                        className="mb-2"
                                        name="claim_deal_link"
                                        type="text"
                                        laceholder="Website to claim deal"
                                        value={
                                            claim_deal_link_ur ?
                                                "None" : addOffer.claim_deal_link
                                        }
                                        onChange={this.handleChangeOffer}
                                        errorMessage="This Field is required"
                                        validate={{
                                            required: { value: true }
                                        }}>
                                    </AvField>
                                    <div className="text-right">
                                        <Button size="sm" className="btn-icon-split mr-2" title="Save"> <span className="text">Save</span> <span className="icon"> <FontAwesomeIcon icon="check" /> </span></Button>
                                        <Button onClick={this.toggleEditOffer} size="sm" title="Cancel" color="dark" className="btn-icon-split ml-0"> <span className="text">Cancel</span> <span className="icon"><FontAwesomeIcon icon="times" /></span> </Button>
                                    </div>
                                    <hr />
                                </div>
                            </AvForm>
                        </div>

                        {/* Hide when User adding */}
                        <div className={isToggleOffer ? "d-none" : ""} onClick={this.setInitialOffer}>
                            <div className="d-flex">
                                <span className="actionable ml-auto"><FontAwesomeIcon icon="plus" /> Add Offer</span>
                            </div>
                        </div>
                    </FormGroup>
                </CollapseBasic>
                <Modal isOpen={isToggleDeleteOffers} toggle={this.toggleDeleteOffers}>
                    <ModalHeader toggle={this.toggleDeleteOffers}></ModalHeader>
                    <ModalBody className="text-center">
                        <h2 className="mb-3">Confirmation</h2>
                        <p className="small">Are you sure you want to delete?</p>

                        <div className="pt-4">
                            <div>
                                <Button onClick={this.toggleDeleteOffers} size="md" color="primary">Cancel</Button>
                                <Button onClick={this.deleteSpecialOffers} size="md" color="primary">Ok</Button>
                            </div>
                        </div>
                    </ModalBody>
                </Modal>

            </React.Fragment>
        )
    }
}

const mapState = (state) => {
    return {
        corporate_id: state.user.corporate_id,
        corporate_details: state.user.corporate_id_details,

    }
}

const mapProps = (dispatch) => {
    return {
        get_corporate_id: () => dispatch(corporate_id()),
        get_corporate_id_details: (id) => dispatch(corporate_id_details(id)),
        post_special_offer: (data, isAdd, userId) => dispatch(post_special_offer(data, isAdd, userId)),
        update_special_offer: (data, userId) => dispatch(update_special_offer(data, userId)),
        delete_offer: (id, userId) => dispatch(delete_offer(id, userId))

    }
}

export default withRouter(connect(mapState, mapProps)(Specialities));