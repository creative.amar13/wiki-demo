import React, { Component } from "react";
import {
  FormGroup,
  Label,
  Button,
  Row,
  Col,
  CustomInput,
} from "reactstrap";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import { AvForm, AvField } from 'availity-reactstrap-validation';
import moment from 'moment';
import { get_employee_detail, update_employee } from "../../../actions/user";

class EmployeesForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reviewsProfessionalId: 0,
      fname: '',
      lname: '',
      choose_profession: '',
      profession_title: '',
      start_date: '',
      end_date: '',
      is_present: false,
      showMore: false,
      isOpenRow: true,
      upstatus: false,
      addstatus: false,
      type: 'update',
      professionList: '',
      employeeDetail: '',
      professionalisPresent: true
    };
    this.handleChange = this.handleChange.bind(this);
    this.hideMore = this.hideMore.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // get_employee detail
    if (nextProps.employeeDetail) {
      this.setState({
        employeeDetail: nextProps.employeeDetail,
        fname: nextProps.employeeDetail.reviews_professional.firstname,
        lname: nextProps.employeeDetail.reviews_professional.lastname,
        start_date: moment(nextProps.employeeDetail.reviews_professional.from_date).format('YYYY-MM'),
        end_date: moment(nextProps.employeeDetail.reviews_professional.to_date).format('YYYY-MM'),
        is_present: nextProps.employeeDetail.reviews_professional.is_present,
        choose_profession: nextProps.employeeDetail.reviews_professional.profession.id,
        profession_title: nextProps.employeeDetail.character_role,
      });
    }
  }

  componentDidMount() {
    const { type, data, professionList, corporateId, professionalisPresent } = this.props;
    if (data !== undefined && data) {
      this.setState({ reviewsProfessionalId: data.id, professionList: professionList, type: type, corporateId: corporateId, professionalisPresent: professionalisPresent }, () => this.props.get_employee_detail(data.id));

    }

  }

  // Hide More
  hideMore = () => {
    let { type } = this.state;
    this.setState({ showMore: false });
    if (type === "update") {
      this.props.updateFunction(this.state.upstatus)
    }
    this.refs.form.reset();
  };

  /**
   * Function for set state on change
   * @param event
   */

  handleChange = (event) => {
    if (event.target.name === "choose_profession") {
      this.setState({ choose_profession: event.target.value });
    }
    if (event.target.type === 'checkbox') {
      if (event.target.checked === true) {
        this.setState({ is_present: !this.state.is_present, end_date: '', isOpenRow: false });
      } else {
        this.setState({ is_present: false, isOpenRow: true });
      }
    }
    this.setState({ [event.target.name]: event.target.value })
  }

  /**
 * Function for submit form
 * @param event
 */

  handleSubmit = (event, errors, values) => {
    const { fname, lname, choose_profession, profession_title, start_date, end_date, is_present, professionalisPresent, corporateId, reviewsProfessionalId } = this.state;
    var check = moment(start_date, 'YYYY-MM');
    var month = check.format('M');
    var year = check.format('YYYY');
    let data = {
      first_name: fname,
      last_name: lname,
      from_date: start_date,
      to_date: end_date,
      profession: choose_profession,
      character_role: profession_title,
      is_present: is_present,
      from_date_month: month,
      from_date_year: year,
      associated_with: corporateId,
    }
    this.setState({ errors, values });
    if (this.state.errors.length === 0) {
      this.props.update_employee(
        data,
        reviewsProfessionalId,
        professionalisPresent
      );
      this.props.saveUpdate();

    }

  }

  render() {
    let { showMore, fname, lname, choose_profession, profession_title, start_date, end_date, is_present, professionList } = this.state;
    var style = {};
    if (!showMore) {
      style.display = "none";
    }
    return (
      <React.Fragment>
        {/* Edit Employee form below */}
        <div className="mt-3">
          <h5 className="text-secondary-dark mb-3 fs-30">Edit Person</h5>
          <AvForm onSubmit={this.handleSubmit} ref="form">
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <AvField
                    type="text"
                    name="fname"
                    className="primary"
                    placeholder="First Name"
                    value={fname} onChange={this.handleChange} id="fname"
                    errorMessage="This Field is required" validate={{ required: { value: true } }}
                    readOnly
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <AvField
                    type="text"
                    name="lname"
                    className="primary"
                    placeholder="Last Name"
                    value={lname} onChange={this.handleChange} id="lname"
                    errorMessage="This Field is required" validate={{ required: { value: true } }}
                    readOnly
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <AvField type="select" value={choose_profession} onChange={this.handleChange} name="choose_profession" className="primary" id="exampleSelectMulti" errorMessage="This Field is required" validate={{ required: { value: true } }} >
                    <option value="">Choose Profession</option>
                    {(typeof professionList !== 'undefined' && professionList.length > 0) ? professionList.map(dropdown => {
                      return <option key={dropdown.id} dropdown={dropdown} value={dropdown.id} >
                        {dropdown.title}</option>;
                    }) : (<option> </option>)
                    }
                  </AvField>
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <AvField
                    type="text"
                    name="profession_title"
                    className="primary"
                    placeholder="Enter profession title"
                    onChange={this.handleChange}
                    value={profession_title}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label className="text-primary mb-0">Start Date</Label>
                  <AvField
                    type="month"
                    name="start_date"
                    className="primary"
                    placeholder="Enter start date"
                    onChange={this.handleChange}
                    value={start_date}
                  />
                </FormGroup>
              </Col>
              {/* Remove below if Present field checked */}
              {!this.state.is_present ? (
                <Col md={6}>
                  <FormGroup>
                    <Label className="text-primary mb-0">End Date</Label>
                    <AvField
                      type="month"
                      name="end_date"
                      className="primary"
                      placeholder="Enter end date"
                      onChange={this.handleChange}
                      value={end_date}
                    />
                  </FormGroup>
                </Col>
              ) : ('')}
            </Row>
            <FormGroup>
              <CustomInput
                type="checkbox"
                id="ifWorkingEmployee"
                className="dark"
                label="Present"
                onChange={this.handleChange}
                checked={is_present}
              />
            </FormGroup>
            <FormGroup>
              <div className="text-right">
                <Button color="primary" className="mw mr-2">
                  {" "}
                  Save
                </Button>
                <Button color="light" className="mw ml-0" onClick={this.hideMore}>
                  {" "}
                  Cancel{" "}
                </Button>
              </div>
            </FormGroup>
          </AvForm>
        </div>

      </React.Fragment>
    );
  }
}

const mapState = (state) => {
  return {
    employeeDetail: state.user.employeeDetail
  }
}

const mapProps = (dispatch) => {
  return {
    get_employee_detail: (employeeId) => dispatch(get_employee_detail(employeeId)),
    update_employee: (data, employeeId, professionalisPresent) => dispatch(update_employee(data, employeeId, professionalisPresent)),
  }
}

export default connect(mapState, mapProps)(EmployeesForm);
