import React, { Component } from "react";
import {
  FormGroup,
  Label,
  Button,
  Row,
  Col,
  ButtonGroup,
  CustomInput,
  Modal, ModalBody, ModalFooter
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import { get_employee_list, add_employee, corporate_id, get_profession_type, delete_employee, approve_employee, disapprove_employee } from "../../../actions/user";
import moment from 'moment';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import EmployeesForm from './employeesform.js';
class Employees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewEmpType: "current_emp",
      employeeList: [],
      showMore: false,
      pageCount: 0,
      reviewsProfessionalId: 0,
      confirmDeleteModal: false,
      professionalisPresent: true,
      fname: '',
      lname: '',
      choose_profession: '',
      profession_title: '',
      start_date: '',
      end_date: '',
      is_present: false,
      isOpenRow: true,
      updateStatus: false,
      corporateId: null,
      professionList: [],
      pageNo: 1,

    };
    this.addMore = this.addMore.bind(this);
    this.hideMore = this.hideMore.bind(this);
    this.handleOnCurrent = this.handleOnCurrent.bind(this);
    this.handleOnEx = this.handleOnEx.bind(this);
    this.handleDisapprove = this.handleDisapprove.bind(this);
    this.handleApprove = this.handleApprove.bind(this);
    this.handleOnDeleteConfirmation = this.handleOnDeleteConfirmation.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.updateShow = this.updateShow.bind(this);
    this.updateStatusFunction = this.updateStatusFunction.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // get_employee_list
    if (
      nextProps.employee_list &&
      nextProps.employee_list.results &&
      Array.isArray(nextProps.employee_list.results) &&
      nextProps.employee_list.results.length > 0
    ) {
      this.setState({
        employeeList: nextProps.employee_list.results,
        pageCount: nextProps.employee_list.count,
      });
    }

    if (nextProps.corporate_id) {
      this.setState({ corporateId: nextProps.corporate_id.id });
    }
    if (
      nextProps.employee_profession_list &&
      nextProps.employee_profession_list.results &&
      Array.isArray(nextProps.employee_profession_list.results) &&
      nextProps.employee_profession_list.results.length > 0
    ) {
      this.setState({
        professionList: nextProps.employee_profession_list.results,
        pageCount: nextProps.employee_profession_list.count,
      });
    }


  }

  componentDidMount() {
    let professionalisPresent = this.state.professionalisPresent;
    this.props.get_employee_list(professionalisPresent);
    this.props.get_profession_type(this.state.pageNo);
  }

  /**
  * Function for confirmation Delete
  * @param event
  */

  confirmDeleteModalToggle = () => {
    if (this.state.reviewsProfessionalId) {
      this.setState({ reviewsProfessionalId: 0 });
    }
    this.setState({ confirmDeleteModal: !this.state.confirmDeleteModal });
  };

  /**
   * Function to Delete Employee
   * @param event
   */
  handleOnDeleteConfirmation = () => {
    if (this.state.reviewsProfessionalId) {
      this.props.delete_employee(this.state.reviewsProfessionalId, this.state.professionalisPresent);
      this.setState({
        confirmDeleteModal: !this.state.confirmDeleteModal,
        reviewsProfessionalId: 0,
      });
    }
  };

  // Function for Disapprove
  handleDisapprove = (id) => {
    if (id) {
      let data = {};
      data.empid = id;
      let disapprove = 'disapprove';
      this.props.disapprove_employee(data, disapprove, id, this.state.professionalisPresent);
    }
  };

  // Function for Approve
  handleApprove = (id) => {
    if (id) {
      let approve = 'approve'
      let data = {};
      data.empid = id;
      this.props.approve_employee(data, approve, id, this.state.professionalisPresent);
    }

  };

  // Add More
  addMore = () => {
    this.setState({ showMore: true, addStatus: true });
  };

  // Update More
  updateShow = (id) => {
    this.setState({ updateStatus: true, reviewsProfessionalId: id });
  };

  /**
   * Function to props from modal to parent
   * @param state
   */
  updateStatusFunction(data) {
    this.setState(prevState => ({
      updateStatus: data.upstatus

    }))
  }

  // Hide More
  hideMore = () => {
    this.setState({ showMore: false });
    this.refs.form.reset();
  };

  handleOnCurrent = (viewEmpType, professionalisPresent) => {
    this.setState({ viewEmpType: viewEmpType, professionalisPresent: professionalisPresent });
    this.props.get_employee_list(professionalisPresent);
  };

  handleOnEx = (viewEmpType, professionalisPresent) => {
    this.setState({ viewEmpType: viewEmpType, professionalisPresent: professionalisPresent });
    this.props.get_employee_list(professionalisPresent);
  };

  /**
   * Function for set state on change
   * @param event
   */

  handleChange = (event) => {

    if (event.target.name === "choose_profession") {
      this.setState({ choose_profession: event.target.value });
    }
    if (event.target.type === 'checkbox') {
      if (event.target.checked === true) {
        this.setState({ is_present: !this.state.is_present, end_date: '', isOpenRow: false });
      } else {
        this.setState({ is_present: false, isOpenRow: true });
      }
    }
    this.setState({ [event.target.name]: event.target.value })
  }

  /**
   * Function for submit form
   * @param event
   */

  handleSubmit = (event, errors, values) => {
    const { fname, lname, choose_profession, profession_title, start_date, end_date, is_present, professionalisPresent, corporateId } = this.state;
    var check = moment(start_date, 'YYYY/MM/DD');
    var month = check.format('M');
    var year = check.format('YYYY');
    let data = {
      first_name: fname,
      last_name: lname,
      from_date: start_date,
      to_date: end_date,
      profession: choose_profession,
      character_role: profession_title,
      is_present: is_present,
      from_date_month: month,
      from_date_year: year,
      associated_with: corporateId,
    }
    this.setState({ errors, values });
    if (this.state.errors.length === 0) {
      this.props.add_employee(
        data,
        professionalisPresent
      );
      this.refs.form.reset();
    }

  }

  handleScroll = (event) => {
    const e = event.nativeEvent;
    if (e.target.scrollTop + 10 >= e.target.scrollHeight - e.target.clientHeight) {
      this.setState((prevState, props) => ({
        pageNo: prevState.pageNo + 1
      }));
      this.props.get_profession_type(this.state.pageNo);
    }
  }


  render() {
    let { showMore, employeeList, fname, lname, choose_profession, profession_title, start_date, end_date, is_present, professionList, corporateId, professionalisPresent } = this.state;
    var style = {};
    var style1 = { display: "block" };
    if (!showMore) {
      style.display = "none";
    }

    return (
      <React.Fragment>
        <div className="mb-3">
          <ButtonGroup className="type-filter flex-wrap" size="sm">
            <div className="item d-flex align-items-center">
              <Button
                color="transparent"
                onClick={() => this.handleOnCurrent("current_emp", true)}
                active={this.state.viewEmpType === "current_emp"}
              >
                Currently Working here
            </Button>
            </div>
            <div className="item d-flex align-items-center">
              <Button
                color="transparent"
                onClick={() => this.handleOnEx("ex_emp", false)}
                active={this.state.viewEmpType === "ex_emp"}
              >
                Former employees
            </Button>
            </div>
          </ButtonGroup>
        </div>
        <div className="bg-white p-3">
          {/* Employees table */}

          {employeeList && employeeList.length > 0 ? (
            employeeList.map((item, index) => {
              return (
                <div className="wr-review-list-new d-flex flex-wrap align-items-center fs-14" key={index}>
                  <div className="wr-review-list-new-left">
                    <img src="https://stagingdatawikireviews.s3.amazonaws.com/images/friend-photo.png" alt="" title="" width="50" height="50" />
                  </div>
                  <div className="wr-review-list-new-right">
                    <span className="d-block">
                      <span className="emp-name"> <a href="#"> {item.reviews_professional.firstname.charAt(0).toUpperCase() + item.reviews_professional.firstname.substring(1)} {item.reviews_professional.lastname.charAt(0).toUpperCase() + item.reviews_professional.lastname.substring(1)}
                      </a> {item.reviews_professional.profession.title} </span>
                      {item.reviews_professional.created_by.user && <span className="added-by">added by <strong>{item.get_created_by_user.name}</strong></span>}
                      <span className="month"> {moment(item.reviews_professional.from_date).format('MMMM YYYY')} - {item.reviews_professional.is_present == true ? 'working' : moment(item.reviews_professional.to_date).format('MMMM YYYY')} </span>
                    </span>
                    <span className="added-by">
                      <strong>for {item.reviews_userentries.name}</strong>
                    </span>
                    <div className="float-right wr-review-list-new-right-actions">
                      <ButtonGroup>
                        <Button size="sm"
                          color="transparent"
                          className="text-muted" title="Edit">
                          <FontAwesomeIcon icon="edit" onClick={() => this.updateShow(item.reviews_professional.id)} />{" "}
                        </Button>
                        <Button size="sm"
                          color="transparent"
                          className="text-muted" title="Delete" onClick={() => {
                            this.setState({
                              reviewsProfessionalId: item.reviews_professional.id,
                            });
                            this.confirmDeleteModalToggle();
                          }}>
                          <FontAwesomeIcon icon="trash-alt" />
                        </Button>
                      </ButtonGroup>
                    </div>
                  </div>
                  {/* ad4db4253cb53d1f626f957a4cf8696dd00f26b8 */}
                  {!item.status ?
                    <div className="wr-review-aproved-new text-right w-100 pt-4">
                      {!item.status && (item.status != '0' && item.staus != '1') && <button type="button" className="btn btn-tertiary btn-sm" onClick={() => { this.handleDisapprove(item.reviews_professional.id) }}>Disapprove</button>}
                      {!item.status && (item.status != '0' && item.staus != '1') && <button type="button" className="btn btn-primary btn-sm" onClick={() => { this.handleApprove(item.reviews_professional.id) }}>Approve</button>}
                    </div>
                    :
                    ''
                  }
                  {this.state.updateStatus === true && this.state.reviewsProfessionalId === item.reviews_professional.id &&
                    <EmployeesForm type="update"  data={item} professionList={professionList} corporateId={corporateId} professionalisPresent={professionalisPresent} updateFunction={this.updateStatusFunction} saveUpdate = {()=>{this.setState({ updateStatus :false})}} />
                  }
                </div>
              );
            })
          ) : (
              <div className="bg-white p-3">
                <h2 className="text-secondary-dark">No employees added yet</h2>
              </div>
            )}
        </div>

        {/* Hide this button while adding */}
        <div className="text-right mt-2">
          <Button color="primary" size="sm" onClick={this.addMore}>
            Add More
            </Button>
        </div>

        {/* Add Employee form below */}
        <div className="mt-3 bg-white p-3" style={style}>
          <h5 className="text-secondary-dark mb-3 fs-30">Add Another Person</h5>
          <AvForm onSubmit={this.handleSubmit} ref="form">
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <AvField
                    type="text"
                    name="fname"
                    className="primary"
                    placeholder="First Name"
                    value={fname} onChange={this.handleChange} id="fname"
                    errorMessage="This Field is required" validate={{ required: { value: true } }}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <AvField
                    type="text"
                    name="lname"
                    className="primary"
                    placeholder="Last Name"
                    value={lname} onChange={this.handleChange} id="lname"
                    errorMessage="This Field is required" validate={{ required: { value: true } }}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row form>
              <Col md={6}>
                <FormGroup onScroll={this.handleScroll}>
                  <AvField type="select" value={choose_profession} onChange={this.handleChange} name="choose_profession" className="primary" id="exampleSelectMulti" errorMessage="This Field is required" validate={{ required: { value: true } }} >
                    <option value="">Choose Profession</option>
                    {(typeof professionList !== 'undefined' && professionList.length > 0) ? professionList.map(dropdown => {
                      return <option key={dropdown.id} dropdown={dropdown} value={dropdown.id} >
                        {dropdown.title}</option>;
                    }) : (<option> </option>)
                    }
                  </AvField>

                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <AvField
                    type="text"
                    name="profession_title"
                    className="primary"
                    placeholder="Enter profession title"
                    onChange={this.handleChange}
                    value={profession_title}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label className="text-primary mb-0">Start Date</Label>
                  <AvField
                    type="month"
                    name="start_date"
                    className="primary"
                    placeholder="Enter start date"
                    onChange={this.handleChange}
                    value={start_date}
                  />
                </FormGroup>
              </Col>
              {/* Remove below if Present field checked */}
              {this.state.isOpenRow ? (
                <Col md={6}>
                  <FormGroup>
                    <Label className="text-primary mb-0">End Date</Label>
                    <AvField
                      type="month"
                      name="end_date"
                      className="primary"
                      placeholder="Enter end date"
                      onChange={this.handleChange}
                      value={end_date}
                    />
                  </FormGroup>
                </Col>
              ) : ('')}
            </Row>
            <FormGroup>
              <CustomInput
                type="checkbox"
                id="ifWorkingEmployee"
                className="dark"
                label="Present"
                onChange={this.handleChange}
                checked={is_present}
              />
            </FormGroup>
            <FormGroup>
              <div className="text-right">
                <Button color="primary" className="mw mr-2">
                  {" "}
                  Save
                </Button>
                <Button color="light" className="mw ml-0" onClick={this.hideMore}>
                  {" "}
                  Cancel{" "}
                </Button>
              </div>
            </FormGroup>
          </AvForm>
        </div>
        {/* Delete Confirmation Modal */}
        <Modal
          isOpen={this.state.confirmDeleteModal}
          toggle={this.confirmDeleteModalToggle}
          className="text-center"
        >
          <div className="bg-white modal-header">
            <h5 className="modal-title mx-auto text-dark">Confirmation</h5>
            <button
              className="btn btn-sm"
              aria-label="Close"
              onClick={this.confirmDeleteModalToggle}
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <ModalBody className="text-dark">
            Are you sure you want to delete this employee?
          </ModalBody>
          <ModalFooter className="bg-white">
            <div className="text-center w-100">
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.confirmDeleteModalToggle}
              >
                Cancel
              </Button>
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.handleOnDeleteConfirmation}
              >
                Ok
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}


const mapState = (state) => {
  return {
    corporate_id: state.user.corporate_id,
    employee_list: state.user.employee_list,
    employee_profession_list: state.user.profession_type

  }
}

const mapProps = (dispatch) => {
  return {
    get_corporate_id: () => dispatch(corporate_id()),
    get_employee_list: (id) => dispatch(get_employee_list(id)),
    get_profession_type: (page) => dispatch(get_profession_type(page)),
    add_employee: (data, professionalisPresent) => dispatch(add_employee(data, professionalisPresent)),
    delete_employee: (employeeId, professionalisPresent) => dispatch(delete_employee(employeeId, professionalisPresent)),
    approve_employee: (data, approve, employeeId, professionalisPresent) => dispatch(approve_employee(data, approve, employeeId, professionalisPresent)),
    disapprove_employee: (data, disapprove, employeeId, professionalisPresent) => dispatch(disapprove_employee(data, disapprove, employeeId, professionalisPresent)),

  }
}

export default connect(mapState, mapProps)(Employees);
