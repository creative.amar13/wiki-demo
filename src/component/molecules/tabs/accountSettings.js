import React, { Component } from "react";
import {
  Table, NavItem, NavLink, Nav, TabPane, TabContent, Button, Row, Col, Form, FormGroup, Label, Input, FormText, DropdownToggle,
  DropdownMenu, UncontrolledDropdown, DropdownItem, Alert, Modal, ModalHeader, ModalBody, ModalFooter, Media, UncontrolledTooltip, CustomInput
} from "reactstrap";
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import { get_account_list, get_biz_callon_details, get_corporate_call_list, getallbranch, get_biz_callon_type, delete_corporate_call, corporate_call_to_action } from "../../../actions/user";
import moment from 'moment';
import MultiSelect from "react-multi-select-component";

class AccountSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accountData: {},
      bizCallonDetails: {},
      user_id: localStorage.getItem('profileId'),
      listing_id: '',
      viewAccSettingsType: 'info',
      corporateCallList: [],
      setup: false,
      getStarted: false,
      allBranches: [],
      selectValue: [],
      typeService: '',
      linked_url: '',
      headline: '',
      sub_headline: '',
      response_time: null,
      postCallList: [],
      visible: false,
      setVisible: true,
      isOpenRow: false,
      checkAll: false,
      rowState: [],
      checkcount: 0,
      isError: false,
      allCountries: [
        {
          label: 'Canada',
          value: 'canada'
        },
        {
          label: 'USA',
          value: 'usa'
        }
      ],
      setAllCountries: [],
      allStates: [
        {
          label: 'Alabama',
          value: 'alabama'
        },
        {
          label: 'California',
          value: 'california'
        },
        {
          label: 'Florida',
          value: 'florida'
        },
        {
          label: 'Georgia',
          value: 'georgia'
        },
        {
          label: 'Minnesota',
          value: 'minnesota'
        }
      ],
      setAllStates: [],
      allCallListings: [
        {
          label: 'Dont\'t have a call to',
          value: 'callTo'
        },
        {
          label: 'Book now',
          value: 'bookNow'
        },
        {
          label: 'Order now',
          value: 'orderNow'
        },
        {
          label: 'Buy Tickets',
          value: 'buyTickets'
        }
      ],
      setAllCallListings: [],
      message: '',
      previewImage: '',
      hoverImage: 'call_on_static1.png',
      deleteCorp_code: '',
      confirmDeleteModal: false,
    };
    this.handleOnInfo = this.handleOnInfo.bind(this);
    this.handleOnBiz = this.handleOnBiz.bind(this);
    this.setupCall = this.setupCall.bind(this);
    this.getStarted = this.getStarted.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onHover = this.onHover.bind(this);
    this.handleOnDeleteConfirmation = this.handleOnDeleteConfirmation.bind(this);

  }

  componentWillReceiveProps(nextProps) {
    //get account detail
    if (
      nextProps.account_list &&
      Object.keys(nextProps.account_list).length > 0
    ) {
      this.setState({
        accountData: nextProps.account_list.membersip_details,
      });
    }
    //get biz callon detail
    if (
      nextProps.biz_callon_details &&
      Object.keys(nextProps.biz_callon_details).length > 0
    ) {
      this.setState({
        bizCallonDetails: nextProps.biz_callon_details,
      });
    }

    // get_corporate call to action
    if (
      Array.isArray(nextProps.corporate_call) &&
      nextProps.corporate_call.length > 0
    ) {
      this.setState({ corporateCallList: nextProps.corporate_call });
    }

    // get all branches
    if (
      nextProps.all_branches && nextProps.all_branches.results &&
      Object.keys(nextProps.all_branches).length > 0
    ) {
      this.setState({ allBranches: nextProps.all_branches.results.result });
      for (var i = 0; i < nextProps.all_branches.results.result.length; i++) {
        this.state.rowState[i] = false;
      }
    }
    // get all biz callon type
    if (
      Array.isArray(nextProps.biz_callon_type) &&
      nextProps.biz_callon_type.length > 0
    ) {
      this.setState({ bizCallonType: nextProps.biz_callon_type });
    }

  }

  componentDidMount() {
    let { user_id, listing_id } = this.state;
    this.props.get_account_list();
    this.props.get_corporate_call_list(listing_id);
    this.props.getallbranch();
    this.props.get_biz_callon_type();

  }

  handleOnInfo = (viewAccSettingsType) => {
    this.setState({ viewAccSettingsType: viewAccSettingsType })
    this.props.get_account_list();

  }
  // mouse hover on action
  onHover = (activeType) => {
    let hoverimag = "";
    if (activeType == "Book Now") {
      hoverimag = "call_on_static1.png"
    } else if (activeType == "Buy Tickets") {
      hoverimag = "on-call-button-buy-ticket-new.png"
    } else if (activeType == "Contact Us") {
      hoverimag = "on-call-button-biz-contact-us-new.png"
    } else if (activeType == "Order Online") {
      hoverimag = "call_on_order.png"
    } else if (activeType == "Request A Consultation") {
      hoverimag = "on-call-button-biz-consult-with-us-new.png"
    } else if (activeType == "Request A Quote") {
      hoverimag = "on-call-button-biz-request-now.png"
    } else if (activeType == "Reserve Now") {
      hoverimag = "on-call-button-biz-reserve-new.png"
    } else if (activeType == "Schedule Appointment") {
      hoverimag = "on-call-button-biz-schedule-now.png"
    } else {
      hoverimag = "call_on_learn_more-new.png"
    }
    this.setState({ hoverImage: hoverimag })
  }

  // Set up Call
  setupCall = () => {
    this.setState({ setup: true })
  }
  // Get Started
  getStarted = () => {
    this.setState({ getStarted: true });
    this.props.getallbranch();
    this.props.get_biz_callon_type();
  }

  handleOnBiz = (viewAccSettingsType) => {
    let { user_id, listing_id } = this.state;
    let data1 = {}
    data1.user_id = user_id;
    data1.listing_id = listing_id;
    this.setState({ viewAccSettingsType: viewAccSettingsType, setup: false, getStarted: false, isOpenRow: false, headline: '', sub_headline: '' })
    this.props.get_biz_callon_details(data1);
    this.props.get_corporate_call_list(listing_id);
    this.refs.form.reset();
  }

  /**
  * Function to Close Alert
  * @param event
  */

  onDismiss = () => {
    this.setState({ visible: false })
  }

  /**
   * Function to Delete Corporate Call To Action
   * @param event
   */
  handleOnDeleteConfirmation = () => {
    if (this.state.deleteCorp_code) {
      this.props.delete_corporate_call(
        this.state.deleteCorp_code
      );
      this.setState({
        confirmDeleteModal: !this.state.confirmDeleteModal,
        deleteCorp_code: '',
      });
      this.props.get_corporate_call_list(this.state.listing_id);
      setTimeout(
        function () {
          this.handleOnBiz("c2a");
        }
          .bind(this),
        3000
      );
    }
  };


  /**
   * Function for set state on change
   * @param event
   */

  handleChange = (event) => {
    let opts = [], opt;
    if (event.target.name == "select_multiple_companies" && event.target.type === 'checkbox') {
      let val = event.target.getAttribute('data-id')
      if (event.target.checked) {
        // Pushing the object into array
        this.state.rowState[val] = true;
        this.state.selectValue.push(parseInt(event.target.value));
        if (this.state.checkAll) {
          this.state.checkAll = !this.state.checkAll;
        }
        this.setState({
          rowState: this.state.rowState,
          checkAll: this.state.checkAll
        });
      } else {
        this.state.rowState[val] = false;
        if (this.state.checkAll) {
          this.state.checkAll = this.state.checkAll;
        }
        this.setState({
          rowState: this.state.rowState,
          checkAll: this.state.checkAll
        });
        let el = this.state.selectValue.find(itm => itm === parseInt(event.target.value));
        if (el)
          this.state.selectValue.splice(this.state.selectValue.indexOf(el), 1);
      }
      this.setState({ selectValue: this.state.selectValue, isError: false });
    }
    if (event.target.name == "type_of_service") {
      let imagename = "";
      if (event.target.value == "Book Now" || event.target.value == "Schedule Appointment") {
        imagename = "calendar.png"
      } else if (event.target.value == "Buy Tickets") {
        imagename = "buytickets.png"
      } else if (event.target.value == "Contact Us") {
        imagename = "contactus.png"
      } else if (event.target.value == "Order Online") {
        imagename = "OrderOnline.png"
      } else if (event.target.value == "Request A Consultation") {
        imagename = "RequestAConsultation.png"
      } else if (event.target.value == "Request A Quote") {
        imagename = "RequestAQuote.png"
      } else if (event.target.value == "Reserve Now") {
        imagename = "ReserveNow.png"
      } else {
        imagename = "LearnMore.png"
      }
      this.setState({ typeService: event.target.value, isOpenRow: true, previewImage: imagename, headline: '', sub_headline: '' });
    }

    this.setState({ [event.target.name]: event.target.value })
  }


  handleAllChecked = (event) => {
    var rowState = [];
    var nocheckAll = [];
    var checkState = !this.state.checkAll;
    for (var i = 0; i < this.state.allBranches.length; i++) {
      rowState[i] = checkState;
      if (checkState == true) {
        let el = this.state.selectValue.find(itm => itm === this.state.allBranches[i].id);
        if (el)
          this.state.selectValue.splice(this.state.selectValue.indexOf(el), 1);
        this.state.selectValue.push(this.state.allBranches[i].id);
      } else {
        let el = this.state.selectValue.find(itm => itm === this.state.allBranches[i].id);
        if (el)
          this.state.selectValue.splice(this.state.selectValue.indexOf(el), 1);
      }
    }

    this.state.checkAll = checkState;
    this.setState({
      rowState: rowState,
      checkAll: this.state.checkAll,
      isError: false
    });

  }

  /**
 * Function for confirmation Delete
 * @param event
 */

  confirmDeleteModalToggle = () => {
    this.setState({ confirmDeleteModal: !this.state.confirmDeleteModal });
  };

  /**
  * Function for submit form
  * @param event
  */

  handleSubmit = (event, errors, values) => {
    event.preventDefault();
    const { selectValue, headline, sub_headline, typeService, linked_url, response_time } = this.state;
    if (selectValue.length == 0) {
      this.setState({ isError: true })
      //return false;
    } else {
      let data = {
        ent: selectValue,
        selected_type: typeService,
        headline: headline,
        subheadline: sub_headline,
        response_time: response_time,
        selected: true,
        linkedurl: linked_url,

      }
      this.setState({ errors, values });
      if (this.state.errors.length == 0) {

        this.setState({ visible: true, message: "Add Successfully", isOpenRow: false })
        this.props.corporate_call_to_action(data);
        this.props.get_corporate_call_list(this.state.listing_id);
        this.refs.form.reset();
        setTimeout(
          function () {
            this.handleOnBiz("c2a");
            this.onDismiss();
          }
            .bind(this),
          3000
        );
      }

    }

  }

  onSelectCountries = (value) => {
    this.setState({ setAllCountries: value });
  }
  onSelectStates = (value) => {
    this.setState({ setAllStates: value });
  }
  onSelectCallListingsBy = (value) => {
    this.setState({ setAllCallListings: value });
  }

  render() {
    const { accountData, user_id, corporateCallList, setup, getStarted, allBranches, bizCallonType, selectValue, typeService, linked_url, headline, sub_headline, visible, setVisible, previewImage, hoverImage, response_time, rowState } = this.state;

    var style = {};
    var self = this;
    var style1 = {};
    var style2 = {};
    if (!setup) {
      style.display = 'none'
      style2.display = 'none'

    } else {
      style1.display = 'none'
      style2.display = 'none'
    }
    if (getStarted) {
      style2.display = 'block'
      style1.display = 'none'
      style.display = 'none'
    }
    return (
      <React.Fragment>
        <div className="text-primary">
          <div>
            <Nav tabs className="mb-3 non-decorated-alt">
              <NavItem>
                <NavLink href="#"
                  active={this.state.viewAccSettingsType === "info"}
                  onClick={() => {
                    this.handleOnInfo("info")
                  }}
                >Account Information
              </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#"
                  active={this.state.viewAccSettingsType === "c2a"}
                  onClick={() => {
                    this.handleOnBiz("c2a")
                  }}
                >Call to Action
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.viewAccSettingsType}>
              <TabPane tabId="info">
                <div className="bg-white p-3">
                  <div>
                    <h2 className="text-secondary">Membership and Billing</h2>
                    <hr />
                    <Table size="sm" borderless>
                      <tbody>
                        <tr>
                          <th scope="row">Email</th>
                          <td>{accountData.email}</td>
                        </tr>
                        <tr>
                          <th scope="row">Password</th>
                          <td>{"******"}</td>
                        </tr>
                      </tbody>
                    </Table>
                  </div>

                  <div className="mt-5">
                    <h2 className="text-secondary">Plan Details</h2>
                    <hr />
                    <Table size="sm" borderless>
                      <tbody>
                        <tr>
                          <th scope="row">Account</th>
                          <td>Enterprise Account</td>
                        </tr>
                      </tbody>
                    </Table>
                  </div>
                </div>
              </TabPane>
              <TabPane tabId="c2a">

                {/* List block */}
                <div className="mb-3">
                  <div className="text-right">
                    <Button color="primary" size="sm" onClick={this.setupCall}><FontAwesomeIcon icon="plus" /> Set up your call</Button>
                  </div>
                  <div style={style1}>
                    <h2 className="text-secondary">List of your Call </h2>
                    <hr className="bg-secondary" />

                    {corporateCallList && corporateCallList.length > 0 ? (
                      corporateCallList.map((call, Index) => {
                        if(call.corp_code){
                        return (
                          <div className="bg-white p-3 mb-3" key={Index}>
                            <div className="d-flex mx-n2">
                              <div className="px-2">
                                <div className="text-primary-dark fs-14">
                                  <span>Your current Call to Action button is <strong className="font-weight-bold">{call.selected_type}</strong></span>
                                  <br />
                                  <strong>Last update:</strong>
                                  <span> {moment(call.last_update).format('MMMM DD YYYY')}</span>
                                </div>
                                <div className="mb-3">
                                  <UncontrolledDropdown>
                                    <DropdownToggle color="transparent" size="sm" caret>
                                      <span className="text-primary font-weight-bold mr-2">{call.businesses ? call.businesses.length : 0} branches</span>
                                    </DropdownToggle>
                                    <DropdownMenu modifiers={{
                                      setMaxHeight: {
                                        enabled: true,
                                        order: 890,
                                        fn: (data) => {
                                          return {
                                            ...data,
                                            styles: {
                                              ...data.styles,
                                              overflow: 'auto',
                                              maxHeight: '200px',
                                            },
                                          };
                                        },
                                      },
                                    }}>
                                      {call.businesses && Array.isArray(call.businesses) && call.businesses.length > 0 ?
                                        call.businesses.map((item, index) => (
                                          <DropdownItem className="mb-2" key={index}>
                                            <div className="d-flex align-items-center">
                                              <span className="mr-4">
                                                {item}
                                              </span>
                                            </div>
                                          </DropdownItem>
                                        )) : <div className="bg-white p-3">
                                          <h2 className="text-secondary-dark">No Actions to Display</h2>
                                        </div>}
                                    </DropdownMenu>
                                  </UncontrolledDropdown>
                                </div>
                              </div>
                              <div className="px-2 ml-auto">
                                <Button
                                  size="sm"
                                  color="transparent"
                                  className="text-muted"
                                  title="Delete"
                                  onClick={() => {
                                    // this.props.delete_corporate_call(
                                    //   call.corp_code
                                    // );
                                    this.setState({
                                      deleteCorp_code: call.corp_code,
                                    }, () => {
                                      this.confirmDeleteModalToggle();
                                    });
                                  }}
                                >
                                  <FontAwesomeIcon icon="trash-alt" />{" "}
                                </Button>
                              </div>
                            </div>

                            <Row className="justify-content-center my-4">
                              <Col md={6}>
                                <div className="bg-primary p-3">
                                  <div className="d-flex mx-n3">
                                    <div className="px-3">
                                      <img width="50" src={"https://enterprise.staging.wikireviews.com/" + call.image_link} alt="" />
                                    </div>
                                    <div className="px-3 flex-fill">
                                      <div className="text-center">
                                        <h3 className="mb-3 text-dark">{call.headline}</h3>
                                        <h4 className="mb-3 text-white">{call.sub_headline}</h4>

                                        <Button color="dark">{call.button_typ}</Button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </Col>
                            </Row>
                          </div>
                        )};
                      })
                    ) : (
                        <div className="bg-white p-3">
                          <h2 className="text-secondary-dark">No Calls to Display</h2>
                        </div>
                      )}
                  </div>
                </div>

                {/* Get Started block */}
                <div className="bg-white mb-3" style={style}>
                  <div className="p-2">
                    <h2 className="text-secondary">Choose the action you want </h2>
                    <hr className="bg-secondary" />
                  </div>

                  <img className="w-100" src={'https://stagingdatawikireviews.s3.amazonaws.com/images/' + hoverImage} alt="Call to action" />

                  <div className="p-3">
                    <div className="text-primary-dark">
                      <p className="font-weight-bold">Available Actions</p>
                      <Row md={3} className="text-center mb-3" noGutters>
                        {bizCallonType && Array.isArray(bizCallonType) && bizCallonType.length > 0 ?
                          bizCallonType.map(item => (
                            <Col key={item.id}>
                              <div className="p-2 bg-light" onMouseEnter={() => this.onHover(item.action_type)}>
                                <span>{item.action_type}</span>
                              </div>
                            </Col>
                          )) : null}
                      </Row>
                      <Button block size="lg" color="primary" onClick={this.getStarted}>
                        Get Started
                      </Button>
                    </div>
                  </div>
                </div>

                {/* Setting up the call block */}
                <div className="bg-white mb-3" style={style2}>
                  <div className="p-2">
                    <h2 className="text-secondary">Setting up your call to action </h2>
                    <hr className="bg-secondary" />
                    <AvForm onSubmit={this.handleSubmit} ref="form">

                      <Row className="justify-content-center">
                        <Col xs={12}>
                          <div>
                            <div className="d-flex mx-n2 mb-2">
                              <div className="px-2">
                                <span className="text-primary-dark">
                                  Company list
                                </span>
                              </div>
                              <div className="px-2 ml-auto">
                                <Button size="sm" color="dark" onClick={this.handleAllChecked} checked={this.state.checkAll}>Select All</Button>
                              </div>
                            </div>
                            <div className="mb-3 p-2 border" style={{ maxHeight: '400px', overflow: 'auto' }}>
                              {allBranches && Array.isArray(allBranches) && allBranches.length > 0 ?
                                allBranches.map((item, index) => (
                                  <div className="d-flex align-items-center" key={index}>
                                    <CustomInput type="checkbox" id={"selectBranch1" + index} data-id={index} onChange={this.handleChange} value={item.id} name="select_multiple_companies" checked={this.state.rowState[index]} />
                                    <label htmlFor={"selectBranch1" + index} className="mb-0">
                                      <FontAwesomeIcon icon="info-circle" className="text-success mr-2" id={"branchInfo" + index} />
                                      <UncontrolledTooltip placement="top" target={"branchInfo" + index}>
                                        There is no call to action assigned
                                </UncontrolledTooltip>
                                      {item.name + ' ( ' + item.address1 + ', ' + item.state_fullname + ', ' + item.city + ', ' + item.country + ')'}
                                    </label>
                                  </div>
                                )) : <div className="bg-white p-3">
                                  <h2 className="text-secondary-dark">No Company to Display</h2>
                                </div>}
                            </div>
                            {this.state.isError ? (
                              <div className="invalid-feedback" style={{ 'display': 'block' }}>This Field is required</div>
                            ) : ''
                            }
                          </div>
                          <FormGroup row>
                            <Label sm={4} className="text-dark" size="sm">Choose Call To Action Button To Apply To Selected Listings Above</Label>
                            <Col sm={8}>
                              <AvField type="select" value={typeService} onChange={this.handleChange} name="type_of_service" className="bg-white" id="exampleSelectMulti" errorMessage="This Field is required" validate={{ required: { value: true } }}>
                                <option>Select Type</option>
                                {(typeof bizCallonType !== 'undefined' && bizCallonType.length > 0) ? bizCallonType.map(dropdown => {
                                  return <option key={dropdown.id} dropdown={dropdown} value={dropdown.action_type}>
                                    {dropdown.action_type}</option>;
                                }) : (<option> </option>)
                                }
                              </AvField>
                            </Col>
                          </FormGroup>
                          {this.state.isOpenRow ? (
                            <div>
                              {this.state.typeService == "Request A Quote" ? (
                                <FormGroup row>
                                  <Label sm={4} className="text-dark" size="sm">Response time</Label>
                                  <Col sm={8}>
                                    <AvField type="text" value={response_time} onChange={this.handleChange} id="response_time" name="response_time" placeholder="i.e. 2 hours" className="bg-white" maxLength="15" errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                    <FormText color="muted">Not more than 15 characters</FormText>
                                  </Col>
                                </FormGroup>
                              ) : ('')}
                              <FormGroup row>
                                <Label sm={4} className="text-dark" size="sm">Headline</Label>
                                <Col sm={8}>
                                  <AvField type="text" value={headline} onChange={this.handleChange} id="headline" name="headline" placeholder="i.e. Save 20%" className="bg-white" maxLength="15" errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                  <FormText color="muted">Not more than 15 characters</FormText>
                                </Col>
                              </FormGroup>
                              <FormGroup row>
                                <Label sm={4} className="text-dark" size="sm">Sub Headline</Label>
                                <Col sm={8}>
                                  <AvField type="text" value={sub_headline} onChange={this.handleChange} id="sub_headline" name="sub_headline" placeholder="i.e. if you book online" className="bg-white" maxLength="30" errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                  <FormText color="muted">Not more than 30 characters</FormText>
                                </Col>
                              </FormGroup>
                              <FormGroup row>
                                <Label sm={4} className="text-dark" size="sm">Linked URL</Label>
                                <Col sm={8}>
                                  <AvField type="url" value={linked_url} onChange={this.handleChange} id="linked_url" name="linked_url" placeholder="ex: http://yourwebsite.com" className="bg-white" errorMessage="This Field is required" validate={{ required: { value: true } }} />
                                </Col>

                              </FormGroup>
                            </div>
                          ) : ('')}
                          {this.state.headline != "" ? (
                            <Row className="my-4">
                              <Col md={5}>
                                <div className="bg-primary p-4">
                                  <div className="d-flex mx-n3">
                                    <div className="px-3">
                                      <img width="50" src={require('../../../assets/images/' + previewImage)} alt="" />
                                    </div>
                                    <div className="px-3 flex-fill">
                                      <div className="text-center">
                                        <h3 className="mb-3 text-dark">{headline}</h3>
                                        <h4 className="mb-3 text-white">{sub_headline}</h4>

                                        <Button color="dark">{typeService}</Button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </Col>
                            </Row>
                          ) : ('')}
                          <Row md={2} form>
                            <Col>
                              <Button block color="light" onClick={() => { this.handleOnBiz("c2a") }}>Cancel</Button>
                            </Col>
                            <Col>
                              <Button block color="primary">Apply To Selected Listings</Button>
                            </Col>
                          </Row>
                        </Col>
                      </Row>

                    </AvForm>
                  </div>
                </div>
              </TabPane>
            </TabContent>
          </div>
        </div>
        {/* Delete Confirmation Modal */}
        <Modal
          isOpen={this.state.confirmDeleteModal}
          toggle={this.confirmDeleteModalToggle}
          className="text-center"
        >
          <div className="bg-white modal-header">
            <h5 className="modal-title mx-auto text-dark">Confirmation</h5>
            <button
              className="btn btn-sm"
              aria-label="Close"
              onClick={this.confirmDeleteModalToggle}
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <ModalBody className="text-dark">
            Are you sure you want to delete this Call to Action?
          </ModalBody>
          <ModalFooter className="bg-white">
            <div className="text-center w-100">
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.confirmDeleteModalToggle}
              >
                Cancel
              </Button>
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.handleOnDeleteConfirmation}
              >
                Ok
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}
const mapState = (state) => ({
  account_list: state.user.account_list,
  biz_callon_details: state.user.biz_callon_details,
  corporate_call: state.user.corporate_call,
  all_branches: state.user.all_branches,
  biz_callon_type: state.user.biz_callon_type,
  delete_corporate_call: state.user.delete_corporate_call,
  post_corporate_call: state.user.post_corporate_call,
});

const mapProps = (dispatch) => {
  return {
    get_account_list: () => dispatch(get_account_list()),
    getallbranch: () => dispatch(getallbranch()),
    get_biz_callon_type: () => dispatch(get_biz_callon_type()),
    corporate_call_to_action: (data) => dispatch(corporate_call_to_action(data)),
    get_corporate_call_list: (listing_id) => dispatch(get_corporate_call_list(listing_id)),
    delete_corporate_call: (corp_code) => dispatch(delete_corporate_call(corp_code)),
    get_biz_callon_details: (data1) => dispatch(get_biz_callon_details(data1)),
  }
}

export default connect(mapState, mapProps)(AccountSettings);

