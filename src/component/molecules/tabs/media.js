import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Button,
  ButtonGroup,
  Row,
  Col,
  FormGroup,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  Nav,
  NavItem,
  TabContent,
  TabPane,
  NavLink,
  Progress,
} from "reactstrap";
import { get_all_media_contents } from "../../../actions/user";
import { callApi } from "../../../utils/apiCaller";
import {
  get_album_types_list,
  get_album_type_data,
  admin_media_apload,
  delete_selected_gallery_media,
  delete_selected_gallery_media_Upload,
} from "../../../actions/user";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ReactHtmlParser from "react-html-parser";
import Label from "reactstrap/lib/Label";
// import { Button, ButtonGroup, NavLink, Nav, NavItem, TabContent, TabPane, Row, Col } from "reactstrap";

class MediaTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ViewMediaType: "image",
      imageUploadedBy: "owner",
      mediaData: {},
      contentType: "uploaded_by_owner_images",
      uploadMediaModal: false,
      selectedUpMediaType: "upload",
      showGalleryType: "images",
      uploadMedia: {
        mediaType: "image",
        albumType: "",
        pageNo: 1,
        albumTypesList: [],
        albumTypeData: {},
        selectedMedia: [],
        selectedMediaIds: [],
        embedLinks: {},
        uploadFiles: [],
        uploadedFiles: [],
        progress: 0,
      },
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.get_media_data &&
      nextProps.get_media_data.media_dict &&
      Object.keys(nextProps.get_media_data.media_dict).length > 0
    ) {
      this.setState({ mediaData: nextProps.get_media_data.media_dict });
    }
    if (nextProps.album_types_list && nextProps.album_types_list.length > 0) {
      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          albumTypesList: nextProps.album_types_list,
        },
        //uploadMediaModal: true,
        selectedUpMediaType: "upload",
        showGalleryType: "images",
      });
    }

    if (
      nextProps.album_type_data &&
      Object.keys(nextProps.album_type_data).length > 0
    ) {
      this.setState({
        ...this.state,
        //selectedUpMediaType: "gallery",
        uploadMedia: {
          ...this.state.uploadMedia,
          albumTypeData: nextProps.album_type_data,
        },
      });
    }
  }

  componentDidMount() {
    this.props.get_all_media_contents({
      media_type: "image",
      user_type: "owner",
    });
  }

  checkValidItems = () => {
    let { ViewMediaType, imageUploadedBy, videoUploadedBy } = this.state;
    if (ViewMediaType == "image" && imageUploadedBy == "owner") {
      this.setState({ contentType: "uploaded_by_owner_images" });
    }

    if (ViewMediaType == "image" && imageUploadedBy == "others") {
      this.setState({ contentType: "uploaded_by_others_images" });
    }

    if (ViewMediaType == "video" && imageUploadedBy == "owner") {
      this.setState({ contentType: "uploaded_by_owner_videos" });
    }

    if (ViewMediaType == "video" && imageUploadedBy == "others") {
      this.setState({ contentType: "uploaded_by_others_videos" });
    }
    this.setState({ mediaData: {} }, () => {
      this.props.get_all_media_contents({
        media_type: ViewMediaType,
        user_type: imageUploadedBy,
      });
    });
  };

  renderItems = ({ type }) => {
    // uploaded_by_owner_image
    let { mediaData, ViewMediaType } = this.state;
    let subItems = mediaData[type];
    let dataItems = [];
    if (mediaData && Array.isArray(subItems) && subItems.length > 0) {
      subItems.map((item) => {
        dataItems.push(
          <Col className="mb-3">
            <div className="gallery-media">
              <img src={item.url} alt={item.category} />
            </div>
            <span className="font-weight-bold fs-14">{item.category}</span>
          </Col>
        );
      });
    }
    return dataItems;
  };

  renderCount = ({ type }) => {
    let { mediaData } = this.state;
    let subItems = mediaData[type];
    let countItems = 0;
    if (mediaData && Array.isArray(subItems) && subItems.length > 0) {
      countItems = subItems.length;
    }
    return countItems;
  };

  handleOnFileUploadChange = (event) => {
    let uploadFiles = event.target.files;
    let showFiles = [];
    for (const key of Object.keys(uploadFiles)) {
      showFiles.push({ id: "", url: URL.createObjectURL(uploadFiles[key]) });
    }
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        uploadFiles: showFiles,
        progress: 0,
      },
    });
    let progressPart = 100 / showFiles.length;
    let progress = 0;
    for (const key of Object.keys(uploadFiles)) {
      let data = new FormData();
      data.append("file", uploadFiles[key]);

      callApi(
        `/upload/multiuploader/?album=&instance=bizimage&image=undefined&doc=undefined`,
        "POST",
        data,
        true
      ).then((response) => {
        this.handleOnClickSelectGalleryMedia(response);
        if (showFiles.length === 1 || key === showFiles.length - 1) {
          progress = 100;
        } else {
          progress = progress + progressPart;
        }
        showFiles[key].id = response.id;
        this.setState({
          ...this.state,
          uploadMedia: {
            ...this.state.uploadMedia,
            progress: progress,
            uploadedFiles: [...this.state.uploadMedia.uploadedFiles, response],
            uploadedFiles: showFiles,
          },
        });
      });
    }
  };
  handleOnClickRemoveSelectedMedia = (id) => () => {
    let removeMediaIds = [];
    removeMediaIds.push(id);
    this.props.delete_selected_gallery_media_Upload(removeMediaIds);
    let uploadFiles = this.state.uploadMedia.uploadFiles.filter(
      (file) => file.id !== id
    );
    let uploadedFiles = this.state.uploadMedia.uploadedFiles.filter(
      (file) => file.id !== id
    );
    let selectedMediaIds = this.state.uploadMedia.selectedMediaIds.filter(
      (item) => item !== id
    );
    let selectedMedia = this.state.uploadMedia.selectedMedia.filter(
      (file) => file.id !== id
    );
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: selectedMedia,
        selectedMediaIds: selectedMediaIds,
        uploadedFiles: uploadedFiles,
        uploadFiles: uploadFiles,
      },
    });
  };
  handleOnClickRemoveSelectedGalleryMedia = (media) => {
    let removeMediaIds = [];
    removeMediaIds.push(media.id);
    let index = this.state.uploadMedia.selectedMediaIds.indexOf(media.id);
    let newSelectedMediaIds;
    let newSelectedMedia;
    if (index !== -1) {
      newSelectedMediaIds = this.state.uploadMedia.selectedMediaIds.filter(
        (item) => item !== media.id
      );
      newSelectedMedia = this.state.uploadMedia.selectedMedia.filter(
        (item) => item.id !== media.id
      );
      this.props.delete_selected_gallery_media(removeMediaIds);
      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          selectedMedia: newSelectedMedia,
          selectedMediaIds: newSelectedMediaIds,
        },
      });
    }
  };
  handleOnClickSelectGalleryMedia = (media) => {
    let index = this.state.uploadMedia.selectedMediaIds.indexOf(media.id);
    let newSelectedMediaIds;
    let newSelectedMedia;
    if (index !== -1) {
      newSelectedMediaIds = this.state.uploadMedia.selectedMediaIds.filter(
        (item) => item !== media.id
      );
      newSelectedMedia = this.state.uploadMedia.selectedMedia.filter(
        (item) => item.id !== media.id
      );
    } else {
      newSelectedMediaIds = [
        ...this.state.uploadMedia.selectedMediaIds,
        media.id,
      ];
      newSelectedMedia = [...this.state.uploadMedia.selectedMedia, media];
    }
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: newSelectedMedia,
        selectedMediaIds: newSelectedMediaIds,
      },
    });
  };
  handleOnClickGalleryType = (type) => {
    let mediaType = "";
    if (type === "images") {
      mediaType = "image";
    } else if (type === "videos") {
      mediaType = "video";
    }
    this.props.get_album_type_data(mediaType, "", 1);
    this.setState({
      ...this.state,
      selectedUpMediaType: "gallery",
      showGalleryType: type,
      uploadMedia: {
        ...this.state.uploadMedia,
        mediaType: mediaType,
        albumType: "",
        pageNo: 1,
        //selectedMedia: [],
        //selectedMediaIds: [],
      },
    });
  };
  handleOnClickAlbumTypeChange = (e) => {
    this.props.get_album_type_data("image", e.target.value, 1);
    this.setState({
      ...this.state,
      selectedUpMediaType: "gallery",
      showGalleryType: "images",
      uploadMedia: {
        ...this.state.uploadMedia,
        mediaType: "image",
        albumType: e.target.value,
        pageNo: 1,
        //selectedMedia: [],
        //selectedMediaIds: [],
      },
    });
  };
  handleOnClickSelectedUploadMediaType = (type) => {
    if (type === "gallery") {
      this.props.get_album_type_data("image", "", 1);
      this.setState({
        ...this.state,
        selectedUpMediaType: type,
        showGalleryType: "images",
        uploadMedia: {
          ...this.state.uploadMedia,
          mediaType: "image",
          albumType: "",
          pageNo: 1,
          //selectedMedia: [],
          //selectedMediaIds: [],
          embedLinks: {},
          progress: 0,
        },
      });
    } else if (type === "upload") {
      this.setState({
        ...this.state,
        selectedUpMediaType: type,
        uploadMedia: {
          ...this.state.uploadMedia,
          //selectedMedia: [],
          //selectedMediaIds: [],
          embedLinks: {},
          progress: 0,
        },
      });
    } else if (type === "embed") {
      let embedLinks = {
        0: "",
        1: "",
      };
      this.setState({
        ...this.state,
        selectedUpMediaType: type,
        uploadMedia: {
          ...this.state.uploadMedia,
          selectedMedia: [],
          selectedMediaIds: [],
          embedLinks: embedLinks,
        },
      });
    }
  };
  embedLinkOnChange = (id) => (e) => {
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        embedLinks: {
          ...this.state.uploadMedia.embedLinks,
          [id]: e.target.value,
        },
      },
    });
  };
  mapEmbedLinks = () => {
    let embedKeys = Object.keys(this.state.uploadMedia.embedLinks);
    return embedKeys.map((emKey) => {
      return (
        <div className="d-flex mx-n2 mb-2" key={emKey}>
          <div className="flex-fill px-2">
            <Input
              type="url"
              bsSize="sm"
              className="bg-white"
              value={this.state.uploadMedia.embedLinks[emKey]}
              onChange={this.embedLinkOnChange(emKey)}
              placeholder="Embeded link"
            />
          </div>
          <div className="px-2">
            <Button
              title="Remove"
              color="danger"
              size="sm"
              hidden={emKey <= 1 ? true : false}
              onClick={() => {
                this.deleteEmbedLinkRow(emKey);
              }}
            >
              <FontAwesomeIcon icon="minus" />
            </Button>
          </div>
        </div>
      );
    });
  };
  deleteEmbedLinkRow = (emKey) => {
    let embedLinks = this.state.uploadMedia.embedLinks;
    if (Object.keys(embedLinks).indexOf(emKey) !== -1) {
      delete embedLinks[emKey];
      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          embedLinks: embedLinks,
        },
      });
    }
  };
  addEmbedlinkRow = () => {
    let newEmbedLinks = {};
    for (let i = 2; i < 5; i += 1) {
      if (
        Object.keys(this.state.uploadMedia.embedLinks).indexOf(i.toString()) ===
        -1
      ) {
        newEmbedLinks[i] = "";
        break;
      }
    }
    if (Object.keys(newEmbedLinks).length > 0) {
      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          embedLinks: {
            ...this.state.uploadMedia.embedLinks,
            ...newEmbedLinks,
          },
        },
      });
    }
  };
  handleOnClickUploadMedia = async () => {
    this.props.get_album_types_list();
    await this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        mediaType: "image",
        albumType: "",
        pageNo: 1,
        albumTypeData: {},
        embedLinks: {},
        progress: 0,
        uploadFiles: [],
        uploadedFiles: [],
      },
      uploadMediaModal: true,
      selectedUpMediaType: "upload",
      showGalleryType: "images",
    });
  };
  uploadMediaModalToggle = () => {
    this.setState({
      ...this.state,
      uploadMediaModal: !this.state.uploadMediaModal,
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: [],
        selectedMediaIds: [],
        mediaType: "image",
        albumType: "",
        pageNo: 1,
        albumTypeData: {},
        //albumTypesList: [],
        embedLinks: {},
        uploadedFiles: [],
        uploadFiles: [],
      },
      selectedUpMediaType: "upload",
      showGalleryType: "images",
    });
  };
  insertEmbedLinks = () => {
    //let post = this.state.addPost;
    let embedValues = Object.values(this.state.uploadMedia.embedLinks).filter(
      (item) => item !== ""
    );
    //if (post.body && post.body.replace(/(<([^>]+)>)/gi, "") !== "") {
    // if (embedValues.length > 0) {
    //   embedValues = embedValues.map((item) => "<p>" + item + "</p>");
    //   post.body = post.body + embedValues.join("");
    //   post.body_preview = post.body;
    // }
    // this.setState({
    //   ...this.state,
    //   addPost: post
    // });
    //} else {
    if (embedValues.length > 0) {
      embedValues = embedValues.map((item) => "<p>" + item + "</p>");
      let newPost = {
        video: embedValues.join("\n"),
        exclude_list: [],
        question_rating_category: [],
        messagemedia_set: [],
      };

      //this.props.add_my_post(newPost, this.state.filterType, this.state.pageNo);

      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          selectedMedia: [],
          selectedMediaIds: [],
          embedLinks: {},
        },
        uploadMediaModal: false,
      });
    }
    //}
  };
  truncate = (filenameString) => {
    // let split = filenameString.split(".");
    let filename = filenameString.substr(0, filenameString.lastIndexOf("."));
    let extension = filenameString.substr(
      filenameString.lastIndexOf("."),
      filenameString.length - 1
    );
    let partial = filename.substring(filename.length - 3, filename.length);
    filename = filename.substring(0, 15);
    return filename + "..." + partial + extension;
  };

  handleInsertMedia = () => {
    const { admin_media_apload, get_all_media_contents } = this.props;
    let data = {
      instanceType: "bizimage",
      multiuploaderfile: this.state.uploadMedia.selectedMediaIds,
      taxonomy_id: "",
      type: "media",
    };
    admin_media_apload(data);
    get_all_media_contents({
      media_type: "image",
      user_type: "owner",
    });
  };

  render() {
    let { mediaData, contentType } = this.state;

    // uploaded_by_others_image: []
    // uploaded_by_others_video: []
    // uploaded_by_owner_image: (618) []
    // uploaded_by_owner_video: []
    // category // thumbnail

    return (
      <React.Fragment>
        <div className="text-primary">
          <div>
            <Nav tabs className="mb-3 non-decorated-alt">
              <NavItem>
                <NavLink
                  // href="#"
                  active={this.state.ViewMediaType === "image"}
                  onClick={() => {
                    this.setState({ ViewMediaType: "image" }, () => {
                      this.checkValidItems()
                    });
                  }}
                >
                  {"image"}
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  // href="#"
                  active={this.state.ViewMediaType === "video"}
                  onClick={() => {
                    this.setState({ ViewMediaType: "video" }, () => {
                      this.checkValidItems()
                    });
                  }}
                >
                  {"video"}
                </NavLink>
              </NavItem>
            </Nav>

            <TabContent activeTab={this.state.ViewMediaType}>
              <TabPane tabId="image">
                <div className="mb-3">
                  <ButtonGroup className="type-filter flex-wrap" size="sm">
                    <div className="item d-flex align-items-center">
                      <Button
                        color="transparent"
                        onClick={() =>
                          this.setState({ imageUploadedBy: "owner" }, () =>
                            this.checkValidItems()
                          )
                        }
                        active={this.state.imageUploadedBy === "owner"}
                      >
                        Uploaded by you
                    </Button>
                    </div>
                    <div className="item d-flex align-items-center">
                      <Button
                        color="transparent"
                        onClick={() =>
                          this.setState({ imageUploadedBy: "others" }, () =>
                            this.checkValidItems()
                          )
                        }
                        active={this.state.imageUploadedBy === "others"}
                      >
                        Uploaded by users
                    </Button>
                    </div>
                  </ButtonGroup>
                </div>
                {/* If no image */}

                {/* If image */}
                <div className="bg-white p-3 ">
                  <div className="text-right mb-2">
                    <span className="small text-muted">
                      Total{" "}
                      <span>
                        {this.renderCount({ type: contentType }, () =>
                          this.checkValidItems()
                        )}
                      </span>{" "}
                      image found
                    </span>
                  </div>
                  <Row xs={2} sm={3} md={4} lg={5} className="scroll-items">
                    {this.renderItems({ type: contentType }, () =>
                      this.checkValidItems()
                    )}
                  </Row>
                  <hr />
                  <div className="d-flex mx-n2">
                    <div className="px-2">
                      <Button
                        color="primary"
                        onClick={this.handleOnClickUploadMedia}
                      >
                        Upload Media
                      </Button>
                    </div>
                    {/* <div className="px-2 ml-auto">
                                            <Button color="link" className="text-secondary">Delete image</Button>
                                        </div> */}
                  </div>
                </div>
              </TabPane>
              <TabPane tabId="video">
                <div className="mb-3">
                  <ButtonGroup className="type-filter flex-wrap" size="sm">
                    <div className="item d-flex align-items-center">
                      <Button
                        color="transparent"
                        onClick={() =>
                          this.setState({ imageUploadedBy: "owner" }, () => {
                            this.checkValidItems()
                          })
                        }
                        active={this.state.imageUploadedBy === "owner"}
                      >
                        Uploaded by you
                    </Button>
                    </div>
                    <div className="item d-flex align-items-center">
                      <Button
                        color="transparent"
                        onClick={() =>
                          this.setState({ imageUploadedBy: "others" }, () => {
                            this.checkValidItems()
                          })
                        }
                        active={this.state.imageUploadedBy === "others"}
                      >
                        Uploaded by users
                    </Button>
                    </div>
                  </ButtonGroup>
                </div>

                {/* If no video */}
                <div className="bg-white p-3" hidden>
                  <h2 className="text-secondary-dark">No video to display.</h2>
                </div>

                <div className="bg-white p-3">
                  <div className="text-right mb-2">
                    <span className="small text-muted">
                      Total{" "}
                      <span>
                        {this.renderCount({ type: contentType }, () =>
                          this.checkValidItems()
                        )}
                      </span>{" "}
                      video found
                    </span>
                  </div>
                  <Row xs={2} sm={3} md={4} lg={5}>
                    {this.renderItems({ type: contentType }, () =>
                      this.checkValidItems()
                    )}
                  </Row>
                  <hr />
                  <div className="d-flex mx-n2">
                    <div className="px-2">
                      <Button
                        onClick={this.uploadMediaModalToggle}
                        color="primary"
                      >
                        Upload Media
                      </Button>
                    </div>
                    {/* <div className="px-2 ml-auto">
                                            <Button color="link" className="text-secondary">Delete video</Button>
                                        </div> */}
                  </div>
                </div>
              </TabPane>
            </TabContent>
          </div>

          <Modal
            isOpen={this.state.uploadMediaModal}
            toggle={this.uploadMediaModalToggle}
            size="lg"
          >
            <ModalHeader toggle={this.uploadMediaModalToggle}>
              {"Upload Media"}
            </ModalHeader>
            <ModalBody className="p-3">
              <Row>
                <Col xs={3}>
                  <Nav pills className="flex-column">
                    <NavItem className="text-center">
                      <NavLink
                        href="#"
                        active={this.state.selectedUpMediaType === "upload"}
                        onClick={() => {
                          this.handleOnClickSelectedUploadMediaType("upload");
                        }}
                      >
                        {"Upload"}
                      </NavLink>
                    </NavItem>
                    <NavItem className="text-center">
                      <NavLink
                        href="#"
                        active={this.state.selectedUpMediaType === "gallery"}
                        onClick={() => {
                          this.handleOnClickSelectedUploadMediaType("gallery");
                        }}
                      >
                        {"Gallery"}
                      </NavLink>
                    </NavItem>
                    <NavItem className="text-center">
                      <NavLink
                        href="#"
                        active={this.state.selectedUpMediaType === "embed"}
                        onClick={() => {
                          this.handleOnClickSelectedUploadMediaType("embed");
                        }}
                      >
                        {"Embed"}
                      </NavLink>
                    </NavItem>
                  </Nav>
                </Col>
                <Col xs={9}>
                  <TabContent activeTab={this.state.selectedUpMediaType}>
                    <TabPane tabId="upload">
                      <div
                        className="mb-3 type-file-block"
                        hidden={
                          this.state.uploadMedia.uploadFiles &&
                            this.state.uploadMedia.uploadFiles.length > 0
                            ? true
                            : false
                        }
                      >
                        <Input
                          type="file"
                          name="upload_media_file"
                          id="uploadFilesFromSystem"
                          accept="image/*, video/*"
                          onChange={this.handleOnFileUploadChange}
                          multiple
                        />
                        <Label for="uploadFilesFromSystem">
                          <FontAwesomeIcon
                            icon="upload"
                            size="2x"
                            className="text-primart"
                          />
                          <br />
                          {"Upload Files From Computer"}
                          <br />
                          <small className="text-muted">
                            {"(or Drag files here)"}
                          </small>
                        </Label>
                      </div>
                      {this.state.uploadMedia.uploadFiles.length > 0 && (
                        <Row className="mb-3" form>
                          <Col xs={12}>
                            <div
                              style={{
                                maxWidth: "120px",
                                margin: "0 1rem 1rem auto",
                              }}
                            >
                              <div className="text-center mb-1 small">
                                {this.state.uploadMedia.progress === 100 ? (
                                  <div className="text-success">
                                    <FontAwesomeIcon
                                      icon="check-circle"
                                      className="mr-1"
                                    />{" "}
                                    {"Uploaded"}
                                  </div>
                                ) : (
                                    <div>
                                      {"Uploading"}{" "}
                                      <span className="text-success font-weight-bold ff-base">
                                        {this.state.uploadMedia.progress.toFixed(
                                          0
                                        )}
                                        {"%"}
                                      </span>
                                    </div>
                                  )}
                              </div>
                              <Progress
                                value={this.state.uploadMedia.progress}
                                style={{ height: "8px" }}
                              ></Progress>
                            </div>
                          </Col>
                          {this.state.uploadMedia.uploadFiles.map((file, index) => {
                            return (
                              <Col key={index} xs="auto">
                                <div className="d-flex pr-3 pt-3">
                                  <div>
                                    <div
                                      className="selectable-media"
                                      style={{ cursor: "default" }}
                                    >
                                      <div className="gallery-media">
                                        <img src={file.url} />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="mx-n3 mt-n3">
                                    <Button
                                      color="dark"
                                      size="sm"
                                      title="Remove Media"
                                      hidden={file.id === "" ? true : false}
                                      onClick={this.handleOnClickRemoveSelectedMedia(
                                        file.id
                                      )}
                                    >
                                      <FontAwesomeIcon icon="minus" />{" "}
                                    </Button>
                                  </div>
                                </div>
                              </Col>
                            );
                          })}
                          <Col xs="auto">
                            <div className="d-flex pt-3">
                              <div className="selectable-media" hidden>
                                <Label
                                  for="uploadFilesFromSystemMini"
                                  className="gallery-media"
                                  style={{
                                    borderStyle: "dashed",
                                    cursor: "pointer",
                                  }}
                                >
                                  <div className="d-flex h-100 align-items-center justify-content-center">
                                    <span className="fs-14">Upload More</span>
                                  </div>
                                </Label>
                                <Input
                                  type="file"
                                  name="upload_media_file"
                                  id="uploadFilesFromSystemMini"
                                  accept="image/*, video/*"
                                  onChange={this.handleOnFileUploadChange}
                                  multiple
                                  style={{ display: "none" }}
                                />
                              </div>
                            </div>
                          </Col>
                        </Row>
                      )}
                      <div className="d-flex mx-n2">
                        <div className="px-2">
                          <Button
                            color="primary"
                            size="sm"
                            className="mw"
                            onClick={this.uploadMediaModalToggle}
                          >
                            {"Cancel"}
                          </Button>
                        </div>
                        <div className="px-2 ml-auto">
                          <Button
                            color="primary"
                            size="sm"
                            className="mw"
                            onClick={() => {
                              this.setState({
                                ...this.state,
                                uploadMediaModal: false,
                              });
                              this.handleInsertMedia();
                            }}
                          >
                            {"Insert"}
                          </Button>
                        </div>
                      </div>
                    </TabPane>
                    <TabPane tabId="gallery">
                      <div className="mb-2">
                        <Nav tabs className="d-inline-flex mb-0">
                          <NavItem>
                            <NavLink
                              href="#"
                              active={this.state.showGalleryType === "images"}
                              onClick={() => {
                                this.handleOnClickGalleryType("images");
                              }}
                            >
                              {" Image Gallery"}
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              href="#"
                              active={this.state.showGalleryType === "videos"}
                              onClick={() => {
                                this.handleOnClickGalleryType("videos");
                              }}
                            >
                              Video Gallery
                            </NavLink>
                          </NavItem>
                        </Nav>
                      </div>
                      <TabContent activeTab={this.state.showGalleryType}>
                        <TabPane tabId="images">
                          <div>
                            <FormGroup className="form-row justify-content-end">
                              <Col xs="auto">
                                <Label className="small" for="selectAlbumType">
                                  {"Album Type:"}
                                </Label>
                              </Col>
                              <Col xs="auto">
                                <Input
                                  bsSize="sm"
                                  type="select"
                                  name="album_type_select"
                                  id="selectAlbumType"
                                  defaultValue={
                                    this.state.uploadMedia.albumType
                                  }
                                  onChange={this.handleOnClickAlbumTypeChange}
                                >
                                  <option value="">All</option>
                                  {this.state.uploadMedia.albumTypesList
                                    .length > 0 &&
                                    this.state.uploadMedia.albumTypesList.map(
                                      (type) => (
                                        <option value={type.category}>
                                          {type.category}
                                        </option>
                                      )
                                    )}
                                </Input>
                              </Col>
                            </FormGroup>
                          </div>
                          <div
                            className="px-3 my-3"
                            style={{ maxHeight: "400px", overflow: "auto" }}
                          >
                            {this.state.uploadMedia.albumTypeData &&
                              this.state.uploadMedia.albumTypeData.count > 0 ? (
                                <Row xs={2} md={3} lg={4}>
                                  {this.state.uploadMedia.albumTypeData.results.map(
                                    (media) => {
                                      return (
                                        <Col className="mb-3" key={media.id}>
                                          {media.type === "image" && (
                                            <div
                                              className={`selectable-media ${this.state.uploadMedia.selectedMediaIds.indexOf(
                                                media.id
                                              ) !== -1
                                                ? "selected"
                                                : ""
                                                }`}
                                              onClick={() =>
                                                this.handleOnClickSelectGalleryMedia(
                                                  media
                                                )
                                              }
                                            >
                                              <div className="gallery-media">
                                                <img
                                                  src={media.file}
                                                  alt={
                                                    media.filename.length < 20
                                                      ? media.filename
                                                      : this.truncate(
                                                        media.filename
                                                      )
                                                  }
                                                />
                                              </div>
                                              <div className="font-weight-bold fs-12 text-secondary-dark mb-0 text-truncate">
                                                {media.category
                                                  ? media.category
                                                  : "No Category"}
                                              </div>
                                              <div className="font-weight-normal fs-15 d-block">
                                                {media.caption
                                                  ? media.caption
                                                  : "No Caption"}
                                              </div>
                                            </div>
                                          )}
                                        </Col>
                                      );
                                    }
                                  )}
                                </Row>
                              ) : (
                                <div className="bg-white p-3">
                                  <h2 className="text-secondary-dark">
                                    No images
                                </h2>
                                </div>
                              )}
                          </div>
                          <div className="d-flex mx-n2">
                            <div className="px-2">
                              <Button
                                color="primary"
                                size="sm"
                                className="mw"
                                onClick={this.uploadMediaModalToggle}
                              >
                                {"Cancel"}
                              </Button>
                            </div>
                            <div className="px-2 ml-auto">
                              <Button
                                color="primary"
                                size="sm"
                                className="mw"
                                onClick={() => {
                                  this.setState({
                                    ...this.state,
                                    uploadMediaModal: false,
                                  });
                                }}
                              >
                                {"Insert"}
                              </Button>
                            </div>
                          </div>
                        </TabPane>
                        <TabPane tabId="videos">
                          <div
                            className="px-3 my-3"
                            style={{ maxHeight: "400px", overflow: "auto" }}
                          >
                            {this.state.uploadMedia.albumTypeData &&
                              this.state.uploadMedia.albumTypeData.count > 0 ? (
                                <Row xs={2} md={3} lg={4}>
                                  {this.state.uploadMedia.albumTypeData.results.map(
                                    (media) => {
                                      return (
                                        <Col className="mb-3" key={media.id}>
                                          {media.type === "video" && (
                                            <div
                                              className={`selectable-media ${this.state.uploadMedia.selectedMediaIds.indexOf(
                                                media.id
                                              ) !== -1
                                                ? "selected"
                                                : ""
                                                }`}
                                              onClick={() =>
                                                this.handleOnClickSelectGalleryMedia(
                                                  media
                                                )
                                              }
                                            >
                                              <div className="gallery-media">
                                                <img
                                                  src={media.thumbnail}
                                                  alt={
                                                    media.filename.length < 20
                                                      ? media.filename
                                                      : this.truncate(
                                                        media.filename
                                                      )
                                                  }
                                                />
                                              </div>
                                              <div className="font-weight-bold fs-12 text-secondary-dark mb-0 text-truncate">
                                                {media.category
                                                  ? media.category
                                                  : "No Category"}
                                              </div>
                                              <div className="font-weight-normal fs-15 d-block">
                                                {media.caption
                                                  ? media.caption
                                                  : "No Caption"}
                                              </div>
                                            </div>
                                          )}
                                        </Col>
                                      );
                                    }
                                  )}
                                </Row>
                              ) : (
                                <div className="bg-white p-3">
                                  <h2 className="text-secondary-dark">
                                    {"No videos"}
                                  </h2>
                                </div>
                              )}
                          </div>
                          <div className="d-flex mx-n2">
                            <div className="px-2">
                              <Button
                                color="primary"
                                size="sm"
                                className="mw"
                                onClick={this.uploadMediaModalToggle}
                              >
                                {"Cancel"}
                              </Button>
                            </div>
                            <div className="px-2 ml-auto">
                              <Button
                                color="primary"
                                size="sm"
                                className="mw"
                                onClick={() => {
                                  this.setState({
                                    ...this.state,
                                    uploadMediaModal: false,
                                  });
                                }}
                              >
                                {"Insert"}
                              </Button>
                            </div>
                          </div>
                        </TabPane>
                      </TabContent>
                    </TabPane>
                    <TabPane tabId="embed">
                      <div className="text-muted mb-2">
                        Submit the link for the image or video you want to embed{" "}
                      </div>
                      <div className="mb-2">
                        {/* Repeat this while adding */}
                        {this.mapEmbedLinks()}
                      </div>
                      <div className="d-flex mx-n2 mb-2">
                        <div className="px-2 ml-auto">
                          <Button
                            title="Add"
                            color="primary"
                            size="sm"
                            onClick={() => {
                              this.addEmbedlinkRow();
                            }}
                          >
                            <FontAwesomeIcon icon="plus" />
                          </Button>
                        </div>
                      </div>
                      <div className="d-flex mx-n2">
                        <div className="px-2">
                          <Button
                            color="primary"
                            size="sm"
                            className="mw"
                            onClick={this.uploadMediaModalToggle}
                          >
                            {"Cancel"}
                          </Button>
                        </div>
                        <div className="px-2 ml-auto">
                          <Button
                            color="primary"
                            size="sm"
                            className="mw"
                            onClick={() => {
                              this.insertEmbedLinks();
                            }}
                          >
                            {"Insert"}
                          </Button>
                        </div>
                      </div>
                    </TabPane>
                  </TabContent>
                </Col>
              </Row>
            </ModalBody>
          </Modal>
        </div>
      </React.Fragment>
    );
  }
}

const mapState = (state) => ({
  get_media_data: state.user.get_media_data,
  album_types_list: state.user.album_types_list,
  album_type_data: state.user.album_type_data,
});

const mapProps = (dispatch) => ({
  get_all_media_contents: ({ media_type, user_type }) =>
    dispatch(get_all_media_contents({ media_type, user_type })),
  get_album_types_list: () => dispatch(get_album_types_list()),
  get_album_type_data: (mediaType, albumType, pageNo) =>
    dispatch(get_album_type_data(mediaType, albumType, pageNo)),
  delete_selected_gallery_media: (removeMediaIds) =>
    dispatch(delete_selected_gallery_media(removeMediaIds)),
  delete_selected_gallery_media_Upload: (id) =>
    dispatch(delete_selected_gallery_media_Upload(id)),
  admin_media_apload: (params) => dispatch(admin_media_apload(params)),
});

export default connect(mapState, mapProps)(MediaTab);
