import React, { Component } from "react";
import {
  FormGroup,
  Input,
  Button,
  ButtonGroup,
  Media,
  Modal,
  ModalBody,
  ModalFooter,
  Badge,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  get_qa_data,
  update_qa_reply,
  delete_questions_corporateQuestiosn,
  add_comment_feedback_qa,
  get_tabs_data_entity_id,
  get_branch_data_by_circle_click
} from "../../../actions/user";
import { connect } from "react-redux";
import Filteration from '../../atoms/Filteration';


class QA extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ViewQAType: "answered",
      answeredQA: "",
      pendingQA: "",
      body: "",
      editpost: true,
      id: "",
      messagemedia_set: [],
      textBox: "",
      currentMessageID: "",
      updateComment: "",
      replyComment: "",
      addComment: {},
      confirmDeleteModal: false,
      qa_answered_count: null,
      qa_pending_count: null,
	  sortyBy:"Latest",
	  qa_listCircle:[]
    };
  }

  async componentWillReceiveProps(nextProps) {
    //get qa props
    const {
      qa_questions_answered,
      qa_questions_pending,
      corporate_qa_count,
    } = nextProps;
    if (
      qa_questions_answered &&
      Object.keys(qa_questions_answered).length > 0
    ) {
      await this.setState({
        answeredQA: qa_questions_answered,
      });
    }

    if (
      corporate_qa_count !== null &&
      Object.keys(corporate_qa_count).length > 0
    ) {
      this.setState({
        qa_answered_count: corporate_qa_count.qa_answered_count,
        qa_pending_count: corporate_qa_count.qa_pending_count,
      });
    }

    if (qa_questions_pending && Object.keys(qa_questions_pending).length > 0) {
      await this.setState({
        pendingQA: qa_questions_pending,
      });
    }
	if(nextProps.qa_list && nextProps.qa_list.results && Array.isArray(nextProps.qa_list.results) && nextProps.qa_list.results.length > 0){
		this.setState({
			qa_listCircle: nextProps.qa_list,
		  }); 
	  }
		
  }


  async componentDidMount() {
    const { get_qa_data, branch_id, circleClick, mapBranchClickData } = this.props;

    if (circleClick && circleClick.topBarQA === false) {
      // if (branch_id !== null) {
      //   this.props.get_tabs_data_entity_id(branch_id, 'answered')
      //   this.props.get_tabs_data_entity_id(branch_id, 'pending')
      // } else 
      if (this.props.tabName === 'Q&A' && mapBranchClickData !== true) {
        await get_qa_data("answered");
        await get_qa_data("pending");
      } else {
        const { dotSelectedType, dotSelectedColor, dotSelectedName, dotSelectedDays, dotSelectedqueryType, dotSelectedBranchId } = this.props;
        let data_type = { 'reviews': '4', 'message': '5', 'qa': '7', 'feedback': '6', 'disputed_Review': '4' }
        this.props.get_branch_data_by_circle_click({
          color: dotSelectedColor,
          type: dotSelectedType,
          days: dotSelectedDays,
          queryType: dotSelectedqueryType,
          branchID: dotSelectedBranchId,
        });
      }
    }
  }

  componentWillUnmount() {
    if (this.props.circleClick && this.props.circleClick.topBarQA) {
      this.props.circleClick.topBarQA = false;
      this.props.circleClick.topBarQAColor = null;
      this.props.user.qa_list = null
    }

  }

  handleChange = async (evt) => {
    await this.setState({
      [evt.target.name]: evt.target.value,
    });
  };
  handleAddComment = (id) => (e) => {
    this.setState({
      addComment: {
        ...this.state.addComment,
        [id]: e.target.value,
      },
    });
  };

  handleDeleteConfirmation = async () => {
    const { delete_questions_corporateQuestiosn, get_qa_data } = this.props;
    const data = {
      entityid: this.state.currentMessageID,
      entity: "question",
    };
    delete_questions_corporateQuestiosn(data);
    let self = this;
    this.setState({
      confirmDeleteModal: !this.state.confirmDeleteModal,
      currentMessageID: "",
    }, () => {
      Promise.all(
        this.props.branch_id !== null ?
          [this.props.get_tabs_data_entity_id(this.props.branch_id, 'answered'),
          this.props.get_tabs_data_entity_id(this.props.branch_id, 'pending')] :
          [get_qa_data("answered"), get_qa_data("pending")]
      ).then((values) => {
        //('Here !')
        self.setState({
          answeredQA: self.props.qa_questions_answered,
          pendingQA: self.props.qa_questions_pending
        });
      });

    });

  };

  confirmDeleteModalToggle = () => {
    this.setState({ confirmDeleteModal: !this.state.confirmDeleteModal });
  };
  handleQAAnsweredPendingType = async (viewType) => {
    const {
      get_qa_data,
      qa_questions_answered,
      qa_questions_pending,
      branch_id
    } = this.props;

    this.setState({
      ViewQAType: viewType,
    });

    if (viewType === "answered") {
      if (branch_id !== null) {
        this.props.get_tabs_data_entity_id(branch_id, 'answered')
      } else {
        await get_qa_data("answered");
      }
      if (
        qa_questions_answered &&
        Object.keys(qa_questions_answered).length > 0
      ) {
        this.setState({
          answeredQA: qa_questions_answered,
        });
      }
    } else if (viewType === "unanswered") {
      if (branch_id !== null) {
        this.props.get_tabs_data_entity_id(branch_id, 'pending')
      } else {
        await get_qa_data("pending");
      }
      if (
        qa_questions_pending &&
        Object.keys(qa_questions_pending).length > 0
      ) {
        this.setState({
          pendingQA: qa_questions_pending,
        });
      }
    }

    // qa Api call
  };

  handleSubmit = async (body, msgID) => {
    const { update_qa_reply, get_qa_data } = this.props;

    const apiData = {
      body: body,
      body_preview: body,
      editpost: true,
      id: msgID,
      messagemedia_set: [],
      reply: true,
      type: true,
    };

    await update_qa_reply(apiData);
    let self = this;
    this.setState({
      textBox: "",
      currentMessageID: "",
      updateComment: "",
      replyComment: "",
    }, () => {
      Promise.all(
        this.props.branch_id !== null ?
          [this.props.get_tabs_data_entity_id(this.props.branch_id, 'answered'),
          this.props.get_tabs_data_entity_id(this.props.branch_id, 'pending')] :
          [get_qa_data("answered"), get_qa_data("pending")]
      ).then((values) => {
        self.setState({
          answeredQA: self.props.qa_questions_answered,
          pendingQA: self.props.qa_questions_pending
        });
      });

    });
  };

  handleSubmitComment = async (body, id) => {
    const { add_comment_feedback_qa, get_qa_data } = this.props;
    const apiData = {
      user_entry: id,
      // apiName: "corporatequestions",
      body: body,
      body_preview: body,
      tag_list: [],
      messagemedia_set: [],
    };
    await add_comment_feedback_qa(apiData, "corporatequestions");
    let self = this;
    this.setState({
      ...this.state,
      addComment: {
        ...this.state.addComment,
        [id]: "",
      },
    }, () => {
      Promise.all(
        this.props.branch_id !== null ?
          [add_comment_feedback_qa(apiData, "corporatequestions"),
          this.props.get_tabs_data_entity_id(this.props.branch_id, 'answered'),
          this.props.get_tabs_data_entity_id(this.props.branch_id, 'pending')] : ""
        // [add_comment_feedback_qa(apiData, "corporatequestions"), get_qa_data("answered"), get_qa_data("pending")]
      ).then((values) => {
        self.setState({
          answeredQA: self.props.qa_questions_answered,
          pendingQA: self.props.qa_questions_pending
        });
      });

    });
  };

  handleAnsweredPendingView = (QAData) => {
    const { pendingQA } = this.state;
    const { circleClick, user } = this.props;
    return (
      QAData &&
      QAData.results &&
      Array.isArray(QAData.results) &&
      QAData.results.length > 0 &&
      QAData.results.map((eachResultData, index) => {
        return (
          <>
            {circleClick && circleClick.topBarQA ?
              <div className={
                circleClick && circleClick.topBarQAColor === 'red' ?
                  "bg-danger text-dark p-2" :
                  circleClick && circleClick.topBarQAColor === 'orange' ?
                    "bg-warning text-dark p-2" :
                    circleClick && circleClick.topBarQAColor === 'green' ?
                      "bg-success text-dark p-2" : ""
              }>
                <div className="d-flex flex-wrap mx-2">
                  <div className="text-white px-2">
                    {/* <span className="font-weight-bold">Not Recommended</span> */}
                    <span>&nbsp;</span>Q&A
                    </div>
                  {/* <div className="fs-14 px-2 ml-auto">
                    <p className="mb-0 d-inline-block">This question is currently being Disputed.</p> <a className="text-dark font-weight-bold" href="#">Help & Cast your vote</a>
                  </div> */}
                </div>
              </div> : ""}

            <div key={index}>
              <div className="p-3 bg-light">
                <Media className="media-post">
                  <Media>
                    <img
                      className='media-object'
                      onError={(error) =>
                        (error.target.src = require("../../../assets/images/user.png"))
                      }
                      src={eachResultData?.current_profile_pic ? eachResultData.current_profile_pic : require("../../../assets/images/user.png")}
                      // src={require("../../../assets/images/user.png")}
                      alt="User Image"
                    />
                  </Media>
                  <Media body>
                    <Media heading>
                      <a href="#" className="text-dark mr-2">
                        {eachResultData.sender}
                      </a>
                      <span className="font-weight-normal mr-2">
                        asked the{" "}
                        <a href="#" className="text-dark">
                          <b>{eachResultData.recipients}</b>
                        </a>{" "}
                      a question.
                    </span>
                      <span className="time-stamp">
                        <span>&middot;</span> {eachResultData.sent_at}
                      </span>
                    </Media>
                    <div>{eachResultData.body_text}</div>
                    {eachResultData.attachments.length > 0 ? (
                      <div>
                        {eachResultData.attachments.map((attachment) => (
                          <div>
                            {attachment.type === "image" && (
                              <img
                                src={attachment.url}
                                alt={
                                  attachment && attachment.filename && attachment.filename.length < 20
                                    ? attachment.filename
                                    : this.truncate(attachment.filename)
                                }
                                width="100%"
                              />
                            )}
                            {attachment.type === "video" && (
                              <video width="320" height="240" controls>
                                <source
                                  src={attachment.url}
                                  type="video/mp4"
                                />
              Your browser does not support the video
              tag.
                              </video>
                            )}
                          </div>
                        ))}
                      </div>
                    ) : (
                        ""
                      )}
                  </Media>
                </Media>
              </div>
              {eachResultData &&
                eachResultData.conversation &&
                Array.isArray(eachResultData.conversation) &&
                eachResultData.conversation.length > 0 &&
                eachResultData.conversation.map((conversation, i) => {
                  return (
                    <div className="p-3">
                      {/* All Answers wrapper */}
                      <div>
                        {/* Answer */}
                        <Media className="media-post mb-2">
                          <Media>
                            <img
                              className='media-object'
                              src={conversation.current_prfile_file}
                              onError={(error) =>
                                (error.target.src = require("../../../assets/images/user.png"))
                              }
                              alt={conversation?.sender || "User Image"}
                            />
                          </Media>
                          <Media body>
                            <Media heading>
                              <div className="d-flex">
                                <div className="align-self-center">
                                  <a href="#" className="text-dark mr-2">
                                    {conversation.sender}
                                  </a>
                                  <span className="time-stamp">
                                    <span>&middot;</span> {conversation.sent_at}
                                  </span>
                                </div>

                                {/* If self posted, then only show actions below */}
                                <div className="ml-auto">
                                  <Button
                                    size="sm"
                                    color="transparent"
                                    className="text-muted"
                                    onClick={() => {
                                      this.setState({
                                        textBox: "updateReply",
                                        currentMessageID: conversation.msg_id,
                                        updateComment: conversation.body,
                                      });
                                    }}
                                  >
                                    <FontAwesomeIcon icon="pencil-alt" />{" "}
                                  </Button>
                                  <Button
                                    size="sm"
                                    color="transparent"
                                    className="ml-0 text-muted"
                                    onClick={() => {
                                      this.setState({
                                        currentMessageID: conversation.msg_id,
                                      });
                                      this.confirmDeleteModalToggle();
                                    }}
                                  >
                                    <FontAwesomeIcon icon="trash-alt" />{" "}
                                  </Button>
                                </div>
                              </div>
                            </Media>

                            {/* Show Answer, hide when editing */}
                            <div>
                              <div>{conversation.body}</div>

                              {/* Reply Button */}
                              <div>
                                <div className="d-flex mx-n2 align-items-center">
                                  <div className="px-2 ml-auto">
                                    {/* <span className="mx-1">|</span> */}
                                    <button
                                      className="btn btn-link btn-sm text-muted"
                                      onClick={() =>
                                        this.setState({
                                          textBox: "reply",
                                          currentMessageID: conversation.msg_id,
                                        })
                                      }
                                    >
                                      REPLY
                                  </button>
                                  </div>
                                </div>
                              </div>
                            </div>

                            {/* Show when editing answer */}
                            {this.state.textBox === "updateReply" &&
                              this.state.currentMessageID ===
                              conversation.msg_id ? (
                                <div>
                                  <div className="mt-2">
                                    <Input
                                      bsSize="sm"
                                      className="mb-2 light border-primary text-color"
                                      type="textarea"
                                      name="updateComment"
                                      onChange={this.handleChange}
                                      placeholder="Edit your answer..."
                                      value={this.state.updateComment}
                                    />
                                    <div className="text-right">
                                      <div className="text-right">
                                        <Button
                                          size="sm"
                                          color="primary"
                                          onClick={() =>
                                            this.handleSubmit(
                                              this.state.updateComment,
                                              this.state.currentMessageID
                                            )
                                          }
                                        >
                                          Update
                                    </Button>
                                        <Button
                                          size="sm"
                                          color="light"
                                          onClick={() => {
                                            this.setState({
                                              textBox: "",
                                              currentMessageID: "",
                                              updateComment: "",
                                            });
                                          }}
                                        >
                                          {" "}
                                      Cancel{" "}
                                        </Button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              ) : (
                                ""
                              )}

                            {/* Show When Replying */}
                            {this.state.textBox === "reply" &&
                              this.state.currentMessageID ===
                              conversation.msg_id ? (
                                <div>
                                  <Media className="media-post mt-2">
                                    <Media>
                                      <img
                                        className='media-object'
                                        onError={(error) =>
                                          (error.target.src = require("../../../assets/images/user.png"))
                                        }
                                        src={conversation.current_prfile_file}
                                        alt={conversation.sender || "User Image"}
                                      />
                                    </Media>
                                    <Media body>
                                      <FormGroup className="mb-0">
                                        <Input
                                          bsSize="sm"
                                          className="mb-2"
                                          type="textarea"
                                          name="replyComment"
                                          onChange={this.handleChange}
                                          placeholder="Reply to an answer..."
                                          value={this.state.replyComment}
                                        />
                                        <div className="text-right">
                                          <div className="text-right">
                                            <Button
                                              size="sm"
                                              color="primary"
                                              className=" mr-2"
                                              onClick={() =>
                                                this.handleSubmit(
                                                  this.state.replyComment,
                                                  this.state.currentMessageID
                                                )
                                              }
                                            >
                                              Submit
                                        </Button>
                                            <Button
                                              size="sm"
                                              color="light"
                                              className="ml-0"
                                              onClick={() => {
                                                this.setState({
                                                  textBox: "",
                                                  currentMessageID: "",
                                                });
                                              }}
                                            >
                                              Cancel
                                        </Button>
                                          </div>
                                        </div>
                                      </FormGroup>
                                    </Media>
                                  </Media>
                                  <hr />
                                </div>
                              ) : (
                                ""
                              )}
                          </Media>
                        </Media>
                      </div>
                    </div>
                  );
                })}

              {/* If QA */}
              {this.state.ViewQAType === "answered" ||
                (pendingQA && pendingQA.count === 0) ||
                circleClick && circleClick.topBarQA ? (
                  ""
                ) : (
                  <div
                    hidden={this.props.mapBranchClickData === true ? true : false}
                    className="bg-white mb-3">
                    {/* Question asked */}

                    <div className="p-3">
                      {/* Post an answer */}
                      <div>
                        <Media className="media-post">
                          <Media>
                            <img
                              className='media-object'
                              onError={(error) =>
                                (error.target.src = require("../../../assets/images/user.png"))
                              }
                              src={user?.current_user?.current_profile_file || require("../../../assets/images/user.png")}
                              alt="User Image"
                            />
                          </Media>
                          <Media body>
                            <FormGroup className="mb-0">
                              <Input
                                bsSize="sm"
                                className="mb-2"
                                type="textarea"
                                rows="4"
                                name="addComment"
                                value={this.state.addComment[eachResultData.id]}
                                onChange={this.handleAddComment(eachResultData.id)}
                                placeholder="Do you know the answer?"
                              />
                              <div className="text-right">
                                <div className="text-right">
                                  <Button
                                    size="sm"
                                    color="primary"
                                    className="mw"
                                    onClick={() => {
                                      this.handleSubmitComment(
                                        this.state.addComment[eachResultData.id],
                                        eachResultData.id
                                      );
                                    }}
                                  >
                                    Submit
                              </Button>
                                </div>
                              </div>
                            </FormGroup>
                          </Media>
                        </Media>
                      </div>
                    </div>
                  </div>
                )}

              {/* on circle click */}
              {circleClick && circleClick.topBarQA ? (
                <div className="bg-white mb-3">

                  <div className="p-3">
                    <div>
                      <Media className="media-post">
                        <Media>
                          <img
                            className='media-object'
                            onError={(error) =>
                              (error.target.src = require("../../../assets/images/user.png"))
                            }
                            src={user?.current_user?.current_profile_file || require("../../../assets/images/user.png")}
                            alt="User Image"
                          />
                        </Media>
                        <Media body>
                          <FormGroup className="mb-0">
                            <Input
                              bsSize="sm"
                              className="mb-2"
                              type="textarea"
                              rows="4"
                              name="addComment"
                              value={this.state.addComment[eachResultData.id]}
                              onChange={this.handleAddComment(eachResultData.id)}
                              placeholder="Do you know the answer?"
                            />
                            <div className="text-right">
                              <div className="text-right">
                                <Button
                                  size="sm"
                                  color="primary"
                                  className="mw"
                                  onClick={() => {
                                    this.handleSubmitComment(
                                      this.state.addComment[eachResultData.id],
                                      eachResultData.id
                                    );
                                  }}
                                >
                                  Submit
                              </Button>
                              </div>
                            </div>
                          </FormGroup>
                        </Media>
                      </Media>
                    </div>
                  </div>
                </div>
              ) : ""}
            </div>
          </>
        );
      })
    );
  };

  truncate = (filenameString) => {
    // let split = filenameString.split(".");
    let filename = filenameString.substr(0, filenameString.lastIndexOf("."));
    let extension = filenameString.substr(
      filenameString.lastIndexOf("."),
      filenameString.length - 1
    );
    let partial = filename.substring(filename.length - 3, filename.length);
    filename = filename.substring(0, 15);
    return filename + "..." + partial + extension;
  };
  
  sortbyfunction = (sortValue) =>{
	 let {qa_listCircle} = this.state;
	 if(sortValue && sortValue == "Oldest"){
		 qa_listCircle.results.sort((a, b) => a.id - b.id);
	 }else{
		  qa_listCircle.results.sort((a, b) => b.id - a.id);
	 }
	 this.setState({qa_listCircle})
	 
  }

  render() {
    const { answeredQA, pendingQA } = this.state;
    const { circleClick, mapBranchClickData } = this.props;
	return (
      <React.Fragment>
        <div className="text-primary">
          {mapBranchClickData === true ?
            <Filteration
              dotSelectedType={this.props.dotSelectedType}
              dotSelectedColor={this.props.dotSelectedColor}
              dotSelectedName={this.props.dotSelectedName}
              dotSelectedDays={this.props.dotSelectedDays}
              dotSelectedqueryType={this.props.dotSelectedqueryType}
              dotSelectedBranchId={this.props.dotSelectedBranchId}
			  sortbyfunction={this.sortbyfunction}
            /> : ""}

          {circleClick && circleClick.topBarQA ? "" : <div
            hidden={mapBranchClickData === true ? true : false}
            className="mb-3">
            <ButtonGroup className="type-filter flex-wrap" size="sm">
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={() => this.handleQAAnsweredPendingType("answered")}
                  active={this.state.ViewQAType === "answered"}
                >
                  Answered{" "}
                  {
                    // this.state.qa_answered_count !== null ? (
                    //   <Badge color="primary">{this.state.qa_answered_count}</Badge>
                    // ) : 
                    <Badge color="primary">
                      {this.state.answeredQA && this.state.answeredQA.count
                        ? this.state.answeredQA.count
                        : 0}
                    </Badge>

                  }

                </Button>
              </div>
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={() => this.handleQAAnsweredPendingType("unanswered")}
                  active={this.state.ViewQAType === "pending"}
                >
                  Pending{" "}
                  {
                    // this.state.qa_pending_count !== null ? (
                    //   <Badge color="primary">{this.state.qa_pending_count}</Badge>
                    // ) :
                    <Badge color="primary">
                      {this.state.pendingQA && this.state.pendingQA.count
                        ? this.state.pendingQA.count
                        : 0}
                    </Badge>

                  }
                </Button>
              </div>
            </ButtonGroup>
          </div>}

          {/* If QA */}

          {circleClick && circleClick.topBarQA ? "" : this.state.ViewQAType === "answered" ? (
            answeredQA && answeredQA.count !== 0 ? (
              this.handleAnsweredPendingView(answeredQA)
            ) : (
                <div
                  hidden={mapBranchClickData === true ? true : false}
                  className="bg-white p-3">
                  {/* If no QA */}
                  <h2 className="text-secondary-dark">
                    No questions to display.
                </h2>
                </div>
              )
          ) : (
              ""
            )}
          {circleClick && circleClick.topBarQA ? "" : this.state.ViewQAType === "unanswered" ? (
            pendingQA && pendingQA.count !== 0 ? (
              this.handleAnsweredPendingView(pendingQA)
            ) : (
                <div
                  hidden={mapBranchClickData === true ? true : false}
                  className="bg-white p-3">
                  {/* If no QA */}
                  <h2 className="text-secondary-dark">
                    No questions to display.
                </h2>
                </div>
              )
          ) : (
              ""
            )}
          {
            this.state.qa_listCircle && this.state.qa_listCircle.count !== 0 ? (
              this.handleAnsweredPendingView(this.state.qa_listCircle)
            ) : (
                <div
                  hidden={mapBranchClickData === true ? false : true}
                  className="bg-white p-3">
                  {/* If no QA */}
                  <h2 className="text-secondary-dark">
                    No questions to display.
              </h2>
                </div>
              )
          }
        </div>
        <Modal
          isOpen={this.state.confirmDeleteModal}
          toggle={this.confirmDeleteModalToggle}
          className="text-center"
        >
          <div className="bg-white modal-header">
            <h5 className="modal-title mx-auto text-dark">Confirmation</h5>
            <button
              className="btn btn-sm"
              aria-label="Close"
              onClick={this.confirmDeleteModalToggle}
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <ModalBody className="text-dark">
            Are you sure you want to delete?
          </ModalBody>
          <ModalFooter className="bg-white">
            <div className="text-center w-100">
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.confirmDeleteModalToggle}
              >
                Cancel
              </Button>
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.handleDeleteConfirmation}
              >
                Ok
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapState = (state) => {
  return {
    branch_id: state.user.branch_id,
    qa_questions_answered: state.user.qa_questions_answered,
    qa_questions_pending: state.user.qa_questions_pending,
    corporate_qa_count: state.user.corporate_qa_count,
    user: state.user,
    circleClick: state.circleClick,
	qa_list:state.user.qa_list
	
  };
};

const mapProps = (dispatch) => {
  return {
    update_qa_reply: (params) => dispatch(update_qa_reply(params)),
    delete_questions_corporateQuestiosn: (params) =>
      dispatch(delete_questions_corporateQuestiosn(params)),
    add_comment_feedback_qa: (params, apiName) =>
      dispatch(add_comment_feedback_qa(params, apiName)),
    get_qa_data: (params) => dispatch(get_qa_data(params)),
    get_tabs_data_entity_id: (entityID, type, subType) =>
      dispatch(get_tabs_data_entity_id(entityID, type, subType)),
    get_branch_data_by_circle_click: ({ color, type, days, queryType, branchID }) =>
      dispatch(get_branch_data_by_circle_click({ color, type, days, queryType, branchID })),

  };
};

export default connect(mapState, mapProps)(QA);
