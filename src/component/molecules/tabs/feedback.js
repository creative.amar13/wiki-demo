import React, { Component } from "react";
import {
  FormGroup,
  Input,
  Button,
  ButtonGroup,
  Badge,
  Media,
  Modal,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  update_feedback_reply,
  get_feedback_questions_data,
  add_comment_feedback_qa,
  delete_questions_corporateQuestiosn,
  get_tabs_data_entity_id,
  get_branch_data_by_circle_click
} from "../../../actions/user";
import { connect } from "react-redux";
import Filteration from '../../atoms/Filteration';

class Feedback extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ViewFeedbackType: "answered",
      answeredFeedback: "",
      pendingFeedback: "",
      body: "",
      editpost: true,
      id: "",
      messagemedia_set: [],
      textBox: "",
      currentMessageID: "",
      updateComment: "",
      replyComment: "",
      addComment: {},
      confirmDeleteModal: false,
	  sortyBy:"Latest",
	  feedback_listCircle:[]
    };
  }

  componentWillReceiveProps(nextProps) {
    //get feedback props
    const {
      feedback_questions_answered,
      feedback_questions_pending,
    } = nextProps;
    if (
      feedback_questions_answered &&
      Object.keys(feedback_questions_answered).length > 0
    ) {
      this.setState({
        answeredFeedback: feedback_questions_answered,
      });
    }

    if (
      feedback_questions_pending &&
      Object.keys(feedback_questions_pending).length > 0
    ) {
      this.setState({
        pendingFeedback: feedback_questions_pending,
      });
    }
	
	if(nextProps.feedback_list && nextProps.feedback_list.results && Array.isArray(nextProps.feedback_list.results) && nextProps.feedback_list.results.length > 0){
		this.setState({
			feedback_listCircle: nextProps.feedback_list,
		  }); 
	  }
  }

  async componentDidMount() {
    const { get_feedback_questions_data, branch_id, circleClick, mapBranchClickData } = this.props;

    if (circleClick && circleClick.topBarFeedback === false) {
      // if (branch_id !== null) {
      //   this.props.get_tabs_data_entity_id(branch_id, 'answered', 'feedback')
      //   this.props.get_tabs_data_entity_id(branch_id, 'pending', 'feedback')
      // } else
      if (this.props.tabName === "Feedback" && mapBranchClickData !== true) {
        await get_feedback_questions_data("pending");
        await get_feedback_questions_data("answered");
      } else {
        const { dotSelectedType, dotSelectedColor, dotSelectedName, dotSelectedDays, dotSelectedqueryType, dotSelectedBranchId } = this.props;
        let data_type = { 'reviews': '4', 'message': '5', 'qa': '7', 'feedback': '6', 'disputed_Review': '4' }
        this.props.get_branch_data_by_circle_click({
          color: dotSelectedColor,
          type: dotSelectedType,
          days: dotSelectedDays,
          queryType: dotSelectedqueryType,
          branchID: dotSelectedBranchId,
        });
      }
    }
  }

  componentWillUnmount() {
    if (this.props.circleClick && this.props.circleClick.topBarFeedback) {
      this.props.circleClick.topBarFeedback = false;
      this.props.circleClick.topBarFeedbackColor = null;
      this.props.user.feedback_list = null
    }

  }

  handleChange = async (evt) => {
    await this.setState({
      [evt.target.name]: evt.target.value,
    });
  };
  handleAddComment = (id) => (e) => {
    this.setState({
      addComment: {
        ...this.state.addComment,
        [id]: e.target.value,
      },
    });
  };
  handleDeleteConfirmation = async () => {
    const { delete_questions_corporateQuestiosn } = this.props;
    const data = {
      entityid: this.state.currentMessageID,
      entity: "question",
    };
    delete_questions_corporateQuestiosn(data);
    let self = this;
    this.setState({
      confirmDeleteModal: !this.state.confirmDeleteModal,
      currentMessageID: "",
    }, () => {
      Promise.all(
        this.props.branch_id !== null ?
          [this.props.get_tabs_data_entity_id(this.props.branch_id, 'answered', 'feedback'),
          this.props.get_tabs_data_entity_id(this.props.branch_id, 'pending', 'feedback')] :
          [get_feedback_questions_data("pending"),
          get_feedback_questions_data("answered")]
      ).then((values) => {
        self.setState({
          answeredFeedback: self.props.feedback_questions_answered,
          pendingFeedback: self.props.feedback_questions_pending,
        });
      });

    });

  };

  confirmDeleteModalToggle = () => {
    this.setState({ confirmDeleteModal: !this.state.confirmDeleteModal });
  };

  handleAnsweredPendingType = async (viewType) => {
    const {
      get_feedback_questions_data,
      feedback_questions_answered,
      feedback_questions_pending,
      branch_id,
    } = this.props;

    if (viewType === "answered") {
      if (branch_id !== null) {
        this.props.get_tabs_data_entity_id(branch_id, 'answered', 'feedback')
      } else {
        await get_feedback_questions_data("answered");
      }

      if (
        feedback_questions_answered &&
        Object.keys(feedback_questions_answered).length > 0
      ) {
        this.setState({
          answeredFeedback: feedback_questions_answered,
        });
      }
    } else if (viewType === "unanswered") {
      if (branch_id !== null) {
        this.props.get_tabs_data_entity_id(branch_id, 'pending', 'feedback')
      } else {
        await get_feedback_questions_data("pending");
      }
      if (
        feedback_questions_pending &&
        Object.keys(feedback_questions_pending).length > 0
      ) {
        this.setState({
          pendingFeedback: feedback_questions_pending,
        });
      }
    }

    // feedback Api call

    this.setState({
      ViewFeedbackType: viewType,
    });
  };

  handleSubmit = async (body, msgID) => {
    const { update_feedback_reply, get_feedback_questions_data } = this.props;

    const apiData = {
      body: body,
      body_preview: body,
      editpost: true,
      id: msgID,
      messagemedia_set: [],
      reply: true,
      type: true,
    };
    await update_feedback_reply(apiData);
    let self = this;
    this.setState({
      textBox: "",
      currentMessageID: "",
      updateComment: "",
      replyComment: "",
    }, () => {
      Promise.all(
        this.props.branch_id !== null ?
          [this.props.get_tabs_data_entity_id(this.props.branch_id, 'answered', 'feedback'),
          this.props.get_tabs_data_entity_id(this.props.branch_id, 'pending', 'feedback')] :
          [get_feedback_questions_data("pending"),
          get_feedback_questions_data("answered")]
      ).then((values) => {
        self.setState({
          answeredFeedback: self.props.feedback_questions_answered,
          pendingFeedback: self.props.feedback_questions_pending,
        });
      });
    });
  };

  handleSubmitComment = async (body, id) => {

    const { add_comment_feedback_qa } = this.props;
    let User_entry = this.props?.user?.corporate_id_details?.id;
    let idobject = {};
    idobject[id] = body;
    idobject['reply'] = body;
    idobject["user_entry"] = User_entry;
    idobject['tag_list'] = [];
    idobject['messagemedia_set'] = [];

    await add_comment_feedback_qa(idobject, "questions", id);
    let self = this;
    this.setState({
      ...this.state,
      addComment: {
        ...this.state.addComment,
        [id]: "",
      },
    }, () => {
      Promise.all(
        this.props.branch_id !== null ?
          [this.props.get_tabs_data_entity_id(this.props.branch_id, 'answered', 'feedback'),
          this.props.get_tabs_data_entity_id(this.props.branch_id, 'pending', 'feedback')] : ""
          // [get_feedback_questions_data("pending"),
          // get_feedback_questions_data("answered")]
      ).then((values) => {
        self.setState({
          answeredFeedback: self.props.feedback_questions_answered,
          pendingFeedback: self.props.feedback_questions_pending,
        });
      });
    });
  };

  handleViewAnsweredPending = (feedback) => {
    const { pendingFeedback } = this.state;
    const { circleClick, user } = this.props;
    return (
      feedback &&
      feedback.results &&
      Array.isArray(feedback.results) &&
      feedback.results.length > 0 &&
      feedback.results.map((eachResultData, index) => {
        return (
          <>
            {circleClick && circleClick.topBarFeedback ?
              <div className={
                circleClick && circleClick.topBarFeedbackColor === 'red' ?
                  "bg-danger text-dark p-2" :
                  circleClick && circleClick.topBarFeedbackColor === 'orange' ?
                    "bg-warning text-dark p-2" :
                    circleClick && circleClick.topBarFeedbackColor === 'green' ?
                      "bg-success text-dark p-2" : ""
              }>
                <div className="d-flex flex-wrap mx-2">
                  <div className="text-white px-2">
                    {/* <span className="font-weight-bold">Not Recommended</span> */}
                    <span>&nbsp;</span>
                      Feedback
                    </div>
                  {/* <div className="fs-14 px-2 ml-auto">
                    <p className="mb-0 d-inline-block">This feedback is currently being Disputed.</p> <a className="text-dark font-weight-bold" href="#">Help & Cast your vote</a>
                  </div> */}
                </div>
              </div> : ""}
            <div key={index}>
              <div className="p-3 bg-light">
                <Media className="media-post">
                  <Media>
                    <img
                      className='media-object'
                      onError={(error) =>
                        (error.target.src = require("../../../assets/images/user.png"))
                      }
                      src={eachResultData.current_profile_pic || require("../../../assets/images/user.png")}
                      // src={require("../../../assets/images/user.png")}
                      alt="User Image"
                    />
                  </Media>
                  <Media body>
                    <Media heading>
                      <a href="#" className="text-dark mr-2">
                        {eachResultData.sender}
                      </a>
                      <span className="font-weight-normal mr-2">
                        asked the{" "}
                        <a href="#" className="text-dark">
                          <b>{eachResultData.recipients}</b>
                        </a>{" "}
                      a question.
                    </span>
                      <span className="time-stamp">
                        <span>&middot;</span> {eachResultData.sent_at}
                      </span>
                    </Media>
                    <div>{eachResultData.body_text}</div>
                    {eachResultData.attachments.length > 0 ? (
                      <div>
                        {eachResultData.attachments.map((attachment) => (
                          <div>
                            {attachment.type === "image" && (
                              <img
                                src={attachment.url}
                                alt={
                                  attachment && attachment.filename && attachment.filename.length < 20
                                    ? attachment.filename
                                    : this.truncate(attachment.filename)
                                }
                                width="100%"
                              />
                            )}
                            {attachment.type === "video" && (
                              <video width="320" height="240" controls>
                                <source
                                  src={attachment.url}
                                  type="video/mp4"
                                />
              Your browser does not support the video
              tag.
                              </video>
                            )}
                          </div>
                        ))}
                      </div>
                    ) : (
                        ""
                      )}
                  </Media>
                </Media>
              </div>
              {eachResultData &&
                eachResultData.conversation &&
                Array.isArray(eachResultData.conversation) &&
                eachResultData.conversation.length > 0 &&
                eachResultData.conversation.map((conversation, i) => {
                  return (
                    <div className="p-3">
                      {/* All Answers wrapper */}
                      <div>
                        {/* Answer */}
                        <Media className="media-post mb-2">
                          <Media>
                            <img
                              className='media-object'
                              onError={(error) =>
                                (error.target.src = require("../../../assets/images/user.png"))
                              }
                              src={conversation.current_profile_pic}
                              alt="User Image"
                            />
                          </Media>
                          <Media body>
                            <Media heading>
                              <div className="d-flex">
                                <div className="align-self-center">
                                  <a href="#" className="text-dark mr-2">
                                    {conversation.sender}
                                  </a>
                                  <span className="time-stamp">
                                    <span>&middot;</span> {conversation.sent_at}
                                  </span>
                                </div>

                                {/* If self posted, then only show actions below */}
                                <div className="ml-auto">
                                  <Button
                                    size="sm"
                                    color="transparent"
                                    className="text-muted"
                                    onClick={() => {
                                      this.setState({
                                        textBox: "updateReply",
                                        currentMessageID: conversation.msg_id,
                                        updateComment: conversation.body,
                                      });
                                    }}
                                  >
                                    <FontAwesomeIcon icon="pencil-alt" />{" "}
                                  </Button>
                                  <Button
                                    size="sm"
                                    color="transparent"
                                    className="ml-0 text-muted"
                                    onClick={() => {
                                      this.setState({
                                        currentMessageID: conversation.msg_id,
                                      });
                                      this.confirmDeleteModalToggle();
                                    }}
                                  >
                                    <FontAwesomeIcon icon="trash-alt" />{" "}
                                  </Button>
                                </div>
                              </div>
                            </Media>

                            {/* Show Answer, hide when editing */}
                            <div>
                              <div>{conversation.body}</div>

                              {/* Reply Button */}
                              <div>
                                <div className="d-flex mx-n2 align-items-center">
                                  <div className="px-2 ml-auto">
                                    {/* <span className="mx-1">|</span> */}
                                    <button
                                      className="btn btn-link btn-sm text-muted"
                                      onClick={() =>
                                        this.setState({
                                          textBox: "reply",
                                          currentMessageID: conversation.msg_id,
                                        })
                                      }
                                    >
                                      REPLY
                                  </button>
                                  </div>
                                </div>
                              </div>
                            </div>

                            {/* Show when editing answer */}
                            {this.state.textBox === "updateReply" &&
                              this.state.currentMessageID ===
                              conversation.msg_id ? (
                                <div>
                                  <div className="mt-2">
                                    <Input
                                      bsSize="sm"
                                      className="mb-2"
                                      type="textarea"
                                      name="updateComment"
                                      onChange={this.handleChange}
                                      placeholder="Edit your answer..."
                                      value={this.state.updateComment}
                                    />
                                    <div className="text-right">
                                      <div className="text-right">
                                        <Button
                                          size="sm"
                                          color="primary"
                                          onClick={() =>
                                            this.handleSubmit(
                                              this.state.updateComment,
                                              this.state.currentMessageID
                                            )
                                          }
                                        >
                                          Update
                                    </Button>
                                        <Button
                                          size="sm"
                                          color="light"
                                          onClick={() => {
                                            this.setState({
                                              textBox: "",
                                              currentMessageID: "",
                                              updateComment: "",
                                            });
                                          }}
                                        >
                                          {" "}
                                      Cancel{" "}
                                        </Button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              ) : (
                                ""
                              )}

                            {/* Show When Replying */}
                            {this.state.textBox === "reply" &&
                              this.state.currentMessageID ===
                              conversation.msg_id ? (
                                <div>
                                  <Media className="media-post mt-2">
                                    <Media>
                                      <img
                                        className='media-object'
                                        onError={(error) =>
                                          (error.target.src = require("../../../assets/images/user.png"))
                                        }
                                        src={conversation.current_profile_pic}
                                        alt="User Image"
                                      />
                                    </Media>
                                    <Media body>
                                      <FormGroup className="mb-0">
                                        <Input
                                          bsSize="sm"
                                          className="mb-2"
                                          type="textarea"
                                          name="replyComment"
                                          onChange={this.handleChange}
                                          placeholder="Reply to an answer..."
                                          value={this.state.replyComment}
                                        />
                                        <div className="text-right">
                                          <div className="text-right">
                                            <Button
                                              size="sm"
                                              color="primary"
                                              className=" mr-2"
                                              onClick={() =>
                                                this.handleSubmit(
                                                  this.state.replyComment,
                                                  this.state.currentMessageID
                                                )
                                              }
                                            >
                                              Submit
                                        </Button>
                                            <Button
                                              size="sm"
                                              color="light"
                                              className="ml-0"
                                              onClick={() => {
                                                this.setState({
                                                  textBox: "",
                                                  currentMessageID: "",
                                                });
                                              }}
                                            >
                                              Cancel
                                        </Button>
                                          </div>
                                        </div>
                                      </FormGroup>
                                    </Media>
                                  </Media>
                                  <hr />
                                </div>
                              ) : (
                                ""
                              )}
                          </Media>
                        </Media>
                      </div>
                    </div>
                  );
                })}

              {/* If feedbacks */}
              {this.state.ViewFeedbackType === "answered" ||
                (pendingFeedback && pendingFeedback.count === 0) ? (
                  ""
                ) : (
                  <div
                    hidden={this.props.mapBranchClickData === true ? true : false}
                    className="bg-white mb-3">
                    {/* Question asked */}

                    <div className="p-3">
                      {/* Post an answer */}
                      <div>
                        <Media className="media-post">
                          <Media>
                            <img
                              className='media-object'
                              onError={(error) =>
                                (error.target.src = require("../../../assets/images/user.png"))
                              }
                              src={user?.current_user?.current_profile_file || require("../../../assets/images/user.png")}
                              alt="User Image"
                            />
                          </Media>
                          <Media body>
                            <FormGroup className="mb-0">
                              <Input
                                bsSize="sm"
                                className="mb-2"
                                type="textarea"
                                rows="4"
                                name="addComment"
                                value={this.state.addComment[eachResultData.id]}
                                onChange={this.handleAddComment(eachResultData.id)}
                                placeholder="Do you know the answer?"
                              />
                              <div className="text-right">
                                <div className="text-right">
                                  <Button
                                    size="sm"
                                    color="primary"
                                    className="mw"
                                    onClick={() => {
                                      this.handleSubmitComment(
                                        this.state.addComment[eachResultData.id],
                                        eachResultData.id
                                      );
                                    }}
                                  >
                                    Submit
                              </Button>
                                </div>
                              </div>
                            </FormGroup>
                          </Media>
                        </Media>
                      </div>
                    </div>
                  </div>
                )}
              {circleClick && circleClick.topBarFeedback ? (
                <div className="bg-white mb-3">

                  <div className="p-3">
                    <div>
                      <Media className="media-post">
                        <Media>
                          <img
                            className='media-object'
                            onError={(error) =>
                              (error.target.src = require("../../../assets/images/user.png"))
                            }
                            src={user?.current_user?.current_profile_file || require("../../../assets/images/user.png")}
                            alt="User Image"
                          />
                        </Media>
                        <Media body>
                          <FormGroup className="mb-0">
                            <Input
                              bsSize="sm"
                              className="mb-2"
                              type="textarea"
                              rows="4"
                              name="addComment"
                              value={this.state.addComment[eachResultData.id]}
                              onChange={this.handleAddComment(eachResultData.id)}
                              placeholder="Do you know the answer?"
                            />
                            <div className="text-right">
                              <div className="text-right">
                                <Button
                                  size="sm"
                                  color="primary"
                                  className="mw"
                                  onClick={() => {
                                    this.handleSubmitComment(
                                      this.state.addComment[eachResultData.id],
                                      eachResultData.id
                                    );
                                  }}
                                >
                                  Submit
                              </Button>
                              </div>
                            </div>
                          </FormGroup>
                        </Media>
                      </Media>
                    </div>
                  </div>
                </div>
              ) : ""}
            </div>
          </>
        );
      })
    );
  };

  truncate = (filenameString) => {
    // let split = filenameString.split(".");
    let filename = filenameString.substr(0, filenameString.lastIndexOf("."));
    let extension = filenameString.substr(
      filenameString.lastIndexOf("."),
      filenameString.length - 1
    );
    let partial = filename.substring(filename.length - 3, filename.length);
    filename = filename.substring(0, 15);
    return filename + "..." + partial + extension;
  };
  
  sortbyfunction = (sortValue) =>{
	 let {feedback_listCircle} = this.state;
	 if(sortValue && sortValue == "Oldest"){
		 feedback_listCircle.results.sort((a, b) => a.id - b.id);
	 }else{
		  feedback_listCircle.results.sort((a, b) => b.id - a.id);
	 }
	 this.setState({feedback_listCircle})
	 
  }

  render() {
    const { pendingFeedback, answeredFeedback } = this.state;
    const { circleClick, mapBranchClickData } = this.props;
    return (
      <React.Fragment>
        <div className="text-primary">
          {mapBranchClickData === true ?
            <Filteration
              dotSelectedType={this.props.dotSelectedType}
              dotSelectedColor={this.props.dotSelectedColor}
              dotSelectedName={this.props.dotSelectedName}
              dotSelectedDays={this.props.dotSelectedDays}
              dotSelectedqueryType={this.props.dotSelectedqueryType}
              dotSelectedBranchId={this.props.dotSelectedBranchId}
			  sortbyfunction={this.sortbyfunction}
            /> : ""}
          {circleClick && circleClick.topBarFeedback ? "" : <div
            hidden={mapBranchClickData === true ? true : false}
            className="mb-3">
            <ButtonGroup className="type-filter flex-wrap" size="sm">
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={() => this.handleAnsweredPendingType("answered")}
                  active={this.state.ViewFeedbackType === "answered"}
                >
                  Answered{" "}
                  <Badge color="primary">
                    {this.state.answeredFeedback && this.state.answeredFeedback.count
                      ? this.state.answeredFeedback.count
                      : 0}
                  </Badge>
                </Button>
              </div>
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={() => this.handleAnsweredPendingType("unanswered")}
                  active={this.state.ViewFeedbackType === "unanswered"}
                >
                  Unanswered{" "}
                  <Badge color="primary">
                    {this.state.pendingFeedback && this.state.pendingFeedback.count
                      ? this.state.pendingFeedback.count
                      : 0}
                  </Badge>
                </Button>
              </div>
            </ButtonGroup>
          </div>}

          {circleClick && circleClick.topBarFeedback ? "" :
            <>{this.state.ViewFeedbackType === "answered" ? (
              answeredFeedback && answeredFeedback.count !== 0 ? (
                this.handleViewAnsweredPending(answeredFeedback)
              ) : (
                  <div
                    hidden={mapBranchClickData === true ? true : false}
                    className="bg-white p-3">
                    <h2 className="text-secondary-dark">
                      There is no feedback yet.
                </h2>
                  </div>
                )
            ) : (
                ""
              )}
              {this.state.ViewFeedbackType === "unanswered" ? (
                pendingFeedback && pendingFeedback.count !== 0 ? (
                  this.handleViewAnsweredPending(pendingFeedback)
                ) : (
                    <div
                      hidden={mapBranchClickData === true ? true : false}
                      className="bg-white p-3">
                      <h2 className="text-secondary-dark">
                        There is no feedback yet.
                </h2>
                    </div>
                  )
              ) : (
                  ""
                )}</>}
          {
            this.state.feedback_listCircle && this.state.feedback_listCircle.count !== 0 ? (
              this.handleViewAnsweredPending(this.state.feedback_listCircle)
            ) : (
                <div
                  hidden={mapBranchClickData === true ? false : true}
                  className="bg-white p-3">
                  <h2 className="text-secondary-dark">
                    There is no feedback yet.
                  </h2>
                </div>
              )
          }
        </div>
        <Modal
          isOpen={this.state.confirmDeleteModal}
          toggle={this.confirmDeleteModalToggle}
          className="text-center"
        >
          <div className="bg-white modal-header">
            <h5 className="modal-title mx-auto text-dark">Confirmation</h5>
            <button
              className="btn btn-sm"
              aria-label="Close"
              onClick={this.confirmDeleteModalToggle}
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <ModalBody className="text-dark">
            Are you sure you want to delete?
          </ModalBody>
          <ModalFooter className="bg-white">
            <div className="text-center w-100">
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.confirmDeleteModalToggle}
              >
                Cancel
              </Button>
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.handleDeleteConfirmation}
              >
                Ok
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      </React.Fragment >
    );
  }
}

const mapState = (state) => ({
  feedback_questions_answered: state.user.feedback_questions_answered,
  feedback_questions_pending: state.user.feedback_questions_pending,
  branch_id: state.user.branch_id,
  user: state.user,
  circleClick: state.circleClick,
  feedback_list: state.user.feedback_list
});

const mapProps = (dispatch) => {
  return {
    update_feedback_reply: (params) => dispatch(update_feedback_reply(params)),
    delete_questions_corporateQuestiosn: (params) =>
      dispatch(delete_questions_corporateQuestiosn(params)),
    add_comment_feedback_qa: (params, apiName, id) =>
      dispatch(add_comment_feedback_qa(params, apiName, id)),
    get_feedback_questions_data: (params) =>
      dispatch(get_feedback_questions_data(params)),
    get_tabs_data_entity_id: (entityID, type, subType) =>
      dispatch(get_tabs_data_entity_id(entityID, type, subType)),
    get_branch_data_by_circle_click: ({ color, type, days, queryType, branchID }) =>
      dispatch(get_branch_data_by_circle_click({ color, type, days, queryType, branchID })),

  };
};

export default connect(mapState, mapProps)(Feedback);
