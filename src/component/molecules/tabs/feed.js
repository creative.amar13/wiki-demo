import React, { Component } from "react";
import {
  FormGroup,
  Input,
  Button,
  Media,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { connect } from "react-redux";
import {
  get_feed_list,
  add_feed_comment,
  add_feed_comment_reply,
  delete_feed_comment,
  edit_feed_comment,
} from "../../../actions/user";
import ReactHtmlParser from "react-html-parser";

class Feed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: "All",
      feedList: {},
      replyFormVisible: {},
      editFormVisible: {},
      confirmDeleteModal: false,
      dropdownOpen: false,
      profilePic: "",
      filterType: "all",
      pageNo: 1,
      editComment: {
        body: "",
      },
      addComment: {
        body: "",
      },
      addCommentReply: {
        body: "",
      },
      deleteCommentId: 0,
    };
    this.toggle = this.toggle.bind(this);
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  onMouseEnter() {
    this.setState({ dropdownOpen: true });
  }

  onMouseLeave() {
    this.setState({ dropdownOpen: false });
  }

  componentWillReceiveProps(nextProps) {
    //get feed list
    if (nextProps.feed_list && Object.keys(nextProps.feed_list).length > 0) {
      this.setState({
        feedList: nextProps.feed_list,
        profilePic: nextProps.loggedInUser
          ? nextProps.loggedInUser.current_profile_file
          : "",
      });
    }
  }

  componentDidMount() {
    this.props.get_feed_list(this.state.filterType, this.state.pageNo);
  }

  handleOnClick = (selected, filterType, pageNo) => {
    this.props.get_feed_list(filterType, pageNo);
    this.setState({ selected, filterType, pageNo });
  };

  toggleReplyForm = (id, level) => {
    id = level + id;
    this.setState({
      ...this.state,
      editFormVisible: {},
      replyFormVisible: {
        //...this.state.replyFormVisible,
        [id]: !this.state.replyFormVisible[id],
      },
      addComment: {
        body: "",
      },
      addCommentReply: {
        body: "",
      },
    });
  };
  toggleEditForm = (id, level, newBody = "") => {
    id = level + id;
    if (this.state.editFormVisible[level + id]) {
      newBody = "";
    }
    this.setState({
      ...this.state,
      replyFormVisible: {},
      editFormVisible: {
        [id]: !this.state.editFormVisible[id],
      },
      editComment: {
        body: newBody,
      },
      addComment: {
        body: "",
      },
      addCommentReply: {
        body: "",
      },
    });
  };
  handleEditCommentOnChange = (e) => {
    this.setState({
      ...this.state,
      editComment: {
        body: e.target.value,
      },
    });
  };
  handleEditCommentOnSubmit = (id, level) => (e) => {
    e.preventDefault();
    if (this.state.editComment.body !== "") {
      let comment = {
        body: this.state.editComment.body,
        edit: true,
        discussion_id: id,
      };
      this.props.edit_feed_comment(
        comment,
        this.state.filterType,
        this.state.pageNo
      );
      this.setState({
        ...this.state,
        editComment: {
          body: "",
        },
        editFormVisible: {
          [level + id]: !this.state.editFormVisible[level + id],
        },
      });
    }
  };
  confirmDeleteModalToggle = () => {
    if (this.state.deleteCommentId) {
      this.setState({ deleteCommentId: 0 });
    }
    this.setState({ confirmDeleteModal: !this.state.confirmDeleteModal });
  };
  handleOnChangeComment = (e) => {
    this.setState({
      ...this.state,
      addComment: {
        ...this.state.addComment,
        body: e.target.value,
      },
    });
  };

  handleOnCommentSubmit = (id, level) => (e) => {
    e.preventDefault();
    if (this.state.addComment.body !== "") {
      let url = "";
      let comment = "";
      if (this.state.filterType === "reviews") {
        comment = { body: this.state.addComment.body, media: [], review: id };
        url = `/api/reviewdiscussion/?review=${id}`;
      } else {
        comment = { body: this.state.addComment.body, action_id: id };
        url = "/api/otherfeeddiscussion/?";
      }
      this.props.add_feed_comment(
        url,
        comment,
        this.state.filterType,
        this.state.pageNo
      );
      this.setState({
        ...this.state,
        addComment: {
          ...this.state.addComment,
          body: "",
        },
      });
      this.toggleReplyForm(id, level);
    }
  };

  handleOnChangeCommentReply = (e) => {
    this.setState({
      ...this.state,
      addCommentReply: {
        ...this.state.addCommentReply,
        body: e.target.value,
      },
    });
  };

  handleOnCommentReplySubmit = (id, level, conversationId = 0) => (e) => {
    e.preventDefault();
    if (this.state.addCommentReply.body !== "") {
      let url = "/api/otherfeeddiscussion/?";
      let reply = {
        body: this.state.addCommentReply.body,
        discussion_id: id,
      };

      this.props.add_feed_comment_reply(
        url,
        reply,
        this.state.filterType,
        this.state.pageNo
      );
      this.setState({
        ...this.state,
        addCommentReply: {
          ...this.state.addCommentReply,
          body: "",
        },
      });
      if (level !== "L3" && conversationId === 0) {
        this.toggleReplyForm(id, level);
      } else {
        this.toggleReplyForm(conversationId, level);
      }
    }
  };

  handleOnDeleteConfirmation = () => {
    if (this.state.deleteCommentId) {
      this.props.delete_feed_comment(
        this.state.deleteCommentId,
        this.state.filterType,
        this.state.pageNo
      );
      this.setState({
        confirmDeleteModal: !this.state.confirmDeleteModal,
        deleteCommentId: 0,
      });
    }
  };

  render() {
    const {
      selected,
      feedList,
      replyFormVisible,
      profilePic,
      editFormVisible,
    } = this.state;
    return (
      <React.Fragment>
        <div className="mb-3">
          <UncontrolledDropdown className="d-inline-block" onMouseOver={this.onMouseEnter} onMouseLeave={this.onMouseLeave} isOpen={this.state.dropdownOpen} toggle={this.toggle}>
            <DropdownToggle color="transparent" className="text-dark-main shadow-none bg-transparent font-weight-bold" size="sm" caret>
              <span className="mr-2">{selected}</span>
            </DropdownToggle>
            <DropdownMenu className="type-tertiary">
              <DropdownItem onClick={() => this.handleOnClick("All", "all", 1)}>
                All
              </DropdownItem>
              <DropdownItem
                onClick={() => this.handleOnClick("Reviews", "reviews", 1)}
              >
                Reviews
              </DropdownItem>
              <DropdownItem
                onClick={() =>
                  this.handleOnClick("Recommends", "recommends", 1)
                }
              >
                Recommends
              </DropdownItem>
              <DropdownItem
                onClick={() =>
                  this.handleOnClick(
                    "Want, Favourite & Watchlist",
                    "pin_list",
                    1
                  )
                }
              >
                Want, Favorite &amp; Watchlist{" "}
              </DropdownItem>
              <DropdownItem
                onClick={() =>
                  this.handleOnClick("Business Tagging", "business_tagging", 1)
                }
              >
                Business Tagging
              </DropdownItem>
              <DropdownItem
                onClick={() =>
                  this.handleOnClick("Tips & Warnings", "tips_warnings", 1)
                }
              >
                Tips &amp; Warnings
              </DropdownItem>
              <DropdownItem
                onClick={() => this.handleOnClick("Q&A", "question_answer", 1)}
              >
                Q&amp;A
              </DropdownItem>
              <DropdownItem
                onClick={() => this.handleOnClick("Offers", "offers", 1)}
              >
                Offers
              </DropdownItem>
              <DropdownItem
                onClick={() =>
                  this.handleOnClick(
                    "Community Feedback",
                    "community_feedback",
                    1
                  )
                }
              >
                Community Feedback
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </div>

        {/* All Posts container */}
        <div>
          {feedList.count ? (
            feedList.results.map((feed) => {
              return (
                <div className="bg-white p-3 mb-2" key={feed.id}>
                  <Media className="media-post">
                    <Media>
                      <img
                        className='media-object'
                        onError={(error) =>
                          (error.target.src = require("../../../assets/images/user.png"))
                        }
                        src={
                          feed.actor.current_profile_pic.url
                            ? feed.actor.current_profile_pic.url
                            : require("../../../assets/images/user.png")
                        }
                        alt="User Image"
                      />
                    </Media>
                    <Media body>
                      <Media heading>
                        {feed.actor.first_name} {feed.actor.last_name}{" "}
                        <span className="time-stamp">
                          <span>&middot;</span> {feed.time_stamp}
                        </span>
                      </Media>

                      {/* Comments */}
                      <div>
                        {feed.action && feed.action.body ? (
                          <div>
                            {ReactHtmlParser(
                              feed.action.body.replace(/\n/g, "<br />")
                            )}
                          </div>
                        ) : (
                            ""
                          )}
                        {feed.action &&
                          feed.action.attachments &&
                          feed.action.attachments.length > 0 ? (
                            <div className="mb-2">
                              {feed.action.attachments.map((attachment) => (
                                <div>
                                  {attachment.type === "image" && (
                                    <img
                                      src={attachment.url}
                                      alt={attachment.filename}
                                      width="100%"
                                    />
                                  )}
                                  {attachment.type === "video" && (
                                    <video width="320" height="240" controls>
                                      <source
                                        src={attachment.url}
                                        type="video/mp4"
                                      />
                                    Your browser does not support the video tag.
                                    </video>
                                  )}
                                </div>
                              ))}
                            </div>
                          ) : (
                            ""
                          )}
                      </div>

                      {/* Quick reactions and REPLY button */}
                      <div>
                        <div className="d-flex">
                          <div className="quick-reactions d-flex flex-wrap">
                            <div className="reaction-holder" hidden>
                              {/* Show on reacting */}
                              <div className="reaction-anim">
                                <img
                                  src={require("../../../assets/images/woohoo_forever.gif")}
                                  alt=""
                                />
                              </div>

                              <button
                                className="reaction woohoo"
                                title="Woohoo"
                              >
                                <span className="icon">
                                  <img
                                    src={require("../../../assets/images/woohoo.png")}
                                    alt="woohoo"
                                  />
                                </span>
                                <span className="name">
                                  Woohoo
                                </span>
                                <span className="count">
                                  {feed &&
                                    feed.action &&
                                    feed.action.review_group_animation &&
                                    feed.action.review_group_animation
                                      .review_group_count.woohoo
                                    ? feed.action.review_group_animation
                                      .review_group_count.woohoo
                                    : 0}
                                </span>
                              </button>
                            </div>
                            <div className="reaction-holder">
                              {/* Show on reacting */}
                              <div className="reaction-anim" hidden>
                                <img
                                  src={require("../../../assets/images/nailedit_forever.gif")}
                                  alt=""
                                />
                              </div>

                              <button
                                className="reaction nailed"
                                title="Nailed It"
                              >
                                <span className="icon">
                                  <img
                                    src={require("../../../assets/images/nailed-it.png")}
                                    alt="nailed it"
                                  />
                                </span>
                                <span className="name">
                                  Nailed It
                                </span>
                                <span className="count">
                                  {feed &&
                                    feed.action &&
                                    feed.action.review_group_animation &&
                                    feed.action.review_group_animation
                                      .review_group_count
                                    ? feed.action.review_group_animation
                                      .review_group_count.nailed
                                    : 0}
                                </span>
                              </button>
                            </div>
                            <div className="reaction-holder">
                              {/* Show on reacting */}
                              <div className="reaction-anim" hidden>
                                <img
                                  src={require("../../../assets/images/insightful_forever.gif")}
                                  alt=""
                                />
                              </div>

                              <button
                                className="reaction insightful"
                                title="Insightful"
                              >
                                <span className="icon">
                                  <img
                                    src={require("../../../assets/images/insightful.png")}
                                    alt="insightful"
                                  />
                                </span>
                                <span className="name">
                                  Insightful
                                </span>
                                <span className="count">
                                  {feed &&
                                    feed.action &&
                                    feed.action.review_group_animation
                                    ? feed.action.review_group_animation
                                      .review_group_count.insightful
                                    : 0}
                                </span>
                              </button>
                            </div>
                            <div className="reaction-holder">
                              {/* Show on reacting */}
                              <div className="reaction-anim" hidden>
                                <img
                                  src={require("../../../assets/images/lol_forever.gif")}
                                  alt=""
                                />
                              </div>

                              <button className="reaction lol" title="Lol">
                                <span className="icon">
                                  <img
                                    src={require("../../../assets/images/lol.png")}
                                    alt="lol"
                                  />
                                </span>
                                <span className="name">
                                  Lol
                                </span>
                                <span className="count">
                                  {feed &&
                                    feed.action &&
                                    feed.action.review_group_animation
                                    ? feed.action.review_group_animation
                                      .review_group_count.lol
                                    : 0}
                                </span>
                              </button>
                            </div>
                            <div className="reaction-holder">
                              {/* Show on reacting */}
                              <div className="reaction-anim" hidden>
                                <img
                                  src={require("../../../assets/images/bummer_forever.gif")}
                                  alt=""
                                />
                              </div>

                              <button
                                className="reaction bummer"
                                title="Bummer"
                              >
                                <span className="icon">
                                  <img
                                    src={require("../../../assets/images/bummer.png")}
                                    alt="bummer"
                                  />
                                </span>
                                <span className="name">
                                  Bummer
                                </span>
                                <span className="count">
                                  {feed &&
                                    feed.action &&
                                    feed.action.review_group_animation
                                    ? feed.action.review_group_animation
                                      .review_group_count.bummer
                                    : 0}
                                </span>
                              </button>
                            </div>
                          </div>

                          {feed.action && (
                            <div className="ml-auto align-self-start px-0 col-auto">
                              {/* <span className="mx-1">|</span> */}
                              <button
                                className="btn btn-link btn-sm text-muted"
                                onClick={() =>
                                  this.toggleReplyForm(feed.id, "L1")
                                }
                              >
                                REPLY
                              </button>
                            </div>
                          )}
                        </div>
                      </div>

                      {/* Show after hitting reply */}
                      {replyFormVisible["L1" + feed.id] && (
                        <div>
                          <Media className="media-post mt-3">
                            <Media>
                              <Media
                                object
                                src={
                                  profilePic
                                    ? profilePic
                                    : require("../../../assets/images/user.png")
                                }
                                alt="User Image"
                              />
                            </Media>
                            <Media body>
                              <FormGroup className="mb-0">
                                <Input
                                  bsSize="sm"
                                  className="mb-2"
                                  type="textarea"
                                  name="reply"
                                  value={this.state.addComment.body}
                                  onChange={this.handleOnChangeComment}
                                  placeholder="Write a Comment..."
                                />
                                <div className="text-right">
                                  <div className="text-right">
                                    <Button
                                      size="sm"
                                      color="primary"
                                      className=" mr-2"
                                      onClick={this.handleOnCommentSubmit(
                                        feed.id,
                                        "L1"
                                      )}
                                    >
                                      {" "}
                                      Submit
                                    </Button>
                                    <Button
                                      size="sm"
                                      color="light"
                                      className="ml-0"
                                      onClick={() =>
                                        this.toggleReplyForm(feed.id, "L1")
                                      }
                                    >
                                      {" "}
                                      Cancel{" "}
                                    </Button>
                                  </div>
                                </div>
                              </FormGroup>
                            </Media>
                          </Media>
                          <hr />
                        </div>
                      )}

                      {/* All Replies starts */}
                      {feed.comments &&
                        feed.comments.map((comment) => {
                          return (
                            <div key={comment.id}>
                              <Media className="media-post mt-3">
                                <Media>
                                  <img
                                    className='media-object'
                                    onError={(error) =>
                                      (error.target.src = require("../../../assets/images/user.png"))
                                    }
                                    src={
                                      comment.current_profile_pic
                                        ? comment.current_profile_pic
                                        : require("../../../assets/images/user.png")
                                    }
                                    alt="User Image"
                                  />
                                </Media>
                                <Media body>
                                  <Media heading>
                                    {comment.sender}{" "}
                                    <span className="time-stamp">
                                      <span>&middot;</span> {comment.sent_at}
                                    </span>
                                  </Media>

                                  {/* Comments */}
                                  <div>
                                    {ReactHtmlParser(
                                      comment.body.replace(/\n/g, "<br />")
                                    )}
                                  </div>

                                  {/* Quick reactionsn and REPLY button */}
                                  <div>
                                    <div className="d-flex">
                                      <div className="quick-reactions d-flex flex-wrap">
                                        <div className="reaction-holder">
                                          {/* Show on reacting */}
                                          <div className="reaction-anim" hidden>
                                            <img
                                              src={require("../../../assets/images/woohoo_forever.gif")}
                                              alt=""
                                            />
                                          </div>

                                          <button
                                            className="reaction woohoo"
                                            title="Woohoo"
                                          >
                                            <span className="icon">
                                              <img
                                                src={require("../../../assets/images/woohoo.png")}
                                                alt="woohoo"
                                              />
                                            </span>
                                            <span className="name">
                                              Woohoo
                                            </span>
                                            <span className="count">
                                              {
                                                comment.review_group_animation
                                                  .review_group_count.woohoo
                                              }
                                            </span>
                                          </button>
                                        </div>
                                        <div className="reaction-holder">
                                          {/* Show on reacting */}
                                          <div className="reaction-anim" hidden>
                                            <img
                                              src={require("../../../assets/images/nailedit_forever.gif")}
                                              alt=""
                                            />
                                          </div>
                                          <button
                                            className="reaction nailed"
                                            title="Nailed It"
                                          >
                                            <span className="icon">
                                              <img
                                                src={require("../../../assets/images/nailed-it.png")}
                                                alt="nailed it"
                                              />
                                            </span>
                                            <span className="name">
                                              Nailed It
                                            </span>
                                            <span className="count">
                                              {
                                                comment.review_group_animation
                                                  .review_group_count.nailed
                                              }
                                            </span>
                                          </button>
                                        </div>
                                        <div className="reaction-holder">
                                          {/* Show on reacting */}
                                          <div className="reaction-anim" hidden>
                                            <img
                                              src={require("../../../assets/images/insightful_forever.gif")}
                                              alt=""
                                            />
                                          </div>

                                          <button
                                            className="reaction insightful"
                                            title="Insightful"
                                          >
                                            <span className="icon">
                                              <img
                                                src={require("../../../assets/images/insightful.png")}
                                                alt="insightful"
                                              />
                                            </span>
                                            <span className="name">
                                              Insightful
                                            </span>
                                            <span className="count">
                                              {
                                                comment.review_group_animation
                                                  .review_group_count.insightful
                                              }
                                            </span>
                                          </button>
                                        </div>
                                        <div className="reaction-holder">
                                          {/* Show on reacting */}
                                          <div className="reaction-anim" hidden>
                                            <img
                                              src={require("../../../assets/images/lol_forever.gif")}
                                              alt=""
                                            />
                                          </div>

                                          <button
                                            className="reaction lol"
                                            title="Lol"
                                          >
                                            <span className="icon">
                                              <img
                                                src={require("../../../assets/images/lol.png")}
                                                alt="lol"
                                              />
                                            </span>
                                            <span className="name">
                                              Lol
                                            </span>
                                            <span className="count">
                                              {
                                                comment.review_group_animation
                                                  .review_group_count.lol
                                              }
                                            </span>
                                          </button>
                                        </div>
                                        <div className="reaction-holder">
                                          {/* Show on reacting */}
                                          <div className="reaction-anim" hidden>
                                            <img
                                              src={require("../../../assets/images/bummer_forever.gif")}
                                              alt=""
                                            />
                                          </div>

                                          <button
                                            className="reaction bummer"
                                            title="Bummer"
                                          >
                                            <span className="icon">
                                              <img
                                                src={require("../../../assets/images/bummer.png")}
                                                alt="bummer"
                                              />
                                            </span>
                                            <span className="name">
                                              Bummer
                                            </span>
                                            <span className="count">
                                              {
                                                comment.review_group_animation
                                                  .review_group_count.bummer
                                              }
                                            </span>
                                          </button>
                                        </div>
                                      </div>

                                      <div className="ml-auto align-self-start px-0 col-auto">
                                        {/* <span className="mx-1">|</span> */}
                                        <button
                                          className="btn btn-link btn-sm text-muted"
                                          onClick={() =>
                                            this.toggleReplyForm(
                                              comment.id,
                                              "L2"
                                            )
                                          }
                                        >
                                          REPLY
                                        </button>
                                        <Button
                                          size="sm"
                                          color="transparent"
                                          className="text-muted"
                                          onClick={() => {
                                            this.toggleEditForm(
                                              comment.id,
                                              "L2",
                                              comment.body
                                            );
                                          }}
                                        >
                                          {" "}
                                          <FontAwesomeIcon icon="pencil-alt" />{" "}
                                        </Button>
                                        <Button
                                          size="sm"
                                          color="transparent"
                                          className="ml-0 text-muted"
                                          onClick={() => {
                                            this.setState({
                                              deleteCommentId: comment.id,
                                            });
                                            this.confirmDeleteModalToggle();
                                          }}
                                        >
                                          {" "}
                                          <FontAwesomeIcon icon="trash-alt" />{" "}
                                        </Button>
                                      </div>
                                    </div>
                                  </div>

                                  {/* Show after hitting reply */}
                                  {replyFormVisible["L2" + comment.id] && (
                                    <div>
                                      <Media className="media-post mt-3">
                                        <Media>
                                          <Media
                                            object
                                            src={
                                              profilePic
                                                ? profilePic
                                                : require("../../../assets/images/user.png")
                                            }
                                            alt="User Image"
                                          />
                                        </Media>
                                        <Media body>
                                          <FormGroup className="mb-0">
                                            <Input
                                              bsSize="sm"
                                              className="mb-2"
                                              type="textarea"
                                              name="reply"
                                              onChange={
                                                this.handleOnChangeCommentReply
                                              }
                                              placeholder="Write a Comment..."
                                            />
                                            <div className="text-right">
                                              <div className="text-right">
                                                <Button
                                                  size="sm"
                                                  color="primary"
                                                  className=" mr-2"
                                                  onClick={this.handleOnCommentReplySubmit(
                                                    comment.id,
                                                    "L2"
                                                  )}
                                                >
                                                  {" "}
                                                  Submit
                                                </Button>
                                                <Button
                                                  size="sm"
                                                  color="light"
                                                  className="ml-0"
                                                  onClick={() =>
                                                    this.toggleReplyForm(
                                                      comment.id,
                                                      "L2"
                                                    )
                                                  }
                                                >
                                                  {" "}
                                                  Cancel{" "}
                                                </Button>
                                              </div>
                                            </div>
                                          </FormGroup>
                                        </Media>
                                      </Media>
                                      <hr />
                                    </div>
                                  )}

                                  {/* Show after hitting edit */}
                                  {editFormVisible["L2" + comment.id] && (
                                    <div>
                                      <Media className="media-post mt-3">
                                        <Media>
                                          <Media
                                            object
                                            src={
                                              profilePic
                                                ? profilePic
                                                : require("../../../assets/images/user.png")
                                            }
                                            alt="User Image"
                                          />
                                        </Media>
                                        <Media body>
                                          <FormGroup className="mb-0">
                                            <Input
                                              bsSize="sm"
                                              className="mb-2"
                                              type="textarea"
                                              name="reply"
                                              value={
                                                this.state.editComment.body
                                              }
                                              onChange={
                                                this.handleEditCommentOnChange
                                              }
                                            />
                                            <div className="text-right">
                                              <div className="text-right">
                                                <Button
                                                  size="sm"
                                                  color="primary"
                                                  className=" mr-2"
                                                  onClick={this.handleEditCommentOnSubmit(
                                                    comment.id,
                                                    "L2"
                                                  )}
                                                >
                                                  {" "}
                                                  Submit
                                                </Button>
                                                <Button
                                                  size="sm"
                                                  color="light"
                                                  className="ml-0"
                                                  onClick={() => {
                                                    this.toggleEditForm(
                                                      comment.id,
                                                      "L2"
                                                    );
                                                  }}
                                                >
                                                  {" "}
                                                  Cancel{" "}
                                                </Button>
                                              </div>
                                            </div>
                                          </FormGroup>
                                        </Media>
                                      </Media>
                                      <hr />
                                    </div>
                                  )}

                                  {comment.conversation &&
                                    comment.conversation.map((conversation) => {
                                      return (
                                        <div key={conversation.msg_id}>
                                          <Media className="media-post mt-3">
                                            <Media>
                                              <img
                                                className='media-object'
                                                onError={(error) =>
                                                  (error.target.src = require("../../../assets/images/user.png"))
                                                }
                                                src={
                                                  conversation.current_profile_pic
                                                    ? conversation.current_profile_pic
                                                    : require("../../../assets/images/user.png")
                                                }
                                                alt="User Image"
                                              />
                                            </Media>
                                            <Media body>
                                              <Media heading>
                                                {conversation.sender}{" "}
                                                <span className="time-stamp">
                                                  <span>&middot;</span>{" "}
                                                  {conversation.sent_at}
                                                </span>
                                              </Media>

                                              {/* Comments */}
                                              <div>
                                                {conversation.body}
                                                <div className="text-right">
                                                  <button
                                                    className="btn btn-link btn-sm text-muted"
                                                    onClick={() =>
                                                      this.toggleReplyForm(
                                                        conversation.msg_id,
                                                        "L3"
                                                      )
                                                    }
                                                  >
                                                    REPLY
                                                  </button>
                                                  <Button
                                                    size="sm"
                                                    color="transparent"
                                                    className="text-muted"
                                                    onClick={() => {
                                                      this.toggleEditForm(
                                                        conversation.msg_id,
                                                        "L3",
                                                        conversation.body
                                                      );
                                                    }}
                                                  >
                                                    {" "}
                                                    <FontAwesomeIcon icon="pencil-alt" />{" "}
                                                  </Button>
                                                  <Button
                                                    size="sm"
                                                    color="transparent"
                                                    className="ml-0 text-muted"
                                                    onClick={() => {
                                                      this.setState({
                                                        deleteCommentId:
                                                          conversation.msg_id,
                                                      });
                                                      this.confirmDeleteModalToggle();
                                                    }}
                                                  >
                                                    {" "}
                                                    <FontAwesomeIcon icon="trash-alt" />{" "}
                                                  </Button>
                                                </div>
                                              </div>

                                              {/* Show after hitting reply */}
                                              {replyFormVisible[
                                                "L3" + conversation.msg_id
                                              ] && (
                                                  <div>
                                                    <Media className="media-post mt-3">
                                                      <Media>
                                                        <Media
                                                          object
                                                          src={
                                                            profilePic
                                                              ? profilePic
                                                              : require("../../../assets/images/user.png")
                                                          }
                                                          alt="User Image"
                                                        />
                                                      </Media>
                                                      <Media body>
                                                        <FormGroup className="mb-0">
                                                          <Input
                                                            bsSize="sm"
                                                            className="mb-2"
                                                            type="textarea"
                                                            name="reply"
                                                            onChange={
                                                              this
                                                                .handleOnChangeCommentReply
                                                            }
                                                            placeholder="Write a Comment..."
                                                          />
                                                          <div className="text-right">
                                                            <div className="text-right">
                                                              <Button
                                                                size="sm"
                                                                color="primary"
                                                                className=" mr-2"
                                                                onClick={this.handleOnCommentReplySubmit(
                                                                  comment.id,
                                                                  "L3",
                                                                  conversation.msg_id
                                                                )}
                                                              >
                                                                {" "}
                                                              Submit
                                                            </Button>
                                                              <Button
                                                                size="sm"
                                                                color="light"
                                                                className="ml-0"
                                                                onClick={() =>
                                                                  this.toggleReplyForm(
                                                                    conversation.msg_id,
                                                                    "L3"
                                                                  )
                                                                }
                                                              >
                                                                {" "}
                                                              Cancel{" "}
                                                              </Button>
                                                            </div>
                                                          </div>
                                                        </FormGroup>
                                                      </Media>
                                                    </Media>
                                                    <hr />
                                                  </div>
                                                )}

                                              {/* Show after hitting edit */}
                                              {editFormVisible[
                                                "L3" + conversation.msg_id
                                              ] && (
                                                  <div>
                                                    <Media className="media-post mt-3">
                                                      <Media>
                                                        <Media
                                                          object
                                                          src={
                                                            profilePic
                                                              ? profilePic
                                                              : require("../../../assets/images/user.png")
                                                          }
                                                          alt="User Image"
                                                        />
                                                      </Media>
                                                      <Media body>
                                                        <FormGroup className="mb-0">
                                                          <Input
                                                            bsSize="sm"
                                                            className="mb-2"
                                                            type="textarea"
                                                            name="reply"
                                                            value={
                                                              this.state
                                                                .editComment.body
                                                            }
                                                            onChange={
                                                              this
                                                                .handleEditCommentOnChange
                                                            }
                                                          />
                                                          <div className="text-right">
                                                            <div className="text-right">
                                                              <Button
                                                                size="sm"
                                                                color="primary"
                                                                className=" mr-2"
                                                                onClick={this.handleEditCommentOnSubmit(
                                                                  conversation.msg_id,
                                                                  "L3"
                                                                )}
                                                              >
                                                                {" "}
                                                              Submit
                                                            </Button>
                                                              <Button
                                                                size="sm"
                                                                color="light"
                                                                className="ml-0"
                                                                onClick={() => {
                                                                  this.toggleEditForm(
                                                                    conversation.msg_id,
                                                                    "L3"
                                                                  );
                                                                }}
                                                              >
                                                                {" "}
                                                              Cancel{" "}
                                                              </Button>
                                                            </div>
                                                          </div>
                                                        </FormGroup>
                                                      </Media>
                                                    </Media>
                                                    <hr />
                                                  </div>
                                                )}
                                            </Media>
                                          </Media>
                                        </div>
                                      );
                                    })}
                                </Media>
                              </Media>
                            </div>
                          );
                        })}
                      {/* All Replies ends */}
                    </Media>
                  </Media>
                </div>
              );
            })
          ) : (
              <div className="bg-white p-3">
                <h2 className="text-secondary-dark">No Feeds to Display</h2>
              </div>
            )}
        </div>
        {/* Delete Confirmation Modal */}
        <Modal
          isOpen={this.state.confirmDeleteModal}
          toggle={this.confirmDeleteModalToggle}
          className="text-center"
        >
          <div className="bg-white modal-header">
            <h5 className="modal-title mx-auto text-dark">Confirmation</h5>
            <button
              className="btn btn-sm"
              aria-label="Close"
              onClick={this.confirmDeleteModalToggle}
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <ModalBody className="text-dark">
            Are you sure you want to delete?
          </ModalBody>
          <ModalFooter className="bg-white">
            <div className="text-center w-100">
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.confirmDeleteModalToggle}
              >
                Cancel
              </Button>
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.handleOnDeleteConfirmation}
              >
                Ok
              </Button>
            </div>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapState = (state) => ({
  feed_list: state.user.feed_list,
  loggedInUser: state.user.current_user,
});

const mapProps = {
  get_feed_list,
  add_feed_comment,
  add_feed_comment_reply,
  edit_feed_comment,
  delete_feed_comment,
};

export default connect(mapState, mapProps)(Feed);
