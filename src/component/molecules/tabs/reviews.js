import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Button,
  ButtonGroup,
  Row,
  Col,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  Media,
  FormGroup,
  Input,
  Badge,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Progress,
  CustomInput,
} from "reactstrap";
import {
  get_reviews_list,
  edit_reviews_discussion,
  add_reviews_discussion,
  delete_reviews_discussion,
  add_reviews_reply,
  get_album_types_list,
  get_album_type_data,
  delete_selected_gallery_media,
  get_reviews_list_entity_id,
  get_all_disputed_reviews,
  get_branch_data_by_circle_click
} from "../../../actions/user";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ReactHtmlParser from "react-html-parser";
import Label from "reactstrap/lib/Label";
import { callApi } from "../../../utils/apiCaller";
import CollapseBasic from '../../atoms/collapse';
import Moment from 'react-moment';
// import DeleteBtn from "../../atoms/deleteBtn";
import Loaderimg from "../../../assets/images/w-brand-black.jpg";
import DisputingReview from "../../atoms/disputeReview/disputingReview";
import DisputedReview from "../../atoms/disputeReview/disputedReview";
import Filteration from '../../atoms/Filteration';

class Reviews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterType: "new",
      pageNo: 1,
      editFormVisible: {},
      replyFormVisible: {},
      disputeReviewVisible: false,
      editDiscussion: {
        body: "",
      },
      addDiscussion: {},
      addReply: {
        body: "",
      },
      deleteId: 0,
      deleteType: "comment",
      confirmDeleteModal: false,
      reviewsList: {},
      profileData: null,
      corporateReviewCount: null,
      uploadMediaModal: false,
      selectedUpMediaType: "upload",
      showGalleryType: "images",
      uploadMedia: {
        mediaType: "image",
        albumType: "",
        pageNo: 1,
        albumTypesList: [],
        albumTypeData: {},
        selectedMedia: [],
        selectedMediaIds: [],
        embedLinks: {},
        uploadFiles: [],
        uploadedFiles: [],
        progress: 0,
      },
      disputeReview: {
        review_id: '',
        reason: 'This review is fake',
        comment: '',
        dispute_file: [],
      },
      reviewId: '',
      all_disputes_list: [],
      disputed_tab: false,
      user_names_list: [],
      get_dispute_data: [],
      showExplanation: false,
      disputeReviewId: '',
      showReply: false,
      dispute_files: [],
      dispute_imgepreview: [],
      dispute_reply_text: '',
      media_validation: false,
      comment_validation: false,
      disputer_name: {},
      days: undefined,
      hours: undefined,
      minutes: undefined,
      seconds: undefined,
      homeDropdownOpen: {},
      homeShareDropdownOpen: {},
      reviewAdded: '',
      is_media_dispute: false,
      can_vote: false,
      timer_started: false,
      is_administrative_review: false,

      confirmationToggle: false,
      confirmationReason: '',
      pk_id: '',
      postStatus: '',
      isLoading: false,

      showDisputeModal: false,
      showDisputedModal: false,
      reviewDetails: '',
      disputeModal: false,
      sortyBy: "Latest"

    };
  }

  componentWillReceiveProps(nextProps) {
    const { postStatus } = this.state
    //disputed reviews
    if (
      nextProps.get_all_disputes_data &&
      Object.keys(nextProps.get_all_disputes_data).length > 0) {
      this.setState({ all_disputes_list: nextProps.get_all_disputes_data })

      //storing names for disputers
      nextProps &&
        nextProps.get_all_disputes_data &&
        nextProps.get_all_disputes_data.map((review) => {
          if (review.is_review_flag) {
            callApi(
              `/api/disputes/?review_id=${review.id}`,
              "GET",
            ).then((response) => {
              if (response && response.code === 200) {
                response && response.results && response.results.map((res) => {
                  if (res.child_key_name === 1 || res.child_key_name === '1') {

                  }
                  else {
                    let rid = review.id
                    let dname = res.user_name
                    this.setState({
                      disputer_name: {
                        ...this.state.disputer_name,
                        [rid]: dname
                      }
                    });
                  }
                })
              }
            });
          }
        });

      if (postStatus === 'dispute_deleted') {
        this.setState({ postStatus: '', voteReviewModalToggleFour: false })
      }
      if (postStatus === 'dispute_added') {
        this.setState({ disputeThisReviewModalToggle: false })
      }
      this.setState({ isLoading: false })
    }

    //get reviews list
    if (
      nextProps.reviews_list &&
      Object.keys(nextProps.reviews_list).length > 0
    ) {

      this.setState({ disputed_tab: false })
      let addDiscussion = {};
      if (
        nextProps.reviews_list.results &&
        nextProps.reviews_list.results.length > 0
      ) {
        nextProps.reviews_list.results.forEach((review) => {
          addDiscussion[review.id] = "";
        });
      }
      this.setState({
        reviewsList: nextProps.reviews_list,
        profileData: nextProps.profile_data,
        corporateReviewCount: nextProps.corporate_review_count,
        addDiscussion: { ...addDiscussion },
      });

      if (postStatus === 'dispute_deleted') {
        this.setState({ postStatus: '', voteReviewModalToggleFour: false })
      }
      if (postStatus === 'dispute_added') {
        this.setState({ disputeThisReviewModalToggle: false })
      }
      this.setState({ isLoading: false })


    }
    if (nextProps.album_types_list && nextProps.album_types_list.length > 0) {
      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          albumTypesList: nextProps.album_types_list,
        },
        //uploadMediaModal: true,
        selectedUpMediaType: "upload",
        showGalleryType: "images",
      });
    }

    if (
      nextProps.album_type_data &&
      Object.keys(nextProps.album_type_data).length > 0
    ) {
      this.setState({
        ...this.state,
        //selectedUpMediaType: "gallery",
        uploadMedia: {
          ...this.state.uploadMedia,
          albumTypeData: nextProps.album_type_data,
        },
      });
    }

    if (this.state.disputeThisReviewModalToggle && nextProps?.get_review_status) {
      this.setState({
        disputeThisReviewModalToggle: false,
      });
    }

    if (this.state.disputeModal && nextProps?.get_dispute_modal_status?.modal === false) {
      this.setState({ disputeModal: false, showDisputeModal: false, reviewDetails: '', showDisputedModal: false })
      if (nextProps?.get_dispute_modal_status?.refresh) {
        this.setState({ disputed_tab: true, filterType: 'disputed' })
        this.props.get_all_disputed_reviews()
      }
    }
  }

  componentDidMount() {
    const { branch_id, get_reviews_list_entity_id, mapBranchClickData } = this.props
    if (this.props.circleClick && this.props.circleClick.topBarReview === false ||
      this.props.circleClick && this.props.circleClick.topBarDisputedReview === false) {
      // if (branch_id !== null) {
      //   get_reviews_list_entity_id(branch_id, 'new')
      // } else 
      if (mapBranchClickData !== true) {
        this.props.get_reviews_list(this.state.filterType, this.state.pageNo);
      } else {
        const { dotSelectedType, dotSelectedColor, dotSelectedName, dotSelectedDays, dotSelectedqueryType, dotSelectedBranchId } = this.props;
        let data_type = { 'reviews': '4', 'message': '5', 'qa': '7', 'feedback': '6', 'disputed_Review': '4' }
        this.props.get_branch_data_by_circle_click({
          color: dotSelectedColor,
          type: dotSelectedType,
          days: dotSelectedDays,
          queryType: dotSelectedqueryType,
          branchID: dotSelectedBranchId,
        });
      }
    }

  }

  handleOnFileUploadChange = (event) => {
    let uploadFiles = event.target.files;
    let showFiles = [];
    for (const key of Object.keys(uploadFiles)) {
      showFiles.push({ id: "", url: URL.createObjectURL(uploadFiles[key]) });
    }
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        uploadFiles: showFiles,
        progress: 0,
      },
    });
    let progressPart = 100 / showFiles.length;
    let progress = 0;
    for (const key of Object.keys(uploadFiles)) {
      let data = new FormData();
      data.append("file", uploadFiles[key]);

      callApi(
        `/upload/multiuploader/?album=feeds&instance=post&image=undefined&doc=undefined`,
        "POST",
        data,
        true
      ).then((response) => {
        this.handleOnClickSelectGalleryMedia(response);
        if (showFiles.length === 1 || key === showFiles.length - 1) {
          progress = 100;
        } else {
          progress = progress + progressPart;
        }
        showFiles[key].id = response.id;
        this.setState({
          ...this.state,
          uploadMedia: {
            ...this.state.uploadMedia,
            progress: progress,
            uploadedFiles: [...this.state.uploadMedia.uploadedFiles, response],
            uploadedFiles: showFiles,
          },
        });
      });
    }
  };
  handleOnClickRemoveSelectedMedia = (id) => () => {
    let removeMediaIds = [];
    removeMediaIds.push(id);
    this.props.delete_selected_gallery_media(removeMediaIds);
    let uploadFiles = this.state.uploadMedia.uploadFiles.filter(
      (file) => file.id !== id
    );
    let uploadedFiles = this.state.uploadMedia.uploadedFiles.filter(
      (file) => file.id !== id
    );
    let selectedMediaIds = this.state.uploadMedia.selectedMediaIds.filter(
      (item) => item !== id
    );
    let selectedMedia = this.state.uploadMedia.selectedMedia.filter(
      (file) => file.id !== id
    );
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: selectedMedia,
        selectedMediaIds: selectedMediaIds,
        uploadedFiles: uploadedFiles,
        uploadFiles: uploadFiles,
      },
    });
  };
  handleOnClickRemoveSelectedGalleryMedia = (media) => {
    let removeMediaIds = [];
    removeMediaIds.push(media.id);
    let index = this.state.uploadMedia.selectedMediaIds.indexOf(media.id);
    let newSelectedMediaIds;
    let newSelectedMedia;
    if (index !== -1) {
      newSelectedMediaIds = this.state.uploadMedia.selectedMediaIds.filter(
        (item) => item !== media.id
      );
      newSelectedMedia = this.state.uploadMedia.selectedMedia.filter(
        (item) => item.id !== media.id
      );
      this.props.delete_selected_gallery_media(removeMediaIds);
      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          selectedMedia: newSelectedMedia,
          selectedMediaIds: newSelectedMediaIds,
        },
      });
    }
  };
  handleOnClickSelectGalleryMedia = (media) => {
    let index = this.state.uploadMedia.selectedMediaIds.indexOf(media.id);
    let newSelectedMediaIds;
    let newSelectedMedia;
    if (index !== -1) {
      newSelectedMediaIds = this.state.uploadMedia.selectedMediaIds.filter(
        (item) => item !== media.id
      );
      newSelectedMedia = this.state.uploadMedia.selectedMedia.filter(
        (item) => item.id !== media.id
      );
    } else {
      newSelectedMediaIds = [
        ...this.state.uploadMedia.selectedMediaIds,
        media.id,
      ];
      newSelectedMedia = [...this.state.uploadMedia.selectedMedia, media];
    }
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: newSelectedMedia,
        selectedMediaIds: newSelectedMediaIds,
      },
    });
  };
  handleOnClickGalleryType = (type) => {
    let mediaType = "";
    if (type === "images") {
      mediaType = "image";
    } else if (type === "videos") {
      mediaType = "video";
    }
    this.props.get_album_type_data(mediaType, "", 1);
    this.setState({
      ...this.state,
      selectedUpMediaType: "gallery",
      showGalleryType: type,
      uploadMedia: {
        ...this.state.uploadMedia,
        mediaType: mediaType,
        albumType: "",
        pageNo: 1,
        //selectedMedia: [],
        //selectedMediaIds: [],
      },
    });
  };
  handleOnClickAlbumTypeChange = (e) => {
    this.props.get_album_type_data("image", e.target.value, 1);
    this.setState({
      ...this.state,
      selectedUpMediaType: "gallery",
      showGalleryType: "images",
      uploadMedia: {
        ...this.state.uploadMedia,
        mediaType: "image",
        albumType: e.target.value,
        pageNo: 1,
        //selectedMedia: [],
        //selectedMediaIds: [],
      },
    });
  };
  handleOnClickSelectedUploadMediaType = (type) => {
    if (type === "gallery") {
      this.props.get_album_type_data("image", "", 1);
      this.setState({
        ...this.state,
        selectedUpMediaType: type,
        showGalleryType: "images",
        uploadMedia: {
          ...this.state.uploadMedia,
          mediaType: "image",
          albumType: "",
          pageNo: 1,
          //selectedMedia: [],
          //selectedMediaIds: [],
          embedLinks: {},
          progress: 0,
        },
      });
    } else if (type === "upload") {
      this.setState({
        ...this.state,
        selectedUpMediaType: type,
        uploadMedia: {
          ...this.state.uploadMedia,
          //selectedMedia: [],
          //selectedMediaIds: [],
          embedLinks: {},
          progress: 0,
        },
      });
    } else if (type === "embed") {
      let embedLinks = {
        0: "",
        1: "",
      };
      this.setState({
        ...this.state,
        selectedUpMediaType: type,
        uploadMedia: {
          ...this.state.uploadMedia,
          selectedMedia: [],
          selectedMediaIds: [],
          embedLinks: embedLinks,
        },
      });
    }
  };
  embedLinkOnChange = (id) => (e) => {
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        embedLinks: {
          ...this.state.uploadMedia.embedLinks,
          [id]: e.target.value,
        },
      },
    });
  };
  mapEmbedLinks = () => {
    let embedKeys = Object.keys(this.state.uploadMedia.embedLinks);
    return embedKeys.map((emKey) => {
      return (
        <div className="d-flex mx-n2 mb-2" key={emKey}>
          <div className="flex-fill px-2">
            <Input
              type="url"
              bsSize="sm"
              className="bg-white"
              value={this.state.uploadMedia.embedLinks[emKey]}
              onChange={this.embedLinkOnChange(emKey)}
              placeholder="Embeded link"
            />
          </div>
          <div className="px-2">
            <Button
              title="Remove"
              color="danger"
              size="sm"
              hidden={emKey <= 1 ? true : false}
              onClick={() => {
                this.deleteEmbedLinkRow(emKey);
              }}
            >
              <FontAwesomeIcon icon="minus" />
            </Button>
          </div>
        </div>
      );
    });
  };
  deleteEmbedLinkRow = (emKey) => {
    let embedLinks = this.state.uploadMedia.embedLinks;
    if (Object.keys(embedLinks).indexOf(emKey) !== -1) {
      delete embedLinks[emKey];
      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          embedLinks: embedLinks,
        },
      });
    }
  };
  addEmbedlinkRow = () => {
    let newEmbedLinks = {};
    for (let i = 2; i < 5; i += 1) {
      if (
        Object.keys(this.state.uploadMedia.embedLinks).indexOf(i.toString()) ===
        -1
      ) {
        newEmbedLinks[i] = "";
        break;
      }
    }
    if (Object.keys(newEmbedLinks).length > 0) {
      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          embedLinks: {
            ...this.state.uploadMedia.embedLinks,
            ...newEmbedLinks,
          },
        },
      });
    }
  };
  handleOnClickUploadMedia = () => {
    this.props.get_album_types_list();
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        mediaType: "image",
        albumType: "",
        pageNo: 1,
        albumTypeData: {},
        embedLinks: {},
        progress: 0,
        uploadFiles: [],
        uploadedFiles: [],
      },
      uploadMediaModal: true,
      selectedUpMediaType: "upload",
      showGalleryType: "images",
    });
  };
  uploadMediaModalToggle = () => {
    this.setState({
      ...this.state,
      uploadMediaModal: !this.state.uploadMediaModal,
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: [],
        selectedMediaIds: [],
        mediaType: "image",
        albumType: "",
        pageNo: 1,
        albumTypeData: {},
        //albumTypesList: [],
        embedLinks: {},
        uploadedFiles: [],
        uploadFiles: [],
      },
      selectedUpMediaType: "upload",
      showGalleryType: "images",
    });
  };
  insertEmbedLinks = () => {
    //let post = this.state.addPost;
    let embedValues = Object.values(this.state.uploadMedia.embedLinks).filter(
      (item) => item !== ""
    );
    //if (post.body && post.body.replace(/(<([^>]+)>)/gi, "") !== "") {
    // if (embedValues.length > 0) {
    //   embedValues = embedValues.map((item) => "<p>" + item + "</p>");
    //   post.body = post.body + embedValues.join("");
    //   post.body_preview = post.body;
    // }
    // this.setState({
    //   ...this.state,
    //   addPost: post
    // });
    //} else {
    if (embedValues.length > 0) {
      embedValues = embedValues.map((item) => "<p>" + item + "</p>");
      // let newPost = {
      //   video: embedValues.join("\n"),
      //   exclude_list: [],
      //   question_rating_category: [],
      //   messagemedia_set: [],
      // };

      //this.props.add_my_post(newPost, this.state.filterType, this.state.pageNo);

      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          selectedMedia: [],
          selectedMediaIds: [],
          embedLinks: {},
        },
        uploadMediaModal: false,
      });
    }
    //}
  };
  truncate = (filenameString) => {
    // let split = filenameString.split(".");
    let filename = filenameString.substr(0, filenameString.lastIndexOf("."));
    let extension = filenameString.substr(
      filenameString.lastIndexOf("."),
      filenameString.length - 1
    );
    let partial = filename.substring(filename.length - 3, filename.length);
    filename = filename.substring(0, 15);
    return filename + "..." + partial + extension;
  };
  handleOnClickToggleDisputeReview = (visibility, id, reason) => {
    const { disputeReview } = this.state
    let disputeReviewVisible;
    if (visibility === "open") {
      disputeReviewVisible = true;
      disputeReview['review_id'] = id.id
      disputeReview['reason'] = reason
      this.setState({ disputeReview })
    } else {
      disputeReviewVisible = false;
    }
    this.setState({
      ...this.state,
      disputeReviewVisible: disputeReviewVisible,
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: [],
        selectedMediaIds: [],
        mediaType: "image",
        albumType: "",
        pageNo: 1,
        albumTypeData: {},
        //albumTypesList: [],
        embedLinks: {},
        uploadedFiles: [],
        uploadFiles: [],
      },
      selectedUpMediaType: "upload",
      showGalleryType: "images",
    });
  };
  confirmDeleteModalToggle = () => {
    if (this.state.deleteId) {
      this.setState({ deleteId: 0 });
    }
    this.setState({ confirmDeleteModal: !this.state.confirmDeleteModal });
  };

  handleOnDeleteConfirmation = () => {
    if (this.state.deleteId) {
      this.props.delete_reviews_discussion(
        this.state.deleteId,
        this.state.deleteType,
        this.state.filterType,
        this.state.pageNo
      );
      this.setState({
        confirmDeleteModal: !this.state.confirmDeleteModal,
        deleteId: 0,
      });
    }
  };
  handleOnClickReviewType = (filterType) => {
    const { branch_id, get_reviews_list_entity_id } = this.props
    this.setState({ disputed_tab: false, all_disputes_list: [] })
    if (filterType === 'disputed') {
      this.setState({ disputed_tab: true, filterType: filterType })
      this.props.get_all_disputed_reviews()
    }
    else {
      this.setState({ all_disputes_list: [], disputed_tab: false })
      if (branch_id !== null) {
        get_reviews_list_entity_id(branch_id, filterType)
        this.setState({
          ...this.state,
          filterType: filterType,
        });
      }
      else {
        this.props.get_reviews_list(filterType, this.state.pageNo);
        this.setState({
          ...this.state,
          filterType: filterType,
        });
      }
    }
  };

  toggleEditForm = (id, level, newBody = "") => {
    if (this.state.editFormVisible[level + id]) {
      newBody = "";
    }
    this.setState({
      ...this.state,
      editFormVisible: {
        [level + id]: !this.state.editFormVisible[level + id],
      },
      replyFormVisible: {},
      editDiscussion: {
        body: newBody,
      },
      addReply: {
        body: "",
      },
    });
  };
  toggleReplyForm = (id, level) => {
    this.setState({
      ...this.state,
      editFormVisible: {},
      replyFormVisible: {
        [level + id]: !this.state.replyFormVisible[level + id],
      },
      addReply: {
        body: "",
      },
    });
  };
  handleOnAddReplyChange = (e) => {
    this.setState({
      ...this.state,
      addReply: {
        ...this.state.addReply,
        body: e.target.value,
      },
    });
  };
  handleOnAddReplySubmit = (id, level, reviewId, replyId = 0) => (e) => {
    e.preventDefault();
    const data = {
      parent_id: id,
      media: [],
      review: reviewId,
    };
    let reply = this.state.addReply;
    if (reply.body !== "") {
      reply = { body: this.state.addReply.body, ...data };
      this.props.add_reviews_reply(
        reply,
        this.state.filterType,
        this.state.pageNo
      );
      if (replyId === 0) {
        this.setState({
          ...this.state,
          addReply: {
            body: "",
          },
          replyFormVisible: {
            [level + id]: !this.state.replyFormVisible[level + id],
          },
        });
      } else {
        this.setState({
          ...this.state,
          addReply: {
            body: "",
          },
          replyFormVisible: {
            [level + replyId]: !this.state.replyFormVisible[level + replyId],
          },
        });
      }
    }
  };
  handleOnAddDiscussionChange = (id) => (e) => {
    this.setState({
      ...this.state,
      addDiscussion: {
        ...this.state.addDiscussion,
        [id]: e.target.value,
      },
    });
  };
  handleOnAddDiscussionSubmit = (id) => (e) => {
    const { mapBranchClickData } = this.props

    e.preventDefault();
    const data = {
      review: id,
      media: [],
    };
    let discussion = {};

    if (this.state.addDiscussion[id] !== "") {
      discussion = { body: this.state.addDiscussion[id], ...data };

      this.props.add_reviews_discussion(
        discussion,
        this.state.filterType,
        this.state.pageNo,
        mapBranchClickData
      );
      this.setState({
        ...this.state,
        addDiscussion: {
          ...this.state.addDiscussion,
          [id]: "",
        },
      });
    }
  };

  handleEditDiscussionOnChange = (e) => {
    this.setState({
      ...this.state,
      editDiscussion: {
        ...this.state.editDiscussion,
        body: e.target.value,
      },
    });
  };
  handleEditDiscussionOnSubmit = (discussionId, level, reviewId) => (e) => {
    e.preventDefault();
    if (this.state.editDiscussion.body !== "") {
      let discussion = {
        body: this.state.editDiscussion.body,
        review: reviewId,
        media: [],
        parent_id: discussionId,
      };
      this.props.edit_reviews_discussion(
        discussion,
        this.state.filterType,
        this.state.pageNo
      );
      this.setState({
        ...this.state,
        editDiscussion: {
          body: "",
        },
        addDiscussion: {
          ...this.state.addDiscussion,
          [reviewId]: "",
        },
        editFormVisible: {
          [level + discussionId]: !this.state.editFormVisible[
            level + discussionId
          ],
        },
      });
    }
  };
  componentWillUnmount() {
    if (this.props.circleClick && this.props.circleClick.topBarReview) {
      this.props.circleClick.topBarReview = false;
      this.props.circleClick.topBarReviewColor = null;
    }
    if (this.props.circleClick && this.props.circleClick.topBarDisputedReview) {
      this.props.circleClick.topBarDisputedReview = false;
      this.props.circleClick.topBarDisputedReviewColor = null;
    }
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  handleChange = (e) => {
    const { disputeReview } = this.state
    disputeReview[e.target.name] = e.target.value
    this.setState({ disputeReview })
    this.disputeValidate()
  }

  // handleHeatedDiscussion = (feed) => {
  //   this.setState({ voteReviewModalToggleFour: true, disputeReviewId: feed.id });
  //   this.setState({
  //     reviewAdded: feed,
  //     timer_started: false,
  //   })
  //   this.props.get_dispute_discussion(feed.id);
  //   this.timerSettings();    
  // }

  handleHeatedDiscussion = (feed) => {
    this.setState({ showDisputedModal: true, reviewDetails: feed, disputeModal: true })
    return
  };

  // handleDeleteDispute=()=> {
  //   this.setState({voteReviewModalToggleFour: false})
  //   this.props.delete_dispute(this.state.disputeReviewId)  
  // }


  handleDispteThisReview = (review) => {
    this.setState({ showDisputeModal: true, reviewDetails: review, disputeModal: true })
  }

  sortbyfunction = (sortValue) => {
    let { reviewsList } = this.state;
    if (sortValue && sortValue == "Oldest") {
      reviewsList.results.sort((a, b) => a.id - b.id);
    } else {
      reviewsList.results.sort((a, b) => b.id - a.id);
    }
    this.setState({ reviewsList })

  }


  render() {
    const { profileData, reviewsList, disputeReview: { reason, comment }, is_media_dispute,
      get_dispute_data, isLoading, showDisputeModal, reviewDetails, showDisputedModal } = this.state;
    const { circleClick, mapBranchClickData } = this.props;
    let user_name = "";
    if (profileData && profileData.user) {
      if (
        profileData.user &&
        profileData.user.first_name &&
        profileData.user.last_name
      ) {
        user_name = `${profileData.user.first_name} ${profileData.user.last_name}`;
      }
    }

    return (
      <React.Fragment>
        { showDisputeModal ?
          <DisputingReview review={reviewDetails} />
          :
          ''
        }

        { showDisputedModal ?
          <DisputedReview review={reviewDetails} />
          :
          ''
        }
        {mapBranchClickData === true ?
          <Filteration
            dotSelectedType={this.props.dotSelectedType}
            dotSelectedColor={this.props.dotSelectedColor}
            dotSelectedName={this.props.dotSelectedName}
            dotSelectedDays={this.props.dotSelectedDays}
            dotSelectedqueryType={this.props.dotSelectedqueryType}
            dotSelectedBranchId={this.props.dotSelectedBranchId}
            sortbyfunction={this.sortbyfunction}
          /> : ""}
        <div className="text-primary-dark">
          {this.props.circleClick && this.props.circleClick.topBarReview ||
            this.props.circleClick && this.props.circleClick.topBarDisputedReview ? ""
            : <div
              hidden={mapBranchClickData === true ? true : false}
              className="mb-3">
              <ButtonGroup className="type-filter flex-wrap" size="sm">
                <div className="item d-flex align-items-center">
                  <Button
                    color="transparent"
                    onClick={() => this.handleOnClickReviewType("new")}
                    active={this.state.filterType === "new"}
                  >
                    New{" "}
                    {this.state.corporateReviewCount && this.state.corporateReviewCount.new_count > 0 &&
                      (
                        <Badge color="primary">
                          {this.state.corporateReviewCount.new_count}
                        </Badge>
                      )}
                  </Button>
                </div>
                <div className="item d-flex align-items-center">
                  <Button
                    color="transparent"
                    onClick={() => this.handleOnClickReviewType("answered")}
                    active={this.state.filterType === "answered"}
                  >
                    Answered{" "}
                    {this.state.corporateReviewCount && this.state.corporateReviewCount.answered_count > 0 &&
                      (
                        <Badge color="primary">
                          {this.state.corporateReviewCount.answered_count}
                        </Badge>
                      )}
                  </Button>
                </div>
                <div className="item d-flex align-items-center">
                  <Button

                    color="transparent"
                    onClick={() => this.handleOnClickReviewType("disputed")}
                    active={this.state.filterType === "disputed"}
                  >
                    Disputed{" "}
                    {this.state.corporateReviewCount && this.state.corporateReviewCount.disputed_count > 0 &&
                      (
                        <Badge color="danger">
                          {this.state.corporateReviewCount.disputed_count}
                        </Badge>
                      )}
                  </Button>
                </div>
              </ButtonGroup>
            </div>}

          {/* Show Disputed Reviews here */}
          {this.state.disputed_tab ?
            <div >
              {/* Repeat this */}
              {this.state.all_disputes_list.map((review) => {

                return (
                  <div className="mb-3">

                    {
                      review.is_review_flag ?
                        circleClick && circleClick.topBarReview ||
                          circleClick && circleClick.topBarDisputedReview ?
                          <div className={
                            circleClick && circleClick.topBarReviewColor === 'red' ||
                              circleClick && circleClick.topBarDisputedReviewColor === 'red' ?
                              "bg-danger text-dark p-2" :
                              circleClick && circleClick.topBarReviewColor === 'orange' ||
                                circleClick && circleClick.topBarDisputedReviewColor === 'orange' ?
                                "bg-warning text-dark p-2" :
                                circleClick && circleClick.topBarReviewColor === 'green' ||
                                  circleClick && circleClick.topBarDisputedReviewColor === 'green' ?
                                  "bg-success text-dark p-2" : ""
                          }>

                          </div> : "" : ""}
                    <div className="bg-white p-3 mb-1" key={review.id}>
                      <Row>
                        <Col sm={4} md={3}>
                          <div>
                            <div className="mb-3">
                              <a href="#" className="text-decoration-none">
                                <img
                                  className="d-block mb-2"
                                  width="48"
                                  src={
                                    review.created_user_media &&
                                      review.created_user_media.url
                                      ? review.created_user_media.url
                                      : require("../../../assets/images/user-circle.png")
                                  }
                                  alt="User Image"
                                />
                                <div>
                                  <span className="text-secondary-dark font-weight-bold d-block">
                                    {review.created_user_media &&
                                      `${review.created_user_media.user.user.first_name} ${review.created_user_media.user.user.last_name}`}
                                  </span>
                                  <small className="text-primary d-block">
                                    {review.created_by.present_address
                                      ? `${review.created_by.present_address.city}, ${review.created_by.present_address.state}`
                                      : ""}
                                  </small>
                                </div>
                              </a>
                            </div>

                            <div>
                              <a
                                href="#"
                                className="text-decoration-none text-primary"
                              >
                                <span className="font-weight-bold">
                                  {review.created_review_user_count ? review.created_review_user_count : 0}
                                </span>{" "}
                                <span>{review.created_review_user_count && review.created_review_user_count == 1 ? 'Review' : 'Reviews'}</span>
                              </a>
                            </div>

                            <div className="mt-3">
                              <ul className="list-unstyled d-flex">
                                <li title="Language 1">
                                  <img
                                    src={require("../../../assets/images/lang-01.png")}
                                    alt="Language"
                                  />
                                </li>
                                <li title="Language 2">
                                  <img
                                    src={require("../../../assets/images/lang-02.png")}
                                    alt="Language"
                                  />
                                </li>
                                <li title="Language 3">
                                  <img
                                    src={require("../../../assets/images/lang-03.png")}
                                    alt="Language"
                                  />
                                </li>
                              </ul>
                            </div>
                          </div>
                        </Col>
                        <Col sm={8} md={9}>



                          <div className="d-flex mb-4">
                            <div className="mr-2">
                              <img
                                className="mr-3"
                                title={review.ratings_sets.avg_rating[1]}

                                src={
                                  review.ratings_sets.avg_rating[1]
                                    ?
                                    Math.floor(
                                      review.ratings_sets.avg_rating[1]) === 0
                                      ?
                                      require(`../../../assets/images/norating.png`)
                                      :
                                      require(`../../../assets/images/${Math.floor(
                                        review.ratings_sets.avg_rating[1]
                                      )}rating.png`)
                                    :
                                    require(`../../../assets/images/norating.png`)
                                }
                              />
                              <span className="fs-14 font-weight-bold">
                                Overall Rating
                        </span>
                              <div className="mt-2 fs-14">
                                <i className="font-weight-bold">
                                  This review was written
                          </i>
                                <span> {review.review_date_time}</span>
                              </div>
                            </div>

                          </div>

                          {/* Dispute review block */}
                          <div
                            className="border border-secondary p-4 mb-3"
                            hidden={this.state.disputeReviewVisible ? false : true}
                          >
                            <div className="mb-3">
                              <Label
                                for="disputed_review"
                                className="text-primary-dark fs-14 font-weight-bold"
                              >
                                {" "}
                          Provide evidence and explanation for disputing the
                          review in question
                        </Label>
                              <Input
                                type="textarea"
                                className="primary"
                                name="disputed_review"
                                id="disputed_review"
                                onChange={this.handleChange}
                                placeholder="Provide explanation1...."
                              />
                            </div>
                            <Row
                              className="mb-3"
                              form
                              hidden={
                                this.state.uploadMedia.selectedMedia.length > 0
                                  ? false
                                  : true
                              }
                            >
                              {this.state.uploadMedia.selectedMedia.length > 0 &&
                                this.state.uploadMedia.selectedMedia.map((item) => {
                                  return (
                                    <Col xs="auto" className="mb-3">
                                      <div className="d-flex pr-3 pt-3">
                                        <div>
                                          {(item.type === "image" ||
                                            item.media_type === "image" ||
                                            item.type === "video" ||
                                            item.media_type === "video") && (
                                              <div className="gallery-media">
                                                <img
                                                  src={
                                                    item.file ? item.file : item.url
                                                  }
                                                  alt={
                                                    item.filename
                                                      ? item.filename.length < 20
                                                        ? item.filename
                                                        : this.truncate(item.filename)
                                                      : item.name
                                                        ? item.name.length < 20
                                                          ? item.name
                                                          : this.truncate(item.name)
                                                        : ""
                                                  }
                                                />
                                              </div>
                                            )}
                                        </div>
                                        <div className="mx-n3 mt-n3">
                                          <Button
                                            color="dark"
                                            size="sm"
                                            title="Remove Media"
                                            onClick={() =>
                                              this.handleOnClickRemoveSelectedGalleryMedia(
                                                item
                                              )
                                            }
                                          >
                                            <FontAwesomeIcon icon="minus" size="sm" />{" "}
                                          </Button>
                                        </div>
                                      </div>
                                    </Col>
                                  );
                                })}
                            </Row>
                            <div className="d-flex mx-n2">
                              <div className="px-2">
                                <Button
                                  color="white"
                                  size="sm"
                                  onClick={() => this.handleOnClickUploadMedia()}
                                >
                                  <FontAwesomeIcon icon="camera" />
                                </Button>
                              </div>
                              <div className="px-2 ml-auto">
                                <Button color="primary" size="sm" className="mw"
                                  onClick={this.handleDisputeSubmit}>
                                  Submit
                          </Button>
                                <Button
                                  color="light"
                                  size="sm"
                                  className="mw"
                                  onClick={() =>
                                    this.handleOnClickToggleDisputeReview("close")
                                  }
                                >
                                  Cancel
                          </Button>
                              </div>
                            </div>
                          </div>

                          <div>
                            <div className="mb-4">
                              <div className="py-2 fs-14 text-primary-dark">
                                <FontAwesomeIcon
                                  className="mr-2"
                                  size="lg"
                                  icon={["fas", "quote-left"]}
                                />
                                <span className="comment-body-renderer">{ReactHtmlParser(review.review)}</span>
                                <FontAwesomeIcon
                                  className="ml-2"
                                  size="lg"
                                  icon={["fas", "quote-right"]}
                                />
                              </div>
                              <div className="fs-14">
                                <span className="font-weight-bold text-muted">
                                  {review.review_group_count.helpful}
                                </span>{" "}
                                <i>people found this review helpful</i>
                              </div>
                            </div>

                            {/* Answered Reviews list */}
                            {review.review_discussion &&
                              review.review_discussion.length > 0 &&
                              review.review_discussion.map((discussion) => {
                                return (
                                  <div key={discussion.id}>
                                    <Media className="media-post mt-3">
                                      <Media>
                                        <Media
                                          object
                                          src={
                                            discussion.sender.current_profile_pic
                                              ? discussion.sender
                                                .current_profile_pic
                                              : require("../../../assets/images/user.png")
                                          }
                                          alt="User Image"
                                        />
                                      </Media>
                                      <Media body>
                                        <Media heading>
                                          <div className="d-flex">
                                            <div className="align-self-center">
                                              {discussion.sender.full_name}
                                              <span className="time-stamp">
                                                <span>&middot;</span>{" "}
                                                {discussion.sent_at}
                                              </span>
                                            </div>
                                            <div className="ml-auto">
                                              <Button
                                                size="sm"
                                                color="transparent"
                                                className="text-muted"
                                                onClick={() => {
                                                  this.toggleReplyForm(
                                                    discussion.id,
                                                    "L1"
                                                  );
                                                }}
                                              >
                                                Reply
                                        </Button>
                                              <Button
                                                size="sm"
                                                color="transparent"
                                                className="text-muted"
                                                onClick={() => {
                                                  this.toggleEditForm(
                                                    discussion.id,
                                                    "L1",
                                                    discussion.body
                                                  );
                                                }}
                                                hidden={
                                                  discussion.sender.id !==
                                                    this.props.profile_data.id
                                                    ? true
                                                    : false
                                                }
                                              >
                                                {" "}
                                                <FontAwesomeIcon icon="pencil-alt" />{" "}
                                              </Button>
                                              <Button
                                                size="sm"
                                                color="transparent"
                                                className="ml-0 text-muted"
                                                onClick={() => {
                                                  this.setState({
                                                    deleteId: discussion.id,
                                                  });
                                                  this.confirmDeleteModalToggle();
                                                }}
                                                hidden={
                                                  discussion.sender.id !==
                                                    this.props.profile_data.id
                                                    ? true
                                                    : false
                                                }
                                              >
                                                {" "}
                                                <FontAwesomeIcon icon="trash-alt" />{" "}
                                              </Button>
                                            </div>
                                          </div>
                                        </Media>

                                        {/* Show Comment, hide when editing */}
                                        <div>{discussion.body}</div>

                                        {/* Show when editing comment */}
                                        {this.state.editFormVisible[
                                          "L1" + discussion.id
                                        ] && (
                                            <div>
                                              <div className="mt-3">
                                                <Input
                                                  bsSize="sm"
                                                  className="mb-2"
                                                  type="textarea"
                                                  name="body"
                                                  value={
                                                    this.state.editDiscussion.body
                                                  }
                                                  placeholder="Edit Comment..."
                                                  onChange={
                                                    this.handleEditDiscussionOnChange
                                                  }
                                                />
                                                <div className="text-right">
                                                  <div className="text-right">
                                                    <Button
                                                      size="sm"
                                                      color="light"
                                                      onClick={() => {
                                                        this.toggleEditForm(
                                                          discussion.id,
                                                          "L1"
                                                        );
                                                      }}
                                                    >
                                                      {" "}
                                              Cancel{" "}
                                                    </Button>
                                                    <Button
                                                      size="sm"
                                                      color="primary"
                                                      onClick={this.handleEditDiscussionOnSubmit(
                                                        discussion.id,
                                                        "L1",
                                                        review.id
                                                      )}
                                                    >
                                                      {" "}
                                              Update{" "}
                                                    </Button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          )}

                                        {this.state.replyFormVisible[
                                          "L1" + discussion.id
                                        ] && (
                                            <div>
                                              <hr />
                                              <Media className="media-post mt-3">
                                                <Media>
                                                  <Media
                                                    object
                                                    src={
                                                      this.state.profileData &&
                                                        this.state.profileData
                                                          .current_profile_file
                                                        ? this.state.profileData
                                                          .current_profile_file
                                                        : require("../../../assets/images/user.png")
                                                    }
                                                    alt="User Image"
                                                  />
                                                </Media>
                                                <Media body>
                                                  <FormGroup className="mb-0">
                                                    <Input
                                                      bsSize="sm"
                                                      className="mb-2"
                                                      type="textarea"
                                                      name="reply"
                                                      value={this.state.addReply.body}
                                                      onChange={
                                                        this.handleOnAddReplyChange
                                                      }
                                                      placeholder="Write a Reply..."
                                                    />
                                                    <div className="text-right">
                                                      <div className="text-right">
                                                        <Button
                                                          size="sm"
                                                          color="light"
                                                          onClick={() => {
                                                            this.toggleReplyForm(
                                                              discussion.id,
                                                              "L1"
                                                            );
                                                          }}
                                                        >
                                                          {" "}
                                                  Cancel{" "}
                                                        </Button>
                                                        <Button
                                                          size="sm"
                                                          color="primary"
                                                          onClick={this.handleOnAddReplySubmit(
                                                            discussion.id,
                                                            "L1",
                                                            review.id
                                                          )}
                                                        >
                                                          Reply
                                                </Button>
                                                      </div>
                                                    </div>
                                                  </FormGroup>
                                                </Media>
                                              </Media>
                                            </div>
                                          )}
                                      </Media>
                                    </Media>
                                    {discussion.comment_reply &&
                                      discussion.comment_reply.length > 0 &&
                                      discussion.comment_reply.map((reply) => {
                                        return (
                                          <div
                                            key={reply.id}
                                            style={{ "margin-left": "50px" }}
                                          >
                                            <Media className="media-post mt-3">
                                              <Media>
                                                <img
                                                  class='media-object'
                                                  onError={(error) =>
                                                    (error.target.src = require("../../../assets/images/user.png"))
                                                  }
                                                  src={
                                                    reply.sender.current_profile_pic
                                                      ? reply.sender
                                                        .current_profile_pic
                                                      : require("../../../assets/images/user.png")
                                                  }
                                                  alt="User Image"
                                                />
                                              </Media>
                                              <Media body>
                                                <Media heading>
                                                  <div className="d-flex">
                                                    <div className="align-self-center">
                                                      {reply.sender.full_name}
                                                      <span className="time-stamp">
                                                        <span>&middot;</span>{" "}
                                                        {reply.sent_at}
                                                      </span>
                                                    </div>
                                                    <div className="ml-auto">
                                                      <Button
                                                        size="sm"
                                                        color="transparent"
                                                        className="text-muted"
                                                        onClick={() => {
                                                          this.toggleReplyForm(
                                                            reply.id,
                                                            "L2"
                                                          );
                                                        }}
                                                      >
                                                        Reply
                                                </Button>
                                                      <Button
                                                        size="sm"
                                                        color="transparent"
                                                        className="text-muted"
                                                        onClick={() => {
                                                          this.toggleEditForm(
                                                            reply.id,
                                                            "L2",
                                                            reply.body
                                                          );
                                                        }}
                                                        hidden={
                                                          reply.sender.id !==
                                                            this.props.profile_data.id
                                                            ? true
                                                            : false
                                                        }
                                                      >
                                                        {" "}
                                                        <FontAwesomeIcon icon="pencil-alt" />{" "}
                                                      </Button>
                                                      <Button
                                                        size="sm"
                                                        color="transparent"
                                                        className="ml-0 text-muted"
                                                        onClick={() => {
                                                          this.setState({
                                                            deleteId: reply.id,
                                                          });
                                                          this.confirmDeleteModalToggle();
                                                        }}
                                                        hidden={
                                                          reply.sender.id !==
                                                            this.props.profile_data.id
                                                            ? true
                                                            : false
                                                        }
                                                      >
                                                        {" "}
                                                        <FontAwesomeIcon icon="trash-alt" />{" "}
                                                      </Button>
                                                    </div>
                                                  </div>
                                                </Media>

                                                {/* Show Comment, hide when editing */}
                                                <div>{reply.body}</div>

                                                {/* Show when editing comment */}
                                                {this.state.editFormVisible[
                                                  "L2" + reply.id
                                                ] && (
                                                    <div>
                                                      <div className="mt-3">
                                                        <Input
                                                          bsSize="sm"
                                                          className="mb-2"
                                                          type="textarea"
                                                          name="body"
                                                          value={
                                                            this.state.editDiscussion
                                                              .body
                                                          }
                                                          placeholder="Edit Reply..."
                                                          onChange={
                                                            this
                                                              .handleEditDiscussionOnChange
                                                          }
                                                        />
                                                        <div className="text-right">
                                                          <div className="text-right">
                                                            <Button
                                                              size="sm"
                                                              color="light"
                                                              onClick={() => {
                                                                this.toggleEditForm(
                                                                  reply.id,
                                                                  "L2"
                                                                );
                                                              }}
                                                            >
                                                              {" "}
                                                      Cancel{" "}
                                                            </Button>
                                                            <Button
                                                              size="sm"
                                                              color="primary"
                                                              onClick={this.handleEditDiscussionOnSubmit(
                                                                reply.id,
                                                                "L2",
                                                                review.id
                                                              )}
                                                            >
                                                              {" "}
                                                      Update{" "}
                                                            </Button>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  )}

                                                {this.state.replyFormVisible[
                                                  "L2" + reply.id
                                                ] && (
                                                    <div>
                                                      <hr />
                                                      <Media className="media-post mt-3">
                                                        <Media>
                                                          <Media
                                                            object
                                                            src={
                                                              this.state
                                                                .profileData &&
                                                                this.state.profileData
                                                                  .current_profile_file
                                                                ? this.state
                                                                  .profileData
                                                                  .current_profile_file
                                                                : require("../../../assets/images/user.png")
                                                            }
                                                            alt="User Image"
                                                          />
                                                        </Media>
                                                        <Media body>
                                                          <FormGroup className="mb-0">
                                                            <Input
                                                              bsSize="sm"
                                                              className="mb-2"
                                                              type="textarea"
                                                              name="reply"
                                                              value={
                                                                this.state.addReply
                                                                  .body
                                                              }
                                                              onChange={
                                                                this
                                                                  .handleOnAddReplyChange
                                                              }
                                                              placeholder="Write a Reply..."
                                                            />
                                                            <div className="text-right">
                                                              <div className="text-right">
                                                                <Button
                                                                  size="sm"
                                                                  color="light"
                                                                  onClick={() => {
                                                                    this.toggleReplyForm(
                                                                      reply.id,
                                                                      "L2"
                                                                    );
                                                                  }}
                                                                >
                                                                  {" "}
                                                          Cancel{" "}
                                                                </Button>
                                                                <Button
                                                                  size="sm"
                                                                  color="primary"
                                                                  onClick={this.handleOnAddReplySubmit(
                                                                    discussion.id,
                                                                    "L2",
                                                                    review.id,
                                                                    reply.id
                                                                  )}
                                                                >
                                                                  Reply
                                                        </Button>
                                                              </div>
                                                            </div>
                                                          </FormGroup>
                                                        </Media>
                                                      </Media>
                                                    </div>
                                                  )}
                                              </Media>
                                            </Media>
                                          </div>
                                        );
                                      })}
                                  </div>
                                );
                              })}

                            {/* Post a comment */}
                            <div hidden>
                              <hr />
                              <Media className="media-post mt-3">
                                <Media>
                                  <Media
                                    object
                                    src={
                                      this.state.profileData &&
                                        this.state.profileData.current_profile_file
                                        ? this.state.profileData
                                          .current_profile_file
                                        : require("../../../assets/images/user.png")
                                    }
                                    alt="User Image"
                                  />
                                </Media>
                                <Media body hidden>
                                  <FormGroup className="mb-0">
                                    <Input
                                      bsSize="sm"
                                      className="mb-2"
                                      type="textarea"
                                      name="reply"
                                      value={this.state.addDiscussion[review.id]}
                                      onChange={this.handleOnAddDiscussionChange(
                                        review.id
                                      )}
                                      placeholder="Write a Comment..."
                                    />
                                    <div className="text-right">
                                      <div className="text-right">
                                        <Button
                                          size="sm"
                                          color="primary"
                                          onClick={this.handleOnAddDiscussionSubmit(
                                            review.id
                                          )}
                                        >
                                          Post a comment
                                  </Button>
                                      </div>
                                    </div>
                                  </FormGroup>
                                </Media>
                              </Media>
                            </div>
                          </div>
                        </Col>


                      </Row>
                    </div>
                    { review.is_review_flag ?
                      <div className="bg-warning p-3 text-primary-dark text-center mt-1 fs-14">
                        <a href="#" className="text-reset font-weight-bold">
                          {this.state.disputer_name[review.id]} </a>
                            filled for <span className="font-weight-bold">Dispute Resolution. </span>
                        <span className="font-weight-bold">Go to&nbsp;</span>
                        <span role="button" className="text-black font-weight-bold"
                          onClick={() => this.handleHeatedDiscussion(review)}>
                          Heated Discussion
                                {/* Community Debate */}
                        </span>
                      </div>
                      :
                      ''
                    }

                    {/* <div className="bg-warning p-3 text-dark text-center mt-1 fs-14">
                  {/* <a href="#" className="text-reset font-weight-bold">User John</a> */}
                    {/* filled for <strong>Dispute Resolution.</strong> 
                    <strong>Go to&nbsp;</strong> 
                    <strong role="button" className="text-black" 
                    onClick={()=>this.handleHeatedDiscussion(review.id)}>Heated Discussion</strong>
                </div> */}
                  </div>
                );

              })}
            </div>




            :

            reviewsList.results && reviewsList.results.length > 0 ? (
              reviewsList.results.map((review) => {
                return (
                  review.is_review_flag ?
                    ''
                    :
                    <>

                      {
                        circleClick && circleClick.topBarReview ||
                          circleClick && circleClick.topBarDisputedReview ?
                          <div className={
                            circleClick && circleClick.topBarReviewColor === 'red' ||
                              circleClick && circleClick.topBarDisputedReviewColor === 'red' ?
                              "bg-danger text-dark p-2" :
                              circleClick && circleClick.topBarReviewColor === 'orange' ||
                                circleClick && circleClick.topBarDisputedReviewColor === 'orange' ?
                                "bg-warning text-dark p-2" :
                                circleClick && circleClick.topBarReviewColor === 'green' ||
                                  circleClick && circleClick.topBarDisputedReviewColor === 'green' ?
                                  "bg-success text-dark p-2" : ""
                          }>
                            <div className="d-flex flex-wrap mx-2">
                              <div className="text-white px-2">
                                <span>&nbsp;</span>Reviews
                            </div>
                              {/* {console.log('review', review)} */}
                              {/* <div className="d-flex justify-content-end align-item-center text-white p-1">
                              <h4 className="fs-18">{review?.associated_to?.name || 'Branch name'}</h4>
                              <img
                                className="ml-2 img-circle _20x20"
                                src={review?.listing_profileimage[0]?.media_type !== 'video' ?
                                  review?.listing_profileimage[0]?.url : require("../../../assets/images/user.png")}
                                alt={review?.associated_to?.name || 'Branch name'}
                                onError={(error) =>
                                  (error.target.src = require("../../../assets/images/user.png"))
                                }
                              />
                            </div> */}
                            </div>
                          </div> : ""}
                      {this.state.filterType === 'answered' ?

                        review.is_review_flag !== true ?
                          <div className="bg-white p-3 mb-3" key={review.id}>
                            <Row>
                              <Col sm={4} md={3}>
                                <div>
                                  <div className="mb-3">
                                    <a href="#" className="text-decoration-none">
                                      <img
                                        className="d-block mb-2"
                                        width="48"
                                        src={
                                          review.created_user_media &&
                                            review.created_user_media.url
                                            ? review.created_user_media.url
                                            : require("../../../assets/images/user-circle.png")
                                        }
                                        alt="User Image"
                                      />
                                      <div>
                                        <span className="text-secondary-dark font-weight-bold d-block">
                                          {review.created_user_media &&
                                            `${review.created_user_media.user.user.first_name} ${review.created_user_media.user.user.last_name}`}
                                        </span>
                                        <small className="text-primary d-block">
                                          {review.created_by.present_address
                                            ? `${review.created_by.present_address.city}, ${review.created_by.present_address.state}`
                                            : ""}
                                        </small>
                                      </div>
                                    </a>
                                  </div>

                                  <div>
                                    <a
                                      href="#"
                                      className="text-decoration-none text-primary"
                                    >
                                      <span className="font-weight-bold">
                                        {review.created_review_user_count ? review.created_review_user_count : 0}
                                      </span>{" "}
                                      <span>{review.created_review_user_count && review.created_review_user_count == 1 ? 'Review' : 'Reviews'}</span>
                                    </a>
                                  </div>

                                  <div className="mt-3">
                                    <ul className="list-unstyled d-flex">
                                      <li title="Language 1">
                                        <img
                                          src={require("../../../assets/images/lang-01.png")}
                                          alt="Language"
                                        />
                                      </li>
                                      <li title="Language 2">
                                        <img
                                          src={require("../../../assets/images/lang-02.png")}
                                          alt="Language"
                                        />
                                      </li>
                                      <li title="Language 3">
                                        <img
                                          src={require("../../../assets/images/lang-03.png")}
                                          alt="Language"
                                        />
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                              </Col>
                              <Col sm={8} md={9}>

                                <div className="d-flex mb-4">
                                  <div className="mr-2">
                                    <img
                                      className="mr-3"
                                      title={review.ratings_sets.avg_rating[1]}

                                      src={
                                        review.ratings_sets.avg_rating[1]
                                          ?
                                          Math.floor(
                                            review.ratings_sets.avg_rating[1]) === 0
                                            ?
                                            require(`../../../assets/images/norating.png`)
                                            :
                                            require(`../../../assets/images/${Math.floor(
                                              review.ratings_sets.avg_rating[1]
                                            )}rating.png`)
                                          :
                                          require(`../../../assets/images/norating.png`)
                                      }
                                    />
                                    <span className="fs-14 font-weight-bold">
                                      Overall Rating
                          </span>
                                    <div className="mt-2 fs-14">
                                      <i className="font-weight-bold">
                                        This review was written
                            </i>
                                      <span> {review.review_date_time}</span>
                                    </div>
                                  </div>
                                  {review.is_review_flag ?
                                    ''
                                    :
                                    <div className="ml-auto">
                                      <UncontrolledDropdown className="dropdown-sm d-inline-block">
                                        <DropdownToggle color="transparent" size="sm">
                                          <FontAwesomeIcon icon="angle-down" size="lg" />
                                        </DropdownToggle>
                                        <DropdownMenu right>
                                          <DropdownItem
                                            // onClick={() => {
                                            //   this.setState({
                                            //     disputeThisReviewModalToggle: true,
                                            //     reviewId: review.id,
                                            //     reviewAdded: review,
                                            //   });
                                            //   this.props.add_dispute_review({ review_id: review.id, check_status: true });
                                            // }}
                                            onClick={() => this.handleDispteThisReview(review)}
                                          >
                                            Dispute this review
                                  </DropdownItem>
                                        </DropdownMenu>
                                      </UncontrolledDropdown>
                                    </div>
                                  }

                                </div>


                                {/* Dispute review block */}
                                <div
                                  className="border border-secondary p-4 mb-3"
                                  hidden={this.state.disputeReviewVisible ? false : true}
                                >
                                  <div className="mb-3">
                                    <Label
                                      for="disputed_review"
                                      className="text-primary-dark fs-14 font-weight-bold"
                                    >
                                      {" "}
                            Provide evidence and explanation for disputing the
                            review in question
                          </Label>
                                    <Input
                                      type="textarea"
                                      className="primary"
                                      name="disputed_review"
                                      id="disputed_review"
                                      onChange={this.handleChange}
                                      placeholder="Provide explanation1...."
                                    />
                                  </div>
                                  <Row
                                    className="mb-3"
                                    form
                                    hidden={
                                      this.state.uploadMedia.selectedMedia.length > 0
                                        ? false
                                        : true
                                    }
                                  >
                                    {this.state.uploadMedia.selectedMedia.length > 0 &&
                                      this.state.uploadMedia.selectedMedia.map((item) => {
                                        return (
                                          <Col xs="auto" className="mb-3">
                                            <div className="d-flex pr-3 pt-3">
                                              <div>
                                                {(item.type === "image" ||
                                                  item.media_type === "image" ||
                                                  item.type === "video" ||
                                                  item.media_type === "video") && (
                                                    <div className="gallery-media">
                                                      <img
                                                        src={
                                                          item.file ? item.file : item.url
                                                        }
                                                        alt={
                                                          item.filename
                                                            ? item.filename.length < 20
                                                              ? item.filename
                                                              : this.truncate(item.filename)
                                                            : item.name
                                                              ? item.name.length < 20
                                                                ? item.name
                                                                : this.truncate(item.name)
                                                              : ""
                                                        }
                                                      />
                                                    </div>
                                                  )}
                                              </div>
                                              <div className="mx-n3 mt-n3">
                                                <Button
                                                  color="dark"
                                                  size="sm"
                                                  title="Remove Media"
                                                  onClick={() =>
                                                    this.handleOnClickRemoveSelectedGalleryMedia(
                                                      item
                                                    )
                                                  }
                                                >
                                                  <FontAwesomeIcon icon="minus" size="sm" />{" "}
                                                </Button>
                                              </div>
                                            </div>
                                          </Col>
                                        );
                                      })}
                                  </Row>
                                  <div className="d-flex mx-n2">
                                    <div className="px-2">
                                      <Button
                                        color="white"
                                        size="sm"
                                        onClick={() => this.handleOnClickUploadMedia()}
                                      >
                                        <FontAwesomeIcon icon="camera" />
                                      </Button>
                                    </div>
                                    <div className="px-2 ml-auto">
                                      <Button color="primary" size="sm" className="mw"
                                        onClick={this.handleDisputeSubmit}>
                                        Submit
                            </Button>
                                      <Button
                                        color="light"
                                        size="sm"
                                        className="mw"
                                        onClick={() =>
                                          this.handleOnClickToggleDisputeReview("close")
                                        }
                                      >
                                        Cancel
                            </Button>
                                    </div>
                                  </div>
                                </div>

                                <div>
                                  <div className="mb-4">
                                    <div className="py-2 fs-14 text-primary-dark">
                                      <FontAwesomeIcon
                                        className="mr-2"
                                        size="lg"
                                        icon={["fas", "quote-left"]}
                                      />
                                      <span className="comment-body-renderer">{ReactHtmlParser(review.review)}</span>
                                      <FontAwesomeIcon
                                        className="ml-2"
                                        size="lg"
                                        icon={["fas", "quote-right"]}
                                      />
                                    </div>
                                    <div className="fs-14">
                                      <span className="font-weight-bold text-muted">
                                        {review.review_group_count.helpful}
                                      </span>{" "}
                                      <i>people found this review helpful</i>
                                    </div>
                                  </div>

                                  {/* Answered Reviews list */}
                                  {review.review_discussion &&
                                    review.review_discussion.length > 0 &&
                                    review.review_discussion.map((discussion) => {
                                      return (
                                        <div key={discussion.id}>
                                          <Media className="media-post mt-3">
                                            <Media>
                                              <img
                                                class='media-object'
                                                src={
                                                  discussion.sender.current_profile_pic
                                                    ? discussion.sender
                                                      .current_profile_pic
                                                    : require("../../../assets/images/user.png")
                                                }
                                                alt="User Image"
                                                onError={(error) =>
                                                  (error.target.src = require("../../../assets/images/user.png"))
                                                }
                                              />
                                            </Media>
                                            <Media body>
                                              <Media heading>
                                                <div className="d-flex">
                                                  <div className="align-self-center">
                                                    {discussion.sender.full_name}
                                                    <span className="time-stamp">
                                                      <span>&middot;</span>{" "}
                                                      {discussion.sent_at}
                                                    </span>
                                                  </div>
                                                  <div className="ml-auto">
                                                    <Button
                                                      size="sm"
                                                      color="transparent"
                                                      className="text-muted"
                                                      onClick={() => {
                                                        this.toggleReplyForm(
                                                          discussion.id,
                                                          "L1"
                                                        );
                                                      }}
                                                    >
                                                      Reply
                                          </Button>
                                                    <Button
                                                      size="sm"
                                                      color="transparent"
                                                      className="text-muted"
                                                      onClick={() => {
                                                        this.toggleEditForm(
                                                          discussion.id,
                                                          "L1",
                                                          discussion.body
                                                        );
                                                      }}
                                                      hidden={
                                                        discussion.sender.id !==
                                                          this.props.profile_data.id
                                                          ? true
                                                          : false
                                                      }
                                                    >
                                                      {" "}
                                                      <FontAwesomeIcon icon="pencil-alt" />{" "}
                                                    </Button>
                                                    <Button
                                                      size="sm"
                                                      color="transparent"
                                                      className="ml-0 text-muted"
                                                      onClick={() => {
                                                        this.setState({
                                                          deleteId: discussion.id,
                                                        });
                                                        this.confirmDeleteModalToggle();
                                                      }}
                                                      hidden={
                                                        discussion.sender.id !==
                                                          this.props.profile_data.id
                                                          ? true
                                                          : false
                                                      }
                                                    >
                                                      {" "}
                                                      <FontAwesomeIcon icon="trash-alt" />{" "}
                                                    </Button>
                                                  </div>
                                                </div>
                                              </Media>

                                              {/* Show Comment, hide when editing */}
                                              <div>{discussion.body}</div>

                                              {/* Show when editing comment */}
                                              {this.state.editFormVisible[
                                                "L1" + discussion.id
                                              ] && (
                                                  <div>
                                                    <div className="mt-3">
                                                      <Input
                                                        bsSize="sm"
                                                        className="mb-2"
                                                        type="textarea"
                                                        name="body"
                                                        value={
                                                          this.state.editDiscussion.body
                                                        }
                                                        placeholder="Edit Comment..."
                                                        onChange={
                                                          this.handleEditDiscussionOnChange
                                                        }
                                                      />
                                                      <div className="text-right">
                                                        <div className="text-right">
                                                          <Button
                                                            size="sm"
                                                            color="light"
                                                            onClick={() => {
                                                              this.toggleEditForm(
                                                                discussion.id,
                                                                "L1"
                                                              );
                                                            }}
                                                          >
                                                            {" "}
                                                Cancel{" "}
                                                          </Button>
                                                          <Button
                                                            size="sm"
                                                            color="primary"
                                                            onClick={this.handleEditDiscussionOnSubmit(
                                                              discussion.id,
                                                              "L1",
                                                              review.id
                                                            )}
                                                          >
                                                            {" "}
                                                Update{" "}
                                                          </Button>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                )}

                                              {this.state.replyFormVisible[
                                                "L1" + discussion.id
                                              ] && (
                                                  <div>
                                                    <hr />
                                                    <Media className="media-post mt-3">
                                                      <Media>
                                                        <Media
                                                          object
                                                          src={
                                                            this.state.profileData &&
                                                              this.state.profileData
                                                                .current_profile_file
                                                              ? this.state.profileData
                                                                .current_profile_file
                                                              : require("../../../assets/images/user.png")
                                                          }
                                                          alt="User Image"
                                                        />
                                                      </Media>
                                                      <Media body>
                                                        <FormGroup className="mb-0">
                                                          <Input
                                                            bsSize="sm"
                                                            className="mb-2"
                                                            type="textarea"
                                                            name="reply"
                                                            value={this.state.addReply.body}
                                                            onChange={
                                                              this.handleOnAddReplyChange
                                                            }
                                                            placeholder="Write a Reply..."
                                                          />
                                                          <div className="text-right">
                                                            <div className="text-right">
                                                              <Button
                                                                size="sm"
                                                                color="light"
                                                                onClick={() => {
                                                                  this.toggleReplyForm(
                                                                    discussion.id,
                                                                    "L1"
                                                                  );
                                                                }}
                                                              >
                                                                {" "}
                                                    Cancel{" "}
                                                              </Button>
                                                              <Button
                                                                size="sm"
                                                                color="primary"
                                                                onClick={this.handleOnAddReplySubmit(
                                                                  discussion.id,
                                                                  "L1",
                                                                  review.id
                                                                )}
                                                              >
                                                                Reply
                                                  </Button>
                                                            </div>
                                                          </div>
                                                        </FormGroup>
                                                      </Media>
                                                    </Media>
                                                  </div>
                                                )}
                                            </Media>
                                          </Media>
                                          {discussion.comment_reply &&
                                            discussion.comment_reply.length > 0 &&
                                            discussion.comment_reply.map((reply) => {
                                              return (
                                                <div
                                                  key={reply.id}
                                                  style={{ "margin-left": "50px" }}
                                                >

                                                  <Media className="media-post mt-3">
                                                    <Media>
                                                      <img
                                                        class='media-object'
                                                        onError={(error) =>
                                                          (error.target.src = require("../../../assets/images/user.png"))
                                                        }
                                                        src={
                                                          reply.sender.current_profile_pic
                                                            ? reply.sender
                                                              .current_profile_pic
                                                            : require("../../../assets/images/user.png")
                                                        }
                                                        alt="User Image"
                                                      />
                                                    </Media>
                                                    <Media body>
                                                      <Media heading>
                                                        <div className="d-flex">
                                                          <div className="align-self-center">
                                                            {reply.sender.full_name}
                                                            <span className="time-stamp">
                                                              <span>&middot;</span>{" "}
                                                              {reply.sent_at}
                                                            </span>
                                                          </div>
                                                          <div className="ml-auto">
                                                            <Button
                                                              size="sm"
                                                              color="transparent"
                                                              className="text-muted"
                                                              onClick={() => {
                                                                this.toggleReplyForm(
                                                                  reply.id,
                                                                  "L2"
                                                                );
                                                              }}
                                                            >
                                                              Reply
                                                  </Button>
                                                            <Button
                                                              size="sm"
                                                              color="transparent"
                                                              className="text-muted"
                                                              onClick={() => {
                                                                this.toggleEditForm(
                                                                  reply.id,
                                                                  "L2",
                                                                  reply.body
                                                                );
                                                              }}
                                                              hidden={
                                                                reply.sender.id !==
                                                                  this.props.profile_data.id
                                                                  ? true
                                                                  : false
                                                              }
                                                            >
                                                              {" "}
                                                              <FontAwesomeIcon icon="pencil-alt" />{" "}
                                                            </Button>
                                                            <Button
                                                              size="sm"
                                                              color="transparent"
                                                              className="ml-0 text-muted"
                                                              onClick={() => {
                                                                this.setState({
                                                                  deleteId: reply.id,
                                                                });
                                                                this.confirmDeleteModalToggle();
                                                              }}
                                                              hidden={
                                                                reply.sender.id !==
                                                                  this.props.profile_data.id
                                                                  ? true
                                                                  : false
                                                              }
                                                            >
                                                              {" "}
                                                              <FontAwesomeIcon icon="trash-alt" />{" "}
                                                            </Button>
                                                          </div>
                                                        </div>
                                                      </Media>

                                                      {/* Show Comment, hide when editing */}
                                                      <div>{reply.body}</div>

                                                      {/* Show when editing comment */}
                                                      {this.state.editFormVisible[
                                                        "L2" + reply.id
                                                      ] && (
                                                          <div>
                                                            <div className="mt-3">
                                                              <Input
                                                                bsSize="sm"
                                                                className="mb-2"
                                                                type="textarea"
                                                                name="body"
                                                                value={
                                                                  this.state.editDiscussion
                                                                    .body
                                                                }
                                                                placeholder="Edit Reply..."
                                                                onChange={
                                                                  this
                                                                    .handleEditDiscussionOnChange
                                                                }
                                                              />
                                                              <div className="text-right">
                                                                <div className="text-right">
                                                                  <Button
                                                                    size="sm"
                                                                    color="light"
                                                                    onClick={() => {
                                                                      this.toggleEditForm(
                                                                        reply.id,
                                                                        "L2"
                                                                      );
                                                                    }}
                                                                  >
                                                                    {" "}
                                                        Cancel{" "}
                                                                  </Button>
                                                                  <Button
                                                                    size="sm"
                                                                    color="primary"
                                                                    onClick={this.handleEditDiscussionOnSubmit(
                                                                      reply.id,
                                                                      "L2",
                                                                      review.id
                                                                    )}
                                                                  >
                                                                    {" "}
                                                        Update{" "}
                                                                  </Button>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        )}

                                                      {this.state.replyFormVisible[
                                                        "L2" + reply.id
                                                      ] && (
                                                          <div>
                                                            <hr />
                                                            <Media className="media-post mt-3">
                                                              <Media>
                                                                <Media
                                                                  object
                                                                  src={
                                                                    this.state
                                                                      .profileData &&
                                                                      this.state.profileData
                                                                        .current_profile_file
                                                                      ? this.state
                                                                        .profileData
                                                                        .current_profile_file
                                                                      : require("../../../assets/images/user.png")
                                                                  }
                                                                  alt="User Image"
                                                                />
                                                              </Media>
                                                              <Media body>
                                                                <FormGroup className="mb-0">
                                                                  <Input
                                                                    bsSize="sm"
                                                                    className="mb-2"
                                                                    type="textarea"
                                                                    name="reply"
                                                                    value={
                                                                      this.state.addReply
                                                                        .body
                                                                    }
                                                                    onChange={
                                                                      this
                                                                        .handleOnAddReplyChange
                                                                    }
                                                                    placeholder="Write a Reply..."
                                                                  />
                                                                  <div className="text-right">
                                                                    <div className="text-right">
                                                                      <Button
                                                                        size="sm"
                                                                        color="light"
                                                                        onClick={() => {
                                                                          this.toggleReplyForm(
                                                                            reply.id,
                                                                            "L2"
                                                                          );
                                                                        }}
                                                                      >
                                                                        {" "}
                                                            Cancel{" "}
                                                                      </Button>
                                                                      <Button
                                                                        size="sm"
                                                                        color="primary"
                                                                        onClick={this.handleOnAddReplySubmit(
                                                                          discussion.id,
                                                                          "L2",
                                                                          review.id,
                                                                          reply.id
                                                                        )}
                                                                      >
                                                                        Reply
                                                          </Button>
                                                                    </div>
                                                                  </div>
                                                                </FormGroup>
                                                              </Media>
                                                            </Media>
                                                          </div>
                                                        )}
                                                    </Media>
                                                  </Media>
                                                </div>
                                              );
                                            })}
                                        </div>
                                      );
                                    })}

                                  {/* Post a comment */}
                                  <div>
                                    <hr />
                                    {/* <Media className="media-post mt-3">
                              <Media>
                                <Media
                                  object
                                  src={
                                    this.state.profileData &&
                                      this.state.profileData.current_profile_file
                                      ? this.state.profileData
                                        .current_profile_file
                                      : require("../../../assets/images/user.png")
                                  }
                                  alt="User Image"
                                />
                              </Media>
                              </Media> */}

                                    {
                                      review.is_review_flag
                                        ?
                                        ''

                                        :

                                        <Media body>
                                          <FormGroup className="mb-0">
                                            <Input
                                              bsSize="sm"
                                              className="mb-2"
                                              type="textarea"
                                              name="reply"
                                              value={this.state.addDiscussion[review.id]}
                                              onChange={this.handleOnAddDiscussionChange(
                                                review.id
                                              )}
                                              placeholder="Write a Comment..."
                                            />
                                            <div className="text-right">
                                              <div className="text-right">
                                                <Button
                                                  size="sm"
                                                  color="primary"
                                                  onClick={this.handleOnAddDiscussionSubmit(
                                                    review.id
                                                  )}
                                                >
                                                  Post a comment
                                      </Button>
                                              </div>
                                            </div>
                                          </FormGroup>
                                        </Media>

                                    }
                                  </div>
                                </div>

                              </Col>

                            </Row>
                            {review.is_review_flag ?
                              <div className="bg-warning p-3 text-primary-dark text-center mt-1 fs-14">
                                <a href="#" className="text-reset font-weight-bold">
                                  {this.state.disputer_name[review.id]} </a>
                          filled for <span className="font-weight-bold">Dispute Resolution. </span>
                                <span className="font-weight-bold">Go to&nbsp;</span>
                                <span role="button" className="text-black font-weight-bold"
                                  onClick={() => this.handleHeatedDiscussion(review)}>Heated Discussion2</span>
                              </div>
                              :
                              ''
                            }
                          </div> : ""
                        :
                        <div className="bg-white p-3 mb-3" key={review.id}>
                          <Row>
                            <Col sm={4} md={3}>
                              <div>
                                <div className="mb-3">
                                  <a href="#" className="text-decoration-none">
                                    <img
                                      className="d-block mb-2"
                                      width="48"
                                      src={
                                        review.created_user_media &&
                                          review.created_user_media.url
                                          ? review.created_user_media.url
                                          : require("../../../assets/images/user-circle.png")
                                      }
                                      alt="User Image"
                                    />
                                    <div>
                                      <span className="text-secondary-dark font-weight-bold d-block">
                                        {review.created_user_media &&
                                          `${review.created_user_media.user.user.first_name} ${review.created_user_media.user.user.last_name}`}
                                      </span>
                                      <small className="text-primary d-block">
                                        {review.created_by.present_address
                                          ? `${review.created_by.present_address.city}, ${review.created_by.present_address.state}`
                                          : ""}
                                      </small>
                                    </div>
                                  </a>
                                </div>

                                <div>
                                  <a
                                    href="#"
                                    className="text-decoration-none text-primary"
                                  >
                                    <span className="font-weight-bold">
                                      {review.created_review_user_count ? review.created_review_user_count : 0}
                                    </span>{" "}
                                    <span>{review.created_review_user_count && review.created_review_user_count == 1 ? 'Review' : 'Reviews'}</span>
                                  </a>
                                </div>

                                <div className="mt-3">
                                  <ul className="list-unstyled d-flex">
                                    <li title="Language 1">
                                      <img
                                        src={require("../../../assets/images/lang-01.png")}
                                        alt="Language"
                                      />
                                    </li>
                                    <li title="Language 2">
                                      <img
                                        src={require("../../../assets/images/lang-02.png")}
                                        alt="Language"
                                      />
                                    </li>
                                    <li title="Language 3">
                                      <img
                                        src={require("../../../assets/images/lang-03.png")}
                                        alt="Language"
                                      />
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </Col>
                            <Col sm={8} md={9}>

                              <div className="d-flex mb-4">
                                <div className="mr-2">
                                  <img
                                    className="mr-3"
                                    title={review.ratings_sets.avg_rating[1]}

                                    src={
                                      review.ratings_sets.avg_rating[1]
                                        ?
                                        Math.floor(
                                          review.ratings_sets.avg_rating[1]) === 0
                                          ?
                                          require(`../../../assets/images/norating.png`)
                                          :
                                          require(`../../../assets/images/${Math.floor(
                                            review.ratings_sets.avg_rating[1]
                                          )}rating.png`)
                                        :
                                        require(`../../../assets/images/norating.png`)
                                    }
                                  />
                                  <span className="fs-14 font-weight-bold">
                                    Overall Rating
                          </span>
                                  <div className="mt-2 fs-14">
                                    <i className="font-weight-bold">
                                      This review was written
                            </i>
                                    <span> {review.review_date_time}</span>
                                  </div>
                                </div>
                                {review.is_review_flag ?
                                  ''
                                  :
                                  <div className="ml-auto">
                                    <UncontrolledDropdown className="dropdown-sm d-inline-block">
                                      <DropdownToggle color="transparent" size="sm">
                                        <FontAwesomeIcon icon="angle-down" size="lg" />
                                      </DropdownToggle>
                                      <DropdownMenu right>
                                        <DropdownItem
                                          // onClick={() => {
                                          //   this.setState({
                                          //     disputeThisReviewModalToggle: true,
                                          //     reviewId: review.id,
                                          //     reviewAdded: review,
                                          //   });
                                          //   this.props.add_dispute_review({ review_id: review.id, check_status: true });
                                          // }
                                          // }
                                          onClick={() => this.handleDispteThisReview(review)}
                                        >
                                          Dispute this review
                                  </DropdownItem>
                                      </DropdownMenu>
                                    </UncontrolledDropdown>
                                  </div>

                                }

                              </div>

                              {/* Dispute review block */}
                              <div
                                className="border border-secondary p-4 mb-3"
                                hidden={this.state.disputeReviewVisible ? false : true}
                              >
                                <div className="mb-3">
                                  <Label
                                    for="disputed_review"
                                    className="text-primary-dark fs-14 font-weight-bold"
                                  >
                                    {" "}
                            Provide evidence and explanation for disputing the
                            review in question
                          </Label>
                                  <Input
                                    type="textarea"
                                    className="primary"
                                    name="disputed_review"
                                    id="disputed_review"
                                    onChange={this.handleChange}
                                    placeholder="Provide explanation1...."
                                  />
                                </div>
                                <Row
                                  className="mb-3"
                                  form
                                  hidden={
                                    this.state.uploadMedia.selectedMedia.length > 0
                                      ? false
                                      : true
                                  }
                                >
                                  {this.state.uploadMedia.selectedMedia.length > 0 &&
                                    this.state.uploadMedia.selectedMedia.map((item) => {
                                      return (
                                        <Col xs="auto" className="mb-3">
                                          <div className="d-flex pr-3 pt-3">
                                            <div>
                                              {(item.type === "image" ||
                                                item.media_type === "image" ||
                                                item.type === "video" ||
                                                item.media_type === "video") && (
                                                  <div className="gallery-media">
                                                    <img
                                                      src={
                                                        item.file ? item.file : item.url
                                                      }
                                                      alt={
                                                        item.filename
                                                          ? item.filename.length < 20
                                                            ? item.filename
                                                            : this.truncate(item.filename)
                                                          : item.name
                                                            ? item.name.length < 20
                                                              ? item.name
                                                              : this.truncate(item.name)
                                                            : ""
                                                      }
                                                    />
                                                  </div>
                                                )}
                                            </div>
                                            <div className="mx-n3 mt-n3">
                                              <Button
                                                color="dark"
                                                size="sm"
                                                title="Remove Media"
                                                onClick={() =>
                                                  this.handleOnClickRemoveSelectedGalleryMedia(
                                                    item
                                                  )
                                                }
                                              >
                                                <FontAwesomeIcon icon="minus" size="sm" />{" "}
                                              </Button>
                                            </div>
                                          </div>
                                        </Col>
                                      );
                                    })}
                                </Row>
                                <div className="d-flex mx-n2">
                                  <div className="px-2">
                                    <Button
                                      color="white"
                                      size="sm"
                                      onClick={() => this.handleOnClickUploadMedia()}
                                    >
                                      <FontAwesomeIcon icon="camera" />
                                    </Button>
                                  </div>
                                  <div className="px-2 ml-auto">
                                    <Button color="primary" size="sm" className="mw"
                                      onClick={this.handleDisputeSubmit}>
                                      Submit
                            </Button>
                                    <Button
                                      color="light"
                                      size="sm"
                                      className="mw"
                                      onClick={() =>
                                        this.handleOnClickToggleDisputeReview("close")
                                      }
                                    >
                                      Cancel
                            </Button>
                                  </div>
                                </div>
                              </div>

                              <div>
                                <div className="mb-4">
                                  <div className="py-2 fs-14 text-primary-dark">
                                    <FontAwesomeIcon
                                      className="mr-2"
                                      size="lg"
                                      icon={["fas", "quote-left"]}
                                    />
                                    <span className="comment-body-renderer">{ReactHtmlParser(review.review)}</span>
                                    <FontAwesomeIcon
                                      className="ml-2"
                                      size="lg"
                                      icon={["fas", "quote-right"]}
                                    />
                                  </div>
                                  <div className="fs-14">
                                    <span className="font-weight-bold text-muted">
                                      {review.review_group_count.helpful}
                                    </span>{" "}
                                    <i>people found this review helpful</i>
                                  </div>
                                </div>

                                {/* Answered Reviews list */}
                                {review.review_discussion &&
                                  review.review_discussion.length > 0 &&
                                  review.review_discussion.map((discussion) => {
                                    return (
                                      <div key={discussion.id}>
                                        <Media className="media-post mt-3">
                                          <Media>
                                            <img
                                              class='media-object'
                                              onError={(error) =>
                                                (error.target.src = require("../../../assets/images/user.png"))
                                              }
                                              src={
                                                discussion.sender.current_profile_pic
                                                  ? discussion.sender
                                                    .current_profile_pic
                                                  : require("../../../assets/images/user.png")
                                              }
                                              alt="User Image"
                                            />
                                          </Media>
                                          <Media body>
                                            <Media heading>
                                              <div className="d-flex">
                                                <div className="align-self-center">
                                                  {discussion.sender.full_name}
                                                  <span className="time-stamp">
                                                    <span>&middot;</span>{" "}
                                                    {discussion.sent_at}
                                                  </span>
                                                </div>
                                                <div className="ml-auto">
                                                  <Button
                                                    size="sm"
                                                    color="transparent"
                                                    className="text-muted"
                                                    onClick={() => {
                                                      this.toggleReplyForm(
                                                        discussion.id,
                                                        "L1"
                                                      );
                                                    }}
                                                  >
                                                    Reply
                                          </Button>
                                                  <Button
                                                    size="sm"
                                                    color="transparent"
                                                    className="text-muted"
                                                    onClick={() => {
                                                      this.toggleEditForm(
                                                        discussion.id,
                                                        "L1",
                                                        discussion.body
                                                      );
                                                    }}
                                                    hidden={
                                                      discussion.sender.id !==
                                                        this.props.profile_data.id
                                                        ? true
                                                        : false
                                                    }
                                                  >
                                                    {" "}
                                                    <FontAwesomeIcon icon="pencil-alt" />{" "}
                                                  </Button>
                                                  <Button
                                                    size="sm"
                                                    color="transparent"
                                                    className="ml-0 text-muted"
                                                    onClick={() => {
                                                      this.setState({
                                                        deleteId: discussion.id,
                                                      });
                                                      this.confirmDeleteModalToggle();
                                                    }}
                                                    hidden={
                                                      discussion.sender.id !==
                                                        this.props.profile_data.id
                                                        ? true
                                                        : false
                                                    }
                                                  >
                                                    {" "}
                                                    <FontAwesomeIcon icon="trash-alt" />{" "}
                                                  </Button>
                                                </div>
                                              </div>
                                            </Media>

                                            {/* Show Comment, hide when editing */}
                                            <div>{discussion.body}</div>

                                            {/* Show when editing comment */}
                                            {this.state.editFormVisible[
                                              "L1" + discussion.id
                                            ] && (
                                                <div>
                                                  <div className="mt-3">
                                                    <Input
                                                      bsSize="sm"
                                                      className="mb-2"
                                                      type="textarea"
                                                      name="body"
                                                      value={
                                                        this.state.editDiscussion.body
                                                      }
                                                      placeholder="Edit Comment..."
                                                      onChange={
                                                        this.handleEditDiscussionOnChange
                                                      }
                                                    />
                                                    <div className="text-right">
                                                      <div className="text-right">
                                                        <Button
                                                          size="sm"
                                                          color="light"
                                                          onClick={() => {
                                                            this.toggleEditForm(
                                                              discussion.id,
                                                              "L1"
                                                            );
                                                          }}
                                                        >
                                                          {" "}
                                                Cancel{" "}
                                                        </Button>
                                                        <Button
                                                          size="sm"
                                                          color="primary"
                                                          onClick={this.handleEditDiscussionOnSubmit(
                                                            discussion.id,
                                                            "L1",
                                                            review.id
                                                          )}
                                                        >
                                                          {" "}
                                                Update{" "}
                                                        </Button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              )}

                                            {this.state.replyFormVisible[
                                              "L1" + discussion.id
                                            ] && (
                                                <div>
                                                  <hr />
                                                  <Media className="media-post mt-3">
                                                    <Media>
                                                      <Media
                                                        object
                                                        src={
                                                          this.state.profileData &&
                                                            this.state.profileData
                                                              .current_profile_file
                                                            ? this.state.profileData
                                                              .current_profile_file
                                                            : require("../../../assets/images/user.png")
                                                        }
                                                        alt="User Image"
                                                      />
                                                    </Media>
                                                    <Media body>
                                                      <FormGroup className="mb-0">
                                                        <Input
                                                          bsSize="sm"
                                                          className="mb-2"
                                                          type="textarea"
                                                          name="reply"
                                                          value={this.state.addReply.body}
                                                          onChange={
                                                            this.handleOnAddReplyChange
                                                          }
                                                          placeholder="Write a Reply..."
                                                        />
                                                        <div className="text-right">
                                                          <div className="text-right">
                                                            <Button
                                                              size="sm"
                                                              color="light"
                                                              onClick={() => {
                                                                this.toggleReplyForm(
                                                                  discussion.id,
                                                                  "L1"
                                                                );
                                                              }}
                                                            >
                                                              {" "}
                                                    Cancel{" "}
                                                            </Button>
                                                            <Button
                                                              size="sm"
                                                              color="primary"
                                                              onClick={this.handleOnAddReplySubmit(
                                                                discussion.id,
                                                                "L1",
                                                                review.id
                                                              )}
                                                            >
                                                              Reply
                                                  </Button>
                                                          </div>
                                                        </div>
                                                      </FormGroup>
                                                    </Media>
                                                  </Media>
                                                </div>
                                              )}
                                          </Media>
                                        </Media>
                                        {discussion.comment_reply &&
                                          discussion.comment_reply.length > 0 &&
                                          discussion.comment_reply.map((reply) => {
                                            return (
                                              <div
                                                key={reply.id}
                                                style={{ "margin-left": "50px" }}
                                              >

                                                <Media className="media-post mt-3">
                                                  <Media>
                                                    <img
                                                      class='media-object'
                                                      onError={(error) =>
                                                        (error.target.src = require("../../../assets/images/user.png"))
                                                      }
                                                      src={
                                                        reply.sender.current_profile_pic
                                                          ? reply.sender
                                                            .current_profile_pic
                                                          : require("../../../assets/images/user.png")
                                                      }
                                                      alt="User Image"
                                                    />
                                                  </Media>
                                                  <Media body>
                                                    <Media heading>
                                                      <div className="d-flex">
                                                        <div className="align-self-center">
                                                          {reply.sender.full_name}
                                                          <span className="time-stamp">
                                                            <span>&middot;</span>{" "}
                                                            {reply.sent_at}
                                                          </span>
                                                        </div>
                                                        <div className="ml-auto">
                                                          <Button
                                                            size="sm"
                                                            color="transparent"
                                                            className="text-muted"
                                                            onClick={() => {
                                                              this.toggleReplyForm(
                                                                reply.id,
                                                                "L2"
                                                              );
                                                            }}
                                                          >
                                                            Reply
                                                  </Button>
                                                          <Button
                                                            size="sm"
                                                            color="transparent"
                                                            className="text-muted"
                                                            onClick={() => {
                                                              this.toggleEditForm(
                                                                reply.id,
                                                                "L2",
                                                                reply.body
                                                              );
                                                            }}
                                                            hidden={
                                                              reply.sender.id !==
                                                                this.props.profile_data.id
                                                                ? true
                                                                : false
                                                            }
                                                          >
                                                            {" "}
                                                            <FontAwesomeIcon icon="pencil-alt" />{" "}
                                                          </Button>
                                                          <Button
                                                            size="sm"
                                                            color="transparent"
                                                            className="ml-0 text-muted"
                                                            onClick={() => {
                                                              this.setState({
                                                                deleteId: reply.id,
                                                              });
                                                              this.confirmDeleteModalToggle();
                                                            }}
                                                            hidden={
                                                              reply.sender.id !==
                                                                this.props.profile_data.id
                                                                ? true
                                                                : false
                                                            }
                                                          >
                                                            {" "}
                                                            <FontAwesomeIcon icon="trash-alt" />{" "}
                                                          </Button>
                                                        </div>
                                                      </div>
                                                    </Media>

                                                    {/* Show Comment, hide when editing */}
                                                    <div>{reply.body}</div>

                                                    {/* Show when editing comment */}
                                                    {this.state.editFormVisible[
                                                      "L2" + reply.id
                                                    ] && (
                                                        <div>
                                                          <div className="mt-3">
                                                            <Input
                                                              bsSize="sm"
                                                              className="mb-2"
                                                              type="textarea"
                                                              name="body"
                                                              value={
                                                                this.state.editDiscussion
                                                                  .body
                                                              }
                                                              placeholder="Edit Reply..."
                                                              onChange={
                                                                this
                                                                  .handleEditDiscussionOnChange
                                                              }
                                                            />
                                                            <div className="text-right">
                                                              <div className="text-right">
                                                                <Button
                                                                  size="sm"
                                                                  color="light"
                                                                  onClick={() => {
                                                                    this.toggleEditForm(
                                                                      reply.id,
                                                                      "L2"
                                                                    );
                                                                  }}
                                                                >
                                                                  {" "}
                                                        Cancel{" "}
                                                                </Button>
                                                                <Button
                                                                  size="sm"
                                                                  color="primary"
                                                                  onClick={this.handleEditDiscussionOnSubmit(
                                                                    reply.id,
                                                                    "L2",
                                                                    review.id
                                                                  )}
                                                                >
                                                                  {" "}
                                                        Update{" "}
                                                                </Button>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      )}

                                                    {this.state.replyFormVisible[
                                                      "L2" + reply.id
                                                    ] && (
                                                        <div>
                                                          <hr />
                                                          <Media className="media-post mt-3">
                                                            <Media>
                                                              <Media
                                                                object
                                                                src={
                                                                  this.state
                                                                    .profileData &&
                                                                    this.state.profileData
                                                                      .current_profile_file
                                                                    ? this.state
                                                                      .profileData
                                                                      .current_profile_file
                                                                    : require("../../../assets/images/user.png")
                                                                }
                                                                alt="User Image"
                                                              />
                                                            </Media>
                                                            <Media body>
                                                              <FormGroup className="mb-0">
                                                                <Input
                                                                  bsSize="sm"
                                                                  className="mb-2"
                                                                  type="textarea"
                                                                  name="reply"
                                                                  value={
                                                                    this.state.addReply
                                                                      .body
                                                                  }
                                                                  onChange={
                                                                    this
                                                                      .handleOnAddReplyChange
                                                                  }
                                                                  placeholder="Write a Reply..."
                                                                />
                                                                <div className="text-right">
                                                                  <div className="text-right">
                                                                    <Button
                                                                      size="sm"
                                                                      color="light"
                                                                      onClick={() => {
                                                                        this.toggleReplyForm(
                                                                          reply.id,
                                                                          "L2"
                                                                        );
                                                                      }}
                                                                    >
                                                                      {" "}
                                                            Cancel{" "}
                                                                    </Button>
                                                                    <Button
                                                                      size="sm"
                                                                      color="primary"
                                                                      onClick={this.handleOnAddReplySubmit(
                                                                        discussion.id,
                                                                        "L2",
                                                                        review.id,
                                                                        reply.id
                                                                      )}
                                                                    >
                                                                      Reply
                                                          </Button>
                                                                  </div>
                                                                </div>
                                                              </FormGroup>
                                                            </Media>
                                                          </Media>
                                                        </div>
                                                      )}
                                                  </Media>
                                                </Media>
                                              </div>
                                            );
                                          })}
                                      </div>
                                    );
                                  })}

                                {/* Post a comment */}
                                <div>
                                  <hr />
                                  {/* <Media className="media-post mt-3">
                              <Media>
                                <Media
                                  object
                                  src={
                                    this.state.profileData &&
                                      this.state.profileData.current_profile_file
                                      ? this.state.profileData
                                        .current_profile_file
                                      : require("../../../assets/images/user.png")
                                  }
                                  alt="User Image"
                                />
                              </Media>
                              </Media> */}

                                  {
                                    review.is_review_flag
                                      ?
                                      ''

                                      :

                                      <Media body>
                                        <FormGroup className="mb-0">
                                          <Input
                                            bsSize="sm"
                                            className="mb-2"
                                            type="textarea"
                                            name="reply"
                                            value={this.state.addDiscussion[review.id]}
                                            onChange={this.handleOnAddDiscussionChange(
                                              review.id
                                            )}
                                            placeholder="Write a Comment..."
                                          />
                                          <div className="text-right">
                                            <div className="text-right">
                                              <Button
                                                size="sm"
                                                color="primary"
                                                onClick={this.handleOnAddDiscussionSubmit(
                                                  review.id
                                                )}
                                              >
                                                Post a comment
                                      </Button>
                                            </div>
                                          </div>
                                        </FormGroup>
                                      </Media>

                                  }
                                </div>
                              </div>

                            </Col>

                          </Row>
                          {review.is_review_flag ?
                            <div className="bg-warning p-3 text-primary-dark text-center mt-1 fs-14">
                              <a href="#" className="text-reset font-weight-bold">
                                {this.state.disputer_name[review.id]} </a>
                            filled for <span className="font-weight-bold">Dispute Resolution. </span>
                              <span className="font-weight-bold">Go to &nbsp;</span>
                              <span role="button" className="text-black font-weight-bold"
                                onClick={() => this.handleHeatedDiscussion(review)}>
                                Heated Discussion
                                {/* Community Debate */}
                              </span>
                            </div>
                            :
                            ''
                          }
                        </div>



                      }
                    </>
                );

              })
            ) : (
                <div className="bg-white p-3">
                  <h2 className="text-secondary-dark">No Reviews to Display</h2>
                </div>
              )}
        </div>

        {/* Delete Confirmation Modal */}
        <Modal
          isOpen={this.state.confirmDeleteModal}
          toggle={this.confirmDeleteModalToggle}
          className="text-center"
        >
          <div className="bg-white modal-header">
            <h5 className="modal-title mx-auto text-dark">Confirmation</h5>
            <button
              className="btn btn-sm"
              aria-label="Close"
              onClick={this.confirmDeleteModalToggle}
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <ModalBody className="text-dark">
            Are you sure you want to delete?
          </ModalBody>
          <ModalFooter className="bg-white">
            <div className="text-center w-100">
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.confirmDeleteModalToggle}
              >
                Cancel
              </Button>
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.handleOnDeleteConfirmation}
              >
                Ok
              </Button>
            </div>
          </ModalFooter>
        </Modal>

        {/* Dispute Review Modal */}
        {/* {this.state.disputeThisReviewModalToggle} */}
        <Modal
          size="xl"
          isOpen={this.state.disputeThisReviewModalToggle}
          toggle={() =>
            this.setState({
              disputeThisReviewModalToggle: !this.state
                .disputeThisReviewModalToggle,
            })
          }
        >
          {isLoading ?
            <div className="loader_div">
              <div className="inner-loader">
                <img src={Loaderimg} alt="" />
              </div>
            </div>
            :
            ''
          }
          <div className="modal-header flex-wrap p-0">
            {/* Dispute review intro design */}
            <div className="row mb-5 form flex-grow-1">
              <div className="col-lg-8 ml-auto">
                <div className="bg-warning text-dark p-2 fs-14">
                  <div className="d-flex flex-wrap mx-n2">
                    <div className="text-white px-2">
                      <span className="font-weight-normal"> This is the <span className="font-weight-bold"> review </span> you are disputing. </span>
                    </div>
                  </div>
                </div>
                <div className="bg-white p-3 fs-14 text-dark">
                  <div className="d-flex mx-n2 mb-2">
                    <div className="px-2">
                      <a href="/people/Dreamy" className="text-decoration-none">
                        <img className="img-circle _50x50"
                          src={
                            this.state.reviewAdded.created_user_media &&
                              this.state.reviewAdded.created_user_media.url
                              ? this.state.reviewAdded.created_user_media.url
                              : require("../../../assets/images/user-circle.png")
                          }
                          alt="" />

                      </a>
                    </div>
                    <div className="px-2 flex-grow-1">
                      <div className="px-2 flex-grow-1">
                        <div className="ff-alt">
                          <a
                            href={`/people/${this.state.reviewAddedUser}`}
                            className="text-reset ff-base font-weight-bold"
                          >

                            {this.state.reviewAdded.created_user_media &&
                              `${this.state.reviewAdded.created_user_media.user.user.first_name} ${this.state.reviewAdded.created_user_media.user.user.last_name}`}
                          </a>
                          <span> wrote a this review </span>
                          {/* <Link to={{
                            // pathname: `/${this.state.reviewAdded.target?.category?.toLowerCase()}/${this.state.reviewAdded.target?.slug ? this.state.reviewAdded.target?.slug : 'null'}`,
                            // state: { id: this.state.reviewAdded.target?.id }
                          }}
                            className="text-reset font-weight-bold">
                            {this.state.reviewAdded?.target?.name}
                            
                          </Link> and gave it */}
                          <br />
                          <img
                            className="mr-3"
                          // title={this.state.reviewAdded.ratings_sets.avg_rating[1]}

                          // src={
                          //   this.state.reviewAdded.ratings_sets.avg_rating[1]
                          //     ? 
                          //      Math.floor(
                          //       this.state.reviewAdded.ratings_sets.avg_rating[1]) === 0 
                          //       ?
                          //       require(`../../../assets/images/norating.png`)
                          //       :                                    
                          //       require(`../../../assets/images/${Math.floor(
                          //         this.state.reviewAdded.ratings_sets.avg_rating[1]
                          //       )}rating.png`)
                          //     : 
                          //     require(`../../../assets/images/norating.png`)
                          // }
                          />
                          <span> {this.state.reviewAdded.review_date_time}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="ff-alt my-4">
                    <i className="icon-comma-left"></i>
                    <span className="comment-body-renderer"> {ReactHtmlParser(this.state.reviewAdded.review)}</span>
                    <i className="icon-comma-right"></i>
                  </div>
                </div>
              </div>
            </div>
            <div className="w-100">
              <div>
                <Button
                  color="primary"
                  onClick={() =>
                    this.setState({
                      disputeThisReviewModalToggle: !this.state
                        .disputeThisReviewModalToggle,
                    })
                  }
                >
                  <FontAwesomeIcon className="mr-2" icon="angle-left" />
                    back to listing
                  </Button>
              </div>
            </div>
          </div>
          <ModalBody className="bg-transparent p-0">
            <Row className="mb-3" noGutters>
              <Col xs="auto">
                <div
                  className="d-flex flex-column justify-content-center bg-tertiary text-white text-center px-3 py-4 h-100"
                  style={{ minWidth: "300px" }}
                >
                  <div>
                    <img
                      src={require("../../../assets/images/icons/star/blue/fill.png")}
                      alt=""
                    />
                    <h2 className="my-2 fs-50 text-shadow">
                      REVIEW DISPUTE
                        </h2>
                    <img
                      src={require("../../../assets/images/icons/star/blue/fill.png")}
                      alt=""
                    />
                  </div>
                </div>
              </Col>
              <Col>
                <div className="d-flex flex-column justify-content-center bg-dark text-white h-100 p-3">
                  <Row>
                    <Col className="text-center">
                      <div className="mb-2">
                        {get_dispute_data && get_dispute_data[0] &&
                          get_dispute_data[0].is_business_owner ?
                          <h3 className="text-light">
                            Business Owner Initiated.
                                </h3>
                          :
                          <h3 className="text-light">
                            Community Initiated.
                                </h3>
                        }
                      </div>
                      <div className="fs-24 ff-headings text-uppercase">
                        Please watch the video to see how Review Disputes work
                      </div>
                      <div className="mt-3">
                        <Row className="justify-content-between align-items-center">
                          <Col xs="12" className="d-flex justify-content-center align-items-center">
                            <div className="text-center d-flex align-items-center justify-content-center">
                              <div>
                                <img
                                  className="img-fluid"
                                  src={require("../../../assets/images/icons/star/torquise/fill.png")}
                                  alt=""
                                />
                              </div>
                            </div>
                            <div className="video-holder play-icon d-inline-block mx-2"
                              role="button"
                              onClick={() =>
                                this.setState({
                                  watchHowDisputeReviewWorksModalToggle: true
                                })
                              }>
                              <img height="80" src={require("../../../assets/images/dispute_jury.webp")} />
                            </div>
                            <div className="text-center d-flex align-items-center justify-content-center">
                              <div>
                                <img
                                  className="img-fluid"
                                  src={require("../../../assets/images/icons/star/torquise/fill.png")}
                                  alt=""
                                />
                              </div>
                            </div>
                          </Col>
                          {/* <Col xs="12">
                            <div className="text-center d-flex align-items-center justify-content-center">
                              <div>
                                <img
                                  className="img-fluid"
                                  src={require("../../../assets/images/icons/star/torquise/fill.png")}
                                  alt=""
                                />
                              </div>
                              <Button className="mx-3" color="primary text-nowrap"
                                onClick={() =>
                                  this.setState({
                                    watchHowDisputeReviewWorksModalToggle: true
                                  })
                                }>
                                Click Here
                                  </Button>
                              <div>
                                <img
                                  className="img-fluid"
                                  src={require("../../../assets/images/icons/star/torquise/fill.png")}
                                  alt=""
                                />
                              </div>
                            </div>
                          </Col> */}
                        </Row>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <div className="bg-white p-2 mb-3">
              <CollapseBasic
                title="Step 1. Reason for Dispute"
                noHr={true}
                containerClass="type-primary bg-transparent"
                bodyClass="mt-3"
                isOpen={true}
                size="sm"
              >
                <hr className="bg-light" />
                <div>
                  {/* For Community Users */}
                  <FormGroup row>
                    <Col sm={6}>
                      <Label className="text-dark font-weight-normal fs-14">
                        Choose why you are disputing this review
                      </Label>
                    </Col>
                    <Col sm={6}>
                      <Input
                        type="select"
                        className="primary"
                        bsSize="sm"
                        name="reason"
                        onChange={this.handleChange}
                        value={reason}
                      >
                        <option value="This review is fake">
                          This review is fake
                        </option>
                        <option value="This review is not relevant">
                          This review is not relevant
                        </option>
                        <option value="This review is inaccurate">
                          This review is inaccurate
                        </option>
                        <option value="" disabled >
                          &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; OR
                        </option>
                        <option value="Request Administrative Review">
                          Request Administrative Review
                        </option>
                      </Input>
                    </Col>
                  </FormGroup>
                  {/* For Admins */}
                  {this.state.is_administrative_review ?
                    <div>
                      <div className="fs-16 mb-2">Administrative Review</div>

                      <Row>
                        <Col className="mb-2" md={6}>
                          <FormGroup check>
                            <Label
                              className="font-weight-normal fs-14 text-dark"
                              check
                            >
                              <Input type="checkbox" /> Conflict of Interest
                          </Label>
                          </FormGroup>
                        </Col>
                        <Col className="mb-2" md={6}>
                          <FormGroup check>
                            <Label
                              className="font-weight-normal fs-14 text-dark"
                              check
                            >
                              <Input type="checkbox" /> Contains threats of
                            violence
                          </Label>
                          </FormGroup>
                        </Col>
                        <Col className="mb-2" md={6}>
                          <FormGroup check>
                            <Label
                              className="font-weight-normal fs-14 text-dark"
                              check
                            >
                              <Input type="checkbox" /> Spam
                          </Label>
                          </FormGroup>
                        </Col>
                      </Row>
                    </div>
                    :
                    ''
                  }
                </div>
              </CollapseBasic>
            </div>
            <div className="bg-white p-2 mb-3">
              <CollapseBasic
                title="Step 2. Evidence and Explanation"
                noHr={true}
                containerClass="type-primary bg-transparent"
                bodyClass="mt-3"
                isOpen={true}
                size="sm"
              >
                <hr className="bg-light" />
                <div>
                  <div className="mb-3">
                    <Label
                      for="disputed_review"
                      className="text-primary-dark fs-14"
                    >
                      {reason === 'This review is fake' ?

                        <span>
                          Provide evidence and explanation for disputing the review in
                          question
                      </span>
                        :
                        ''
                      }

                      {reason === 'This review is not relevant' || reason === "Request Administrative Review" ?
                        <span>
                          Provide evidence for disputing the review in question.
                          <br />
                          Uploading an explanation (optional)
                        </span>
                        :
                        ''
                      }

                      {reason === 'This review is inaccurate' ?
                        <span>
                          Provide an explanation for disputing the review in question.
                        <br />
                        Uploading evidence (optional)
                      </span>
                        :
                        ''
                      }
                    </Label>
                    <div>
                      <Input
                        type="textarea"
                        // className="text-primary-dark fs-14 font-weight-bold"
                        className="primary text-primary-dark fs-14 font-weight-bold"
                        bsSize="sm"
                        rows="4"
                        name="comment"
                        id="disputed_review"
                        onChange={this.handleChange}
                        value={comment}
                        placeholder="Provide explanation..."
                        required
                      />
                      {/* <div className="attachable-area">
                        <hr className="bg-primary mt-0" />
                        <div className="d-flex mx-n1">
                          <div className="px-1">
                            <Input type="file" id="attach_files_modal" multiple style={{ display: 'none' }} />
                            <Label for="attach_files_modal" className="btn btn-sm btn-transparent" title="Attach Files">
                              <FontAwesomeIcon icon="paperclip" fixedWidth />
                            </Label>
                          </div>
                          <div className="px-1">
                            <Button color="transparent" size="sm" title="Upload Media">
                              <FontAwesomeIcon icon="camera" fixedWidth />
                            </Button>
                          </div>
                        </div>
                      </div> */}
                    </div>
                  </div>

                  {/* Attach file types documents */}
                  {/* <div className="mt-3">
                    <div className="fs-14 font-weight-bold mb-2">
                      Attached Files
                    </div>
                    <Row form>
                      <Col xs="auto" className="mb-3">
                        <div className="d-flex pr-3 pt-3">
                          <div>
                            <div className="bg-light p-2 text-truncate border border-dark" style={{ maxWidth: '200px' }}>
                              Filefleafejfke_123.pdf
                          </div>
                          </div>
                          <div className="mx-n3 mt-n3">
                            <Button color="dark" size="sm" title="Remove File">
                              <FontAwesomeIcon icon="minus" size="sm" />{" "}
                            </Button>
                          </div>
                        </div>
                      </Col>
                      <Col xs="auto" className="mb-3">
                        <div className="d-flex pr-3 pt-3">
                          <div>
                            <div className="bg-light p-2 text-truncate border border-dark" style={{ maxWidth: '200px' }}>
                              Filefleafejfke_123.pdf
                          </div>
                          </div>
                          <div className="mx-n3 mt-n3">
                            <Button color="dark" size="sm" title="Remove File">
                              <FontAwesomeIcon icon="minus" size="sm" />{" "}
                            </Button>
                          </div>
                        </div>
                      </Col>
                      <Col xs="auto" className="mb-3">
                        <div className="d-flex pr-3 pt-3">
                          <div>
                            <div className="bg-light p-2 text-truncate border border-dark" style={{ maxWidth: '200px' }}>
                              Filefleafejfke_123.pdf
                          </div>
                          </div>
                          <div className="mx-n3 mt-n3">
                            <Button color="dark" size="sm" title="Remove File">
                              <FontAwesomeIcon icon="minus" size="sm" />{" "}
                            </Button>
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </div> */}

                  {/* Uploaded file type images/videos */}
                  {/* <div className="mt-3">
                    <div className="fs-14 font-weight-bold mb-2">
                      Uploaded Files
                    </div>
                    <Row form>
                      <Col xs="auto" className="mb-3">
                        <div className="d-flex pr-3 pt-3">
                          <div>
                            <div className="gallery-media">
                              <img
                                src={require("../../../assets/images/cvs-logo.jpg")}
                                alt=""
                              />
                            </div>
                          </div>
                          <div className="mx-n3 mt-n3">
                            <Button color="dark" size="sm" title="Remove File">
                              <FontAwesomeIcon icon="minus" size="sm" />{" "}
                            </Button>
                          </div>
                        </div>
                      </Col>
                      <Col xs="auto" className="mb-3">
                        <div className="d-flex pr-3 pt-3">
                          <div>
                            <div className="gallery-media">
                              <img
                                src={require("../../../assets/images/cvs-logo.jpg")}
                                alt=""
                              />
                            </div>
                          </div>
                          <div className="mx-n3 mt-n3">
                            <Button color="dark" size="sm" title="Remove File">
                              <FontAwesomeIcon icon="minus" size="sm" />{" "}
                            </Button>
                          </div>
                        </div>
                      </Col>
                      <Col xs="auto" className="mb-3">
                        <div className="d-flex pr-3 pt-3">
                          <div>
                            <div className="gallery-media">
                              <img
                                src={require("../../../assets/images/cvs-logo.jpg")}
                                alt=""
                              />
                            </div>
                          </div>
                          <div className="mx-n3 mt-n3">
                            <Button color="dark" size="sm" title="Remove File">
                              <FontAwesomeIcon icon="minus" size="sm" />{" "}
                            </Button>
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </div>
                 */}
                </div>
              </CollapseBasic>
            </div>

            <div className="bg-white p-2 mb-3">
              <CollapseBasic
                title="Step 3. Upload Images and/or Video (optional)"
                noHr={true}
                containerClass="type-primary bg-transparent"
                bodyClass="mt-3"
                isOpen={true}
                size="sm"
              >
                <hr className="bg-light" />
                {/* <div className="mb-3">
                  <p className="text-dark font-weight-normal fs-14">
                    Upload an image or video to accompany your review. Additional images/videos can be added later.
                  </p>
                  <Button color="dark">Upload Media</Button>
                </div> */}

                {/* Dispute review uplaod files*/}
                <div className="mb-3 mt-3">
                  {/* <input type="file" onChange={this.handleImageChange} multiple color="dark"/> */}

                  <input
                    type="file"
                    name="imageUpload"
                    id="imageUpload"
                    style={{ display: "None" }}
                    onChange={this.handleImageChange}
                    multiple
                  />
                  <label
                    for="imageUpload"
                    className="btn btn-tertiary bg-tertiary border-tertiary"
                  >
                    Upload Media
                  </label>
                </div>

                {/* Uploaded media shown here */}
                <div className="mb-3">
                  <Row xs={2} sm={3} lg={4} form>
                    {this.state.dispute_imgepreview.map((image, index) => (
                      <Col className="mb-3">
                        <div
                          className="d-flex pr-3 pt-3"
                          role="button"
                          onClick={() =>
                            this.setState({ viewMyPhotosModal: true })
                          }
                        >
                          <div className="flex-grow-1 mw-100">
                            {image.toString().includes("data:image") ? (
                              <img
                                className="img-fluid img-thumbnail"
                                src={image}
                                alt="Image"
                                title="Image Name"
                              />
                            ) : image.toString().includes("data:video") ? (
                              <div
                                className="bg-light p-2 text-truncate border fs-13"
                                title={
                                  this.state.dispute_files &&
                                  this.state.dispute_files[index] &&
                                  this.state.dispute_files[index].name
                                }
                              >
                                <img
                                  width={24}
                                  className="mr-2"
                                  src={
                                    "https://userdatawikireviews.s3-us-west-2.amazonaws.com/images/dispute/videoicon.jpg"
                                  }
                                  alt="Icon"
                                />{" "}
                                {this.state.dispute_files &&
                                  this.state.dispute_files[index] &&
                                  this.state.dispute_files[index].name}
                              </div>
                            )

                                :
                                (<div
                                  className="bg-light p-2 text-truncate border fs-13"
                                  title={
                                    this.state.dispute_files &&
                                    this.state.dispute_files[index] &&
                                    this.state.dispute_files[index].name
                                  }
                                >
                                  <img
                                    width={24}
                                    className="mr-2"
                                    src={
                                      "https://userdatawikireviews.s3-us-west-2.amazonaws.com/images/dispute/file_icon.png"
                                    }
                                    alt="Icon"
                                  />{" "}
                                  {this.state.dispute_files &&
                                    this.state.dispute_files[index] &&
                                    this.state.dispute_files[index].name}
                                </div>
                                )}
                          </div>
                          <div className="mx-n3 mt-n3">
                            <Button
                              color="delete-media"
                              size="sm"
                              title="Remove"
                              onClick={() => this.deleteImage(index)}
                            >
                              <FontAwesomeIcon icon="trash-alt" />{" "}
                            </Button>
                          </div>
                        </div>
                      </Col>
                    ))}
                  </Row>
                </div>

                <div className="text-right">
                  <div className="px-2 ml-auto">
                    <Button
                      color="grey"
                      className="mw"
                      onClick={() =>
                        this.setState({ disputeThisReviewModalToggle: false })
                      }
                    >
                      Cancel Dispute
                    </Button>
                    <Button color="tertiary" className="mw" disabled>
                      Save Review Dispute Draft
                    </Button>
                    {is_media_dispute ?
                      <Button
                        color="primary"
                        className="mw"
                        onClick={() => {
                          this.setState({ isLoading: true });
                          this.handleDisputeSubmit()
                        }
                        }
                      >
                        Submit Review Dispute
                      </Button>
                      :
                      <Button
                        color="primary"
                        className="mw"
                        disabled

                      >
                        Submit Review Dispute
                      </Button>
                    }
                  </div>
                </div>
              </CollapseBasic>
            </div>
          </ModalBody>
        </Modal>


        {/* Voting Modals */}
        <Modal
          size="xl"
          scrollable
          className="drawer-modal"
          isOpen={this.state.voteReviewModalToggleOne}
          // isOpen={true}
          toggle={() =>
            this.setState({
              voteReviewModalToggleOne: !this.state.voteReviewModalToggleOne,
            })
          }
        >
          <form className="d-flex flex-column h-100">
            <div className="modal-header p-0">
              <div className="w-100">
                <div>
                  <Button
                    color="primary"
                    onClick={() =>
                      this.setState({
                        voteReviewModalToggleOne: !this.state
                          .voteReviewModalToggleOne,
                      })
                    }
                  >
                    <FontAwesomeIcon className="mr-2" icon="angle-left" />
                    back to listing
                  </Button>
                </div>
              </div>
            </div>
            <ModalBody className="p-0 fs-14 text-dark bg-transparent">
              <div className="bg-dark text-white">
                <Row noGutters>
                  <Col xs="auto">
                    <div
                      className="d-flex flex-column justify-content-center bg-tertiary text-white text-center px-3 py-4 h-100"
                      style={{ minWidth: "300px" }}
                    >
                      <div>
                        <img
                          // src={require("../../../assets/images/icons/star/blue/fill.png")}
                          alt=""
                        />
                        <h2 className="my-2 fs-50 text-shadow">
                          REVIEW DISPUTE
                        </h2>
                        <img
                          // src={require("../../../assets/images/icons/star/blue/fill.png")}
                          alt=""
                        />
                      </div>
                    </div>
                  </Col>
                  <Col>
                    <div className="d-flex flex-column justify-content-center bg-dark text-white h-100 p-3">
                      <Row>
                        <Col>
                          <div className="pr-md-5">
                            <h3 className="text-light">
                              Business Owner initiated.
                            </h3>
                            <div className="fs-14">
                              <div className="text-primary fs-16 font-weight-bold">
                                Step 1. Lorem Ipsum.
                              </div>
                              <p className="mb-0">
                                Brief description of why you vote your counts
                                and what you'd be voting on. Lorem ipsum, dolor
                                sit amet consectetur adipisicing elit.
                                Dignissimos, non?
                              </p>
                            </div>
                          </div>
                        </Col>
                        <Col md="auto" className="align-self-end">
                          <div className="text-light font-weight-bold">
                            Submission time ends in
                          </div>
                          <div className="d-flex mx-n2 flex-nowrap">
                            <div className="px-2">
                              <div className="text-white">
                                <div className="fs-32 ff-headings">7</div>
                                <div className="font-weight-bold">days</div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  15
                                </div>
                                <div className="font-weight-normal">hours</div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  32
                                </div>
                                <div className="font-weight-normal">
                                  minutes
                                </div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  56
                                </div>
                                <div className="font-weight-normal">
                                  seconds
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="body-bg px-3 py-3">
                <Row>
                  <Col>
                    In case you don't know the correct address, try looking for
                    it with our search bar.
                  </Col>
                  <Col xs="auto">
                    <div className="d-flex">
                      <Input
                        className="primary"
                        bsSize="sm"
                        type="search"
                        placeholder="Custom Search"
                        style={{ minWidth: "300px" }}
                      ></Input>
                      <Button color="primary" size="sm" className="ml-3">
                        <FontAwesomeIcon icon="search" />
                      </Button>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="bg-white px-3 py-4">
                <Row>
                  <Col md={4}>
                    <div className="font-weight-bold fs-16">
                      Dispute History
                    </div>
                    <ul className="d-flex flex-column list-unstyled">
                      <li className="py-2 border-bottom">
                        <div className="d-flex mx-n1">
                          <div className="px-1">
                            <div className="ff-alt">
                              <Moment format="DD-MM-YYYY">
                                {get_dispute_data && get_dispute_data[0] && get_dispute_data[0].created_on}
                              </Moment>
                                &nbsp; at &nbsp;
                                <Moment format="HH:MM:SS">
                                {get_dispute_data && get_dispute_data[0] && get_dispute_data[0].created_on}
                              </Moment>
                            </div>
                          </div>
                          <div className="px-1">
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Sau6402
                            </a>
                            &nbsp; added a Punctuation in Tips.
                          </div>
                        </div>
                      </li>
                      <li className="py-2 border-bottom">
                        <div className="d-flex mx-n1">
                          <div className="px-1">
                            <div className="ff-alt">22/04/2014</div>
                          </div>
                          <div className="px-1">
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Danielbauwens
                            </a>
                            &nbsp; reverted edits by{" "}
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Fart biscuit
                            </a>
                            &nbsp; changed back to last version by{" "}
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Anna
                            </a>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </Col>
                  <Col md={8}>
                    <Row>
                      <Col lg={6}>
                        <FormGroup className="main-post-formgroup">
                          <div className="text-primary fs-16 font-weight-bold">
                            Step 2. Write the points of your dispute
                          </div>
                          <div className="input-label-block">
                            <Input
                              className="primary"
                              type="textarea"
                              rows="4"
                              placeholder="&nbsp;"
                            />
                          </div>
                        </FormGroup>
                      </Col>
                      <Col lg={6}>
                        <div className="mt-4">
                          <FormGroup>
                            <div className="text-tertiary font-weight-bold mb-2">
                              Submit proofs
                            </div>
                            <CustomInput bsSize="sm" type="file" />
                          </FormGroup>
                          <div className="text-right">
                            <div className="text-tertiary">
                              or{" "}
                              <span className="font-weight-bold" role="button">
                                embed link
                              </span>
                            </div>
                          </div>
                        </div>
                      </Col>
                    </Row>

                    <hr />
                    <div className="text-right">
                      <Button
                        color="link"
                        className="text-primary p-0 font-weight-bold"
                      >
                        <FontAwesomeIcon
                          className="mr-2"
                          icon="plus"
                          size="sm"
                        />
                        add another
                      </Button>
                    </div>
                  </Col>
                </Row>
              </div>
            </ModalBody>
            <ModalFooter className="bg-white">
              <Button color="primary">Submit</Button>
              <Button color="link"
                className="text-tertiary font-weight-bold"
                onClick={() => this.setState({ voteReviewModalToggleOne: false })}>
                Skip
              </Button>
            </ModalFooter>
          </form>
        </Modal>

        <Modal
          size="xl"
          scrollable
          className="drawer-modal"
          isOpen={this.state.voteReviewModalToggleTwo}
          toggle={() =>
            this.setState({
              voteReviewModalToggleTwo: !this.state.voteReviewModalToggleTwo,
            })
          }
        >
          <form className="d-flex flex-column h-100">
            <div className="modal-header p-0">
              <div className="w-100">
                <div>
                  <Button
                    color="primary"
                    onClick={() =>
                      this.setState({
                        voteReviewModalToggleTwo: !this.state
                          .voteReviewModalToggleTwo,
                      })
                    }
                  >
                    <FontAwesomeIcon className="mr-2" icon="angle-left" />
                    back to listing
                  </Button>
                </div>
                <div className="bg-dark text-white fs-14 px-3">
                  <div className="mb-2 pt-2">
                    <div className="d-flex mx-n2">
                      <div className="px-2 col">
                        <span className="fs-14 text-light font-weight-bold">
                          You’re Voting{" "}
                        </span>
                        <UncontrolledDropdown className="d-inline-block">
                          <DropdownToggle
                            className="text-white bg-transparent shadow-none font-weight-bold"
                            color="transparent"
                            size="sm"
                            caret
                          >
                            <span className="pr-2">Disputed Reviews</span>
                          </DropdownToggle>
                          <DropdownMenu>
                            <DropdownItem className="fs-14">
                              Disputed Reviews
                            </DropdownItem>
                          </DropdownMenu>
                        </UncontrolledDropdown>
                      </div>
                      <div className="px-2 col-auto">
                        near{" "}
                        <span className="text-light font-weight-bold">
                          Brooklyn, NY
                        </span>
                      </div>
                    </div>
                  </div>
                  <ul className="list-unstyled d-flex flex-wrap mx-n2">
                    <li className="px-2">
                      <div className="d-flex">
                        <FormGroup>
                          <Input
                            type="select"
                            className="transparent"
                            size="sm"
                          >
                            <option>Businesses</option>
                          </Input>
                        </FormGroup>
                        <div className="ml-2">
                          <span className="text-danger">*</span>
                        </div>
                      </div>
                    </li>
                    <li className="px-2">
                      <FormGroup>
                        <Input type="select" className="transparent" size="sm">
                          <option>Select Category</option>
                          <option>IT Services</option>
                          <option>Apartment Rentals</option>
                          <option>Automotive</option>
                          <option>Education</option>
                          <option>Beauty and Spas</option>
                        </Input>
                      </FormGroup>
                    </li>
                    <li className="px-2">
                      <FormGroup>
                        <Input type="select" className="transparent" size="sm">
                          <option>Select Category</option>
                          <option>Asian</option>
                          <option>European</option>
                        </Input>
                      </FormGroup>
                    </li>
                    <li className="px-2">
                      <FormGroup>
                        <Input type="select" className="transparent" size="sm">
                          <option>Select Category</option>
                          <option>Indian</option>
                        </Input>
                      </FormGroup>
                    </li>
                    <li className="px-2">
                      <FormGroup>
                        <Input type="select" className="transparent" size="sm">
                          <option>Select Category</option>
                          <option>Level 6</option>
                        </Input>
                      </FormGroup>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <ModalBody className="p-0 fs-14 text-dark bg-transparent">
              <div className="bg-dark text-white">
                <Row noGutters>
                  <Col xs="auto">
                    <div
                      className="d-flex flex-column justify-content-center bg-dark text-white text-center px-3 py-4 h-100 bg-vote"
                      style={{ minWidth: "300px" }}
                    >
                      <div>
                        <img
                          // src={require("../../../assets/images/icons/star/white/fill.png")}
                          alt=""
                        />
                        <h2 className="my-2 fs-50 text-shadow">JURY DUTY</h2>
                        <img
                          // src={require("../../../assets/images/icons/star/white/fill.png")}
                          alt=""
                        />
                      </div>
                    </div>
                  </Col>
                  <Col>
                    <div className="d-flex flex-column justify-content-center bg-dark text-white h-100 p-3">
                      <Row>
                        <Col>
                          <div className="pr-md-5">
                            <h3 className="text-light">
                              Community Initiated dispute.
                            </h3>
                            <div className="fs-14">
                              <div className="text-primary fs-16 font-weight-bold">
                                Step 1. Your vote is needed.
                              </div>
                              <p className="mb-0">
                                Brief description of why you vote your counts
                                and what you'd be voting on. Lorem ipsum, dolor
                                sit amet consectetur adipisicing elit.
                                Dignissimos, non?
                              </p>
                            </div>
                          </div>
                        </Col>
                        <Col md="auto" className="align-self-end">
                          <div className="text-light font-weight-bold">
                            Submission time ends in
                          </div>
                          <div className="d-flex mx-n2 flex-nowrap">
                            <div className="px-2">
                              <div className="text-white">
                                <div className="fs-32 ff-headings">7</div>
                                <div className="font-weight-bold">days</div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  15
                                </div>
                                <div className="font-weight-normal">hours</div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  32
                                </div>
                                <div className="font-weight-normal">
                                  minutes
                                </div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  56
                                </div>
                                <div className="font-weight-normal">
                                  seconds
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="body-bg px-3 py-3">
                <Row>
                  <Col>
                    In case you don't know the correct address, try looking for
                    it with our search bar.
                  </Col>
                  <Col xs="auto">
                    <div className="d-flex">
                      <Input
                        className="primary"
                        bsSize="sm"
                        type="search"
                        placeholder="Custom Search"
                        style={{ minWidth: "300px" }}
                      ></Input>
                      <Button color="primary" size="sm" className="ml-3">
                        <FontAwesomeIcon icon="search" />
                      </Button>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="bg-white px-3 py-4">
                <Row>
                  <Col md={4}>
                    <div className="font-weight-bold fs-16">
                      Dispute History
                    </div>
                    <ul className="d-flex flex-column list-unstyled">
                      <li className="py-2 border-bottom">
                        <div className="d-flex mx-n1">
                          <div className="px-1">
                            <div className="ff-alt">22/04/2014</div>
                          </div>
                          <div className="px-1">
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Sau6402
                            </a>
                            &nbsp; added a Punctuation in Tips.
                          </div>
                        </div>
                      </li>
                      <li className="py-2 border-bottom">
                        <div className="d-flex mx-n1">
                          <div className="px-1">
                            <div className="ff-alt">22/04/2014</div>
                          </div>
                          <div className="px-1">
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Danielbauwens
                            </a>
                            &nbsp; reverted edits by{" "}
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Fart biscuit
                            </a>
                            &nbsp; changed back to last version by{" "}
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Anna
                            </a>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </Col>
                  <Col md={8}>
                    <div className="mb-3">
                      <div className="text-primary fs-16 font-weight-bold mb-2">
                        Step 2. Some Copy Line here
                      </div>
                      <div>
                        <div className="bg-light p-2 text-dark mb-2">
                          <div className="text-tertiary">
                            Flagged for <strong>Reason Something</strong>
                          </div>
                          <hr className="my-2" />
                          <div>
                            <div className="d-flex mx-n2">
                              <div className="px-2">
                                <img
                                  className="img-circle _50x50"
                                  src="https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/PANO_20200111_165638.dcf3bff63e1ebdf3db9823eae44c8ffd7ca89c65.jpg"
                                  alt=""
                                />
                              </div>
                              <div className="px-2">
                                <div className="mb-2">
                                  by{" "}
                                  <a href="#" className="text-reset">
                                    <strong>User's-Name</strong>
                                  </a>
                                  &nbsp;|&nbsp;
                                  <span className="fs-12 text-muted">
                                    one hour ago via iOS7
                                  </span>
                                </div>
                                <div>
                                  "Lorem ipsum dolor sit amet consectetur
                                  adipisicing elit. Aut nobis officia odit
                                  aspernatur obcaecati, dolorum quasi
                                  voluptatibus porro ullam! Perferendis libero
                                  atque perspiciatis voluptatibus quia similique
                                  iste recusandae sequi enim cum, nam quis est
                                  minus corporis sit porro aliquid at."
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="bg-light p-2 text-dark mb-2">
                          <div className="text-tertiary">
                            Flagged for{" "}
                            <strong>Another Reason Something</strong>
                          </div>
                          <hr className="my-2" />
                          <div>
                            <div className="d-flex mx-n2">
                              <div className="px-2">
                                <img
                                  className="img-circle _50x50"
                                  src="https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/PANO_20200111_165638.dcf3bff63e1ebdf3db9823eae44c8ffd7ca89c65.jpg"
                                  alt=""
                                />
                              </div>
                              <div className="px-2">
                                <div className="mb-2">
                                  by{" "}
                                  <a href="#" className="text-reset">
                                    <strong>NameNameName</strong>
                                  </a>
                                  &nbsp;|&nbsp;
                                  <span className="fs-12 text-muted">
                                    one hour ago in Chicago, IL
                                  </span>
                                </div>
                                <div>
                                  "Lorem ipsum dolor sit amet consectetur
                                  adipisicing elit. Aut nobis officia odit
                                  aspernatur obcaecati, dolorum quasi
                                  voluptatibus porro ullam! Perferendis libero
                                  atque perspiciatis voluptatibus quia similique
                                  iste recusandae sequi enim cum, nam quis est
                                  minus corporis sit porro aliquid at."
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="bg-light p-2 text-dark mb-2">
                          <div className="text-tertiary">
                            Flagged for{" "}
                            <strong>Yet Another Reason Something</strong>
                          </div>
                          <hr className="my-2" />
                          <div>
                            <div className="d-flex mx-n2">
                              <div className="px-2">
                                <img
                                  className="img-circle _50x50"
                                  src="https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/PANO_20200111_165638.dcf3bff63e1ebdf3db9823eae44c8ffd7ca89c65.jpg"
                                  alt=""
                                />
                              </div>
                              <div className="px-2">
                                <div className="mb-2">
                                  by{" "}
                                  <a href="#" className="text-reset">
                                    <strong>Messi10</strong>
                                  </a>
                                  &nbsp;|&nbsp;
                                  <span className="fs-12 text-muted">
                                    one hour ago via Android
                                  </span>
                                </div>
                                <div>
                                  "Lorem ipsum dolor sit amet consectetur
                                  adipisicing elit. Aut nobis officia odit
                                  aspernatur obcaecati, dolorum quasi
                                  voluptatibus porro ullam! Perferendis libero
                                  atque perspiciatis voluptatibus quia similique
                                  iste recusandae sequi enim cum, nam quis est
                                  minus corporis sit porro aliquid at."
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="text-tertiary fs-16 font-weight-bold mt-4">
                        Discussion between parties
                      </div>
                      <hr />
                      <div className="text-center mb-2">
                        <div className="text-dark">Dec 16, 2013</div>
                      </div>
                      <div>
                        <div className="p-2 text-dark">
                          <div className="mb-2">
                            <div className="d-flex mx-n2">
                              <div className="px-2">
                                <img
                                  className="img-circle _30x30"
                                  src="https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/PANO_20200111_165638.dcf3bff63e1ebdf3db9823eae44c8ffd7ca89c65.jpg"
                                  alt=""
                                />
                              </div>
                              <div className="px-2">
                                <div className="mb-3">
                                  <div className="d-flex mx-n2 mb-1">
                                    <div className="px-2 col">
                                      <a href="#" className="text-reset">
                                        <strong>User Name</strong>
                                      </a>
                                      &nbsp;
                                      <span className="fs-12 text-muted">
                                        Business Owner
                                      </span>
                                    </div>
                                    <div className="col-auto">
                                      <div className="text-tertiary">
                                        Dec 16, 2013, 15:41
                                      </div>
                                    </div>
                                  </div>
                                  <div>
                                    Lorem ipsum dolor sit amet consectetur
                                    adipisicing elit. Voluptate, incidunt iste,
                                    obcaecati nostrum reprehenderit veniam et
                                    saepe reiciendis voluptatibus tenetur a.
                                    Porro debitis consectetur iste saepe ipsam
                                    libero reprehenderit sed!
                                  </div>
                                </div>
                                <div className="bg-light p-3">
                                  <div className="d-flex mx-n2">
                                    <div className="px-2">
                                      <img
                                        src="https://stagingdatawikireviews.s3.amazonaws.com/images/placeholder.png"
                                        alt=""
                                      />
                                    </div>
                                    <div className="px-2">
                                      <div className="mb-2">
                                        <div className="mb-2">
                                          <a
                                            href="#"
                                            className="text-dark font-weight-bold"
                                          >
                                            www.website.com
                                          </a>
                                        </div>
                                        Lorem ipsum dolor sit amet consectetur
                                        adipisicing elit.
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="mb-2">
                            <div className="d-flex mx-n2">
                              <div className="px-2">
                                <img
                                  className="img-circle _30x30"
                                  src="https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/PANO_20200111_165638.dcf3bff63e1ebdf3db9823eae44c8ffd7ca89c65.jpg"
                                  alt=""
                                />
                              </div>
                              <div className="px-2">
                                <div className="mb-3">
                                  <div className="d-flex mx-n2 mb-1">
                                    <div className="px-2 col">
                                      <a href="#" className="text-reset">
                                        <strong>User Name</strong>
                                      </a>
                                      &nbsp;
                                      <span className="fs-12 text-muted">
                                        Expert Reviewer
                                      </span>
                                    </div>
                                    <div className="col-auto">
                                      <div className="text-tertiary">
                                        Dec 16, 2013, 15:50
                                      </div>
                                    </div>
                                  </div>
                                  <div>
                                    Lorem ipsum dolor sit amet consectetur
                                    adipisicing elit. Voluptate, incidunt iste,
                                    obcaecati nostrum reprehenderit veniam et
                                    saepe reiciendis voluptatibus tenetur a.
                                    Porro debitis consectetur iste saepe ipsam
                                    libero reprehenderit sed!
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="bg-light px-3 py-4">
                <Row>
                  <Col md={4}>
                    <div className="bg-dark text-white text-center p-3 bg-vote-alt">
                      <div className="my-2 ff-headings fs-50">CAST</div>
                      <img
                        className="my-2"
                        // src={require("../../../assets/images/icons/star/white/fill.png")}
                        alt=""
                      />
                      <div className="my-2 ff-headings fs-50">YOUR</div>
                      <img
                        className="my-2"
                        // src={require("../../../assets/images/icons/star/white/fill.png")}
                        alt=""
                      />
                      <div className="my-2 ff-headings fs-50">VOTE</div>
                    </div>
                  </Col>
                  <Col md={8}>
                    <div>
                      <div className="text-primary fs-16 font-weight-bold mb-2">
                        Step 3. Some copy line
                      </div>
                      <div>
                        <div className="bg-white p-3 mb-1">
                          <div className="d-flex mx-n2 mb-2">
                            <div className="px-2 col">
                              <FormGroup className="mb-2" check>
                                <Label
                                  className="text-reset font-weight-normal fs-14 d-block"
                                  check
                                >
                                  <Input type="radio" name="reviewToKeep" />{" "}
                                  <div className="mb-1">
                                    Fake, remove the review.
                                  </div>
                                  <div className="d-flex flex-wrap mx-n2 fs-14">
                                    <div className="px-2 mb-2">25%</div>
                                    <div className="px-2 mb-2 align-self-center">
                                      <Progress
                                        color="tertiary"
                                        value="25"
                                        style={{
                                          height: "6px",
                                          width: "240px",
                                        }}
                                      ></Progress>
                                    </div>
                                    <div className="px-2 mb-2">
                                      5 votes out of 12
                                    </div>
                                  </div>
                                </Label>
                              </FormGroup>
                            </div>
                            <div className="px-2 col-auto">
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Expand"
                              >
                                <FontAwesomeIcon icon="plus" />{" "}
                              </Button>
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Collapse"
                              >
                                <FontAwesomeIcon icon="minus" />{" "}
                              </Button>
                            </div>
                          </div>

                          {/* Collapse content */}
                          <div className="pl-3">Collapse content</div>
                        </div>
                        <div className="bg-white p-3 mb-1">
                          <div className="d-flex mx-n2 mb-2">
                            <div className="px-2 col">
                              <FormGroup className="mb-2" check>
                                <Label
                                  className="text-reset font-weight-normal fs-14 d-block"
                                  check
                                >
                                  <Input type="radio" name="reviewToKeep" />{" "}
                                  <div className="mb-1">
                                    Review is valid, leave it.
                                  </div>
                                  <div className="d-flex flex-wrap mx-n2 fs-14">
                                    <div className="px-2 mb-2">25%</div>
                                    <div className="px-2 mb-2 align-self-center">
                                      <Progress
                                        color="tertiary"
                                        value="25"
                                        style={{
                                          height: "6px",
                                          width: "240px",
                                        }}
                                      ></Progress>
                                    </div>
                                    <div className="px-2 mb-2">
                                      5 votes out of 12
                                    </div>
                                  </div>
                                </Label>
                              </FormGroup>
                            </div>
                            <div className="px-2 col-auto">
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Expand"
                              >
                                <FontAwesomeIcon icon="plus" />{" "}
                              </Button>
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Collapse"
                              >
                                <FontAwesomeIcon icon="minus" />{" "}
                              </Button>
                            </div>
                          </div>

                          {/* Collapse content */}
                          <div className="pl-3">Collapse content</div>
                        </div>
                        <div className="bg-primary text-white p-3 mb-1">
                          <div className="d-flex mx-n2 mb-2">
                            <div className="px-2 col">
                              <FormGroup className="mb-2" check>
                                <Label
                                  className="text-reset font-weight-normal fs-14 d-block"
                                  check
                                >
                                  <Input type="radio" name="reviewToKeep" />{" "}
                                  <div className="mb-1">
                                    Move to Non-Recommended.
                                  </div>
                                  <div className="d-flex flex-wrap mx-n2 fs-14">
                                    <div className="px-2 mb-2">25%</div>
                                    <div className="px-2 mb-2 align-self-center">
                                      <Progress
                                        color="tertiary"
                                        value="25"
                                        style={{
                                          height: "6px",
                                          width: "240px",
                                        }}
                                      ></Progress>
                                    </div>
                                    <div className="px-2 mb-2">
                                      5 votes out of 12
                                    </div>
                                  </div>
                                </Label>
                              </FormGroup>
                            </div>
                            <div className="px-2 col-auto">
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Expand"
                              >
                                <FontAwesomeIcon icon="plus" />{" "}
                              </Button>
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Collapse"
                              >
                                <FontAwesomeIcon icon="minus" />{" "}
                              </Button>
                            </div>
                          </div>

                          {/* Collapse content */}
                          <div className="pl-3">Collapse content</div>
                        </div>
                      </div>
                    </div>
                    <div className="mt-4">
                      <div className="text-primary fs-16 font-weight-bold mb-2">
                        Step 4. Please, tell us why.
                      </div>
                      <div className="text-dark">
                        It would be really helpful for fellow voters to know the
                        reasoning of your decision. It has to be 140 characters
                        long. Some text like that.
                      </div>
                      <div className="text-right text-primary fs-14">65</div>
                      <Input
                        type="textarea"
                        className="primary"
                        rows="3"
                        defaultValue="The Info is accurate, I've been there today."
                        placeholder="Please elaborate..."
                      />
                    </div>
                  </Col>
                </Row>
              </div>
            </ModalBody>
            <ModalFooter className="bg-white">
              <Button color="primary">Submit</Button>
              <Button color="link" className="text-tertiary font-weight-bold">
                Skip
              </Button>
            </ModalFooter>
          </form>
        </Modal>

        <Modal
          size="xl"
          scrollable
          className="drawer-modal"
          isOpen={this.state.voteReviewModalToggleThree}
          toggle={() =>
            this.setState({
              voteReviewModalToggleThree: !this.state
                .voteReviewModalToggleThree,
            })
          }
        >
          <form className="d-flex flex-column h-100">
            <div className="modal-header p-0">
              <div className="w-100">
                <div>
                  <Button
                    color="primary"
                    onClick={() =>
                      this.setState({
                        voteReviewModalToggleThree: !this.state
                          .voteReviewModalToggleThree,
                      })
                    }
                  >
                    <FontAwesomeIcon className="mr-2" icon="angle-left" />
                    back to listing
                  </Button>
                </div>
                <div className="bg-dark text-white fs-14 px-3">
                  <div className="mb-2 pt-2">
                    <div className="d-flex mx-n2">
                      <div className="px-2 col">
                        <span className="fs-14 text-light font-weight-bold">
                          You’re Voting{" "}
                        </span>
                        <UncontrolledDropdown className="d-inline-block">
                          <DropdownToggle
                            className="text-white bg-transparent shadow-none font-weight-bold"
                            color="transparent"
                            size="sm"
                            caret
                          >
                            <span className="pr-2">Disputed Reviews</span>
                          </DropdownToggle>
                          <DropdownMenu>
                            <DropdownItem className="fs-14">
                              Disputed Reviews
                            </DropdownItem>
                          </DropdownMenu>
                        </UncontrolledDropdown>
                      </div>
                      <div className="px-2 col-auto">
                        near{" "}
                        <span className="text-light font-weight-bold">
                          Brooklyn, NY
                        </span>
                      </div>
                    </div>
                  </div>
                  <ul className="list-unstyled d-flex flex-wrap mx-n2">
                    <li className="px-2">
                      <div className="d-flex">
                        <FormGroup>
                          <Input
                            type="select"
                            className="transparent"
                            size="sm"
                          >
                            <option>Businesses</option>
                          </Input>
                        </FormGroup>
                        <div className="ml-2">
                          <span className="text-danger">*</span>
                        </div>
                      </div>
                    </li>
                    <li className="px-2">
                      <FormGroup>
                        <Input type="select" className="transparent" size="sm">
                          <option>Select Category</option>
                          <option>IT Services</option>
                          <option>Apartment Rentals</option>
                          <option>Automotive</option>
                          <option>Education</option>
                          <option>Beauty and Spas</option>
                        </Input>
                      </FormGroup>
                    </li>
                    <li className="px-2">
                      <FormGroup>
                        <Input type="select" className="transparent" size="sm">
                          <option>Select Category</option>
                          <option>Asian</option>
                          <option>European</option>
                        </Input>
                      </FormGroup>
                    </li>
                    <li className="px-2">
                      <FormGroup>
                        <Input type="select" className="transparent" size="sm">
                          <option>Select Category</option>
                          <option>Indian</option>
                        </Input>
                      </FormGroup>
                    </li>
                    <li className="px-2">
                      <FormGroup>
                        <Input type="select" className="transparent" size="sm">
                          <option>Select Category</option>
                          <option>Level 6</option>
                        </Input>
                      </FormGroup>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <ModalBody className="p-0 fs-14 text-dark bg-transparent">
              <div className="bg-dark text-white">
                <Row noGutters>
                  <Col xs="auto">
                    <div
                      className="d-flex flex-column justify-content-center bg-dark text-white text-center px-3 py-4 h-100 bg-vote"
                      style={{ minWidth: "300px" }}
                    >
                      <div>
                        <img
                          // src={require("../../../assets/images/icons/star/white/fill.png")}
                          alt=""
                        />
                        <h2 className="my-2 fs-50 text-shadow">JURY DUTY</h2>
                        <img
                          // src={require("../../../assets/images/icons/star/white/fill.png")}
                          alt=""
                        />
                      </div>
                    </div>
                  </Col>
                  <Col>
                    <div className="d-flex flex-column justify-content-center bg-dark text-white h-100 p-3">
                      <Row>
                        <Col>
                          <div className="pr-md-5">
                            <h3 className="text-light">
                              Business Owner dispute.
                            </h3>
                            <div className="fs-14">
                              <div className="text-primary fs-16 font-weight-bold">
                                Step 1. Your vote is needed.
                              </div>
                              <p className="mb-0">
                                Brief description of why you vote your counts
                                and what you'd be voting on. Lorem ipsum, dolor
                                sit amet consectetur adipisicing elit.
                                Dignissimos, non?
                              </p>
                            </div>
                          </div>
                        </Col>
                        <Col md="auto" className="align-self-end">
                          <div className="text-light font-weight-bold">
                            Submission time ends in
                          </div>
                          <div className="d-flex mx-n2 flex-nowrap">
                            <div className="px-2">
                              <div className="text-white">
                                <div className="fs-32 ff-headings">7</div>
                                <div className="font-weight-bold">days</div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  15
                                </div>
                                <div className="font-weight-normal">hours</div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  32
                                </div>
                                <div className="font-weight-normal">
                                  minutes
                                </div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  56
                                </div>
                                <div className="font-weight-normal">
                                  seconds
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="body-bg px-3 py-3">
                <Row>
                  <Col>
                    In case you don't know the correct address, try looking for
                    it with our search bar.
                  </Col>
                  <Col xs="auto">
                    <div className="d-flex">
                      <Input
                        className="primary"
                        bsSize="sm"
                        type="search"
                        placeholder="Custom Search"
                        style={{ minWidth: "300px" }}
                      ></Input>
                      <Button color="primary" size="sm" className="ml-3">
                        <FontAwesomeIcon icon="search" />
                      </Button>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="bg-white px-3 py-4">
                <Row>
                  <Col md={4}>
                    <div className="font-weight-bold fs-16 text-primary">
                      Step 2. Voting History
                    </div>
                    <ul className="d-flex flex-column list-unstyled">
                      <li className="py-2 border-bottom">
                        <div className="d-flex mx-n1">
                          <div className="px-1">
                            <div className="ff-alt">22/04/2014</div>
                          </div>
                          <div className="px-1">
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Sau6402
                            </a>
                            &nbsp; added a Punctuation in Tips.
                          </div>
                        </div>
                      </li>
                      <li className="py-2 border-bottom">
                        <div className="d-flex mx-n1">
                          <div className="px-1">
                            <div className="ff-alt">22/04/2014</div>
                          </div>
                          <div className="px-1">
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Danielbauwens
                            </a>
                            &nbsp; reverted edits by{" "}
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Fart biscuit
                            </a>
                            &nbsp; changed back to last version by{" "}
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Anna
                            </a>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </Col>
                  <Col md={8}>
                    <div className="mb-3">
                      <div className="text-primary fs-16 font-weight-bold mb-2">
                        Step 3. Review Lorem Ipsum
                      </div>
                      <div>
                        <Row>
                          <Col lg={6}>
                            <div className="bg-white text-center text-tertiary p-3">
                              <div className="mb-2">
                                <img
                                  className="img-circle _50x50"
                                  // src={require("../../../assets/images/icons/user-circle.png")}
                                  alt=""
                                />
                              </div>
                              <div className="font-weight-bold fs-18">
                                User John
                              </div>
                              <div className="text-tertiary">
                                a.k.a. The Reviewer
                              </div>
                              <div className="mt-3">
                                {/* Repeat this */}
                                <div className="mb-4">
                                  <div className="ff-alt">
                                    <FontAwesomeIcon
                                      icon="quote-left"
                                      size="lg"
                                    />
                                    Lorem ipsum dolor, sit amet consectetur
                                    adipisicing elit. Aut facilis nesciunt odio
                                    illum debitis vitae corporis suscipit
                                    molestias porro aperiam.
                                    <FontAwesomeIcon
                                      icon="quote-right"
                                      size="lg"
                                    />
                                  </div>
                                  <div className="mt-4">
                                    <a
                                      href="#"
                                      className="text-reset font-weight-bold"
                                    >
                                      letterpfintent.doc
                                    </a>
                                  </div>
                                </div>
                                <div className="mb-3">
                                  <div className="ff-alt">
                                    <FontAwesomeIcon
                                      icon="quote-left"
                                      size="lg"
                                    />
                                    Lorem ipsum dolor, sit amet consectetur
                                    adipisicing elit. Aut facilis nesciunt odio
                                    illum debitis vitae corporis suscipit
                                    molestias porro aperiam.
                                    <FontAwesomeIcon
                                      icon="quote-right"
                                      size="lg"
                                    />
                                  </div>
                                  <div className="mt-4">
                                    <a
                                      href="#"
                                      className="text-reset font-weight-bold"
                                    >
                                      supporting.doc
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Col>
                          <Col lg={6}>
                            <div className="bg-light text-center text-tertiary p-3">
                              <div className="mb-2">
                                <img
                                  className="img-circle _50x50"
                                  // src={require("../../../assets/images/icons/user-circle.png")}
                                  alt=""
                                />
                              </div>
                              <div className="font-weight-bold fs-18">
                                Business Owner
                              </div>
                              <div className="text-tertiary">
                                a.k.a. The Business Owner
                              </div>
                              <div className="mt-3">
                                {/* Repeat this */}
                                <div className="mb-4">
                                  <div className="ff-alt">
                                    <FontAwesomeIcon
                                      icon="quote-left"
                                      size="lg"
                                    />
                                    Lorem ipsum dolor, sit amet consectetur
                                    adipisicing elit. Aut facilis nesciunt odio
                                    illum debitis vitae corporis suscipit
                                    molestias porro aperiam.
                                    <FontAwesomeIcon
                                      icon="quote-right"
                                      size="lg"
                                    />
                                  </div>
                                  <div className="mt-4">
                                    <span>no proofs submitten</span>
                                  </div>
                                </div>
                                <div className="mb-3">
                                  <div className="ff-alt">
                                    <FontAwesomeIcon
                                      icon="quote-left"
                                      size="lg"
                                    />
                                    Lorem ipsum dolor, sit amet consectetur
                                    adipisicing elit. Aut facilis nesciunt odio
                                    illum debitis vitae corporis suscipit
                                    molestias porro aperiam.
                                    <FontAwesomeIcon
                                      icon="quote-right"
                                      size="lg"
                                    />
                                  </div>
                                  <div className="mt-4">
                                    <a
                                      href="#"
                                      className="text-reset font-weight-bold"
                                    >
                                      proof-of-some-type.pdf
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="bg-white px-3 py-4">
                <Row>
                  <Col md={4}>&nbsp;</Col>
                  <Col md={8}>
                    <div>
                      <div className="text-primary fs-16 font-weight-bold mb-2">
                        Step 4. Does the Business Owner have sufficient evidence
                        to support any of the following:
                        <div className="fs-13 text-dark font-weight-normal">
                          Please put as much info (images, video, documents
                          etc.) as possible to support your statements.
                        </div>
                      </div>
                      <div>
                        <div className="bg-light p-3 mb-1">
                          <div className="d-flex mx-n2">
                            <div className="px-2 col">
                              <FormGroup className="mb-2" check>
                                <Label
                                  className="text-reset font-weight-normal fs-14 d-block"
                                  check
                                >
                                  <Input type="radio" name="reviewToKeep" />{" "}
                                  <div className="mb-1">
                                    The review is inaccurate
                                  </div>
                                  <div className="ff-alt">
                                    Must have evidence.
                                  </div>
                                </Label>
                              </FormGroup>
                            </div>
                          </div>
                        </div>
                        <div className="bg-light p-3 mb-1">
                          <div className="d-flex mx-n2">
                            <div className="px-2 col">
                              <FormGroup className="mb-2" check>
                                <Label
                                  className="text-reset font-weight-normal fs-14 d-block"
                                  check
                                >
                                  <Input type="radio" name="reviewToKeep" />{" "}
                                  <div className="mb-1">Review is fake.</div>
                                  <div className="ff-alt">
                                    Must explain why or have some type of
                                    evidence.
                                  </div>
                                </Label>
                              </FormGroup>
                            </div>
                          </div>
                        </div>
                        <div className="bg-light p-3 mb-1">
                          <div className="d-flex mx-n2">
                            <div className="px-2 col">
                              <FormGroup className="mb-2" check>
                                <Label
                                  className="text-reset font-weight-normal fs-14 d-block"
                                  check
                                >
                                  <Input type="radio" name="reviewToKeep" />{" "}
                                  <div className="mb-1">
                                    Review is Not Relevant.
                                  </div>
                                  <div className="ff-alt">
                                    Must explain why.
                                  </div>
                                </Label>
                              </FormGroup>
                            </div>
                          </div>
                        </div>
                        <div className="bg-light p-3 mb-1">
                          <div className="d-flex mx-n2">
                            <div className="px-2 col">
                              <FormGroup className="mb-2" check>
                                <Label
                                  className="text-reset font-weight-normal fs-14 d-block"
                                  check
                                >
                                  <Input type="radio" name="reviewToKeep" />{" "}
                                  <div className="mb-1">
                                    Do not dispute review.
                                  </div>
                                  <div className="ff-alt">
                                    The dispute does not sufficiently meet any
                                    of the first three criteria
                                  </div>
                                </Label>
                              </FormGroup>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="mt-4">
                      <div className="text-primary fs-16 font-weight-bold mb-2">
                        Step 5. Please, tell us why.
                      </div>
                      <div className="text-dark">
                        It would be really helpful for fellow voters to know the
                        reasoning of your decision. It has to be 140 characters
                        long. Some text like that.
                      </div>
                      <div className="text-right text-primary fs-14">65</div>
                      <Input
                        type="textarea"
                        className="primary"
                        rows="3"
                        defaultValue="The Info is accurate, I've been there today."
                        placeholder="Please elaborate..."
                      />
                    </div>
                  </Col>
                </Row>
              </div>
            </ModalBody>
            <ModalFooter className="bg-white">
              <Button color="primary">Submit</Button>
              <Button color="link" className="text-tertiary font-weight-bold">
                Skip
              </Button>
            </ModalFooter>
          </form>
        </Modal>

        <Modal
          size="xl"
          scrollable
          className="drawer-modal"
          isOpen={this.state.voteReviewModalToggleFive}
          toggle={() =>
            this.setState({
              voteReviewModalToggleFive: !this.state.voteReviewModalToggleFive,
            })
          }
        >
          <form className="d-flex flex-column h-100">
            <div className="modal-header p-0">
              <div className="w-100">
                <div>
                  <Button
                    color="primary"
                    onClick={() =>
                      this.setState({
                        voteReviewModalToggleFive: !this.state
                          .voteReviewModalToggleFive,
                      })
                    }
                  >
                    <FontAwesomeIcon className="mr-2" icon="angle-left" />
                    back to listing
                  </Button>
                </div>
              </div>
            </div>
            <ModalBody className="p-0 fs-14 text-dark bg-transparent">
              <div className="bg-dark text-white">
                <Row noGutters>
                  <Col xs="auto">
                    <div
                      className="d-flex flex-column justify-content-center bg-tertiary text-white text-center px-3 py-4 h-100"
                      style={{ minWidth: "300px" }}
                    >
                      <div>
                        <img
                          // src={require("../../../assets/images/icons/star/blue/fill.png")}
                          alt=""
                        />
                        <h2 className="my-2 fs-50 text-shadow">
                          REVIEW DISPUTE
                        </h2>
                        <img
                          // src={require("../../../assets/images/icons/star/blue/fill.png")}
                          alt=""
                        />
                      </div>
                    </div>
                  </Col>
                  <Col>
                    <div className="d-flex flex-column justify-content-center bg-dark text-white h-100 p-3">
                      <Row>
                        <Col>
                          <div className="pr-md-5">
                            <h3 className="text-light">
                              Business Owner initiated.
                            </h3>
                            <div className="fs-14">
                              <div className="text-primary fs-16 font-weight-bold">
                                Step 1. Lorem Ipsum.
                              </div>
                              <p className="mb-0">
                                Brief description of why you vote your counts
                                and what you'd be voting on. Lorem ipsum, dolor
                                sit amet consectetur adipisicing elit.
                                Dignissimos, non?
                              </p>
                            </div>
                          </div>
                        </Col>
                        <Col md="auto" className="align-self-end">
                          <div className="text-light font-weight-bold">
                            Submission time ends in
                          </div>
                          <div className="d-flex mx-n2 flex-nowrap">
                            <div className="px-2">
                              <div className="text-white">
                                <div className="fs-32 ff-headings">7</div>
                                <div className="font-weight-bold">days</div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  15
                                </div>
                                <div className="font-weight-normal">hours</div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  32
                                </div>
                                <div className="font-weight-normal">
                                  minutes
                                </div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  56
                                </div>
                                <div className="font-weight-normal">
                                  seconds
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="body-bg px-3 py-3">
                <Row>
                  <Col>
                    In case you don't know the correct address, try looking for
                    it with our search bar.
                  </Col>
                  <Col xs="auto">
                    <div className="d-flex">
                      <Input
                        className="primary"
                        bsSize="sm"
                        type="search"
                        placeholder="Custom Search"
                        style={{ minWidth: "300px" }}
                      ></Input>
                      <Button color="primary" size="sm" className="ml-3">
                        <FontAwesomeIcon icon="search" />
                      </Button>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="bg-white px-3 py-4">
                <Row>
                  <Col md={4}>
                    <div className="font-weight-bold fs-16 text-dark">
                      Dispute History
                    </div>
                    <ul className="d-flex flex-column list-unstyled">
                      <li className="py-2 border-bottom">
                        <div className="d-flex mx-n1">
                          <div className="px-1">
                            <div className="ff-alt">22/04/2014</div>
                          </div>
                          <div className="px-1">
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Sau6402
                            </a>
                            &nbsp; added a Punctuation in Tips.
                          </div>
                        </div>
                      </li>
                      <li className="py-2 border-bottom">
                        <div className="d-flex mx-n1">
                          <div className="px-1">
                            <div className="ff-alt">22/04/2014</div>
                          </div>
                          <div className="px-1">
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Danielbauwens
                            </a>
                            &nbsp; reverted edits by{" "}
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Fart biscuit
                            </a>
                            &nbsp; changed back to last version by{" "}
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Anna
                            </a>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </Col>
                  <Col md={8}>
                    <Row xs={1} lg={2}>
                      <Col>
                        <div className="text-primary fs-16 font-weight-bold mb-2">
                          Step 2. Review Owner claims
                        </div>
                      </Col>
                      <Col>
                        <div className="text-primary fs-16 font-weight-bold mb-2">
                          Step 2. Respond to claims
                        </div>
                      </Col>
                      <Col className="bg-light text-center text-tertiary">
                        <div className="p-3">
                          <div className="mb-2 user-profile-img-holder">
                            <img
                              className="img-circle _50x50"
                              // src={require("../../../assets/images/icons/user-circle.png")}
                              alt=""
                            />
                            {/* <span className="user-profile-badge">
                              <img width="24" src="https://cdn.icon-icons.com/icons2/1527/PNG/512/shield_106660.png" alt="" />
                            </span> */}
                          </div>
                          <div className="font-weight-bold fs-18">
                            Business Owner
                          </div>
                          <div className="text-tertiary">
                            a.k.a. The Business Owner
                          </div>
                        </div>
                      </Col>
                      <Col className="bg-white text-center text-dark">
                        <div className="p-3">
                          <div className="mb-2 user-profile-img-holder">
                            <img
                              className="img-circle _50x50"
                              // src={require("../../../assets/images/icons/user-circle.png")}
                              alt=""
                            />
                            <span className="user-profile-badge">
                              <img
                                width="24"
                                src="https://cdn.icon-icons.com/icons2/1527/PNG/512/shield_106660.png"
                                alt=""
                              />
                            </span>
                          </div>
                          <div className="font-weight-bold fs-18">
                            Name LastName
                          </div>
                          <div className="text-tertiary">You</div>
                        </div>
                      </Col>
                      <Col className="bg-light text-center text-tertiary d-flex flex-column">
                        <div className="my-4">
                          <div className="ff-alt">
                            <FontAwesomeIcon icon="quote-left" size="lg" />
                            Lorem ipsum dolor, sit amet consectetur adipisicing
                            elit. Aut facilis nesciunt odio illum debitis vitae
                            corporis suscipit molestias porro aperiam.
                            <FontAwesomeIcon icon="quote-right" size="lg" />
                          </div>
                          <div className="mt-4">
                            <a href="#" className="text-reset font-weight-bold">
                              proof-number-one.pdf
                            </a>
                          </div>
                        </div>
                        <div className="mt-auto">
                          <hr className="mt-0" />
                        </div>
                      </Col>
                      <Col className="bg-white text-dark d-flex flex-column">
                        <div className="mb-4">
                          <FormGroup className="main-post-formgroup">
                            <div className="input-label-block">
                              <Input
                                className="primary"
                                type="textarea"
                                rows="2"
                                placeholder="&nbsp;"
                              />
                            </div>
                          </FormGroup>
                          <FormGroup>
                            <div className="text-tertiary font-weight-bold mb-2">
                              Submit proofs
                            </div>
                            <CustomInput bsSize="sm" type="file" />
                          </FormGroup>
                          <div className="text-right">
                            <div className="text-tertiary">
                              or{" "}
                              <span className="font-weight-bold" role="button">
                                embed link
                              </span>
                            </div>
                          </div>
                        </div>
                        <div className="mt-auto">
                          <hr className="mt-0" />
                        </div>
                      </Col>
                      <Col className="bg-light text-center text-tertiary d-flex flex-column">
                        <div className="my-4">
                          <div className="ff-alt">
                            <FontAwesomeIcon icon="quote-left" size="lg" />
                            Lorem ipsum dolor, sit amet consectetur adipisicing
                            elit. Aut facilis nesciunt odio illum debitis vitae
                            corporis suscipit molestias porro aperiam.
                            <FontAwesomeIcon icon="quote-right" size="lg" />
                          </div>
                          <div className="mt-4">
                            <a href="#" className="text-reset font-weight-bold">
                              proof-number-two.pdf
                            </a>
                          </div>
                        </div>
                        <div className="mt-auto">
                          <hr className="mt-0" />
                        </div>
                      </Col>
                      <Col className="bg-white text-dark d-flex flex-column">
                        <div className="mb-4">
                          <FormGroup className="main-post-formgroup">
                            <div className="input-label-block">
                              <Input
                                className="primary"
                                type="textarea"
                                rows="2"
                                placeholder="&nbsp;"
                              />
                            </div>
                          </FormGroup>
                          <FormGroup>
                            <div className="text-tertiary font-weight-bold mb-2">
                              Submit proofs
                            </div>
                            <CustomInput bsSize="sm" type="file" />
                          </FormGroup>
                          <div className="text-right">
                            <div className="text-tertiary">
                              or{" "}
                              <span className="font-weight-bold" role="button">
                                embed link
                              </span>
                            </div>
                          </div>
                        </div>
                        <div className="mt-auto">
                          <hr className="mt-0" />
                        </div>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </div>
            </ModalBody>
            <ModalFooter className="bg-white">
              <Button color="primary">Submit</Button>
              <Button color="link" className="text-tertiary font-weight-bold">
                Skip
              </Button>
            </ModalFooter>
          </form>
        </Modal>

        <Modal
          size="xl"
          scrollable
          className="drawer-modal"
          isOpen={this.state.voteReviewModalToggleSix}
          toggle={() =>
            this.setState({
              voteReviewModalToggleSix: !this.state.voteReviewModalToggleSix,
            })
          }
        >
          <form className="d-flex flex-column h-100">
            <div className="modal-header p-0">
              <div className="w-100">
                <div>
                  <Button
                    color="primary"
                    onClick={() =>
                      this.setState({
                        voteReviewModalToggleSix: !this.state
                          .voteReviewModalToggleSix,
                      })
                    }
                  >
                    <FontAwesomeIcon className="mr-2" icon="angle-left" />
                    back to listing
                  </Button>
                </div>
                <div className="bg-dark text-white fs-14 px-3">
                  <div className="mb-2 pt-2">
                    <div className="d-flex mx-n2">
                      <div className="px-2 col">
                        <span className="fs-14 text-light font-weight-bold">
                          You’re Voting{" "}
                        </span>
                        <UncontrolledDropdown className="d-inline-block">
                          <DropdownToggle
                            className="text-white bg-transparent shadow-none font-weight-bold"
                            color="transparent"
                            size="sm"
                            caret
                          >
                            <span className="pr-2">Disputed Reviews</span>
                          </DropdownToggle>
                          <DropdownMenu>
                            <DropdownItem className="fs-14">
                              Disputed Reviews
                            </DropdownItem>
                          </DropdownMenu>
                        </UncontrolledDropdown>
                      </div>
                      <div className="px-2 col-auto">
                        near{" "}
                        <span className="text-light font-weight-bold">
                          Brooklyn, NY
                        </span>
                      </div>
                    </div>
                  </div>
                  <ul className="list-unstyled d-flex flex-wrap mx-n2">
                    <li className="px-2">
                      <div className="d-flex">
                        <FormGroup>
                          <Input
                            type="select"
                            className="transparent"
                            size="sm"
                          >
                            <option>Businesses</option>
                          </Input>
                        </FormGroup>
                        <div className="ml-2">
                          <span className="text-danger">*</span>
                        </div>
                      </div>
                    </li>
                    <li className="px-2">
                      <FormGroup>
                        <Input type="select" className="transparent" size="sm">
                          <option>Select Category</option>
                          <option>IT Services</option>
                          <option>Apartment Rentals</option>
                          <option>Automotive</option>
                          <option>Education</option>
                          <option>Beauty and Spas</option>
                        </Input>
                      </FormGroup>
                    </li>
                    <li className="px-2">
                      <FormGroup>
                        <Input type="select" className="transparent" size="sm">
                          <option>Select Category</option>
                          <option>Asian</option>
                          <option>European</option>
                        </Input>
                      </FormGroup>
                    </li>
                    <li className="px-2">
                      <FormGroup>
                        <Input type="select" className="transparent" size="sm">
                          <option>Select Category</option>
                          <option>Indian</option>
                        </Input>
                      </FormGroup>
                    </li>
                    <li className="px-2">
                      <FormGroup>
                        <Input type="select" className="transparent" size="sm">
                          <option>Select Category</option>
                          <option>Level 6</option>
                        </Input>
                      </FormGroup>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <ModalBody className="p-0 fs-14 text-dark bg-transparent">
              <div className="bg-dark text-white">
                <Row noGutters>
                  <Col xs="auto">
                    <div
                      className="d-flex flex-column justify-content-center bg-dark text-white text-center px-3 py-4 h-100 bg-vote"
                      style={{ minWidth: "300px" }}
                    >
                      <div>
                        <img
                          // src={require("../../../assets/images/icons/star/white/fill.png")}
                          alt=""
                        />
                        <h2 className="my-2 fs-50 text-shadow">JURY DUTY</h2>
                        <img
                          // src={require("../../../assets/images/icons/star/white/fill.png")}
                          alt=""
                        />
                      </div>
                    </div>
                  </Col>
                  <Col>
                    <div className="d-flex flex-column justify-content-center bg-dark text-white h-100 p-3">
                      <Row>
                        <Col>
                          <div className="pr-md-5">
                            <h3 className="text-light">
                              Business Owner dispute.
                            </h3>
                            <div className="fs-14">
                              <div className="text-primary fs-16 font-weight-bold">
                                Step 1. Your vote is needed.
                              </div>
                              <p className="mb-0">
                                Brief description of why you vote your counts
                                and what you'd be voting on. Lorem ipsum, dolor
                                sit amet consectetur adipisicing elit.
                                Dignissimos, non?
                              </p>
                            </div>
                          </div>
                        </Col>
                        <Col md="auto" className="align-self-end">
                          <div className="text-light font-weight-bold">
                            Submission time ends in
                          </div>
                          <div className="d-flex mx-n2 flex-nowrap">
                            <div className="px-2">
                              <div className="text-white">
                                <div className="fs-32 ff-headings">7</div>
                                <div className="font-weight-bold">days</div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  15
                                </div>
                                <div className="font-weight-normal">hours</div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  32
                                </div>
                                <div className="font-weight-normal">
                                  minutes
                                </div>
                              </div>
                            </div>
                            <div className="px-2">
                              <div className="text-muted">
                                <div className="fs-32 ff-headings text-right">
                                  56
                                </div>
                                <div className="font-weight-normal">
                                  seconds
                                </div>
                              </div>
                            </div>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="body-bg px-3 py-3">
                <Row>
                  <Col>
                    In case you don't know the correct address, try looking for
                    it with our search bar.
                  </Col>
                  <Col xs="auto">
                    <div className="d-flex">
                      <Input
                        className="primary"
                        bsSize="sm"
                        type="search"
                        placeholder="Custom Search"
                        style={{ minWidth: "300px" }}
                      ></Input>
                      <Button color="primary" size="sm" className="ml-3">
                        <FontAwesomeIcon icon="search" />
                      </Button>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="bg-white px-3 py-4">
                <Row>
                  <Col md={4}>
                    <div className="font-weight-bold fs-16 text-primary">
                      Step 2. Voting History
                    </div>
                    <ul className="d-flex flex-column list-unstyled">
                      <li className="py-2 border-bottom">
                        <div className="d-flex mx-n1">
                          <div className="px-1">
                            <div className="ff-alt">22/04/2014</div>
                          </div>
                          <div className="px-1">
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Sau6402
                            </a>
                            &nbsp; added a Punctuation in Tips.
                          </div>
                        </div>
                      </li>
                      <li className="py-2 border-bottom">
                        <div className="d-flex mx-n1">
                          <div className="px-1">
                            <div className="ff-alt">22/04/2014</div>
                          </div>
                          <div className="px-1">
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Danielbauwens
                            </a>
                            &nbsp; reverted edits by{" "}
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Fart biscuit
                            </a>
                            &nbsp; changed back to last version by{" "}
                            <a
                              href="#"
                              className="text-dark ff-base font-weight-bold"
                            >
                              Anna
                            </a>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </Col>
                  <Col md={8}>
                    <div className="mb-3">
                      <div className="text-primary fs-16 font-weight-bold mb-2">
                        Step 3. Review Lorem Ipsum
                      </div>
                      <div>
                        <Row>
                          <Col lg={6}>
                            <div className="bg-white text-center text-tertiary p-3">
                              <div className="mb-2">
                                <img
                                  className="img-circle _50x50"
                                  // src={require("../../../assets/images/icons/user-circle.png")}
                                  alt=""
                                />
                              </div>
                              <div className="font-weight-bold fs-18">
                                User John
                              </div>
                              <div className="text-tertiary">
                                a.k.a. The Reviewer
                              </div>
                              <div className="mt-3">
                                {/* Repeat this */}
                                <div className="mb-4">
                                  <div className="ff-alt">
                                    <FontAwesomeIcon
                                      icon="quote-left"
                                      size="lg"
                                    />
                                    Lorem ipsum dolor, sit amet consectetur
                                    adipisicing elit. Aut facilis nesciunt odio
                                    illum debitis vitae corporis suscipit
                                    molestias porro aperiam.
                                    <FontAwesomeIcon
                                      icon="quote-right"
                                      size="lg"
                                    />
                                  </div>
                                  <div className="mt-4">
                                    <a
                                      href="#"
                                      className="text-reset font-weight-bold"
                                    >
                                      letterpfintent.doc
                                    </a>
                                  </div>
                                </div>
                                <div className="mb-3">
                                  <div className="ff-alt">
                                    <FontAwesomeIcon
                                      icon="quote-left"
                                      size="lg"
                                    />
                                    Lorem ipsum dolor, sit amet consectetur
                                    adipisicing elit. Aut facilis nesciunt odio
                                    illum debitis vitae corporis suscipit
                                    molestias porro aperiam.
                                    <FontAwesomeIcon
                                      icon="quote-right"
                                      size="lg"
                                    />
                                  </div>
                                  <div className="mt-4">
                                    <a
                                      href="#"
                                      className="text-reset font-weight-bold"
                                    >
                                      supporting.doc
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Col>
                          <Col lg={6}>
                            <div className="bg-light text-center text-tertiary p-3">
                              <div className="mb-2">
                                <img
                                  className="img-circle _50x50"
                                  // src={require("../../../assets/images/icons/user-circle.png")}
                                  alt=""
                                />
                              </div>
                              <div className="font-weight-bold fs-18">
                                Business Owner
                              </div>
                              <div className="text-tertiary">
                                a.k.a. The Business Owner
                              </div>
                              <div className="mt-3">
                                {/* Repeat this */}
                                <div className="mb-4">
                                  <div className="ff-alt">
                                    <FontAwesomeIcon
                                      icon="quote-left"
                                      size="lg"
                                    />
                                    Lorem ipsum dolor, sit amet consectetur
                                    adipisicing elit. Aut facilis nesciunt odio
                                    illum debitis vitae corporis suscipit
                                    molestias porro aperiam.
                                    <FontAwesomeIcon
                                      icon="quote-right"
                                      size="lg"
                                    />
                                  </div>
                                  <div className="mt-4">
                                    <span>no proofs submitten</span>
                                  </div>
                                </div>
                                <div className="mb-3">
                                  <div className="ff-alt">
                                    <FontAwesomeIcon
                                      icon="quote-left"
                                      size="lg"
                                    />
                                    Lorem ipsum dolor, sit amet consectetur
                                    adipisicing elit. Aut facilis nesciunt odio
                                    illum debitis vitae corporis suscipit
                                    molestias porro aperiam.
                                    <FontAwesomeIcon
                                      icon="quote-right"
                                      size="lg"
                                    />
                                  </div>
                                  <div className="mt-4">
                                    <a
                                      href="#"
                                      className="text-reset font-weight-bold"
                                    >
                                      proof-of-some-type.pdf
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
              <div className="bg-white px-3 py-4">
                <Row>
                  <Col md={4}>&nbsp;</Col>
                  <Col md={8}>
                    {/* Step 4 */}
                    <div>
                      <div className="text-primary fs-16 font-weight-bold mb-2">
                        Step 4. Does the Business Owner have sufficient evidence
                        to support any of the following:
                        <div className="fs-13 text-dark font-weight-normal">
                          Please put as much info (images, video, documents
                          etc.) as possible to support your statements.
                        </div>
                      </div>
                      <div>
                        <div className="bg-light p-3 mb-1">
                          <div className="d-flex mx-n2 mb-2">
                            <div className="px-2 col">
                              <FormGroup className="mb-2" check>
                                <Label
                                  className="text-reset font-weight-normal fs-14 d-block"
                                  check
                                >
                                  <Input type="radio" name="reviewToKeep" />{" "}
                                  <div className="mb-1">
                                    This review is inaccurate
                                  </div>
                                  <div className="fs-12 ff-alt">
                                    Must have evidence
                                  </div>
                                  <div className="d-flex flex-wrap mx-n2 fs-14">
                                    <div className="px-2 mb-2">25%</div>
                                    <div className="px-2 mb-2 align-self-center">
                                      <Progress
                                        color="tertiary"
                                        value="25"
                                        style={{
                                          height: "6px",
                                          width: "240px",
                                        }}
                                      ></Progress>
                                    </div>
                                    <div className="px-2 mb-2">
                                      5 votes out of 12
                                    </div>
                                  </div>
                                </Label>
                              </FormGroup>
                            </div>
                            <div className="px-2 col-auto">
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Expand"
                              >
                                <FontAwesomeIcon icon="plus" />{" "}
                              </Button>
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Collapse"
                              >
                                <FontAwesomeIcon icon="minus" />{" "}
                              </Button>
                            </div>
                          </div>

                          {/* Collapse content */}
                          <div className="pl-3">Collapse content</div>
                        </div>
                        <div className="bg-light p-3 mb-1">
                          <div className="d-flex mx-n2 mb-2">
                            <div className="px-2 col">
                              <FormGroup className="mb-2" check>
                                <Label
                                  className="text-reset font-weight-normal fs-14 d-block"
                                  check
                                >
                                  <Input type="radio" name="reviewToKeep" />{" "}
                                  <div className="mb-1">Review is fake.</div>
                                  <div className="fs-12 ff-alt">
                                    Must explain why or have some type of
                                    evidence.
                                  </div>
                                  <div className="d-flex flex-wrap mx-n2 fs-14">
                                    <div className="px-2 mb-2">25%</div>
                                    <div className="px-2 mb-2 align-self-center">
                                      <Progress
                                        color="tertiary"
                                        value="25"
                                        style={{
                                          height: "6px",
                                          width: "240px",
                                        }}
                                      ></Progress>
                                    </div>
                                    <div className="px-2 mb-2">
                                      5 votes out of 12
                                    </div>
                                  </div>
                                </Label>
                              </FormGroup>
                            </div>
                            <div className="px-2 col-auto">
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Expand"
                              >
                                <FontAwesomeIcon icon="plus" />{" "}
                              </Button>
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Collapse"
                              >
                                <FontAwesomeIcon icon="minus" />{" "}
                              </Button>
                            </div>
                          </div>

                          {/* Collapse content */}
                          <div className="pl-3">Collapse content</div>
                        </div>
                        <div className="bg-light p-3 mb-1">
                          <div className="d-flex mx-n2 mb-2">
                            <div className="px-2 col">
                              <FormGroup className="mb-2" check>
                                <Label
                                  className="text-reset font-weight-normal fs-14 d-block"
                                  check
                                >
                                  <Input type="radio" name="reviewToKeep" />{" "}
                                  <div className="mb-1">
                                    Review is Not Relevant.
                                  </div>
                                  <div className="fs-12 ff-alt">
                                    Must explain why.
                                  </div>
                                  <div className="d-flex flex-wrap mx-n2 fs-14">
                                    <div className="px-2 mb-2">25%</div>
                                    <div className="px-2 mb-2 align-self-center">
                                      <Progress
                                        color="tertiary"
                                        value="25"
                                        style={{
                                          height: "6px",
                                          width: "240px",
                                        }}
                                      ></Progress>
                                    </div>
                                    <div className="px-2 mb-2">
                                      5 votes out of 12
                                    </div>
                                  </div>
                                </Label>
                              </FormGroup>
                            </div>
                            <div className="px-2 col-auto">
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Expand"
                              >
                                <FontAwesomeIcon icon="plus" />{" "}
                              </Button>
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Collapse"
                              >
                                <FontAwesomeIcon icon="minus" />{" "}
                              </Button>
                            </div>
                          </div>

                          {/* Collapse content */}
                          <div className="pl-3">Collapse content</div>
                        </div>
                        <div className="bg-light p-3 mb-1">
                          <div className="d-flex mx-n2 mb-2">
                            <div className="px-2 col">
                              <FormGroup className="mb-2" check>
                                <Label
                                  className="text-reset font-weight-normal fs-14 d-block"
                                  check
                                >
                                  <Input type="radio" name="reviewToKeep" />{" "}
                                  <div className="mb-1">
                                    Do not dispute review.
                                  </div>
                                  <div className="fs-12 ff-alt">
                                    The dispute does not sufficiently meet any
                                    of the first three criteria.
                                  </div>
                                  <div className="d-flex flex-wrap mx-n2 fs-14">
                                    <div className="px-2 mb-2">25%</div>
                                    <div className="px-2 mb-2 align-self-center">
                                      <Progress
                                        color="tertiary"
                                        value="25"
                                        style={{
                                          height: "6px",
                                          width: "240px",
                                        }}
                                      ></Progress>
                                    </div>
                                    <div className="px-2 mb-2">
                                      5 votes out of 12
                                    </div>
                                  </div>
                                </Label>
                              </FormGroup>
                            </div>
                            <div className="px-2 col-auto">
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Expand"
                              >
                                <FontAwesomeIcon icon="plus" />{" "}
                              </Button>
                              <Button
                                color="outline-tertiary rounded-circle"
                                size="sm"
                                title="Collapse"
                              >
                                <FontAwesomeIcon icon="minus" />{" "}
                              </Button>
                            </div>
                          </div>

                          {/* Collapse content */}
                          <div className="pl-3">Collapse content</div>
                        </div>
                      </div>

                      {/* Step 4.b */}
                      <div className="mt-3">
                        <div className="text-primary fs-16 font-weight-bold mb-2">
                          Step 4.b. Should this review be...
                        </div>
                        <div>
                          <div className="bg-light p-3 mb-1">
                            <div className="d-flex mx-n2 mb-2">
                              <div className="px-2 col">
                                <FormGroup className="mb-2" check>
                                  <Label
                                    className="text-reset font-weight-normal fs-14 d-block"
                                    check
                                  >
                                    <Input
                                      type="radio"
                                      name="keepOrRemoveReview"
                                    />{" "}
                                    <div className="mb-1">
                                      ...kept on WikiReviews?
                                    </div>
                                    <div className="d-flex flex-wrap mx-n2 fs-14">
                                      <div className="px-2 mb-2">25%</div>
                                      <div className="px-2 mb-2 align-self-center">
                                        <Progress
                                          color="tertiary"
                                          value="25"
                                          style={{
                                            height: "6px",
                                            width: "240px",
                                          }}
                                        ></Progress>
                                      </div>
                                      <div className="px-2 mb-2">
                                        5 votes out of 12
                                      </div>
                                    </div>
                                  </Label>
                                </FormGroup>
                              </div>
                              <div className="px-2 col-auto">
                                <Button
                                  color="outline-tertiary rounded-circle"
                                  size="sm"
                                  title="Expand"
                                >
                                  <FontAwesomeIcon icon="plus" />{" "}
                                </Button>
                                <Button
                                  color="outline-tertiary rounded-circle"
                                  size="sm"
                                  title="Collapse"
                                >
                                  <FontAwesomeIcon icon="minus" />{" "}
                                </Button>
                              </div>
                            </div>

                            {/* Collapse content */}
                            <div className="pl-3">Collapse content</div>
                          </div>
                          <div className="bg-light p-3 mb-1">
                            <div className="d-flex mx-n2 mb-2">
                              <div className="px-2 col">
                                <FormGroup className="mb-2" check>
                                  <Label
                                    className="text-reset font-weight-normal fs-14 d-block"
                                    check
                                  >
                                    <Input
                                      type="radio"
                                      name="keepOrRemoveReview"
                                    />{" "}
                                    <div className="mb-1">
                                      ...removed from WikiReviews?
                                    </div>
                                    <div className="d-flex flex-wrap mx-n2 fs-14">
                                      <div className="px-2 mb-2">25%</div>
                                      <div className="px-2 mb-2 align-self-center">
                                        <Progress
                                          color="tertiary"
                                          value="25"
                                          style={{
                                            height: "6px",
                                            width: "240px",
                                          }}
                                        ></Progress>
                                      </div>
                                      <div className="px-2 mb-2">
                                        5 votes out of 12
                                      </div>
                                    </div>
                                  </Label>
                                </FormGroup>
                              </div>
                              <div className="px-2 col-auto">
                                <Button
                                  color="outline-tertiary rounded-circle"
                                  size="sm"
                                  title="Expand"
                                >
                                  <FontAwesomeIcon icon="plus" />{" "}
                                </Button>
                                <Button
                                  color="outline-tertiary rounded-circle"
                                  size="sm"
                                  title="Collapse"
                                >
                                  <FontAwesomeIcon icon="minus" />{" "}
                                </Button>
                              </div>
                            </div>

                            {/* Collapse content */}
                            <div className="pl-3">Collapse content</div>
                          </div>
                        </div>
                      </div>

                      {/* Step 4.c */}
                      <div className="mt-3">
                        <div className="text-primary fs-16 font-weight-bold mb-2">
                          Step 4.c. Should this kept review be...
                        </div>
                        <div>
                          <div className="bg-light p-3 mb-1">
                            <div className="d-flex mx-n2 mb-2">
                              <div className="px-2 col">
                                <FormGroup className="mb-2" check>
                                  <Label
                                    className="text-reset font-weight-normal fs-14 d-block"
                                    check
                                  >
                                    <Input
                                      type="radio"
                                      name="reasonToKeepReview"
                                    />{" "}
                                    <div className="mb-1">
                                      ...put in the filtered section?
                                    </div>
                                    <div className="d-flex flex-wrap mx-n2 fs-14">
                                      <div className="px-2 mb-2">25%</div>
                                      <div className="px-2 mb-2 align-self-center">
                                        <Progress
                                          color="tertiary"
                                          value="25"
                                          style={{
                                            height: "6px",
                                            width: "240px",
                                          }}
                                        ></Progress>
                                      </div>
                                      <div className="px-2 mb-2">
                                        5 votes out of 12
                                      </div>
                                    </div>
                                  </Label>
                                </FormGroup>
                              </div>
                              <div className="px-2 col-auto">
                                <Button
                                  color="outline-tertiary rounded-circle"
                                  size="sm"
                                  title="Expand"
                                >
                                  <FontAwesomeIcon icon="plus" />{" "}
                                </Button>
                                <Button
                                  color="outline-tertiary rounded-circle"
                                  size="sm"
                                  title="Collapse"
                                >
                                  <FontAwesomeIcon icon="minus" />{" "}
                                </Button>
                              </div>
                            </div>

                            {/* Collapse content */}
                            <div className="pl-3">Collapse content</div>
                          </div>
                          <div className="bg-light p-3 mb-1">
                            <div className="d-flex mx-n2 mb-2">
                              <div className="px-2 col">
                                <FormGroup className="mb-2" check>
                                  <Label
                                    className="text-reset font-weight-normal fs-14 d-block"
                                    check
                                  >
                                    <Input
                                      type="radio"
                                      name="reasonToKeepReview"
                                    />{" "}
                                    <div className="mb-1">
                                      ...kept where it is? (in the unfiltered
                                      section)
                                    </div>
                                    <div className="d-flex flex-wrap mx-n2 fs-14">
                                      <div className="px-2 mb-2">25%</div>
                                      <div className="px-2 mb-2 align-self-center">
                                        <Progress
                                          color="tertiary"
                                          value="25"
                                          style={{
                                            height: "6px",
                                            width: "240px",
                                          }}
                                        ></Progress>
                                      </div>
                                      <div className="px-2 mb-2">
                                        5 votes out of 12
                                      </div>
                                    </div>
                                  </Label>
                                </FormGroup>
                              </div>
                              <div className="px-2 col-auto">
                                <Button
                                  color="outline-tertiary rounded-circle"
                                  size="sm"
                                  title="Expand"
                                >
                                  <FontAwesomeIcon icon="plus" />{" "}
                                </Button>
                                <Button
                                  color="outline-tertiary rounded-circle"
                                  size="sm"
                                  title="Collapse"
                                >
                                  <FontAwesomeIcon icon="minus" />{" "}
                                </Button>
                              </div>
                            </div>

                            {/* Collapse content */}
                            <div className="pl-3">Collapse content</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Step 5 */}
                    <div className="mt-4">
                      <div className="text-primary fs-16 font-weight-bold mb-2">
                        Step 5. Please, tell us why.
                      </div>
                      <div className="text-dark">
                        It would be really helpful for fellow voters to know the
                        reasoning of your decision. It has to be 140 characters
                        long. Some text like that.
                      </div>
                      <div className="text-right text-primary fs-14">65</div>
                      <Input
                        type="textarea"
                        className="primary"
                        rows="3"
                        defaultValue="The Info is accurate, I've been there today."
                        placeholder="Please elaborate..."
                      />
                    </div>
                  </Col>
                </Row>
              </div>
            </ModalBody>
            <ModalFooter className="bg-white">
              <Button color="primary">Submit</Button>
              <Button color="link" className="text-tertiary font-weight-bold">
                Skip
              </Button>
            </ModalFooter>
          </form>
        </Modal>

        {/* SEE HOW REVIEW DISPUTES WORK */}
        <Modal
          isOpen={this.state.watchHowDisputeReviewWorksModalToggle}
          toggle={() =>
            this.setState({
              watchHowDisputeReviewWorksModalToggle: !this.state.watchHowDisputeReviewWorksModalToggle
            })
          }
        >
          <ModalHeader
            className="text-uppercase px-0"
            toggle={() =>
              this.setState({
                watchHowDisputeReviewWorksModalToggle: !this.state.watchHowDisputeReviewWorksModalToggle,
              })
            }
          >
            SEE HOW REVIEW DISPUTES WORK
        </ModalHeader>
          <ModalBody className="p-0 bg-transparent">
            <div className="text-center">
              <video controls autoPlay width="100%">
                <source src="https://stagingdatawikireviews.s3-us-west-2.amazonaws.com/media/Dispute.mp4" type="video/mp4" />
                Your browser does not support the video tag.
              </video>
            </div>
          </ModalBody>
        </Modal>

      </React.Fragment >
    );
  }
}

const mapState = (state) => {
  return {
    reviews_list: state.user.reviews_list,
    profile_data: state.user.current_user,
    corporate_review_count: state.user.corporate_review_count,
    album_types_list: state.user.album_types_list,
    album_type_data: state.user.album_type_data,
    branch_id: state.user.branch_id,
    circleClick: state.circleClick,
    get_all_disputes_data: state.user.get_all_disputes_data,
    get_review_status: state.user.get_review_status,
    get_dispute_modal_status: state.dispute.get_dispute_modal_status,
  }
};

const mapProps = {
  get_reviews_list,
  edit_reviews_discussion,
  add_reviews_discussion,
  delete_reviews_discussion,
  add_reviews_reply,
  get_album_types_list,
  get_album_type_data,
  delete_selected_gallery_media,
  get_reviews_list_entity_id,
  get_branch_data_by_circle_click,
  get_all_disputed_reviews,
};

export default connect(mapState, mapProps)(Reviews);