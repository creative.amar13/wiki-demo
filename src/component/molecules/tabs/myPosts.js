import React, { Component } from "react";
import {
  FormGroup,
  Label,
  Input,
  Button,
  Media,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row,
  Col,
  Nav,
  NavItem,
  TabContent,
  TabPane,
  NavLink,
  Progress,
  Badge,
  CustomInput,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import {
  get_my_posts_list,
  edit_my_post,
  delete_my_post,
  delete_my_post_comment,
  add_my_post,
  add_my_post_comment,
  add_my_post_reply,
  get_share_with_list,
  get_album_types_list,
  get_album_type_data,
  delete_selected_gallery_media,
  get_branch_copy
} from "../../../actions/user";
import ReactHtmlParser from "react-html-parser";
import { callApi } from "../../../utils/apiCaller";

class MyPosts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: "All",
      branchID: '',

      replyFormVisible: {},
      editFormVisible: {},
      editPost: null,
      editPostType: "",
      editPostError: "",
      myPostsList: {},
      filterType: "all",
      pageNo: 1,
      confirmDeleteModal: false,
      limitedPostsModal: false,
      uploadMediaModal: false,
      selectedUpMediaType: "upload",
      showGalleryType: "images",
      sharePostModal: false,
      dropdownOpen: false,
      sharePostWithType: "Favourites",
      limitedPostsCount: 3,
      deletePostId: 0,
      deletePostType: "",
      addReply: {
        body: "",
      },
      addPost: {
        body: "",
      },
      addComment: {},
      shareWith: {
        type: "",
        selected: "Public",
        list: {},
      },
      uploadMedia: {
        mediaType: "image",
        albumType: "",
        pageNo: 1,
        albumTypesList: [],
        albumTypeData: {},
        selectedMedia: [],
        selectedMediaIds: [],
        embedLinks: {},
        uploadFiles: [],
        uploadedFiles: [],
        progress: 0,
        allBranches: [],
      },
      popupSelectedBranches: [],
      popupUserEntryBranchIDs: [],
      popupNewBranches: [],
      userEntryBranchIDs: [],
      selectedBranches: [],
      branchText: "",
      getallBranchCopy: {},
      branches: [],
      newBranches: [],
      pendingBranches: [],
      country: "",
      countryList: [],
      withoutfilterBranches: [],
      countryList: [],
      cState: {},
      countryState: [],
      countryName: "",
      countryListingName: "",
      provinceName: "",
      branchesData: [],
      rowState: [],
      copyPostToBranches: {},
      checkAll: false,
      popupCheckAll: false,
    };
    this.toggle = this.toggle.bind(this);
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  toggle() {
    this.setState((prevState) => ({
      dropdownOpen: !prevState.dropdownOpen,
    }));
  }

  onMouseEnter() {
    this.setState({ dropdownOpen: true });
  }

  onMouseLeave() {
    this.setState({ dropdownOpen: false });
  }
  handleOnFileUploadChange = (event) => {
    let uploadFiles = event.target.files;

    let showFiles = [];
    // for (const key of Object.keys(uploadFiles)) {
    //   showFiles.push({ id: "", url: URL.createObjectURL(uploadFiles[key]) });
    // }
    for (const key of Object.keys(uploadFiles)) {
      let itemType = uploadFiles[key].type.split("/");
      let extName = itemType[0];
      showFiles.push({
        id: "",
        url: extName === "image" ?
          URL.createObjectURL(uploadFiles[key]) :
          require("../../../assets/images/blank.png")
      });
    }
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        uploadFiles: showFiles,
        progress: 0,
      },
    });
    let progressPart = 100 / showFiles.length;
    let progress = 0;
    for (const key of Object.keys(uploadFiles)) {
      let data = new FormData();
      data.append("file", uploadFiles[key]);

      callApi(
        `/upload/multiuploader/?album=feeds&instance=post&image=undefined&doc=undefined`,
        "POST",
        data,
        true
      ).then((response) => {
        this.handleOnClickSelectGalleryMedia(response);
        if (showFiles.length === 1 || key === showFiles.length - 1) {
          progress = 100;
        } else {
          progress = progress + progressPart;
        }
        showFiles[key].id = response.id;
        this.setState({
          ...this.state,
          uploadMedia: {
            ...this.state.uploadMedia,
            progress: progress,
            uploadedFiles: [...this.state.uploadMedia.uploadedFiles, response],
            uploadedFiles: showFiles,
          },
        });
      });
    }
  };
  handleOnClickRemoveSelectedMedia = (id) => () => {
    let removeMediaIds = [];
    removeMediaIds.push(id);
    this.props.delete_selected_gallery_media(removeMediaIds);
    let uploadFiles = this.state.uploadMedia.uploadFiles.filter(
      (file) => file.id !== id
    );
    let uploadedFiles = this.state.uploadMedia.uploadedFiles.filter(
      (file) => file.id !== id
    );
    let selectedMediaIds = this.state.uploadMedia.selectedMediaIds.filter(
      (item) => item !== id
    );
    let selectedMedia = this.state.uploadMedia.selectedMedia.filter(
      (file) => file.id !== id
    );
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: selectedMedia,
        selectedMediaIds: selectedMediaIds,
        uploadedFiles: uploadedFiles,
        uploadFiles: uploadFiles,
      },
    });
  };
  handleOnClickRemoveSelectedGalleryMedia = (media) => {
    let removeMediaIds = [];
    removeMediaIds.push(media.id);
    let index = this.state.uploadMedia.selectedMediaIds.indexOf(media.id);
    let newSelectedMediaIds;
    let newSelectedMedia;
    if (index !== -1) {
      newSelectedMediaIds = this.state.uploadMedia.selectedMediaIds.filter(
        (item) => item !== media.id
      );
      newSelectedMedia = this.state.uploadMedia.selectedMedia.filter(
        (item) => item.id !== media.id
      );
      this.props.delete_selected_gallery_media(removeMediaIds);
      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          selectedMedia: newSelectedMedia,
          selectedMediaIds: newSelectedMediaIds,
        },
      });
    }
  };
  handleOnClickSelectGalleryMedia = (media) => {
    let index = this.state.uploadMedia.selectedMediaIds.indexOf(media.id);
    let newSelectedMediaIds;
    let newSelectedMedia;
    if (index !== -1) {
      newSelectedMediaIds = this.state.uploadMedia.selectedMediaIds.filter(
        (item) => item !== media.id
      );
      newSelectedMedia = this.state.uploadMedia.selectedMedia.filter(
        (item) => item.id !== media.id
      );
    } else {
      newSelectedMediaIds = [
        ...this.state.uploadMedia.selectedMediaIds,
        media.id,
      ];
      newSelectedMedia = [...this.state.uploadMedia.selectedMedia, media];
    }
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: newSelectedMedia,
        selectedMediaIds: newSelectedMediaIds,
      },
    });
  };
  handleOnClickGalleryType = (type) => {
    let mediaType = "";
    if (type === "images") {
      mediaType = "image";
    } else if (type === "videos") {
      mediaType = "video";
    }
    this.props.get_album_type_data(mediaType, "", 1);
    this.setState({
      ...this.state,
      selectedUpMediaType: "gallery",
      showGalleryType: type,
      uploadMedia: {
        ...this.state.uploadMedia,
        mediaType: mediaType,
        albumType: "",
        pageNo: 1,
        //selectedMedia: [],
        //selectedMediaIds: [],
      },
    });
  };
  handleOnClickAlbumTypeChange = (e) => {
    this.props.get_album_type_data("image", e.target.value, 1);
    this.setState({
      ...this.state,
      selectedUpMediaType: "gallery",
      showGalleryType: "images",
      uploadMedia: {
        ...this.state.uploadMedia,
        mediaType: "image",
        albumType: e.target.value,
        pageNo: 1,
        //selectedMedia: [],
        //selectedMediaIds: [],
      },
    });
  };
  handleOnClickSelectedUploadMediaType = (type) => {
    if (type === "gallery") {
      this.props.get_album_type_data("image", "", 1);
      this.setState({
        ...this.state,
        selectedUpMediaType: type,
        showGalleryType: "images",
        uploadMedia: {
          ...this.state.uploadMedia,
          mediaType: "image",
          albumType: "",
          pageNo: 1,
          //selectedMedia: [],
          //selectedMediaIds: [],
          embedLinks: {},
          progress: 0,
        },
      });
    } else if (type === "upload") {
      this.setState({
        ...this.state,
        selectedUpMediaType: type,
        uploadMedia: {
          ...this.state.uploadMedia,
          //selectedMedia: [],
          //selectedMediaIds: [],
          embedLinks: {},
          progress: 0,
        },
      });
    } else if (type === "embed") {
      let embedLinks = {
        0: "",
        1: "",
      };
      this.setState({
        ...this.state,
        selectedUpMediaType: type,
        uploadMedia: {
          ...this.state.uploadMedia,
          selectedMedia: [],
          selectedMediaIds: [],
          embedLinks: embedLinks,
        },
      });
    }
  };
  embedLinkOnChange = (id) => (e) => {
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        embedLinks: {
          ...this.state.uploadMedia.embedLinks,
          [id]: e.target.value,
        },
      },
    });
  };
  mapEmbedLinks = () => {
    let embedKeys = Object.keys(this.state.uploadMedia.embedLinks);
    return embedKeys.map((emKey) => {
      return (
        <div className="d-flex mx-n2 mb-2" key={emKey}>
          <div className="flex-fill px-2">
            <Input
              type="url"
              bsSize="sm"
              className="bg-white"
              value={this.state.uploadMedia.embedLinks[emKey]}
              onChange={this.embedLinkOnChange(emKey)}
              placeholder="Embeded link"
            />
          </div>
          <div className="px-2">
            <Button
              title="Remove"
              color="danger"
              size="sm"
              hidden={emKey <= 1 ? true : false}
              onClick={() => {
                this.deleteEmbedLinkRow(emKey);
              }}
            >
              <FontAwesomeIcon icon="minus" />
            </Button>
          </div>
        </div>
      );
    });
  };
  deleteEmbedLinkRow = (emKey) => {
    let embedLinks = this.state.uploadMedia.embedLinks;
    if (Object.keys(embedLinks).indexOf(emKey) !== -1) {
      delete embedLinks[emKey];
      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          embedLinks: embedLinks,
        },
      });
    }
  };
  addEmbedlinkRow = () => {
    let newEmbedLinks = {};
    for (let i = 2; i < 5; i += 1) {
      if (
        Object.keys(this.state.uploadMedia.embedLinks).indexOf(i.toString()) ===
        -1
      ) {
        newEmbedLinks[i] = "";
        break;
      }
    }
    if (Object.keys(newEmbedLinks).length > 0) {
      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          embedLinks: {
            ...this.state.uploadMedia.embedLinks,
            ...newEmbedLinks,
          },
        },
      });
    }
  };
  handleOnClickUploadMedia = () => {
    this.props.get_album_types_list();
    this.setState({
      ...this.state,
      uploadMedia: {
        ...this.state.uploadMedia,
        mediaType: "image",
        albumType: "",
        pageNo: 1,
        albumTypeData: {},
        embedLinks: {},
        progress: 0,
        uploadFiles: [],
        uploadedFiles: [],
      },
      uploadMediaModal: true,
      selectedUpMediaType: "upload",
      showGalleryType: "images",
    });
  };
  truncate = (filenameString) => {
    // let split = filenameString.split(".");
    let filename = filenameString.substr(0, filenameString.lastIndexOf("."));
    let extension = filenameString.substr(
      filenameString.lastIndexOf("."),
      filenameString.length - 1
    );
    let partial = filename.substring(filename.length - 3, filename.length);
    filename = filename.substring(0, 15);
    return filename + "..." + partial + extension;
  };

  componentWillReceiveProps(nextProps) {

    // get country state and branches

    if (
      nextProps.getbranch_copy &&
      Object.keys(nextProps.getbranch_copy).length > 0
    ) {
      if (this.state.country == "") {

        nextProps.getbranch_copy.result.forEach((itemData, key) => {
          if (itemData.id === this.props?.corporateId) {
            nextProps.getbranch_copy.result.splice(key, 1);
          }
          itemData["isChecked"] = false;
        }
        );
        this.setState({
          getallBranchCopy: nextProps.getbranch_copy,
          branches: nextProps.getbranch_copy.result,
          newBranches: nextProps.getbranch_copy.result,
          popupNewBranches: nextProps.getbranch_copy.result,
          pendingBranches: nextProps.getbranch_copy.result,
          popupPendingBranches: nextProps.getbranch_copy.result,
          withoutfilterBranches: nextProps.getbranch_copy.result,
          countryList: nextProps.getbranch_copy.country_list,
          cState: nextProps.getbranch_copy.c_states,
          countryState: nextProps.getbranch_copy.country_state,
        });
        for (var i = 0; i < nextProps && nextProps.getbranch_copy && nextProps.getbranch_copy.result.length; i++) {
          this.state.rowState[i] = false;
        }
      }
    }
    //get my posts list
    if (
      nextProps.my_posts_list &&
      Object.keys(nextProps.my_posts_list).length > 0
    ) {
      let addComment = {};
      if (
        nextProps.my_posts_list &&
        nextProps.my_posts_list.results &&
        Array.isArray(nextProps.my_posts_list.results) &&
        nextProps.my_posts_list.results.length > 0
      ) {
        nextProps.my_posts_list.results.forEach((post) => {
          addComment[post.id] = "";
        });
      }
      this.setState({
        myPostsList: nextProps.my_posts_list,
        addComment: {
          ...addComment,
        },
      });
    }

    if (nextProps?.all_branches?.results?.result &&
      nextProps?.all_branches?.results?.result.length > 0) {
      this.setState({
        allBranches: nextProps?.all_branches?.results?.result
      });
    }
    if (nextProps?.corporateId) {
      this.setState({
        branchID: nextProps?.corporateId,
      });
    }

    if (
      nextProps.share_with_list &&
      Object.keys(nextProps.share_with_list).length > 0
    ) {
      this.setState({
        ...this.state,
        shareWith: {
          ...this.state.shareWith,
          list: nextProps.share_with_list,
        },
      });
    }

    if (nextProps.album_types_list && nextProps.album_types_list.length > 0) {
      this.setState({
        ...this.state,
        uploadMedia: {
          ...this.state.uploadMedia,
          albumTypesList: nextProps.album_types_list,
        },
        //uploadMediaModal: true,
        selectedUpMediaType: "upload",
        showGalleryType: "images",
      });
    }

    if (
      nextProps.album_type_data &&
      Object.keys(nextProps.album_type_data).length > 0
    ) {
      this.setState({
        ...this.state,
        //selectedUpMediaType: "gallery",
        uploadMedia: {
          ...this.state.uploadMedia,
          albumTypeData: nextProps.album_type_data,
        },
      });
    }
  }

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):

    if (this.props.my_posts_list !== prevProps.my_posts_list) {

      if (
        this.props.my_posts_list &&
        Object.keys(this.props.my_posts_list).length > 0
      ) {
        if (
          this.props.my_posts_list &&
          Object.keys(this.props.my_posts_list).length > 0
        ) {
          let addComment = {};
          if (
            this.props.my_posts_list &&
            this.props.my_posts_list.results &&
            Array.isArray(this.props.my_posts_list.results) &&
            this.props.my_posts_list.results.length > 0
          ) {
            this.props.my_posts_list.results.forEach((post) => {
              addComment[post.id] = "";
            });
          }
          this.setState({
            myPostsList: this.props.my_posts_list,
            addComment: {
              ...addComment,
            },
          });
        }
      }

    }
  }
  componentDidMount() {

    this.props.get_my_posts_list(this.state.filterType, this.state.pageNo, this.props.corporateId);

  }

  componentDidUpdate(prevProps) {
    if (prevProps.corporateId !== this.props.corporateId) {
      this.props.get_my_posts_list(this.state.filterType, this.state.pageNo, this.props.corporateId);
    }
  }

  handleOnClick = (selected, filterType, pageNo) => {
    const { branchID } = this.state;
    this.props.get_my_posts_list(filterType, pageNo, branchID);
    this.setState({ selected, filterType, pageNo });
  };
  handleOnClickShareWith = (selected, filterType) => {
    let sharePostWithType = "";
    if (selected === "Custom" || selected === "Public") {
      sharePostWithType = "Favourites";
    } else {
      sharePostWithType = selected;
    }
    if (filterType !== "") {
      //type=recommends&userentry=2207130&biz_owner=true
      this.props.get_share_with_list(
        filterType,
        this.props.corporate_id.id,
        this.props.profile_data.business_owner
      );
      this.setState({
        ...this.state,
        sharePostModal: true,
        shareWith: {
          ...this.state.shareWith,
          type: filterType,
          selected: selected,
        },
        sharePostWithType: sharePostWithType,
      });
    } else {
      this.setState({
        ...this.state,
        sharePostModal: false,
        shareWith: {
          ...this.state.shareWith,
          type: filterType,
          selected: selected,
          list: {},
        },
        sharePostWithType: sharePostWithType,
      });
    }
  };
  confirmDeleteModalToggle = () => {
    if (this.state.deletePostId) {
      this.setState({ deletePostId: 0, deletePostType: "" });
    }
    this.setState({ confirmDeleteModal: !this.state.confirmDeleteModal });
  };
  limitedPostsModalToggle = () => {
    this.setState({ limitedPostsModal: !this.state.limitedPostsModal });
  };

  uploadMediaModalToggle = () => {
    this.setState({
      ...this.state,
      uploadMediaModal: !this.state.uploadMediaModal,
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: [],
        selectedMediaIds: [],
        mediaType: "image",
        albumType: "",
        pageNo: 1,
        albumTypeData: {},
        //albumTypesList: [],
        embedLinks: {},
        uploadedFiles: [],
        uploadFiles: [],
      },
      selectedUpMediaType: "upload",
      showGalleryType: "images",
    });
  };

  sharePostModalToggle = () => {
    this.setState({ sharePostModal: !this.state.sharePostModal });
  };

  toggleReplyForm = (id, level) => {
    id = level + id;
    this.setState({
      ...this.state,
      replyFormVisible: {
        [id]: !this.state.replyFormVisible[id],
      },
      editFormVisible: {},
      addReply: {
        body: "",
      },
    });
  };
  toggleEditForm = (id, level, type = "post", body = "") => {
    let post;
    let postType;
    let selectedMedia = [];
    let selectedMediaIds = [];
    if (type === "post") {
      if (this.state.editFormVisible[level + id]) {
        post = null;
        postType = "";
        selectedMedia = [];
        selectedMediaIds = [];
      } else {
        post = this.state.myPostsList.results.filter(
          (post) => id === post.id
        )[0];
        postType = "post";
        selectedMedia = post.attachments;
        selectedMediaIds =
          post.attachments.length > 0
            ? selectedMedia.map((item) => item.id)
            : [];
      }
    } else if (type === "comment") {
      if (this.state.editFormVisible[level + id]) {
        post = null;
        postType = "";
      } else {
        postType = "comment";
        post = {
          id: id,
          msg_id: id,
          editpost: true,
          body: "<p>" + body + "</p>",
          body_text: body,
          attachments: [],
          messagemedia_set: [],
          exclude_list: [],
        };
      }
    }
    id = level + id;
    this.setState({
      ...this.state,
      editPost: post,
      editPostType: postType,
      editFormVisible: {
        [id]: !this.state.editFormVisible[id],
      },
      replyFormVisible: {},
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: selectedMedia,
        selectedMediaIds: selectedMediaIds,
      },
      editPostError: "",
    });
  };
  handleCreatePostOnChange = (e) => {
    this.setState({
      ...this.state,
      addPost: {
        ...this.state.addPost,
        body: "<p>" + e.target.value + "</p>",
      },
    });
  };
  handleOnClearCreate = async (e) => {
    // e.preventDefault();
    await this.state.newBranches.map((item, index) => {
      item.isChecked = false;
    })
    this.setState({
      ...this.state,
      addPost: {
        ...this.state.addPost,
        body: "<p>" + "" + "</p>",
      },
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: [],
        selectedMediaIds: [],
        embedLinks: {},
      },
      selectedBranches: [],
      userEntryBranchIDs: [],
      checkAll: false
    });
  };
  insertEmbedLinks = () => {
    //let post = this.state.addPost;
    let embedValues = Object.values(this.state.uploadMedia.embedLinks).filter(
      (item) => item !== ""
    );
    //if (post.body && post.body.replace(/(<([^>]+)>)/gi, "") !== "") {
    // if (embedValues.length > 0) {
    //   embedValues = embedValues.map((item) => "<p>" + item + "</p>");
    //   post.body = post.body + embedValues.join("");
    //   post.body_preview = post.body;
    // }
    // this.setState({
    //   ...this.state,
    //   addPost: post
    // });
    //} else {
    if (embedValues.length > 0) {
      embedValues = embedValues.map((item) => "<p>" + item + "</p>");
      let newPost = {
        video: embedValues.join("\n"),
        exclude_list: [],
        question_rating_category: [],
        messagemedia_set: [],
      };
      if (this.state.myPostsList.count < this.state.limitedPostsCount) {
        this.props.add_my_post(
          newPost,
          this.state.filterType,
          this.state.pageNo
        );
      }
      this.setState({
        ...this.state,
        limitedPostsModal:
          this.state.myPostsList.count < this.state.limitedPostsCount
            ? false
            : true,
        uploadMedia: {
          ...this.state.uploadMedia,
          selectedMedia: [],
          selectedMediaIds: [],
          embedLinks: {},
        },
        uploadMediaModal: false,
      });
    }
    //}
  };
  handleOnSubmitCreatePost = (e) => {
    e.preventDefault();
    const { userEntryBranchIDs, branchID } = this.state
    let allBranchPostIDs = [];
    allBranchPostIDs = userEntryBranchIDs;
    allBranchPostIDs.push(branchID)
    const otherDetails = {
      exclude_list: [],
      tag_list: [],
      listing_tag_list: [],
      question_rating_category: [],
      messagemedia_set: this.state.uploadMedia?.selectedMediaIds,
      user_entry_ids: this.state.userEntryBranchIDs,
    };
    let post = this.state.addPost;
    if (post.body && post.body.replace(/(<([^>]+)>)/gi, "") !== "") {
      post = {
        ...this.state.addPost,
        ...otherDetails,
        body_preview: this.state.addPost.body,

      };
      if (this.state.myPostsList.count < this.state.limitedPostsCount) {
        this.props.add_my_post(post, this.state.filterType, this.state.pageNo, branchID);
        this.setState({
          ...this.state,
          addPost: {
            body: "",
          },
          selectedBranches: [],
          userEntryBranchIDs: [],
          uploadMedia: {
            ...this.state.uploadMedia,
            selectedMedia: [],
            selectedMediaIds: [],
          },
        });
        this.handleOnClearCreate();
      } else {
        this.handleOnClearCreate();
        this.setState({
          ...this.state,
          limitedPostsModal: true,
          addPost: {
            body: "",
          },
          userEntryBranchIDs: [],
          selectedBranches: [],
          uploadMedia: {
            ...this.state.uploadMedia,
            selectedMedia: [],
            selectedMediaIds: [],
          },
        });
      }
    }
  };
  handleOnReplyChange = (e) => {
    this.setState({
      ...this.state,
      addReply: {
        ...this.state.addReply,
        body: e.target.value,
      },
    });
  };
  handleOnReplySubmit = (id, level) => (e) => {
    e.preventDefault();
    const otherDetails = {
      answer_id: id,
      type: "feed",
    };

    const reply = { ...otherDetails, body: this.state.addReply.body };
    if (reply.body && reply.body.replace(/(<([^>]+)>)/gi, "") !== "") {
      this.props.add_my_post_reply(
        reply,
        this.state.filterType,
        this.state.pageNo,
        this.state.branchID
      );
      this.setState({
        ...this.state,
        replyFormVisible: {
          [level + id]: !this.state.replyFormVisible[level + id],
        },
        editFormVisible: {},
        addReply: {
          body: "",
        },
      });
    }
  };
  handleOnCommentChange = (id) => (e) => {
    this.setState({
      ...this.state,
      addComment: {
        ...this.state.addComment,
        [id]: e.target.value,
      },
    });
  };
  handleOnCommentSubmit = (id) => (e) => {
    e.preventDefault();
    const otherDetails = {
      owner_premium: "False",
      [id]: this.state.addComment[id],
      exclude_list: [],
      tag_list: [],
      listing_tag_list: [],
      question_rating_category: [],
      messagemedia_set: [],
    };

    let comment = this.state.addComment;

    let limitedPostsModalValue = false;
    if (comment[id] !== "") {
      comment = { body: this.state.addComment[id], ...otherDetails };
      if (this.state.myPostsList.count < this.state.limitedPostsCount) {
        this.props.add_my_post_comment(
          id,
          comment,
          this.state.filterType,
          this.state.pageNo,
          this.state.branchID
        );
      } else {
        limitedPostsModalValue = true;
      }
      this.setState({
        ...this.state,
        limitedPostsModal: limitedPostsModalValue,
        addComment: {
          ...this.state.addComment,
          [id]: "",
        },
      });
    }
  };
  handleEditPostOnChange = (e) => {
    this.setState({
      ...this.state,
      editPost: {
        ...this.state.editPost,
        body: "<p>" + e.target.value + "</p>",
      },
    });
  };
  handleOnClearEdit = (e) => {
    e.preventDefault();
    this.setState({
      ...this.state,
      editPost: {
        ...this.state.editPost,
        body: "<p>" + "" + "</p>",
      },
      uploadMedia: {
        ...this.state.uploadMedia,
        selectedMedia: [],
        selectedMediaIds: [],
      },
      editPostError: "",
    });
  };
  handleOnSubmitEditPost = (e) => {
    e.preventDefault();
    let post = this.state.editPost;
    if (post.body === "<p></p>") {
      this.setState({
        editPostError:
          "Please provide message content or photos/video to send this message",
      });
    } else if (post.body && post.body.replace(/(<([^>]+)>)/gi, "") !== "") {
      post = {
        ...this.state.editPost,
        editpost: true,
        body_preview: this.state.editPost.body,
        messagemedia_set:
          this.state.uploadMedia.selectedMediaIds.length > 0 &&
            this.state.editPost
            ? this.state.uploadMedia.selectedMediaIds
            : this.state.editPost.messagemedia_set,
        attachments:
          this.state.uploadMedia.selectedMedia.length > 0 && this.state.editPost
            ? this.state.uploadMedia.selectedMedia
            : this.state.editPost.attachments,
      };

      this.props.edit_my_post(
        post,
        this.state.editPostType,
        this.state.filterType,
        this.state.pageNo
      );
      let level;
      if (this.state.editPostType === "post") {
        level = "L1";
      } else if (this.state.editPostType === "comment") {
        level = "L2";
      }
      this.setState({
        ...this.state,
        editPost: null,
        editPostType: "",
        editFormVisible: {
          [level + post.id]: !this.state.editFormVisible[level + post.id],
        },
        uploadMedia: {
          ...this.state.uploadMedia,
          selectedMedia: [],
          selectedMediaIds: [],
        },
      });
    }
  };
  handleOnDeleteConfirmation = () => {
    if (this.state.deletePostId && this.state.deletePostType === "post") {
      this.props.delete_my_post(
        this.state.deletePostId,
        this.state.filterType,
        this.state.pageNo,
        this.state.branchID
      );
    } else if (
      this.state.deletePostId &&
      (this.state.deletePostType === "comment" ||
        this.state.deletePostType === "reply")
    ) {
      this.props.delete_my_post_comment(
        this.state.deletePostId,
        this.props.corporate_id.id,
        this.state.filterType,
        this.state.pageNo
      );
    }
    this.setState({
      confirmDeleteModal: !this.state.confirmDeleteModal,
      deletePostId: 0,
      deletePostType: "",
    });
  };

  handleSelectFromDropdown = async (item, type) => {
    let newItems = [];
    let newItemsID = [];
    let index;

    index = this.state.selectedBranches &&
      this.state.selectedBranches.length > 0 &&
      this.state.selectedBranches.map((branch) => branch.admin_entries_id)
        .includes(item.admin_entries_id);

    if (index) {
      newItems = this.state.selectedBranches.filter(
        (branch) => branch.admin_entries_id !== item.admin_entries_id
      );
    } else {
      newItems = [...this.state.selectedBranches, item];
    }

    if (index) {
      newItemsID = this.state.userEntryBranchIDs.filter(
        (branch) => branch !== item.id
      );
    } else {
      newItemsID = [...this.state.userEntryBranchIDs, item.id];
    }


    await this.setState({
      ...this.state,
      selectedBranches: newItems,
      userEntryBranchIDs: newItemsID,
    });

  };

  removeFromSelected = async (item, type) => {
    let newItems = [];
    let newItemsID = [];

    newItems = this.state.selectedBranches.filter(
      (branch) => branch.id !== item.id
    );
    newItemsID = this.state.userEntryBranchIDs.filter(
      (branch) => branch !== item.id
    );
    await this.setState({
      ...this.state,
      selectedBranches: newItems,
      userEntryBranchIDs: newItemsID
    });

  };

  handleOnChangeBranches = (e) => {
    let allBranches = this.props.all_branches?.results?.result;
    let value = (e.target.value).toLowerCase();
    if (value) {
      allBranches = this.props.all_branches?.results?.result.filter((branch) => {
        let branchString = (`${branch.name}`).toLowerCase();
        if (branchString.includes(value)) {
          return true;
        } else {
          return false;
        }
      });
      this.setState({
        ...this.state,
        allBranches: allBranches,
        branchText: e.target.value
      });
    } else {
      this.setState({
        ...this.state,
        allBranches: [...allBranches],
        branchText: e.target.value
      });
    }
  };

  handleChange = async (event, item) => {

    const { cState, newBranches } = this.state;
    let branchId = 0;
    let stateName = "";
    if (event.target.name == "select_country") {

      let country = event.target.value;
      var item;
      if (cState.hasOwnProperty(country)) {
        item = cState[country];
      }
      let newArray = [];
      for (var i = 0; i < this.state.branches.length; i++) {
        if (country == this.state.branches[i].country) {
          newArray.push(this.state.branches[i]);
        }
      }
      this.setState(
        {
          country: event.target.value,
          newBranches: newArray,
          pendingBranches: newArray,
          countryState: item,
          stateName: ''
        },
        () => this.props.get_branch_copy(country)
      );
    }

    if (event.target.name == "select_state") {
      let state = event.target.value;
      let newArray = [];
      if (this.state.branches) {
        for (var i = 0; i < this.state.branches.length; i++) {
          if (
            this.state.country == this.state.branches[i].country &&
            state == this.state.branches[i].state
          ) {
            newArray.push(this.state.branches[i]);
          }
        }
        this.setState(
          { state: event.target.value, isdisabled: false, newBranches: newArray },
          () => this.props.get_branch_copy(this.state.country)
        );
      }
    }

    if (event.target.name == "select_branch" && event.target.type === "checkbox") {
      let { pendingBranches } = this.state;
      let count = pendingBranches.length;
      let newItems = [];
      let newItemsID = [];
      let index;
      index = this.state.selectedBranches &&
        this.state.selectedBranches.length > 0 &&
        this.state.selectedBranches.map((branch) => branch.address_id)
          .includes(item.address_id);

      if (index) {
        item.isChecked = false;
        newItems = this.state.selectedBranches.filter(
          (branch) => {
            return (branch.address_id !== item.address_id)
          }
        );
        this.setState({ checkAll: false });
      } else {
        item.isChecked = true;
        newItems = [...this.state.selectedBranches, item];
        if (count == newItems.length) {
          this.setState({ checkAll: true })
        }
      }
      if (index) {
        newItemsID = this.state.userEntryBranchIDs.filter(
          (branch) => branch !== item.id
        );
      } else {
        newItemsID = [...this.state.userEntryBranchIDs, item.id];
      }


      await this.setState({
        selectedBranches: newItems,
        userEntryBranchIDs: newItemsID,
      });

    }

    if (event?.target?.name == "select_state") {
      let stateName = event.target?.value;
      this.setState({ stateName: stateName });
    }

    this.setState({ [event.target?.name]: event.target?.value });
  };


  handlePopupChange = async (event, item) => {
    const { cState, popupNewBranches, popupPendingBranches } = this.state;
    let branchId = 0;
    let stateName = "";
    if (event.target.name == "popup_select_country") {

      let country = event.target.value;
      var item;
      if (cState.hasOwnProperty(country)) {
        item = cState[country];
      }
      let newArray = [];
      for (var i = 0; i < this.state.branches.length; i++) {
        if (country == this.state.branches[i].country) {
          newArray.push(this.state.branches[i]);
        }
      }
      this.setState(
        {
          country: event.target.value,
          popupNewBranches: newArray,
          popupPendingBranches: newArray,
          countryState: item,
          stateName: ''
        },
        () => this.props.get_branch_copy(country)
      );
    }

    if (event.target.name == "popup_select_state") {
      let state = event.target.value;
      let newArray = [];
      if (this.state.branches) {
        for (var i = 0; i < this.state.branches.length; i++) {
          if (
            this.state.country == this.state.branches[i].country &&
            state == this.state.branches[i].state
          ) {
            newArray.push(this.state.branches[i]);
          }
        }
        this.setState(
          { state: event.target.value, isdisabled: false, popupNewBranches: newArray },
          () => this.props.get_branch_copy(this.state.country)
        );
      }
    }

    if (event.target.name == "popup_select_branch" && event.target.type === "checkbox") {
      let count = popupPendingBranches.length;
      let newItems = [];
      let newItemsID = [];
      let index;
      index = this.state.popupSelectedBranches &&
        this.state.popupSelectedBranches.length > 0 &&
        this.state.popupSelectedBranches.map((branch) => branch.address_id)
          .includes(item.address_id);

      if (index) {
        item.isChecked = false;
        newItems = this.state.popupSelectedBranches.filter(
          (branch) => branch.address_id !== item.address_id
        );
        this.setState({ popupCheckAll: false });
      } else {
        item.isChecked = true;
        newItems = [...this.state.popupSelectedBranches, item];
        if (count == newItems.length) {
          this.setState({ popupCheckAll: true })
        }
      }
      if (index) {
        newItemsID = this.state.popupUserEntryBranchIDs.filter(
          (branch) => branch !== item.id
        );
      } else {
        newItemsID = [...this.state.popupUserEntryBranchIDs, item.id];
      }


      await this.setState({
        popupSelectedBranches: newItems,
        popupUserEntryBranchIDs: newItemsID,
      });

    }

    if (event?.target?.name == "select_state") {
      let stateName = event.target?.value;
      this.setState({ stateName: stateName });
    }

    this.setState({ [event.target?.name]: event.target?.value });
  };

  handleCopyPostToBranches = (data) => {
    const { copyPostToBranches } = this.state;
    let copyPostToBranchesData = {}
    let postMedia = []
    data.attachments && Array.isArray(data.attachments) && data.attachments.length > 0 && data.attachments.map((media) => {
      postMedia.push(media.id);
    })
    copyPostToBranchesData.body = data.body;
    copyPostToBranchesData.exclude_list = [];
    copyPostToBranchesData.tag_list = [];
    copyPostToBranchesData.listing_tag_list = [];
    copyPostToBranchesData.question_rating_category = [];
    copyPostToBranchesData.messagemedia_set = postMedia;
    copyPostToBranchesData.body_preview = data.body_preview;
    this.setState({
      copyPostToBranches: copyPostToBranchesData
    });
  }

  handleSubmitCopyPost = () => {
    const { copyPostToBranches, popupUserEntryBranchIDs } = this.state;
    copyPostToBranches.user_entry_ids = popupUserEntryBranchIDs;
    this.props.add_my_post(this.state.copyPostToBranches, this.state.filterType, this.state.pageNo, this.state.branchID);
    this.toggleModal();
  }

  toggleModal = async () => {
    await this.state.newBranches.map((item, index) => {
      item.isChecked = false;
    })
    await this.state.newBranches.map((item, index) => {
      item.isChecked = false;
    })
    this.setState({
      copyPostToBranchesModal: !this.state.copyPostToBranchesModal,
      popupSelectedBranches: [],
      popupUserEntryBranchIDs: [],
      selectedBranches: [],
      popupCheckAll: false,
      copyPostToBranches: {},
      checkAll: false,
    })
  }

  // Branceh All checked
  handleAllCheckedBranchnew = (event, checkType) => {
    let newBranches = this.state.newBranches
    let { selectedBranches } = this.state;
    let self = this;
    newBranches.forEach((branch, index) => {
      branch.isChecked = event != null ? event.target.checked : checkType;
      if (event?.target?.checked == true) {
        let el = this.state.selectedBranches && this.state.selectedBranches.find((itm) => itm === branch.id);
        if (el)
          this.state.selectedBranches.splice(
            this.state.selectedBranches.indexOf(el),
            1
          );
        selectedBranches.push(branch.id)
        this.setState({ checkAll: true })
      } else {
        let el = selectedBranches && this.state.selectedBranches.find((itm) => itm === branch.id);
        if (el)
          this.state.selectedBranches.splice(
            this.state.selectedBranches.indexOf(el),
            1
          );
        this.setState({ checkAll: false })
      }
    }
    )

    this.setState(newBranches)
  }

  handleAllCheckedBranch = async (event) => {
    let allCheckBrnaches = this.state.newBranches
    let allCheckBrnachesID = []
    let SelectedValue = event.target.checked;

    if (SelectedValue) {
      this.setState({
        checkAll: true
      });
      await allCheckBrnaches.map((element, index) => {
        if (element.id !== this.state.branchID) {
          element.isChecked = true
          allCheckBrnachesID.push(element.id)
        }
      });
      await this.setState({
        selectedBranches: allCheckBrnaches,
        userEntryBranchIDs: allCheckBrnachesID,
      });
    } else if (SelectedValue === false) {
      this.setState({
        checkAll: false
      });
      await allCheckBrnaches.map((element, index) => {
        if (element.id !== this.state.branchID) { element.isChecked = false }
      });
      await this.setState({
        selectedBranches: [],
        userEntryBranchIDs: [],
      });
    }

  };

  handlePopupAllCheckedBranch = async (event) => {
    let popupAllCheckBrnaches = this.state.popupNewBranches
    let popupAllCheckBrnachesID = []
    let SelectedValue = event.target.checked;
    if (SelectedValue) {
      this.setState({
        popupCheckAll: true
      });
      await popupAllCheckBrnaches.map((element, index) => {
        if (element.id !== this.state.branchID) {
          element.isChecked = true
          popupAllCheckBrnachesID.push(element.id)
        }
      });
      await this.setState({
        popupSelectedBranches: popupAllCheckBrnaches,
        popupUserEntryBranchIDs: popupAllCheckBrnachesID,
      });
    } else if (SelectedValue === false) {
      this.setState({
        popupCheckAll: false
      });
      await popupAllCheckBrnaches.map((element, index) => {
        if (element.id !== this.state.branchID) { element.isChecked = false }
      });
      await this.setState({
        popupSelectedBranches: [],
        popupUserEntryBranchIDs: [],
      });
    }
  };
  render() {
    const {
      selected,
      myPostsList,
      replyFormVisible,
      editFormVisible,
      editPost,
      allBranches,
      country,
      countryList,
      countryState,
      stateName,
      getBranchData,
      newBranches,
      popupNewBranches,
      pendingBranches,
    } = this.state;
    return (
      <React.Fragment>
        <div className="mb-3">
          <UncontrolledDropdown
            onMouseOver={this.onMouseEnter}
            onMouseLeave={this.onMouseLeave}
            isOpen={this.state.dropdownOpen}
            toggle={this.toggle}
          >
            <DropdownToggle color="transparent" size="sm" caret>
              {selected}
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem onClick={() => this.handleOnClick("All", "all", 1)}>
                All
              </DropdownItem>
              <DropdownItem
                onClick={() => this.handleOnClick("Posts", "posts", 1)}
              >
                Posts
              </DropdownItem>
              <DropdownItem
                onClick={() => this.handleOnClick("Replies", "replies", 1)}
              >
                Replies
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </div>

        <div>
          <div>
            <FormGroup className="main-post-formgroup">
              <div className="input-label-block">
                <Input
                  className="primary light"
                  type="textarea"
                  name="post"
                  rows="4"
                  id="post"
                  value={this.state.addPost.body.replace(/(<([^>]+)>)/gi, "")}
                  onChange={this.handleCreatePostOnChange}
                  placeholder="&nbsp;"
                />

                <Label for="post" className="text-primary text-center">
                  Create a Post
                  <br />
                  <span className="font-weight-normal">
                    Showcase your business, mention specials/promotions, inform
                    people about your business.
                  </span>
                </Label>
              </div>
            </FormGroup>

            {/* Uploaded media shown here */}
            <div
              className="mb-3"
              hidden={
                !this.state.editPost &&
                  this.state.editPostType !== "post" &&
                  this.state.uploadMedia.selectedMedia.length > 0
                  ? false
                  : true
              }
            >
              <Row form>
                {this.state.uploadMedia.selectedMedia.length > 0 &&
                  this.state.uploadMedia.selectedMedia.map((item) => {
                    return (
                      <Col xs="auto" className="mb-3" key={item.id}>
                        <div className="d-flex pr-3 pt-3">
                          <div>
                            {(item.type === "image" ||
                              item.media_type === "image" ||
                              item.type === "video" ||
                              item.media_type === "video") && (
                                <div
                                  className="gallery-media"
                                  style={{ border: "none" }}
                                >
                                  <img
                                    src={item.file ? item.file : item.url}
                                    alt={
                                      item.filename
                                        ? item.filename.length < 20
                                          ? item.filename
                                          : this.truncate(item.filename)
                                        : item.name
                                          ? item.name.length < 20
                                            ? item.name
                                            : this.truncate(item.name)
                                          : ""
                                    }
                                  />
                                </div>
                              )}
                          </div>
                          <div className="mx-n3 mt-n3">
                            <Button
                              color="dark"
                              size="sm"
                              title="Remove Media"
                              onClick={() =>
                                this.handleOnClickRemoveSelectedGalleryMedia(
                                  item
                                )
                              }
                            >
                              <FontAwesomeIcon icon="minus" />{" "}
                            </Button>
                          </div>
                        </div>
                      </Col>
                    );
                  })}
              </Row>
            </div>

            {/* Selected branches shown here */}
            <div className="d-flex flex-wrap mx-n1 mb-2 scrollbar _visible" style={{ maxHeight: '150px', overflowY: 'auto' }}>
              {/* repeat this */}
              {this.state.selectedBranches.map((branch) => {
                return (
                  <div className="d-block mb-1 px-1">
                    <span className="fs-18">
                      <Badge
                        color="primary"
                        className="d-inline-flex align-items-center"
                      >
                        <span className="mr-1 font-weight-normal text-truncate">
                          {/* {`${branch.name}`} */}
                          {`${branch.name || ''}
                            (${branch.address1 + ", " || ''}
                            ${branch.city + ", " || ''}
                            ${branch.state + ", " || ''}
                            ${branch.country || ''}
                            )`}
                        </span>
                        <button
                          type="button"
                          className="close btn-sm ml-auto"
                          aria-label="Close"
                          onClick={() =>
                            this.removeFromSelected(
                              branch
                            )
                          }
                        >
                          <span>
                            <FontAwesomeIcon icon="times-circle" />{" "}
                          </span>
                        </button>
                      </Badge>
                    </span>
                  </div>
                )

              })}
            </div>
            <div className="d-flex">
              <div>
                <Button
                  color="white"
                  size="sm"
                  className="mr-2"
                  onClick={() => this.handleOnClickUploadMedia()}
                >
                  <FontAwesomeIcon icon="camera" />{" "}
                </Button>
                <UncontrolledDropdown className="dropdown-sm d-inline-block mr-2">
                  <DropdownToggle
                    color="white"
                    className="mw"
                    size="sm"
                    caret
                  >
                    {this.state.shareWith.selected}
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem header>Share with</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem
                      onClick={() =>
                        this.handleOnClickShareWith("Public", "")
                      }
                    >
                      <div className="d-flex">
                        <div className="mr-2">
                          <FontAwesomeIcon icon="globe-americas" />
                        </div>
                        <div>
                          <div>Public</div>
                          <div>
                            Anyone in <b>WikiReviews</b>
                          </div>
                        </div>

                        {/* Show if Selected */}
                        {this.state.shareWith.selected === "Public" && (
                          <div className="ml-auto align-self-center">
                            <FontAwesomeIcon icon="check" size="lg" />
                          </div>
                        )}
                      </div>
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem
                      onClick={() =>
                        this.handleOnClickShareWith(
                          "Recommenders",
                          "recommends"
                        )
                      }
                    >
                      <div className="d-flex">
                        <div className="mr-2">
                          <FontAwesomeIcon icon="users" />
                        </div>
                        <div>
                          <div>Recommenders</div>
                          <div>
                            Users that recommend <b>Your Business Page</b>
                          </div>
                        </div>

                        {/* Show if Selected */}
                        {this.state.shareWith.selected === "Recommenders" && (
                          <div className="ml-auto align-self-center">
                            <FontAwesomeIcon icon="check" size="lg" />
                          </div>
                        )}
                      </div>
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem
                      onClick={() =>
                        this.handleOnClickShareWith("Wants", "wants")
                      }
                    >
                      <div className="d-flex">
                        <div className="mr-2">
                          <FontAwesomeIcon icon="heart" />
                        </div>
                        <div>
                          <div>Wants</div>
                          <div>
                            People that have <b>Your Business Page </b> in
                              their Wants
                            </div>
                        </div>

                        {/* Show if Selected */}
                        {this.state.shareWith.selected === "Wants" && (
                          <div className="ml-auto align-self-center">
                            <FontAwesomeIcon icon="check" size="lg" />
                          </div>
                        )}
                      </div>
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem
                      onClick={() =>
                        this.handleOnClickShareWith("Favourites", "favorites")
                      }
                    >
                      <div className="d-flex">
                        <div className="mr-2">
                          <FontAwesomeIcon icon="star" />
                        </div>
                        <div>
                          <div>Favourites</div>
                          <div>
                            People that have <b>Your Business Page </b> in
                              their Favourites
                            </div>
                        </div>

                        {/* Show if Selected */}
                        {this.state.shareWith.selected === "Favourites" && (
                          <div className="ml-auto align-self-center">
                            <FontAwesomeIcon icon="check" size="lg" />
                          </div>
                        )}
                      </div>
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem
                      onClick={() =>
                        this.handleOnClickShareWith("Custom", "favorites")
                      }
                    >
                      <div className="d-flex">
                        <div className="mr-2">
                          <FontAwesomeIcon icon="cog" />
                        </div>
                        <div>
                          <div>Custom</div>
                          <div>Choose who can see the post</div>
                        </div>

                        {/* Show if Selected */}
                        {this.state.shareWith.selected === "Custom" && (
                          <div className="ml-auto align-self-center">
                            <FontAwesomeIcon icon="check" size="lg" />
                          </div>
                        )}
                      </div>
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>

                <UncontrolledDropdown className="dropdown-sm d-inline-block">
                  <DropdownToggle
                    color="white"
                    className="mw"
                    size="sm"
                    caret
                  >
                    Choose Branches
                    </DropdownToggle>
                  <DropdownMenu className="force-down">
                    <div className="position-relative" style={{ width: '500px' }}>
                      {/* <Input
                        type="text"
                        className="primary border-top-0 border-left-0 border-right-0"
                        name="branch_search"
                        onChange={this.handleOnChangeBranches}
                        value={this.state.branchText}
                        placeholder="Search..."
                        /> */}
                      <div className="px-2">
                        <Row form>
                          <Col md={6}>
                            <FormGroup>
                              <Label for="selectCountry">Select Country</Label>
                              <Input
                                className="primary p-1 fs-14"
                                type="select"
                                value={country}
                                name="select_country"
                                onChange={this.handleChange}
                                id="selectCountry"
                              >
                                <option>--Select--</option>
                                {typeof countryList !== "undefined" &&
                                  countryList.length > 0 ? (
                                    countryList.map((dropdown, index) => {
                                      return (
                                        <option
                                          key={index}
                                          dropdown={dropdown}
                                          value={dropdown}
                                        >
                                          {dropdown}
                                        </option>
                                      );
                                    })
                                  ) : (
                                    <option> </option>
                                  )}
                              </Input>
                            </FormGroup>
                          </Col>
                          <Col md={6}>
                            <FormGroup>
                              <Label for="selectState">Select State</Label>
                              <Input
                                className="primary p-1 fs-14"
                                type="select"
                                name="select_state"
                                value={stateName}
                                id="selectState"
                                onChange={this.handleChange}
                              >
                                <option>--Select--</option>
                                {typeof countryState !== "undefined" &&
                                  countryState.length > 0 ? (
                                    countryState.map((dropdown, index) => {
                                      return (
                                        <option
                                          key={index}
                                          dropdown={dropdown}
                                          value={dropdown}
                                        >
                                          {dropdown}
                                        </option>
                                      );
                                    })
                                  ) : (
                                    <option> </option>
                                  )}
                              </Input>
                            </FormGroup>
                          </Col>
                          <Col xs={{ size: "auto", order: 2 }} lg={{ order: 1 }} className="align-self-center">
                            <CustomInput
                              type="checkbox"
                              id={"selectallBranchesForPost"}
                              name="selectallBranchesForPost"
                              onChange={""}
                              checked={this.state.checkAll}
                              onClick={this.handleAllCheckedBranch}
                            >
                              <Label
                                for={"selectallBranchesForPost"}
                                className="text-dark font-weight-normal fs-14"
                              >
                                {this.state.checkAll ? "Deselect All" : "Select All"}
                              </Label>
                            </CustomInput>
                          </Col>
                        </Row>
                        <div
                          style={{
                            maxHeight: "calc(200px)",
                            overflowY: "auto"
                          }}
                        >

                          {newBranches &&
                            Array.isArray(newBranches) &&
                            newBranches.length > 0 ? (
                              newBranches.map((item, index) => {

                                return (

                                  <CustomInput
                                    type="checkbox"
                                    id={"post_select_branch_" + item.address_id}
                                    name="select_branch"
                                    key={item.address_id}
                                    onChange={(evt) => { this.handleChange(evt, item) }}
                                    // value={item.isChecked}
                                    checked={item?.isChecked ? true : false}
                                  >
                                    <Label
                                      for={"post_select_branch_" + item.address_id}
                                      className="text-dark font-weight-normal fs-14"
                                    >
                                      {item.name +
                                        " ( " +
                                        item.address1 +
                                        ", " +
                                        item.city +
                                        ", " +
                                        item.state +
                                        ", " +
                                        item.country +
                                        ")"}
                                    </Label>
                                  </CustomInput>
                                )

                              })
                            ) : (
                              <div className="bg-white p-3">
                                <h2 className="text-secondary-dark">
                                  No Company to Display
                                  </h2>
                              </div>
                            )}
                        </div>
                      </div>
                      {/* <div
                        hidden
                        className="position-absolute"

                        style={{ zIndex: 9 }}
                      >
                        <ul className="list-unstyled bg-white border shadow-sm"
                          style={{ maxHeight: "200px", overflowY: "auto" }}>
                          {
                            allBranches && allBranches.length > 0 && allBranches.map((branch, index) => {
                              if (branch.id !== this.state.branchID) {
                                return (
                                  <li
                                    className={`selectable-item ${this.state.selectedBranches
                                      .map((item) => item.id)
                                      .includes(branch.id)
                                      ? "active"
                                      : ""
                                      }`}
                                    onClick={() => {
                                      this.handleSelectFromDropdown(
                                        branch,
                                      )
                                    }}
                                  >{`${branch.name}`}
                                    <br />
                                    <span>
                                      {branch.address1
                                        ? "" + branch.address1 + ", "
                                        : " "}

                                      {branch.city
                                        ? branch.city +
                                        ", "
                                        : " "}
                                      {branch.state
                                        ? branch.state +
                                        ", "
                                        : " "}
                                      {branch.country
                                        ? branch.country +
                                        ", "
                                        : " "}
                                      {branch.zipcode}
                                    </span>

                                  </li>
                                )
                              }
                            })
                          }
                         </ul>
                      </div> */}
                    </div>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </div>
              <div className="ml-auto">
                <Button
                  color="transparent"
                  className="mw mr-2"
                  onClick={this.handleOnClearCreate}
                >
                  {" "}
                    Clear
                  </Button>
                <Button
                  color="primary"
                  className="mw ml-0"
                  onClick={this.handleOnSubmitCreatePost}
                >
                  {" "}
                    Post{" "}
                </Button>
              </div>
            </div>
          </div>

          {/* All Posts by me container */}
          <div className="mt-3">
            {myPostsList.count ? (
              myPostsList.results.map((post) => {
                return (
                  <div className="bg-white p-3 mb-2" key={post.id}>
                    <Media className="media-post">
                      <Media>
                        <img
                          className="media-object"
                          src={
                            post.current_profile_pic
                              ? post.current_profile_pic
                              : require("../../../assets/images/user.png")
                          }
                          onError={(error) =>
                            (error.target.src = require("../../../assets/images/user.png"))
                          }
                          alt="User Image"
                        />
                      </Media>
                      <Media body>
                        <Media heading>
                          <div className="d-flex">
                            <div className="align-self-center">
                              {post.sender}{" "}
                              <span className="time-stamp">
                                <span>&middot;</span> {post.sent_at}
                              </span>
                            </div>
                            <div className="ml-auto">
                              <Button
                                size="sm"
                                color="transparent"
                                className="ml-1 text-muted"
                                onClick={() => {
                                  this.setState({ copyPostToBranchesModal: true });
                                  this.handleCopyPostToBranches(post);
                                }}
                                title="Copy"
                              >
                                <FontAwesomeIcon icon="copy" />{" "}
                              </Button>
                              <Button
                                size="sm"
                                color="transparent"
                                className="ml-1 text-muted"
                                onClick={() =>
                                  this.toggleEditForm(post.id, "L1")
                                }
                                title="Edit"
                              >
                                {" "}
                                <FontAwesomeIcon icon="pencil-alt" />{" "}
                              </Button>
                              <Button
                                size="sm"
                                color="transparent"
                                className="ml-1 text-muted"
                                onClick={() => {
                                  this.setState({
                                    deletePostId: post.id,
                                    deletePostType: "post",
                                  });
                                  this.confirmDeleteModalToggle();
                                }}
                                title="Delete"
                              >
                                {" "}
                                <FontAwesomeIcon icon="trash-alt" />{" "}
                              </Button>
                            </div>
                          </div>
                        </Media>

                        {/* Hide when editing Post */}
                        <div>
                          <div>
                            <div>
                              {ReactHtmlParser(
                                post.body.replace(/\n/g, "<br />")
                              )}
                            </div>
                            {/* {post.body.replace(/(<([^>]+)>)/gi, "")} */}
                            {post.attachments.length > 0 ? (
                              <div
                                hidden={
                                  this.state.editPost &&
                                    this.state.editPostType === "post" &&
                                    editFormVisible["L1" + post.id]
                                    ? true
                                    : false
                                }
                              >
                                {post.attachments.map((attachment) => (
                                  <div>
                                    {attachment.type === "image" && (
                                      <img
                                        src={attachment.url}
                                        alt={
                                          attachment.filename.length < 20
                                            ? attachment.filename
                                            : this.truncate(attachment.filename)
                                        }
                                        width="100%"
                                      />
                                    )}
                                    {attachment.type === "video" && (
                                      <video width="320" height="240" controls>
                                        <source
                                          src={attachment.url}
                                          type="video/mp4"
                                        />
                                        Your browser does not support the video
                                        tag.
                                      </video>
                                    )}
                                  </div>
                                ))}
                              </div>
                            ) : (
                                ""
                              )}
                          </div>
                        </div>

                        {/* Show when editing post */}
                        {editFormVisible["L1" + post.id] && (
                          <div>
                            <FormGroup className="main-post-formgroup">
                              <div className="input-label-block">
                                <Input
                                  bsSize="sm"
                                  className="primary"
                                  type="textarea"
                                  name="main_post"
                                  rows="3"
                                  placeholder="What's in your mind?"
                                  value={editPost.body.replace(
                                    /(<([^>]+)>)/gi,
                                    ""
                                  )}
                                  onChange={this.handleEditPostOnChange}
                                />
                                <span className="text-danger">
                                  {this.state.editPostError}
                                </span>
                              </div>
                              <div
                                className="mb-3"
                                hidden={
                                  this.state.editPost &&
                                    this.state.editPostType === "post" &&
                                    this.state.uploadMedia.selectedMedia.length >
                                    0
                                    ? false
                                    : true
                                }
                              >
                                <Row form>
                                  {this.state.uploadMedia.selectedMedia.length >
                                    0 &&
                                    this.state.uploadMedia.selectedMedia.map(
                                      (item) => {
                                        return (
                                          <Col
                                            xs="auto"
                                            className="mb-3"
                                            key={item.id}
                                          >
                                            <div className="d-flex pr-3 pt-3">
                                              <div>
                                                {(item.media_type === "image" ||
                                                  item.media_type ===
                                                  "video") && (
                                                    <div
                                                      className="selectable-media"
                                                      style={{
                                                        cursor: "default",
                                                      }}
                                                    >
                                                      <div className="gallery-media">
                                                        <img
                                                          src={
                                                            item.url
                                                              ? item.url
                                                              : item.file
                                                          }
                                                          alt={
                                                            item.filename
                                                              ? item.filename
                                                                .length < 20
                                                                ? item.filename
                                                                : this.truncate(
                                                                  item.filename
                                                                )
                                                              : item.name
                                                                ? item.name.length <
                                                                  20
                                                                  ? item.name
                                                                  : this.truncate(
                                                                    item.name
                                                                  )
                                                                : ""
                                                          }
                                                        />
                                                      </div>
                                                    </div>
                                                  )}
                                              </div>
                                              <div className="mx-n3 mt-n3">
                                                <Button
                                                  color="dark"
                                                  size="sm"
                                                  title="Remove Media"
                                                  onClick={() =>
                                                    this.handleOnClickRemoveSelectedGalleryMedia(
                                                      item
                                                    )
                                                  }
                                                >
                                                  <FontAwesomeIcon icon="minus" />{" "}
                                                </Button>
                                              </div>
                                            </div>
                                          </Col>
                                        );
                                      }
                                    )}
                                </Row>
                              </div>
                              <div className="d-flex">
                                <div>
                                  <Button
                                    color="white"
                                    size="sm"
                                    className="mr-2"
                                    onClick={() =>
                                      this.handleOnClickUploadMedia()
                                    }
                                  >
                                    <FontAwesomeIcon icon="camera" />{" "}
                                  </Button>
                                </div>
                                <div className="ml-auto">
                                  <Button
                                    color="transparent"
                                    size="sm"
                                    className="mr-2"
                                    onClick={this.handleOnClearEdit}
                                  >
                                    {" "}
                                    Clear
                                  </Button>
                                  <Button
                                    color="transparent"
                                    size="sm"
                                    className="ml-0 mr-2"
                                    onClick={() =>
                                      this.toggleEditForm(post.id, "L1")
                                    }
                                  >
                                    {" "}
                                    Cancel
                                  </Button>
                                  <Button
                                    color="primary"
                                    size="sm"
                                    className="mw ml-0"
                                    onClick={this.handleOnSubmitEditPost}
                                  >
                                    {" "}
                                    Post{" "}
                                  </Button>
                                </div>
                              </div>
                            </FormGroup>
                            <hr />
                          </div>
                        )}

                        {/* Comments Listing */}
                        {post.conversation.length > 0 && (
                          <div>
                            {post.conversation.map((comment) => {
                              return (
                                <div key={comment.msg_id}>
                                  <Media className="media-post">
                                    <Media>
                                      <img
                                        className="media-object"
                                        src={
                                          comment.current_profile_pic
                                            ? comment.current_profile_pic
                                            : require("../../../assets/images/user.png")
                                        }
                                        onError={(error) =>
                                          (error.target.src = require("../../../assets/images/user.png"))
                                        }
                                        alt="User Image"
                                      />
                                    </Media>
                                    <Media body>
                                      <Media heading>
                                        <div className="d-flex">
                                          <div className="align-self-center">
                                            {comment.sender}{" "}
                                            <span className="time-stamp">
                                              <span>&middot;</span>{" "}
                                              {comment.sent_at}
                                            </span>
                                          </div>
                                          <div className="ml-auto">
                                            <button
                                              className="btn btn-link btn-sm text-muted"
                                              onClick={() =>
                                                this.toggleReplyForm(
                                                  comment.msg_id,
                                                  "L2"
                                                )
                                              }
                                            >
                                              REPLY
                                            </button>
                                            <Button
                                              size="sm"
                                              color="transparent"
                                              className="text-muted"
                                              onClick={() =>
                                                this.toggleEditForm(
                                                  comment.msg_id,
                                                  "L2",
                                                  "comment",
                                                  comment.body
                                                )
                                              }
                                            >
                                              {" "}
                                              <FontAwesomeIcon icon="pencil-alt" />{" "}
                                            </Button>
                                            <Button
                                              size="sm"
                                              color="transparent"
                                              className="ml-0 text-muted"
                                              onClick={() => {
                                                this.setState({
                                                  deletePostId: comment.msg_id,
                                                  deletePostType: "comment",
                                                });
                                                this.confirmDeleteModalToggle();
                                              }}
                                            >
                                              {" "}
                                              <FontAwesomeIcon icon="trash-alt" />{" "}
                                            </Button>
                                          </div>
                                        </div>
                                      </Media>
                                    </Media>
                                  </Media>
                                  <div>
                                    <div style={{ "margin-left": "50px" }}>
                                      {ReactHtmlParser(
                                        comment.body.replace(/\n/g, "<br />")
                                      )}
                                    </div>
                                  </div>
                                  {/* Show when editing post */}
                                  {editFormVisible["L2" + comment.msg_id] && (
                                    <div>
                                      <FormGroup className="main-post-formgroup">
                                        <div className="input-label-block">
                                          <Input
                                            bsSize="sm"
                                            className="primary"
                                            type="textarea"
                                            name="main_post"
                                            rows="3"
                                            placeholder="What's in your mind?"
                                            value={editPost.body.replace(
                                              /(<([^>]+)>)/gi,
                                              ""
                                            )}
                                            onChange={
                                              this.handleEditPostOnChange
                                            }
                                          />
                                          {this.state.editPostError}
                                        </div>
                                        <div className="d-flex">
                                          <div>
                                            {/* <Button
                                              color="white"
                                              size="sm"
                                              className="mr-2"
                                            >
                                              <FontAwesomeIcon icon="camera" />{" "}
                                            </Button> */}
                                          </div>
                                          <div className="ml-auto">
                                            <Button
                                              color="transparent"
                                              size="sm"
                                              className="mr-2"
                                              onClick={this.handleOnClearEdit}
                                            >
                                              {" "}
                                              Clear
                                            </Button>
                                            <Button
                                              color="transparent"
                                              size="sm"
                                              className="ml-0 mr-2"
                                              onClick={() =>
                                                this.toggleEditForm(
                                                  comment.msg_id,
                                                  "L2"
                                                )
                                              }
                                            >
                                              {" "}
                                              Cancel
                                            </Button>
                                            <Button
                                              color="primary"
                                              size="sm"
                                              className="mw ml-0"
                                              onClick={
                                                this.handleOnSubmitEditPost
                                              }
                                            >
                                              {" "}
                                              Post{" "}
                                            </Button>
                                          </div>
                                        </div>
                                      </FormGroup>
                                      <hr />
                                    </div>
                                  )}

                                  {/* Show when replying to comment */}
                                  {replyFormVisible["L2" + comment.msg_id] && (
                                    <div>
                                      <Media className="media-post mt-3">
                                        <Media>
                                          <Media
                                            object
                                            src={
                                              this.props.profile_data
                                                .current_profile_file
                                                ? this.props.profile_data
                                                  .current_profile_file
                                                : require("../../../assets/images/user.png")
                                            }
                                            alt="User Image"
                                          />
                                        </Media>
                                        <Media body>
                                          <FormGroup className="mb-0">
                                            <Input
                                              bsSize="sm"
                                              className="mb-2"
                                              type="textarea"
                                              name="body"
                                              value={this.state.addReply.body}
                                              onChange={
                                                this.handleOnReplyChange
                                              }
                                              placeholder="Write a Reply..."
                                            />
                                            <div className="text-right">
                                              <div className="text-right">
                                                <Button
                                                  size="sm"
                                                  color="primary"
                                                  className=" mr-2"
                                                  onClick={this.handleOnReplySubmit(
                                                    comment.msg_id,
                                                    "L2"
                                                  )}
                                                >
                                                  {" "}
                                                  Submit
                                                </Button>
                                                <Button
                                                  size="sm"
                                                  color="light"
                                                  className="ml-0"
                                                  onClick={() =>
                                                    this.toggleReplyForm(
                                                      comment.msg_id,
                                                      "L2"
                                                    )
                                                  }
                                                >
                                                  {" "}
                                                  Cancel{" "}
                                                </Button>
                                              </div>
                                            </div>
                                          </FormGroup>
                                        </Media>
                                      </Media>
                                      <hr />
                                    </div>
                                  )}

                                  {/* Show replies */}
                                  {comment.replies &&
                                    comment.replies.length > 0 && (
                                      <div style={{ "margin-left": "50px" }}>
                                        {comment.replies.map((reply) => {
                                          return (
                                            <div key={reply.id}>
                                              <Media className="media-post">
                                                <Media>
                                                  <img
                                                    className='media-object'
                                                    src={
                                                      reply.sender
                                                        .current_profile_pic
                                                        ? reply.sender
                                                          .current_profile_pic
                                                        : require("../../../assets/images/user.png")
                                                    }
                                                    onError={(error) =>
                                                      (error.target.src = require("../../../assets/images/user.png"))
                                                    }
                                                    alt="User Image"
                                                  />
                                                </Media>
                                                <Media body>
                                                  <Media heading>
                                                    <div className="d-flex">
                                                      <div className="align-self-center">
                                                        {reply.sender.full_name}{" "}
                                                        <span className="time-stamp">
                                                          <span>&middot;</span>{" "}
                                                          {reply.sent_at}
                                                        </span>
                                                      </div>
                                                      <div className="ml-auto">
                                                        <Button
                                                          size="sm"
                                                          color="transparent"
                                                          className="ml-0 text-muted"
                                                          onClick={() => {
                                                            this.setState({
                                                              deletePostId:
                                                                reply.id,
                                                              deletePostType:
                                                                "reply",
                                                            });
                                                            this.confirmDeleteModalToggle();
                                                          }}
                                                        >
                                                          {" "}
                                                          <FontAwesomeIcon icon="trash-alt" />{" "}
                                                        </Button>
                                                      </div>
                                                    </div>
                                                  </Media>
                                                </Media>
                                              </Media>
                                              <div>
                                                <div
                                                  style={{
                                                    "margin-left": "50px",
                                                  }}
                                                >
                                                  {reply.body.replace(
                                                    /(<([^>]+)>)/gi,
                                                    ""
                                                  )}
                                                </div>
                                              </div>
                                            </div>
                                          );
                                        })}
                                      </div>
                                    )}
                                </div>
                              );
                            })}
                          </div>
                        )}

                        {/* Reply Button */}
                        <div hidden>
                          <div className="d-flex">
                            <div className="ml-auto align-self-start">
                              {/* <span className="mx-1">|</span> */}
                              <button
                                className="btn btn-link btn-sm text-muted"
                                onClick={() =>
                                  this.toggleReplyForm(post.id, "L1")
                                }
                              >
                                REPLY
                              </button>
                            </div>
                          </div>
                        </div>

                        {/* Show When Replying */}

                        <div>
                          <Media className="media-post mt-3">
                            <Media>
                              <Media
                                object
                                src={
                                  this.props.profile_data.current_profile_file
                                    ? this.props.profile_data
                                      .current_profile_file
                                    : require("../../../assets/images/user.png")
                                }
                                alt="User Image"
                              />
                            </Media>
                            <Media body>
                              <FormGroup className="mb-0">
                                <Input
                                  bsSize="sm"
                                  className="mb-2"
                                  type="textarea"
                                  name="body"
                                  value={this.state.addComment[post.id]}
                                  onChange={this.handleOnCommentChange(post.id)}
                                  placeholder="Write a Comment..."
                                />
                                <div className="text-right">
                                  <div className="text-right">
                                    <Button
                                      disabled={this.state.addComment[post.id] === "" ? true : false}
                                      size="sm"
                                      color="primary"
                                      className=" mr-2"
                                      onClick={this.handleOnCommentSubmit(
                                        post.id
                                      )}
                                    >
                                      {" "}
                                      Submit
                                    </Button>
                                    <Button
                                      size="sm"
                                      color="light"
                                      className="ml-0"
                                      hidden
                                    >
                                      {" "}
                                      Cancel{" "}
                                    </Button>
                                  </div>
                                </div>
                              </FormGroup>
                            </Media>
                          </Media>
                          <hr />
                        </div>
                      </Media>
                    </Media>
                  </div>
                );
              })
            ) : (
                <div className="bg-white p-3">
                  <h2 className="text-secondary-dark">No Posts to Display</h2>
                </div>
              )}
          </div>
        </div>

        {/* Delete Confirmation Modal */}
        <Modal
          isOpen={this.state.confirmDeleteModal}
          toggle={this.confirmDeleteModalToggle}
          className="text-center"
        >
          <div className="bg-white modal-header">
            <h5 className="modal-title mx-auto text-dark">Confirmation</h5>
            <button
              className="btn btn-sm"
              aria-label="Close"
              onClick={this.confirmDeleteModalToggle}
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <ModalBody className="text-dark">
            Are you sure you want to delete?
          </ModalBody>
          <ModalFooter className="bg-white">
            <div className="text-center w-100">
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.confirmDeleteModalToggle}
              >
                Cancel
              </Button>
              <Button
                color="primary"
                size="sm"
                className="mw"
                onClick={this.handleOnDeleteConfirmation}
              >
                Ok
              </Button>
            </div>
          </ModalFooter>
        </Modal>

        {/* Limited Post modal popup */}
        <Modal
          isOpen={this.state.limitedPostsModal}
          toggle={this.limitedPostsModalToggle}
        >
          <ModalBody className="p-3">
            <Row className="mb-3">
              <Col xs={3}>
                <img
                  className="img-fluid"
                  src={require("../../../assets/images/paywall.png")}
                  alt="Bell"
                />
              </Col>
              <Col xs={9}>
                <h4 className="text-primary mb-2 ff-base fs-16">
                  You have added 3 posts for free.
                </h4>
                <p className="text-primary-dark ff-base">
                  Upgrade to <strong>Premium Business</strong> account to get
                  unlimited ability to dispute and other more benefits.
                </p>
              </Col>
            </Row>
            <Button color="primary" size="lg" block>
              {" "}
              Upgrade to PREMIUM and get more benefits
            </Button>
            <div className="text-center">
              <Button color="link" onClick={this.limitedPostsModalToggle}>
                Stay with FREE account
              </Button>
            </div>
          </ModalBody>
        </Modal>

        {/* Upload Media Modal */}
        <Modal
          isOpen={this.state.uploadMediaModal}
          toggle={this.uploadMediaModalToggle}
          size="lg"
        >
          <ModalHeader toggle={this.uploadMediaModalToggle}>
            Upload Media
          </ModalHeader>
          <ModalBody className="p-3">
            <Row>
              <Col xs={3}>
                <Nav pills className="flex-column">
                  <NavItem className="text-center">
                    <NavLink
                      href="#"
                      active={this.state.selectedUpMediaType === "upload"}
                      onClick={() => {
                        this.handleOnClickSelectedUploadMediaType("upload");
                      }}
                    >
                      Upload
                    </NavLink>
                  </NavItem>
                  <NavItem className="text-center">
                    <NavLink
                      href="#"
                      active={this.state.selectedUpMediaType === "gallery"}
                      onClick={() => {
                        this.handleOnClickSelectedUploadMediaType("gallery");
                      }}
                    >
                      Gallery
                    </NavLink>
                  </NavItem>
                  <NavItem className="text-center">
                    <NavLink
                      href="#"
                      active={this.state.selectedUpMediaType === "embed"}
                      onClick={() => {
                        this.handleOnClickSelectedUploadMediaType("embed");
                      }}
                    >
                      Embed
                    </NavLink>
                  </NavItem>
                </Nav>
              </Col>
              <Col xs={9}>
                <TabContent activeTab={this.state.selectedUpMediaType}>
                  <TabPane tabId="upload">
                    <div
                      className="mb-3 type-file-block"
                      hidden={
                        this.state.uploadMedia.uploadFiles &&
                          this.state.uploadMedia.uploadFiles.length > 0
                          ? true
                          : false
                      }
                    >
                      <Input
                        type="file"
                        name="upload_media_file"
                        id="uploadFilesFromSystem"
                        accept="image/*, video/*"
                        onChange={this.handleOnFileUploadChange}
                        multiple
                      />
                      <Label for="uploadFilesFromSystem">
                        <FontAwesomeIcon
                          icon="upload"
                          size="2x"
                          className="text-primart"
                        />
                        <br />
                        Upload Files From Computer
                        <br />
                        <small className="text-muted">
                          (or Drag files here)
                        </small>
                      </Label>
                    </div>
                    {this.state.uploadMedia.uploadFiles.length > 0 && (
                      <Row className="mb-3" form>
                        <Col xs={12}>
                          <div
                            style={{
                              maxWidth: "120px",
                              margin: "0 1rem 1rem auto",
                            }}
                          >
                            <div className="text-center mb-1 small">
                              {this.state.uploadMedia.progress === 100 ? (
                                <div className="text-success">
                                  <FontAwesomeIcon
                                    icon="check-circle"
                                    className="mr-1"
                                  />{" "}
                                  Uploaded
                                </div>
                              ) : (
                                  <div>
                                    Uploading{" "}
                                    <span className="text-success font-weight-bold ff-base">
                                      {this.state.uploadMedia.progress.toFixed(0)}
                                    %
                                  </span>
                                  </div>
                                )}
                            </div>
                            <Progress
                              value={this.state.uploadMedia.progress}
                              style={{ height: "8px" }}
                            ></Progress>
                          </div>
                        </Col>
                        {this.state.uploadMedia.uploadFiles.map((file) => {
                          return (
                            <Col xs="auto">
                              <div className="d-flex pr-3 pt-3">
                                <div>
                                  <div
                                    className="selectable-media"
                                    style={{ cursor: "default" }}
                                  >
                                    <div className="gallery-media">
                                      <img
                                        src={file.url}
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div className="mx-n3 mt-n3">
                                  <Button
                                    color="dark"
                                    size="sm"
                                    title="Remove Media"
                                    hidden={file.id === "" ? true : false}
                                    onClick={this.handleOnClickRemoveSelectedMedia(
                                      file.id
                                    )}
                                  >
                                    <FontAwesomeIcon icon="minus" />{" "}
                                  </Button>
                                </div>
                              </div>
                            </Col>
                          );
                        })}
                        <Col xs="auto">
                          <div className="d-flex pt-3">
                            <div className="selectable-media" hidden>
                              <Label
                                for="uploadFilesFromSystemMini"
                                className="gallery-media"
                                style={{
                                  borderStyle: "dashed",
                                  cursor: "pointer",
                                }}
                              >
                                <div className="d-flex h-100 align-items-center justify-content-center">
                                  <span className="fs-14">Upload More</span>
                                </div>
                              </Label>
                              <Input
                                type="file"
                                name="upload_media_file"
                                id="uploadFilesFromSystemMini"
                                accept="image/*, video/*"
                                onChange={this.handleOnFileUploadChange}
                                multiple
                                style={{ display: "none" }}
                              />
                            </div>
                          </div>
                        </Col>
                      </Row>
                    )}
                    <div className="d-flex mx-n2">
                      <div className="px-2">
                        <Button
                          color="primary"
                          size="sm"
                          className="mw"
                          onClick={this.uploadMediaModalToggle}
                        >
                          Cancel
                        </Button>
                      </div>
                      <div className="px-2 ml-auto">
                        <Button
                          color="primary"
                          size="sm"
                          className="mw"
                          onClick={() => {
                            this.setState({
                              ...this.state,
                              uploadMediaModal: false,
                            });
                          }}
                        >
                          Insert
                        </Button>
                      </div>
                    </div>
                  </TabPane>
                  <TabPane tabId="gallery">
                    <div className="mb-2">
                      <Nav tabs className="d-inline-flex mb-0">
                        <NavItem>
                          <NavLink
                            href="#"
                            active={this.state.showGalleryType === "images"}
                            onClick={() => {
                              this.handleOnClickGalleryType("images");
                            }}
                          >
                            Image Gallery
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            href="#"
                            active={this.state.showGalleryType === "videos"}
                            onClick={() => {
                              this.handleOnClickGalleryType("videos");
                            }}
                          >
                            Video Gallery
                          </NavLink>
                        </NavItem>
                      </Nav>
                    </div>
                    <TabContent activeTab={this.state.showGalleryType}>
                      <TabPane tabId="images">
                        <div>
                          <FormGroup className="form-row justify-content-end">
                            <Col xs="auto">
                              <Label className="small" for="selectAlbumType">
                                Album Type:
                              </Label>
                            </Col>
                            <Col xs="auto">
                              <Input
                                bsSize="sm"
                                type="select"
                                name="album_type_select"
                                id="selectAlbumType"
                                defaultValue={this.state.uploadMedia.albumType}
                                onChange={this.handleOnClickAlbumTypeChange}
                              >
                                <option value="">All</option>
                                {this.state.uploadMedia.albumTypesList.length >
                                  0 &&
                                  this.state.uploadMedia.albumTypesList.map(
                                    (type) => (
                                      <option value={type.category}>
                                        {type.category}
                                      </option>
                                    )
                                  )}
                              </Input>
                            </Col>
                          </FormGroup>
                        </div>
                        <div
                          className="px-3 my-3"
                          style={{ maxHeight: "400px", overflow: "auto" }}
                        >
                          {this.state.uploadMedia.albumTypeData &&
                            this.state.uploadMedia.albumTypeData.count > 0 ? (
                              <Row xs={2} md={3} lg={4}>
                                {this.state.uploadMedia.albumTypeData.results.map(
                                  (media) => {
                                    return (
                                      <Col className="mb-3" key={media.id}>
                                        {media.type === "image" && (
                                          <div
                                            className={`selectable-media ${this.state.uploadMedia.selectedMediaIds.indexOf(
                                              media.id
                                            ) !== -1
                                              ? "selected"
                                              : ""
                                              }`}
                                            onClick={() =>
                                              this.handleOnClickSelectGalleryMedia(
                                                media
                                              )
                                            }
                                          >
                                            <div className="gallery-media">
                                              <img
                                                src={media.file}
                                                alt={
                                                  media.filename.length < 20
                                                    ? media.filename
                                                    : this.truncate(
                                                      media.filename
                                                    )
                                                }
                                              />
                                            </div>
                                            <div className="font-weight-bold fs-12 text-secondary-dark mb-0 text-truncate">
                                              {media.category
                                                ? media.category
                                                : "No Category"}
                                            </div>
                                            <div className="font-weight-normal fs-15 d-block">
                                              {media.caption
                                                ? media.caption
                                                : "No Caption"}
                                            </div>
                                          </div>
                                        )}
                                      </Col>
                                    );
                                  }
                                )}
                              </Row>
                            ) : (
                              <div className="bg-white p-3">
                                <h2 className="text-secondary-dark">No images</h2>
                              </div>
                            )}
                        </div>
                        <div className="d-flex mx-n2">
                          <div className="px-2">
                            <Button
                              color="primary"
                              size="sm"
                              className="mw"
                              onClick={this.uploadMediaModalToggle}
                            >
                              Cancel
                            </Button>
                          </div>
                          <div className="px-2 ml-auto">
                            <Button
                              color="primary"
                              size="sm"
                              className="mw"
                              onClick={() => {
                                this.setState({
                                  ...this.state,
                                  uploadMediaModal: false,
                                });
                              }}
                            >
                              Insert
                            </Button>
                          </div>
                        </div>
                      </TabPane>
                      <TabPane tabId="videos">
                        <div
                          className="px-3 my-3"
                          style={{ maxHeight: "400px", overflow: "auto" }}
                        >
                          {this.state.uploadMedia.albumTypeData &&
                            this.state.uploadMedia.albumTypeData.count > 0 ? (
                              <Row xs={2} md={3} lg={4}>
                                {this.state.uploadMedia.albumTypeData.results.map(
                                  (media) => {
                                    return (
                                      <Col className="mb-3" key={media.id}>
                                        {media.type === "video" && (
                                          <div
                                            className={`selectable-media ${this.state.uploadMedia.selectedMediaIds.indexOf(
                                              media.id
                                            ) !== -1
                                              ? "selected"
                                              : ""
                                              }`}
                                            onClick={() =>
                                              this.handleOnClickSelectGalleryMedia(
                                                media
                                              )
                                            }
                                          >
                                            <div className="gallery-media">
                                              <img
                                                src={media.thumbnail}
                                                alt={
                                                  media.filename.length < 20
                                                    ? media.filename
                                                    : this.truncate(
                                                      media.filename
                                                    )
                                                }
                                              />
                                            </div>
                                            <div className="font-weight-bold fs-12 text-secondary-dark mb-0 text-truncate">
                                              {media.category
                                                ? media.category
                                                : "No Category"}
                                            </div>
                                            <div className="font-weight-normal fs-15 d-block">
                                              {media.caption
                                                ? media.caption
                                                : "No Caption"}
                                            </div>
                                          </div>
                                        )}
                                      </Col>
                                    );
                                  }
                                )}
                              </Row>
                            ) : (
                              <div className="bg-white p-3">
                                <h2 className="text-secondary-dark">No videos</h2>
                              </div>
                            )}
                        </div>
                        <div className="d-flex mx-n2">
                          <div className="px-2">
                            <Button
                              color="primary"
                              size="sm"
                              className="mw"
                              onClick={this.uploadMediaModalToggle}
                            >
                              Cancel
                            </Button>
                          </div>
                          <div className="px-2 ml-auto">
                            <Button
                              color="primary"
                              size="sm"
                              className="mw"
                              onClick={() => {
                                this.setState({
                                  ...this.state,
                                  uploadMediaModal: false,
                                });
                              }}
                            >
                              Insert
                            </Button>
                          </div>
                        </div>
                      </TabPane>
                    </TabContent>
                  </TabPane>
                  <TabPane tabId="embed">
                    <div className="text-muted mb-2">
                      Submit the link for the image or video you want to embed{" "}
                    </div>
                    <div className="mb-2">
                      {/* Repeat this while adding */}
                      {this.mapEmbedLinks()}
                    </div>
                    <div className="d-flex mx-n2 mb-2">
                      <div className="px-2 ml-auto">
                        <Button
                          title="Add"
                          color="primary"
                          size="sm"
                          onClick={() => {
                            this.addEmbedlinkRow();
                          }}
                        >
                          <FontAwesomeIcon icon="plus" />
                        </Button>
                      </div>
                    </div>
                    <div className="d-flex mx-n2">
                      <div className="px-2">
                        <Button
                          color="primary"
                          size="sm"
                          className="mw"
                          onClick={this.uploadMediaModalToggle}
                        >
                          Cancel
                        </Button>
                      </div>
                      <div className="px-2 ml-auto">
                        <Button
                          color="primary"
                          size="sm"
                          className="mw"
                          onClick={() => {
                            this.insertEmbedLinks();
                          }}
                        >
                          Insert
                        </Button>
                      </div>
                    </div>
                  </TabPane>
                </TabContent>
              </Col>
            </Row>
          </ModalBody>
        </Modal>

        {/* Share post with others modal */}
        <Modal
          isOpen={this.state.sharePostModal}
          toggle={this.sharePostModalToggle}
        >
          <ModalHeader toggle={this.sharePostModalToggle}>
            Share Post with...
          </ModalHeader>
          <ModalBody>
            <Nav tabs className="nav-justified">
              {(this.state.shareWith.selected === "Favourites" ||
                this.state.shareWith.selected === "Custom") && (
                  <NavItem>
                    <NavLink
                      href="#"
                      active={this.state.sharePostWithType === "Favourites"}
                      onClick={() => {
                        this.setState({ sharePostWithType: "Favourites" });
                      }}
                    >
                      Favourites
                  </NavLink>
                  </NavItem>
                )}
              {(this.state.shareWith.selected === "Wants" ||
                this.state.shareWith.selected === "Custom") && (
                  <NavItem>
                    <NavLink
                      href="#"
                      active={this.state.sharePostWithType === "Wants"}
                      onClick={() => {
                        this.setState({ sharePostWithType: "Wants" });
                      }}
                    >
                      Wants
                  </NavLink>
                  </NavItem>
                )}
              {(this.state.shareWith.selected === "Recommenders" ||
                this.state.shareWith.selected === "Custom") && (
                  <NavItem>
                    <NavLink
                      href="#"
                      active={this.state.sharePostWithType === "Recommenders"}
                      onClick={() => {
                        this.setState({ sharePostWithType: "Recommenders" });
                      }}
                    >
                      Recommends
                  </NavLink>
                  </NavItem>
                )}
            </Nav>
            <TabContent activeTab={this.state.sharePostWithType}>
              {(this.state.shareWith.selected === "Favourites" ||
                this.state.shareWith.selected === "Custom") && (
                  <TabPane tabId="Favourites">Favourites</TabPane>
                )}
              {(this.state.shareWith.selected === "Wants" ||
                this.state.shareWith.selected === "Custom") && (
                  <TabPane tabId="Wants">Wants</TabPane>
                )}
              {(this.state.shareWith.selected === "Recommenders" ||
                this.state.shareWith.selected === "Custom") && (
                  <TabPane tabId="Recommenders">Recommends</TabPane>
                )}
            </TabContent>
          </ModalBody>
          <ModalFooter className="p-0">
            <div className="text-right py-2 m-0">
              <Button
                color="primary"
                size="sm"
                onClick={this.sharePostModalToggle}
              >
                Cancel
              </Button>
              <Button color="primary" size="sm">
                Submit
              </Button>
            </div>
          </ModalFooter>
        </Modal>

        {/* Copy post to other branches Modal */}
        <Modal
          scrollable
          size="lg"
          isOpen={this.state.copyPostToBranchesModal}
          toggle={() => this.setState({ copyPostToBranchesModal: !this.state.copyPostToBranchesModal })}
        >
          <div className="bg-white modal-header">
            <div className="flex-grow-1">
              <h5 className="modal-title text-dark">Copy this post to other branches</h5>

              <Row>
                <Col xs={{ size: "auto", order: 2 }} lg={{ order: 1 }} className="align-self-center">
                  <CustomInput
                    type="checkbox"
                    id={"popupSelectallBranchesForPost"}
                    name="popupSelectallBranchesForPost"
                    onChange={""}
                    checked={this.state.popupCheckAll}
                    onClick={this.handlePopupAllCheckedBranch}
                  >
                    <Label
                      for={"popupSelectallBranchesForPost"}
                      className="text-dark font-weight-normal fs-14"
                    >
                      Select/Deselect All
                    </Label>
                  </CustomInput>
                </Col>
                <Col xs={{ size: 12, order: 1 }} lg={{ size: "", order: 2 }}>
                  <Row>
                    <Col md>
                      <FormGroup>
                        <Label for="popupselectCountry">Select Country</Label>
                        <Input
                          className="primary p-1 fs-14"
                          type="select"
                          value={country}
                          name="popup_select_country"
                          onChange={this.handlePopupChange}
                          id="popupselectCountry"
                        >
                          <option>--Select--</option>
                          {typeof countryList !== "undefined" &&
                            countryList.length > 0 ? (
                              countryList.map((dropdown, index) => {
                                return (
                                  <option
                                    key={index}
                                    dropdown={dropdown}
                                    value={dropdown}
                                  >
                                    {dropdown}
                                  </option>
                                );
                              })
                            ) : (
                              <option> </option>
                            )}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col md>
                      <FormGroup>
                        <Label for="popupselectState">Select State</Label>
                        <Input
                          className="primary p-1 fs-14"
                          type="select"
                          name="popup_select_state"
                          value={stateName}
                          id="popupselectState"
                          onChange={this.handlePopupChange}
                        >
                          <option>--Select--</option>
                          {typeof countryState !== "undefined" &&
                            countryState.length > 0 ? (
                              countryState.map((dropdown, index) => {
                                return (
                                  <option
                                    key={index}
                                    dropdown={dropdown}
                                    value={dropdown}
                                  >
                                    {dropdown}
                                  </option>
                                );
                              })
                            ) : (
                              <option> </option>
                            )}
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
            {/* <div className="align-self-start">
              <button
                className="btn"
                aria-label="Close"
                onClick={() => this.setState({ copyPostToBranchesModal: false })}
              >
                <span aria-hidden="true">×</span>
              </button>
            </div> */}
          </div>
          <ModalBody className="text-dark">
            <div>
              {popupNewBranches &&
                Array.isArray(popupNewBranches) &&
                popupNewBranches.length > 0 ? (
                  popupNewBranches.map((item, index) => (
                    <CustomInput
                      type="checkbox"
                      id={"popup_post_select_branch_" + item.address_id}
                      name="popup_select_branch"
                      key={item.address_id}
                      onChange={(evt) => { this.handlePopupChange(evt, item) }}
                      // value={item.isChecked}
                      checked={item?.isChecked ? true : false}
                    >
                      <Label
                        for={"popup_post_select_branch_" + item.address_id}
                        className="text-dark font-weight-normal fs-14"
                      >
                        {item.name +
                          " ( " +
                          item.address1 +
                          ", " +
                          item.city +
                          ", " +
                          item.state +
                          ", " +
                          item.country +
                          ")"}
                      </Label>
                    </CustomInput>
                  ))
                ) : (
                  <div className="bg-white p-3">
                    <h2 className="text-secondary-dark">
                      No Company to Display
                                  </h2>
                  </div>
                )}
            </div>
          </ModalBody>
          <ModalFooter className="bg-white">
            <Button
              color="primary"
              className="mw"
              onClick={this.toggleModal}
            >
              Cancel
              </Button>
            <Button
              color="primary"
              className="mw"
              disabled={this.state.popupUserEntryBranchIDs.length > 0 ? false : true}
              onClick={this.handleSubmitCopyPost}
            >
              Post
              </Button>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

const mapState = (state) => ({
  my_posts_list: state.user.my_posts_list,
  share_with_list: state.user.share_with_list,
  album_types_list: state.user.album_types_list,
  album_type_data: state.user.album_type_data,
  profile_data: state.user.current_user,
  corporate_id: state.user.corporate_id,
  all_branches: state.user.all_branches,
  getbranch_copy: state.user.getbranch_copy,

});

const mapProps = {
  get_my_posts_list,
  edit_my_post,
  delete_my_post,
  delete_my_post_comment,
  add_my_post,
  add_my_post_comment,
  add_my_post_reply,
  get_share_with_list,
  get_album_types_list,
  get_album_type_data,
  delete_selected_gallery_media,
  get_branch_copy
};

export default connect(mapState, mapProps)(MyPosts);
