import React, { Component } from "react";
import {
  Button,
  ButtonGroup,
  Media,
  FormGroup,
  Input,
  Badge,
  Label,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Form,
} from "reactstrap";
import { connect } from "react-redux";
import { get_messages_list, add_message } from "../../../actions/user";
import Moment from "react-moment";

class Messages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewMessagesType: "call_to_action",
      viewMessagesSubType: "inbox",
      profilePic: "",
      message: {},
      messagesList: {},
    };
  }
  componentWillReceiveProps(nextProps) {
    //get messages list
    if (
      nextProps.messages_list &&
      Object.keys(nextProps.messages_list).length > 0
    ) {
      let messageObj = {};
      if (
        nextProps.messages_list.count &&
        nextProps.messages_list.results.length > 0
      ) {
        let messagesList = nextProps.messages_list.results.map((message) => ({
          conversation: message.conversation,
        }));
		messagesList.map((onDateMessage) => {
          let keys = Object.keys(onDateMessage.conversation);
		  keys.map((keyDate) => {
            messageObj[keyDate] = "";
          });
        });
      }
      this.setState({
        ...this.state,
        messagesList: nextProps.messages_list,
        profilePic: nextProps.loggedInUser
          ? nextProps.loggedInUser.current_profile_file
          : "",
        message: {
          ...this.state.message,
          ...messageObj,
        },
      });
    }
  }
  componentDidMount() {
    this.props.get_messages_list(
      this.state.viewMessagesType,
      this.state.viewMessagesSubType
    );
  }
  handleOnClickMessageType = (viewMessagesType, viewMessagesSubType) => {
    this.props.get_messages_list(viewMessagesType, viewMessagesSubType);
    this.setState({
      viewMessagesType,
      viewMessagesSubType,
    });
  };
  handleOnChange = (keyDate) => (e) => {
    this.setState({
      ...this.state,
      message: {
        ...this.state.message,
        [keyDate]: e.target.value,
      },
    });
  };
  handleOnMessageSubmit = (onDateMessage, keyDate) => (e) => {
    e.preventDefault();
    if (this.state.message[keyDate] !== "") {
      let message = {
        ...onDateMessage,
        reply: this.state.message[keyDate],
        body: this.state.message[keyDate],
      };
      delete message.conversation;
      this.props.add_message(
        message,
        this.state.viewMessagesType,
        this.state.viewMessagesSubType
      );
      this.setState({
        ...this.state,
        message: {
          ...this.state.message,
          [keyDate]: "",
        },
      });
    }
  };
  getMappedDiv = (onDateMessage) => {
    let keys = Object.keys(onDateMessage.conversation);
	let result = keys.map((keyDate) => {
      return (
        <>

          <div className="bg-white p-3 mb-3">
            <div className="text-center small">
              <Moment format="MMM DD, YYYY">{keyDate}</Moment>
            </div>
            <div>
              {/* Message list */}
              {onDateMessage.conversation[keyDate].map((item) => {
                return (
                  <Media className="media-post mt-2 bg-light p-3" key={item.id}>

                    <Media>
                      <img
                        className="media-object"
                        // object
                        src={
                          item.current_profile_pic
                            ? item.current_profile_pic
                            : require("../../../assets/images/user.png")
                        }
                        onError={(error) =>
                          (error.target.src = require("../../../assets/images/user.png"))
                        }
                        alt={item?.sender}
                      />
                    </Media>
                    <Media body>
                      <Media heading>
                        <div className="d-flex mx-n2 justify-content-between">
                          <div className="px-2">
                            <a href="#">{item.sender}</a>
                          </div>
                          <div className="px-2">
                            <span className="time-stamp text-primary-dark">
                              <span>&middot;</span>{" "}
                              <Moment format="MMM DD, YYYY h:mm A">
                                {item.sent_at}
                              </Moment>
                            </span>
                          </div>
                        </div>
                      </Media>
                      <div>{item.body}</div>
                    </Media>
                  </Media>
                );
              })}
            </div>
            {/* Reply Message */}
            <div>
              <Media className="media-post mt-3">
                <Media>
                  <Media
                    object
                    src={
                      this.state.profilePic
                        ? this.state.profilePic
                        : require("../../../assets/images/user.png")
                    }
                    alt="User Image"
                  />
                </Media>
                <Media body>
                  <FormGroup className="mb-0">
                    <Input
                      bsSize="sm"
                      className="mb-2"
                      type="textarea"
                      name="reply"
                      placeholder="Write a message..."
                      value={this.state.message[keyDate]}
                      onChange={this.handleOnChange(keyDate)}
                    />
                    <div className="text-right">
                      <div className="text-right">
                        <Button
                          size="sm"
                          color="primary"
                          onClick={this.handleOnMessageSubmit(
                            onDateMessage,
                            keyDate
                          )}
                        >
                          Submit
                      </Button>
                      </div>
                    </div>
                  </FormGroup>
                </Media>
              </Media>
            </div>
          </div>
        </>
      );
    });
    return result;
  };
  render() {
    const { viewMessagesType, viewMessagesSubType, messagesList } = this.state;
    return (
      <React.Fragment>

        {/* New Design */}
        <div hidden>
          {/* Filters */}
          <div className="form-inline mb-3">
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label className="mr-sm-2 text-secondary-dark" size="sm">Sorted By</Label>
              <Input className="light" type="select" name="select" bsSize="sm">
                <option>Latest</option>
                <option>Oldest</option>
              </Input>
            </FormGroup>
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label className="mr-sm-2 text-secondary-dark" size="sm">Showing</Label>
              {/* <Input className="light" type="select" name="select" bsSize="sm">
                <option>All</option>
                <option>Red Alert</option>
                <option>Orange Alert</option>
                <option>Good Standing</option>
              </Input> */}
              <UncontrolledDropdown>
                <DropdownToggle
                  className="d-inline-block text-left text-dark bg-white"
                  color="white"
                  size="sm"
                  caret
                >
                  <span className="text-dark fs-12 d-inline-block"
                    style={{ minWidth: '100px' }}>
                    All
                  </span>
                </DropdownToggle>
                <DropdownMenu
                  className="dropdown-list"
                >
                  <DropdownItem>
                    <div className="d-flex align-items-center">
                      <div className="text-danger font-weight-bold mr-2">
                        Red Alert
                      </div>
                      <div className="ml-auto">
                        <Badge color="danger" className="badge-empty rounded-circle">&nbsp;</Badge>
                      </div>
                    </div>
                  </DropdownItem>
                  <DropdownItem>
                    <div className="d-flex align-items-center">
                      <div className="text-warning font-weight-bold mr-2">
                        Orange Alert
                      </div>
                      <div className="ml-auto">
                        <Badge color="warning" className="badge-empty rounded-circle">&nbsp;</Badge>
                      </div>
                    </div>
                  </DropdownItem>
                  <DropdownItem>
                    <div className="d-flex align-items-center">
                      <div className="text-success font-weight-bold mr-2">
                        Good Standing
                      </div>
                      <div className="ml-auto">
                        <Badge color="success" className="badge-empty rounded-circle">&nbsp;</Badge>
                      </div>
                    </div>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </FormGroup>
          </div>

          <div>
            {/* First Item */}
            <div>
              <div className="bg-danger" style={{ minHeight: '9px' }}></div>
              <div className="py-3 px-4 bg-white">
                <div className="d-flex mx-n2">
                  <div className="px-2">
                    <img className="img-circle _50x50" src="https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/test.4fe86463aab3dbd5fdcc5f02cf8d12ca54aef58e.jpg" alt="" />
                  </div>
                  <div className="px-2 flex-grow-1 text-dark">
                    <div className="d-flex mx-n2 fs-13 mb-1">
                      <div className="px-2">
                        <span className="font-weight-bold">From:</span>
                        <span>Kimberly Johnson</span>
                      </div>
                      <div className="ml-auto col-auto px-2">
                        September 9, 2017
                      </div>
                    </div>
                    <div className="font-weight-bold mb-2">
                      Reservation Saturdary, Nov 5th 2017
                    </div>
                    <div className="font-weight-bold fs-14">
                      <span className="mr-3">Zip: 10001</span>
                      <span>Phone: 7180987654</span>
                    </div>
                    <div className="fs-14">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. A accusantium, quasi officiis adipisci facilis doloremque eum. Voluptate blanditiis quidem tempore. Cum, incidunt, minima nihil eligendi atque similique nesciunt maxime consequatur, sed commodi non porro. Odit voluptas neque voluptatum, iure, explicabo eos dolore magni autem sed soluta, veniam cupiditate. Labore, quaerat?
                    </div>

                    <div className="d-flex align-items-center mt-2">
                      <Button color="link" className="text-primary font-weight-bold fs-14 px-0" size="sm">Open</Button>
                      <span className="mx-2">|</span>
                      <Button color="link" className="text-primary font-weight-bold fs-14 px-0" size="sm">Mark as read</Button>
                      <span className="mx-2">|</span>
                      <Button color="link" className="text-primary font-weight-bold fs-14 px-0" size="sm">Delete</Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className="bg-warning" style={{ minHeight: '9px' }}></div>
              <div className="py-3 px-4 bg-white">
                <div className="d-flex mx-n2">
                  <div className="px-2">
                    <img className="img-circle _50x50" src="https://userdatawikireviews.s3.amazonaws.com/media/thumbnails/test.4fe86463aab3dbd5fdcc5f02cf8d12ca54aef58e.jpg" alt="" />
                  </div>
                  <div className="px-2 flex-grow-1 text-dark">
                    <div className="d-flex mx-n2 fs-13 mb-1">
                      <div className="px-2">
                        <span className="font-weight-bold">From:</span>
                        <span>Kimberly Johnson</span>
                      </div>
                      <div className="ml-auto col-auto px-2">
                        September 9, 2017
                      </div>
                    </div>
                    <div className="mb-2">
                      Consultation Saturdary, Nov 5th 2017
                    </div>
                    <div className="fs-14">
                      <span className="mr-3">Zip: 10001</span>
                      <span>Phone: 7180987654</span>
                    </div>
                    <div className="fs-14">
                      Lorem ipsum dolor sit amet consectetur adipisicing elit. A accusantium, quasi officiis adipisci facilis doloremque eum. Voluptate blanditiis quidem tempore. Cum, incidunt, minima nihil eligendi atque similique nesciunt maxime consequatur, sed commodi non porro. Odit voluptas neque voluptatum, iure, explicabo eos dolore magni autem sed soluta, veniam cupiditate. Labore, quaerat?
                    </div>

                    <div className="d-flex align-items-center mt-2">
                      <Button color="link" className="text-primary font-weight-bold fs-14 px-0" size="sm">Open</Button>
                      <span className="mx-2">|</span>
                      <Button color="link" className="text-primary font-weight-bold fs-14 px-0" size="sm">Mark as read</Button>
                      <span className="mx-2">|</span>
                      <Button color="link" className="text-primary font-weight-bold fs-14 px-0" size="sm">Delete</Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="text-primary">
          <div className="mb-3">
            <ButtonGroup className="type-filter flex-wrap" size="sm">
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={() =>
                    this.handleOnClickMessageType("call_to_action", "inbox")
                  }
                  active={viewMessagesType === "call_to_action"}
                >
                  Call to Action Messages{" "}
                  {this.props.branchMessageCount === null && this.props.ownerMessageCount &&
                    (
                      <Badge color="primary">
                        {this.props.ownerMessageCount.action_response_count}
                      </Badge>
                    )}
                  {this.props.branchMessageCount !== null && (
                    <Badge color="primary">
                      {this.props.branchMessageCount.action_response_count}
                    </Badge>
                  )}
                </Button>
              </div>
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={() => this.handleOnClickMessageType("business", "")}
                  active={viewMessagesType === "business"}
                >
                  Business Messages{" "}
                  {this.props.branchMessageCount === null && this.props.ownerMessageCount &&
                    (
                      <Badge color="primary">
                        {this.props.ownerMessageCount.message_count}
                      </Badge>
                    )}
                  {this.props.branchMessageCount !== null && (
                    <Badge color="primary">
                      {this.props.branchMessageCount.message_count}
                    </Badge>
                  )}
                </Button>
              </div>
            </ButtonGroup>
          </div>

          <div
            className="mb-3"
            hidden={viewMessagesType === "call_to_action" ? false : true}
          >
            <ButtonGroup className="type-filter flex-wrap" size="sm">
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={() =>
                    this.handleOnClickMessageType("call_to_action", "inbox")
                  }
                  active={
                    viewMessagesType === "call_to_action" &&
                    viewMessagesSubType === "inbox"
                  }
                >
                  Inbox
              </Button>
              </div>
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={() =>
                    this.handleOnClickMessageType("call_to_action", "draft")
                  }
                  active={
                    viewMessagesType === "call_to_action" &&
                    viewMessagesSubType === "draft"
                  }
                >
                  Draft
              </Button>
              </div>
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={() =>
                    this.handleOnClickMessageType("call_to_action", "sent")
                  }
                  active={
                    viewMessagesType === "call_to_action" &&
                    viewMessagesSubType === "sent"
                  }
                >
                  Sent
              </Button>
              </div>
            </ButtonGroup>
          </div>

          {/* If Messages */}
          {messagesList.count ? (
            messagesList.results
              .map((message) => ({
                parent_id_id: message.id,
                receiver: message.sender_id,
                user_entry_id: message.userentry,
                conversation: message.conversation,
              }))
              .map((onDateMessage) => {
                let content = this.getMappedDiv(onDateMessage);
                return content;
              })
          ) : (
              <div className="bg-white p-3">
                <h2 className="text-secondary-dark">No Messages to Display</h2>
              </div>
            )}
        </div>
      </React.Fragment>
    );
  }
}

const mapState = (state) => ({
  messages_list: state.user.messages_list,
  loggedInUser: state.user.current_user,
  ownerMessageCount: state.user.owner_message_count,
  branchMessageCount: state.user.branch_message_count,
});

const mapProps = {
  get_messages_list,
  add_message,
};

export default connect(mapState, mapProps)(Messages);
