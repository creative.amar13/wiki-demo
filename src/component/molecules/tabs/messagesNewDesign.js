import React, { Component } from "react";
import {
  Button,
  ButtonGroup,
  Media,
  FormGroup,
  Input,
  Badge,
  Form,
  Modal,
  ModalHeader,
  ModalBody
} from "reactstrap";
import { connect } from "react-redux";
import {
  get_messages_list,
  add_message,
  set_messages_action,
  set_messages_submit_draft,
  set_drafted_messages,
  delete_message,
  get_branch_data_by_circle_click
} from "../../../actions/user";
import Moment from "react-moment";
import { Link } from "react-router-dom";
import Filteration from '../../atoms/Filteration';

class Messages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: 'Red',
      viewMessagesType: "call_to_action",
      viewMessagesSubType: "inbox",
      profilePic: "",
      message: {},
      messagesList: {},
      replyToMessage: false,
      replyToMessageObj: {},
      replyToMessageID: null,
      replyToMessageText: null,
      editMessageValue: {},
      deleteMessageID: '',
      sortyBy: "Latest"
    };
  }
  componentWillReceiveProps(nextProps) {
    //get messages list
    let { sortyBy } = this.state;
    if (this.state.viewMessagesType !== 'call_to_action') {
      if (
        nextProps.messages_list &&
        Object.keys(nextProps.messages_list).length > 0
      ) {
        let messageObj = {};
        if (
          nextProps.messages_list.count &&
          nextProps.messages_list.results.length > 0
        ) {
          let messagesList = nextProps.messages_list.results.map((message) => ({
            conversation: message.conversation,
          }));
          messagesList.map((onDateMessage) => {
            let keys = Object.keys(onDateMessage.conversation);
            keys.map((keyDate) => {
              messageObj[keyDate] = "";
            });
          });
        }
        this.setState({
          ...this.state,
          messagesList: nextProps.messages_list,
          profilePic: nextProps.loggedInUser
            ? nextProps.loggedInUser.current_profile_file
            : "",
          message: {
            ...this.state.message,
            ...messageObj,
          },
        });
      }
    } else {
      if (
        nextProps.messages_list &&
        Object.keys(nextProps.messages_list).length > 0
      ) {
        if (
          nextProps.messages_list.count &&
          nextProps.messages_list.results.length > 0
        ) {

          this.setState({
            ...this.state,
            messagesList: nextProps.messages_list,
          });
        }
      }
    }
  }
  componentDidMount() {
    const { mapBranchClickData } = this.props;
    if (mapBranchClickData === true) {
      // this.handleOnClickMessageType("business", "")
      this.setState({
        viewMessagesType: 'business',
        viewMessagesSubType: '',
      });
      const { dotSelectedType, dotSelectedColor, dotSelectedName, dotSelectedDays, dotSelectedqueryType, dotSelectedBranchId } = this.props;
      let data_type = { 'reviews': '4', 'message': '5', 'qa': '7', 'feedback': '6', 'disputed_Review': '4' }
      this.props.get_branch_data_by_circle_click({
        color: dotSelectedColor,
        type: dotSelectedType,
        days: dotSelectedDays,
        queryType: dotSelectedqueryType,
        branchID: dotSelectedBranchId,
      });
    } else {
      this.props.get_messages_list(
        this.state.viewMessagesType,
        this.state.viewMessagesSubType
      );
    }
  }
  componentWillUnmount() {
    if (this.props.circleClick && this.props.circleClick.topBarMessage) {
      this.props.circleClick.topBarMessage = false;
      this.props.circleClick.topBarMessageColor = null;
    }

  }

  componentDidUpdate(prevProps) {

    if (this.props.mapBranchClickData !== prevProps.mapBranchClickData) {
      if (this.props.mapBranchClickData === true) {
        // this.handleOnClickMessageType("business", "")
        this.setState({
          viewMessagesType: 'business',
          viewMessagesSubType: '',
        });
      }
    }
  }

  handleOnClickMessageType = (viewMessagesType, viewMessagesSubType) => {
    this.props.get_messages_list(viewMessagesType, viewMessagesSubType);
    this.setState({
      viewMessagesType,
      viewMessagesSubType,
    });
  };
  handleOnChange = (keyDate) => (e) => {
    this.setState({
      ...this.state,
      message: {
        ...this.state.message,
        [keyDate]: e.target.value,
      },
    });
  };
  handleOnMessageSubmit = (onDateMessage, keyDate) => (e) => {
    const { mapBranchClickData } = this.props

    e.preventDefault();
    if (this.state.message[keyDate] !== "") {
      let message = {
        ...onDateMessage,
        reply: this.state.message[keyDate],
        body: this.state.message[keyDate],
      };
      delete message.conversation;
      this.props.add_message(
        message,
        this.state.viewMessagesType,
        this.state.viewMessagesSubType,
        mapBranchClickData
      );
      this.setState({
        ...this.state,
        message: {
          ...this.state.message,
          [keyDate]: "",
        },
      });
    }
  };
  getMappedDiv = (onDateMessage) => {
    const { circleClick } = this.props;
    if (onDateMessage?.conversation) {
      let keys = Object.keys(onDateMessage?.conversation);
      keys.sort((a, b) => a.keyDate - b.keyDate)
      let result = keys.map((keyDate) => {
        return (
          <>
            {circleClick && circleClick.topBarMessage ?
              <div className={
                circleClick && circleClick.topBarMessageColor === 'red' ?
                  "bg-danger text-dark p-2" :
                  circleClick && circleClick.topBarMessageColor === 'orange' ?
                    "bg-warning text-dark p-2" :
                    circleClick && circleClick.topBarMessageColor === 'green' ?
                      "bg-success text-dark p-2" : ""
              }>
                <div hidden className="d-flex flex-wrap mx-2">
                  <div className="text-white px-2">
                    <span>&nbsp;</span>Message
                    </div>
                </div>
                {/* <div className="d-flex justify-content-end align-item-center text-white p-1">
                  <h4 className="fs-18">userDetail?.name</h4>
                  <img
                    className="ml-2 img-circle _20x20"
                    // src={userDetail?.profile_pic} 
                    // alt={userDetail.name}
                    src={require("../../../assets/images/user.png")}
                    alt={'userDetail.name'}
                    onError={(error) =>
                      (error.target.src = require("../../../assets/images/user.png"))
                    }
                  />
                </div> */}
              </div> : ""}
            <div className="bg-white p-3 mb-3">
              <div className="text-center small">
                <Moment format="MMM DD, YYYY">{keyDate}</Moment>
              </div>
              <div>
                {/* Message list */}
                {onDateMessage.conversation[keyDate].map((item) => {
                  return (
                    <Media className="media-post mt-2 bg-light p-3" key={item.id}>

                      <Media>
                        <img
                          className="media-object"
                          // object
                          src={
                            item.current_profile_pic
                              ? item.current_profile_pic
                              : require("../../../assets/images/user.png")
                          }
                          onError={(error) =>
                            (error.target.src = require("../../../assets/images/user.png"))
                          }
                          alt="User Image"
                        />
                      </Media>
                      <Media body>
                        <Media heading>
                          <div className="d-flex mx-n2 justify-content-between">
                            <div className="px-2">
                              <a href="#">{item.sender}</a>
                            </div>
                            <div className="px-2">
                              <span className="time-stamp text-primary-dark">
                                <span>&middot;</span>{" "}
                                <Moment format="MMM DD, YYYY h:mm A">
                                  {item.sent_at}
                                </Moment>
                              </span>
                            </div>
                          </div>
                        </Media>
                        <div>{item.body}</div>
                      </Media>
                    </Media>
                  );
                })}
              </div>
              {/* Reply Message */}
              <div>
                <Media className="media-post mt-3">
                  <Media>
                    <Media
                      object
                      src={
                        this.state.profilePic
                          ? this.state.profilePic
                          : require("../../../assets/images/user.png")
                      }
                      alt="User Image"
                    />
                  </Media>
                  <Media body>
                    <FormGroup className="mb-0">
                      <Input
                        bsSize="sm"
                        className="mb-2"
                        type="textarea"
                        name="reply"
                        placeholder="Write a message..."
                        value={this.state.message[keyDate]}
                        onChange={this.handleOnChange(keyDate)}
                      />
                      <div className="text-right">
                        <div className="text-right">
                          <Button
                            size="sm"
                            color="primary"
                            onClick={this.handleOnMessageSubmit(
                              onDateMessage,
                              keyDate
                            )}
                          >
                            Submit
                      </Button>
                        </div>
                      </div>
                    </FormGroup>
                  </Media>
                </Media>
              </div>
            </div>
          </>
        );
      });
      return result;
    }
  };

  getBusinessMappedDiv = (onDateMessage) => {
    const { viewMessagesSubType } = this.state;

    if (viewMessagesSubType === 'inbox') {
      let userDetail = onDateMessage?.user_details;
      const data = { "read_unread": onDateMessage?.read_unread ? 1 : 0 };
      if (onDateMessage?.user_details) {
        return (
          <div className="mb-2">
            {/* First Item */}
            <div hidden className="bg-warning" style={{ minHeight: '9px' }}>
              <div className="d-flex justify-content-end align-item-center text-white p-1">
                <h4>{userDetail?.name}</h4>
                <img
                  className="ml-2 img-circle _30x30"
                  src={userDetail?.profile_pic} alt={userDetail.name}
                  onError={(error) =>
                    (error.target.src = require("../../../assets/images/user.png"))
                  }
                />
              </div>
            </div>
            <div className="py-3 px-4 bg-white">
              <div className="d-flex mx-n2">
                <div className="px-2">
                  <img className="img-circle _50x50"
                    src={userDetail?.profile_pic} alt={userDetail.name}
                    onError={(error) =>
                      (error.target.src = require("../../../assets/images/user.png"))
                    }
                  />
                </div>
                <div className="px-2 flex-grow-1 text-dark">
                  <div className="d-flex mx-n2 fs-13 mb-1">
                    <div className="px-2">
                      <span className="font-weight-bold ">From: </span>
                      <span>{userDetail.name}</span>
                    </div>
                    <div className="ml-auto col-auto px-2">
                      <div className="text-center small">
                        <Moment format="MMM DD, YYYY">{onDateMessage.created_on}</Moment>
                      </div>
                    </div>
                  </div>
                  <div className="font-weight-bold mb-2">
                    {onDateMessage.call_type || ''}, <Moment format="MMM DD, YYYY">{onDateMessage.created_on}</Moment>

                  </div>
                  <div className="font-weight-bold fs-14">
                    {onDateMessage?.zipcode ? <span className="mr-3">Zip: {onDateMessage.zipcode}</span> : ""}
                    {onDateMessage?.phone_number ? <span>Phone: {onDateMessage.phone_number}</span> : ""}
                  </div>
                  <div className="fs-14">
                    {onDateMessage?.description || ''}
                  </div>

                  <div className="d-flex align-items-center mt-2">
                    <Button color="link" className="text-primary font-weight-bold fs-14 px-0" size="sm"
                      onClick={async () => {
                        await this.setState({
                          replyToMessage: true,
                          replyToMessageID: onDateMessage.id,
                          replyToMessageObj: onDateMessage
                        })
                      }}
                    >Open</Button>
                    <span className="mx-2">|</span>
                    <Button color="link" className="text-primary font-weight-bold fs-14 px-0" size="sm"
                      onClick={() => {
                        this.props.set_messages_action(onDateMessage.id, data, this.state.viewMessagesType, this.state.viewMessagesSubType)
                      }}
                    >{onDateMessage?.read_unread ? 'Mark as read' : 'Mark as unread'}</Button>
                    <span className="mx-2">|</span>
                    <Button color="link" className="text-primary font-weight-bold fs-14 px-0" size="sm"
                      onClick={() => {
                        this.deleteToggle(onDateMessage.id,);
                      }}
                    >Delete</Button>
                  </div>
                  {
                    this.state.replyToMessage && this.state.replyToMessageID === onDateMessage.id ?
                      <div className="d-flex mx-n2 mt-2">
                        <div className="px-2">
                          <img className="img-circle _50x50"
                            src={this.props.loggedInUser?.current_profile_file}
                            onError={(error) =>
                              (error.target.src = require("../../../assets/images/user.png"))
                            } />
                        </div>
                        <div className="px-2 flex-grow-1 text-dark">
                          <Form>
                            <FormGroup>
                              <Input
                                type="textarea"
                                bsSize="sm"
                                name="replyToMessageText"
                                onChange={this.handleChange}
                                value={this.state.replyToMessageText}
                                className="border-primary ff-alt text-primary"
                                rows="2"
                              />
                            </FormGroup>
                            <div className="d-flex flex-column flex-sm-row mx-n2 justify-content-end">
                              <div className="px-2 mb-2">
                                <Button
                                  color="primary"
                                  size="sm"
                                  block
                                  onClick={() => {
                                    this.handleSubmitReply('submit')
                                  }}
                                >Submit</Button>
                              </div>
                              <div className="px-2 mb-2">
                                <Button
                                  color="primary"
                                  size="sm"
                                  block
                                  onClick={() => {
                                    this.handleSubmitReply('draft')
                                  }}
                                >Save Draft</Button>
                              </div>
                              <div className="px-2 mb-2">
                                <Button
                                  color="primary"
                                  size="sm"
                                  block
                                  onClick={() => {
                                    this.setState({
                                      replyToMessage: false,
                                      replyToMessageID: null,
                                      replyToMessageText: null,
                                      replyToMessageObj: {},
                                    })
                                  }}
                                >Cancel</Button>
                              </div>
                            </div>
                          </Form>
                        </div>
                      </div>
                      :
                      ''
                  }
                </div>
              </div>
            </div>
          </div>
        )
      }
    } else if (viewMessagesSubType === 'sent' || viewMessagesSubType === 'draft') {
      let sentMessageDetails = onDateMessage?.draft_msg;
      // let { editMessageValue } = this.state;

      // if (onDateMessage?.response) {
      //   editMessageValue[onDateMessage.id] = onDateMessage?.response;
      // }


      if (onDateMessage?.draft_msg) {
        return (
          <div className="mb-2 py-3 px-4 bg-white">
            <div className="d-flex mx-n2">
              <div className="px-2">
                <img className="img-circle _50x50"
                  src={sentMessageDetails?.user_details?.profile_pic}
                  onError={(error) =>
                    (error.target.src = require("../../../assets/images/user.png"))
                  }
                  alt={sentMessageDetails?.user_details?.name} />
              </div>
              <div className="px-2 flex-grow-1 text-dark-main">
                <div className="d-flex mx-n2 fs-13 mb-1">
                  <div className="px-2">
                    <span className="font-weight-bold">From:{" "}</span>
                    <span>{sentMessageDetails?.user_details?.name}</span>
                  </div>
                  <div className="ml-auto col-auto px-2">
                    <Moment format="MMM DD, YYYY">
                      {sentMessageDetails?.created_on}
                    </Moment>
                  </div>
                </div>
                <div className="font-weight-bold mb-2">
                  {sentMessageDetails?.call_type} <Moment format="MMM DD, YYYY">
                    {sentMessageDetails?.appointment_date}
                  </Moment>
                </div>
                <div className="font-weight-bold fs-14">
                  {sentMessageDetails?.zipcode ? <span className="mr-3">Zip: {sentMessageDetails?.zipcode}</span> : ""}
                  {sentMessageDetails?.phone_number ? <span>Phone: {sentMessageDetails?.phone_number}</span> : ""}
                </div>
                <div className="fs-14">
                  {sentMessageDetails?.description}
                </div>
                {viewMessagesSubType === 'draft' ?
                  <div className="d-flex mx-n2 mt-2">
                    <div className="px-2">
                      <img className="img-circle _50x50"
                        src={this.props.loggedInUser?.current_profile_file}
                        onError={(error) =>
                          (error.target.src = require("../../../assets/images/user.png"))
                        } />
                    </div>
                    <div className="px-2 flex-grow-1 text-dark">
                      <Form>
                        <FormGroup>
                          <Input
                            type="textarea"
                            bsSize="sm"
                            name="replyToMessageText"
                            onChange={this.handleDraftedMessageChange}
                            id={onDateMessage.id}
                            value={
                              this.state.editMessageValue[onDateMessage.id] ||
                                this.state.editMessageValue[onDateMessage.id] === '' ?
                                this.state.editMessageValue[onDateMessage.id] :
                                onDateMessage.response
                            }
                            className="border-primary ff-alt text-primary"
                            rows="2"
                          />
                        </FormGroup>
                        <div className="d-flex justify-content-end">
                          <Button
                            color="primary"
                            size="sm"
                            disabled={this.state.editMessageValue[onDateMessage.id] === '' ? true : false}
                            onClick={() => {
                              this.handleDraftedSubmitReply('Publish', this.state.editMessageValue[onDateMessage.id], onDateMessage.id)
                            }}
                          >Publish</Button>
                          <Button
                            color="primary"
                            size="sm"
                            onClick={() => {
                              this.handleDraftedSubmitReply('saveDraft', this.state.editMessageValue[onDateMessage.id], onDateMessage.id)
                            }}
                          >Save Draft</Button>
                        </div>
                      </Form>
                    </div>
                  </div>
                  :
                  <div className="d-flex mx-n2 mt-3">
                    <div className="px-2">
                      <img className="img-circle _40x40"
                        src={sentMessageDetails?.sender?.current_profile_pic}
                        onError={(error) =>
                          (error.target.src = require("../../../assets/images/user.png"))
                        }
                        alt={sentMessageDetails?.user_details?.name}
                      />
                    </div>
                    <div className="px-2 flex-grow-1 text-dark-main">
                      <div className="fs-14">
                        <Link className="text-reset" to="#">{sentMessageDetails?.sender?.name}</Link>
                        {" "}
                        <span className="text-tertiary">
                          {onDateMessage?.response}
                        </span>
                      </div>
                    </div>
                  </div>
                }
              </div>
            </div>

          </div>
        )
      }
    }
  }

  deleteToggle = (id,) => {
    this.setState({
      isDeleteToggle: !this.state.isDeleteToggle,
      deleteMessageID: id ? id : ""
    });

  }

  deleteBusinessRecord = () => {
    let { deleteMessageID, viewMessagesType, viewMessagesSubType } = this.state;
    this.props.delete_message(deleteMessageID, viewMessagesType, viewMessagesSubType);
    this.deleteToggle();
  }

  handleChange = (evt) => {
    let name = evt.target.name;
    let value = evt.target.value;
    this.setState({
      [name]: value
    })
  }

  handleSubmitReply = async (submitType) => {
    const {
      replyToMessageID,
      replyToMessageText,
      replyToMessageObj,
      viewMessagesType,
      viewMessagesSubType
    } = this.state
    const { set_messages_submit_draft } = this.props
    const data = {
      "response": replyToMessageText,
      "call_to_action_transaction_id": replyToMessageID,
      "parent_id": null,
      "reviews_userentries_id": 25811939,
      "to_user": replyToMessageObj?.user_details?.username,
      "is_draft": submitType === 'draft' ? true : false
    }
    set_messages_submit_draft(data, viewMessagesType, viewMessagesSubType);

    await this.setState({
      replyToMessage: false,
      replyToMessageID: null,
      replyToMessageText: null,
      replyToMessageObj: {},
    })
  }

  handleDraftedMessageChange = async (evt) => {
    let id = evt.target.id;
    let value = evt.target.value;
    let { editMessageValue } = this.state;
    editMessageValue[id] = value;
    await this.setState({ editMessageValue });
  }

  handleDraftedSubmitReply = (submitType, message, messageId) => {
    const { set_drafted_messages } = this.props;
    const {
      viewMessagesType,
      viewMessagesSubType
    } = this.state
    const data = {
      "response": message,
      "is_draft": submitType === 'saveDraft' ? true : false
    }
    set_drafted_messages(data, messageId, viewMessagesType, viewMessagesSubType)

  }

  sortbyfunction = (sortValue) => {
    let { messagesList } = this.state;
    if (sortValue && sortValue == "Oldest") {
      messagesList.results.sort((a, b) => a.id - b.id);
    } else {
      messagesList.results.sort((a, b) => b.id - a.id);
    }
    this.setState({ messagesList })
  }


  render() {
    const { viewMessagesType, viewMessagesSubType, messagesList, selected } = this.state;
    const { mapBranchClickData } = this.props;
    return (
      <React.Fragment>

        {/* New Design */}
        <div >
          {/* Filters */}
          {mapBranchClickData === true ?
            <Filteration
              dotSelectedType={this.props.dotSelectedType}
              dotSelectedColor={this.props.dotSelectedColor}
              dotSelectedName={this.props.dotSelectedName}
              dotSelectedDays={this.props.dotSelectedDays}
              dotSelectedqueryType={this.props.dotSelectedqueryType}
              dotSelectedBranchId={this.props.dotSelectedBranchId}
              sortbyfunction={this.sortbyfunction}
            /> : ""}

          {/* <div
            hidden={mapBranchClickData === true ? false : true}
            className="form-inline mb-3">
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label className="mr-sm-2 text-secondary-dark" size="sm">Sorted By</Label>
              <Input className="light" type="select" name="select" bsSize="sm"
                onClick={() => {
                  this.sortDataBytime();
                }}
              >
                <option>Latest</option>
                <option>Oldest</option>
              </Input>
            </FormGroup>
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label className="mr-sm-2 text-secondary-dark" size="sm">Showing</Label>
             
              <UncontrolledDropdown>
                <DropdownToggle
                  className="d-inline-block text-left text-dark bg-white"
                  color="white"
                  size="sm"
                  caret
                >
                  <span className="text-dark fs-12 d-inline-block"
                    style={{ minWidth: '100px' }}>
                    {selected}
                  </span>
                </DropdownToggle>
                <DropdownMenu
                  className="dropdown-list"
                >
                  <DropdownItem>
                    <div className="d-flex align-items-center"
                      onClick={() => this.handleOnClickAlert("Red", "red")}
                    >
                      <div className="text-danger font-weight-bold mr-2">
                        Red Alert
                      </div>
                      <div className="ml-auto">
                        <Badge color="danger" className="badge-empty rounded-circle">&nbsp;</Badge>
                      </div>
                    </div>
                  </DropdownItem>
                  <DropdownItem>
                    <div className="d-flex align-items-center"
                      onClick={() => this.handleOnClickAlert("Orange", "orange")}
                    >
                      <div className="text-warning font-weight-bold mr-2">
                        Orange Alert
                      </div>
                      <div className="ml-auto">
                        <Badge color="warning" className="badge-empty rounded-circle">&nbsp;</Badge>
                      </div>
                    </div>
                  </DropdownItem>
                  <DropdownItem>
                    <div className="d-flex align-items-center"
                      onClick={() => this.handleOnClickAlert("Good", "green")}
                    >
                      <div className="text-success font-weight-bold mr-2">
                        Good Standing
                      </div>
                      <div className="ml-auto">
                        <Badge color="success" className="badge-empty rounded-circle">&nbsp;</Badge>
                      </div>
                    </div>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </FormGroup>
          </div> */}

        </div>
        <div className="text-primary">
          <div
            hidden={mapBranchClickData === true ? true : false}
            className="mb-3">

            <ButtonGroup className="type-filter flex-wrap" size="sm">
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={() =>
                    this.handleOnClickMessageType("call_to_action", "inbox")
                  }
                  active={viewMessagesType === "call_to_action"}
                >
                  Call to Action Messages{" "}
                  {this.props.branchMessageCount === null && this.props.ownerMessageCount &&
                    (
                      <Badge color="primary">
                        {this.props.ownerMessageCount.action_response_count}
                      </Badge>
                    )}
                  {this.props.branchMessageCount !== null && (
                    <Badge color="primary">
                      {this.props.branchMessageCount.action_response_count}
                    </Badge>
                  )}
                </Button>
              </div>
              <Button
                color="transparent"
                onClick={() => this.handleOnClickMessageType("business", "")}
                active={viewMessagesType === "business"}
              >
                Business Messages{" "}
                {this.props.branchMessageCount === null && this.props.ownerMessageCount &&
                  (
                    <Badge color="primary">
                      {this.props.ownerMessageCount.message_count}
                    </Badge>
                  )}
                {this.props.branchMessageCount !== null && (
                  <Badge color="primary">
                    {this.props.branchMessageCount.message_count}
                  </Badge>
                )}
              </Button>
            </ButtonGroup>

          </div>

          <div
            className="mb-3"
            hidden={viewMessagesType === "call_to_action" ? false : true}
          >
            <ButtonGroup className="type-filter flex-wrap" size="sm">
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={async () => {
                    this.handleOnClickMessageType("call_to_action", "inbox");
                    await this.setState({
                      editMessageValue: {}
                    })
                  }
                  }
                  active={
                    viewMessagesType === "call_to_action" &&
                    viewMessagesSubType === "inbox"
                  }
                >
                  Inbox
              </Button>
              </div>
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={() =>
                    this.handleOnClickMessageType("call_to_action", "draft")
                  }
                  active={
                    viewMessagesType === "call_to_action" &&
                    viewMessagesSubType === "draft"
                  }
                >
                  Draft
              </Button>
              </div>
              <div className="item d-flex align-items-center">
                <Button
                  color="transparent"
                  onClick={async () => {
                    this.handleOnClickMessageType("call_to_action", "sent");
                    await this.setState({
                      editMessageValue: {}
                    })
                  }
                  }
                  active={
                    viewMessagesType === "call_to_action" &&
                    viewMessagesSubType === "sent"
                  }
                >
                  Sent
              </Button>
              </div>
            </ButtonGroup>
          </div>
          {/* If Messages */}
          {this.state.viewMessagesType !== 'call_to_action' ? messagesList.count ? (
            messagesList.results
              .map((message) => ({
                parent_id_id: message.id,
                receiver: message.sender_id,
                user_entry_id: message.userentry,
                conversation: message.conversation,
              }))
              .map((onDateMessage) => {
                let content = this.getMappedDiv(onDateMessage);
                return content;
              })
          ) : (
              <div className="bg-white p-3">
                <h2 className="text-secondary-dark">No Messages to Display</h2>
              </div>
            ) :
            messagesList.count ? (
              messagesList.results
                .map((message) => {
                  let content = this.getBusinessMappedDiv(message);
                  return content;
                })
            ) : (
                <div className="bg-white p-3">
                  <h2 className="text-secondary-dark">No Messages to Display</h2>
                </div>
              )
          }
        </div>
        <Modal isOpen={this.state.isDeleteToggle} toggle={this.deleteToggle}>
          <ModalHeader toggle={this.deleteToggle}></ModalHeader>
          <ModalBody className="text-center">
            <h2 className="mb-3">Confirmation</h2>
            <p className="small">Are you sure you want to delete?</p>

            <div className="pt-4">
              <div>
                <Button onClick={this.deleteToggle} size="md" color="primary">Cancel</Button>
                <Button onClick={this.deleteBusinessRecord} size="md" color="primary">Ok</Button>
              </div>
            </div>
          </ModalBody>
        </Modal>


      </React.Fragment>
    );
  }
}

const mapState = (state) => ({
  messages_list: state.user.messages_list,
  loggedInUser: state.user.current_user,
  ownerMessageCount: state.user.owner_message_count,
  branchMessageCount: state.user.branch_message_count,
  circleClick: state.circleClick,
});

const mapProps = {
  get_messages_list,
  add_message,
  set_messages_action,
  set_messages_submit_draft,
  set_drafted_messages,
  delete_message,
  get_branch_data_by_circle_click
};

export default connect(mapState, mapProps)(Messages);
